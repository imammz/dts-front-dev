"use strict";

// Class definition
var KTSubvitTrivia = function () {

	var stepper;
	
	var form;
	var formSubmitButton;
	var formContinueButton;
	// Variables
	var stepperObj;
	var validations = [];

	// Private Functions
	var initStepper = function () {
		// Initialize Stepper
		stepperObj = new KTStepper(stepper);

		/* stepperObj.on("kt.stepper.click", function (stepper) {
			stepper.goTo(stepper.getClickedStepIndex()); // go to clicked step
		}); */

		// Stepper change event
		stepperObj.on('kt.stepper.changed', function (stepper) {
			
		});

		// Validation before going to next page
		stepperObj.on('kt.stepper.next', function (stepper) {
			console.log('stepper.next');

			// Validate form before change stepper step
			var validator = validations[stepper.getCurrentStepIndex() - 1]; // get validator for currnt step

			if (validator) {
				validator.validate().then(function (status) {
					console.log('validated!');

					if (status == 'Valid') {
						stepper.goNext();

						KTUtil.scrollTop();
					} else {
						Swal.fire({
							title: "Maaf, masih ada yang belum diisi.",
							icon: "warning",
							// buttonsStyling: false,
							confirmButtonText: "Ok, dicek lagi"
							// customClass: {
							// 	confirmButton: "btn btn-light"
							// }
						}).then(function () {
							KTUtil.scrollTop();
						});
					}
				});
			} else {
				stepper.goNext();

				KTUtil.scrollTop();
			}
		});

		// Prev event
		stepperObj.on('kt.stepper.previous', function (stepper) {
			console.log('stepper.previous');

			stepper.goPrevious();
			KTUtil.scrollTop();
		});
	}

	return {
		// Public Functions
		init: function () {									
			stepper = document.querySelector('#kt_subvit_trivia');
			if (stepper) {
				form = stepper.querySelector('#kt_subvit_trivia_form');
				formSubmitButton = stepper.querySelector('[data-kt-stepper-action="submit"]');
				formContinueButton = stepper.querySelector('[data-kt-stepper-action="next"]');

				initStepper();								
			}

		}
	};
}();

// On document ready
KTUtil.onDOMContentLoaded(function () {
	KTSubvitTrivia.init();
});