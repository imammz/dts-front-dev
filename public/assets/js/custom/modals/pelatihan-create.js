// "use strict";

// Class definition
var KTCreateAccount = (function () {
  // Elements
  var modal;
  var modalEl;

  var stepper;
  var form;
  var formSubmitButton;
  var formSubmitButtonDummy;
  var formSimpanSilabus;
  var formContinueButton;
  var newPendaftarFormSimpan;

  // Variables
  var stepperObj;
  var validations = [];

  function fileToBase64(file) {
    var sizex;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      var arr = reader.result.split(",");
      // window.localStorage.setItem("upload_silabus", reader.result);
      // window.localStorage.setItem("upload_silabus_name", file.name);
      // window.localStorage.setItem("upload_silabus_size", file.size);
      //document.getElementByName('upload_silabus').classList.remove('Not_Valid');
      //alert(window.localStorage.getItem("upload_silabus"));
      //alert(file.size);
      sizex = file.size;
    };
    return sizex;
  }

  function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return "0 Bytes";

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  }

  function validateStep(stepper, tabNav = false, onValidate) {
    $("input[name='index_stepper']").val(stepper.getCurrentStepIndex() - 1);

    if (stepper.getCurrentStepIndex() - 1 == "1")
      $("div[id='reset_id']").css("display", "none");

    if (
      tabNav &&
      stepper.getClickedStepIndex() < stepper.getCurrentStepIndex()
    ) {
      onValidate();
      return;
    }
    // Validate form before change stepper step
    var validator = validations[stepper.getCurrentStepIndex() - 1]; // get validator for currnt step

    if (validator) {
      // cek tab silabus yang kebuka
      if (stepper.getCurrentStepIndex() == 4) {
        const validatorFields = validator.getFields();
        Object.keys(validatorFields).forEach((key) => {
          validator.removeField(key);
        });

        const formField = addFormValidation();
        Object.keys(formField).forEach((key) => {
          validator.addField(key, formField[key]);
        });
        // }
      } else if (stepper.getCurrentStepIndex() == 3) {
        if (document.getElementById("div_selsilabus").style.display == "") {
          const validatorFields = validator.getFields();
          Object.keys(validatorFields).forEach((key) => {
            validator.removeField(key);
          });
          validator.addField("select_silabus", {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          });
        }
        // else if (
        //   document.getElementById("div_createsilabus").style.display == ""
        // ) {
        //   const validatorFields = validator.getFields();
        //   Object.keys(validatorFields).forEach((key) => {
        //     validator.removeField(key);
        //   });

        //   const silabusField = silabusValidation();
        //   Object.keys(silabusField).forEach((key) => {
        //     validator.addField(key, silabusField[key]);
        //   });
        // }
      }
      validator.validate().then(function (status) {
        if (status == "Valid") {
          if (
            stepper.getCurrentStepIndex() == 4 &&
            document.getElementById("div_createform").style.display == ""
          ) {
            let checked = document.querySelectorAll(
              `div[data-rbd-draggable-id="column-draggable-2"] input[type='checkbox']:checked`
            );
            if (checked.length == 0) {
              Swal.fire({
                title: "Minimal harus terdapat 1 isian wajib (required)",
                icon: "warning",
                confirmButtonText: "Ok",
              }).then(function () {
                KTUtil.scrollTop();
              });
              return;
            }
          }
          setCookie(
            "pendaftaran_tanggal_mulai",
            $("input[name='pendaftaran_tanggal_mulai']").val(),
            1440
          );
          setCookie(
            "pendaftaran_tanggal_selesai",
            $("input[name='pendaftaran_tanggal_selesai']").val(),
            1440
          );
          setCookie(
            "pelatihan_tanggal_mulai",
            $("input[name='pelatihan_tanggal_mulai']").val(),
            1440
          );
          setCookie(
            "pelatihan_tanggal_selesai",
            $("input[name='pelatihan_tanggal_selesai']").val(),
            1440
          );
          // set cookie active stepper
          setCookie("step", stepper.getCurrentStepIndex(), 1440);
          // alert($("input[name='upload_silabus']")[0].files[0]);
          // if ($("input[name='upload_silabus']")[0].files[0] != undefined) {

          let title = "";
          const filex = $("input[name='upload_silabus']")[0].files[0];
          if ($("input[name='upload_silabus']")[0].files[0] == undefined) {
            title = "File silabus tidak boleh kosong";
          } else if (filex.size > 10000000) {
            // fileToBase64(filex);
            var filexconvert = formatBytes(filex.size);
            title = "Maaf, ukuran file terlalu besar (" + filexconvert + ")";
          }
          if (title.length > 0) {
            Swal.fire({
              title: title,
              icon: "warning",
              confirmButtonText: "Ok",
            }).then(function () {
              //donothing
              KTUtil.scrollTop();
            });
          } else {
            onValidate();
            KTUtil.scrollTop();
          }
          // stepper.goNext();
        } else {
          Swal.fire({
            title: "Maaf, masih terdapat error pada isian.",
            icon: "warning",
            // buttonsStyling: false,
            confirmButtonText: "Ok",
            // customClass: {
            // 	confirmButton: "btn btn-light"
            // }
          }).then(function () {
            KTUtil.scrollTop();
          });
        }
      });
    } else {
      onValidate();
      KTUtil.scrollTop();
    }
  }
  // Private Functions
  var initStepper = function () {
    // Initialize Stepper
    $("div[id='next_id']").css("display", "inline");
    stepperObj = new KTStepper(stepper);

    // Stepper change event
    stepperObj.on("kt.stepper.changed", function (stepper) {
      if (stepperObj.getCurrentStepIndex() === 5) {
        formSubmitButtonDummy.classList.remove("d-none");
        formSubmitButtonDummy.classList.add("d-inline-block");
        // formContinueButton.classList.add("d-none");
      } else {
        formSubmitButtonDummy.classList.remove("d-inline-block");
        formSubmitButtonDummy.classList.add("d-none");
        // formContinueButton.classList.remove("d-none");
      }
    });

    // stepperObj.on
    // Validation before going to next page
    stepperObj.on("kt.stepper.next", function (stepper) {
      validateStep(stepper, false, () => {
        stepper.goNext();
      });
    });
    stepperObj.on("kt.stepper.click", function (stepper) {
      validateStep(stepper, true, () => {
        stepper.goTo(stepper.getClickedStepIndex());
      });
    });

    // Prev event
    stepperObj.on("kt.stepper.previous", function (stepper) {
      if (stepper.getCurrentStepIndex() - 1 == "2")
        $("div[id='reset_id']").css("display", "inline");
      if (stepper.getCurrentStepIndex() - 1 == "1") {
        $("div[id='next_id']").css("display", "inline");
        $("div[id='reset_id']").css("display", "none");
        // $("div[id='div_selform']").css("display", "none");
        // $("div[id='div_createform']").css("display", "none");
      }
      stepper.goPrevious();
      KTUtil.scrollTop();
    });
  };

  var silabusValidation = function () {
    const fields = {
      nama_silabus: {
        validators: {
          notEmpty: {
            message: "Tidak boleh kosong",
          },
        },
      },
      total_jp: {
        validators: {
          notEmpty: {
            message: "Tidak boleh kosong",
          },
          formatJamSilabusCheck: {
            message: "Format input tidak sesuai.",
          },
          // maxJamSilabusCheck: {
          //   message: "Jam pelajaran silabus tidak boleh melebihi 100",
          // },
          jpnominusnumber: {
            message: "Angka harus melebihi 0",
          },
        },
      },
    };
    const silabusWrapper = document.getElementsByClassName("trigger-silabus");
    silabusWrapper.forEach((elem) => {
      const inputs = elem.querySelectorAll("input.silabus");
      fields[inputs[0].name] = {
        validators: {
          notEmpty: {
            message: "Tidak boleh kosong",
          },
        },
      };
      fields[inputs[1].name] = {
        validators: {
          notEmpty: {
            message: "Tidak boleh kosong",
          },
          sumjpcheck: {
            message:
              "Jumlah jam pelajaran harus sama dengan total jam pelajaran",
          },
          formatJamSilabusCheck: {
            message: "Format input tidak sesuai.",
          },
          // maxJamSilabusCheck: {
          //   message: "Jam pelajaran silabus tidak boleh melebihi 100",
          // },
          jpnominusnumber: {
            message: "Angka harus melebihi 0",
          },
        },
      };
    });
    return fields;
  };

  var handleTambahSilabus = function () {
    formSimpanSilabus.addEventListener("click", function (e) {
      // Validate form before change stepper step
      const validator = validations[2];
      // get validator for last form
      const validatorFields = validator.getFields();
      Object.keys(validatorFields).forEach((key) => {
        validator.removeField(key);
      });
      const silabusField = silabusValidation();
      Object.keys(silabusField).forEach((key) => {
        validator.addField(key, silabusField[key]);
      });
      validator.validate().then(function (status) {
        if (status == "Valid") {
          // Prevent default button action
          // e.preventDefault();

          document.getElementById("simpan-silabus").click();

          // Disable button to avoid multiple click
          formSimpanSilabus.disabled = true;

          // Show loading indication
          formSimpanSilabus.setAttribute("data-kt-indicator", "on");

          // Simulate form submission
          setTimeout(function () {
            // Hide loading indication
            formSimpanSilabus.removeAttribute("data-kt-indicator");

            // Enable button
            formSimpanSilabus.disabled = false;

            // stepperObj.goNext();
            //KTUtil.scrollTop();
          }, 2000);
        } else {
          Swal.fire({
            text: "Maaf, masih terdapat error pada isian.",
            icon: "error",
            buttonsStyling: false,
            confirmButtonText: "Ok",
            customClass: {
              confirmButton: "btn btn-light",
            },
          }).then(function () {
            KTUtil.scrollTop();
          });
        }
      });
    });
  };

  var addFormValidation = function () {
    const fields = {
      judul_form_builder: {
        validators: {
          notEmpty: {
            message: "Tidak boleh kosong",
          },
        },
      },
      status_publish: {
        validators: {
          notEmpty: {
            message: "Tidak boleh kosong",
          },
        },
      },
      board_value: {
        validators: {
          notEmpty: {
            message: "",
          },
        },
      },
    };

    return fields;
  };
  var handleTambahForm = function () {
    newPendaftarFormSimpan.addEventListener("click", function (e) {
      // Validate form before change stepper step
      const validator = validations[3];
      // get validator for last form
      const validatorFields = validator.getFields();
      Object.keys(validatorFields).forEach((key) => {
        validator.removeField(key);
      });
      const formField = addFormValidation();
      Object.keys(formField).forEach((key) => {
        validator.addField(key, formField[key]);
      });
      validator.validate().then(function (status) {
        if (status == "Valid") {
          // Prevent default button action
          // e.preventDefault();

          document.getElementById("simpan-form").click();

          // Disable button to avoid multiple click
          newPendaftarFormSimpan.disabled = true;

          // Show loading indication
          newPendaftarFormSimpan.setAttribute("data-kt-indicator", "on");

          // Simulate form submission
          setTimeout(function () {
            // Hide loading indication
            newPendaftarFormSimpan.removeAttribute("data-kt-indicator");

            // Enable button
            newPendaftarFormSimpan.disabled = false;

            // stepperObj.goNext();
            //KTUtil.scrollTop();
          }, 2000);
        } else {
          Swal.fire({
            text: "Maaf, masih terdapat error pada isian.",
            icon: "error",
            buttonsStyling: false,
            confirmButtonText: "Ok",
            customClass: {
              confirmButton: "btn btn-light",
            },
          }).then(function () {
            KTUtil.scrollTop();
          });
        }
      });
    });
  };

  var handleForm = function () {
    formSubmitButton.addEventListener("click", function (e) {
      // Validate form before change stepper step
      var validator = validations[4]; // get validator for last form
      // e.preventDefault();

      validator.validate().then(function (status) {
        console.log(status);
        if (status == "Valid") {
          // Prevent default button action

          // Disable button to avoid multiple click
          // formSubmitButton.disabled = true;

          // Show loading indication
          formSubmitButton.setAttribute("data-kt-indicator", "on");

          // formSimpanSilabus.disabled = true;
          // formContinueButton.disabled = true;

          // Simulate form submission
          // Hide loading indication
          formSubmitButton.removeAttribute("data-kt-indicator");

          // Enable button
          // formSubmitButton.disabled = false;

          document.getElementById("submit-form").click();
          // stepperObj.goNext();
          // form.submit();
          //KTUtil.scrollTop();
        } else {
          Swal.fire({
            text: "Maaf, masih terdapat error pada isian.",
            icon: "error",
            buttonsStyling: false,
            confirmButtonText: "Ok, got it!",
            customClass: {
              confirmButton: "btn btn-light",
            },
          }).then(function () {
            KTUtil.scrollTop();
          });
        }
      });
    });
  };

  var kuotapeserta = function () {
    return {
      validate: function (input) {
        var valuekuotapendaftar =
          document.getElementById("kuota_pendaftar").value;
        var valuekuotapeserta = input.value;
        if (parseInt(valuekuotapeserta) > parseInt(valuekuotapendaftar)) {
          return {
            valid: false,
          };
        } else {
          return {
            valid: true,
          };
        }
      },
    };
  };

  var jampelajarancheck = function () {
    return {
      validate: function (input) {
        if (input == "") {
          return {
            valid: true,
          };
        }
        let elemTotalJp = document.getElementById("total_jp");
        let valTotalJp = elemTotalJp ? elemTotalJp.value : "";
        let jamPelajaranArray =
          document.getElementsByClassName("numeric-silabus");
        let sumJp = 0;
        jamPelajaranArray.forEach((elem, index) => {
          sumJp = sumJp + (elem ? parseFloat(elem.value, 0) : 0);
        });
        if (parseFloat(sumJp) == parseFloat(valTotalJp)) {
          return {
            valid: true,
          };
        } else {
          return {
            valid: false,
          };
        }
      },
    };
  };

  var temacheck = function () {
    return {
      validate: function (input) {
        var valueakademi = document.getElementById("id_akademi");
        var valuex = valueakademi.options[valueakademi.selectedIndex].value;
        alert("valuex :" + valuex);
        alert("input.value :" + input.value);
        if (valuex != "" && input.value == "") {
          return { valid: true };
        } else {
          return { valid: false };
        }
      },
    };
  };

  var numbercheck = function (e) {
    return {
      validate: function (input) {
        var valuenum = input.value;
        if (valuenum != "") {
          const pattern = /^[0-9]$/;
          var check = pattern.test(valuenum);
          alert(check);
          if (check) {
            return { valid: true };
          } else {
            return { valid: false };
          }
        }
      },
    };
  };

  var nominusnumber = function (e) {
    return {
      validate: function (input) {
        var valuenum = input.value;
        if (valuenum != "") {
          if (parseInt(valuenum, 0) < 1) {
            return { valid: false };
          }
        }
        return { valid: true };
      },
    };
  };

  var jpnominusnumber = function (e) {
    return {
      validate: function (input) {
        var valuenum = input.value;
        if (valuenum != "") {
          if (parseFloat(valuenum, 0) <= 0) {
            return { valid: false };
          }
        }
        return { valid: true };
      },
    };
  };

  var formatJamSilabusCheck = function () {
    return {
      validate: function (input) {
        if (input == "") {
          return {
            valid: true,
          };
        }
        valueInput = input.value;

        if (!parseFloat(valueInput) || valueInput.split(".").length > 2) {
          return {
            valid: false,
          };
        } else {
          return {
            valid: true,
          };
        }
      },
    };
  };

  var maxJamSilabusCheck = function () {
    return {
      validate: function (input) {
        if (input == "") {
          return {
            valid: true,
          };
        }
        valueInput = input.value;

        if (parseFloat(valueInput) > 100) {
          return {
            valid: false,
          };
        } else {
          return {
            valid: true,
          };
        }
      },
    };
  };

  var checkEmpty = function (e) {
    return {
      validate: function (input) {
        var valuenum = input.value;
        if (valuenum != "") {
          if (valuenum == "-1") {
            return { valid: false };
          } else {
            return { valid: true };
          }
        } else {
          return { valid: false };
        }
      },
    };
  };

  var validURL = function (str) {
    var pattern = new RegExp(
      "^(https?:\\/\\/)?" + // protocol
        "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
        "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
        "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
        "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
        "(\\#[-a-z\\d_]*)?$",
      "i"
    ); // fragment locator

    return {
      validate: function (input) {
        var valueurl = input.value;
        if (!!pattern.test(valueurl)) {
          return { valid: true };
        } else {
          return { valid: false };
        }
      },
    };
  };

  var checkEmptyAlamat = function (e) {
    return {
      validate: function (input) {
        var valuenum = input.value;
        if (valuenum != "") {
          console.log($("input[name='metode_pelatihan']:checked").val());
          if (
            valuenum == "online" &&
            $("input[name='metode_pelatihan']:checked").val() != "Online"
          ) {
            return { valid: false };
          } else {
            return { valid: true };
          }
        } else {
          console.log($("input[name='metode_pelatihan']:checked").val());
          if ($("input[name='metode_pelatihan']:checked").val() != "Online") {
            return { valid: false };
          } else {
            return { valid: true };
          }
        }
      },
    };
  };

  var checkfilesize = function (e) {
    return {
      validate: function (input) {
        //var valuenum = input.value;
        var file = document.querySelector('[name="upload_silabus"]');
        // const fileSize = input.files[0].size / 1024 / 1024;
        var fileSize = file.files[0].size / 1024 / 1024;
        console.log("file size:", fileSize);
        if (fileSize > 10) {
          return { valid: false };
        } else {
          return { valid: true };
        }
      },
    };
  };

  var initValidation = function () {
    // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
    // Step 1
    validations.push(
      FormValidation.formValidation(form, {
        fields: {
          program_dts: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          },
          name_pelatihan: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          },
          input_nilai: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          },
          level_pelatihan: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          },
          akademi_id: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          },
          tema: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          },
          metode_pelaksanaan: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          },
          penyelenggara: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          },
          mitra: {
            validators: {
              callback: {
                message: "Tidak boleh kosong",
                callback: function (input) {
                  const radioValue = form.querySelector(
                    '[name="metode_pelaksanaan"]:checked'
                  );
                  const alur = radioValue ? radioValue.value : "";
                  return alur == "Mitra" ? input.value !== "" : true;
                },
              },
            },
          },
          deskripsi_hidden: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          },
          kuota_target_pendaftar: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
              // digits: {
              // 	message: 'Harus Angka'
              // },
              nominusnumber: {
                message: "Angka tidak boleh kurang dari 1",
              },
            },
          },
          kuota_target_peserta: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
              // digits: {
              // 	message: 'Harus Angka'
              // },
              nominusnumber: {
                message: "Angka tidak boleh kurang dari 1",
              },
              kuotapesertacheck: {
                message: "Kuota peserta tidak boleh lebih dari kuota pendaftar",
              },
            },
          },
          status_kuota: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          },
          sertifikasi: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          },
          pelaksana_assesment: {
            validators: {
              callback: {
                message: "Tidak boleh kosong",
                callback: function (input) {
                  const radioValue = form.querySelector(
                    '[name="sertifikasi"]:checked'
                  );
                  const pelaksanaAssesment = radioValue ? radioValue.labels[0] : "";
                  return pelaksanaAssesment.innerText === "Tidak Ada"
                    ? true
                    : input.value !== "";
                },
              },
            },
          },
          // lpj_peserta: {
          //   validators: {
          //     notEmpty: {
          //       message: "Tidak boleh kosong",
          //     },
          //   },
          // },
          alamat_hidden: {
            validators: {
              checkEmptyAlamat: {
                message: "Tidak boleh kosong",
              },
            },
          },
          zonasi: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          },
          batch: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
              // digits: {
              // 	message: 'Harus Angka'
              // },
              nominusnumber: {
                message: "Angka tidak boleh kurang dari 1",
              },
            },
          },
          metode_pelatihan: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          },
          url_vicon: {
            validators: {
              callback: {
                message: "Bukan valid URL",
                callback: function (input) {
                  const radioValue = form.querySelector(
                    '[name="metode_pelatihan"]:checked'
                  );
                  const metoddePelatihan = radioValue ? radioValue.value : "";
                  var pattern = new RegExp(
                    "^(https?:\\/\\/)?" + // protocol
                      "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
                      "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
                      "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
                      "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
                      "(\\#[-a-z\\d_]*)?$",
                    "i"
                  ); // fragment locator
                  return metoddePelatihan.includes("Online")
                    ? !!pattern.test(input.value)
                    : true;
                },
              },
            },
          },
          provinsi: {
            validators: {
              callback: {
                message: "Tidak boleh kosong",
                callback: function (input) {
                  const radioValue = form.querySelector(
                    '[name="metode_pelatihan"]:checked'
                  );
                  const metoddePelatihan = radioValue ? radioValue.value : "";
                  return metoddePelatihan.includes("Offline")
                    ? input.value !== ""
                    : true;
                },
              },
            },
          },
          kota_kabupaten: {
            validators: {
              callback: {
                message: "Tidak boleh kosong",
                callback: function (input) {
                  const radioValue = form.querySelector(
                    '[name="metode_pelatihan"]:checked'
                  );
                  const metoddePelatihan = radioValue ? radioValue.value : "";
                  return metoddePelatihan.includes("Offline")
                    ? input.value !== ""
                    : true;
                },
              },
            },
          },
          alamat_hidden: {
            validators: {
              callback: {
                message: "Tidak boleh kosong",
                callback: function (input) {
                  const radioValue = form.querySelector(
                    '[name="metode_pelatihan"]:checked'
                  );
                  const metoddePelatihan = radioValue ? radioValue.value : "";
                  return metoddePelatihan.includes("Offline")
                    ? input.value !== ""
                    : true;
                },
              },
            },
          },
          disabilitas: {
            validators: {
              checkEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          },
          upload_silabus: {
            validators: {
              checkEmpty: {
                message: "Tidak boleh kosong",
              },
              checkfilesize: {
                message: "Tidak boleh lebih dari 10 MB",
              },
            },
          },
        },
        plugins: {
          trigger: new FormValidation.plugins.Trigger(),
          bootstrap: new FormValidation.plugins.Bootstrap5({
            rowSelector: ".fv-row",
            eleInvalidClass: "",
            eleValidClass: "",
          }),
        },
      })
        .registerValidator("kuotapesertacheck", kuotapeserta)
        .registerValidator("temacheck", temacheck)
        .registerValidator("numbercheck", numbercheck)
        .registerValidator("nominusnumber", nominusnumber)
        .registerValidator("checkEmpty", checkEmpty)
        .registerValidator("checkEmptyAlamat", checkEmptyAlamat)
        .registerValidator("checkfilesize", checkfilesize)
        .registerValidator("checkURL", validURL)
    );

    // Step 2
    validations.push(
      FormValidation.formValidation(form, {
        fields: {
          alur_pendaftaran: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          },
          pendaftaran_tanggal_mulai: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          },
          pendaftaran_tanggal_selesai: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          },
          pelatihan_tanggal_mulai: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          },
          pelatihan_tanggal_selesai: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          },
          adm_test_administrasi_tanggal_mulai: {
            validators: {
              callback: {
                message: "Tidak boleh kosong",
                callback: function (input) {
                  console.log("input validate:", input);
                  const radioValue = form.querySelector(
                    '[name="alur_pendaftaran"]:checked'
                  );
                  const alur = radioValue ? radioValue.value : "";
                  return alur == "Administrasi - Test Substansi"
                    ? input.value !== ""
                    : true;
                },
              },
            },
          },
          adm_test_administrasi_tanggal_selesai: {
            validators: {
              callback: {
                message: "Tidak boleh kosong",
                callback: function (input) {
                  const radioValue = form.querySelector(
                    '[name="alur_pendaftaran"]:checked'
                  );
                  const alur = radioValue ? radioValue.value : "";
                  return alur == "Administrasi - Test Substansi"
                    ? input.value !== ""
                    : true;
                },
              },
            },
          },
          adm_test_testsubstansi_tanggal_mulai: {
            validators: {
              callback: {
                message: "Tidak boleh kosong",
                callback: function (input) {
                  const radioValue = form.querySelector(
                    '[name="alur_pendaftaran"]:checked'
                  );
                  const alur = radioValue ? radioValue.value : "";
                  return alur == "Administrasi - Test Substansi"
                    ? input.value !== ""
                    : true;
                },
              },
            },
          },
          adm_test_testsubstansi_tanggal_selesai: {
            validators: {
              callback: {
                message: "Tidak boleh kosong",
                callback: function (input) {
                  const radioValue = form.querySelector(
                    '[name="alur_pendaftaran"]:checked'
                  );
                  const alur = radioValue ? radioValue.value : "";
                  return alur == "Administrasi - Test Substansi"
                    ? input.value !== ""
                    : true;
                },
              },
            },
          },
          test_adm_testsubstansi_tanggal_mulai: {
            validators: {
              callback: {
                message: "Tidak boleh kosong",
                callback: function (input) {
                  console.log("input validate:", input);
                  const radioValue = form.querySelector(
                    '[name="alur_pendaftaran"]:checked'
                  );
                  const alur = radioValue ? radioValue.value : "";
                  return alur == "Test Substansi - Administrasi"
                    ? input.value !== ""
                    : true;
                },
              },
            },
          },
          test_adm_testsubstansi_tanggal_selesai: {
            validators: {
              callback: {
                message: "Tidak boleh kosong",
                callback: function (input) {
                  const radioValue = form.querySelector(
                    '[name="alur_pendaftaran"]:checked'
                  );
                  const alur = radioValue ? radioValue.value : "";
                  return alur == "Test Substansi - Administrasi"
                    ? input.value !== ""
                    : true;
                },
              },
            },
          },
          test_adm_administrasi_tanggal_mulai: {
            validators: {
              callback: {
                message: "Tidak boleh kosong",
                callback: function (input) {
                  const radioValue = form.querySelector(
                    '[name="alur_pendaftaran"]:checked'
                  );
                  const alur = radioValue ? radioValue.value : "";
                  return alur == "Test Substansi - Administrasi"
                    ? input.value !== ""
                    : true;
                },
              },
            },
          },
          test_adm_administrasi_tanggal_selesai: {
            validators: {
              callback: {
                message: "Tidak boleh kosong",
                callback: function (input) {
                  const radioValue = form.querySelector(
                    '[name="alur_pendaftaran"]:checked'
                  );
                  const alur = radioValue ? radioValue.value : "";
                  return alur == "Test Substansi - Administrasi"
                    ? input.value !== ""
                    : true;
                },
              },
            },
          },
          apakah_mid_test: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          },
          midtest_tanggal: {
            validators: {
              callback: {
                message: "Tidak boleh kosong",
                callback: function (input) {
                  const radioValue = form.querySelector(
                    '[name="apakah_mid_test"]:checked'
                  );
                  const alur = radioValue ? radioValue.value : "";
                  return alur == "Iya" ? input.value !== "" : true;
                },
              },
            },
          },
        },
        plugins: {
          trigger: new FormValidation.plugins.Trigger(),
          // Bootstrap Framework Integration
          bootstrap: new FormValidation.plugins.Bootstrap5({
            rowSelector: ".fv-row",
            eleInvalidClass: "",
            eleValidClass: "",
          }),
        },
      })
    );

    // Step 3
    validations.push(
      FormValidation.formValidation(form, {
        fields: {},
        plugins: {
          trigger: new FormValidation.plugins.Trigger(),
          // Bootstrap Framework Integration
          bootstrap: new FormValidation.plugins.Bootstrap5({
            rowSelector: ".fv-row",
            eleInvalidClass: "",
            eleValidClass: "",
          }),
        },
      })
        .registerValidator("sumjpcheck", jampelajarancheck)
        .registerValidator("formatJamSilabusCheck", formatJamSilabusCheck)
        // .registerValidator("maxJamSilabusCheck", maxJamSilabusCheck)
        .registerValidator("jpnominusnumber", jpnominusnumber)
    );

    validations.push(
      FormValidation.formValidation(form, {
        fields: {},
        plugins: {
          trigger: new FormValidation.plugins.Trigger(),
          // Bootstrap Framework Integration
          bootstrap: new FormValidation.plugins.Bootstrap5({
            rowSelector: ".fv-row",
            eleInvalidClass: "",
            eleValidClass: "",
          }),
        },
      })
    );

    // Step 5
    validations.push(
      FormValidation.formValidation(form, {
        fields: {
          komitmen_peserta: {
            validators: {
              notEmpty: {
                message: "Tidak boleh kosong",
              },
            },
          },
          deskripsi_komitmen_hidden: {
            validators: {
              callback: {
                message: "Tidak boleh kosong",
                callback: function (input) {
                  const radioValue = form.querySelector(
                    '[name="komitmen_peserta"]:checked'
                  );
                  const alur = radioValue ? radioValue.value : "";
                  return alur == "Iya" ? input.value !== "" : true;
                },
              },
            },
          },
          deskripsi_komitmen: {
            validators: {
              callback: {
                message: "Tidak boleh kosong",
                callback: function (input) {
                  const radioValue = form.querySelector(
                    '[name="komitmen_peserta"]:checked'
                  );
                  const alur = radioValue ? radioValue.value : "";
                  return alur == "Iya" ? input.value !== "" : true;
                },
              },
            },
          },
        },
        plugins: {
          trigger: new FormValidation.plugins.Trigger(),
          // Bootstrap Framework Integration
          bootstrap: new FormValidation.plugins.Bootstrap5({
            rowSelector: ".fv-row",
            eleInvalidClass: "",
            eleValidClass: "",
          }),
        },
      })
    );

    // validations.push(
    //   FormValidation.formValidation(form, {
    //     fields: {
    //       komitmen_peserta: {
    //         validators: {
    //           notEmpty: {
    //             message: "Tidak boleh kosong",
    //           },
    //         },
    //       },
    //       deskripsi_komitmen: {
    //         validators: {
    //           notEmpty: {
    //             message: "Tidak boleh kosong",
    //           },
    //         },
    //       },
    //     },
    //     plugins: {
    //       trigger: new FormValidation.plugins.Trigger(),
    //       bootstrap: new FormValidation.plugins.Bootstrap5({
    //         rowSelector: ".fv-row",
    //         eleInvalidClass: "",
    //         eleValidClass: "",
    //       }),
    //     },
    //   }).registerValidator("checkfilesize", checkfilesize)
    // );

    //validasi file
    // validations.push(
    //   FormValidation.formValidation(form, {
    //     fields: {
    //       // upload_silabus: {
    //       // 	validators: {
    //       // 		notEmpty: {
    //       // 			message: 'Tidak boleh kosong'
    //       // 		}
    //       // 		// ,
    //       // 		// checkfilesize: {
    //       // 		// 	message: 'Tidak boleh lebih dari 10 MB'
    //       // 		// }
    //       // 	}
    //       // }
    //     },
    //     plugins: {
    //       trigger: new FormValidation.plugins.Trigger(),
    //       bootstrap: new FormValidation.plugins.Bootstrap5({
    //         rowSelector: ".fv-row",
    //         eleInvalidClass: "",
    //         eleValidClass: "",
    //       }),
    //     },
    //   }).registerValidator("checkfilesize", checkfilesize)
    // );
  };

  var handleFormSubmit = function () {};

  function setCookie(cname, cvalue, minutes) {
    const d = new Date();
    d.setTime(d.getTime() + minutes * 60 * 1000);
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(";");
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == " ") {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  return {
    // Public Functions
    init: function () {
      // Elements
      modalEl = document.querySelector("#kt_modal_create_account");
      if (modalEl) {
        modal = new bootstrap.Modal(modalEl);
      }

      stepper = document.querySelector("#kt_create_account_stepper");
      form = stepper.querySelector("#kt_create_account_form");
      formSubmitButton = stepper.querySelector(
        '[data-kt-stepper-action="submit"]'
      );
      formSubmitButtonDummy = stepper.querySelector('[id="dummy-submit"]');
      formSimpanSilabus = stepper.querySelector(
        '[data-kt-stepper-action="simpan-silabus"]'
      );
      newPendaftarFormSimpan = stepper.querySelector(
        '[data-kt-stepper-action="simpan-form"]'
      );
      formContinueButton = stepper.querySelector(
        '[data-kt-stepper-action="next"]'
      );

      initStepper();
      initValidation();
      // handleTambahSilabus();
      handleTambahForm();
      handleForm();
      $("input[name='batch']").on("input", function (e) {
        $(this).val(
          $(this)
            .val()
            .replace(/[^0-9]/g, "")
        );
      });
      $("input[name='kuota_target_pendaftar']").on("input", function (e) {
        $(this).val(
          $(this)
            .val()
            .replace(/[^0-9]/g, "")
        );
      });
      $("input[name='kuota_target_peserta']").on("input", function (e) {
        $(this).val(
          $(this)
            .val()
            .replace(/[^0-9]/g, "")
        );
      });
      $("input[name='total_jp']").on("input", function (e) {
        $(this).val(
          $(this)
            .val()
            .replace(/[^\d*\.?\d*$]/g, "")
        );
      });
      $("input[name='jp_silabus']").on("input", function (e) {
        $(this).val(
          $(this)
            .val()
            .replace(/[^\d*\.?\d*$]/g, "")
        );
      });
    },
  };
})();

// On document ready
KTUtil.onDOMContentLoaded(function () {
  KTCreateAccount.init();
});
