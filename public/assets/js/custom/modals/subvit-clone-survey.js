"use strict";

// Class definition
var KTSubvitCloneSurvey = function () {
	// Elements
	var modal;	
	var modalEl;

	var stepper;
	var form;
	var formSubmitButton;
	var formContinueButton;

	// Variables
	var stepperObj;
	var validations = [];

	// Private Functions
	var initStepper = function () {
		// Initialize Stepper
		stepperObj = new KTStepper(stepper);

		stepperObj.on("kt.stepper.click", function (stepper) {
			
			stepper.goTo(stepper.getClickedStepIndex()); // go to clicked step
		});

		// Stepper change event
		stepperObj.on('kt.stepper.changed', function (stepper) {						
			if (stepperObj.getCurrentStepIndex() === 4) {
				formSubmitButton.classList.remove('d-none');
				formSubmitButton.classList.add('d-inline-block');
				formContinueButton.classList.add('d-none');
			} else if (stepperObj.getCurrentStepIndex() === 5) {
				formSubmitButton.classList.add('d-none');
				formContinueButton.classList.add('d-none');
			} else {
				formSubmitButton.classList.remove('d-inline-block');
				formSubmitButton.classList.remove('d-none');
				formContinueButton.classList.remove('d-none');
			}
		});

		// Validation before going to next page
		stepperObj.on('kt.stepper.next', function (stepper) {
			console.log('stepper.next');

			// Validate form before change stepper step
			var validator = validations[stepper.getCurrentStepIndex() - 1]; // get validator for currnt step

			if (validator) {
				validator.validate().then(function (status) {
					console.log('validated!');

					if (status == 'Valid') {
						stepper.goNext();

						KTUtil.scrollTop();
					} else {
						Swal.fire({
							title: "Maaf, masih ada yang belum diisi.",
							icon: "warning",
							// buttonsStyling: false,
							confirmButtonText: "Ok, dicek lagi"
							// customClass: {
							// 	confirmButton: "btn btn-light"
							// }
						}).then(function () {
							KTUtil.scrollTop();
						});
					}
				});
			} else {
				stepper.goNext();

				KTUtil.scrollTop();
			}
		});

		// Prev event
		stepperObj.on('kt.stepper.previous', function (stepper) {
			console.log('stepper.previous');

			stepper.goPrevious();
			KTUtil.scrollTop();
		});
	}

	return {
		// Public Functions
		init: function () {
			// Elements
			modalEl = document.querySelector('#kt_modal_create_account');
			if (modalEl) {
				modal = new bootstrap.Modal(modalEl);	
			}					

			stepper = document.querySelector('#kt_subvit_clone_survey');
			if(stepper)
			{
				form = stepper.querySelector('#kt_subvit_clone_survey_form');
				formSubmitButton = stepper.querySelector('[data-kt-stepper-action="submit"]');
				formContinueButton = stepper.querySelector('[data-kt-stepper-action="next"]');

				initStepper();				
			}
			
		}
	};
}();

// On document ready
KTUtil.onDOMContentLoaded(function() {
    KTSubvitCloneSurvey.init();
});