"use strict";

// Class definition
var ktsubvit_trivia_clone = function () {
	// Elements	
	var modalEl;

	var stepper;
	var form;

	// Variables
	var stepperObj;
	var validations = [];

	// Private Functions
	var initStepper = function () {
		console.log("AKU");
		// Initialize Stepper
		stepperObj = new KTStepper(stepper);

		stepperObj.on("kt.stepper.click", function (stepper) {
			//stepper.goTo(stepper.getClickedStepIndex()); // go to clicked step
			console.log('sx');
		}); 

		// Stepper change event
		stepperObj.on('kt.stepper.changed', function (stepper) {

		});

		// Validation before going to next page
		stepperObj.on('kt.stepper.next', function (stepper) {
			console.log('stepper.next');
			// stepper.goTo(stepper.getClickedStepIndex());
			// Validate form before change stepper step
			var validator = validations[stepper.getCurrentStepIndex() - 1]; // get validator for currnt step

			if (validator) {
				validator.validate().then(function (status) {
					console.log('validated!');

					if (status == 'Valid') {
						stepper.goNext();

						KTUtil.scrollTop();
					} else {
						Swal.fire({
							title: "Maaf, masih ada yang belum diisi.",
							icon: "warning",
							// buttonsStyling: false,
							confirmButtonText: "Ok, dicek lagi"
							// customClass: {
							// 	confirmButton: "btn btn-light"
							// }
						}).then(function () {
							KTUtil.scrollTop();
						});
					}
				});
			} else {
				stepper.goNext();

				KTUtil.scrollTop();
			}
		});

		// Prev event
		stepperObj.on('kt.stepper.previous', function (stepper) {
			console.log('stepper.previous');

			stepper.goPrevious();
			KTUtil.scrollTop();
		});

	}


	return {
		// Public Functions
		init: function () {
			stepper = document.querySelector('#ktsubvit_trivia_clone');
			if (stepper) {
				form = stepper.querySelector('#kt_subvit_trivia_clone_form');
				formSubmitButton = stepper.querySelector('[data-kt-stepper-action="submit"]');
				formContinueButton = stepper.querySelector('[data-kt-stepper-action="next"]');
				initStepper();
			}
		}
	};
}();

// On document ready
KTUtil.onDOMContentLoaded(function () {
	ktsubvit_trivia_clone.init();
});