let id = {
  weekdays: {
    shorthand: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"],
    longhand: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"],
  },
  months: {
    shorthand: [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "Mei",
      "Jun",
      "Jul",
      "Agt",
      "Sep",
      "Okt",
      "Nov",
      "Des",
    ],
    longhand: [
      "Januari",
      "Februari",
      "Maret",
      "April",
      "Mei",
      "Juni",
      "Juli",
      "Agustus",
      "September",
      "Oktober",
      "November",
      "Desember",
    ],
  },
};

function fp_inc(dateObj, days) {
  return new Date(
    dateObj.getFullYear(),
    dateObj.getMonth(),
    dateObj.getDate() + (typeof days === "string" ? parseInt(days, 10) : days)
  );
}

$(function () {
  $(".datatable").DataTable({
    scrollY: 300,
    scrollX: true,
  });
  $("#datetime").flatpickr({
    enableTime: true,
    // minDate: "today",
    // locale: id,
    // time_24hr: true,
    dateFormat: "d F Y H:i",
    // onChange: function (dateStr, dateObj, d) {
    //   datetime2.set("minDate", dateStr[0]);
    //   datetime_test_adm_teststart.set("minDate", dateStr[0]);
    //   datetime_test_adm_teststart.set("defaultDate", dateStr[0]);
    // },
  });
  var datetime2 = flatpickr("#datetime2", {
    enableTime: true,
    minDate: "today",
    locale: id,
    time_24hr: true,
    dateFormat: "d F Y H:i",
    onChange: function (dateStr, dateObj, d) {
      datetime3.set("minDate", dateStr[0]);
      datetime3.set("defaultDate", dateStr[0]);

      // Administrasi
      datetime_adm_test_admstart.set("minDate", dateStr[0]);
      datetime_adm_test_admstart.set("defaultDate", dateStr[0]);
    },
  });
  var datetime3 = flatpickr("#datetime3", {
    enableTime: true,
    minDate: "today",
    locale: id,
    time_24hr: true,
    dateFormat: "d F Y H:i",
    onChange: function (dateStr, dateObj) {
      datetime4.set("minDate", dateStr[0]);
      datetime4.set("defaultDate", dateStr[0]);
      const date = fp_inc(new Date(dateStr[0]), -1);
      datetime_adm_test_admend.set("maxDate", date);
      datetime_adm_test_admend.set("defaultDate", date);
      datetime_adm_test_testend.set("maxDate", dateStr[0]);
      datetime_adm_test_testend.set("defaultDate", dateStr[0]);

      datetime_test_adm_testend.set("maxDate", date);
      datetime_test_adm_testend.set("defaultDate", date);
      datetime_test_adm_admend.set("maxDate", dateStr[0]);
      datetime_test_adm_admend.set("defaultDate", dateStr[0]);

      // mid test
      // datetime9.set("minDate", dateStr[0]);
      // datetime9.set("defaultDate", dateStr[0]);
    },
  });
  var datetime4 = flatpickr("#datetime4", {
    enableTime: true,
    minDate: "today",
    locale: id,
    time_24hr: true,
    dateFormat: "d F Y H:i",
    onChange: function (dateStr, dateObj, d) {
      // mid test
      // datetime9.set("maxDate", dateStr[0]);
      // datetime9.set("defaultDate", dateStr[0]);
    },
  });
  // Administrasi -> Test Substansi
  var datetime_adm_test_admstart = flatpickr("#datetime_adm_test_admstart", {
    enableTime: true,
    minDate: "today",
    time_24hr: true,
    dateFormat: "d F Y H:i",
    onChange: function (dateStr, dateObj, d) {
      datetime_adm_test_teststart.set("minDate", dateStr[0]);
      datetime_adm_test_teststart.set("defaultDate", dateStr[0]);
    },
  });

  var datetime_adm_test_admend = flatpickr("#datetime_adm_test_admend", {
    enableTime: true,
    minDate: "today",
    time_24hr: true,
    dateFormat: "d F Y H:i",
    onChange: function (dateStr, dateObj, d) {
      datetime_adm_test_teststart.set("minDate", dateStr[0]);
      datetime_adm_test_teststart.set("defaultDate", dateStr[0]);
    },
  });

  var datetime_adm_test_teststart = flatpickr("#datetime_adm_test_teststart", {
    enableTime: true,
    minDate: "today",
    time_24hr: true,
    dateFormat: "d F Y H:i",
    onChange: function (dateStr, dateObj, d) {
      // Administrasi
      datetime_adm_test_admend.set("maxDate", date);
      datetime_adm_test_admend.set("defaultDate", date);
    },
  });

  var datetime_adm_test_testend = flatpickr("#datetime_adm_test_testend", {
    enableTime: true,
    minDate: "today",
    time_24hr: true,
    dateFormat: "d F Y H:i",
  });

  // Test Substansi -> Administrasi
  var datetime_test_adm_admstart = flatpickr("#datetime_test_adm_admstart", {
    enableTime: true,
    minDate: "today",
    time_24hr: true,
    dateFormat: "d F Y H:i",
  });

  var datetime_test_adm_admend = flatpickr("#datetime_test_adm_admend", {
    enableTime: true,
    minDate: "today",
    time_24hr: true,
    dateFormat: "d F Y H:i",
  });

  var datetime_test_adm_teststart = flatpickr("#datetime_adm_test_teststart", {
    enableTime: true,
    minDate: "today",
    time_24hr: true,
    dateFormat: "d F Y H:i",
  });

  var datetime_test_adm_testend = flatpickr("#datetime_test_adm_testend", {
    enableTime: true,
    minDate: "today",
    time_24hr: true,
    dateFormat: "d F Y H:i",
    onChange: function (dateStr, dateObj, d) {
      datetime_test_adm_admstart.set("minDate", dateStr[0]);
      datetime_test_adm_admstart.set("defaultDate", dateStr[0]);
    },
  });

  var datetime9 = flatpickr("#datetime9", {
    enableTime: true,
    minDate: "today",
    time_24hr: true,
    dateFormat: "d F Y H:i",
  });

  $("#date_publish").flatpickr({
    enableTime: false,
    minDate: "today",
    dateFormat: "Y-m-d",
  });

  $("#date_event").flatpickr({
    enableTime: false,
    minDate: "today",
    dateFormat: "Y-m-d",
  });

  $("#tanggal_lahir").flatpickr({
    enableTime: false,    
    dateFormat: "j M Y",
    locale: id
  });

  $("#date_survey_start").flatpickr({
    enableTime: false,
    minDate: "today",
    dateFormat: "d-M-Y",
    onChange: function (dateStr, dateObj) {
      date_survey_end.set("minDate", dateObj);
    },
  });

  var date_survey_end = flatpickr("#date_survey_end", {
    enableTime: false,
    minDate: "today",
    dateFormat: "d-M-Y",
  });

  $("#date_trivia_start").flatpickr({
    enableTime: false,
    minDate: "today",
    dateFormat: "d-M-Y",
    onChange: function (dateStr, dateObj) {
      date_trivia_end.set("minDate", dateObj);
    },
  });

  var date_trivia_end = flatpickr("#date_trivia_end", {
    enableTime: false,
    minDate: "today",
    dateFormat: "d-M-Y",
  });

  function previewImage(srcComponent, targetComponent) {
    var target = $(targetComponent).attr(
      "src",
      URL.createObjectURL(srcComponent.target.files[0])
    );
    target.onload = function () {
      URL.revokeObjectURL(output.src);
    };
  }
  $("#kerjasama_awal").flatpickr({
    enableTime: false,
    // minDate: "today",
    dateFormat: "j M Y",
    locale: id
  });
  $("#kerjasama_ttd").flatpickr({
    enableTime: false,
    // minDate: "today",
    dateFormat: "j M Y",
    locale: id
  });
  // $("#kerjasama_akhir").flatpickr({
  //     enableTime: false,
  //     minDate: "today",
  //     dateFormat: "Y-m-d"
  // });
  var kerjasama_akhir = flatpickr("#kerjasama_akhir", {
    enableTime: false,
    // minDate: "today",
    dateFormat: "j M Y",
    locale: id
  });
});
