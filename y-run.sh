if [ ! -d "/app/node_modules" ]
then
    npm install --legacy-peer-deps;
fi
cd /app && npm start
