FROM node:17.6.0
ENV NODE_OPTIONS=--max_old_space_size=2048
WORKDIR /app
EXPOSE 3002

# CMD ["npm", "start"]