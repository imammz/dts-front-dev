import http from "../HttpCommon";

const getAll = () => {
  return http.post(`/list_user`);
};
const get = (data) => {
  return http.post("/akademi/cari-akademi", data);
};
// `/tutorials/${id}`
const create = (data) => {
  return http.post("/", data);
};
const update = (data) => {
  return http.post("/", data);
};
const remove = (id) => {
  return http.post("");
};
const removeAll = () => {
  return http.post("");
};
const findByTitle = (title) => {
  return http.get("/xxx=${title}");
};

const AkademiService = {
  getAll,
  get,
  create,
  update,
  remove,
  findByTitle,
};

export default AkademiService;
