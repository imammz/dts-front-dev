import http from "../HttpCommon";

const getAll = (params) => {
  return http.post(`/akademi/list-akademi`, { params });
};
const getid = (data) => {
  return http.post("/akademi/cari-akademi", data);
};
// `/tutorials/${id}`
const create = (data) => {
  return http.post("/akademi/tambah-akademi", data);
};
const update = (data) => {
  return http.post("/akademi/update-akademi", data);
};
const remove = (id) => {
  return http.post("");
};
const removeAll = () => {
  return http.post("");
};
const findByTitle = (title) => {
  return http.get("/xxx=${title}");
};

const AkademiService = {
  getAll,
  getid,
  create,
  update,
  remove,
  findByTitle,
};

export default AkademiService;
