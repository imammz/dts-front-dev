import http from "../HttpCommonMockUp";

const getAll = () => {
  return http.get(`/ListAdministrator`);
};
const get = (id) => {
  return http.post("/akademi/cari-akademi", { id: "147" });
};
const lvRole = () => {
  return http.get("/cari-role");
};
// `/tutorials/${id}`
const create = (data) => {
  return http.post("/", data);
};
const update = (data) => {
  return http.post("/", data);
};
const remove = (id) => {
  return http.post("");
};
const removeAll = () => {
  return http.post("");
};
const findByTitle = (title) => {
  return http.get("/xxx=${title}");
};

const AdministratorService = {
  getAll,
  get,
  create,
  update,
  remove,
  findByTitle,
};

export default AdministratorService;
