import http from "../HttpCommon";

const getAll = () => {
  return http.post(`/list-frombuilder`);
};
const get = (id) => {
  return http.get(`/data?name=${id}`);
};
// `/tutorials/${id}`
const create = (data) => {
  return http.post("/", data);
};
const update = (data) => {
  return http.post("/", data);
};
const remove = (id) => {
  return http.post("");
};
const removeAll = () => {
  return http.post("");
};
const findByTitle = (title) => {
  return http.get("/xxx=${title}");
};

const PendaftaranService = {
  getAll,
  get,
  create,
  update,
  remove,
  findByTitle,
};

export default PendaftaranService;
