import http from "../HttpCommon";

const getAll = () => {
  return http.post(`/list-peserta-simonas`);
};
const get = (id) => {
  return http.get(`/data?id=${id}`);
};
// `/tutorials/${id}`
const create = (data) => {
  return http.post("/", data);
};
const update = (data) => {
  return http.post("/", data);
};
const remove = (id) => {
  return http.post("");
};
const removeAll = () => {
  return http.post("");
};
const findByTitle = (title) => {
  return http.get("/xxx=${title}");
};

const BeasiswaService = {
  getAll,
  get,
  create,
  update,
  remove,
  findByTitle,
};

export default BeasiswaService;
