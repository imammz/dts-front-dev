import React from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";

export default class VerifikasiOtp extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleSubmit.bind(this);
    this.showConfirmPassword = this.showConfirmPassword.bind(this);
    this.state = {
      fields: {},
      errors: {},
      datax: [],
      isRevealPwd: false,
      isLoading: false,
      classConfirmEye: "fa fa-eye-slash",
      typeConfirmPassword: "password",
      atemps: 0,
    };
  }
  resetError() {
    let errors = {};
    errors["name"] = "";
    errors["deskripsi"] = "";
    this.setState({ errors: errors });
  }
  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    let errors = {};
    let formIsValid = true;

    if (dataForm.get("email") == "") {
      formIsValid = false;
      errors["email"] = "Tidak boleh kosong";
    }
    if (dataForm.get("password") == "") {
      formIsValid = false;
      errors["password"] = "Tidak boleh kosong";
    }
    this.setState({ errors });
    return formIsValid;
  }

  handleShowPWD = () => {
    if (this.state.isRevealPwd) {
      this.setState({ isRevealPwd: false });
    } else {
      this.setState({ isRevealPwd: true });
    }
  };

  handleSubmit(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      let data = {
        email: Cookies.get("user_email_reset"),
        pin:
          dataForm.get("code_1") +
          dataForm.get("code_2") +
          dataForm.get("code_3") +
          dataForm.get("code_4") +
          dataForm.get("code_5") +
          dataForm.get("code_6"),
      };

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI +
            "/auth/reset-password/verify-otp",
          data,
        )
        .then((res) => {
          const statusx = res.data.success;
          const messagex = res.data.message;

          if (statusx) {
            swal
              .fire({
                title: "Kode OTP Sesuai, Silahkan lakukan pergantian password",
                icon: "success",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                Cookies.set(
                  "pin",
                  dataForm.get("code_1") +
                    dataForm.get("code_2") +
                    dataForm.get("code_3") +
                    dataForm.get("code_4") +
                    dataForm.get("code_5") +
                    dataForm.get("code_6"),
                );

                if (result.isConfirmed) {
                  window.location = "/konfirm-password";
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                this.setState({ isLoading: false });
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let message =
            error.response.status == 504
              ? "Terjadi kesalahan, aplikasi sedang sibuk silahkan coba lagi nanti."
              : error.response.status == 401
                ? error.response.data.result.Message
                : error.response.status == 429
                  ? "Terlalu banyak percobaan login, mohon tunggu 1 menit kemudian"
                  : null;
          swal
            .fire({
              title: message ?? "Login Gagal",
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        });
    } else {
      console.log("validasi");
    }
  }
  componentDidMount() {
    const user_email_reset = Cookies.get("user_email_reset");

    if (!user_email_reset) {
      window.location = "/";
    }
  }

  showConfirmPassword() {
    if (this.state.typeConfirmPassword == "password") {
      this.setState({
        typeConfirmPassword: "text",
        classConfirmEye: "fa fa-eye",
      });
    } else {
      this.setState({
        typeConfirmPassword: "password",
        classConfirmEye: "fa fa-eye-slash",
      });
    }
  }

  render() {
    return (
      <div>
        <div className="col-12">
          <div className="row">
            <div
              className="col-lg-8 mb-0 align-middle d-none d-lg-inline-block"
              style={{ backgroundColor: "#CDE9F6" }}
            >
              <div className="pb-5 px-5 px-lg-3 px-xl-5 vh-100 d-flex flex-column flex-center">
                <h1 className="text-gray-800 fs-2qx fw-bolder mb-7">
                  Login Administrator
                </h1>
                <img
                  className="theme-light-show mx-auto mt-6 mw-100 w-150px w-lg-300px mb-10 mb-lg-20"
                  src="assets/media/logos/login.png"
                  alt=""
                />
                <div className="text-dark text-center mt-6">
                  <a href="#" className="text-muted text-hover-primary">
                    Badan Pengembangan SDM Kominfo{" "}
                    <span className="text-muted fw-bold me-1">
                      © {new Date().getFullYear()}
                    </span>{" "}
                  </a>
                </div>
              </div>
            </div>
            <div
              className="col-lg-4 mb-0 align-middle d-none d-lg-inline-block"
              style={{ height: "100vh", backgroundColor: "#fff" }}
            >
              <div className="pb-5 p-0 vh-100 d-flex flex-column flex-center">
                <div className="col-lg-10">
                  <form
                    noValidate="novalidate"
                    id="kt_sign_in_form"
                    action="#"
                    onSubmit={this.handleClick}
                  >
                    <div className="text-center">
                      <img
                        alt="Logo"
                        src="assets/media/logos/dts.png"
                        className="align-items-center h-100px mt-5 mb-5"
                      />
                    </div>
                    <h3 className="text-center fw-bolder text-gray-600 mt-10">
                      Kode Verifikasi
                    </h3>
                    <p className="text-center">
                      Silahkan masukkan kode verifikasi yang dikirim ke
                      email/inbox mu. Jangan bagikan kode verifikasi kepada
                      siapapun.
                    </p>
                    <div className="fv-row mt-8 mb-10">
                      <div className="d-flex flex-wrap flex-stack">
                        <input
                          type="text"
                          name="code_1"
                          data-inputmask="'mask': '9', 'placeholder': ''"
                          maxlength="1"
                          className="form-control bg-transparent h-60px w-60px fs-2qx text-center mx-1 my-2"
                          inputmode="text"
                        />
                        <input
                          type="text"
                          name="code_2"
                          data-inputmask="'mask': '9', 'placeholder': ''"
                          maxlength="1"
                          className="form-control bg-transparent h-60px w-60px fs-2qx text-center mx-1 my-2"
                          inputmode="text"
                        />
                        <input
                          type="text"
                          name="code_3"
                          data-inputmask="'mask': '9', 'placeholder': ''"
                          maxlength="1"
                          className="form-control bg-transparent h-60px w-60px fs-2qx text-center mx-1 my-2"
                          inputmode="text"
                        />
                        <input
                          type="text"
                          name="code_4"
                          data-inputmask="'mask': '9', 'placeholder': ''"
                          maxlength="1"
                          className="form-control bg-transparent h-60px w-60px fs-2qx text-center mx-1 my-2"
                          inputmode="text"
                        />
                        <input
                          type="text"
                          name="code_5"
                          data-inputmask="'mask': '9', 'placeholder': ''"
                          maxlength="1"
                          className="form-control bg-transparent h-60px w-60px fs-2qx text-center mx-1 my-2"
                          inputmode="text"
                        />
                        <input
                          type="text"
                          name="code_6"
                          data-inputmask="'mask': '9', 'placeholder': ''"
                          maxlength="1"
                          className="form-control bg-transparent h-60px w-60px fs-2qx text-center mx-1 my-2"
                          inputmode="text"
                        />
                      </div>
                    </div>

                    <div className="text-center">
                      <button
                        type="submit"
                        className="btn btn-lg btn-primary mb-5 me-4"
                        disabled={this.state.isLoading}
                      >
                        {this.state.isLoading ? (
                          <>
                            <span
                              className="spinner-border spinner-border-sm me-2"
                              role="status"
                              aria-hidden="true"
                            ></span>
                            <span className="sr-only">Loading...</span>
                            Loading...
                          </>
                        ) : (
                          <>
                            <span className="indicator-label">Submit</span>
                          </>
                        )}
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div
          className="col-12 d-lg-none align-middle px-10"
          style={{ height: "100vh", backgroundColor: "#fff" }}
        >
          <div className="container py-8">
            <form
              noValidate="novalidate"
              id="kt_sign_in_form"
              action="#"
              onSubmit={this.handleClick}
            >
              <div className="text-center">
                <img
                  alt="Logo"
                  src="assets/media/logos/dts.png"
                  className="align-items-center h-100px mt-5 mb-5"
                />
              </div>
              <h3 className="text-center fw-bolder text-gray-600 mt-10">
                Kode Verifikasi
              </h3>
              <p className="text-center">
                Silahkan masukkan kode verifikasi yang dikirim ke email/inbox
                mu. Jangan bagikan kode verifikasi kepada siapapun.
              </p>
              <div className="fv-row mt-8 mb-10">
                <div className="d-flex flex-wrap flex-stack">
                  <input
                    type="text"
                    name="code_1"
                    data-inputmask="'mask': '9', 'placeholder': ''"
                    maxlength="1"
                    className="form-control bg-transparent h-60px w-60px fs-2qx text-center mx-1 my-2"
                    value=""
                    inputmode="text"
                  />
                  <input
                    type="text"
                    name="code_2"
                    data-inputmask="'mask': '9', 'placeholder': ''"
                    maxlength="1"
                    className="form-control bg-transparent h-60px w-60px fs-2qx text-center mx-1 my-2"
                    value=""
                    inputmode="text"
                  />
                  <input
                    type="text"
                    name="code_3"
                    data-inputmask="'mask': '9', 'placeholder': ''"
                    maxlength="1"
                    className="form-control bg-transparent h-60px w-60px fs-2qx text-center mx-1 my-2"
                    value=""
                    inputmode="text"
                  />
                  <input
                    type="text"
                    name="code_4"
                    data-inputmask="'mask': '9', 'placeholder': ''"
                    maxlength="1"
                    className="form-control bg-transparent h-60px w-60px fs-2qx text-center mx-1 my-2"
                    value=""
                    inputmode="text"
                  />
                  <input
                    type="text"
                    name="code_5"
                    data-inputmask="'mask': '9', 'placeholder': ''"
                    maxlength="1"
                    className="form-control bg-transparent h-60px w-60px fs-2qx text-center mx-1 my-2"
                    value=""
                    inputmode="text"
                  />
                  <input
                    type="text"
                    name="code_6"
                    data-inputmask="'mask': '9', 'placeholder': ''"
                    maxlength="1"
                    className="form-control bg-transparent h-60px w-60px fs-2qx text-center mx-1 my-2"
                    value=""
                    inputmode="text"
                  />
                </div>
              </div>

              <div className="text-center">
                <button
                  disabled={this.state.isLoading}
                  type="submit"
                  className="btn btn-lg btn-primary me-4 mb-5"
                >
                  {this.state.isLoading ? (
                    <>
                      <span
                        className="spinner-border spinner-border-sm me-2"
                        role="status"
                        aria-hidden="true"
                      ></span>
                      <span className="sr-only">Loading...</span>Loading...
                    </>
                  ) : (
                    <>
                      <span className="indicator-label">Submit</span>
                    </>
                  )}
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
