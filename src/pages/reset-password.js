import React from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";

export default class ResetPassword extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleSubmit.bind(this);
    this.showConfirmPassword = this.showConfirmPassword.bind(this);
    this.state = {
      fields: {},
      errors: {},
      datax: [],
      isRevealPwd: false,
      isLoading: false,
      classConfirmEye: "fa fa-eye-slash",
      typeConfirmPassword: "password",
      atemps: 0,
    };
  }
  resetError() {
    let errors = {};
    errors["name"] = "";
    errors["deskripsi"] = "";
    this.setState({ errors: errors });
  }

  checkEmail(emailAddress) {
    var sQtext = "[^\\x0d\\x22\\x5c\\x80-\\xff]";
    var sDtext = "[^\\x0d\\x5b-\\x5d\\x80-\\xff]";
    var sAtom =
      "[^\\x00-\\x20\\x22\\x28\\x29\\x2c\\x2e\\x3a-\\x3c\\x3e\\x40\\x5b-\\x5d\\x7f-\\xff]+";
    var sQuotedPair = "\\x5c[\\x00-\\x7f]";
    var sDomainLiteral = "\\x5b(" + sDtext + "|" + sQuotedPair + ")*\\x5d";
    var sQuotedString = "\\x22(" + sQtext + "|" + sQuotedPair + ")*\\x22";
    var sDomain_ref = sAtom;
    var sSubDomain = "(" + sDomain_ref + "|" + sDomainLiteral + ")";
    var sWord = "(" + sAtom + "|" + sQuotedString + ")";
    var sDomain = sSubDomain + "(\\x2e" + sSubDomain + ")*";
    var sLocalPart = sWord + "(\\x2e" + sWord + ")*";
    var sAddrSpec = sLocalPart + "\\x40" + sDomain; // complete RFC822 email address spec
    var sValidEmail = "^" + sAddrSpec + "$"; // as whole string

    var reValidEmail = new RegExp(sValidEmail);

    return reValidEmail.test(emailAddress);
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    let errors = {};
    let formIsValid = true;

    if (dataForm.get("email") == "") {
      formIsValid = false;
      errors["email"] = "Tidak boleh kosong";
    }
    if (dataForm.get("password") == "") {
      formIsValid = false;
      errors["password"] = "Tidak boleh kosong";
    }
    if (this.checkEmail(dataForm.get("email")) == false) {
      formIsValid = false;
      errors["email"] = "Format Email Salah";
    }
    this.setState({ errors });
    return formIsValid;
  }

  handleShowPWD = () => {
    if (this.state.isRevealPwd) {
      this.setState({ isRevealPwd: false });
    } else {
      this.setState({ isRevealPwd: true });
    }
  };

  handleSubmit(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();

    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      let data = {
        email: dataForm.get("email").toLowerCase(),
      };

      axios
        .post(process.env.REACT_APP_BASE_API_URI + "/auth/reset-password", data)
        .then((res) => {
          this.setState({ isLoading: false });
          const statusx = res.data.success;
          const messagex = res.data.message;
          if (statusx) {
            swal
              .fire({
                title: "Berhasil mengirimkan OTP, silahkan cek email anda",
                icon: "success",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                Cookies.set(
                  "user_email_reset",
                  dataForm.get("email").toLowerCase(),
                );

                if (result.isConfirmed) {
                  window.location = "/verifikasi-otp";
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {});
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let message =
            error.response.status == 504
              ? "Terjadi kesalahan, aplikasi sedang sibuk silahkan coba lagi nanti."
              : error.response.status == 401
                ? error.response.data.result.Message
                : error.response.status == 429
                  ? "Terlalu banyak percobaan login, mohon tunggu 1 menit kemudian"
                  : null;
          swal
            .fire({
              title: message ?? "Login Gagal",
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        });
    } else {
      console.log("validasi");
    }
  }
  componentDidMount() {}

  showConfirmPassword() {
    if (this.state.typeConfirmPassword == "password") {
      this.setState({
        typeConfirmPassword: "text",
        classConfirmEye: "fa fa-eye",
      });
    } else {
      this.setState({
        typeConfirmPassword: "password",
        classConfirmEye: "fa fa-eye-slash",
      });
    }
  }

  render() {
    return (
      <div>
        <div className="col-12">
          <div className="row">
            <div
              className="col-lg-8 mb-0 align-middle d-none d-lg-inline-block"
              style={{ backgroundColor: "#CDE9F6" }}
            >
              <div className="pb-5 px-5 px-lg-3 px-xl-5 vh-100 d-flex flex-column flex-center">
                <h1 className="text-gray-800 fs-2qx fw-bolder mb-7">
                  Login Administrator
                </h1>
                <img
                  className="theme-light-show mx-auto mt-6 mw-100 w-150px w-lg-300px mb-10 mb-lg-20"
                  src="assets/media/logos/login.png"
                  alt=""
                />
                <div className="text-dark text-center mt-6">
                  <a href="#" className="text-muted text-hover-primary">
                    Badan Pengembangan SDM Kominfo{" "}
                    <span className="text-muted fw-bold me-1">
                      © {new Date().getFullYear()}
                    </span>{" "}
                  </a>
                </div>
              </div>
            </div>
            <div
              className="col-lg-4 mb-0 align-middle d-none d-lg-inline-block"
              style={{ height: "100vh", backgroundColor: "#fff" }}
            >
              <div className="pb-5 p-0 vh-100 d-flex flex-column flex-center">
                <div className="col-lg-10">
                  <form
                    noValidate="novalidate"
                    id="kt_sign_in_form"
                    action="#"
                    onSubmit={this.handleClick}
                  >
                    <div className="text-center">
                      <img
                        alt="Logo"
                        src="assets/media/logos/dts.png"
                        className="align-items-center h-100px mt-5 mb-5"
                      />
                    </div>
                    <h3 className="text-center fw-bolder text-gray-600 mt-10">
                      Reset Password
                    </h3>
                    <p className="text-center">
                      Silahkan masukkan email anda untuk verifikasi
                    </p>
                    <div className="fv-row mt-8 mb-10">
                      <label className="form-label fs-6 fw-bolder text-dark">
                        Email
                      </label>
                      <input
                        className="form-control"
                        type="email"
                        name="email"
                        autoComplete="off"
                      />
                      <span style={{ color: "red" }}>
                        {this.state.errors["email"]}
                      </span>
                    </div>

                    <div className="text-center">
                      <button
                        type="submit"
                        className="btn btn-lg btn-primary mb-5 me-4"
                        disabled={this.state.isLoading}
                      >
                        {this.state.isLoading ? (
                          <>
                            <span
                              className="spinner-border spinner-border-sm me-2"
                              role="status"
                              aria-hidden="true"
                            ></span>
                            <span className="sr-only">Loading...</span>
                            Loading...
                          </>
                        ) : (
                          <>
                            <span className="indicator-label">Submit</span>
                          </>
                        )}
                      </button>
                      <a href="/" className="btn btn-lg btn-light mb-5">
                        Batal
                      </a>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div
          className="col-12 d-lg-none align-middle px-10"
          style={{ height: "100vh", backgroundColor: "#fff" }}
        >
          <div className="container py-8">
            <form
              noValidate="novalidate"
              id="kt_sign_in_form"
              action="#"
              onSubmit={this.handleClick}
            >
              <div className="text-center">
                <img
                  alt="Logo"
                  src="assets/media/logos/dts.png"
                  className="align-items-center h-100px mt-5 mb-5"
                />
              </div>
              <h3 className="text-center fw-bolder text-gray-600 mt-10">
                Reset Password
              </h3>
              <p className="text-center">
                Silahkan masukkan email anda untuk verifikasi
              </p>
              <div className="fv-row mt-8 mb-10">
                <label className="form-label fs-6 fw-bolder text-dark">
                  Email
                </label>
                <input
                  className="form-control"
                  type="text"
                  name="email"
                  autoComplete="off"
                />
                <span style={{ color: "red" }}>
                  {this.state.errors["email"]}
                </span>
              </div>

              <div className="text-center">
                <button
                  disabled={this.state.isLoading}
                  type="submit"
                  className="btn btn-lg btn-primary me-4 mb-5"
                >
                  {this.state.isLoading ? (
                    <>
                      <span
                        className="spinner-border spinner-border-sm me-2"
                        role="status"
                        aria-hidden="true"
                      ></span>
                      <span className="sr-only">Loading...</span>Loading...
                    </>
                  ) : (
                    <>
                      <span className="indicator-label">Submit</span>
                    </>
                  )}
                </button>
                <a href="/" className="btn btn-lg btn-light mb-5">
                  Batal
                </a>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
