import React, { useEffect, useState } from "react";
import axios from "axios";

const StrestTest = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    axios
      .post(process.env.REACT_APP_BASE_API_URI + "/strest-test", {})
      .then((response) => {
        console.log(response);

        setData(response.data.result.Data);
        setLoading(false);
      });
  }, []);

  return (
    <div>
      <div>
        <h1> Strest Test Page</h1>

        {loading ? (
          <h6>
            <i> Loading Data .....</i>
          </h6>
        ) : (
          ""
        )}

        <ul>{data.q1 && data.q1.map((row) => <li> {row.deskripsi}</li>)}</ul>

        {/* 
            <ul>
            {data.q2  && data.q2.map(row => 
              (<li> {row.email }</li> )
            )}
            </ul>

            <ul>
            {data.q3  && data.q3.map(row => 
              (<li> {row.name }</li> )
            )}
            </ul>

            <ul>
            {data.q4  && data.q4.map(row => 
              (<li> {row.name }</li> )
            )}
            </ul>

            <ul>
            {data.q5  && data.q5.map(row => 
              (<li> {row.name }</li> )
            )}
            </ul>

            <ul>
            {data.q6  && data.q6.map(row => 
              (<li> {row.name }</li> )
            )}
            </ul>

            <ul>
            {data.q7  && data.q7.map(row => 
              (<li> {row.nomor_registrasi }</li> )
            )}
            </ul> */}
      </div>
    </div>
  );
};

export default StrestTest;
