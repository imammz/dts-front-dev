import React from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import swal from "sweetalert2";

export default class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleSubmit.bind(this);
    this.state = {
      fields: {},
      errors: {},
      datax: [],
      dataxmitra: [],
    };
  }
  componentDidMount() {
    let config = {
      headers: {
        Authorization: "Bearer 19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp",
      },
    };
    const dataMitra = { start: 0, length: 100 };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list-mitra",
        dataMitra,
        config,
      )
      .then((res) => {
        const dataxmitra = res.data.result.Data;
        this.setState({ dataxmitra });
      });
  }
  resetError() {
    let errors = {};
    errors["name"] = "";
    errors["nik"] = "";
    errors["email"] = "";
    errors["password"] = "";
    errors["c_password"] = "";
    errors["nomor_hp"] = "";
    this.setState({ errors: errors });
  }
  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    let errors = {};
    let formIsValid = true;

    if (dataForm.get("name") == "") {
      formIsValid = false;
      errors["name"] = "Tidak boleh kosong";
    }
    if (dataForm.get("nik") == "") {
      formIsValid = false;
      errors["nik"] = "Tidak boleh kosong";
    }
    if (dataForm.get("email") == "") {
      formIsValid = false;
      errors["email"] = "Tidak boleh kosong";
    }
    if (dataForm.get("password") == "") {
      formIsValid = false;
      errors["password"] = "Tidak boleh kosong";
    }
    if (dataForm.get("c_password") == "") {
      formIsValid = false;
      errors["c_password"] = "Tidak boleh kosong";
    }
    if (dataForm.get("nomor_hp") == "") {
      formIsValid = false;
      errors["nomor_hp"] = "Tidak boleh kosong";
    }
    if (dataForm.get("toc") == null) {
      formIsValid = false;
      errors["toc"] = "Tidak boleh kosong";
    }
    this.setState({ errors: errors });
    return formIsValid;
  }
  handleSubmit(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      let data = {
        name: dataForm.get("name"),
        nik: dataForm.get("nik"),
        email: dataForm.get("email"),
        password: dataForm.get("password"),
        c_password: dataForm.get("c_password"),
        nomor_hp: dataForm.get("nomor_hp"),
        role_id: 4,
        mitra_id: dataForm.get("mitra"),
      };

      axios
        .post(process.env.REACT_APP_BASE_API_URI + "/registerx", data)
        .then((res) => {
          const statusx = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statusx) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/";
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          let statusx = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statusx) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          } else {
          }
        });
    } else {
    }
  }
  render() {
    return (
      <div>
        <div className="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
          <div className="d-flex flex-center flex-column flex-column-fluid p-10 mb-10">
            <div className="w-lg-600px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
              <form
                className="form w-100"
                noValidate="novalidate"
                id="kt_sign_up_form"
                onSubmit={this.handleClick}
              >
                <div className="mb-10 text-center">
                  <img
                    alt="Logo"
                    src="assets/media/logos/logo-black.png"
                    className="align-items-center h-60px mb-6"
                  />
                  <h4 className="text-dark mb-3">Registrasi Mitra</h4>
                  <div className="text-gray-400 fw-bold fs-5">
                    Jika sudah memiliki akun,
                    <br />
                    Anda dapat login di tautan ini&nbsp;
                    <Link to="/" className="link-primary fw-bolder">
                      Login
                    </Link>
                  </div>
                </div>
                <div className="row fv-row mb-7">
                  <div className="col-xl-6">
                    <label className="form-label fw-bolder text-dark fs-6 required">
                      Nama Depan
                    </label>
                    <input
                      className="form-control form-control-sm"
                      placeholder="Masukkan nama tema"
                      name="name"
                      value={this.state.fields["name"]}
                      autoComplete="off"
                    />
                    <span style={{ color: "red" }}>
                      {this.state.errors["name"]}
                    </span>
                  </div>
                  <div className="col-xl-6">
                    <label className="form-label fw-bolder text-dark fs-6 required">
                      NIK
                    </label>
                    <input
                      className="form-control form-control-sm"
                      type="text"
                      name="nik"
                      autoComplete="off"
                    />
                    <span style={{ color: "red" }}>
                      {this.state.errors["nik"]}
                    </span>
                  </div>
                </div>
                <div className="row fv-row mb-7">
                  <div className="col-xl-6">
                    <label className="form-label fw-bolder text-dark fs-6 required">
                      Email
                    </label>
                    <input
                      className="form-control form-control-sm"
                      type="email"
                      name="email"
                      autoComplete="off"
                    />
                    <span style={{ color: "red" }}>
                      {this.state.errors["email"]}
                    </span>
                  </div>
                  <div className="col-xl-6">
                    <label className="form-label fw-bolder text-dark fs-6 required">
                      Nomor HP
                    </label>
                    <input
                      className="form-control form-control-sm"
                      type="text"
                      name="nomor_hp"
                      autoComplete="off"
                    />
                    <span style={{ color: "red" }}>
                      {this.state.errors["nomor_hp"]}
                    </span>
                  </div>
                </div>
                <div className="col-lg-12 mb-7 fv-row">
                  <label className="form-label fw-bolder text-dark fs-6 required">
                    Pilih Mitra
                  </label>
                  <select
                    className="form-select form-select-sm selectpicker"
                    data-control="select2"
                    data-placeholder="Select an option"
                    name="mitra"
                  >
                    {this.state.dataxmitra.map((data) => (
                      <option value={data.id}>{data.pic_name}</option>
                    ))}
                  </select>
                </div>
                <div className="mb-10 fv-row" data-kt-password-meter="true">
                  <div className="mb-1">
                    <label className="form-label fw-bolder text-dark fs-6 required">
                      Password
                    </label>
                    <div className="position-relative mb-3">
                      <input
                        className="form-control form-control-sm"
                        type="password"
                        name="password"
                        autoComplete="off"
                      />
                      <span style={{ color: "red" }}>
                        {this.state.errors["password"]}
                      </span>
                      <span
                        className="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2"
                        data-kt-password-meter-control="visibility"
                      >
                        <i className="bi bi-eye-slash fs-2" />
                        <i className="bi bi-eye fs-2 d-none" />
                      </span>
                    </div>
                    <div
                      className="d-flex align-items-center mb-3"
                      data-kt-password-meter-control="highlight"
                    >
                      <div className="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2" />
                      <div className="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2" />
                      <div className="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2" />
                      <div className="flex-grow-1 bg-secondary bg-active-success rounded h-5px" />
                    </div>
                  </div>
                  {/* <div className="text-muted">Gunakan 8 karakter atau lebih dengan kombinasi huruf, angka &amp; dan simbol.</div> */}
                </div>
                <div className="fv-row mb-5">
                  <label className="form-label fw-bolder text-dark fs-6 required">
                    Konfirmasi Password
                  </label>
                  <input
                    className="form-control form-control-sm"
                    type="password"
                    name="c_password"
                    autoComplete="off"
                  />
                  <span style={{ color: "red" }}>
                    {this.state.errors["c_password"]}
                  </span>
                </div>
                <div className="fv-row mb-10">
                  <label className="form-check form-check-custom form-check-solid form-check-inline">
                    <input
                      className="form-check-input"
                      type="checkbox"
                      name="toc"
                      defaultValue={1}
                    />
                    <span className="form-check-label fw-bold text-gray-700 fs-6">
                      Saya Setuju
                      <a href="#" className="ms-1 link-primary">
                        Syarat dan ketentuan
                      </a>
                      .
                    </span>
                  </label>
                  <span style={{ color: "red" }}>
                    {this.state.errors["toc"]}
                  </span>
                </div>
                <div className="text-center">
                  <button
                    type="submit"
                    id="kt_sign_up_submit"
                    className="btn btn-lg btn-primary"
                  >
                    <span className="indicator-label">Submit</span>
                    <span className="indicator-progress">
                      Tunggu...
                      <span className="spinner-border spinner-border-sm align-middle ms-2" />
                    </span>
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
