import React from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";

export default class SignInMitra extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleSubmit.bind(this);
    this.showConfirmPassword = this.showConfirmPassword.bind(this);
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.state = {
      fields: {},
      errors: {},
      datax: [],
      isRevealPwd: false,
      isLoading: false,
      classConfirmEye: "fa fa-eye-slash",
      typeConfirmPassword: "password",
    };
  }
  resetError() {
    let errors = {};
    errors["name"] = "";
    errors["deskripsi"] = "";
    this.setState({ errors: errors });
  }
  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    let errors = {};
    let formIsValid = true;

    if (dataForm.get("email") == "") {
      formIsValid = false;
      errors["email"] = "Tidak boleh kosong";
    }
    if (dataForm.get("password") == "") {
      formIsValid = false;
      errors["password"] = "Tidak boleh kosong";
    }
    this.setState({ errors: errors });
    return formIsValid;
  }

  handleShowPWD = () => {
    if (this.state.isRevealPwd) {
      this.setState({ isRevealPwd: false });
    } else {
      this.setState({ isRevealPwd: true });
    }
  };

  handleChangeEmail(e) {
    if (e.currentTarget.value !== "") {
      this.setState({
        errors: { email: "", password: this.state.errors["password"] },
      });
    } else {
      this.setState({
        errors: {
          email: "Tidak boleh kosong",
          password: this.state.errors["password"],
        },
      });
    }
  }

  handleChangePassword(e) {
    if (e.currentTarget.value !== "") {
      this.setState({
        errors: { password: "", email: this.state.errors["email"] },
      });
    } else {
      this.setState({
        errors: {
          password: "Tidak boleh kosong",
          email: this.state.errors["email"],
        },
      });
    }
  }

  handleSubmit(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      let data = {
        email: dataForm.get("email").toLowerCase(),
        password: dataForm.get("password"),
      };

      axios
        .post(process.env.REACT_APP_BASE_API_URI + "/loginx", data)
        .then((res) => {
          this.setState({ isLoading: false });
          const statusx = res.data.result.Status;
          const messagex = res.data.result.Message;
          // console.log(data)
          // return
          if (statusx) {
            let isMitra = 0;

            res.data.result.role_in_user.map((row) => {
              if (row.role_id == 4 || row.role_id == 86) {
                isMitra = 1;
              }
            });

            if (isMitra == 1) {
              Cookies.set("token", res.data.result.Aksestoken);
              Cookies.set("user_id", res.data.result.Data[0].id);
              Cookies.set("user_email", res.data.result.Data[0].email);
              Cookies.set("user_name", res.data.result.Data[0].name);
              Cookies.set("user_nik", res.data.result.Data[0].nik);
              Cookies.set("user_role", res.data.result.Data[0].role_name);
              Cookies.set("menus", res.data.result.m_role_menu_admin);
              Cookies.set(
                "role_id_user",
                res.data.result.role_in_user[0].role_id,
              );
              Cookies.set(
                "user_role_name",
                res.data.result.role_in_user[0].name,
              );

              localStorage.setItem(
                "dataMenus",
                JSON.stringify(res.data.result.m_role_menu_admin),
              );

              if (
                res.data.result.partner_id !== undefined &&
                res.data.result.partner_id !== null
              ) {
                Cookies.set("partner_id", res.data.result.partner_id);
              }

              console.log(res.data.result.m_role_menu_admin);
              console.log(localStorage.getItem("dataMenus"));

              console.log(res.data.result);

              window.location = "/partnership/kerjasama";
            } else {
              swal
                .fire({
                  title: "Akses Khusus Mitra, Akun anda tidak diizinkan",
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {});
            }
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {});
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          console.log(error.response);
          let message =
            error.response.status == 504
              ? "Terjadi kesalahan, aplikasi sedang sibuk silahkan coba lagi nanti."
              : "Terjadi kesalahan";
          swal
            .fire({
              title: message ?? "Login Gagal, Email atau Password Salah",
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        });
    } else {
    }
  }
  componentDidMount() {
    const token = Cookies.get("token");
    const partner_id = Cookies.get("partner_id");
    if (partner_id) {
      window.location = "/partnership/kerjasama";
    } else if (token) {
      window.location = "/dashboard/digitalent";
    }
  }

  showConfirmPassword() {
    if (this.state.typeConfirmPassword == "password") {
      this.setState({
        typeConfirmPassword: "text",
        classConfirmEye: "fa fa-eye",
      });
    } else {
      this.setState({
        typeConfirmPassword: "password",
        classConfirmEye: "fa fa-eye-slash",
      });
    }
  }

  render() {
    return (
      <div>
        <div className="row">
          <div
            className="col-lg-8 mb-0 align-middle d-none d-lg-block"
            style={{ backgroundColor: "#CDE9F6" }}
          >
            <div className="pb-5 px-5 px-lg-3 px-xl-5 vh-100 d-flex flex-column flex-center">
              <h1 className="text-gray-800 fs-2qx fw-bolder mb-7">
                Login Mitra
              </h1>
              <img
                className="theme-light-show mx-auto mt-6 mw-100 w-150px w-lg-300px mb-10 mb-lg-20"
                src="assets/media/logos/login.png"
                alt=""
              />
              <div className="text-dark text-center mt-6">
                <a href="#" className="text-muted text-hover-primary">
                  Badan Pengembangan SDM Kominfo{" "}
                  <span className="text-muted fw-bold me-1">
                    © {new Date().getFullYear()}
                  </span>{" "}
                </a>
              </div>
            </div>
          </div>
          <div
            className="col-lg-4 mb-0 align-middle d-none d-lg-block"
            style={{ height: "100vh", backgroundColor: "#fff" }}
          >
            <div className="pb-5 p-0 vh-100 d-flex flex-column flex-center">
              <div className="col-lg-12 px-10">
                <form
                  className="form w-100"
                  noValidate="novalidate"
                  id="kt_sign_in_form"
                  action="#"
                  onSubmit={this.handleClick}
                >
                  <div className="text-center">
                    <img
                      alt="Logo"
                      src="assets/media/logos/logo-black.png"
                      className="align-items-center h-60px mt-5 mb-5"
                    />
                  </div>
                  <div className="fv-row mt-8 mb-10">
                    <input
                      className="form-control form-control-sm"
                      type="text"
                      name="email"
                      onChange={this.handleChangeEmail}
                      autoComplete="off"
                    />
                    <span style={{ color: "red" }}>
                      {this.state.errors["email"]}
                    </span>
                  </div>
                  <div className="fv-row mb-10">
                    <div className="d-flex flex-stack mb-2">
                      <label className="form-label fw-bolder text-dark fs-6 mb-0">
                        Password
                      </label>
                      {/* <Link to="/forget-password" className="link-primary fs-6 fw-bolder">Lupa Kata Sandi ?</Link> */}
                    </div>
                    <div className="row ">
                      <div className="col-12">
                        <div className="input-group mb-3">
                          <input
                            className="form-control form-control-sm"
                            type={this.state.typeConfirmPassword}
                            name="password"
                            onChange={this.handleChangePassword}
                            autoComplete="off"
                          />

                          <span
                            title="show/hide password"
                            style={{ cursor: "pointer" }}
                            className="input-group-text"
                            id="basic-addon2"
                            onClick={this.showConfirmPassword}
                          >
                            <i className={this.state.classConfirmEye}></i>
                          </span>
                        </div>
                      </div>
                    </div>
                    <span style={{ color: "red" }}>
                      {this.state.errors["password"]}
                    </span>
                  </div>
                  <div className="text-center">
                    <button
                      type="submit"
                      id="kt_sign_in_submit"
                      className="btn btn-lg btn-primary w-100 mb-5"
                      disabled={this.state.isLoading}
                    >
                      {this.state.isLoading ? (
                        <>
                          <span
                            className="spinner-border spinner-border-sm me-2"
                            role="status"
                            aria-hidden="true"
                          ></span>
                          <span className="sr-only">Loading...</span>Loading...
                        </>
                      ) : (
                        <>
                          <span className="indicator-label">Log In</span>
                        </>
                      )}
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div
          className="col-12 d-lg-none align-middle px-10"
          style={{ height: "100vh", backgroundColor: "#fff" }}
        >
          <div className="container py-8">
            <form
              className="form w-100"
              noValidate="novalidate"
              id="kt_sign_in_form"
              action="#"
              onSubmit={this.handleClick}
            >
              <div className="text-center">
                <img
                  alt="Logo"
                  src="assets/media/logos/logo-black.png"
                  className="align-items-center h-60px mt-5 mb-5"
                />
              </div>
              <div className="fv-row mt-8 mb-10">
                <input
                  className="form-control form-control-sm"
                  type="text"
                  name="email"
                  onChange={this.handleChangeEmail}
                  autoComplete="off"
                />
                <span style={{ color: "red" }}>
                  {this.state.errors["email"]}
                </span>
              </div>
              <div className="fv-row mb-10">
                <div className="d-flex flex-stack mb-2">
                  <label className="form-label fw-bolder text-dark fs-6 mb-0">
                    Password
                  </label>
                  {/* <Link to="/forget-password" className="link-primary fs-6 fw-bolder">Lupa Kata Sandi ?</Link> */}
                </div>
                <div className="row ">
                  <div className="col-12">
                    <div className="input-group mb-3">
                      <input
                        className="form-control form-control-sm"
                        type={this.state.typeConfirmPassword}
                        name="password"
                        onChange={this.handleChangePassword}
                        autoComplete="off"
                      />

                      <span
                        title="show/hide password"
                        style={{ cursor: "pointer" }}
                        className="input-group-text"
                        id="basic-addon2"
                        onClick={this.showConfirmPassword}
                      >
                        <i className={this.state.classConfirmEye}></i>
                      </span>
                    </div>
                  </div>
                </div>
                <span style={{ color: "red" }}>
                  {this.state.errors["password"]}
                </span>
              </div>
              <div className="text-center">
                <button
                  type="submit"
                  id="kt_sign_in_submit"
                  className="btn btn-lg btn-primary w-100 mb-5"
                >
                  <span className="indicator-label">Log In</span>
                  <span className="indicator-progress">
                    Tunggu ...
                    <span className="spinner-border spinner-border-sm align-middle ms-2" />
                  </span>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
