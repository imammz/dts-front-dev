import React from "react";
import { Link } from "react-router-dom";

const ForgetPassword = () => {
  return (
    <div>
      <div className="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
        {/*begin::Content*/}
        <div className="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
          {/*begin::Logo*/}
          <a href="/dashboard/digitalent" className="mb-12">
            <img
              alt="Logo"
              src="assets/media/logos/logo-1.svg"
              className="h-40px"
            />
          </a>
          {/*end::Logo*/}
          {/*begin::Wrapper*/}
          <div className="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
            {/*begin::Form*/}
            <form
              className="form w-100"
              noValidate="novalidate"
              id="kt_password_reset_form"
            >
              {/*begin::Heading*/}
              <div className="text-center mb-10">
                {/*begin::Title*/}
                <h1 className="text-dark mb-3">Lupa Kata Sandi ?</h1>
                {/*end::Title*/}
                {/*begin::Link*/}
                <div className="text-gray-400 fw-bold fs-4">
                  Ketikkan email Anda untuk me-reset kata sandi Anda.
                </div>
                {/*end::Link*/}
              </div>
              {/*begin::Heading*/}
              {/*begin::Input group*/}
              <div className="fv-row mb-10">
                <label className="form-label fw-bolder text-gray-900 fs-6">
                  Email
                </label>
                <input
                  className="form-control form-control-solid"
                  type="email"
                  placeholder
                  name="email"
                  autoComplete="off"
                />
              </div>
              {/*end::Input group*/}
              {/*begin::Actions*/}
              <div className="d-flex flex-wrap justify-content-center pb-lg-0">
                <button
                  type="button"
                  id="kt_password_reset_submit"
                  className="btn btn-lg btn-primary fw-bolder me-4"
                >
                  <span className="indicator-label">Submit</span>
                  <span className="indicator-progress">
                    Please wait...
                    <span className="spinner-border spinner-border-sm align-middle ms-2" />
                  </span>
                </button>
                <Link to="/" className="btn btn-lg btn-light-primary fw-bolder">
                  Batal
                </Link>
              </div>
              {/*end::Actions*/}
            </form>
            {/*end::Form*/}
          </div>
          {/*end::Wrapper*/}
        </div>
        {/*end::Content*/}
        {/*begin::Footer*/}
        <div className="d-flex flex-center flex-column-auto p-10">
          {/*begin::Links*/}
          <div className="d-flex align-items-center fw-bold fs-6">
            <a
              href="https://keenthemes.com"
              className="text-muted text-hover-primary px-2"
            >
              Tentang
            </a>
            <a
              href="mailto:support@keenthemes.com"
              className="text-muted text-hover-primary px-2"
            >
              Kontak
            </a>
          </div>
          {/*end::Links*/}
        </div>
        {/*end::Footer*/}
      </div>
    </div>
  );
};

export default ForgetPassword;
