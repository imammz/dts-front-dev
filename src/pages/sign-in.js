import React from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";

export default class SignIn extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleSubmit.bind(this);
    this.showConfirmPassword = this.showConfirmPassword.bind(this);
    this.state = {
      fields: {},
      errors: {},
      datax: [],
      isRevealPwd: false,
      isLoading: false,
      classConfirmEye: "fa fa-eye-slash",
      typeConfirmPassword: "password",
      atemps: 0,
    };
  }
  resetError() {
    let errors = {};
    errors["name"] = "";
    errors["deskripsi"] = "";
    this.setState({ errors: errors });
  }
  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    let errors = {};
    let formIsValid = true;

    if (dataForm.get("email") == "") {
      formIsValid = false;
      errors["email"] = "Tidak boleh kosong";
    }
    if (dataForm.get("password") == "") {
      formIsValid = false;
      errors["password"] = "Tidak boleh kosong";
    }
    this.setState({ errors });
    return formIsValid;
  }

  handleShowPWD = () => {
    if (this.state.isRevealPwd) {
      this.setState({ isRevealPwd: false });
    } else {
      this.setState({ isRevealPwd: true });
    }
  };

  handleSubmit(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      let data = {
        email: dataForm.get("email").toLowerCase(),
        password: dataForm.get("password"),
      };

      axios
        .post(process.env.REACT_APP_BASE_API_URI + "/loginx", data)
        .then((res) => {
          this.setState({ isLoading: false });
          const statusx = res.data.result.Status;
          const messagex = res.data.result.Message;
          const reset_pass = res.data.result.reset_pass;

          if (statusx && !reset_pass) {
            Cookies.set("token", res.data.result.Aksestoken);
            Cookies.set("user_id", res.data.result.Data[0].id);
            Cookies.set("user_email", res.data.result.Data[0].email);
            Cookies.set("user_name", res.data.result.Data[0].name);
            Cookies.set("user_nik", res.data.result.Data[0].nik);
            Cookies.set("user_flag_nik", res.data.result.Data[0].flag_nik);
            Cookies.set("user_role", res.data.result.Data[0].role_name);
            Cookies.set("menus", res.data.result.m_role_menu_admin);
            Cookies.set("mrole_id", res.data.result.role_in_user[0].role_id);
            Cookies.set("satker_id", res.data.result.role_in_user[0].id_satker);
            Cookies.set(
              "satker_name",
              res.data.result.role_in_user[0].nama_satker,
            );
            Cookies.set("user_role_name", res.data.result.role_in_user[0].name);
            Cookies.set(
              "role_id_user",
              res.data.result.role_in_user[0].role_id,
            );

            localStorage.setItem(
              "dataMenus",
              JSON.stringify(res.data.result.m_role_menu_admin),
            );

            let isAdmin = 0;

            res.data.result.role_in_user.map((row) => {
              if (row.role_id != 4 && row.role_id != 86) {
                isAdmin = 1;
              }
            });

            if (isAdmin == 1) {
              window.location = "/dashboard/digitalent";
            } else {
              swal
                .fire({
                  title: "Akses Anda Tidak Diizinkan",
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {});
            }
          } else if (statusx && reset_pass) {
            swal
              .fire({
                title:
                  "Anda belum melakukan pergantian password selama kurun waktu 3 bulan atau lebih, Mohon lakukan pergantian password",
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/reset-password";
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {});
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let message =
            error.response.status == 504
              ? "Terjadi kesalahan, aplikasi sedang sibuk silahkan coba lagi nanti."
              : error.response.status == 401
                ? error.response.data.result.Message
                : error.response.status == 429
                  ? "Terlalu banyak percobaan login, mohon tunggu 1 menit kemudian"
                  : null;
          swal
            .fire({
              title: message ?? "Login Gagal",
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        });
    } else {
      console.log("validasi");
    }
  }
  componentDidMount() {
    const token = Cookies.get("token");
    const partner_id = Cookies.get("partner_id");
    if (partner_id) {
      window.location = "/partnership/kerjasama";
    } else if (token) {
      window.location = "/dashboard/digitalent";
    }
  }

  showConfirmPassword() {
    if (this.state.typeConfirmPassword == "password") {
      this.setState({
        typeConfirmPassword: "text",
        classConfirmEye: "fa fa-eye",
      });
    } else {
      this.setState({
        typeConfirmPassword: "password",
        classConfirmEye: "fa fa-eye-slash",
      });
    }
  }

  render() {
    return (
      <div>
        <div className="col-12">
          <div className="row">
            <div
              className="col-lg-8 mb-0 align-middle d-none d-lg-inline-block"
              style={{ backgroundColor: "#CDE9F6" }}
            >
              <div className="pb-5 px-5 px-lg-3 px-xl-5 vh-100 d-flex flex-column flex-center">
                <h1 className="text-gray-800 fs-2qx fw-bolder mb-7">
                  Login Administrator
                </h1>
                <img
                  className="theme-light-show mx-auto mt-6 mw-100 w-150px w-lg-300px mb-10 mb-lg-20"
                  src="assets/media/logos/login.png"
                  alt=""
                />
                <div className="text-dark text-center mt-6">
                  <a href="#" className="text-muted text-hover-primary">
                    Badan Pengembangan SDM Kominfo{" "}
                    <span className="text-muted fw-bold me-1">
                      © {new Date().getFullYear()}
                    </span>{" "}
                  </a>
                </div>
              </div>
            </div>
            <div
              className="col-lg-4 mb-0 align-middle d-none d-lg-inline-block"
              style={{ height: "100vh", backgroundColor: "#fff" }}
            >
              <div className="pb-5 p-0 vh-100 d-flex flex-column flex-center">
                <div className="col-lg-10">
                  <form
                    noValidate="novalidate"
                    id="kt_sign_in_form"
                    action="#"
                    onSubmit={this.handleClick}
                  >
                    <div className="text-center">
                      <img
                        alt="Logo"
                        src="assets/media/logos/dts.png"
                        className="align-items-center h-100px mt-5 mb-5"
                      />
                    </div>
                    <div className="fv-row mt-8 mb-10">
                      <label className="form-label fs-6 fw-bolder text-dark">
                        Email
                      </label>
                      <input
                        className="form-control"
                        type="text"
                        name="email"
                        autoComplete="off"
                      />
                      <span style={{ color: "red" }}>
                        {this.state.errors["email"]}
                      </span>
                    </div>
                    <div className="fv-row mb-10">
                      <div className="d-flex flex-stack mb-2">
                        <label className="form-label fw-bolder text-dark fs-6 mb-0">
                          Password
                        </label>
                        {/* <Link to="/forget-password" className="link-primary fs-6 fw-bolder">Lupa Kata Sandi ?</Link> */}
                      </div>
                      <div className="row ">
                        <div className="col-12">
                          <div className="input-group mb-3">
                            <input
                              className="form-control"
                              type={this.state.typeConfirmPassword}
                              name="password"
                              autoComplete="off"
                            />

                            <span
                              title="show/hide password"
                              style={{ cursor: "pointer" }}
                              className="input-group-text"
                              id="basic-addon2"
                              onClick={this.showConfirmPassword}
                            >
                              <i className={this.state.classConfirmEye}></i>
                            </span>
                          </div>
                        </div>
                      </div>

                      <span style={{ color: "red" }}>
                        {this.state.errors["password"]}
                      </span>
                    </div>
                    <div className="text-center">
                      <button
                        type="submit"
                        className="btn btn-lg btn-primary w-100 mb-5"
                        disabled={this.state.isLoading}
                      >
                        {this.state.isLoading ? (
                          <>
                            <span
                              className="spinner-border spinner-border-sm me-2"
                              role="status"
                              aria-hidden="true"
                            ></span>
                            <span className="sr-only">Loading...</span>
                            Loading...
                          </>
                        ) : (
                          <>
                            <span className="indicator-label">Log In</span>
                          </>
                        )}
                      </button>
                    </div>
                  </form>
                  <div className="text-center pt-5 mt-5 mb-0">
                    <p>Didukung oleh</p>
                    <img
                      alt="Logo"
                      src="assets/media/logos/bsre-logo.png"
                      className="align-items-center h-40px mt-3 mb-5"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div
          className="col-12 d-lg-none align-middle px-10"
          style={{ height: "100vh", backgroundColor: "#fff" }}
        >
          <div className="container py-8">
            <form
              noValidate="novalidate"
              id="kt_sign_in_form"
              action="#"
              onSubmit={this.handleClick}
            >
              <div className="text-center">
                <img
                  alt="Logo"
                  src="assets/media/logos/dts.png"
                  className="align-items-center h-100px mt-5 mb-5"
                />
              </div>
              <div className="fv-row mt-8 mb-10">
                <label className="form-label fs-6 fw-bolder text-dark">
                  Email
                </label>
                <input
                  className="form-control"
                  type="text"
                  name="email"
                  autoComplete="off"
                />
                <span style={{ color: "red" }}>
                  {this.state.errors["email"]}
                </span>
              </div>
              <div className="fv-row mb-10">
                <div className="d-flex flex-stack mb-2">
                  <label className="form-label fw-bolder text-dark fs-6 mb-0">
                    Password
                  </label>
                  {/* <Link to="/forget-password" className="link-primary fs-6 fw-bolder">Lupa Kata Sandi ?</Link> */}
                </div>
                <div className="row ">
                  <div className="col-12">
                    <div className="input-group mb-3">
                      <input
                        className="form-control"
                        type={this.state.typeConfirmPassword}
                        name="password"
                        autoComplete="off"
                      />

                      <span
                        title="show/hide password"
                        style={{ cursor: "pointer" }}
                        className="input-group-text"
                        id="basic-addon2"
                        onClick={this.showConfirmPassword}
                      >
                        <i className={this.state.classConfirmEye}></i>
                      </span>
                    </div>
                  </div>
                </div>

                <span style={{ color: "red" }}>
                  {this.state.errors["password"]}
                </span>
              </div>
              <div className="text-center">
                <button
                  disabled={this.state.isLoading}
                  type="submit"
                  className="btn btn-lg btn-primary w-100 mb-5"
                >
                  {this.state.isLoading ? (
                    <>
                      <span
                        className="spinner-border spinner-border-sm me-2"
                        role="status"
                        aria-hidden="true"
                      ></span>
                      <span className="sr-only">Loading...</span>Loading...
                    </>
                  ) : (
                    <>
                      <span className="indicator-label">Log In</span>
                    </>
                  )}
                </button>
              </div>
            </form>
            <div className="text-center pt-5 mt-5 mb-0">
              <p>Didukung oleh</p>
              <img
                alt="Logo"
                src="assets/media/logos/bsre-logo.png"
                className="align-items-center h-40px mt-3 mb-5"
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
