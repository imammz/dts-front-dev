import React from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";

export default class KonfirmPassword extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleSubmit.bind(this);
    this.showConfirmPassword = this.showConfirmPassword.bind(this);
    this.state = {
      fields: {},
      errors: {},
      datax: [],
      isRevealPwd: false,
      isLoading: false,
      classConfirmEye: "fa fa-eye-slash",
      typeConfirmPassword: "password",
      atemps: 0,
    };
  }
  resetError() {
    let errors = {};
    errors["name"] = "";
    errors["deskripsi"] = "";
    this.setState({ errors: errors });
  }

  checkPass(pass) {
    var sValidPass =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[-._!"`'#%&,:;<>=@{}~\$\(\)\*\+\/\\\?\[\]\^\|])[A-Za-z\d-._!"`'#%&,:;<>=@{}~\$\(\)\*\+\/\\\?\[\]\^\|]{8,}$/;

    var reValidPass = new RegExp(sValidPass);

    return reValidPass.test(pass);
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    let errors = {};
    let formIsValid = true;

    if (dataForm.get("email") == "") {
      formIsValid = false;
      errors["email"] = "Tidak boleh kosong";
    }
    if (dataForm.get("password") == "") {
      formIsValid = false;
      errors["password"] = "Tidak boleh kosong";
    }
    if (dataForm.get("password_confirmation") !== dataForm.get("password")) {
      formIsValid = false;
      errors["password_confirmation"] = "Konfirmasi Password Tidak Sama";
    }
    if (this.checkPass(dataForm.get("password")) == false) {
      formIsValid = false;
      errors["password"] =
        "Password minimal 8 karakter, huruf besar dan kecil, angka, serta spesial karakter (@$!#%*?&)";
    }
    this.setState({ errors });
    return formIsValid;
  }

  handleShowPWD = () => {
    if (this.state.isRevealPwd) {
      this.setState({ isRevealPwd: false });
    } else {
      this.setState({ isRevealPwd: true });
    }
  };

  handleSubmit(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      let data = {
        email: Cookies.get("user_email_reset"),
        pin: Cookies.get("pin"),
        password: dataForm.get("password"),
        password_confirmation: dataForm.get("password_confirmation"),
      };

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/auth/change-password",
          data,
        )
        .then((res) => {
          this.setState({ isLoading: false });

          const statusx = res.data.success;
          const messagex = res.data.message;

          if (statusx) {
            swal
              .fire({
                title:
                  "Berhasil melakukan pergantian password, silahkan login kembali",
                icon: "success",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/";
                  Cookies.remove("user_email_reset");
                  Cookies.remove("pin");
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {});
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let message =
            error.response.status == 504
              ? "Terjadi kesalahan, aplikasi sedang sibuk silahkan coba lagi nanti."
              : error.response.status == 401
                ? error.response.data.result.Message
                : error.response.status == 429
                  ? "Terlalu banyak percobaan login, mohon tunggu 1 menit kemudian"
                  : null;
          swal
            .fire({
              title: message ?? "Login Gagal",
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        });
    } else {
      console.log("validasi");
    }
  }
  componentDidMount() {
    const user_email_reset = Cookies.get("user_email_reset");

    if (!user_email_reset) {
      window.location = "/";
    }
  }

  showConfirmPassword() {
    if (this.state.typeConfirmPassword == "password") {
      this.setState({
        typeConfirmPassword: "text",
        classConfirmEye: "fa fa-eye",
      });
    } else {
      this.setState({
        typeConfirmPassword: "password",
        classConfirmEye: "fa fa-eye-slash",
      });
    }
  }

  render() {
    return (
      <div>
        <div className="col-12">
          <div className="row">
            <div
              className="col-lg-8 mb-0 align-middle d-none d-lg-inline-block"
              style={{ backgroundColor: "#CDE9F6" }}
            >
              <div className="pb-5 px-5 px-lg-3 px-xl-5 vh-100 d-flex flex-column flex-center">
                <h1 className="text-gray-800 fs-2qx fw-bolder mb-7">
                  Login Administrator
                </h1>
                <img
                  className="theme-light-show mx-auto mt-6 mw-100 w-150px w-lg-300px mb-10 mb-lg-20"
                  src="assets/media/logos/login.png"
                  alt=""
                />
                <div className="text-dark text-center mt-6">
                  <a href="#" className="text-muted text-hover-primary">
                    Badan Pengembangan SDM Kominfo{" "}
                    <span className="text-muted fw-bold me-1">
                      © {new Date().getFullYear()}
                    </span>{" "}
                  </a>
                </div>
              </div>
            </div>
            <div
              className="col-lg-4 mb-0 align-middle d-none d-lg-inline-block"
              style={{ height: "100vh", backgroundColor: "#fff" }}
            >
              <div className="pb-5 p-0 vh-100 d-flex flex-column flex-center">
                <div className="col-lg-10">
                  <form
                    noValidate="novalidate"
                    id="kt_sign_in_form"
                    action="#"
                    onSubmit={this.handleClick}
                  >
                    <div className="text-center">
                      <img
                        alt="Logo"
                        src="assets/media/logos/dts.png"
                        className="align-items-center h-100px mt-5 mb-5"
                      />
                    </div>
                    <h3 className="text-center fw-bolder text-gray-600 mt-10">
                      Password Baru
                    </h3>
                    <p className="text-center">
                      Silahkan buat password baru anda
                    </p>
                    <div className="fv-row mt-8 mb-10">
                      <div className="d-flex flex-stack mb-2">
                        <label className="form-label fw-bolder text-dark fs-6 mb-0">
                          Password Baru
                        </label>
                        {/* <Link to="/forget-password" className="link-primary fs-6 fw-bolder">Lupa Kata Sandi ?</Link> */}
                      </div>
                      <div className="row ">
                        <div className="col-12">
                          <div className="input-group mb-3">
                            <input
                              className="form-control"
                              type={this.state.typeConfirmPassword}
                              name="password"
                              autoComplete="off"
                            />

                            <span
                              title="show/hide password"
                              style={{ cursor: "pointer" }}
                              className="input-group-text"
                              id="basic-addon2"
                              onClick={this.showConfirmPassword}
                            >
                              <i className={this.state.classConfirmEye}></i>
                            </span>
                          </div>
                        </div>
                      </div>

                      <span style={{ color: "red" }}>
                        {this.state.errors["password"]}
                      </span>
                    </div>

                    <div className="fv-row mb-10">
                      <div className="d-flex flex-stack mb-2">
                        <label className="form-label fw-bolder text-dark fs-6 mb-0">
                          Konfirmasi Password Baru
                        </label>
                        {/* <Link to="/forget-password" className="link-primary fs-6 fw-bolder">Lupa Kata Sandi ?</Link> */}
                      </div>
                      <div className="row ">
                        <div className="col-12">
                          <div className="input-group mb-3">
                            <input
                              className="form-control"
                              type={this.state.typeConfirmPassword}
                              name="password_confirmation"
                              autoComplete="off"
                            />

                            <span
                              title="show/hide password"
                              style={{ cursor: "pointer" }}
                              className="input-group-text"
                              id="basic-addon2"
                              onClick={this.showConfirmPassword}
                            >
                              <i className={this.state.classConfirmEye}></i>
                            </span>
                          </div>
                        </div>
                      </div>

                      <span style={{ color: "red" }}>
                        {this.state.errors["password_confirmation"]}
                      </span>
                    </div>

                    <div className="text-center">
                      <button
                        type="submit"
                        className="btn btn-lg btn-primary mb-5 w-100"
                        disabled={this.state.isLoading}
                      >
                        {this.state.isLoading ? (
                          <>
                            <span
                              className="spinner-border spinner-border-sm me-2"
                              role="status"
                              aria-hidden="true"
                            ></span>
                            <span className="sr-only">Loading...</span>
                            Loading...
                          </>
                        ) : (
                          <>
                            <span className="indicator-label">Submit</span>
                          </>
                        )}
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div
          className="col-12 d-lg-none align-middle px-10"
          style={{ height: "100vh", backgroundColor: "#fff" }}
        >
          <div className="container py-8">
            <form
              noValidate="novalidate"
              id="kt_sign_in_form"
              action="#"
              onSubmit={this.handleClick}
            >
              <div className="text-center">
                <img
                  alt="Logo"
                  src="assets/media/logos/dts.png"
                  className="align-items-center h-100px mt-5 mb-5"
                />
              </div>
              <h3 className="text-center fw-bolder text-gray-600 mt-10">
                Password Baru
              </h3>
              <p className="text-center">Silahkan buat password baru anda</p>
              <div className="fv-row mt-8 mb-10">
                <div className="d-flex flex-stack mb-2">
                  <label className="form-label fw-bolder text-dark fs-6 mb-0">
                    Password Baru
                  </label>
                  {/* <Link to="/forget-password" className="link-primary fs-6 fw-bolder">Lupa Kata Sandi ?</Link> */}
                </div>
                <div className="row ">
                  <div className="col-12">
                    <div className="input-group mb-3">
                      <input
                        className="form-control"
                        type={this.state.typeConfirmPassword}
                        name="password"
                        autoComplete="off"
                      />

                      <span
                        title="show/hide password"
                        style={{ cursor: "pointer" }}
                        className="input-group-text"
                        id="basic-addon2"
                        onClick={this.showConfirmPassword}
                      >
                        <i className={this.state.classConfirmEye}></i>
                      </span>
                    </div>
                  </div>
                </div>

                <span style={{ color: "red" }}>
                  {this.state.errors["password"]}
                </span>
              </div>

              <div className="fv-row mb-10">
                <div className="d-flex flex-stack mb-2">
                  <label className="form-label fw-bolder text-dark fs-6 mb-0">
                    Konfirmasi Password Baru
                  </label>
                  {/* <Link to="/forget-password" className="link-primary fs-6 fw-bolder">Lupa Kata Sandi ?</Link> */}
                </div>
                <div className="row ">
                  <div className="col-12">
                    <div className="input-group mb-3">
                      <input
                        className="form-control"
                        type={this.state.typeConfirmPassword}
                        name="password"
                        autoComplete="off"
                      />

                      <span
                        title="show/hide password"
                        style={{ cursor: "pointer" }}
                        className="input-group-text"
                        id="basic-addon2"
                        onClick={this.showConfirmPassword}
                      >
                        <i className={this.state.classConfirmEye}></i>
                      </span>
                    </div>
                  </div>
                </div>

                <span style={{ color: "red" }}>
                  {this.state.errors["password"]}
                </span>
              </div>

              <div className="text-center">
                <button
                  disabled={this.state.isLoading}
                  type="submit"
                  className="btn btn-lg btn-primary w-100 mb-5"
                >
                  {this.state.isLoading ? (
                    <>
                      <span
                        className="spinner-border spinner-border-sm me-2"
                        role="status"
                        aria-hidden="true"
                      ></span>
                      <span className="sr-only">Loading...</span>Loading...
                    </>
                  ) : (
                    <>
                      <span className="indicator-label">Submit</span>
                    </>
                  )}
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
