import axios from "axios";
export default axios.create({
  baseURL: "https://dtsapi.sdmdigital.id",
  headers: {
    "Content-type": "Application/Json;multipart/form-data;charset=utf-8;",
    Authorization: "Bearer 10|QHc6uxmBgXtpLYVc9l7yvCsZpW5VsVAoD1jj0jzr",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "POST, GET, OPTIONS, PUT",
    // "Accept" : "Application/Json",
    "Cache-Control": "false",
    "Access-Control-Allow-Headers":
      "Content-Type, api-key, Authorization, Origin, X-Requested-With,Accept",
  },
});
