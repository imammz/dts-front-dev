import React from "react";

export default function Custom502() {
  return (
    <>
      <div>
        <div className="row">
          <div
            className="col-lg-12 mb-0 align-middle"
            style={{ backgroundColor: "#CDE9F6" }}
          >
            <div className="pb-5 px-5 px-lg-3 px-xl-5 vh-100 d-flex flex-column flex-center">
              <img
                alt="Logo"
                src="assets/media/logos/logo-black.png"
                className="align-items-center h-60px mb-n5"
              />
              <img
                className="theme-light-show mx-auto mt-6 mw-100 w-300px w-lg-400px mb-10 mb-lg-20"
                src="assets/media/something-wrong.png"
                alt=""
              />
              <h3 className="text-gray-800 text-center fw-bolder mb-7">
                Maaf, Server sedang Sibuk.
                <br />
                Silahkan coba beberapa saat lagi
              </h3>
              <div className="text-center mt-6">
                <a
                  href="javascript:history.back()"
                  className="btn btn-lg rounded-pill btn-primary"
                >
                  Reload Halaman
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
