import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";

export default class PelatihanContent extends React.Component {
  constructor(props) {
    super(props);
    Cookies.remove("pelatian_id");
    this.handleClick = this.handleClickAction.bind(this);
    this.handleClick2 = this.handleClick2Action.bind(this);
    this.handleClick3 = this.handleClick3Action.bind(this);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleClickMetode = this.handleClickMetodeAction.bind(this);
    this.handleClickPreviewForm = this.handleClickPreviewFormAction.bind(this);
    this.handleClickTambahForm = this.handleClickTambahFormAction.bind(this);
    this.handleClickRefForm = this.handleClickRefFormAction.bind(this);
    this.handleChangeProv = this.handleChangeProvAction.bind(this);
    this.handleChangeKomitmen = this.handleChangeKomitmenAction.bind(this);
    this.handleChangeSilabus = this.handleChangeSilabusAction.bind(this);
    this.handleChangeDeskripsiKomitmen =
      this.handleChangeDeskripsiKomitmenAction.bind(this);
    this.handleKuotaPendaftar = this.handleKuotaPendaftarAction.bind(this);
    this.handleKuotaPeserta = this.handleKuotaPesertaAction.bind(this);
    this.handleSubmit = this.handleSubmitAction.bind(this);
    this.state = {
      fields: {},
      errors: {},
      dataxakademi: [],
      dataxtema: [],
      dataxprov: [],
      dataxkab: [],
      dataxpenyelenggara: [],
      dataxmitra: [],
      dataxzonasi: [],
      silabusfile: [],
      showing: true,
      showingdeskripsi: false,
      dataxselform: [],
      valuekomitmen: "",
      valuedeskripsikomitmen: "",
      valuejudulform: "",
      dataxlevelpelatihan: [],
      dataxpreview: [],
      dataxpreviewtitle: "",
      selectedOption: null,
      akademi_id: null,
      isDisabled: true,
      isDisabledKab: true,
      kuotapendaftar: 0,
      kuotapeserta: 0,
    };
    this.formDatax = new FormData();
  }
  componentDidMount() {
    let config = {
      headers: {
        Authorization: "Bearer 19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp",
      },
    };
    const dataAkademik = { start: 0, length: 100 };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/akademi/list-akademi",
        dataAkademik,
        config,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxakademi = [];
        optionx.map((data) =>
          dataxakademi.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxakademi });
      });
    const dataProv = { start: 1, rows: 1000 };
    axios
      .post(process.env.REACT_APP_BASE_API_URI + "/provinsi", dataProv, config)
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxprov = [];
        optionx.map((data) =>
          dataxprov.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxprov });
      });
    const dataSelForm = { start: 0, length: 1000 };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/list-formbuilder",
        dataSelForm,
        config,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxselform = [];
        optionx.map((data) =>
          dataxselform.push({ value: data.id, label: data.judul_form }),
        );
        this.setState({ dataxselform });
      });
    const dataLvl = {};
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/levelpelatihan",
        dataLvl,
        config,
      )
      .then((res) => {
        const options = res.data.result.Data;
        const dataxlevelpelatihan = [];
        options.map((data) =>
          dataxlevelpelatihan.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxlevelpelatihan });
      });
    const dataPenyelenggara = {
      mulai: 0,
      limit: 9999,
      cari: "",
      sort: "id desc",
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list_satker",
        dataPenyelenggara,
        config,
      )
      .then((res) => {
        const options = res.data.result.Data;
        const dataxpenyelenggara = [];
        options.map((data) =>
          dataxpenyelenggara.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxpenyelenggara });
      });
    const dataMitra = { start: 0, length: 100 };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/umum/list-mitra",
        dataMitra,
        config,
      )
      .then((res) => {
        const options = res.data.result.Data;
        const dataxmitra = [];
        options.map((data) =>
          dataxmitra.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxmitra });
      });
    const dataZonasi = { start: 1, length: 100 };
    axios
      .post(process.env.REACT_APP_BASE_API_URI + "/zonasi", dataZonasi, config)
      .then((res) => {
        const options = res.data.result.Data;
        const dataxzonasi = [];
        options.map((data) =>
          dataxzonasi.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxzonasi });
      });
  }
  handleChangeProvAction = (provinsi_id) => {
    let config = {
      headers: {
        Authorization: "Bearer 19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp",
      },
    };
    const dataKab = { kdprop: provinsi_id.value };
    axios
      .post(process.env.REACT_APP_BASE_API_URI + "/kabupaten", dataKab, config)
      .then((res) => {
        this.setState({ isDisabledKab: false });
        const options = res.data.result.Data;
        const dataxkab = [];
        options.map((data) =>
          dataxkab.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxkab });
      });
  };
  handleChangeKomitmenAction(e) {
    this.setState({ valuekomitmen: e.target.value });
    if (e.target.value == "Iya") {
      this.setState({ showingdeskripsi: true });
    } else {
      this.setState({ showingdeskripsi: false });
    }
  }
  handleChangeDeskripsiKomitmenAction(e) {
    this.setState({ valuedeskripsikomitmen: e.target.value });
  }
  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    let errors = {};
    let formIsValid = true;

    if (dataForm.get("level_pelatihan") === "") {
      formIsValid = false;
      errors["level_pelatihan"] = "Tidak boleh kosong";
    }
    if (dataForm.get("akademi_id") === "") {
      formIsValid = false;
      errors["akademi_id"] = "Tidak boleh kosong";
    } else {
      if (dataForm.get("tema_id") === "") {
        formIsValid = false;
        errors["tema_id"] = "Tidak boleh kosong";
      }
    }
    if (dataForm.get("penyelenggara") === "") {
      formIsValid = false;
      errors["penyelenggara"] = "Tidak boleh kosong";
    }
    if (dataForm.get("mitra") === "") {
      formIsValid = false;
      errors["mitra"] = "Tidak boleh kosong";
    }
    if (dataForm.get("zonasi") === "") {
      formIsValid = false;
      errors["zonasi"] = "Tidak boleh kosong";
    }
    if (dataForm.get("program_dts") == null) {
      formIsValid = false;
      errors["program_dts"] = "Tidak boleh kosong";
    }
    if (dataForm.get("name") === "") {
      formIsValid = false;
      errors["name"] = "Tidak boleh kosong";
    }
    if (dataForm.get("upload_silabus").name == "") {
      formIsValid = false;
      errors["upload_silabus"] = "Tidak boleh kosong";
    }
    if (dataForm.get("metode_pelaksanaan") == null) {
      formIsValid = false;
      errors["metode_pelaksanaan"] = "Tidak boleh kosong";
    }
    if (dataForm.get("pendaftaran_tanggal_mulai") == "") {
      formIsValid = false;
      errors["pendaftaran_tanggal_mulai"] = "Tidak boleh kosong";
    }
    if (dataForm.get("pendaftaran_tanggal_selesai") == "") {
      formIsValid = false;
      errors["pendaftaran_tanggal_selesai"] = "Tidak boleh kosong";
    }
    if (dataForm.get("pelatihan_tanggal_mulai") == "") {
      formIsValid = false;
      errors["pelatihan_tanggal_mulai"] = "Tidak boleh kosong";
    }
    if (dataForm.get("pelatihan_tanggal_selesai") == "") {
      formIsValid = false;
      errors["pelatihan_tanggal_selesai"] = "Tidak boleh kosong";
    }
    if (dataForm.get("deskripsi") == "") {
      formIsValid = false;
      errors["deskripsi"] = "Tidak boleh kosong";
    }
    if (dataForm.get("kuota_target_pendaftar") == "") {
      formIsValid = false;
      errors["kuota_target_pendaftar"] = "Tidak boleh kosong";
    }
    if (dataForm.get("kuota_target_peserta") == "") {
      formIsValid = false;
      errors["kuota_target_peserta"] = "Tidak boleh kosong";
    }
    if (dataForm.get("kuota_target_peserta") == null) {
      formIsValid = false;
      errors["kuota_target_peserta"] = "Tidak boleh kosong";
    } else {
      if (
        parseInt(dataForm.get("kuota_target_peserta")) >
        parseInt(dataForm.get("kuota_target_pendaftar"))
      ) {
        formIsValid = false;
        errors["kuota_target_peserta"] =
          "Kuota peserta tidak boleh lebih dari kuota pendaftar";
      }
    }
    if (dataForm.get("alur_pendaftaran") == null) {
      formIsValid = false;
      errors["alur_pendaftaran"] = "Tidak boleh kosong";
    }
    if (dataForm.get("sertifikasi") == null) {
      formIsValid = false;
      errors["sertifikasi"] = "Tidak boleh kosong";
    }
    if (dataForm.get("lpj_peserta") == null) {
      formIsValid = false;
      errors["lpj_peserta"] = "Tidak boleh kosong";
    }
    if (dataForm.get("metode_pelatihan") == null) {
      formIsValid = false;
      errors["metode_pelatihan"] = "Tidak boleh kosong";
    }
    if (
      dataForm.get("metode_pelatihan") == "Offline" ||
      dataForm.get("metode_pelatihan") == null ||
      dataForm.get("metode_pelatihan") == "Online & Offline"
    ) {
      if (dataForm.get("provinsi") === "") {
        formIsValid = false;
        errors["provinsi"] = "Tidak boleh kosong";
      } else {
        if (dataForm.get("kota_kabupaten") === "") {
          formIsValid = false;
          errors["kota_kabupaten"] = "Tidak boleh kosong";
        }
      }
      if (dataForm.get("alamat") == "") {
        formIsValid = false;
        errors["alamat"] = "Tidak boleh kosong";
      }
    }
    if (dataForm.get("batch") == "") {
      formIsValid = false;
      errors["batch"] = "Tidak boleh kosong";
    } else {
      if (/[0-9]/.test(dataForm.get("batch")) == false) {
        formIsValid = false;
        errors["batch"] = "Diisi dengan angka";
      }
    }
    this.setState({ errors: errors });
    return formIsValid;
  }
  handleValidation2(e) {
    let errors = {};
    let formIsValid = true;
    if (this.state.valuejudulform === "") {
      formIsValid = false;
      errors["judul_form"] = "Tidak boleh kosong";
    }
    this.setState({ errors: errors });
    return formIsValid;
  }
  handleValidation3(e) {
    let errors = {};
    let formIsValid = true;
    if (this.state.valuekomitmen == "") {
      formIsValid = false;
      errors["komitmen_peserta"] = "Tidak boleh kosong";
    }
    if (this.state.valuekomitmen == "Iya") {
      if (this.state.valuedeskripsikomitmen == "") {
        formIsValid = false;
        errors["deskripsi_komitmen"] = "Tidak boleh kosong";
      }
    }
    this.setState({ errors: errors });
    return formIsValid;
  }
  resetError() {
    let errors = {};
    errors["name"] = "";
    errors["deskripsi"] = "";
    this.setState({ errors: errors });
  }
  handleChangeSilabusAction(e) {
    const silabusfile = e.target.files[0];
    this.formDatax.append("silabus", silabusfile);
  }
  handleClickMetodeAction(e) {
    if (e.target.value == "Online") {
      this.setState({ showing: false });
    } else {
      this.setState({ showing: true });
    }
  }
  handleDate(dateString) {
    if (dateString != "") {
      const arr = dateString.split("-");
      const arrTime = arr[2].split(" ");
      const newDate =
        arrTime[0].trim() +
        "-" +
        arr[1].trim() +
        "-" +
        arr[0].trim() +
        " " +
        arrTime[1].trim() +
        ":00";
      return newDate;
    }
  }
  handleClickAction(e) {
    const dataForm = new FormData(e.currentTarget);
    const dataFormSubmit = new FormData();
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      let config = {
        headers: {
          Authorization: "Bearer 19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp",
        },
      };
      let pendaftaran_mulai = this.handleDate(
        dataForm.get("pendaftaran_tanggal_mulai"),
      );
      let pendaftaran_selesai = this.handleDate(
        dataForm.get("pendaftaran_tanggal_selesai"),
      );
      let pelatihan_mulai = this.handleDate(
        dataForm.get("pelatihan_tanggal_mulai"),
      );
      let pelatihan_selesai = this.handleDate(
        dataForm.get("pelatihan_tanggal_selesai"),
      );
      dataFormSubmit.append("program_dts", dataForm.get("program_dts"));
      dataFormSubmit.append("name", dataForm.get("name"));
      dataFormSubmit.append("level_pelatihan", dataForm.get("level_pelatihan"));
      dataFormSubmit.append("akademi_id", dataForm.get("akademi_id"));
      dataFormSubmit.append("tema_id", dataForm.get("tema_id"));
      dataFormSubmit.append(
        "metode_pelaksanaan",
        dataForm.get("metode_pelaksanaan"),
      );
      dataFormSubmit.append("id_penyelenggara", dataForm.get("penyelenggara"));
      dataFormSubmit.append("mitra", dataForm.get("mitra"));
      dataFormSubmit.append("deskripsi", dataForm.get("deskripsi"));
      dataFormSubmit.append("pendaftaran_mulai", pendaftaran_mulai);
      dataFormSubmit.append("pendaftaran_selesai", pendaftaran_selesai);
      dataFormSubmit.append("pelatihan_mulai", pelatihan_mulai);
      dataFormSubmit.append("pelatihan_selesai", pelatihan_selesai);
      dataFormSubmit.append(
        "kuota_pendaftar",
        dataForm.get("kuota_target_pendaftar"),
      );
      dataFormSubmit.append(
        "kuota_peserta",
        dataForm.get("kuota_target_peserta"),
      );
      dataFormSubmit.append("status_kuota", dataForm.get("status_kuota"));
      dataFormSubmit.append(
        "alur_pendaftaran",
        dataForm.get("alur_pendaftaran"),
      );
      dataFormSubmit.append("sertifikasi", dataForm.get("sertifikasi"));
      dataFormSubmit.append("lpj_peserta", dataForm.get("lpj_peserta"));
      dataFormSubmit.append(
        "metode_pelatihan",
        dataForm.get("metode_pelatihan"),
      );
      if (dataForm.get("metode_pelatihan") == "Offline") {
        dataFormSubmit.append("alamat", dataForm.get("alamat"));
        dataFormSubmit.append("provinsi", dataForm.get("provinsi"));
        dataFormSubmit.append("kabupaten", dataForm.get("kota_kabupaten"));
      } else {
        dataFormSubmit.append("alamat", "online");
        dataFormSubmit.append("provinsi", "online");
        dataFormSubmit.append("kabupaten", "online");
      }
      dataFormSubmit.append("zonasi", dataForm.get("zonasi"));
      dataFormSubmit.append("batch", dataForm.get("batch"));
      dataFormSubmit.append("umum", dataForm.get("umum") == null ? "0" : "1");
      dataFormSubmit.append(
        "tuna_netra",
        dataForm.get("tuna_netra") == null ? "0" : "1",
      );
      dataFormSubmit.append(
        "tuna_rungu",
        dataForm.get("tuna_rungu") == null ? "0" : "1",
      );
      dataFormSubmit.append(
        "tuna_daksa",
        dataForm.get("tuna_daksa") == null ? "0" : "1",
      );
      dataFormSubmit.append("silabus", this.formDatax.get("silabus"));
      dataFormSubmit.append("status_publish", "1");
      dataFormSubmit.append("status_substansi", "review");
      dataFormSubmit.append("status_pelatihan", "review");
      // dataFormSubmit.append("logo", e.target.upload_thumbnail.files[0]);
      // dataFormSubmit.append("thumbnail", e.target.upload_thumbnail.files[0]);

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/addpelatihan",
          dataFormSubmit,
          config,
        )
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          Cookies.set("pelatian_id", res.data.result.Data);
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          let messagex = error.response.data.message;
          let contentx =
            error.response.data.data.silabus[0] +
            " " +
            error.response.data.data.silabus[1];
          swal
            .fire({
              title: messagex,
              text: contentx,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        });
    } else {
      window.scrollTo(0, 0);
    }
  }
  handleClickPreviewFormAction(e) {
    console.log("handlePreviewForm");
    e.preventDefault();
    if (this.handleValidation2(e)) {
      const dataFormSubmit = new FormData();
      let config = {
        headers: {
          Authorization: "Bearer 19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp",
        },
      };
      dataFormSubmit.append("id", this.state.valuejudulform);
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/cari-formbuilder",
          dataFormSubmit,
          config,
        )
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            const dataxpreview = res.data.result.detail;
            const dataxpreviewtitle = res.data.result.utama[0].judul_form;
            this.setState({ dataxpreview });
            this.setState({ dataxpreviewtitle });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  //this.handleReload();
                }
              });
          }
        });
    }
  }
  handleClick2Action(e) {
    e.preventDefault();
    const cook_pelatian_id = Cookies.get("pelatian_id");
    if (cook_pelatian_id == undefined) {
      swal
        .fire({
          title: "Harap isi Form Tambah Pelatihan terlebih Dahulu",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/pelatihan/tambah-pelatihan";
          }
        });
    } else {
      if (this.handleValidation2(e)) {
        const dataFormSubmit = new FormData();
        let config = {
          headers: {
            Authorization: "Bearer 19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp",
          },
        };
        dataFormSubmit.append("pelatian_id", Cookies.get("pelatian_id"));
        dataFormSubmit.append("form_pendaftaran_id", this.state.valuejudulform);
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/addformpendaftaran",
            dataFormSubmit,
            config,
          )
          .then((res) => {
            const statux = res.data.result.Status;
            const messagex = res.data.result.Message;
            if (statux) {
              swal
                .fire({
                  title: messagex,
                  icon: "success",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                  }
                });
            } else {
              swal
                .fire({
                  title: messagex,
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                  }
                });
            }
          })
          .catch((error) => {
            let messagex = error.response.data.result.Message;
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          });
      }
    }
  }
  handleClick3Action(e) {
    e.preventDefault();
    const cook_pelatian_id = Cookies.get("pelatian_id");
    if (cook_pelatian_id == undefined) {
      swal
        .fire({
          title: "Harap isi Form Tambah Pelatihan terlebih Dahulu",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/pelatihan/tambah-pelatihan";
          }
        });
    } else {
      if (this.handleValidation3(e)) {
        const dataFormSubmit = new FormData();
        let config = {
          headers: {
            Authorization: "Bearer 19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp",
          },
        };
        dataFormSubmit.append("pelatian_id", Cookies.get("pelatian_id"));
        dataFormSubmit.append("komitmen", this.state.valuekomitmen);
        dataFormSubmit.append("deskripsi", this.state.valuedeskripsikomitmen);
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/addkomitmen",
            dataFormSubmit,
            config,
          )
          .then((res) => {
            const statux = res.data.result.Status;
            const messagex = res.data.result.Message;
            if (statux) {
              swal
                .fire({
                  title: messagex,
                  icon: "success",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                    window.location = "/pelatihan/pelatihan";
                  }
                });
            } else {
              swal
                .fire({
                  title: messagex,
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                    //this.handleReload();
                  }
                });
            }
          });
      }
    }
  }
  handleClickBatalAction(e) {
    window.location = "/pelatihan/pelatihan";
  }
  handleClickTambahFormAction(e) {
    window.open("/pelatihan/pendaftaran/add", "_blank");
  }
  handleChangeJudulFormAction = (judul_form) => {
    this.setState({ valuejudulform: judul_form.value });
  };
  handleClickRefFormAction(e) {
    let config = {
      headers: {
        Authorization: "Bearer 19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp",
      },
    };
    const dataSelForm = { start: 0, length: 1000 };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/list-formbuilder",
        dataSelForm,
        config,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxselform = [];
        optionx.map((data) =>
          dataxselform.push({ value: data.id, label: data.judul_form }),
        );
        this.setState({ dataxselform });
      });
  }
  handleChangeAkademi = (akademi_id) => {
    let config = {
      headers: {
        Authorization: "Bearer 19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp",
      },
    };
    const dataBody = { start: 1, rows: 100, id: akademi_id.value };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/cari_tema_byakademi",
        dataBody,
        config,
      )
      .then((res) => {
        this.setState({ isDisabled: false });
        const optionx = res.data.result.Data;
        const dataxtema = [];
        optionx.map((data) =>
          dataxtema.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxtema });
      })
      .catch((error) => {
        const dataxtema = [];
        this.setState({ dataxtema });
        let messagex = error.response.data.result.Message;
        console.log(error);
        // swal
        //   .fire({
        //     title: messagex,
        //     icon: "warning",
        //     confirmButtonText: "Ok",
        //   })
        //   .then((result) => {
        //     if (result.isConfirmed) {
        //     }
        //   });
      });
    // this.setState({ akademi_id }, () =>
    //   this.state.akademi_id.value
    // );
  };
  handleKuotaPendaftarAction(e) {
    this.setState({ kuotapendaftar: e.target.kuota_target_pendaftar });
  }
  handleKuotaPesertaAction(e) {
    let errors = {};
    if (parseInt(this.state.kuotapendaftar) < parseInt(e.target.value)) {
      errors["kuota_target_peserta"] =
        "Kuota peserta tidak boleh lebih dari kuota pendaftar";
      e.target.value = this.state.kuotapendaftar;
    } else {
      errors["kuota_target_peserta"] = "";
    }
    this.setState({ errors: errors });
  }

  handleSubmitAction(e) {
    console.log("handleSubmitAction");
    e.preventDefault();
    if (this.handleValidation3(e)) {
      const dataForm = new FormData(e.currentTarget);
      const dataFormSubmit = new FormData();
      e.preventDefault();
      let config = {
        headers: {
          Authorization: "Bearer 19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp",
        },
      };
      let pendaftaran_mulai = this.handleDate(
        dataForm.get("pendaftaran_tanggal_mulai"),
      );
      let pendaftaran_selesai = this.handleDate(
        dataForm.get("pendaftaran_tanggal_selesai"),
      );
      let pelatihan_mulai = this.handleDate(
        dataForm.get("pelatihan_tanggal_mulai"),
      );
      let pelatihan_selesai = this.handleDate(
        dataForm.get("pelatihan_tanggal_selesai"),
      );
      dataFormSubmit.append("program_dts", dataForm.get("program_dts"));
      dataFormSubmit.append("name", dataForm.get("name"));
      dataFormSubmit.append("level_pelatihan", dataForm.get("level_pelatihan"));
      dataFormSubmit.append("akademi_id", dataForm.get("akademi_id"));
      dataFormSubmit.append("tema_id", dataForm.get("tema_id"));
      dataFormSubmit.append(
        "metode_pelaksanaan",
        dataForm.get("metode_pelaksanaan"),
      );
      dataFormSubmit.append("id_penyelenggara", dataForm.get("penyelenggara"));
      dataFormSubmit.append("mitra", dataForm.get("mitra"));
      dataFormSubmit.append("deskripsi", dataForm.get("deskripsi"));
      dataFormSubmit.append("pendaftaran_mulai", pendaftaran_mulai);
      dataFormSubmit.append("pendaftaran_selesai", pendaftaran_selesai);
      dataFormSubmit.append("pelatihan_mulai", pelatihan_mulai);
      dataFormSubmit.append("pelatihan_selesai", pelatihan_selesai);
      dataFormSubmit.append(
        "kuota_pendaftar",
        dataForm.get("kuota_target_pendaftar"),
      );
      dataFormSubmit.append(
        "kuota_peserta",
        dataForm.get("kuota_target_peserta"),
      );
      dataFormSubmit.append("status_kuota", dataForm.get("status_kuota"));
      dataFormSubmit.append(
        "alur_pendaftaran",
        dataForm.get("alur_pendaftaran"),
      );
      dataFormSubmit.append("sertifikasi", dataForm.get("sertifikasi"));
      dataFormSubmit.append("lpj_peserta", dataForm.get("lpj_peserta"));
      dataFormSubmit.append(
        "metode_pelatihan",
        dataForm.get("metode_pelatihan"),
      );
      if (dataForm.get("metode_pelatihan") == "Offline") {
        dataFormSubmit.append("alamat", dataForm.get("alamat"));
        dataFormSubmit.append("provinsi", dataForm.get("provinsi"));
        dataFormSubmit.append("kabupaten", dataForm.get("kota_kabupaten"));
      } else {
        dataFormSubmit.append("alamat", "online");
        dataFormSubmit.append("provinsi", "online");
        dataFormSubmit.append("kabupaten", "online");
      }
      dataFormSubmit.append("zonasi", dataForm.get("zonasi"));
      dataFormSubmit.append("batch", dataForm.get("batch"));
      dataFormSubmit.append("umum", dataForm.get("umum") == null ? "0" : "1");
      dataFormSubmit.append(
        "tuna_netra",
        dataForm.get("tuna_netra") == null ? "0" : "1",
      );
      dataFormSubmit.append(
        "tuna_rungu",
        dataForm.get("tuna_rungu") == null ? "0" : "1",
      );
      dataFormSubmit.append(
        "tuna_daksa",
        dataForm.get("tuna_daksa") == null ? "0" : "1",
      );
      dataFormSubmit.append("silabus", this.formDatax.get("silabus"));
      dataFormSubmit.append("status_publish", "1");
      dataFormSubmit.append("status_substansi", "review");
      dataFormSubmit.append("status_pelatihan", "review");

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/addpelatihan",
          dataFormSubmit,
          config,
        )
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          Cookies.set("pelatian_id", res.data.result.Data);
          if (statux) {
            const dataFormSubmit = new FormData();
            let config = {
              headers: {
                Authorization:
                  "Bearer 19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp",
              },
            };
            dataFormSubmit.append("pelatian_id", Cookies.get("pelatian_id"));
            dataFormSubmit.append(
              "form_pendaftaran_id",
              this.state.valuejudulform,
            );
            axios
              .post(
                process.env.REACT_APP_BASE_API_URI + "/addformpendaftaran",
                dataFormSubmit,
                config,
              )
              .then((res) => {
                const statux = res.data.result.Status;
                const messagex = res.data.result.Message;
                if (statux) {
                  const dataFormSubmit = new FormData();
                  let config = {
                    headers: {
                      Authorization:
                        "Bearer 19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp",
                    },
                  };
                  dataFormSubmit.append("id", Cookies.get("pelatian_id"));
                  dataFormSubmit.append("komitmen", this.state.valuekomitmen);
                  dataFormSubmit.append(
                    "deskripsi",
                    this.state.valuedeskripsikomitmen,
                  );
                  axios
                    .post(
                      process.env.REACT_APP_BASE_API_URI + "/addkomitmen",
                      dataFormSubmit,
                      config,
                    )
                    .then((res) => {
                      const statux = res.data.result.Status;
                      const messagex = res.data.result.Message;
                      if (statux) {
                        swal
                          .fire({
                            title: messagex,
                            icon: "success",
                            confirmButtonText: "Ok",
                          })
                          .then((result) => {
                            if (result.isConfirmed) {
                              window.location = "/pelatihan/pelatihan";
                            }
                          });
                      } else {
                        swal
                          .fire({
                            title: messagex,
                            icon: "warning",
                            confirmButtonText: "Ok",
                          })
                          .then((result) => {
                            if (result.isConfirmed) {
                              //this.handleReload();
                            }
                          });
                      }
                    })
                    .catch((error) => {
                      let messagex = error.response.data.result.Message;
                      swal
                        .fire({
                          title: messagex,
                          icon: "warning",
                          confirmButtonText: "Ok",
                        })
                        .then((result) => {
                          if (result.isConfirmed) {
                          }
                        });
                    });
                  // swal.fire({
                  //   title: messagex,
                  //   icon: 'success',
                  //   confirmButtonText: 'Ok'
                  // }).then((result) => {
                  //   if (result.isConfirmed) {}
                  // });
                } else {
                  swal
                    .fire({
                      title: messagex,
                      icon: "warning",
                      confirmButtonText: "Ok",
                    })
                    .then((result) => {
                      if (result.isConfirmed) {
                      }
                    });
                }
              })
              .catch((error) => {
                let messagex = error.response.data.result.Message;
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              });
            // swal.fire({
            //   title: messagex,
            //   icon: 'success',
            //   confirmButtonText: 'Ok'
            // }).then((result) => {
            //   if (result.isConfirmed) {}
            // });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          let messagex = error.response.data.result.Message;
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        });
    }
  }
  render() {
    return (
      <div>
        <input
          type="hidden"
          name="csrf-token"
          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
        />
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="card">
                      <div
                        className="stepper stepper-links d-flex flex-column"
                        id="kt_subvit_testsubstansi"
                      >
                        <div className="stepper-nav mb-5">
                          <div
                            className="stepper-item current"
                            data-kt-stepper-element="nav"
                          >
                            <h3 className="stepper-title">
                              1. Buat Soal Substansi
                            </h3>
                          </div>
                          <div
                            className="stepper-item"
                            data-kt-stepper-element="nav"
                          >
                            <h3 className="stepper-title">
                              2. Bank Soal Substansi
                            </h3>
                          </div>
                          <div
                            className="stepper-item"
                            data-kt-stepper-element="nav"
                          >
                            <h3 className="stepper-title">
                              3. Publish Substansi
                            </h3>
                          </div>
                        </div>
                        <div className="modal-body scroll-y">
                          <form
                            action="#"
                            onSubmit={this.handleSubmit}
                            id="kt_subvit_testsubstansi_form"
                          >
                            <div
                              className="current"
                              data-kt-stepper-element="content"
                            >
                              <div className="card-body">
                                <div className="card-title">
                                  <div className="card mb-12 mb-xl-8">
                                    <h2 className="me-3 mr-2">
                                      Tambah Test Substansi
                                    </h2>
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Akademi
                                    </label>
                                    <Select
                                      id="id_akademi"
                                      name="akademi_ids"
                                      placeholder="Silahkan pilih"
                                      noOptionsMessage={({ inputValue }) =>
                                        !inputValue
                                          ? this.state.dataxakademi
                                          : "Data tidak tersedia"
                                      }
                                      className="form-select-sm selectpicker p-0"
                                      onChange={this.handleChangeAkademi}
                                      options={this.state.dataxakademi}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["akademi_id"]}
                                    </span>
                                  </div>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Tema
                                    </label>
                                    <Select
                                      name="tema_ids"
                                      placeholder="Silahkan pilih"
                                      noOptionsMessage={({ inputValue }) =>
                                        !inputValue
                                          ? this.state.dataxtema
                                          : "Data tidak tersedia"
                                      }
                                      className="form-select-sm selectpicker p-0"
                                      options={this.state.dataxtema}
                                      isDisabled={this.state.isDisabled}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["tema_id"]}
                                    </span>
                                  </div>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Pelatihan
                                    </label>
                                    <Select
                                      name="tema_ids"
                                      placeholder="Silahkan pilih"
                                      noOptionsMessage={({ inputValue }) =>
                                        !inputValue
                                          ? this.state.dataxtema
                                          : "Data tidak tersedia"
                                      }
                                      className="form-select-sm selectpicker p-0"
                                      options={this.state.dataxtema}
                                      isDisabled={this.state.isDisabled}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["tema_id"]}
                                    </span>
                                  </div>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Kategori
                                    </label>
                                    <Select
                                      name="tema_ids2"
                                      placeholder="Silahkan pilih"
                                      noOptionsMessage={({ inputValue }) =>
                                        !inputValue
                                          ? this.state.dataxtema
                                          : "Data tidak tersedia"
                                      }
                                      className="form-select-sm selectpicker p-0"
                                      options={this.state.dataxtema}
                                      isDisabled={this.state.isDisabled}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["tema_id"]}
                                    </span>
                                  </div>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Metode
                                    </label>
                                    <div className="d-flex">
                                      <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                        <input
                                          className="form-check-input"
                                          type="radio"
                                          name="metode"
                                          value="Iya"
                                          id="metode1"
                                        />
                                        <label
                                          className="form-check-label"
                                          for="metode1"
                                        >
                                          Entry Soal
                                        </label>
                                      </div>
                                      <div className="form-check form-check-sm form-check-custom form-check-solid">
                                        <input
                                          className="form-check-input"
                                          type="radio"
                                          name="metode"
                                          value="Tidak"
                                          id="metode2"
                                        />
                                        <label
                                          className="form-check-label"
                                          for="metode2"
                                        >
                                          Import .csv/.xls
                                        </label>
                                      </div>
                                    </div>
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["program_dts"]}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div data-kt-stepper-element="content">
                              <div
                                id="kt_content_container"
                                className="container-xxl"
                              >
                                <div className="card-body">
                                  <div className="card-title">
                                    <div className="card mb-12 mb-xl-8">
                                      <h2 className="me-3 mr-2">
                                        Bank Soal Substansi
                                      </h2>
                                      <h1 className="me-3 mr-2">Soal 1</h1>
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label required">
                                        Tipe Soal
                                      </label>
                                      <Select
                                        id="id_akademix2"
                                        name="akademi_id"
                                        placeholder="Silahkan pilih"
                                        noOptionsMessage={({ inputValue }) =>
                                          !inputValue
                                            ? this.state.dataxakademi
                                            : "Data tidak tersedia"
                                        }
                                        className="form-select-sm selectpicker p-0"
                                        onChange={this.handleChangeAkademi}
                                        options={this.state.dataxakademi}
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["akademi_id"]}
                                      </span>
                                    </div>
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label required">
                                        Pertanyaan
                                      </label>
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan pertanyaan"
                                        name="pertanyaan"
                                        value={this.state.fields["pertanyaan"]}
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["pertanyaan"]}
                                      </span>
                                    </div>
                                    <div className="col-lg-12">
                                      <div className="mb-7 fv-row">
                                        <label className="form-label">
                                          Gambar Pertanyaan (Optional)
                                        </label>
                                        <input
                                          type="file"
                                          className="form-control form-control-sm mb-2"
                                          name="upload_silabus"
                                          accept=".jpg/.jpeg/.png/.svg"
                                          onChange={this.handleChangeSilabus}
                                        />
                                        <small className="text-muted">
                                          Format File (.jpg/.jpeg/.png/.svg),
                                          Max 10240
                                        </small>
                                        <br />
                                        <span style={{ color: "red" }}>
                                          {this.state.errors["upload_silabus"]}
                                        </span>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label required">
                                        Jawaban A
                                      </label>
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan jawaban A"
                                        name="pertanyaan"
                                        value={this.state.fields["pertanyaan"]}
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["pertanyaan"]}
                                      </span>
                                    </div>
                                    <div className="col-lg-6">
                                      <div className="mb-7 fv-row">
                                        <label className="form-label">
                                          Gambar Pertanyaan (Optional)
                                        </label>
                                        <input
                                          type="file"
                                          className="form-control form-control-sm mb-2"
                                          name="upload_silabus"
                                          accept=".jpg/.jpeg/.png/.svg"
                                          onChange={this.handleChangeSilabus}
                                        />
                                        <small className="text-muted">
                                          Format File (.jpg/.jpeg/.png/.svg),
                                          Max 10240
                                        </small>
                                        <br />
                                        <span style={{ color: "red" }}>
                                          {this.state.errors["upload_silabus"]}
                                        </span>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label required">
                                        Jawaban B
                                      </label>
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan jawaban B"
                                        name="pertanyaan"
                                        value={this.state.fields["pertanyaan"]}
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["pertanyaan"]}
                                      </span>
                                    </div>
                                    <div className="col-lg-6">
                                      <div className="mb-7 fv-row">
                                        <label className="form-label">
                                          Gambar Pertanyaan (Optional)
                                        </label>
                                        <input
                                          type="file"
                                          className="form-control form-control-sm mb-2"
                                          name="upload_silabus"
                                          accept=".jpg/.jpeg/.png/.svg"
                                          onChange={this.handleChangeSilabus}
                                        />
                                        <small className="text-muted">
                                          Format File (.jpg/.jpeg/.png/.svg),
                                          Max 10240
                                        </small>
                                        <br />
                                        <span style={{ color: "red" }}>
                                          {this.state.errors["upload_silabus"]}
                                        </span>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label required">
                                        Jawaban C
                                      </label>
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan jawaban C"
                                        name="pertanyaan"
                                        value={this.state.fields["pertanyaan"]}
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["pertanyaan"]}
                                      </span>
                                    </div>
                                    <div className="col-lg-6">
                                      <div className="mb-7 fv-row">
                                        <label className="form-label">
                                          Gambar Pertanyaan (Optional)
                                        </label>
                                        <input
                                          type="file"
                                          className="form-control form-control-sm mb-2"
                                          name="upload_silabus"
                                          accept=".jpg/.jpeg/.png/.svg"
                                          onChange={this.handleChangeSilabus}
                                        />
                                        <small className="text-muted">
                                          Format File (.jpg/.jpeg/.png/.svg),
                                          Max 10240
                                        </small>
                                        <br />
                                        <span style={{ color: "red" }}>
                                          {this.state.errors["upload_silabus"]}
                                        </span>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label required">
                                        Jawaban D
                                      </label>
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan jawaban D"
                                        name="pertanyaan"
                                        value={this.state.fields["pertanyaan"]}
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["pertanyaan"]}
                                      </span>
                                    </div>
                                    <div className="col-lg-6">
                                      <div className="mb-7 fv-row">
                                        <label className="form-label">
                                          Gambar Pertanyaan (Optional)
                                        </label>
                                        <input
                                          type="file"
                                          className="form-control form-control-sm mb-2"
                                          name="upload_silabus"
                                          accept=".jpg/.jpeg/.png/.svg"
                                          onChange={this.handleChangeSilabus}
                                        />
                                        <small className="text-muted">
                                          Format File (.jpg/.jpeg/.png/.svg),
                                          Max 10240
                                        </small>
                                        <br />
                                        <span style={{ color: "red" }}>
                                          {this.state.errors["upload_silabus"]}
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div data-kt-stepper-element="content">
                              <div
                                id="kt_content_container"
                                className="container-xxl"
                              >
                                <div className="card-body">
                                  <div className="row">
                                    <div className="card-title">
                                      <div className="card mb-12 mb-xl-8">
                                        <h2 className="me-3 mr-2">
                                          Publish Soal
                                        </h2>
                                        <h1 className="me-3 mr-2">
                                          Test Tanggal 1
                                        </h1>
                                      </div>
                                    </div>
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label">
                                        Tanggal Pendaftaran : 18-04-2022 s.d.
                                        18-04-2022
                                      </label>
                                      <br />
                                      <label className="form-label">
                                        Tanggal Pelatihan : 19-04-2022 s.d.
                                        19-04-2022
                                      </label>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label required">
                                        Pelaksanaan Dari
                                      </label>
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan pertanyaan"
                                        name="pertanyaan"
                                        value={this.state.fields["pertanyaan"]}
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["pertanyaan"]}
                                      </span>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label required">
                                        Pelaksanaan Sampai
                                      </label>
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan pertanyaan"
                                        name="pertanyaan"
                                        value={this.state.fields["pertanyaan"]}
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["pertanyaan"]}
                                      </span>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label required">
                                        Jumlah Soal
                                      </label>
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan jumlah soal"
                                        name="pertanyaan"
                                        value={this.state.fields["pertanyaan"]}
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["pertanyaan"]}
                                      </span>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label required">
                                        Durasi Test
                                      </label>
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan durasi test"
                                        name="pertanyaan"
                                        value={this.state.fields["pertanyaan"]}
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["pertanyaan"]}
                                      </span>
                                    </div>
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label required">
                                        Passing Grade (Tanpa %)
                                      </label>
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan passing grade"
                                        name="pertanyaan"
                                        value={this.state.fields["pertanyaan"]}
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["pertanyaan"]}
                                      </span>
                                    </div>
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label required">
                                        Status
                                      </label>
                                      <select
                                        className="form-select form-select-sm selectpicker"
                                        data-control="select2"
                                        data-placeholder="Select an option"
                                        name="level_pelatihan"
                                      >
                                        <option>Publish</option>
                                        <option>Draft</option>
                                      </select>
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["pertanyaan"]}
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="text-center">
                              {/* <button onClick={this.handleClickBatal} className="btn btn-secondary btn-sm">Daftar Pelatihan</button> */}
                              <button
                                className="btn btn-secondary btn-sm me-3 mr-2"
                                data-kt-stepper-action="previous"
                              >
                                Sebelumnya
                              </button>
                              <button
                                type="submit"
                                className="btn btn-primary btn-sm"
                                data-kt-stepper-action="submit"
                              >
                                Simpan
                              </button>
                              <button
                                type="button"
                                className="btn btn-warning btn-sm"
                                data-kt-stepper-action="next"
                              >
                                Lanjutkan
                              </button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="modal fade" tabindex="-1" id="kt_modal_3">
          <div className="modal-dialog">
            <div className="modal-content position-absolute">
              <div className="modal-header">
                <h5 id="preview_title" className="modal-title">
                  {this.state.dataxpreviewtitle}
                </h5>
                <div
                  className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <span className="svg-icon svg-icon-2x"></span>
                </div>
              </div>
              <div className="modal-body">
                {this.state.dataxpreview.map((data) => (
                  <div dangerouslySetInnerHTML={{ __html: data.html }}></div>
                ))}
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-light"
                  data-bs-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
