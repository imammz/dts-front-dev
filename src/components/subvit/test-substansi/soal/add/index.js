import { Observer } from "mobx-react-lite";
import React from "react";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import Footer from "../../../../Footer";
import Header from "../../../../Header";
import SideNav from "../../../../SideNav";
import {
  AddEditComponent,
  astate,
} from "../../components/add-edit-soal-test-substansi";
import { withRouter } from "../../components/utils";
import state from "./state";

const Content = (props) => {
  const history = useNavigate();
  const { id = null } = useParams();

  React.useEffect(() => {
    state.fetchTipeSoal();
  }, []);

  const errorMessage = (state) => {
    let error = { answer: [] };

    if (!state.question) error.question = "Pertanyaan tidak boleh kosong";
    if (!state.question_type_id)
      error.question_type_id = "Tipe soal harus terpilih salah satu";
    if (state.status == null) error.status = "Status harus terpilih salah satu";
    if (!state.answer_key)
      error.answer_key = "Jawaban harus terpilih salah satu";
    if ((state.answer || []).length == 0)
      error.answer_num = "Pilihan jawaban tidak boleh kosong";

    (state.answer || []).forEach((a, idx) => {
      if (!a.option)
        error.answer[idx] = {
          ...(error.answer[idx] || {}),
          option: "Jawaban tidak boleh kosong",
        };
    });

    const { answer = [], ...rest } = error || {};

    if (Object.keys(rest).length > 0 || answer.length > 0) {
      Swal.fire({
        title: "Masih terdapat kesalahan isian",
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }

    return error;
  };

  return (
    <div>
      <div className="toolbar" id="kt_toolbar">
        <div
          id="kt_toolbar_container"
          className="container-fluid d-flex flex-stack my-2"
        >
          <div className="d-flex align-items-start my-2">
            <div>
              <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                <span className="me-3">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <path
                      opacity="0.3"
                      d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                      fill="#7239ea"
                    ></path>
                    <path
                      d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                      fill="#7239ea"
                    ></path>
                  </svg>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Subvit
                  <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                </span>
                Test Substansi
              </h1>
            </div>
          </div>

          <div className="d-flex align-items-end my-2">
            <div>
              <button
                onClick={() => props?.history && props?.history(-1)}
                className="btn btn-sm btn-light btn-active-light-primary"
              >
                <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                  <i className="fa fa-chevron-left"></i>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Kembali
                </span>
              </button>
            </div>
          </div>
        </div>
      </div>
      <div
        className="wrapper d-flex flex-column flex-row-fluid pt-0"
        id="kt_wrapper"
      >
        <div
          className="content d-flex flex-column flex-column-fluid pt-0"
          id="kt_content"
        >
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container-xxl">
              <div className="col-lg-12 mt-7">
                <div className="card border">
                  <div className="card-header">
                    <div className="card-title">
                      <h1
                        className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                        style={{ textTransform: "capitalize" }}
                      >
                        Tambah Soal
                      </h1>
                    </div>
                  </div>
                  <div className="card-body">
                    <Observer>
                      {() => {
                        return (
                          <AddEditComponent
                            tipeSoal={{
                              data: state.dataTipeSoal,
                              mapper: ({ value, label } = {}) => ({
                                value,
                                label,
                              }),
                              findById: (id) => {
                                const [t] = state.dataTipeSoal.filter(
                                  ({ value }) => value == id,
                                );
                                return t;
                              },
                            }}
                          />
                        );
                      }}
                    </Observer>

                    <div className="row mt-7">
                      <div className="col-lg-12 mb-7 fv-row text-center">
                        <button
                          onClick={() =>
                            (window.location.href = `/subvit/test-substansi/soal/list/${id}`)
                          }
                          className="btn btn-md btn-light mr-3 me-3"
                        >
                          Batal
                        </button>
                        <button
                          className="btn btn-md btn-primary"
                          onClick={() => {
                            astate.error = errorMessage(astate.question);
                            const { answer = [], ...rest } = astate.error || {};

                            if (
                              !(
                                Object.keys(rest).length > 0 ||
                                Object.keys(answer).length > 0
                              )
                            ) {
                              state
                                .addRows(id, [astate.question])
                                .then((success) => {
                                  if (success)
                                    window.location.href = `/subvit/test-substansi/soal/list/${id}`;
                                });
                            }
                          }}
                        >
                          <i className="fa fa-paper-plane ms-1"></i>Simpan
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const ListSoalTrivia = (props) => {
  return (
    <>
      <SideNav />
      <Header />
      <Content {...props} />
      <Footer />
    </>
  );
};

export default withRouter(ListSoalTrivia);
