import imageCompression from "browser-image-compression";
import { Observer } from "mobx-react-lite";
import React, { useRef } from "react";
import DataTable from "react-data-table-component";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import { read, utils } from "xlsx";
import Footer from "../../../../Footer";
import Header from "../../../../Header";
import SideNav from "../../../../SideNav";
import { defaultQuestion } from "../../components/add-edit-soal-test-substansi";
import {
  astate,
  ImportComponent,
} from "../../components/import-soal-test-substansi";
import { withRouter } from "../../components/utils";
import state from "./state";

const Content = (props) => {
  const history = useNavigate();
  const { id = null } = useParams();

  const btnDialogRef = useRef();
  const [errorTriviaQuestions, setErrorTriviaQuestions] = React.useState([]);

  React.useEffect(() => {
    state.fetchTipeSoal();
  }, []);

  const tipeSoal = {
    data: state.dataTipeSoal,
    mapper: ({ value, label } = {}) => ({ value, label }),
    findById: (id) => {
      const [t] = state.dataTipeSoal.filter(({ value }) => value == id);
      return t;
    },
  };

  const resizeFile = async (file) => {
    const options = {
      maxSizeMB: 0.2,
      maxWidthOrHeight: 300,
      useWebWorker: true,
    };

    return imageCompression(file, options);
  };

  const errorMessage = (state) => {
    if (!state.question) return "Pertanyaan tidak boleh kosong";
    if (!state.question_type_id) return "Tipe soal harus terpilih salah satu";
    if (state.status == null) return "Status harus terpilih salah satu";
    if (!state.answer_key) return "Kunci jawaban tidak boleh kosong";
    if (
      !(state.answer || [])
        .map((a) => (a.key || "").toLowerCase())
        .includes((state.answer_key || "").toLowerCase())
    )
      return "Kunci jawaban tidak ada dalam pilihan jawaban";
    if ((state.answer || []).length < 2)
      return "Pilihan jawaban minimal 2 opsi";

    (state.answer || []).forEach((a, idx) => {
      if (a?.option == null || a?.option == undefined)
        return "Jawaban tidak boleh kosong";
    });

    return null;
  };

  const doImport = async () => {
    try {
      const file = astate.fileTemplateXlsx;
      const images = (
        await Promise.all(
          (astate.fileTemplateImages || []).map(async (f) => {
            const cf = await resizeFile(f);
            return new Promise((resolve, reject) => {
              var reader = new FileReader();
              reader.onload = () => {
                resolve({ name: f.name, data: reader.result });
              };
              reader.readAsDataURL(cf);
            });
          }),
        )
      ).reduce((obj, { name, data }) => ({ ...obj, [name]: data }), {});

      const templateBin = await new Promise((resolve, reject) => {
        var reader = new FileReader();
        reader.onload = () => {
          resolve(reader.result);
        };
        reader.readAsArrayBuffer(file);
      });

      var result = new Uint8Array(templateBin);
      var wb = read(result, { type: "array" });

      let triviaQuestions = [];
      let errorTriviaQuestions = [];

      wb.SheetNames.splice(0, 1).forEach((s) => {
        const [header = [], ...data] = utils.sheet_to_json(wb.Sheets[s], {
          header: 1,
        });

        if (header[3] != "ID Tipe Soal") {
          throw new Error(
            "Template Tidak Sesuai, Pastikan Menggunakan Template Test Substansi",
          );
        }

        // Validate image must be exists in uploaded images
        let notExistingImages = [];
        data.forEach((row) => {
          header.forEach((cname, cidx) => {
            if (cname.toLowerCase().includes("gambar")) {
              const img = row[cidx];
              if (img) {
                if (!Object.keys(images).includes(img)) {
                  notExistingImages = [...notExistingImages, img];
                }
              }
            }
          });
        });

        if (notExistingImages.length > 0)
          throw new Error(
            `Gambar berikut tidak terdapat di gambar yang di-upload: ${notExistingImages.join(
              ", ",
            )}`,
          );

        data.forEach((row, ridx) => {
          const [
            q,
            qimg,
            skeys = "",
            id_tipe_soal = null,
            status = null,
            ...answs
          ] = header.map((cname, idx) => row[idx]);
          const [key] = skeys
            .split(",")
            .map((k) => k.trim())
            .map((k) => k.toLowerCase());
          const { value: idts } = tipeSoal.findById(id_tipe_soal) || {};

          if (q) {
            const tq = { ...defaultQuestion };
            tq.question = q;
            tq.question_image = images[qimg];
            tq.question_type_id = idts;
            tq.answer_key = key;
            tq.status = status;

            let tas = [];
            const letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];
            for (let aidx = 0; aidx < answs.length; aidx += 2) {
              if (answs[aidx] == null || answs[aidx] == undefined) continue;

              let ta = {};
              ta.option = answs[aidx];
              ta.key = aidx / 3 < letters.length ? letters[aidx / 2] : null;
              ta.image =
                answs[aidx + 1] in images ? images[answs[aidx + 1]] : null;
              tas = [...tas, ta];
            }

            tq.answer = tas;

            const error = errorMessage(tq);
            // if (error) throw new Error(error);
            if (error)
              errorTriviaQuestions = [
                ...errorTriviaQuestions,
                { ...tq, rowNum: ridx + 2, error },
              ];

            triviaQuestions = [...triviaQuestions, tq];
          }
        });
      });

      if (errorTriviaQuestions.length) {
        setErrorTriviaQuestions(errorTriviaQuestions);
        btnDialogRef.current?.click();

        Swal.fire({
          title: "Soal Gagal Diimport",
          text: "Silahkan perbaiki file Excel berdasarkan kesalahan pada dialog yang muncul, kemudian lakukan import kembali!",
          icon: "warning",
          confirmButtonText: "Ok",
        });

        return false;
      } else {
        await state.addRows(id, triviaQuestions);

        Swal.fire({
          title: "Soal Berhasil Diimport",
          icon: "success",
          confirmButtonText: "Ok",
        });

        return true;
      }

      // await state.addRows(id, triviaQuestions);

      // // await Swal.fire({
      // //   title: "Soal Berhasil Diimport",
      // //   icon: "success",
      // //   confirmButtonText: "Ok"
      // // });

      // return true;
    } catch (err) {
      await Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });

      return false;
    }
  };

  const renderErrorDialog = () => {
    return (
      <>
        <button
          className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2 d-none"
          data-bs-toggle="modal"
          data-bs-target="#modal_error"
          ref={btnDialogRef}
        >
          <i className="bi bi-sliders"></i>
          <span className="d-md-inline d-lg-inline d-xl-inline d-none">
            Show Error Dialog
          </span>
        </button>
        <div className="modal fade" tabindex="-1" id="modal_error">
          <div className="modal-dialog modal-lg">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">
                  <span className="svg-icon svg-icon-5 me-1">
                    <i className="bi bi-sliders text-black"></i>
                  </span>
                  Error Import Tes Substansi
                </h5>
                <div
                  className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <span className="svg-icon svg-icon-2x">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <rect
                        opacity="0.5"
                        x="6"
                        y="17.3137"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(-45 6 17.3137)"
                        fill="currentColor"
                      />
                      <rect
                        x="7.41422"
                        y="6"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(45 7.41422 6)"
                        fill="currentColor"
                      />
                    </svg>
                  </span>
                </div>
              </div>
              <div className="modal-body">
                <DataTable
                  columns={[
                    {
                      name: "Baris",
                      sortable: false,
                      center: true,
                      width: "70px",
                      grow: 1,
                      cell: (row, index) => row?.rowNum,
                    },
                    {
                      name: "Soal",
                      sortable: false,
                      center: true,
                      width: "200px",
                      grow: 1,
                      cell: (row, index) => row?.question,
                    },
                    {
                      name: "Kesalahan",
                      sortable: false,
                      center: true,
                      width: "300px",
                      grow: 1,
                      cell: (row, index) => row?.error,
                    },
                  ]}
                  data={errorTriviaQuestions}
                  highlightOnHover
                  pointerOnHover
                  pagination
                  customStyles={{
                    headCells: {
                      style: {
                        background: "rgb(243, 246, 249)",
                      },
                    },
                  }}
                  noDataComponent={<div className="mt-5">Tidak Ada Data</div>}
                  persistTableHead={true}
                />
              </div>
              <div className="modal-footer">
                <div className="d-flex justify-content-between">
                  <a className="btn btn-sm btn-primary" data-bs-dismiss="modal">
                    Tutup
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  };

  return (
    <div>
      <div className="toolbar" id="kt_toolbar">
        <div
          id="kt_toolbar_container"
          className="container-fluid d-flex flex-stack my-2"
        >
          <div className="d-flex align-items-start my-2">
            <div>
              <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                <span className="me-3">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <path
                      opacity="0.3"
                      d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                      fill="#7239ea"
                    ></path>
                    <path
                      d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                      fill="#7239ea"
                    ></path>
                  </svg>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Subvit
                  <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                </span>
                Test Substansi
              </h1>
            </div>
          </div>

          <div className="d-flex align-items-end my-2">
            <div>
              <button
                onClick={() => props?.history && props?.history(-1)}
                className="btn btn-sm btn-light btn-active-light-primary"
              >
                <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                  <i className="fa fa-chevron-left"></i>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Kembali
                </span>
              </button>
            </div>
          </div>
        </div>
      </div>

      <div
        className="wrapper d-flex flex-column flex-row-fluid pt-0"
        id="kt_wrapper"
      >
        <div
          className="content d-flex flex-column flex-column-fluid pt-0"
          id="kt_content"
        >
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container-xxl">
              <div className="col-lg-12 mt-7">
                <div className="card border">
                  <div className="card-header">
                    <div className="card-title">
                      <h1
                        className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                        style={{ textTransform: "capitalize" }}
                      >
                        Import Soal
                      </h1>
                    </div>
                  </div>
                  <div className="card-body">
                    <Observer>
                      {() => {
                        return (
                          <ImportComponent
                            tipeSoal={{
                              data: state.dataTipeSoal,
                              mapper: ({ value, label } = {}) => ({
                                value,
                                label,
                              }),
                              findById: (id) => {
                                const [t] = state.dataTipeSoal.filter(
                                  ({ value }) => value == id,
                                );
                                return t;
                              },
                            }}
                          />
                        );
                      }}
                    </Observer>
                    <div className="row">
                      <div className="col-lg-12 mb-7 fv-row text-center">
                        <button
                          onClick={() =>
                            (window.location.href = `/subvit/test-substansi/soal/list/${id}`)
                          }
                          className="btn btn-md btn-light me-3"
                        >
                          Batal
                        </button>
                        <button
                          className="btn btn-md btn-primary"
                          onClick={() => {
                            doImport().then((noError) => {
                              // if (noError) history(`/subvit/test-substansi/soal/import/${id}`);
                              if (noError) window.location.reload();
                            });

                            // astate.error = errorMessage(astate.question);
                            // const { answer = [], ...rest } = astate.error || {};

                            // if (!(Object.keys(rest).length > 0 || Object.keys(answer).length > 0)) {
                            //   state.addRows(id, [astate.question]).then((success) => {
                            //     if (success) history(`/subvit/test-substansi/soal/list/${id}`);
                            //   })
                            // }
                            // history(`/subvit/test-substansi/soal/list/${id}`);
                          }}
                        >
                          <i className="fa fa-paper-plane ms-1"></i>Import
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {renderErrorDialog()}
    </div>
  );
};

const ListSoalTrivia = (props) => {
  return (
    <>
      <SideNav />
      <Header />
      <Content {...props} />
      <Footer />
    </>
  );
};

export default withRouter(ListSoalTrivia);
