import { makeAutoObservable } from "mobx";
import Cookies from "js-cookie";
import axios from "axios";
import Swal from "sweetalert2";

class State {
  axiosConfig = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  dataTipeSoal = [];
  fetchingTipeSoal = false;

  constructor() {
    makeAutoObservable(this);
  }

  *fetchTipeSoal() {
    this.fetchingTipeSoal = true;

    // Swal.fire({
    //   title: 'Mohon Tunggu!',
    //   icon: 'info',
    //   allowOutsideClick: false,
    //   didOpen: () => {
    //     Swal.showLoading();
    //   }
    // });

    const dataBody = {
      start: 0,
      length: 200,
      status: 1,
      sort: "id",
      sort_val: "DESC",
    };
    yield axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/tessubstansi/list-tipesoal",
        dataBody,
        this.axiosConfig,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = res.data;
        const { Data = [], Status = false, Message = "" } = result || {};

        if (!Status) throw new Error(Message);
        this.dataTipeSoal = Data.map(({ id, name }) => ({
          value: id,
          label: name,
        }));
      })
      .catch((err) => {
        Swal.fire({
          title: err,
          icon: "warning",
          confirmButtonText: "Ok",
        }).then((result) => {
          if (result.isConfirmed) {
          }
        });
      });

    // Swal.close();

    this.fetchingTipeSoal = false;
  }

  *addRows(id_substansi, rows) {
    try {
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info",
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });

      rows = rows.map(
        ({
          id,
          question,
          question_image,
          question_type_id,
          answer_key,
          answer,
          status,
        }) => {
          return {
            id_substansi: id_substansi,
            id_user: Cookies.get("user_id"),
            // substansi_id: triviaId,
            pertanyaan: question,
            gambar_pertanyaan: question_image,
            id_tipe_soal: question_type_id,
            kunci_jawaban: answer_key,
            jawaban: answer,
            status: status,
          };
        },
      );
      const res = yield axios.post(
        process.env.REACT_APP_BASE_API_URI +
          "/tessubstansi/tambah-soal-substansibulk",
        rows,
        this.axiosConfig,
      );

      const { data } = res || {};
      const { result } = data || {};
      const { Status = false, Message = "" } = result || {};
      // console.log(Status, Message)

      // Swal.close();

      if (Status) {
        const { isConfirmed } = yield Swal.fire({
          title: Message,
          icon: "success",
          confirmButtonText: "Ok",
        });

        return true;
      } else {
        yield Swal.fire({
          title: Message,
          icon: "warning",
          confirmButtonText: "Ok",
        });
      }
    } catch (err) {
      const { data } = err.response || {};
      const { result } = data || {};
      const { Status = false, Message = "" } = result || {};

      Swal.close();

      if (!Status) {
        yield Swal.fire({
          title: Message,
          icon: "warning",
          confirmButtonText: "Ok",
        });
      }
    }
  }
}

const state = new State();
export default state;
