import Cookies from "js-cookie";
import "line-awesome/dist/line-awesome/css/line-awesome.min.css";
import React from "react";
import Swal from "sweetalert2";
import Footer from "../../../../Footer";
import Header from "../../../../Header";
import SideNav from "../../../../SideNav";
import EntrySoalScreen from "../../add/EntrySoalScreen";
import {
  fetchTestSubstansiById,
  fetchTipeSoal,
  updateSoalTestSubstansi,
} from "../../components/actions";
import { withRouter } from "../../components/utils";

class Content extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      dataTipeSoal: [],
      header: null,
      questions: [],
      sortField: "id",
      sortDir: "asc",
    };
  }

  init = async () => {
    if (Cookies.get("token") == null) {
      Swal.fire({
        title: "Unauthenticated.",
        text: "Silahkan login ulang",
        icon: "warning",
        confirmButtonText: "Ok",
      }).then((result) => {
        if (result.isConfirmed) {
          window.location = "/";
        }
      });
    }

    try {
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });

      const { params } = this.props || {};
      const { testid } = params || {};

      let [dataTipeSoal, { header, soals }] = await Promise.all([
        fetchTipeSoal(1, "id", "DESC"),
        fetchTestSubstansiById(testid),
      ]);

      soals.sort((a, b) => {
        if (this.state.sortDir == "asc")
          return `${a[this.state.sortField]}`.toLowerCase() >
            `${b[this.state.sortField]}`.toLowerCase()
            ? 1
            : -1;
        else
          return `${a[this.state.sortField]}`.toLowerCase() >
            `${b[this.state.sortField]}`.toLowerCase()
            ? -1
            : 1;
      });

      this.setState({
        dataTipeSoal,
        header: header,
        questions: soals,
      });

      Swal.close();
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  save = async (q) => {
    try {
      const { history } = this.props || {};
      const { params } = this.props || {};
      const { testid } = params || {};

      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });

      await updateSoalTestSubstansi([q]);

      Swal.close();

      await Swal.fire({
        title: "Soal test substansi berhasil diupdate",
        icon: "success",
        confirmButtonText: "Ok",
      });

      window.location.href = `/subvit/test-substansi/soal/list/${testid}`;
    } catch (err) {
      await Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  componentDidMount() {
    this.init();
  }

  render() {
    const { params } = this.props || {};
    const { testid, id } = params || {};

    const { header, pelatihan = [] } = this.state || {};
    const {
      nama_akademi,
      nama_tema,
      nama_pelatihan,
      nama_substansi,
      pelatihan_start,
      pelatihan_end,
      tgl_pelaksanaan,
      tgl_selesai_pelaksanaan,
      passing_grade,
      status_substansi,
      status_pelaksanaan,
    } = header || {};

    let title = "";
    if (nama_substansi) title = `${nama_substansi}`;
    else if (nama_pelatihan) title = `Pelatihan ${nama_pelatihan}`;
    else if (nama_tema) title = `Tema ${nama_tema}`;
    else if (nama_akademi) title = `Akademi ${nama_akademi}`;

    let selectedEditSoalIdx = null;
    this.state.questions.forEach(({ id: qidx }, idx) => {
      if (qidx == id) selectedEditSoalIdx = idx;
    });

    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <path
                        opacity="0.3"
                        d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                        fill="#7239ea"
                      ></path>
                      <path
                        d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                        fill="#7239ea"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Subvit
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Test Substansi
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={() =>
                    this?.props?.history && this?.props?.history(-1)
                  }
                  className="btn btn-sm btn-light btn-active-light-primary"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header mt-5 pt-10 pb-8">
                      <div className="col-12">
                        <h1
                          className="align-items-center text-dark fw-bolder my-1 fs-4"
                          style={{ textTransform: "capitalize" }}
                        >
                          Edit Soal {title || "-"}
                        </h1>
                        <p className="text-dark fs-7 mb-0">
                          {nama_akademi || "-"}{" "}
                          <span className="text-muted fs-7 mb-0">
                            - {nama_tema || "-"}
                          </span>
                        </p>
                      </div>
                    </div>
                    <div className="card-body">
                      <EntrySoalScreen
                        data={{
                          dataTipeSoal: this.state.dataTipeSoal,
                          selectedEditSoalIdx,
                          questions: this.state.questions,
                          updateQuestion: (idx, q) => this.save(q),
                          showBackButton: true,
                          onBackButtonClick: () => {
                            window.location.href = `/subvit/test-substansi/soal/list/${testid}`;
                          },
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const AddTrivia = (props) => {
  return (
    <>
      <SideNav />
      <Header />
      <Content {...props} />
      <Footer />
    </>
  );
};

export default withRouter(AddTrivia);
