import Cookies from "js-cookie";
import { Observer } from "mobx-react-lite";
import moment from "moment";
import React from "react";
import DataTable from "react-data-table-component";
import Swal from "sweetalert2";
import Footer from "../../../../Footer";
import Header from "../../../../Header";
import SideNav from "../../../../SideNav";
import {
  fetchTestSubstansiById,
  fetchTestSubstansiByIdV2,
  fetchTipeSoal,
  removeSoalsTestSubstansi,
  removeTestSubstansiById,
  reportTestSubstansi,
} from "../../components/actions";
import { PreviewModal } from "../../components/preview-test-substansi";
import { withRouter } from "../../components/utils";

moment.locale("id");

class Content extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      dataTipeSoal: [],
      header: null,
      questions: [],
      keyword: "",
      searchQuestions: [],
      selectedPreviewIdx: null,
      selectedPreviewRows: [],
      selectedRowIds: [],
      loading: false,
      sortField: "id",
      sortDir: "asc",
      dataPeserta: [],
    };

    this.prevBtnRef = React.createRef();
    this.nextBtnRef = React.createRef();
  }

  init = async () => {
    if (Cookies.get("token") == null) {
      Swal.fire({
        title: "Unauthenticated.",
        text: "Silahkan login ulang",
        icon: "warning",
        confirmButtonText: "Ok",
      }).then((result) => {
        if (result.isConfirmed) {
          window.location = "/";
        }
      });
    }

    try {
      this.setState({ loading: true });
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });

      const { params } = this.props || {};
      const { id } = params || {};

      // const { header, soals } = await fetchTestSubstansiById(id);

      const [dataTipeSoal, { header, soals }, { pelatihan }, { Data: data }] =
        await Promise.all([
          fetchTipeSoal(0),
          fetchTestSubstansiById(id),
          fetchTestSubstansiByIdV2(id),
          new Promise((resolve) => {
            reportTestSubstansi(id, 0, 1000, "nama_akademi", "asc", "")
              .then(resolve)
              .catch(() => resolve([]));
          }),
        ]);

      this.setState({
        dataTipeSoal,
        header: header,
        questions: soals,
        pelatihan,
        dataPeserta: data,
      });

      Swal.close();
      this.setState({ loading: false });
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  remove = async (id) => {
    const { isConfirmed = false } = await Swal.fire({
      title: "Apakah anda yakin ?",
      text: "Data ini tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, hapus!",
      cancelButtonText: "Tidak",
    });

    if (isConfirmed) {
      try {
        const Message = await removeTestSubstansiById(id);

        Swal.close();

        await Swal.fire({
          title: "Data berhasil dihapus",
          icon: "success",
          confirmButtonText: "Ok",
        });

        window.location.href = "/subvit/test-substansi";
      } catch (err) {
        await Swal.fire({
          title: err,
          icon: "warning",
          confirmButtonText: "Ok",
        });
      }
    }
  };

  removeSoals = async (ids) => {
    try {
      if (ids.length == 0) throw new Error("Belum ada data terpilih");

      const { isConfirmed = false } = await Swal.fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      });

      if (isConfirmed) {
        this.setState({
          loading: true,
          questions: [],
        });

        Swal.fire({
          title: "Mohon Tunggu!",
          icon: "info", // add html attribute if you want or remove
          allowOutsideClick: false,
          didOpen: () => {
            Swal.showLoading();
          },
        });

        await removeSoalsTestSubstansi(ids);

        Swal.close();

        await Swal.fire({
          title: "Data berhasil dihapus",
          icon: "success",
          confirmButtonText: "Ok",
        });

        await this.init();
      }
    } catch (err) {
      await Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }

    this.setState({ loading: false });
  };

  search = (keyword) => {
    let searchQuestions = this.state.questions
      .filter(({ question }) =>
        question.toLowerCase().includes(keyword.toLowerCase()),
      )
      .sort((a, b) => {
        if (a?.question?.toLowerCase() > b?.question?.toLowerCase()) return 1;
        return -1;
      });

    if (searchQuestions.length == 0) {
      Swal.fire({
        title: "Data tidak ditemukan",
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }

    this.setState({
      keyword,
      searchQuestions,
    });
  };

  componentDidMount() {
    this.init();
  }

  render() {
    const { header, pelatihan = [] } = this.state || {};
    const {
      nama_akademi,
      nama_tema,
      nama_pelatihan,
      nama_substansi,
      pelatihan_start,
      pelatihan_end,
      subtansi_mulai: tgl_pelaksanaan,
      subtansi_selesai: tgl_selesai_pelaksanaan,
      passing_grade,
      status_substansi,
      status_pelaksanaan,
    } = header || {};
    const { params } = this.props || {};
    const { id } = params || {};

    let title = "";
    if (nama_substansi) title = `${nama_substansi}`;
    else if (nama_pelatihan) title = `Pelatihan ${nama_pelatihan}`;
    else if (nama_tema) title = `Tema ${nama_tema}`;
    else if (nama_akademi) title = `Akademi ${nama_akademi}`;

    let gDataAkademi = {};
    let gDataTema = {};
    let dataPelatihan = [];
    pelatihan.forEach((p) => {
      const {
        akademi,
        akademi_id,
        pelatihan,
        pelatihan_id,
        pelatihan_mulai,
        pelatihan_selesai,
        status_akademi,
        status_pelatihan,
        status_publish,
        status_tema,
        tema,
        tema_id,
      } = p || {};

      if (!Object.keys(gDataAkademi).includes(akademi_id))
        gDataAkademi[akademi_id] = [];
      gDataAkademi[akademi_id] = [
        ...gDataAkademi[akademi_id],
        {
          akademi_id,
          akademi,
          status_akademi,
        },
      ];

      if (!Object.keys(gDataTema).includes(tema_id)) gDataTema[tema_id] = [];
      gDataTema[tema_id] = [
        ...gDataTema[tema_id],
        {
          tema_id,
          tema,
          status_tema,
        },
      ];

      dataPelatihan = [
        ...dataPelatihan,
        {
          akademi_id,
          akademi,
          pelatihan_id,
          label: pelatihan,
          status: status_publish,
          status_pelatihan,
          pelatihan_mulai,
          pelatihan_selesai,
        },
      ];
    });

    let dataAkademi = [];
    Object.keys(gDataAkademi).forEach((id) => {
      const [a] = gDataAkademi[id];
      const { akademi_id, akademi, status_akademi } = a || {};
      dataAkademi = [
        ...dataAkademi,
        {
          akademi_id,
          label: akademi,
          status: status_akademi,
          jumlah: gDataAkademi[id].length,
        },
      ];
    });

    let dataTema = [];
    Object.keys(gDataTema).forEach((id) => {
      const [a] = gDataTema[id];
      const { tema_id, tema, status_tema } = a || {};
      dataTema = [
        ...dataTema,
        {
          tema_id,
          label: tema,
          status: status_tema,
          jumlah: gDataTema[id].length,
        },
      ];
    });

    let badgeStatus = "";
    let labelStatus = "";
    if (status_substansi == 1) {
      badgeStatus = "success";
      labelStatus = "Publish";
    } else if (status_substansi == 2) {
      badgeStatus = "danger";
      labelStatus = "Batal";
    } else if (status_substansi == 0) {
      badgeStatus = "warning";
      labelStatus = "Draft";
    }

    let data = this.state.keyword
      ? this.state.searchQuestions
      : this.state.questions;
    if (this.state.sortField && this.state.sortDir) {
      data.sort((a, b) => {
        if (this.state.sortDir == "asc")
          return `${a[this.state.sortField]}`.toLowerCase() >
            `${b[this.state.sortField]}`.toLowerCase()
            ? 1
            : -1;
        else
          return `${a[this.state.sortField]}`.toLowerCase() >
            `${b[this.state.sortField]}`.toLowerCase()
            ? -1
            : 1;
      });
    }

    data = data.map((s, idx) => ({ idx, ...s }));

    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <path
                        opacity="0.3"
                        d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                        fill="#7239ea"
                      ></path>
                      <path
                        d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                        fill="#7239ea"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Subvit
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Test Substansi
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={() =>
                    this?.props?.history && this?.props?.history(-1)
                  }
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>

                <a
                  href={"/subvit/test-substansi/edit/" + id}
                  className={`btn btn-warning fw-bolder btn-sm me-2 ${
                    status_substansi != 0 ? "disabled" : ""
                  }`}
                >
                  <i className="bi bi-gear-fill"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Edit
                  </span>
                </a>

                <a
                  href="#"
                  title="Hapus"
                  className={`btn btn-sm btn-danger btn-active-light-info ${
                    status_substansi != 0 ? "disabled" : ""
                  }`}
                  onClick={(e) => this.remove(id)}
                >
                  <i className="bi bi-trash-fill text-white"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div
                    className="stepper stepper-links mb-n3"
                    id="kt_subvit_trivia_list_soal"
                  >
                    <div className="col-lg-12 mt-7">
                      <div className="card border">
                        <div className="card-body mt-5 pt-10 pb-8">
                          <div className="col-12">
                            <h1
                              className="align-items-center text-dark fw-bolder my-1 fs-4"
                              style={{ textTransform: "capitalize" }}
                            >
                              {title || "-"}
                            </h1>
                            <p className="text-dark fs-7 mb-0">
                              {nama_akademi || "-"}{" "}
                              <span className="text-muted fw-semibold fs-7 mb-0">
                                - {nama_tema || "-"}
                              </span>
                            </p>
                          </div>
                        </div>
                        <div className="card-header">
                          <div className="card-title">
                            <div className="stepper-nav flex-wrap mb-n4">
                              <div
                                className="stepper-item ms-0 my-2 current"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label ms-0">
                                    <h3 className="stepper-title fs-6">
                                      Bank Soal
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Targeting
                                    </h3>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="card-body ">
                          <div>
                            <div
                              className="flex-column current"
                              data-kt-stepper-element="content"
                            >
                              <div className="row mt-7">
                                <div className="col-lg-6 mb-7">
                                  <label className="form-label">
                                    Tgl. Mulai Test Substansi
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {tgl_pelaksanaan
                                        ? moment(tgl_pelaksanaan).format(
                                            "DD MMM YYYY",
                                          )
                                        : "-"}
                                    </b>
                                  </div>
                                </div>
                                <div className="col-lg-6 mb-7">
                                  <label className="form-label">
                                    Tgl. Akhir Test Substansi
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {tgl_selesai_pelaksanaan
                                        ? moment(
                                            tgl_selesai_pelaksanaan,
                                          ).format("DD MMM YYYY")
                                        : "-"}
                                    </b>
                                  </div>
                                </div>
                                <div className="col-lg-6 mb-7">
                                  <label className="form-label">
                                    Passing Grade
                                  </label>
                                  <div className="d-flex">
                                    <b>{passing_grade || "-"}</b>
                                  </div>
                                </div>
                                <div className="col-lg-6 mb-7">
                                  <label className="form-label">
                                    Status Test Substansi
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      <span
                                        className={
                                          "badge badge-light-" +
                                          badgeStatus +
                                          " fs-7 m-1"
                                        }
                                      >
                                        {labelStatus}
                                      </span>
                                    </b>
                                  </div>
                                </div>
                                {/*{status_substansi == 1 && (
                                  <div className="col-lg-6 mb-7 s">
                                    <label className="form-label">
                                      Status Pelaksanaan
                                    </label>
                                    <div className="d-flex">
                                      <b>
                                        <span
                                          className={
                                            "badge badge-light-" +
                                            (status_pelaksanaan == "Selesai"
                                              ? "success"
                                              : "danger") +
                                            " fs-7 m-1"
                                          }
                                        >
                                          {status_pelaksanaan}
                                        </span>
                                      </b>
                                    </div>
                                  </div>
                                )}*/}
                              </div>
                              <div>
                                <div className="mt-5 border-top mx-0 my-10"></div>
                              </div>

                              <div className="card-header m-0 p-0">
                                <div className="card-title">
                                  <span className="svg-icon svg-icon-1 position-absolute ms-6">
                                    <svg
                                      width="24"
                                      height="24"
                                      viewBox="0 0 24 24"
                                      fill="none"
                                      xmlns="http://www.w3.org/2000/svg"
                                      className="mh-50px"
                                    >
                                      <rect
                                        opacity="0.5"
                                        x="17.0365"
                                        y="15.1223"
                                        width="8.15546"
                                        height="2"
                                        rx="1"
                                        transform="rotate(45 17.0365 15.1223)"
                                        fill="currentColor"
                                      ></rect>
                                      <path
                                        d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                        fill="currentColor"
                                      ></path>
                                    </svg>
                                  </span>
                                  <input
                                    type="text"
                                    data-kt-user-table-filter="search"
                                    className="form-control form-control-sm form-control-solid w-250px ps-14"
                                    placeholder="Cari Soal"
                                    onKeyDown={(e) => {
                                      const keyword = e.target.value;
                                      if (e.key == "Enter") {
                                        this.search(keyword);
                                        // state.setKeyword(keyword);
                                        e.preventDefault();
                                      }
                                    }}
                                    onChange={(e) => {
                                      const keyword = e.target.value;
                                      if (!keyword) {
                                        this.search(keyword);
                                        // state.setKeyword(keyword);
                                        e.preventDefault();
                                      }
                                    }}
                                  />
                                </div>
                                <div className="card-toolbar">
                                  <div className="d-flex align-items-center position-relative my-1 me-2">
                                    <a
                                      href="#"
                                      className="btn btn-light fw-bolder btn-sm ms-2"
                                      data-kt-menu-trigger="click"
                                      data-kt-menu-placement="bottom-end"
                                      data-kt-menu-flip="top-end"
                                    >
                                      <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                        Kelola Soal
                                      </span>
                                      <i className="bi bi-chevron-down ms-1"></i>
                                    </a>
                                    <div
                                      className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-7 w-auto "
                                      data-kt-menu="true"
                                    >
                                      <div>
                                        <div className="menu-item">
                                          <a
                                            href="#"
                                            data-bs-toggle="modal"
                                            data-bs-target="#preview_soal"
                                            onClick={() => {
                                              this.setState({
                                                selectedPreviewIdx: null,
                                                selectedPreviewRows:
                                                  this.state.questions,
                                              });
                                            }}
                                            className="menu-link px-5"
                                          >
                                            <i className="bi bi-eye text-dark text-hover-primary me-1"></i>
                                            Preview
                                          </a>
                                        </div>
                                        <div className={`menu-item`}>
                                          <a
                                            href={`/subvit/test-substansi/soal/add/${id}`}
                                            className={`menu-link px-5 ${
                                              status_substansi != 0
                                                ? "disabled pe-none"
                                                : ""
                                            }`}
                                          >
                                            <i className="bi bi-plus-circle text-dark text-hover-primary me-1"></i>
                                            Tambah Soal
                                          </a>
                                        </div>
                                        <div className="menu-item">
                                          <a
                                            href={`/subvit/test-substansi/soal/import/${id}`}
                                            className={`menu-link px-5 ${
                                              status_substansi != 0
                                                ? "disabled pe-none"
                                                : ""
                                            }`}
                                          >
                                            <i className="bi bi-cloud-download text-dark text-hover-primary me-1"></i>
                                            Import Soal
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                                    <Observer>
                                      {() => {
                                        return (
                                          <a
                                            href="#"
                                            onClick={async () => {
                                              if (
                                                this.state.selectedRowIds
                                                  .length ==
                                                this.state.questions.length
                                              ) {
                                                await Swal.fire({
                                                  title:
                                                    "Harus terdapat minimal 1 soal tersisa",
                                                  icon: "warning",
                                                  confirmButtonText: "Ok",
                                                });

                                                return;
                                              }

                                              this.removeSoals(
                                                this.state.selectedRowIds,
                                              );
                                            }}
                                            className={`btn btn-danger btn-sm ms-2 ${
                                              this.state.selectedRowIds
                                                .length == 0
                                                ? "disabled"
                                                : ""
                                            }`}
                                          >
                                            <i className="las la-trash"></i>
                                            <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                              Hapus Soal
                                            </span>
                                          </a>
                                        );
                                      }}
                                    </Observer>
                                  </div>
                                </div>
                              </div>
                              <DataTable
                                columns={[
                                  {
                                    name: "No",
                                    sortable: false,
                                    center: true,
                                    width: "70px",
                                    grow: 1,
                                    allowOverflow: true,
                                    cell: ({ idx }, index) => idx + 1,
                                  },
                                  {
                                    name: "Soal",
                                    className: "min-w-300px mw-300px",
                                    grow: 6,
                                    sortable: true,
                                    sortField: "question",
                                    wrap: true,
                                    allowOverflow: false,
                                    selector: ({ question }) => (
                                      <>
                                        <label className="d-flex flex-stack mb- mt-1">
                                          <span className="d-flex align-items-center me-2">
                                            <span className="d-flex flex-column">
                                              <h6 className="fw-bolder fs-7 mb-0">
                                                {question}
                                              </h6>
                                            </span>
                                          </span>
                                        </label>
                                      </>
                                    ),
                                  },
                                  {
                                    className: "min-w-200px mw-200px",
                                    sortType: "basic",
                                    sortable: true,
                                    name: "Tipe Soal",
                                    grow: 3,
                                    sortField: "question_type_id",
                                    selector: ({ question_type_id }) => {
                                      const [ts] =
                                        this.state.dataTipeSoal.filter(
                                          ({ value }) =>
                                            value == question_type_id,
                                        );
                                      const { label } = ts || {};
                                      return <span>{label || "-"}</span>;
                                    },
                                  },
                                  {
                                    name: "Status",
                                    className: "min-w-200px mw-200px",
                                    sortable: true,
                                    sortField: "status",
                                    center: true,
                                    selector: ({ status }) => (
                                      <span
                                        className={
                                          "badge badge-light-" +
                                          (status == 1 ? "success" : "danger") +
                                          " fs-7 m-1"
                                        }
                                      >
                                        {status == 1 ? "Publish" : "Draft"}
                                      </span>
                                    ),
                                  },
                                  {
                                    name: "Aksi",
                                    center: true,
                                    width: "180px",
                                    grow: 3,
                                    cell: (row, idx) => (
                                      <div>
                                        <a
                                          href="#"
                                          data-bs-toggle="modal"
                                          data-bs-target="#preview_soal"
                                          onClick={() => {
                                            this.setState({
                                              selectedPreviewIdx: row?.idx || 0,
                                              selectedPreviewRows: [row],
                                            });
                                          }}
                                          title="Preview Soal"
                                          className="btn btn-icon btn-bg-primary btn-sm me-1"
                                        >
                                          <i className="bi bi-eye-fill text-white"></i>
                                        </a>
                                        <a
                                          href={`/subvit/test-substansi/soal/edit/${id}/${row.id}`}
                                          title="Edit"
                                          className={`btn btn-icon btn-bg-warning btn-sm me-1 ${
                                            status_substansi != 0
                                              ? "disabled"
                                              : ""
                                          }`}
                                        >
                                          <i className="bi bi-gear-fill text-white"></i>
                                        </a>
                                        <a
                                          href="#"
                                          onClick={async () =>
                                            this.removeSoals([row.id])
                                          }
                                          title="Hapus"
                                          className={`btn btn-icon btn-bg-danger btn-sm me-1 ${
                                            status_substansi != 0
                                              ? "disabled"
                                              : ""
                                          }`}
                                        >
                                          <i className="bi bi-trash-fill text-white"></i>
                                        </a>
                                      </div>
                                    ),
                                  },
                                ]}
                                data={data}
                                highlightOnHover
                                pointerOnHover
                                onSort={({ sortField }, sortDir) =>
                                  this.setState({ sortField, sortDir })
                                }
                                pagination
                                paginationTotalRows={
                                  (this.state.keyword
                                    ? this.state.searchQuestions
                                    : this.state.questions
                                  ).length
                                }
                                selectableRows={status_substansi == 0}
                                selectableRowSelected={({ id = null } = {}) =>
                                  this.state.selectedRowIds.includes(id)
                                }
                                onSelectedRowsChange={({ selectedRows }) => {
                                  // const _selectedRowIds = selectedRows.map(({ id }) => id);
                                  // if (JSON.stringify(_selectedRowIds) != JSON.stringify(this.state.selectedRowIds)) {
                                  //   this.setState({ selectedRowIds: _selectedRowIds });
                                  // }

                                  const _selectedQuestionIds = selectedRows.map(
                                    ({ id }) => id,
                                  );

                                  let filteredQuestions = this.state.keyword
                                    ? this.state.searchQuestions
                                    : this.state.questions;
                                  let tobeKeepIds = filteredQuestions
                                    .map(({ id }) => id)
                                    .filter((id) =>
                                      _selectedQuestionIds.includes(id),
                                    );
                                  let tobeRemoveIds = filteredQuestions
                                    .map(({ id }) => id)
                                    .filter(
                                      (id) =>
                                        !_selectedQuestionIds.includes(id),
                                    );

                                  let newSelectedQuestionIds =
                                    this.state.selectedRowIds.filter(
                                      (id) => !tobeRemoveIds.includes(id),
                                    );
                                  newSelectedQuestionIds = [
                                    ...new Set([
                                      ...newSelectedQuestionIds,
                                      ...tobeKeepIds,
                                    ]),
                                  ];

                                  if (
                                    JSON.stringify(newSelectedQuestionIds) !=
                                    JSON.stringify(this.state.selectedRowIds)
                                  ) {
                                    this.setState({
                                      selectedRowIds: newSelectedQuestionIds,
                                    });
                                    // onSelectedChange(this.state.questions.filter(({ id }) => newSelectedQuestionIds.includes(id)));
                                  }
                                }}
                                customStyles={{
                                  headCells: {
                                    style: {
                                      background: "rgb(243, 246, 249)",
                                    },
                                  },
                                }}
                                noDataComponent={
                                  <div className="mt-5">Tidak Ada Data</div>
                                }
                                persistTableHead={true}
                                progressPending={this.state.loading}
                              />
                            </div>

                            <div
                              className="flex-column"
                              data-kt-stepper-element="content"
                            >
                              <div className="row pt-7">
                                <h2 className="fs-5 text-muted mb-3">
                                  Target Akademi
                                </h2>
                                <div className="col-lg-12">
                                  <div className="table-responsive">
                                    <DataTable
                                      columns={[
                                        {
                                          name: "No",
                                          width: "70px",
                                          center: true,
                                          cell: ({ idx }) => idx + 1,
                                        },
                                        {
                                          name: "Akademi",
                                          selector: ({ label }) => (
                                            <div className="mt-2">{label}</div>
                                          ),
                                        },
                                        {
                                          name: "Pelatihan",
                                          width: "300px",
                                          center: true,
                                          selector: ({ jumlah }) => {
                                            return <span>{jumlah}</span>;
                                          },
                                        },
                                        {
                                          name: "Status",
                                          width: "200px",
                                          center: true,
                                          selector: ({ status }) => (
                                            <span
                                              className={
                                                "badge badge-light-" +
                                                (status == 1
                                                  ? "success"
                                                  : "danger") +
                                                " fs-7 m-1"
                                              }
                                            >
                                              {status == 1
                                                ? "Publish"
                                                : "Unpublish"}
                                            </span>
                                          ),
                                        },
                                      ]}
                                      data={Object.values(dataAkademi)
                                        .filter(({ label }) => !!label)
                                        .map((s, idx) => ({ idx, ...s }))}
                                      highlightOnHover
                                      pointerOnHover
                                      pagination
                                      // paginationTotalRows={state.soals.length}
                                      // selectableRows
                                      // selectableRowSelected={({ id = null } = {}) => state.selectedRows.map(({ id } = {}) => id).includes(id)}
                                      // onSelectedRowsChange={({ selectedRows }) => state.setSelectedRows(selectedRows)}
                                      customStyles={{
                                        headCells: {
                                          style: {
                                            background: "rgb(243, 246, 249)",
                                          },
                                        },
                                      }}
                                      noDataComponent={
                                        <div className="mt-5">
                                          Tidak Ada Data
                                        </div>
                                      }
                                      persistTableHead={true}
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="row pt-7">
                                <h2 className="fs-5 text-muted mb-3">
                                  Target Tema
                                </h2>
                                <div className="col-lg-12">
                                  <div className="table-responsive">
                                    <DataTable
                                      columns={[
                                        {
                                          name: "No",
                                          width: "70px",
                                          center: true,
                                          cell: ({ idx }) => idx + 1,
                                        },
                                        {
                                          name: "Tema",
                                          selector: ({ label }) => (
                                            <div className="mt-2">{label}</div>
                                          ),
                                        },
                                        {
                                          name: "Pelatihan",
                                          width: "300px",
                                          center: true,
                                          selector: ({ jumlah }) => {
                                            return <span>{jumlah}</span>;
                                          },
                                        },
                                        {
                                          name: "Status",
                                          width: "200px",
                                          center: true,
                                          selector: ({ status }) => (
                                            <span
                                              className={
                                                "badge badge-light-" +
                                                (status == 1
                                                  ? "success"
                                                  : "danger") +
                                                " fs-7 m-1"
                                              }
                                            >
                                              {status == 1
                                                ? "Publish"
                                                : "Unpublish"}
                                            </span>
                                          ),
                                        },
                                      ]}
                                      data={Object.values(dataTema)
                                        .filter(({ label }) => !!label)
                                        .map((s, idx) => ({ idx, ...s }))}
                                      highlightOnHover
                                      pointerOnHover
                                      pagination
                                      // paginationTotalRows={state.soals.length}
                                      // selectableRows
                                      // selectableRowSelected={({ id = null } = {}) => state.selectedRows.map(({ id } = {}) => id).includes(id)}
                                      // onSelectedRowsChange={({ selectedRows }) => state.setSelectedRows(selectedRows)}
                                      customStyles={{
                                        headCells: {
                                          style: {
                                            width: "100%",
                                            background: "rgb(243, 246, 249)",
                                          },
                                        },
                                      }}
                                      noDataComponent={
                                        <div className="mt-5">
                                          Tidak Ada Data
                                        </div>
                                      }
                                      persistTableHead={true}
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="row pt-7">
                                <h2 className="fs-5 text-muted mb-3">
                                  Target Pelatihan
                                </h2>
                                <div className="col-lg-12">
                                  <div className="table-responsive">
                                    <DataTable
                                      columns={[
                                        {
                                          name: "No",
                                          width: "70px",
                                          center: true,
                                          cell: ({ idx }) => idx + 1,
                                        },
                                        {
                                          name: "Pelatihan",
                                          selector: ({ label, akademi }) => (
                                            <div className="mt-2">
                                              <label className="d-flex flex-stack mb- mt-1">
                                                <span className="d-flex align-items-center me-2">
                                                  <span className="d-flex flex-column">
                                                    <h6 className="fw-bolder fs-7 mb-0">
                                                      {label}
                                                    </h6>
                                                    <span className="fs-7 text-muted fw-semibold">
                                                      {akademi}
                                                    </span>
                                                  </span>
                                                </span>
                                              </label>
                                            </div>
                                          ),
                                        },
                                        {
                                          name: "Jadwal Pelatihan",
                                          width: "300px",
                                          center: false,
                                          selector: ({
                                            pelatihan_mulai,
                                            pelatihan_selesai,
                                          }) => {
                                            return (
                                              <span>
                                                {moment(pelatihan_mulai).format(
                                                  "D MMM YYYY",
                                                )}{" "}
                                                -{" "}
                                                {moment(
                                                  pelatihan_selesai,
                                                ).format("D MMM YYYY")}
                                              </span>
                                            );
                                          },
                                        },
                                        {
                                          name: "Pelaksanaan Test",
                                          width: "200px",
                                          center: false,
                                          selector: ({ status_pelatihan }) => {
                                            return (
                                              <span>{status_pelatihan}</span>
                                            );
                                          },
                                        },
                                        {
                                          name: "Jumlah Peserta",
                                          width: "200px",
                                          center: false,
                                          selector: ({ pelatihan_id }) => {
                                            const data = (
                                              this.state.dataPeserta || []
                                            )?.filter(
                                              (p) =>
                                                p?.training_id == pelatihan_id,
                                            );
                                            return (
                                              <span>{data.length} Peserta</span>
                                            );
                                          },
                                        },
                                        {
                                          name: "Status",
                                          width: "200px",
                                          center: false,
                                          selector: ({ status }) => (
                                            <span
                                              className={
                                                "badge badge-light-" +
                                                (status == 1
                                                  ? "success"
                                                  : "danger") +
                                                " fs-7 m-1"
                                              }
                                            >
                                              {status == 1
                                                ? "Publish"
                                                : "Unpublish"}
                                            </span>
                                          ),
                                        },
                                      ]}
                                      data={dataPelatihan
                                        .filter(({ label }) => !!label)
                                        .map((s, idx) => ({ idx, ...s }))}
                                      highlightOnHover
                                      pointerOnHover
                                      pagination
                                      // paginationTotalRows={state.soals.length}
                                      // selectableRows
                                      // selectableRowSelected={({ id = null } = {}) => state.selectedRows.map(({ id } = {}) => id).includes(id)}
                                      // onSelectedRowsChange={({ selectedRows }) => state.setSelectedRows(selectedRows)}
                                      customStyles={{
                                        headCells: {
                                          style: {
                                            background: "rgb(243, 246, 249)",
                                          },
                                        },
                                      }}
                                      noDataComponent={
                                        <div className="mt-5">
                                          Tidak Ada Data
                                        </div>
                                      }
                                      persistTableHead={true}
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <PreviewModal
          questionIdx={this.state.selectedPreviewIdx}
          questions={this.state.selectedPreviewRows}
          onClose={() =>
            this.setState({
              selectedPreviewIdx: null,
              selectedPreviewRows: [],
            })
          }
        />
      </div>
    );
  }
}

const ListSoalTrivia = (props) => {
  return (
    <>
      <SideNav />
      <Header />
      <Content {...props} />
      <Footer />
    </>
  );
};

export default withRouter(ListSoalTrivia);
