import { withRouter } from "../../components/utils";
import { AddTrivia } from "../add";

export default withRouter(AddTrivia);
