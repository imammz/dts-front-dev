import "line-awesome/dist/line-awesome/css/line-awesome.min.css";
import React from "react";
import Swal from "sweetalert2";
import Footer from "../../../../Footer";
import Header from "../../../../Header";
import SideNav from "../../../../SideNav";
import {
  getTipeSoalTestSubstansi,
  tambahTipeSoalTestSubstansi,
  updateTipeSoalTestSubstansi,
} from "../../components/actions";
import { withRouter } from "../../components/utils";
import Cookies from "js-cookie";

const resetErrorTestSubstansi = () => {
  return {
    namaError: null,
    valueError: null,
    statusError: null,
  };
};

const validateTipeSoalTestSubstansi = (state) => {
  let error = {};

  if (!state.nama) error.namaError = "Nama tidak boleh kosong";
  if (!state.value) error.valueError = "Nilai tidak boleh kosong";
  if (state.status == null)
    error.statusError = "Status harus terpilih salah satu";

  return error;
};

class Content extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      nama: "",
      namaError: null,
      value: null,
      valueError: null,
      status: null,
      statusError: null,
    };
  }

  save = async () => {
    try {
      const { history } = this.props || {};
      const { params } = this.props || {};
      const { id } = params || {};

      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });

      if (id) {
        await updateTipeSoalTestSubstansi(
          id,
          this.state.nama,
          this.state.value,
          this.state.status,
        );
      } else {
        await tambahTipeSoalTestSubstansi(
          this.state.nama,
          this.state.value,
          this.state.status,
        );
      }

      Swal.close();

      await Swal.fire({
        title: "Tipe soal test substansi berhasil disimpan",
        icon: "success",
        confirmButtonText: "Ok",
      });

      window.location.href = "/subvit/test-substansi/tipe-soal/list";
    } catch (err) {
      await Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  init = async () => {
    const { params } = this.props || {};
    const { id } = params || {};

    if (id) {
      if (Cookies.get("token") == null) {
        Swal.fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        }).then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
      }

      try {
        Swal.fire({
          title: "Mohon Tunggu!",
          icon: "info", // add html attribute if you want or remove
          allowOutsideClick: false,
          didOpen: () => {
            Swal.showLoading();
          },
        });

        const substansi = await getTipeSoalTestSubstansi(id);
        const { name, value, status } = substansi || {};
        this.setState({ nama: name, value, status });

        // const [dataLevel, dataStatus, dataTipeSoal, dataKategori, dataAkademi] = await Promise.all([
        //   fetchLevel(), fetchStatus(), fetchTipeSoal(), fetchKategori(), fetchAkademi()
        // ]);

        Swal.close();
      } catch (err) {
        Swal.fire({
          title: err,
          icon: "warning",
          confirmButtonText: "Ok",
        });
      }
    }
  };

  componentDidMount() {
    this.init();
  }

  render() {
    const { params } = this.props || {};
    const { id } = params || {};

    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <path
                        opacity="0.3"
                        d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                        fill="#7239ea"
                      ></path>
                      <path
                        d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                        fill="#7239ea"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Subvit
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Test Substansi
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={() =>
                    this?.props?.history && this?.props?.history(-1)
                  }
                  className="btn btn-sm btn-light btn-active-light-primary"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Tambah Tipe Soal
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="row">
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">
                            Tipe Soal
                          </label>
                          <input
                            className="form-control form-control-sm"
                            placeholder="Masukkan Nama Tipe Soal"
                            name="pertanyaan"
                            value={this.state.nama}
                            onChange={(e) =>
                              this.setState({ nama: e.target.value })
                            }
                          />
                          <span style={{ color: "red" }}>
                            {this.state.namaError}
                          </span>
                        </div>
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">
                            Bobot Nilai
                          </label>
                          <input
                            className="form-control form-control-sm"
                            placeholder="Masukkan Bobot Nilai Tipe Soal"
                            name="pertanyaan"
                            value={this.state.value}
                            onChange={(e) => {
                              let intVal = parseInt(e.target.value);
                              if (isNaN(intVal)) intVal = 0;
                              this.setState({ value: intVal });
                            }}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.valueError}
                          </span>
                        </div>
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">Status</label>
                          <div className="d-flex">
                            <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                              <input
                                className="form-check-input"
                                type="radio"
                                name="status"
                                value="1"
                                id="status_draft"
                                checked={this.state.status == 1}
                                onChange={(e) =>
                                  this.setState({ status: e.target.value })
                                }
                              />
                              <label
                                className="form-check-label"
                                for="status_draft"
                              >
                                Publish
                              </label>
                            </div>
                            <div className="form-check form-check-sm form-check-custom form-check-solid">
                              <input
                                className="form-check-input"
                                type="radio"
                                name="status"
                                value="2"
                                id="status_publish"
                                checked={this.state.status == 2}
                                onChange={(e) =>
                                  this.setState({ status: e.target.value })
                                }
                              />
                              <label
                                className="form-check-label"
                                htmlFor="status_publish"
                              >
                                Unpublish
                              </label>
                            </div>
                          </div>
                          <span style={{ color: "red" }}>
                            {this.state.statusError}
                          </span>
                        </div>
                      </div>
                      <div className="row mt-7">
                        <div className="col-lg-12 mb-7 fv-row text-center">
                          <button
                            onClick={() =>
                              (window.location.href = `/subvit/test-substansi/tipe-soal/list/`)
                            }
                            className="btn btn-md btn-light me-3"
                          >
                            Batal
                          </button>
                          <button
                            className="btn btn-md btn-primary"
                            onClick={async () => {
                              const error = validateTipeSoalTestSubstansi(
                                this.state,
                              );

                              if (Object.keys(error).length > 0) {
                                this.setState({
                                  ...resetErrorTestSubstansi(),
                                  ...error,
                                });

                                Swal.fire({
                                  title:
                                    "Masih terdapat isian yang belum lengkap",
                                  icon: "warning",
                                  confirmButtonText: "Ok",
                                });
                              } else {
                                Swal.fire({
                                  title: "Mohon Tunggu!",
                                  icon: "info", // add html attribute if you want or remove
                                  allowOutsideClick: false,
                                  didOpen: () => {
                                    Swal.showLoading();
                                  },
                                });

                                await this.save();
                              }

                              // astate.error = errorMessage(astate.question);
                              // const { answer = [], ...rest } = astate.error || {};

                              // if (!(Object.keys(rest).length > 0 || Object.keys(answer).length > 0)) {
                              //   state.addRows(id, [astate.question]).then((success) => {
                              //     if (success) history(`/subvit/test-substansi/soal/list/${id}`);
                              //   })
                              // }
                              // history(`/subvit/test-substansi/soal/list/${id}`);
                            }}
                          >
                            <i className="fa fa-paper-plane ms-1"></i>Simpan
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export const AddTrivia = (props) => {
  return (
    <>
      <SideNav />
      <Header />
      <Content {...props} />
      <Footer />
    </>
  );
};

export default withRouter(AddTrivia);
