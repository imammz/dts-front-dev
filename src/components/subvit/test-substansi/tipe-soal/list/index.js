import "line-awesome/dist/line-awesome/css/line-awesome.min.css";
import React from "react";
import Swal from "sweetalert2";
import Footer from "../../../../Footer";
import Header from "../../../../Header";
import SideNav from "../../../../SideNav";
import {
  cariTipeSoalTestSubstansi,
  deleteTipeSoalTestSubstansi,
  fetchTipeSoal,
  getTipeSoalTestSubstansi,
  listTipeSoalTestSubstansi,
  tambahTipeSoalTestSubstansi,
  updateTipeSoalTestSubstansi,
} from "../../components/actions";
import { withRouter } from "../../components/utils";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import { capitalizeFirstLetter } from "../../../../publikasi/helper";
import _ from "lodash";
import Select from "react-select";

const sortField = ["_", "id", "id", "name", "value", "status"];

class Content extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      keyword: "",
      searchData: [],
      sort: 1,
      sortDir: "asc",
    };
  }

  init = async (sort = 1, dir = "desc") => {
    if (this.state.keyword) return this.search(this.state.keyword);

    if (this.state.keyword) {
      sort = 1;
      dir = "desc";
    }

    if (Cookies.get("token") == null) {
      Swal.fire({
        title: "Unauthenticated.",
        text: "Silahkan login ulang",
        icon: "warning",
        confirmButtonText: "Ok",
      }).then((result) => {
        if (result.isConfirmed) {
          window.location = "/";
        }
      });
    }

    try {
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });

      const data = await fetchTipeSoal(0, sortField[sort], dir.toUpperCase());
      this.setState({ data, sort, sortDir: dir });

      Swal.close();
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  remove = async (id) => {
    const { isConfirmed = false } = await Swal.fire({
      title: "Apakah anda yakin ?",
      text: "Data ini tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, hapus!",
      cancelButtonText: "Tidak",
    });

    if (isConfirmed) {
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });

      await deleteTipeSoalTestSubstansi(id);

      Swal.close();

      await Swal.fire({
        title: "Tipe soal test substansi berhasil dihapus",
        icon: "success",
        confirmButtonText: "Ok",
      });

      await this.init();
    }
  };

  search = async (keyword) => {
    let searchData = [];

    let sort = this.state.sort,
      sortDir = this.state.sortDir.toUpperCase();
    if (!this.state.keyword) {
      sort = 3;
      sortDir = "ASC";
    }

    console.log(this.state.keyword, sort, sortDir);

    if (keyword) {
      try {
        searchData = await cariTipeSoalTestSubstansi(
          keyword,
          sortField[sort],
          sortDir.toUpperCase(),
        );
        if (searchData.length == 0) {
          throw new Error("Data Tipe Soal Tidak Di Temukan!");
        }
      } catch (err) {
        // console.log('searchData',err )
        Swal.fire({
          title: err,
          icon: "warning",
          confirmButtonText: "Ok",
        });
      }
    }

    this.setState({
      keyword,
      searchData,
      sort,
      sortDir,
    });
  };

  componentDidMount() {
    this.init();
  }

  render() {
    const data = (
      this.state.keyword ? this.state.searchData : this.state.data
    ).map((d, idx) => ({ idx, ...d }));

    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <path
                        opacity="0.3"
                        d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                        fill="#7239ea"
                      ></path>
                      <path
                        d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                        fill="#7239ea"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Subvit
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Test Substansi
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <a
                  href="/subvit/test-substansi/tipe-soal/add"
                  className="btn btn-success fw-bolder btn-sm"
                >
                  <i className="bi bi-plus-circle"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Tambah Tipe Soal
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Tipe Soal
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <div className="d-flex align-items-center position-relative my-1 me-2">
                            <span className="svg-icon svg-icon-1 position-absolute ms-6">
                              <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className="mh-50px"
                              >
                                <rect
                                  opacity="0.5"
                                  x="17.0365"
                                  y="15.1223"
                                  width="8.15546"
                                  height="2"
                                  rx="1"
                                  transform="rotate(45 17.0365 15.1223)"
                                  fill="currentColor"
                                ></rect>
                                <path
                                  d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                  fill="currentColor"
                                ></path>
                              </svg>
                            </span>
                            <input
                              type="text"
                              data-kt-user-table-filter="search"
                              className="form-control form-control-sm form-control-solid w-250px ps-14"
                              placeholder="Cari Tipe Soal"
                              onChange={(e) => {
                                const keyword = e.target.value;
                                if (!keyword) {
                                  this.search(keyword);
                                  e.preventDefault();
                                }
                              }}
                              onKeyDown={(e) => {
                                const keyword = e.target.value;
                                if (e.key == "Enter") {
                                  this.search(keyword);
                                  e.preventDefault();
                                }
                              }}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="table-responsive">
                          <DataTable
                            columns={[
                              {
                                name: "No",
                                width: "70px",
                                center: true,
                                cell: ({ idx }, index) => idx + 1,
                              },
                              {
                                name: "ID Tipe Soal",
                                sortable: true,
                                selector: ({ value = "" }) => (
                                  <div className="mt-2">
                                    <b>{value}</b>
                                  </div>
                                ),
                              },
                              {
                                name: "Tipe Soal",
                                sortable: true,
                                selector: ({ label = "" }) => (
                                  <div className="mt-2">
                                    <b>{capitalizeFirstLetter(label)}</b>
                                  </div>
                                ),
                              },
                              {
                                name: "Bobot Nilai",
                                width: "150px",
                                center: true,
                                sortable: true,
                                selector: ({ nilai = "" }) => (
                                  <>
                                    <span
                                      className="badge badge-light-primary fs-7 m-1"
                                      style={{
                                        overflow: "hidden",
                                        whiteSpace: "wrap",
                                        textOverflow: "ellipses",
                                      }}
                                    >
                                      {nilai}
                                    </span>
                                  </>
                                ),
                              },
                              {
                                name: "Jml. Bank Soal",
                                width: "170px",
                                center: true,
                                sortable: true,
                                selector: ({ jml: substansi = 0 }) => (
                                  <>
                                    <span
                                      className="badge badge-light-primary fs-7 m-1"
                                      style={{
                                        overflow: "hidden",
                                        whiteSpace: "wrap",
                                        textOverflow: "ellipses",
                                      }}
                                    >
                                      {substansi}
                                    </span>
                                  </>
                                ),
                              },
                              {
                                name: "Status",
                                width: "120px",
                                center: true,
                                sortable: true,
                                selector: ({ status, ...rest }) => {
                                  let badge = "",
                                    label = "";
                                  if (status == 1) {
                                    badge = "success";
                                    label = "Publish";
                                  } else if (status == 2) {
                                    badge = "danger";
                                    label = "Unpublish";
                                  }

                                  return (
                                    <div>
                                      <span
                                        className={
                                          "badge badge-light-" +
                                          badge +
                                          " fs-7 m-1"
                                        }
                                      >
                                        {label}
                                      </span>
                                    </div>
                                  );
                                },
                              },
                              {
                                name: "Aksi",
                                center: true,
                                width: "150px",
                                cell: (row) => {
                                  const {
                                    value: id,
                                    status,
                                    jml: substansi = 0,
                                  } = row || {};
                                  return (
                                    <div>
                                      <a
                                        href={`/subvit/test-substansi/tipe-soal/edit/${id}`}
                                        title="Edit"
                                        className={`btn btn-icon btn-warning btn-sm me-3 ${
                                          substansi > 0 ? "disabled" : ""
                                        }`}
                                        // data-bs-toggle="modal"
                                        // data-bs-target="#add_edit_soal"
                                        // onClick={e => state.startEdit(row)}
                                      >
                                        <i className="bi bi-gear-fill text-white"></i>
                                      </a>
                                      <a
                                        href="#"
                                        onClick={(e) => this.remove(id)}
                                        title="Hapus"
                                        className={`btn btn-icon btn-sm btn-danger ${
                                          status == 1 || substansi > 0
                                            ? "disabled"
                                            : ""
                                        }`}
                                      >
                                        <i className="bi bi-trash-fill text-white"></i>
                                      </a>
                                    </div>
                                  );
                                },
                              },
                            ]}
                            data={data.map((d, idx) => ({ ...d, idx }))}
                            // progressPending={state.fetching}
                            highlightOnHover
                            pointerOnHover
                            pagination
                            onSort={({ id }, direction, rows) =>
                              this.init(id, direction)
                            }
                            customStyles={{
                              headCells: {
                                style: {
                                  background: "rgb(243, 246, 249)",
                                },
                              },
                            }}
                            noDataComponent={
                              <div className="mt-5">Tidak Ada Data</div>
                            }
                            persistTableHead={true}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export const AddTrivia = (props) => {
  return (
    <>
      <SideNav />
      <Header />
      <Content {...props} />
      <Footer />
    </>
  );
};

export default withRouter(AddTrivia);
