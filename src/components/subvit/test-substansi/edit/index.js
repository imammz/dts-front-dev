import Cookies from "js-cookie";
import "line-awesome/dist/line-awesome/css/line-awesome.min.css";
import React from "react";
import Swal from "sweetalert2";
import Footer from "../../../Footer";
import Header from "../../../Header";
import SideNav from "../../../SideNav";
import {
  fetchAkademi,
  fetchKategori,
  fetchLevel,
  fetchListPelatihanByAkademiTema,
  fetchPelatihan,
  fetchStatus,
  fetchStatusPelatihan,
  fetchStatusSubstansi,
  fetchTema,
  fetchTestSubstansiById,
  fetchTestSubstansiByIdV2,
  fetchTipeSoal,
  updateTestSubstansi,
} from "../components/actions";
// import AddEditScreen from './AddEditScreen';
import DraftPublishScreen from "../add/DraftPublishScreen";
// import EditReorderScreen from './EditReorderScreen';
import HeaderScreen from "../add/HeaderScreen";
import {
  resetErrorTestSubstansi,
  validateTestSubstansi,
  withRouter,
} from "../components/utils";

class Content extends React.Component {
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  constructor(props) {
    super(props);

    this.state = {
      fetchingAkademi: false,
      dataAkademi: [],
      fetchingKategori: false,
      dataKategori: [],
      fetchingTipeSoal: false,
      dataTipeSoal: [],

      nama: "",
      namaError: null,

      dataLevel: [],
      selectedLevel: null,
      selectedLevelError: null,

      selectedAkademi: null,
      selectedAkademiError: null,

      fetchingTema: false,
      dataTema: [],
      selectedTema: null,
      selectedTemaError: null,

      fetchingPelatihan: false,
      dataPelatihan: [],
      selectedPelatihan: null,
      selectedPelatihanInfo: null,
      listPelatihanByAkademiTema: [],
      selectedPelatihanError: null,

      selectedKategori: null,
      selectedKategoriError: null,

      selectedMetodeSoal: "entry",
      selectedEditSoalIdx: null,

      mulaiPelaksanaan: null,
      mulaiPelaksanaanError: null,
      selesaiPelaksanaan: null,
      selesaiPelaksanaanError: null,
      jumlahSoal: null,
      jumlahSoalError: null,
      durasiDetik: null,
      durasiDetikError: null,
      passingGrade: null,
      passingGradeError: null,
      dataStatus: [],
      originalSelectedStatus: 0,
      selectedStatus: 0,
      selectedStatusError: null,

      questions: [],

      questionsPreview: [],

      pelatihan: [],
    };

    this.prevBtnRef = React.createRef();
    this.nextBtnRef = React.createRef();
  }

  init = async () => {
    if (Cookies.get("token") == null) {
      Swal.fire({
        title: "Unauthenticated.",
        text: "Silahkan login ulang",
        icon: "warning",
        confirmButtonText: "Ok",
      }).then((result) => {
        if (result.isConfirmed) {
          window.location = "/";
        }
      });
    }

    try {
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });

      const [dataLevel, dataStatus, dataTipeSoal, dataKategori, dataAkademi] =
        await Promise.all([
          fetchLevel(),
          fetchStatus(),
          fetchTipeSoal(1),
          fetchKategori(),
          fetchAkademi(),
        ]);

      let { params } = this.props || {};
      let { id } = params || {};
      let [{ header, soals }, { pelatihan, substansi }] = await Promise.all([
        fetchTestSubstansiById(id),
        fetchTestSubstansiByIdV2(id),
      ]);
      [substansi] = substansi;

      const {
        category,
        duration,
        id_akademi = 0,
        id_pelatihan,
        id_substansi,
        jml_soal,
        nama_akademi,
        nama_pelatihan,
        nama_substansi,
        nama_tema,
        passing_grade,
        status_pelaksanaan,
        status_substansi,
        tgl_pelaksanaan,
        tgl_selesai_pelaksanaan,
        theme_id = 0,
        level = -1,
      } = header || {};

      const [selectedAkademi] = dataAkademi.filter(
        ({ value }) => value == id_akademi,
      );

      let dataTema = [];
      let selectedTema = null;
      let dataPelatihan = [];
      let selectedPelatihan = null;
      let selectedPelatihanInfo = null;
      let selectedKategori = null;
      let selectedLevel = null;
      let listPelatihanByAkademiTema = [];

      if (category)
        [selectedKategori] = dataKategori.filter(
          ({ label }) => label == category,
        );
      const { value: id_kategori } = selectedKategori || {};

      try {
        if (id_akademi) dataTema = await fetchTema(id_akademi);
      } catch (err) {}

      try {
        if (id_akademi && theme_id)
          dataPelatihan = await fetchPelatihan(id_akademi, theme_id);
      } catch (err) {}

      try {
        if (id_akademi && theme_id && id_pelatihan) {
          const [_selectedPelatihanInfo, selectedSubstansiInfo] =
            await Promise.all([
              fetchStatusPelatihan(id_akademi, theme_id, id_pelatihan),
              fetchStatusSubstansi(id_akademi, theme_id, id_pelatihan),
            ]);

          selectedPelatihanInfo = {
            ..._selectedPelatihanInfo,
            ...selectedSubstansiInfo,
          };
        }
      } catch (err) {}

      try {
        listPelatihanByAkademiTema = await fetchListPelatihanByAkademiTema(
          id_kategori,
          id_akademi || 0,
          theme_id || 0,
          id_pelatihan || 0,
        );
      } catch (err) {}

      [selectedLevel] = dataLevel.filter(({ value }) => value == level);
      if (level >= 1 && theme_id)
        [selectedTema] = dataTema.filter(({ value }) => value == theme_id);
      if (level >= 2 && id_pelatihan)
        [selectedPelatihan] = dataPelatihan.filter(
          ({ value }) => value == id_pelatihan,
        );

      // if (selectedPelatihan) [selectedLevel] = dataLevel.filter(({ value }) => value == 2);
      // else if (selectedTema) [selectedLevel] = dataLevel.filter(({ value }) => value == 1);
      // else if (selectedAkademi) [selectedLevel] = dataLevel.filter(({ value }) => value == 0);

      this.setState({
        header,

        dataLevel,
        dataTipeSoal,
        dataKategori,
        dataAkademi,
        dataTema,
        dataPelatihan,
        dataStatus,

        nama: nama_substansi,
        selectedLevel,
        selectedAkademi,
        selectedTema,
        selectedPelatihan,
        selectedPelatihanInfo,
        listPelatihanByAkademiTema,
        selectedKategori,

        mulaiPelaksanaan: tgl_pelaksanaan,
        selesaiPelaksanaan: tgl_selesai_pelaksanaan,
        jumlahSoal: substansi?.jml_soal_tayang || 0,
        durasiDetik: duration,
        passingGrade: passing_grade,
        originalSelectedStatus: status_substansi,
        selectedStatus: status_substansi,

        questions: soals,
        pelatihan,
      });

      Swal.close();
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  componentDidUpdate(prevProps, prevState) {
    (async () => {
      const { value: prevSelectedKategori = 0 } =
        prevState.selectedKategori || {};
      const { value: prevSelectedAkademi = 0 } =
        prevState.selectedAkademi || {};
      const { value: prevSelectedTema = 0 } = prevState.selectedTema || {};
      const { value: prevSelectedPelatihan = 0 } =
        prevState.selectedPelatihan || {};

      const { value: selectedKategori = 0 } = this.state.selectedKategori || {};
      const { value: selectedAkademi = 0 } = this.state.selectedAkademi || {};
      const { value: selectedTema = 0 } = this.state.selectedTema || {};
      const { value: selectedPelatihan = 0 } =
        this.state.selectedPelatihan || {};

      let listPelatihanByAkademiTema = [];

      if (
        prevSelectedKategori != selectedKategori ||
        prevSelectedAkademi != selectedAkademi ||
        prevSelectedTema != selectedTema ||
        prevSelectedPelatihan != selectedPelatihan
      ) {
        try {
          listPelatihanByAkademiTema = await fetchListPelatihanByAkademiTema(
            selectedKategori,
            selectedAkademi,
            selectedTema,
            selectedPelatihan,
          );
          this.setState({ listPelatihanByAkademiTema });
        } catch (err) {}
      }

      if (prevSelectedPelatihan != selectedPelatihan) {
        if (selectedKategori == 1) {
          // const [subtansi_mulai] = listPelatihanByAkademiTema.map(({ subtansi_mulai }) => subtansi_mulai);
          if (!this.state.selectedPelatihanInfo?.subtansi_mulai)
            this.setState({ selectedKategori: null });
        } else if (selectedKategori == 2) {
          // const [midtest_mulai] = listPelatihanByAkademiTema.map(({ midtest_mulai }) => midtest_mulai);
          if (!this.state.selectedPelatihanInfo?.midtest_mulai)
            this.setState({ selectedKategori: null });
        }
      }
    })();
  }

  save = async () => {
    try {
      const { params, history } = this.props || {};
      const { id } = params || {};

      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });

      await updateTestSubstansi(id, this.state);

      Swal.close();

      await Swal.fire({
        title: "Test substansi berhasil diupdate",
        icon: "success",
        confirmButtonText: "Ok",
      });

      window.location.href = "/subvit/test-substansi";
    } catch (err) {
      await Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  onLevelChange = (v) => {
    this.setState({
      selectedLevel: v,
      selectedAkademi: null,
      selectedTema: null,
      selectedPelatihan: null,
    });
  };

  onAkademiChange = async (v) => {
    let dataTema = [];

    try {
      const { value: valueLevel } = this.state.selectedLevel || {};
      const { value } = v || {};

      if (valueLevel > 0) dataTema = await fetchTema(value);
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }

    this.setState({
      selectedAkademi: v,
      dataTema,
      selectedTema: null,
      dataPelatihan: [],
      selectedPelatihan: null,
    });
  };

  onTemaChange = async (v) => {
    let dataPelatihan = [];

    try {
      const { value: valueLevel } = this.state.selectedLevel || {};
      const { value: valueAkademi } = this.state.selectedAkademi || {};
      const { value } = v || {};

      if (valueLevel > 1)
        dataPelatihan = await fetchPelatihan(valueAkademi, value);
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }

    this.setState({
      selectedTema: v,
      dataPelatihan,
      selectedPelatihan: null,
    });
  };

  onPelatihanChange = async (v) => {
    const { value: selectedAkademi = -1 } = this.state.selectedAkademi || {};
    const { value: selectedTema = -1 } = this.state.selectedTema || {};
    const { value: selectedPelatihan = -1 } = v || {};
    const selectedPelatihanInfo = await fetchStatusPelatihan(
      selectedAkademi,
      selectedTema,
      selectedPelatihan,
    );

    this.setState({
      selectedPelatihan: v,
      selectedPelatihanInfo,
    });
  };

  onKategoriChange = (v) => {
    this.setState({
      selectedKategori: v,
    });
  };

  componentDidMount() {
    this.init();
  }

  render() {
    const { header, pelatihan = [] } = this.state || {};
    const {
      nama_akademi,
      nama_tema,
      nama_pelatihan,
      nama_substansi,
      status_substansi,
    } = header || {};
    const { params } = this.props || {};
    const { id } = params || {};

    let title = "";
    if (nama_substansi) title = `${nama_substansi}`;
    else if (nama_pelatihan) title = `Pelatihan ${nama_pelatihan}`;
    else if (nama_tema) title = `Tema ${nama_tema}`;
    else if (nama_akademi) title = `Akademi ${nama_akademi}`;

    const { value: selectedLevelValue = -1 } = this.state.selectedLevel || {};
    const {
      midtest_mulai = null,
      midtest_selesai = null,
      subtansi_mulai = null,
      subtansi_selesai = null,
    } = this.state.selectedPelatihanInfo || {};

    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <path
                        opacity="0.3"
                        d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                        fill="#7239ea"
                      ></path>
                      <path
                        d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                        fill="#7239ea"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Subvit
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Test Substansi
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={() =>
                    this?.props?.history && this?.props?.history(-1)
                  }
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>

                <a
                  href={"/subvit/test-substansi/soal/list/" + id}
                  className="btn btn-sm btn-primary me-2"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-list"></i>
                  </span>
                  List Soal
                </a>

                <a
                  href="#"
                  title="Hapus"
                  className={`btn btn-sm btn-danger btn-active-light-info ${
                    status_substansi != 0 ? "disabled" : ""
                  }`}
                  onClick={(e) => this.remove(id)}
                >
                  <i className="bi bi-trash-fill text-white"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div
                    className="stepper stepper-links mb-n3"
                    id="kt_subvit_trivia"
                  >
                    <div className="col-lg-12 mt-7">
                      <div className="card border">
                        <div className="card-body mt-5 pt-10 pb-8">
                          <div className="col-12">
                            <h1
                              className="align-items-center text-dark fw-bolder my-1 fs-4"
                              style={{ textTransform: "capitalize" }}
                            >
                              {title || "-"}
                            </h1>
                            <p className="text-dark fs-7 mb-0">
                              {nama_akademi || "-"}
                              <span className="text-muted fs-7 mb-0">
                                {" "}
                                - {nama_tema || "-"}
                              </span>
                            </p>
                          </div>
                        </div>
                        <div className="card-header">
                          <div className="card-title">
                            <div className="stepper-nav flex-wrap mb-n4">
                              <div
                                className="stepper-item ms-0 my-2 current"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label ms-0">
                                    <h3 className="stepper-title fs-6">
                                      Informasi Tes Substansi
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Targeting
                                    </h3>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="card-body">
                          <form
                            action="#"
                            id="kt_subvit_trivia_form"
                            method="post"
                            onSubmit={async (e) => {
                              e.preventDefault();

                              const { selectedPelatihanInfo = {}, ...rest } =
                                this.state;
                              const error = validateTestSubstansi({
                                ...rest,
                                ...selectedPelatihanInfo,
                              });

                              if (Object.keys(error).length > 0) {
                                this.setState({
                                  ...resetErrorTestSubstansi(),
                                  ...error,
                                });

                                Swal.fire({
                                  title:
                                    "Masih terdapat isian yang belum lengkap",
                                  icon: "warning",
                                  confirmButtonText: "Ok",
                                });
                              } else {
                                Swal.fire({
                                  title: "Mohon Tunggu!",
                                  icon: "info", // add html attribute if you want or remove
                                  allowOutsideClick: false,
                                  didOpen: () => {
                                    Swal.showLoading();
                                  },
                                });

                                await this.save();
                              }
                            }}
                          >
                            <div
                              className="current"
                              data-kt-stepper-element="content"
                            >
                              <div className="row mt-7">
                                <HeaderScreen
                                  data={{
                                    nama: this.state.nama,
                                    setNama: (v) => this.setState({ nama: v }),
                                    namaError: this.state.namaError,
                                    dataLevel: this.state.dataLevel,
                                    selectedLevel: this.state.selectedLevel,
                                    onLevelChange: this.onLevelChange,
                                    selectedLevelError:
                                      this.state.selectedLevelError,
                                    fetchingAkademi: this.state.fetchingAkademi,
                                    dataAkademi: this.state.dataAkademi,
                                    selectedAkademi: this.state.selectedAkademi,
                                    onAkademiChange: this.onAkademiChange,
                                    selectedAkademiError:
                                      this.state.selectedAkademiError,
                                    fetchingTema: this.state.fetchingTema,
                                    dataTema: this.state.dataTema,
                                    selectedTema: this.state.selectedTema,
                                    onTemaChange: this.onTemaChange,
                                    selectedTemaError:
                                      this.state.selectedTemaError,
                                    fetchingPelatihan:
                                      this.state.fetchingPelatihan,
                                    dataPelatihan: this.state.dataPelatihan.map(
                                      ({
                                        nama_unit_kerja,
                                        pid,
                                        nama,
                                        wilayah,
                                        batch = 1,
                                        ...rest
                                      }) => ({
                                        ...rest,
                                        value: pid,
                                        label: `[${nama_unit_kerja}] - ${
                                          this.state.selectedAkademi?.slug
                                        }${pid} - ${nama} Batch ${batch} - ${
                                          wilayah || "Online"
                                        }`,
                                      }),
                                    ),
                                    selectedPelatihan:
                                      this.state.selectedPelatihan,
                                    onPelatihanChange: this.onPelatihanChange,
                                    selectedPelatihanError:
                                      this.state.selectedPelatihanError,
                                    dataKategori: this.state.dataKategori,
                                    selectedKategori:
                                      this.state.selectedKategori,
                                    onKategoriChange: this.onKategoriChange,
                                    selectedKategoriError:
                                      this.state.selectedKategoriError,
                                    isMetodeSoalEntry: () =>
                                      this.state.selectedMetodeSoal == "entry",
                                    isMetodeSoalImport: () =>
                                      this.state.selectedMetodeSoal == "import",
                                    onMetodeSoalChange: this.onMetodeSoalChange,
                                    levelDisabled:
                                      this.state.originalSelectedStatus != 0,
                                    akademiDisabled:
                                      this.state.originalSelectedStatus != 0,
                                    temaDisabled:
                                      this.state.originalSelectedStatus != 0,
                                    pelatihanDisabled:
                                      this.state.originalSelectedStatus != 0,
                                    kategoriDisabled:
                                      this.state.originalSelectedStatus != 0,
                                    kategoriOptionDisabled: ({ value }) => {
                                      if (selectedLevelValue != 2) return false;
                                      if (
                                        value == 1 &&
                                        !!subtansi_mulai &&
                                        !!subtansi_selesai
                                      )
                                        return false;
                                      if (
                                        value == 2 &&
                                        !!midtest_mulai &&
                                        !!midtest_selesai
                                      )
                                        return false;
                                      return true;
                                    },
                                  }}
                                />
                              </div>
                            </div>

                            <div data-kt-stepper-element="content">
                              <DraftPublishScreen
                                data={{
                                  questions: this.state.questions,
                                  selectedLevel: this.state.selectedLevel,
                                  selectedPelatihanInfo:
                                    this.state.selectedPelatihanInfo,
                                  listPelatihanByAkademiTema:
                                    this.state.listPelatihanByAkademiTema,
                                  selectedKategori: this.state.selectedKategori,
                                  mulaiPelaksanaan: this.state.mulaiPelaksanaan,
                                  mulaiPelaksanaanError:
                                    this.state.mulaiPelaksanaanError,
                                  // mulaiPelaksanaanDisabled: [0, 1].includes(this.state.originalSelectedStatus),
                                  selesaiPelaksanaan:
                                    this.state.selesaiPelaksanaan,
                                  selesaiPelaksanaanError:
                                    this.state.selesaiPelaksanaanError,
                                  // selesaiPelaksanaanDisabled: [0, 1].includes(this.state.originalSelectedStatus),
                                  jumlahSoal: this.state.jumlahSoal,
                                  jumlahSoalDisabled:
                                    this.state.originalSelectedStatus != 0,
                                  jumlahSoalError: this.state.jumlahSoalError,
                                  durasiDetik: this.state.durasiDetik,
                                  durasiDetikDisabled:
                                    this.state.originalSelectedStatus != 0,
                                  durasiDetikError: this.state.durasiDetikError,
                                  passingGrade: this.state.passingGrade,
                                  passingGradeError:
                                    this.state.passingGradeError,
                                  dataStatus: this.state.dataStatus.map(
                                    (s) => ({
                                      ...s,
                                      disabled:
                                        s.value <
                                        this.state.originalSelectedStatus,
                                    }),
                                  ),
                                  selectedStatus: this.state.selectedStatus,
                                  selectedStatusError:
                                    this.state.selectedStatusError,
                                  selectedAkademi: this.state.selectedAkademi,
                                  selectedTema: this.state.selectedTema,
                                  selectedPelatihan:
                                    this.state.selectedPelatihan,
                                  dataAkademi: this.state.dataAkademi,
                                  dataTema: this.state.dataTema,
                                  dataPelatihan: this.state.dataPelatihan,
                                  pelatihan: this.state.pelatihan,
                                  updateState: (k, v) => {
                                    this.setState({ [k]: v });
                                  },
                                }}
                              />
                            </div>

                            <div className="text-center my-7">
                              {/* <button onClick={this.handleClickBatal} className="btn btn-secondary btn-sm">Daftar Pelatihan</button> */}
                              <button
                                className="btn btn-light btn-md me-3 mr-2"
                                data-kt-stepper-action="previous"
                                ref={this.prevBtnRef}
                              >
                                <i className="fa fa-chevron-left"></i>Sebelumnya
                              </button>
                              <button
                                type="submit"
                                className="btn btn-primary btn-md"
                                data-kt-stepper-action="submit"
                              >
                                <i className="fa fa-paper-plane ms-1"></i>Simpan
                              </button>
                              <button
                                type="button"
                                className="btn btn-primary btn-md"
                                data-kt-stepper-action="next"
                                ref={this.nextBtnRef}
                              >
                                <i className="fa fa-chevron-right"></i>Lanjutkan
                              </button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* <PreviewModal questions={this.state.questionsPreview} /> */}
      </div>
    );
  }
}

const AddTrivia = (props) => {
  return (
    <>
      <SideNav />
      <Header />
      <Content {...props} />
      <Footer />
    </>
  );
};

export default withRouter(AddTrivia);
