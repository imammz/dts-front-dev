import axios from "axios";
import Cookies from "js-cookie";
import "line-awesome/dist/line-awesome/css/line-awesome.min.css";
import { Observer } from "mobx-react-lite";
import React from "react";
import DataTable from "react-data-table-component";
import Swal from "sweetalert2";
import Footer from "../../../Footer";
import Header from "../../../Header";
import SideNav from "../../../SideNav";
import { capitalizeFirstLetter } from "../../../publikasi/helper";
import {
  fetchAkademi,
  fetchKategori,
  fetchLevel,
  fetchListPelatihanByAkademiTema,
  fetchListTestSubstansi,
  fetchStatus,
  fetchStatusPelaksanaan,
  fetchTema,
  fetchTestSubstansiById,
  fetchTestSubstansiHeaderById,
  fetchTipeSoal,
  removeTestSubstansiById,
  tambahTestSubstansi,
} from "../components/actions";
// import { PreviewModal } from '../components/preview-test-substansi';
import { letters, withRouter } from "../components/utils";
import { PreviewComponents } from "../components/preview-test-substansi";
import Select from "react-select";
import moment from "moment";
import { fixBootstrapDropdown } from "../../../../utils/commonutil";
import { isExceptionRole } from "../../../AksesHelper";
// import AddEditScreen, { Metode } from './AddEditScreen';
// import DraftPublishScreen from './DraftPublishScreen';
// import EditReorderScreen from './EditReorderScreen';
// import HeaderScreen from './HeaderScreen';

class RemotePreview extends React.Component {
  constructor(props) {
    super(props);

    this.ref = React.createRef();
    this.state = {
      soals: [],
    };
  }

  fetch = async (id) => {
    Swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      },
    });

    this.setState({
      soals: [],
    });

    try {
      const { soals = [] } = await fetchTestSubstansiById(id);
      this.setState({
        soals,
      });

      Swal.close();
    } catch (err) {
      await Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  componentDidUpdate(prevProps) {
    const { id: selectedPrevIdToPreview } = prevProps;
    const { id } = this.props;
    if (id != selectedPrevIdToPreview) {
      this.fetch(id);
    }
  }

  render() {
    return (
      <div
        className="modal fade"
        tabindex="-1"
        id="preview_soal"
        ref={this.ref}
      >
        <div className="modal-dialog modal-lg">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Preview Soal</h5>
              <div
                className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal"
                aria-label="Close"
              >
                <span className="svg-icon svg-icon-2x">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <rect
                      opacity="0.5"
                      x="6"
                      y="17.3137"
                      width="16"
                      height="2"
                      rx="1"
                      transform="rotate(-45 6 17.3137)"
                      fill="currentColor"
                    />
                    <rect
                      x="7.41422"
                      y="6"
                      width="16"
                      height="2"
                      rx="1"
                      transform="rotate(45 7.41422 6)"
                      fill="currentColor"
                    />
                  </svg>
                </span>
              </div>
            </div>
            <div className="modal-body">
              <Observer>
                {() => <PreviewComponents questions={this.state.soals} />}
              </Observer>
            </div>
            <div className="modal-footer">
              <button className="btn btn-light btn-sm" data-bs-dismiss="modal">
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

class Filter extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedStatus: null,
      selectedStatusPelaksanaan: null,
      selectedLevel: null,

      dataAkademi: [],
      selectedAkademi: 0,
      dataTema: [],
      selectedTema: 0,
      dataKategori: [],
      selectedKategori: 0,
    };
  }

  doFetchAkademi = async () => {
    try {
      const dataAkademi = await fetchAkademi();
      this.setState({
        dataAkademi: [{ value: 0, label: "Semua" }, ...dataAkademi],
      });
    } catch (err) {
      await Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  doFetchTema = async () => {
    try {
      const dataTema = await fetchTema(this.state.selectedAkademi);
      this.setState({ dataTema: [{ value: 0, label: "Semua" }, ...dataTema] });
    } catch (err) {
      await Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  doFetchKategori = async () => {
    try {
      const dataKategori = await fetchKategori();
      this.setState({
        dataKategori: [{ value: 0, label: "Semua" }, ...dataKategori],
      });
    } catch (err) {
      await Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  componentDidMount() {
    const { filter } = this.props || {};
    const { selectedStatus, selectedStatusPelaksanaan, selectedLevel } =
      filter || {};

    this.setState({ selectedStatus, selectedStatusPelaksanaan, selectedLevel });

    this.doFetchAkademi();
    this.doFetchKategori();
  }

  componentDidUpdate(prevProps) {
    const { filter: prevFilter } = prevProps || {};
    const {
      selectedStatus: prevSelectedStatus,
      selectedStatusPelaksanaan: prevSelectedStatusPelaksanaan,
      selectedLevel: prevSelectedLevel,
    } = prevFilter || {};
    const { filter } = this.props || {};
    const { selectedStatus, selectedStatusPelaksanaan, selectedLevel } =
      filter || {};

    if (prevSelectedStatusPelaksanaan != selectedStatusPelaksanaan) {
      this.setState({ selectedStatusPelaksanaan });
    }

    if (prevSelectedStatus != selectedStatus) {
      this.setState({ selectedStatus });
    }

    if (prevSelectedLevel != selectedLevel) {
      this.setState({ selectedLevel });
    }

    fixBootstrapDropdown();
  }

  render() {
    let {
      dataStatus,
      dataStatusPelaksanaan,
      dataLevel,
      setState = () => {},
    } = this.props || {};
    const { selectedStatusPelaksanaan, selectedStatus, selectedLevel } =
      this.state || {};

    dataStatus = [
      { value: 99, label: "Semua" },
      ...dataStatus.map(({ label }) => ({ value: label, label })),
    ];
    dataStatusPelaksanaan = [
      { value: 0, label: "Semua" },
      ...dataStatusPelaksanaan,
    ];
    dataLevel = [{ value: 99, label: "Semua" }, ...dataLevel];

    const [oselectedStatusPelaksanaan] = dataStatusPelaksanaan.filter(
      ({ value }) => value == selectedStatusPelaksanaan,
    );
    const [oselectedStatus] = dataStatus.filter(
      ({ value }) => value == selectedStatus,
    );
    const [oselectedLevel] = dataLevel.filter(
      ({ value }) => value == selectedLevel,
    );

    return (
      <div className="modal fade" tabindex="-1" id="modal_filter">
        <div className="modal-dialog modal-lg">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">
                <span className="svg-icon svg-icon-5 me-1">
                  <i className="bi bi-sliders text-black"></i>
                </span>
                Filter Data Test Substansi
              </h5>
              <div
                className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal"
                aria-label="Close"
              >
                <span className="svg-icon svg-icon-2x">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <rect
                      opacity="0.5"
                      x="6"
                      y="17.3137"
                      width="16"
                      height="2"
                      rx="1"
                      transform="rotate(-45 6 17.3137)"
                      fill="currentColor"
                    />
                    <rect
                      x="7.41422"
                      y="6"
                      width="16"
                      height="2"
                      rx="1"
                      transform="rotate(45 7.41422 6)"
                      fill="currentColor"
                    />
                  </svg>
                </span>
              </div>
            </div>
            {/* <form action="#" onSubmit={this.handleClickFilter}> */}
            <div className="modal-body">
              <div className="row">
                <div className="col-lg-12 mb-7 fv-row">
                  <label className="form-label">Targeting</label>
                  <Select
                    placeholder="Silahkan pilih"
                    value={oselectedLevel}
                    className="form-select-sm selectpicker p-0"
                    onChange={({ value }) =>
                      this.setState({ selectedLevel: value })
                    }
                    options={dataLevel}
                  />
                </div>
              </div>
              {/* <div className="row">
                <div className="col-lg-12 mb-7 fv-row">
                  <label className="form-label">Status Pelaksanaan</label>
                  <Select
                    placeholder="Silahkan pilih"
                    value={oselectedStatusPelaksanaan}
                    className="form-select-sm selectpicker p-0"
                    onChange={({ value }) =>
                      this.setState({ selectedStatusPelaksanaan: value })
                    }
                    options={dataStatusPelaksanaan}
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12 mb-7 fv-row">
                  <label className="form-label">Akademi</label>
                  <Select
                    placeholder="Silahkan pilih"
                    value={this.state.dataAkademi.find(a => a.value == this.state.selectedAkademi)}
                    className="form-select-sm selectpicker p-0"
                    onChange={({ value }) =>
                      this.setState({ selectedAkademi: value, dataTema: [], selectedTema: 0 }, () => this.doFetchTema())
                    }
                    options={this.state.dataAkademi}
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12 mb-7 fv-row">
                  <label className="form-label">Tema</label>
                  <Select
                    placeholder="Silahkan pilih"
                    value={this.state.dataTema.find(a => a.value == this.state.selectedTema)}
                    className="form-select-sm selectpicker p-0"
                    onChange={({ value }) =>
                      this.setState({ selectedTema: value })
                    }
                    options={this.state.dataTema}
                  />
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12 mb-7 fv-row">
                  <label className="form-label">Jenis Test Substansi</label>
                  <Select
                    placeholder="Silahkan pilih"
                    value={this.state.dataKategori.find(a => a.value == this.state.selectedKategori)}
                    className="form-select-sm selectpicker p-0"
                    onChange={({ value }) =>
                      this.setState({ selectedKategori: value })
                    }
                    options={this.state.dataKategori}
                  />
                </div>
              </div> */}
              <div className="row">
                <div className="col-lg-12 mb-7 fv-row">
                  <label className="form-label">Status</label>
                  <Select
                    placeholder="Silahkan pilih"
                    value={oselectedStatus}
                    className="form-select-sm selectpicker p-0"
                    onChange={({ value }) =>
                      this.setState({ selectedStatus: value })
                    }
                    options={dataStatus}
                  />
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <div className="d-flex justify-content-between">
                <button
                  type="reset"
                  className="btn btn-sm btn-light me-3"
                  onClick={() => {
                    setState({
                      selectedStatus: 99,
                      selectedStatusPelaksanaan: 0,
                      selectedLevel: 99,
                    });
                  }}
                >
                  Reset
                </button>
                <button
                  type="submit"
                  className="btn btn-sm btn-primary"
                  data-bs-dismiss="modal"
                  onClick={() => {
                    // setSelectedStatusPelaksanaan(this.state.selectedStatusPelaksanaan);
                    // setSelectedStatus(this.state.selectedStatus);
                    // setSelectedSLevel(this.state.selectedLevel);
                    setState({
                      selectedStatusPelaksanaan:
                        this.state.selectedStatusPelaksanaan,
                      selectedStatus: this.state.selectedStatus,
                      selectedLevel: this.state.selectedLevel,
                    });
                  }}
                >
                  Apply Filter
                </button>
              </div>
            </div>
            {/* </form> */}
          </div>
        </div>
      </div>
    );
  }
}

class Content extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      filter: {
        selectedStatusPelaksanaan: 0,
        selectedStatus: 99,
        selectedLevel: 99,
      },
      initialSearchKeyword: "",
      searchKeyword: 0,
      currentPage: 1,
      currentRowsPerPage: 10,
      sortBy: 1,
      sortDir: "DESC",
      data: [],
      headerData: {},
      totalData: 0,
      fetching: false,
      selectedIdToPreview: null,
      dataLevel: [],
      dataStatus: [],
      dataStatusPelaksanaan: [],
      loading: false,
    };
  }

  init = async () => {
    const [dataStatus, dataStatusPelaksanaan, dataLevel] = await Promise.all([
      fetchStatus(),
      fetchStatusPelaksanaan(),
      fetchLevel(),
    ]);

    this.setState({
      dataLevel,
      dataStatus,
      dataStatusPelaksanaan,
    });
  };

  fetch = async (state, prevState) => {
    let Data = [],
      Total = 0,
      dataLevel = [],
      dataStatus = [],
      dataStatusPelaksanaan = [];

    try {
      // Swal.fire({
      //   title: 'Mohon Tunggu!',
      //   icon: 'info',// add html attribute if you want or remove
      //   allowOutsideClick: false,
      //   didOpen: () => {
      //     Swal.showLoading();
      //   }
      // });

      let { searchKeyword: prevSearchKeyword } = prevState || {};
      let {
        currentPage,
        currentRowsPerPage,
        searchKeyword,
        filter,
        sortBy,
        sortDir,
      } = state || {};
      const { selectedStatus, selectedLevel, selectedStatusPelaksanaan } =
        filter || {};
      const sortFields = [
        "_",
        "id_substansi",
        "nama_substansi",
        "level",
        "category",
        "jml_pelatihan",
        "start_at",
        "jml_soal",
        "status_substansi",
      ];

      if (
        searchKeyword ||
        selectedStatus != 99 ||
        selectedLevel != 99 ||
        selectedStatusPelaksanaan != 0
      ) {
        sortBy = 2;
        sortDir = "asc";
      }

      this.setState({ loading: true });

      await this.init();

      [{ Data, Total }] = await Promise.all([
        fetchListTestSubstansi(
          (currentPage - 1) * currentRowsPerPage,
          currentRowsPerPage,
          searchKeyword || 0,
          selectedStatus,
          sortFields[sortBy],
          (sortDir || "").toUpperCase(),
          selectedLevel,
          selectedStatusPelaksanaan,
        ),
      ]);

      // Swal.close();
    } catch (err) {
      await Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }

    this.setState({
      data: Data,
      totalData: Total,
      loading: false,
    });
  };

  remove = async (id) => {
    const { isConfirmed = false } = await Swal.fire({
      title: "Apakah anda yakin ?",
      text: "Data ini tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, hapus!",
      cancelButtonText: "Tidak",
    });

    if (isConfirmed) {
      try {
        const Message = await removeTestSubstansiById(id);

        Swal.close();

        await Swal.fire({
          title: "Data berhasil dihapus",
          icon: "success",
          confirmButtonText: "Ok",
        });

        this.fetch(this.state);
      } catch (err) {
        await Swal.fire({
          title: err,
          icon: "warning",
          confirmButtonText: "Ok",
        });
      }
    }
  };

  componentDidMount() {
    this.fetch(this.state);
  }

  // fetchDateBatch = async () => {
  //   try {
  //     let dates = await Promise.all(
  //       this.state.data
  //         .filter(({ level }) => level == 2)
  //         .map(async ({ id_substansi }) => {
  //           const { header } = await fetchTestSubstansiHeaderById(id_substansi);
  //           let { category, id_akademi, id_pelatihan, theme_id } = header || {};
  //           if (category == "Test Substansi") category = 1;
  //           if (category == "Mid Test") category = 2;
  //           const [pel = {}] = await fetchListPelatihanByAkademiTema(
  //             category,
  //             id_akademi,
  //             theme_id,
  //             id_pelatihan
  //           );

  //           console.log(pel)
  //           return { ...pel, id_substansi };
  //         })
  //     );

  //     dates = dates.reduce((obj, d) => ({ ...obj, [d?.id_substansi]: d }), {});

  //     this.setState({ headerData: dates });
  //   } catch (err) {
  //     await Swal.fire({
  //       title: err,
  //       icon: "warning",
  //       confirmButtonText: "Ok",
  //     });
  //   }
  // };

  componentDidUpdate(prevProps, prevState) {
    const {
      searchKeyword: prevSearchKeyword,
      filter: prevFilter,
      currentPage: prevcurrentPage,
      currentRowsPerPage: prevcurrentRowsPerPage,
      sortBy: prevsortBy,
      sortDir: prevsortDir,
      data: prevData,
      headerData: prevHeaderData,
    } = prevState || {};
    const {
      selectedStatus: prevSelectedStatus,
      selectedLevel: prevSelectedLevel,
      selectedStatusPelaksanaan: prevSelectedStatusPelaksanaan,
    } = prevFilter || {};

    const {
      searchKeyword,
      filter,
      currentPage,
      currentRowsPerPage,
      sortBy,
      sortDir,
      data,
      headerData,
    } = this.state || {};
    const { selectedStatus, selectedLevel, selectedStatusPelaksanaan } =
      filter || {};

    // console.log(selectedStatus, selectedLevel, selectedStatusPelaksanaan)

    if (
      searchKeyword != prevSearchKeyword ||
      selectedStatus != prevSelectedStatus ||
      currentPage != prevcurrentPage ||
      currentRowsPerPage != prevcurrentRowsPerPage ||
      sortBy != prevsortBy ||
      sortDir != prevsortDir ||
      selectedLevel != prevSelectedLevel ||
      selectedStatusPelaksanaan != prevSelectedStatusPelaksanaan
    ) {
      this.fetch(this.state, prevState);
    }

    if (
      JSON.stringify(prevData.map(({ id_substansi }) => id_substansi)) !=
        JSON.stringify(data.map(({ id_substansi }) => id_substansi)) ||
      JSON.stringify(Object.keys(prevHeaderData)) !=
        JSON.stringify(Object.keys(headerData))
    ) {
      // this.fetchDateBatch();
    }
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <path
                        opacity="0.3"
                        d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                        fill="#7239ea"
                      ></path>
                      <path
                        d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                        fill="#7239ea"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Subvit
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Test Substansi
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                  data-bs-toggle="modal"
                  data-bs-target="#modal_filter"
                >
                  <i className="bi bi-sliders"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Filter
                  </span>
                </button>

                <button
                  className="btn btn-success fw-bolder btn-sm"
                  data-kt-menu-trigger="click"
                  data-kt-menu-placement="bottom-end"
                  data-kt-menu-flip="top-end"
                >
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kelola Test Substansi
                  </span>
                  <i className="bi bi-chevron-down ms-1"></i>
                </button>
                <div
                  className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-7 w-auto "
                  data-kt-menu="true"
                >
                  <div>
                    <div className="menu-item">
                      <a
                        href="/subvit/test-substansi/add"
                        className="menu-link px-5"
                      >
                        <i className="bi bi-plus-circle text-dark text-hover-primary me-1"></i>
                        Tambah Test Substansi
                      </a>
                    </div>
                    <div className="menu-item">
                      <a
                        href="/subvit/test-substansi/tipe-soal/list"
                        className="menu-link px-5"
                      >
                        <i className="bi bi-folder text-dark text-hover-primary me-1"></i>
                        Tipe Soal
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Daftar Test Substansi
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <div className="d-flex align-items-center position-relative my-1 me-2">
                            <span className="svg-icon svg-icon-1 position-absolute ms-6">
                              <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className="mh-50px"
                              >
                                <rect
                                  opacity="0.5"
                                  x="17.0365"
                                  y="15.1223"
                                  width="8.15546"
                                  height="2"
                                  rx="1"
                                  transform="rotate(45 17.0365 15.1223)"
                                  fill="currentColor"
                                ></rect>
                                <path
                                  d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                  fill="currentColor"
                                ></path>
                              </svg>
                            </span>
                            <input
                              type="text"
                              data-kt-user-table-filter="search"
                              className="form-control form-control-sm form-control-solid w-250px ps-14"
                              placeholder="Cari Test Substansi"
                              // onKeyPress={(e) => state.onSearchKeyPress(e)}
                              value={this.state.initialSearchKeyword || ""}
                              onChange={(e) => {
                                const keyword = e.target.value;
                                this.setState({
                                  initialSearchKeyword: keyword,
                                });

                                if (!keyword) {
                                  this.setState({
                                    searchKeyword: 0,
                                    currentPage: 1,
                                    filter: {
                                      selectedStatus: 99,
                                      selectedLevel: 99,
                                      selectedStatusPelaksanaan: 0,
                                    },
                                  });
                                  e.preventDefault();
                                }
                              }}
                              onKeyDown={(e) => {
                                const keyword = e.target.value;
                                if (e.key == "Enter") {
                                  this.setState({
                                    searchKeyword: keyword,
                                    currentPage: 1,
                                    filter: {
                                      selectedStatus: 99,
                                      selectedLevel: 99,
                                      selectedStatusPelaksanaan: 0,
                                    },
                                  });
                                  e.preventDefault();
                                }
                              }}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="table-responsive">
                          <Observer>
                            {() => {
                              return (
                                <DataTable
                                  columns={[
                                    {
                                      name: "No",
                                      sortable: false,
                                      center: true,
                                      width: "70px",
                                      grow: 1,
                                      cell: (row, index) =>
                                        this.state.currentRowsPerPage *
                                          (this.state.currentPage - 1) +
                                        index +
                                        1,
                                    },
                                    {
                                      name: "Nama Test Substansi",
                                      className: "min-w-300px mw-300px",
                                      width: "300px",
                                      sortable: true,
                                      grow: 6,
                                      wrap: true,
                                      allowOverflow: false,
                                      selector: ({
                                        nama_substansi = "-",
                                        category = "-",
                                        name: nama_akademi = "-",
                                      }) => (
                                        <>
                                          <label className="d-flex flex-stack my-2">
                                            <span className="d-flex align-items-center me-2">
                                              <span className="d-flex flex-column">
                                                <span className="text-muted fs-7 fw-semibold">
                                                  {capitalizeFirstLetter(
                                                    category || "-",
                                                  )}
                                                </span>
                                                <h6 className="fw-bolder fs-7 mb-1">
                                                  {capitalizeFirstLetter(
                                                    nama_substansi || "-",
                                                  )}
                                                </h6>
                                                <h6 className="text-muted fs-7 fw-semibold mb-1">
                                                  {capitalizeFirstLetter(
                                                    nama_akademi || "-",
                                                  )}
                                                </h6>
                                              </span>
                                            </span>
                                          </label>
                                        </>
                                      ),
                                    },
                                    {
                                      className: "min-w-220px mw-220px",
                                      sortable: true,
                                      name: "Targeting",
                                      width: "300px",
                                      grow: 6,
                                      wrap: true,
                                      allowOverflow: false,
                                      selector: ({
                                        level = "-",
                                        nama_level = "-",
                                      }) => {
                                        const [olevel] =
                                          this.state.dataLevel.filter(
                                            ({ value }) => value == level,
                                          );
                                        const { label = "-" } = olevel || {};

                                        return (
                                          <>
                                            <label className="d-flex flex-stack mb- mt-1">
                                              <span className="d-flex align-items-center me-2">
                                                <span className="d-flex flex-column">
                                                  <h6 className="fw-bolder fs-7 mb-1">
                                                    {capitalizeFirstLetter(
                                                      label || "-",
                                                    )}
                                                  </h6>
                                                  <h6 className="text-muted fs-7 fw-semibold">
                                                    {capitalizeFirstLetter(
                                                      nama_level || "-",
                                                    )}
                                                  </h6>
                                                </span>
                                              </span>
                                            </label>
                                          </>
                                        );
                                      },
                                    },
                                    {
                                      name: "Jadwal",
                                      sortable: true,
                                      className: "min-w-220px mw-220px",
                                      grow: 5,
                                      width: "250px",
                                      // width: "150px",
                                      selector: ({
                                        id_substansi,
                                        category = "",
                                        level = -1,
                                        tgl_mulai,
                                        tgl_selesai,
                                        start_at,
                                        end_at,
                                        status_substansi,
                                        jadwal_substansi_mulai,
                                        jadwal_substansi_akhir,
                                      }) => {
                                        if (level != 2) {
                                          return (
                                            <span className="badge badge-light-primary fs-7">
                                              Multiple Date
                                            </span>
                                          );
                                        }

                                        moment.locale("id");
                                        jadwal_substansi_mulai = moment(
                                          jadwal_substansi_mulai,
                                        );
                                        jadwal_substansi_akhir = moment(
                                          jadwal_substansi_akhir,
                                        );
                                        let badge = "",
                                          label = "-",
                                          start_end = `${jadwal_substansi_mulai.format(
                                            "DD MMM YYYY",
                                          )} - ${jadwal_substansi_akhir.format(
                                            "DD MMM YYYY",
                                          )}`;
                                        if (
                                          jadwal_substansi_mulai.isBefore(
                                            moment(),
                                          )
                                        ) {
                                          return (
                                            <span>
                                              <i className="bi bi-circle-fill fs-8 text-gray me-1"></i>
                                              {start_end}
                                            </span>
                                          );
                                        } else if (
                                          jadwal_substansi_akhir.isAfter(
                                            moment(),
                                          )
                                        ) {
                                          return (
                                            <span>
                                              <i className="bi bi-check-circle-fill fs-8 text-success me-1"></i>
                                              {start_end}
                                            </span>
                                          );
                                        } else {
                                          return (
                                            <span>
                                              <i className="bi bi-clock-fill fs-8 text-primary me-1"></i>
                                              {start_end}
                                            </span>
                                          );
                                        }

                                        // const header =
                                        //   this.state.headerData[id_substansi];
                                        // if (!header) return "-";

                                        // const {
                                        //   midtest_mulai,
                                        //   midtest_selesai,
                                        //   status_pelaksanaan_midsubstansi,
                                        //   status_pelaksanaan_substansi,
                                        //   subtansi_mulai,
                                        //   subtansi_selesai,
                                        // } = header || {};

                                        // let start_end = (
                                        //   <span className="badge badge-secondary fs-7 m-1">
                                        //     Multiple Date
                                        //   </span>
                                        // );
                                        // let badge = "";
                                        // let label = "-";

                                        // if (
                                        //   category.toLowerCase() ==
                                        //   "test substansi"
                                        // ) {
                                        //   start_end = `${subtansi_mulai} - ${subtansi_selesai}`;
                                        //   label = status_pelaksanaan_substansi;
                                        // } else if (
                                        //   category.toLowerCase() == "mid test"
                                        // ) {
                                        //   start_end = `${midtest_mulai} - ${midtest_selesai}`;
                                        //   label = status_pelaksanaan_midsubstansi;
                                        // }

                                        // if (
                                        //   label.toLowerCase() ==
                                        //   "belum dilaksanakan"
                                        // )
                                        //   badge = "danger";
                                        // else if (
                                        //   label.toLowerCase() ==
                                        //   "sedang dilaksanakan"
                                        // )
                                        //   badge = "primary";
                                        // else if (label.toLowerCase() == "selesai")
                                        //   badge = "success";

                                        // return (
                                        //   <div className="mt-2 text-center">
                                        //     <div>{start_end}</div>
                                        //     {status_substansi == 1 && (
                                        //       <div>
                                        //         <span
                                        //           className={
                                        //             "badge badge-light-" +
                                        //             badge +
                                        //             " fs-7 m-1"
                                        //           }
                                        //         >
                                        //           {label}
                                        //         </span>
                                        //       </div>
                                        //     )}
                                        //   </div>
                                        // );
                                      },
                                    },
                                    {
                                      name: "Bank Soal",
                                      sortable: true,
                                      grow: 4,
                                      selector: ({ jml_soal }) => {
                                        return jml_soal + " Soal";
                                      },
                                    },
                                    {
                                      name: "Status",
                                      sortable: true,
                                      grow: 3,
                                      selector: ({ status_substansi }) => {
                                        let badge = "";
                                        let label = "";
                                        if (status_substansi == 0) {
                                          badge = "warning";
                                          label = "Draft";
                                        } else if (status_substansi == 1) {
                                          badge = "success";
                                          label = "Publish";
                                        } else if (status_substansi == 2) {
                                          badge = "danger";
                                          label = "Batal";
                                        }

                                        return (
                                          <div>
                                            <span
                                              className={
                                                "badge badge-light-" +
                                                badge +
                                                " fs-7 m-1"
                                              }
                                            >
                                              {label}
                                            </span>
                                          </div>
                                        );
                                      },
                                    },
                                    {
                                      name: "Aksi",
                                      center: false,
                                      width: "200px",
                                      cell: (row) => {
                                        const {
                                          id_substansi: id,
                                          status_substansi,
                                        } = row || {};
                                        return (
                                          <div>
                                            <div className="dropdown d-inline">
                                              <button
                                                className="btn btn-primary fw-bolder btn-sm dropdown-toggle fs-7 me-2"
                                                type="button"
                                                data-bs-toggle="dropdown"
                                                aria-expanded="false"
                                              >
                                                <i className="bi bi-eye-fill text-white"></i>
                                                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                                  Detail
                                                </span>
                                              </button>
                                              <ul
                                                className="dropdown-menu"
                                                style={{
                                                  position: "absolute",
                                                  zIndex: "999999",
                                                }}
                                              >
                                                <li>
                                                  <a
                                                    href="#"
                                                    title="Preview Soal"
                                                    data-bs-toggle="modal"
                                                    data-bs-target="#preview_soal"
                                                    onClick={(e) =>
                                                      this.setState({
                                                        selectedIdToPreview: id,
                                                      })
                                                    }
                                                    className="dropdown-item px-5 my-1"
                                                  >
                                                    <i className="bi bi-eye text-dark text-hover-primary me-1"></i>
                                                    Preview
                                                  </a>
                                                </li>

                                                {row.unit_work_id ==
                                                  Cookies.get("satker_id") ||
                                                isExceptionRole() ? (
                                                  <>
                                                    <li>
                                                      <a
                                                        href={
                                                          "/subvit/test-substansi/soal/list/" +
                                                          id
                                                        }
                                                        title="Detail"
                                                        className={`dropdown-item px-5 my-1`}
                                                      >
                                                        <i className="bi bi-list-ul text-dark text-hover-primary me-1"></i>
                                                        Detail
                                                      </a>
                                                    </li>
                                                    <li>
                                                      <a
                                                        href={
                                                          "/subvit/test-substansi/edit/" +
                                                          id
                                                        }
                                                        title="Edit"
                                                        className={`dropdown-item px-5 my-1 ${
                                                          status_substansi == 2
                                                            ? "disabled"
                                                            : ""
                                                        }`}
                                                      >
                                                        <i className="bi bi-gear text-dark text-hover-primary me-1"></i>
                                                        Edit
                                                      </a>
                                                    </li>
                                                  </>
                                                ) : (
                                                  <>
                                                    <li>
                                                      <span
                                                        title="Detail"
                                                        className={`dropdown-item px-5 my-1 text-muted`}
                                                      >
                                                        <i className="bi bi-list-ul text-dark text-hover-primary me-1 text-muted"></i>
                                                        Detail
                                                      </span>
                                                    </li>
                                                    <li>
                                                      <span
                                                        title="Edit"
                                                        className={`dropdown-item px-5 my-1 text-muted ${
                                                          status_substansi == 2
                                                            ? "disabled"
                                                            : ""
                                                        }`}
                                                      >
                                                        <i className="bi bi-gear text-dark text-hover-primary me-1 text-muted"></i>
                                                        Edit
                                                      </span>
                                                    </li>
                                                  </>
                                                )}

                                                <li>
                                                  {" "}
                                                  <a
                                                    href={
                                                      "/subvit/test-substansi/report/" +
                                                      id
                                                    }
                                                    className="dropdown-item px-5"
                                                  >
                                                    <i className="bi bi-bar-chart text-dark text-hover-primary me-1"></i>
                                                    Report
                                                  </a>
                                                </li>
                                                <li>
                                                  {" "}
                                                  <a
                                                    href={
                                                      "/subvit/test-substansi/clone/" +
                                                      id
                                                    }
                                                    className="dropdown-item px-5 my-1"
                                                  >
                                                    <i className="bi bi-files text-dark text-hover-primary me-1"></i>
                                                    Clone
                                                  </a>
                                                </li>
                                              </ul>
                                            </div>

                                            {row.unit_work_id ==
                                              Cookies.get("satker_id") ||
                                            isExceptionRole() ? (
                                              <>
                                                <a
                                                  href="#"
                                                  onClick={(e) =>
                                                    this.remove(id)
                                                  }
                                                  title="Hapus"
                                                  className={`btn btn-icon btn-bg-danger btn-sm me-1 ${
                                                    status_substansi != 0
                                                      ? "disabled"
                                                      : ""
                                                  }`}
                                                >
                                                  <i className="bi bi-trash-fill text-white"></i>
                                                </a>
                                              </>
                                            ) : (
                                              <></>
                                            )}
                                          </div>
                                        );
                                      },
                                    },
                                  ]}
                                  data={this.state.data}
                                  progressPending={this.state.loading}
                                  highlightOnHover
                                  pointerOnHover
                                  pagination
                                  paginationServer
                                  sortServer
                                  paginationTotalRows={this.state.totalData}
                                  paginationComponentOptions={{
                                    selectAllRowsItem: true,
                                    selectAllRowsItemText: "Semua",
                                  }}
                                  onChangeRowsPerPage={(
                                    currentRowsPerPage,
                                    currentPage,
                                  ) => {
                                    this.setState({
                                      currentRowsPerPage,
                                      currentPage,
                                    });
                                    // this.fetch();
                                  }}
                                  onChangePage={(page, totalRows) => {
                                    this.setState({
                                      currentPage: page,
                                    });
                                    // this.fetch();
                                  }}
                                  onSort={({ id }, direction, rows) => {
                                    this.setState({
                                      sortBy: id,
                                      sortDir: direction,
                                    });
                                    // this.fetch();
                                  }}
                                  customStyles={{
                                    headCells: {
                                      style: {
                                        background: "rgb(243, 246, 249)",
                                      },
                                    },
                                  }}
                                  noDataComponent={
                                    <div className="mt-5">Tidak Ada Data</div>
                                  }
                                  persistTableHead={true}
                                />
                              );
                            }}
                          </Observer>
                        </div>
                        <RemotePreview id={this.state.selectedIdToPreview} />
                        <Filter
                          {...this.state}
                          // setSelectedStatusPelaksanaan={(v) => this.setState({ searchKeyword: null, filter: { selectedStatusPelaksanaan: v } })}
                          // setSelectedStatus={(v) => this.setState({ searchKeyword: null, filter: { selectedStatus: v } })}
                          // setSelectedSLevel={(v) => this.setState({ searchKeyword: null, filter: { selectedLevel: v } })}
                          dataStatus={this.state.dataStatus}
                          dataStatusPelaksanaan={
                            this.state.dataStatusPelaksanaan
                          }
                          dataLevel={this.state.dataLevel}
                          setState={(state) => {
                            this.setState({
                              searchKeyword: null,
                              filter: state,
                            });
                          }}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const AddTrivia = (props) => {
  return (
    <>
      <SideNav />
      <Header />
      <Content {...props} />
      <Footer />
    </>
  );
};

export default withRouter(AddTrivia);
