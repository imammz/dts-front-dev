import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../Report-Individu";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const TestSubstansiReportIndividu = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default TestSubstansiReportIndividu;
