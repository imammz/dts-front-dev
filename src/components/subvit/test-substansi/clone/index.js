import Cookies from "js-cookie";
import "line-awesome/dist/line-awesome/css/line-awesome.min.css";
import React from "react";
import Swal from "sweetalert2";
import Footer from "../../../Footer";
import Header from "../../../Header";
import SideNav from "../../../SideNav";
import {
  fetchAkademi,
  fetchKategori,
  fetchLevel,
  fetchListPelatihanByAkademiTema,
  fetchPelatihan,
  fetchStatus,
  fetchStatusPelatihan,
  fetchStatusSubstansi,
  fetchTema,
  fetchTestSubstansiById,
  fetchTipeSoal,
  tambahTestSubstansi,
  updateTestSubstansi,
} from "../components/actions";
import AddEditScreen, { Metode } from "../add/AddEditScreen";
import DraftPublishScreen from "../add/DraftPublishScreen";
import EditReorderScreen from "../add/EditReorderScreen";
import HeaderInfoScreen from "../add/HeaderInfoScreen";
import HeaderScreen from "../add/HeaderScreen";
import { PreviewModal } from "../components/preview-test-substansi";
import {
  resetErrorTestSubstansi,
  validateTestSubstansi,
  withRouter,
} from "../components/utils";

class Content extends React.Component {
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  constructor(props) {
    super(props);

    this.state = {
      fetchingAkademi: false,
      dataAkademi: [],
      fetchingKategori: false,
      dataKategori: [],
      fetchingTipeSoal: false,
      dataTipeSoal: [],

      nama: "Ini diabaikan",
      namaError: null,

      dataLevel: [],
      selectedLevel: null,
      selectedLevelError: null,

      selectedAkademi: null,
      selectedAkademiError: null,

      fetchingTema: false,
      dataTema: [],
      selectedTema: null,
      selectedTemaError: null,

      fetchingPelatihan: false,
      dataPelatihan: [],
      listPelatihanByAkademiTema: [],
      selectedPelatihan: null,
      selectedPelatihanInfo: null,
      selectedPelatihanError: null,

      selectedKategori: null,
      selectedKategoriError: null,

      selectedMetodeSoal: "entry",
      selectedEditSoalIdx: null,
      showAddEditScreen: false,

      mulaiPelaksanaan: null,
      mulaiPelaksanaanError: null,
      selesaiPelaksanaan: null,
      selesaiPelaksanaanError: null,
      jumlahSoal: null,
      jumlahSoalError: null,
      durasiDetik: null,
      durasiDetikError: null,
      passingGrade: null,
      passingGradeError: null,
      dataStatus: [],
      originalSelectedStatus: 0,
      selectedStatus: null,
      selectedStatusError: null,

      header: null,
      questions: [],
      selectedQuestions: [],
      selectedQuestionIds: [],

      questionsPreviewIdx: null,
      questionsPreview: [],
    };

    this.prevBtnRef = React.createRef();
    this.nextBtnRef = React.createRef();
  }

  init = async () => {
    if (Cookies.get("token") == null) {
      Swal.fire({
        title: "Unauthenticated.",
        text: "Silahkan login ulang",
        icon: "warning",
        confirmButtonText: "Ok",
      }).then((result) => {
        if (result.isConfirmed) {
          window.location = "/";
        }
      });
    }

    try {
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });

      const [dataLevel, dataStatus, dataTipeSoal, dataKategori, dataAkademi] =
        await Promise.all([
          fetchLevel(),
          fetchStatus(),
          fetchTipeSoal(1, "id", "DESC"),
          fetchKategori(),
          fetchAkademi(),
        ]);

      const { params } = this.props || {};
      const { id } = params || {};
      const { header, soals } = await fetchTestSubstansiById(id);

      const {
        category,
        duration,
        id_akademi = 0,
        id_pelatihan,
        id_substansi,
        jml_soal,
        nama_akademi,
        nama_pelatihan,
        nama_substansi,
        nama_tema,
        passing_grade,
        status_pelaksanaan,
        status_substansi,
        tgl_pelaksanaan,
        tgl_selesai_pelaksanaan,
        theme_id = 0,
      } = header || {};

      const [selectedAkademi] = dataAkademi.filter(
        ({ value }) => value == id_akademi,
      );

      let dataTema = [];
      let selectedTema = null;
      let dataPelatihan = [];
      let selectedPelatihan = null;
      let selectedPelatihanInfo = null;
      let selectedKategori = null;
      let selectedLevel = null;

      try {
        if (id_akademi) dataTema = await fetchTema(id_akademi);
      } catch (err) {}

      try {
        if (id_akademi && theme_id)
          dataPelatihan = await fetchPelatihan(id_akademi, theme_id);
      } catch (err) {}

      try {
        if (id_akademi && theme_id && id_pelatihan) {
          selectedPelatihanInfo = await fetchStatusPelatihan(
            id_akademi,
            theme_id,
            id_pelatihan,
          );
        }
      } catch (err) {}

      if (theme_id)
        [selectedTema] = dataTema.filter(({ value }) => value == theme_id);
      if (id_pelatihan)
        [selectedPelatihan] = dataPelatihan.filter(
          ({ value }) => value == id_pelatihan,
        );
      if (category)
        [selectedKategori] = dataKategori.filter(
          ({ label }) => label == category,
        );

      if (selectedPelatihan)
        [selectedLevel] = dataLevel.filter(({ value }) => value == 2);
      else if (selectedTema)
        [selectedLevel] = dataLevel.filter(({ value }) => value == 1);
      else if (selectedAkademi)
        [selectedLevel] = dataLevel.filter(({ value }) => value == 0);

      this.setState({
        dataLevel,
        dataTipeSoal,
        dataKategori,
        dataAkademi,
        dataTema,
        dataPelatihan,
        dataStatus,

        selectedPelatihanInfo,

        // nama: nama_substansi,
        // selectedLevel,
        // selectedAkademi,
        // selectedTema,
        // selectedPelatihan,
        // selectedKategori,

        // mulaiPelaksanaan: tgl_pelaksanaan,
        // selesaiPelaksanaan: tgl_selesai_pelaksanaan,
        // jumlahSoal: jml_soal,
        // durasiDetik: duration,
        // passingGrade: passing_grade,
        // originalSelectedStatus: status_substansi,
        // selectedStatus: status_substansi,

        header: header,
        questions: soals,
      });

      Swal.close();
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  componentDidUpdate(prevProps, prevState) {
    (async () => {
      const { value: prevSelectedKategori = 0 } =
        prevState.selectedKategori || {};
      const { value: prevSelectedAkademi = 0 } =
        prevState.selectedAkademi || {};
      const { value: prevSelectedTema = 0 } = prevState.selectedTema || {};
      const { value: prevSelectedPelatihan = 0 } =
        prevState.selectedPelatihan || {};

      const { value: selectedKategori = 0 } = this.state.selectedKategori || {};
      const { value: selectedAkademi = 0 } = this.state.selectedAkademi || {};
      const { value: selectedTema = 0 } = this.state.selectedTema || {};
      const { value: selectedPelatihan = 0 } =
        this.state.selectedPelatihan || {};

      if (
        prevSelectedKategori != selectedKategori ||
        prevSelectedAkademi != selectedAkademi ||
        prevSelectedTema != selectedTema ||
        prevSelectedPelatihan != selectedPelatihan
      ) {
        try {
          const listPelatihanByAkademiTema =
            await fetchListPelatihanByAkademiTema(
              selectedKategori,
              selectedAkademi,
              selectedTema,
              selectedPelatihan,
            );
          this.setState({ listPelatihanByAkademiTema });
        } catch (err) {}
      }

      if (prevSelectedPelatihan != selectedPelatihan) {
        this.setState({ selectedKategori: null });
      }
    })();
  }

  resetError = () => {
    return {
      namaError: null,
      selectedLevelError: null,
      selectedAkademiError: null,
      selectedTemaError: null,
      selectedPelatihanError: null,
      selectedKategoriError: null,
      mulaiPelaksanaanError: null,
      selesaiPelaksanaanError: null,
      jumlahSoalError: null,
      durasiDetikError: null,
      passingGradeError: null,
      selectedStatusError: null,
    };
  };

  save = async () => {
    try {
      const { history } = this.props || {};

      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });

      await tambahTestSubstansi(this.state);

      Swal.close();

      await Swal.fire({
        title: "Test substansi berhasil diclone",
        icon: "success",
        confirmButtonText: "Ok",
      });

      window.location.href = "/subvit/test-substansi";
    } catch (err) {
      await Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  onAkademiChange = async (v) => {
    let dataTema = [];
    // let listPelatihanByAkademiTema = [];

    try {
      const { value: valueLevel } = this.state.selectedLevel || {};
      const { value } = v || {};

      if (valueLevel > 0) dataTema = await fetchTema(value);
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }

    this.setState({
      selectedAkademi: v,
      dataTema,
      selectedTema: null,
      dataPelatihan: [],
      selectedPelatihan: null,
      // listPelatihanByAkademiTema
    });
  };

  onTemaChange = async (v) => {
    let dataPelatihan = [];
    // let listPelatihanByAkademiTema = [];

    try {
      const { value: valueLevel } = this.state.selectedLevel || {};
      const { value: valueAkademi } = this.state.selectedAkademi || {};
      const { value } = v || {};

      if (valueLevel > 1)
        dataPelatihan = await fetchPelatihan(valueAkademi, value);
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }

    this.setState({
      selectedTema: v,
      dataPelatihan,
      selectedPelatihan: null,
      // listPelatihanByAkademiTema
    });
  };

  onPelatihanChange = async (v) => {
    const { value: selectedLevel } = this.state.selectedLevel || {};
    const { value: selectedAkademi = -1 } = this.state.selectedAkademi || {};
    const { value: selectedTema = -1 } = this.state.selectedTema || {};
    const { value: selectedPelatihan = -1 } = v || {};

    const [selectedPelatihanInfo, selectedSubstansiInfo] = await Promise.all([
      fetchStatusPelatihan(selectedAkademi, selectedTema, selectedPelatihan),
      fetchStatusSubstansi(selectedAkademi, selectedTema, selectedPelatihan),
    ]);

    this.setState({
      selectedPelatihan: v,
      selectedPelatihanInfo: {
        ...selectedPelatihanInfo,
        ...selectedSubstansiInfo,
      },
      // listPelatihanByAkademiTema
    });
  };

  onKategoriChange = (v) => {
    this.setState({
      selectedKategori: v,
    });
  };

  onMetodeSoalChange = (v) => {
    this.setState({
      selectedMetodeSoal: v,
    });
  };

  removeQuestion = async (idx) => {
    const { isConfirmed = false } = await Swal.fire({
      title: "Apakah anda yakin ?",
      text: "Data ini tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, hapus!",
      cancelButtonText: "Tidak",
    });

    if (isConfirmed) {
      let questions = this.state.questions;
      questions.splice(idx, 1);
      this.setState({ questions });

      await Swal.fire({
        title: "Soal berhasil dihapus",
        icon: "success",
        confirmButtonText: "Ok",
      });
    }
  };

  removeSelectedQuestion = async () => {
    const { isConfirmed = false } = await Swal.fire({
      title: "Apakah anda yakin ?",
      text: "Data ini tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, hapus!",
      cancelButtonText: "Tidak",
    });

    if (isConfirmed) {
      let selectedQuestionsId = this.state.selectedQuestions.map(
        ({ id }) => id,
      );
      let questions = this.state.questions.filter(
        ({ id }) => !selectedQuestionsId.includes(id),
      );

      this.setState({ questions });

      await Swal.fire({
        title: "Soal berhasil dihapus",
        icon: "success",
        confirmButtonText: "Ok",
      });
    }
  };

  showPreview = (idx, questions) => {
    this.setState({
      questionsPreviewIdx: idx,
      questionsPreview: [...questions],
    });
  };

  componentDidMount() {
    this.init();
  }

  // componentDidUpdate(_, prevState) {
  //   const {questions: prevQuestions} = prevState || {};
  //   const {questions} = this.state || {};

  //   if (JSON.stringify(prevQuestions) != JSON.stringify(questions)) {
  //     this.setState({
  //       // selectedQuestions: [...questions],
  //       selectedQuestionIds: questions.map(({id}) => id)
  //     })
  //   }
  // }

  render() {
    const { value: selectedLevelValue = -1 } = this.state.selectedLevel || {};
    const {
      midtest_mulai = null,
      midtest_selesai = null,
      subtansi_mulai = null,
      subtansi_selesai = null,
    } = this.state.selectedPelatihanInfo || {};

    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <path
                        opacity="0.3"
                        d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                        fill="#7239ea"
                      ></path>
                      <path
                        d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                        fill="#7239ea"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Subvit
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Test Substansi
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={() =>
                    this?.props?.history && this?.props?.history(-1)
                  }
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div
                    className="stepper stepper-links d-flex flex-column"
                    id="kt_subvit_trivia"
                  >
                    <div className="col-lg-12 mt-7">
                      <div className="card border">
                        <div className="card-header">
                          <div className="card-title">
                            <div className="stepper-nav flex-wrap mb-n4">
                              <div
                                className="stepper-item ms-0 me-3 my-2 current"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label ms-0">
                                    <h3 className="stepper-title fs-6">
                                      Clone Test Substansi
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Bank Soal
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Publish
                                    </h3>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="card-body">
                          <div className="highlight bg-light-primary my-5">
                            <div className="col-lg-12 mb-7 fv-row text-primary">
                              <h5 className="text-primary fs-5">Panduan</h5>
                              <p className="text-primary">
                                Sebelum membuat Tes Substansi, mohon untuk
                                membaca panduan berikut :
                              </p>
                              <ul>
                                <li>
                                  Pelaksanaan Level Akademi : Berlaku untuk{" "}
                                  <strong>SEMUA</strong> Pelatihan pada akademi
                                  terpilih, setiap pelatihan pada akademi
                                  tersebut maupun yang baru dibuat kemudian akan
                                  otomatis merujuk kepada Tes Substansi
                                  tersebut.
                                </li>
                                <li>
                                  Pelaksanaan Level Tema : Berlaku untuk{" "}
                                  <strong>SEMUA</strong> Pelatihan pada tema
                                  terpilih, setiap pelatihan pada tema tersebut
                                  maupun yang baru dibuat kemudian akan otomatis
                                  merujuk kepada Tes Substansi tersebut.
                                </li>
                                <li>
                                  Pelaksanaan Level Pelatihan : Berlaku{" "}
                                  <strong>HANYA</strong> pada pelatihan yang
                                  terpilih
                                </li>
                              </ul>
                            </div>
                          </div>
                          <form
                            action="#"
                            id="kt_subvit_trivia_form"
                            method="post"
                            onSubmit={async (e) => {
                              e.preventDefault();

                              const error = validateTestSubstansi({
                                ...this.state,
                                questions: this.state.selectedQuestions,
                              });

                              if (Object.keys(error).length > 0) {
                                this.setState({
                                  ...resetErrorTestSubstansi(),
                                  ...error,
                                });

                                Swal.fire({
                                  title:
                                    "Masih terdapat isian yang belum lengkap",
                                  icon: "warning",
                                  confirmButtonText: "Ok",
                                });
                              } else {
                                Swal.fire({
                                  title: "Mohon Tunggu!",
                                  icon: "info", // add html attribute if you want or remove
                                  allowOutsideClick: false,
                                  didOpen: () => {
                                    Swal.showLoading();
                                  },
                                });

                                await this.save();
                              }
                            }}
                          >
                            <div
                              className="current"
                              data-kt-stepper-element="content"
                            >
                              <div className="col-lg-12 fv-row">
                                <h5 className="mt-7 text-muted me-3 mb-5">
                                  Asal Clone Test Substansi
                                </h5>
                                <div className="row">
                                  <HeaderInfoScreen
                                    data={{ header: this.state.header }}
                                  />
                                </div>
                                <div>
                                  <div className="mt-5 border-top mx-0 my-10"></div>
                                </div>
                                <h5 className="mt-7 text-muted me-3 mb-5">
                                  Tujuan Clone
                                </h5>
                                <div className="row">
                                  <HeaderScreen
                                    data={{
                                      nama: this.state.nama,
                                      setNama: (v) =>
                                        this.setState({ nama: v }),
                                      namaError: this.state.namaError,
                                      dataLevel: this.state.dataLevel,
                                      selectedLevel: this.state.selectedLevel,
                                      onLevelChange: (v) =>
                                        this.setState({ selectedLevel: v }),
                                      selectedLevelError:
                                        this.state.selectedLevelError,
                                      fetchingAkademi:
                                        this.state.fetchingAkademi,
                                      dataAkademi: this.state.dataAkademi,
                                      selectedAkademi:
                                        this.state.selectedAkademi,
                                      onAkademiChange: this.onAkademiChange,
                                      selectedAkademiError:
                                        this.state.selectedAkademiError,
                                      fetchingTema: this.state.fetchingTema,
                                      dataTema: this.state.dataTema,
                                      selectedTema: this.state.selectedTema,
                                      onTemaChange: this.onTemaChange,
                                      selectedTemaError:
                                        this.state.selectedTemaError,
                                      fetchingPelatihan:
                                        this.state.fetchingPelatihan,
                                      dataPelatihan:
                                        this.state.dataPelatihan.map(
                                          ({
                                            nama_unit_kerja,
                                            pid,
                                            nama,
                                            wilayah,
                                            batch = 1,
                                            ...rest
                                          }) => ({
                                            ...rest,
                                            value: pid,
                                            label: `[${nama_unit_kerja}] - ${
                                              this.state.selectedAkademi?.slug
                                            }${pid} - ${nama} Batch ${batch} - ${
                                              wilayah || "Online"
                                            }`,
                                          }),
                                        ),
                                      selectedPelatihan:
                                        this.state.selectedPelatihan,
                                      selectedPelatihanInfo:
                                        this.state.selectedPelatihanInfo,
                                      onPelatihanChange: this.onPelatihanChange,
                                      selectedPelatihanError:
                                        this.state.selectedPelatihanError,
                                      dataKategori: this.state.dataKategori,
                                      selectedKategori:
                                        this.state.selectedKategori,
                                      onKategoriChange: this.onKategoriChange,
                                      kategoriOptionDisabled: ({ value }) => {
                                        if (selectedLevelValue != 2)
                                          return false;
                                        if (
                                          value == 1 &&
                                          !!subtansi_mulai &&
                                          !!subtansi_selesai
                                        )
                                          return false;
                                        if (
                                          value == 2 &&
                                          !!midtest_mulai &&
                                          !!midtest_selesai
                                        )
                                          return false;
                                        return true;
                                      },
                                      selectedKategoriError:
                                        this.state.selectedKategoriError,
                                      isMetodeSoalEntry: () =>
                                        this.state.selectedMetodeSoal ==
                                        "entry",
                                      isMetodeSoalImport: () =>
                                        this.state.selectedMetodeSoal ==
                                        "import",
                                      onMetodeSoalChange:
                                        this.onMetodeSoalChange,
                                    }}
                                  />
                                </div>
                              </div>
                            </div>

                            <div data-kt-stepper-element="content">
                              <div className="col-lg-12 fv-row">
                                {this.state.showAddEditScreen ? (
                                  <AddEditScreen
                                    data={{
                                      dataTipeSoal: this.state.dataTipeSoal,
                                      isMetodeSoalEntry: () =>
                                        this.state.selectedMetodeSoal ==
                                        "entry",
                                      isMetodeSoalImport: () =>
                                        this.state.selectedMetodeSoal ==
                                        "import",
                                      onMetodeSoalChange:
                                        this.onMetodeSoalChange,
                                      addQuestions: (v) => {
                                        let questions = this.state.questions;
                                        let selectedQuestions =
                                          this.state.selectedQuestions;
                                        let selectedQuestionIds =
                                          this.state.selectedQuestionIds;
                                        (v || []).forEach((q, idx) => {
                                          const id =
                                            Math.max.apply(
                                              null,
                                              selectedQuestionIds,
                                            ) +
                                            idx +
                                            1;

                                          q = { ...q, id };
                                          questions = [...questions, q];
                                          selectedQuestions = [
                                            ...selectedQuestions,
                                            q,
                                          ];
                                          selectedQuestionIds = [
                                            ...selectedQuestionIds,
                                            id,
                                          ];
                                        });

                                        this.setState({
                                          questions,
                                          selectedQuestions,
                                          showAddEditScreen: false,
                                          selectedQuestionIds,
                                        });
                                      },
                                      updateQuestion: (idx, v) => {
                                        const newquestions =
                                          this.state.questions;
                                        newquestions[idx] = { ...v };
                                        this.setState({
                                          questions: [...newquestions],
                                          showAddEditScreen: false,
                                        });
                                      },
                                      setSelectedEditSoal: (idx) =>
                                        this.setState({
                                          selectedEditSoalIdx: idx,
                                        }),
                                      selectedEditSoalIdx:
                                        this.state.selectedEditSoalIdx,
                                      questions: this.state.questions,
                                      // prevBtnRef: this.prevBtnRef,
                                      // nextBtnRef: this.nextBtnRef,
                                      showBackButton: true,
                                      onBackButtonClick: () =>
                                        this.setState({
                                          showAddEditScreen: false,
                                        }),
                                      showPreviewButton: true,
                                      showPreview: this.showPreview,
                                    }}
                                  />
                                ) : (
                                  <EditReorderScreen
                                    data={{
                                      questions: this.state.questions,
                                      selectedQuestions:
                                        this.state.selectedQuestions,
                                      selectedQuestionIds:
                                        this.state.selectedQuestionIds,
                                      setSelectedEditSoal: (idx) =>
                                        this.setState({
                                          selectedEditSoalIdx: idx,
                                          showAddEditScreen: true,
                                        }),
                                      showPreview: this.showPreview,
                                      removeQuestion: this.removeQuestion,
                                      // prevBtnRef: this.prevBtnRef,
                                      // nextBtnRef: this.nextBtnRef,
                                      showImportButton: true,
                                      onMetodeSoalChange:
                                        this.onMetodeSoalChange,
                                      showRemoveAllButton: true,
                                      onRemoveAllItem: () => {
                                        this.removeSelectedQuestion();
                                      },
                                      selectable: true,
                                      onSelectedChange: (rows) =>
                                        this.setState({
                                          selectedQuestions: rows,
                                          selectedQuestionIds: rows.map(
                                            ({ id }) => id,
                                          ),
                                        }),
                                    }}
                                  />
                                )}
                              </div>
                            </div>

                            <div data-kt-stepper-element="content">
                              <DraftPublishScreen
                                data={{
                                  questions: this.state.questions,
                                  selectedPelatihanInfo:
                                    this.state.selectedPelatihanInfo,
                                  mulaiPelaksanaan: this.state.mulaiPelaksanaan,
                                  listPelatihanByAkademiTema:
                                    this.state.listPelatihanByAkademiTema,
                                  selectedKategori: this.state.selectedKategori,
                                  mulaiPelaksanaanError:
                                    this.state.mulaiPelaksanaanError,
                                  selesaiPelaksanaan:
                                    this.state.selesaiPelaksanaan,
                                  selesaiPelaksanaanError:
                                    this.state.selesaiPelaksanaanError,
                                  jumlahSoal: this.state.jumlahSoal,
                                  jumlahSoalError: this.state.jumlahSoalError,
                                  durasiDetik: this.state.durasiDetik,
                                  durasiDetikError: this.state.durasiDetikError,
                                  passingGrade: this.state.passingGrade,
                                  passingGradeError:
                                    this.state.passingGradeError,
                                  dataStatus: this.state.dataStatus.map(
                                    (s) => ({
                                      ...s,
                                      disabled:
                                        s.value <
                                        this.state.originalSelectedStatus,
                                    }),
                                  ),
                                  selectedStatus: this.state.selectedStatus,
                                  selectedStatusError:
                                    this.state.selectedStatusError,
                                  updateState: (k, v) => {
                                    this.setState({ [k]: v });
                                  },
                                }}
                              />
                            </div>

                            <div className="text-center border-top pt-10 my-7">
                              <button
                                className="btn btn-light btn-md me-3 mr-2"
                                data-kt-stepper-action="previous"
                                ref={this.prevBtnRef}
                              >
                                <i className="fa fa-chevron-left me-1"></i>
                                Sebelumnya
                              </button>
                              <button
                                type="submit"
                                className="btn btn-primary btn-md"
                                data-kt-stepper-action="submit"
                              >
                                <i className="fa fa-paper-plane ms-1"></i>Simpan
                              </button>
                              <button
                                type="button"
                                className="btn btn-primary btn-md"
                                data-kt-stepper-action="next"
                                ref={this.nextBtnRef}
                              >
                                <i className="fa fa-chevron-right"></i>Lanjutkan
                              </button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <PreviewModal
          questionIdx={this.state.questionsPreviewIdx}
          questions={this.state.questionsPreview}
        />
      </div>
    );
  }
}

const AddTrivia = (props) => {
  return (
    <>
      <SideNav />
      <Header />
      <Content {...props} />
      <Footer />
    </>
  );
};

export default withRouter(AddTrivia);
