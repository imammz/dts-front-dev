import axios from "axios";
import Cookies from "js-cookie";
import "line-awesome/dist/line-awesome/css/line-awesome.min.css";
import moment from "moment";
import React from "react";
import Swal from "sweetalert2";
import { withExitPrompt } from "../../../../utils/useExitPrompt";
import Footer from "../../../Footer";
import Header from "../../../Header";
import SideNav from "../../../SideNav";
import {
  fetchAkademi,
  fetchKategori,
  fetchLevel,
  fetchListPelatihanByAkademiTema,
  fetchStatus,
  fetchStatusPelatihan,
  fetchStatusSubstansi,
  fetchTema,
  fetchTipeSoal,
  tambahTestSubstansi,
} from "../components/actions";
import { PreviewModal } from "../components/preview-test-substansi";
import {
  letters,
  resetErrorHeaderScreen,
  resetErrorPublishScreen,
  resetErrorTestSubstansi,
  validateHeaderScreen,
  validatePublishScreen,
  validateTestSubstansi,
  withRouter,
} from "../components/utils";
import AddEditScreen, { Metode } from "./AddEditScreen";
import DraftPublishScreen from "./DraftPublishScreen";
import EditReorderScreen from "./EditReorderScreen";
import HeaderScreen from "./HeaderScreen";
import template from "../Template_Soal_Test_Substansi.xlsx";
import { isExceptionRole, isSuperAdmin } from "../../../AksesHelper";

const validate = (test, updateState) => {
  const {
    nama,
    selectedLevel,
    selectedAkademi,
    selectedTema,
    selectedPelatihan,
    selectedKategori,
    mulaiPelaksanaan,
    selesaiPelaksanaan,
    jumlahSoal,
    durasiDetik,
    passingGrade,
    selectedStatus,
    questions,
  } = test || {};
  let isValid = true;

  if (!nama) {
    updateState("namaError", "Nama tidak boleh kosong");
    isValid = false;
  }

  if (!selectedLevel) {
    updateState("selectedLevelError", "Level harus terpilih salah satu");
    isValid = false;
  }

  if (!selectedAkademi) {
    updateState("selectedAkademiError", "Akademi harus terpilih salah satu");
    isValid = false;
  }

  const { value: selectedLevelValue } = selectedLevel || {};
  if (selectedLevelValue > 0 && !selectedTema) {
    updateState("selectedTemaError", "Tema harus terpilih salah satu");
    isValid = false;
  }

  if (selectedLevelValue > 1 && !selectedPelatihan) {
    updateState(
      "selectedPelatihanError",
      "Pelatihan harus terpilih salah satu",
    );
    isValid = false;
  }

  if (!selectedKategori) {
    updateState(
      "selectedKategoriError",
      "Kategori soal harus terpilih salah satu",
    );
    isValid = false;
  }

  if (!mulaiPelaksanaan) {
    updateState(
      "mulaiPelaksanaanError",
      "Mulai pelaksanaan tidak boleh kosong",
    );
    isValid = false;
  }

  if (!selesaiPelaksanaan) {
    updateState(
      "selesaiPelaksanaanError",
      "Selesai pelaksanaan tidak boleh kosong",
    );
    isValid = false;
  }

  if ((selesaiPelaksanaan || 0) < (mulaiPelaksanaan || 0)) {
    updateState(
      "selesaiPelaksanaanError",
      "Pelaksanaan sampai harus setelah pelaksanaan dari",
    );
    isValid = false;
  }

  if (!jumlahSoal) {
    updateState("jumlahSoalError", "Jumlah soal tidak boleh kosong");
    isValid = false;
  } else if (jumlahSoal > (questions || []).length) {
    updateState(
      "jumlahSoalError",
      "Jumlah soal tidak boleh melebihi bank soal",
    );
    isValid = false;
  }

  if (!durasiDetik) {
    updateState("durasiDetikError", "Durasi pengerjaan tidak boleh kosong");
    isValid = false;
  }

  if (!passingGrade) {
    updateState("passingGradeError", "Passing grade tidak boleh kosong");
    isValid = false;
  }

  if (selectedStatus == null) {
    updateState("selectedStatusError", "Status harus terpilih salah satu");
    isValid = false;
  }

  return isValid;
};

class Content extends React.Component {
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  constructor(props) {
    super(props);

    this.state = {
      fetchingAkademi: false,
      dataAkademi: [],
      fetchingKategori: false,
      dataKategori: [],
      fetchingTipeSoal: false,
      dataTipeSoal: [],

      step: 1,
      preventNext: true,

      nama: "Ini diabaikan",
      namaError: null,

      dataLevel: [
        { value: 0, label: "Akademi" },
        { value: 1, label: "Tema" },
        { value: 2, label: "Pelatihan" },
      ],
      selectedLevel: null,
      selectedLevelError: null,

      selectedAkademi: null,
      selectedAkademiError: null,

      fetchingTema: false,
      dataTema: [],
      selectedTema: null,
      selectedTemaError: null,

      fetchingPelatihan: false,
      dataPelatihan: [],
      selectedPelatihan: null,
      selectedPelatihanInfo: null,
      listPelatihanByAkademiTema: [],
      selectedPelatihanError: null,

      selectedKategori: null,
      selectedKategoriError: null,

      selectedMetodeSoal: "entry",
      selectedEditSoalIdx: null,

      mulaiPelaksanaan: null,
      mulaiPelaksanaanError: null,
      selesaiPelaksanaan: null,
      selesaiPelaksanaanError: null,
      jumlahSoal: null,
      jumlahSoalError: null,
      durasiDetik: null,
      durasiDetikError: null,
      passingGrade: null,
      passingGradeError: null,
      selectedStatus: null,
      selectedStatusError: null,

      questions: [],

      questionsPreviewIdx: null,
      questionsPreview: [],
    };

    this.prevBtnRef = React.createRef();
    this.submitBtnRef = React.createRef();
    this.nextBtnRef = React.createRef();
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.state.nama != prevState?.nama ||
      this.state.selectedLevel != prevState?.selectedLevel ||
      this.state.selectedAkademi != prevState?.selectedAkademi ||
      this.state.selectedTema != prevState?.selectedTema ||
      this.state.selectedPelatihan != prevState?.selectedPelatihan ||
      this.state.selectedKategori != prevState?.selectedKategori ||
      this.state.questions != prevState?.questions ||
      this.state.jumlahSoal != prevState?.jumlahSoal ||
      this.state.durasiDetik != prevState?.durasiDetik ||
      this.state.passingGrade != prevState?.passingGrade ||
      this.state.selectedStatus != prevState?.selectedStatus
    )
      this.props.setShowExitPrompt(true);

    (async () => {
      const { value: prevSelectedKategori = 0 } =
        prevState.selectedKategori || {};
      const { value: prevSelectedAkademi = 0 } =
        prevState.selectedAkademi || {};
      const { value: prevSelectedTema = 0 } = prevState.selectedTema || {};
      const { value: prevSelectedPelatihan = 0 } =
        prevState.selectedPelatihan || {};

      const { value: selectedKategori = 0 } = this.state.selectedKategori || {};
      const { value: selectedAkademi = 0 } = this.state.selectedAkademi || {};
      const { value: selectedTema = 0 } = this.state.selectedTema || {};
      const { value: selectedPelatihan = 0 } =
        this.state.selectedPelatihan || {};

      if (
        prevSelectedKategori != selectedKategori ||
        prevSelectedAkademi != selectedAkademi ||
        prevSelectedTema != selectedTema ||
        prevSelectedPelatihan != selectedPelatihan
      ) {
        try {
          const listPelatihanByAkademiTema =
            await fetchListPelatihanByAkademiTema(
              selectedKategori,
              selectedAkademi,
              selectedTema,
              selectedPelatihan,
            );
          this.setState({ listPelatihanByAkademiTema });
        } catch (err) {}
      }

      if (prevSelectedPelatihan != selectedPelatihan) {
        this.setState({ selectedKategori: null });
      }
    })();
  }

  fetchAkademi = async () => {
    this.setState({
      fetchingAkademi: true,
      fetchingKategori: true,
      fetchingTipeSoal: true,
    });

    var formdata = new FormData();
    formdata.append("length", 1000);
    formdata.append("param", "");
    formdata.append("sort", "created_at");
    formdata.append("sort_val", "desc");
    formdata.append("start", 0);
    formdata.append("status", "99");
    formdata.append("tahun", "0");

    await Promise.all([
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/list-akademi-s3",
          formdata,
          this.configs,
        )
        .then((res) => {
          const { result } = res.data || {};
          const { Data = [] } = result || {};

          const dataAkademi = Data.map(({ id, name, ...rest }) => ({
            value: id,
            label: name,
            ...rest,
          }));
          this.setState({ dataAkademi: dataAkademi });
        }),
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/umum/list-kategori",
          {},
          this.configs,
        )
        .then((res) => {
          const { data } = res || {};
          const { result } = res.data;
          const { Data = [], Status = false, Message = "" } = result || {};

          const dataKategori = Data.map(({ id, name }) => ({
            value: id,
            label: name,
          }));
          this.setState({ dataKategori: dataKategori });
        }),
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/tessubstansi/list-tipesoal",
          { start: 0, length: 200, status: 0 },
          this.configs,
        )
        .then((res) => {
          const { data } = res || {};
          const { result } = res.data;
          const { Data = [], Status = false, Message = "" } = result || {};

          const dataTipeSoal = Data.map(({ id, name }) => ({
            value: id,
            label: name,
          }));
          this.setState({ dataTipeSoal: dataTipeSoal });
        }),
    ]);

    Swal.close();

    this.setState({
      fetchingAkademi: false,
      fetchingKategori: false,
      fetchingTipeSoal: false,
    });
  };

  fetchTema = async (selectedAkademi) => {
    this.setState({
      fetchingTema: true,
      dataTema: [],
    });

    const { value } = selectedAkademi || {};

    let dataTemaAlt = [];
    try {
      dataTemaAlt = await fetchTema(value);
    } catch (err) {}

    // const dataBody = { start: 0, rows: 100, akademi_id: value, status: "null" };
    const dataBody = {
      start: 0,
      rows: 100,
      id_akademi: value,
      status: "null",
      cari: 0,
      sort: "name",
      sort_val: "DESC",
    };

    await axios
      .post(
        isExceptionRole()
          ? process.env.REACT_APP_BASE_API_URI + "/list-tema-filter-substansi"
          : process.env.REACT_APP_BASE_API_URI + "/list-tema-filter-substansi",
        dataBody,
        this.configs,
      )
      .then((res) => {
        const { result } = res.data;
        const { Data = [] } = result || {};

        const dataTema = Data.map(({ id, name, ...rest }) => ({
          value: id,
          label: name,
          ...rest,
          ...(dataTemaAlt.find((a) => a?.value == id) || {}),
        }));

        this.setState({
          dataTema: dataTema,
        });
      })
      .catch((error) => {
        const { response } = error || {};
        const { data } = response || {};
        const { result } = data || {};
        const { Message = "" } = result || {};

        Swal.fire({
          title: Message,
          icon: "warning",
          confirmButtonText: "Ok",
        });
      });

    this.setState({
      fetchingTema: false,
    });
  };

  fetchPelatihan = async (selectedTema) => {
    this.setState({
      fetchingPelatihan: true,
      dataPelatihan: [],
    });

    const { value: akademiId } = this.state.selectedAkademi || {};
    const { value: temaId } = selectedTema || {};

    const dataBody = {
      jns_param: 3,
      akademi_id: akademiId,
      theme_id: temaId,
      pelatihan_id: 0,
    };
    await axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/survey/combo_akademi_pelatihan",
        dataBody,
        this.configs,
      )
      .then((res) => {
        const { result } = res.data;
        const { Data = [] } = result || {};

        const dataPelatihan = Data;
        this.setState({
          dataPelatihan: dataPelatihan,
        });
      })
      .catch((error) => {
        const { response } = error || {};
        const { data } = response || {};
        const { result } = data || {};
        const { Message = "" } = result || {};

        Swal.fire({
          title: Message,
          icon: "warning",
          confirmButtonText: "Ok",
        });
      });

    this.setState({
      fetchingPelatihan: false,
    });
  };

  onLevelChange = (v) => {
    // const { value } = v || {};

    this.setState({
      selectedLevel: v,
      // selectedAkademi: value > -1 ? this.state.selectedAkademi : null,
      // selectedTema: value > 0 ? this.state.selectedTema : null,
      // selectedPelatihan: value > 1 ? this.state.selectedPelatihan : null,
      selectedAkademi: null,
      selectedTema: null,
      selectedPelatihan: null,
    });
  };

  onAkademiChange = async (v) => {
    const { value: level } = this.state.selectedLevel || {};
    const { value: id_akademi } = v || {};

    this.setState(
      {
        selectedAkademi: v,
        selectedTema: null,
        selectedPelatihan: null,
        selectedPelatihanInfo: null,
        // listPelatihanByAkademiTema
      },
      () => {
        this.fetchTema(this.state.selectedAkademi);
      },
    );

    // const { value = -1 } = this.state.selectedLevel;
    // if (value > 0) this.fetchTema(v);
  };

  onTemaChange = async (v) => {
    const { value: level } = this.state.selectedLevel || {};
    const { value: id_akademi } = this.state.selectedAkademi || {};
    const { value: theme_id } = v || {};

    this.setState({
      selectedTema: v,
      selectedPelatihan: null,
      selectedPelatihanInfo: null,
      // listPelatihanByAkademiTema
    });

    const { value = -1 } = this.state.selectedLevel;
    if (value > 1) this.fetchPelatihan(v);
  };

  onPelatihanChange = async (v) => {
    const { value: level } = this.state.selectedLevel || {};
    const { value: selectedAkademi = -1 } = this.state.selectedAkademi || {};
    const { value: selectedTema = -1 } = this.state.selectedTema || {};
    const { value: selectedPelatihan = -1 } = v || {};

    const [selectedPelatihanInfo, selectedSubstansiInfo] = await Promise.all([
      fetchStatusPelatihan(selectedAkademi, selectedTema, selectedPelatihan),
      fetchStatusSubstansi(selectedAkademi, selectedTema, selectedPelatihan),
    ]);

    this.setState({
      selectedPelatihan: v,
      selectedPelatihanInfo: {
        ...selectedPelatihanInfo,
        ...selectedSubstansiInfo,
      },
      // listPelatihanByAkademiTema
    });
  };

  onKategoriChange = (v) => {
    this.setState({
      selectedKategori: v,
    });
  };

  onMetodeSoalChange = (v) => {
    this.setState({
      selectedMetodeSoal: v,
    });
  };

  init = async () => {
    if (Cookies.get("token") == null) {
      await Swal.fire({
        title: "Unauthenticated.",
        text: "Silahkan login ulang",
        icon: "warning",
        confirmButtonText: "Ok",
      });

      window.location = "/";
    }

    try {
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });

      const [dataLevel, dataStatus, dataTipeSoal, dataKategori, dataAkademi] =
        await Promise.all([
          fetchLevel(),
          fetchStatus(),
          new Promise((resolve, reject) => {
            fetchTipeSoal(1, "id", "DESC")
              .then(resolve)
              .catch(async (err) => {
                await Swal.fire({
                  title: err,
                  icon: "warning",
                  confirmButtonText: "Ok",
                });
                resolve([]);
              });
          }),
          fetchKategori(),
          fetchAkademi(),
        ]);

      this.setState({
        dataLevel,
        dataTipeSoal,
        dataKategori,
        dataAkademi,
        dataStatus,
      });

      Swal.close();
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  componentDidMount() {
    this.init();
  }

  save = async () => {
    try {
      const { history } = this.props || {};

      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });

      await tambahTestSubstansi({
        ...this.state,
        selectedQuestionIds: this.state.questions.map(({ id }) => id),
      });

      Swal.close();

      this.props.setShowExitPrompt(false);

      await Swal.fire({
        title: "Test substansi berhasil disimpan",
        icon: "success",
        confirmButtonText: "Ok",
      });

      window.location.href = "/subvit/test-substansi";
    } catch (err) {
      await Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  removeQuestion = async (idx) => {
    const { isConfirmed = false } = await Swal.fire({
      title: "Apakah anda yakin ?",
      text: "Data ini tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, hapus!",
      cancelButtonText: "Tidak",
    });

    if (isConfirmed) {
      let questions = this.state.questions;
      questions.splice(idx, 1);
      this.setState({ questions });

      await Swal.fire({
        title: "Soal berhasil dihapus",
        icon: "success",
        confirmButtonText: "Ok",
      });
    }
  };

  showPreview = (idx, questions) => {
    this.setState({
      questionsPreviewIdx: idx,
      questionsPreview: [...questions],
    });
  };

  handlePrevStep = (e) => {
    this.setState({ step: this.state.step - 1 });
    this.prevBtnRef.current.click();
  };

  handleNextStep = (e) => {
    const { selectedPelatihanInfo = {}, ...rest } = this.state;

    if (this.state.step == 1) {
      this.setState({ ...resetErrorHeaderScreen() });
      const error = validateHeaderScreen({ ...rest, ...selectedPelatihanInfo });
      if (Object.keys(error).length > 0) {
        this.setState({ ...error });

        Swal.fire({
          title: "Masih terdapat isian yang belum lengkap",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      } else {
        this.setState({ step: this.state.step + 1 });
        this.nextBtnRef.current.click();
      }
    } else if (this.state.step == 2 || this.state.step == 3) {
      if ((this.state.questions || []).length == 0) {
        Swal.fire({
          title: "Bank soal tidak boleh kosong",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      } else {
        this.setState({ step: this.state.step + 1 });
        this.nextBtnRef.current.click();
      }
    } else if (this.state.step == 4) {
      this.setState({ ...resetErrorPublishScreen() });
      const error = validatePublishScreen({
        ...rest,
        ...selectedPelatihanInfo,
      });
      if (Object.keys(error).length > 0) {
        this.setState({ ...error });

        Swal.fire({
          title: "Masih terdapat isian yang belum lengkap",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      } else {
        this.setState({ step: this.state.step + 1 });
        this.nextBtnRef.current.click();
      }
    } else {
      this.setState({ step: this.state.step + 1 });
      this.nextBtnRef.current.click();
    }
  };

  handleSubmitStep = (e) => {
    this.submitBtnRef.current.click();
  };

  render() {
    const { value: selectedLevelValue = -1 } = this.state.selectedLevel || {};
    const {
      midtest_mulai = null,
      midtest_selesai = null,
      subtansi_mulai = null,
      subtansi_selesai = null,
    } = this.state.selectedPelatihanInfo || {};

    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <path
                        opacity="0.3"
                        d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                        fill="#7239ea"
                      ></path>
                      <path
                        d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                        fill="#7239ea"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Subvit
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Test Substansi
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={() =>
                    this?.props?.history && this?.props?.history(-1)
                  }
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div
                    className="stepper stepper-links mb-n3"
                    id="kt_subvit_trivia"
                  >
                    <div className="col-lg-12 mt-7">
                      <div className="card border">
                        <div className="card-header">
                          <div className="card-title">
                            <div className="stepper-nav flex-wrap mb-n4">
                              <div
                                className="stepper-item ms-0 me-3 my-2 current"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label ms-0">
                                    <h3 className="stepper-title fs-6">
                                      Informasi Tes Substansi
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Bank Soal
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Review
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Publish
                                    </h3>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="card-body">
                          <div className="highlight bg-light-primary my-5">
                            <div className="col-lg-12 mb-7 fv-row text-primary">
                              <h5 className="text-primary fs-5">Panduan</h5>
                              <p className="text-primary">
                                Sebelum membuat Tes Substansi, mohon untuk
                                membaca panduan berikut :
                              </p>
                              <ul>
                                <li>
                                  Pelaksanaan Level Akademi : Berlaku untuk{" "}
                                  <strong>SEMUA</strong> Pelatihan pada akademi
                                  terpilih, setiap pelatihan pada akademi
                                  tersebut maupun yang baru dibuat kemudian akan
                                  otomatis merujuk kepada Tes Substansi
                                  tersebut.
                                </li>
                                <li>
                                  Pelaksanaan Level Tema : Berlaku untuk{" "}
                                  <strong>SEMUA</strong> Pelatihan pada tema
                                  terpilih, setiap pelatihan pada tema tersebut
                                  maupun yang baru dibuat kemudian akan otomatis
                                  merujuk kepada Tes Substansi tersebut.
                                </li>
                                <li>
                                  Pelaksanaan Level Pelatihan : Berlaku{" "}
                                  <strong>HANYA</strong> pada pelatihan yang
                                  terpilih
                                </li>
                              </ul>
                            </div>
                          </div>
                          <form
                            action="#"
                            id="kt_subvit_trivia_form"
                            method="post"
                            onSubmit={async (e) => {
                              e.preventDefault();

                              const { selectedPelatihanInfo = {}, ...rest } =
                                this.state;
                              const error = validateTestSubstansi({
                                ...rest,
                                ...selectedPelatihanInfo,
                              });

                              if (Object.keys(error).length > 0) {
                                this.setState({
                                  ...resetErrorTestSubstansi(),
                                  ...error,
                                });

                                Swal.fire({
                                  title:
                                    "Masih terdapat isian yang belum lengkap",
                                  icon: "warning",
                                  confirmButtonText: "Ok",
                                });
                              } else {
                                Swal.fire({
                                  title: "Mohon Tunggu!",
                                  icon: "info", // add html attribute if you want or remove
                                  allowOutsideClick: false,
                                  didOpen: () => {
                                    Swal.showLoading();
                                  },
                                });

                                await this.save();
                              }
                            }}
                          >
                            <div
                              className="flex-column current"
                              data-kt-stepper-element="content"
                            >
                              <div className="row mt-7">
                                <HeaderScreen
                                  data={{
                                    nama: this.state.nama,
                                    setNama: (v) => this.setState({ nama: v }),
                                    namaError: this.state.namaError,
                                    dataLevel: this.state.dataLevel,
                                    selectedLevel: this.state.selectedLevel,
                                    onLevelChange: this.onLevelChange,
                                    selectedLevelError:
                                      this.state.selectedLevelError,
                                    fetchingAkademi: this.state.fetchingAkademi,
                                    dataAkademi: this.state.dataAkademi,
                                    selectedAkademi: this.state.selectedAkademi,
                                    onAkademiChange: this.onAkademiChange,
                                    selectedAkademiError:
                                      this.state.selectedAkademiError,
                                    fetchingTema: this.state.fetchingTema,
                                    dataTema: this.state.dataTema,
                                    selectedTema: this.state.selectedTema,
                                    onTemaChange: this.onTemaChange,
                                    selectedTemaError:
                                      this.state.selectedTemaError,
                                    fetchingPelatihan:
                                      this.state.fetchingPelatihan,
                                    dataPelatihan: this.state.dataPelatihan.map(
                                      ({
                                        nama_unit_kerja,
                                        pid,
                                        nama,
                                        wilayah,
                                        batch = 1,
                                        ...rest
                                      }) => ({
                                        ...rest,
                                        value: pid,
                                        label: `[${nama_unit_kerja}] - ${
                                          this.state.selectedAkademi?.slug
                                        }${pid} - ${nama} Batch ${batch} - ${
                                          wilayah || "Online"
                                        }`,
                                      }),
                                    ),
                                    selectedPelatihan:
                                      this.state.selectedPelatihan,
                                    selectedPelatihanInfo:
                                      this.state.selectedPelatihanInfo,
                                    onPelatihanChange: this.onPelatihanChange,
                                    selectedPelatihanError:
                                      this.state.selectedPelatihanError,
                                    dataKategori: this.state.dataKategori,
                                    selectedKategori:
                                      this.state.selectedKategori,
                                    onKategoriChange: this.onKategoriChange,
                                    selectedKategoriError:
                                      this.state.selectedKategoriError,
                                    kategoriOptionDisabled: ({ value }) => {
                                      if (selectedLevelValue != 2) return false;
                                      if (
                                        value == 1 &&
                                        !!subtansi_mulai &&
                                        !!subtansi_selesai
                                      )
                                        return false;
                                      if (
                                        value == 2 &&
                                        !!midtest_mulai &&
                                        !!midtest_selesai
                                      )
                                        return false;
                                      return true;
                                    },
                                    isMetodeSoalEntry: () =>
                                      this.state.selectedMetodeSoal == "entry",
                                    isMetodeSoalImport: () =>
                                      this.state.selectedMetodeSoal == "import",
                                    onMetodeSoalChange: this.onMetodeSoalChange,
                                  }}
                                />
                                <Metode
                                  data={{
                                    isMetodeSoalEntry: () =>
                                      this.state.selectedMetodeSoal == "entry",
                                    isMetodeSoalImport: () =>
                                      this.state.selectedMetodeSoal == "import",
                                    onMetodeSoalChange: this.onMetodeSoalChange,
                                  }}
                                />
                              </div>
                            </div>

                            <div
                              className="flex-column"
                              data-kt-stepper-element="content"
                            >
                              <AddEditScreen
                                data={{
                                  dataTipeSoal: this.state.dataTipeSoal,
                                  isMetodeSoalEntry: () =>
                                    this.state.selectedMetodeSoal == "entry",
                                  isMetodeSoalImport: () =>
                                    this.state.selectedMetodeSoal == "import",
                                  onMetodeSoalChange: this.onMetodeSoalChange,
                                  addQuestions: (v) =>
                                    this.setState({
                                      questions: [
                                        ...this.state.questions,
                                        ...v,
                                      ],
                                    }),
                                  updateQuestion: (idx, v) => {
                                    const newquestions = this.state.questions;
                                    newquestions[idx] = { ...v };
                                    this.setState({
                                      questions: [...newquestions],
                                    });
                                  },
                                  setSelectedEditSoal: (idx) =>
                                    this.setState({ selectedEditSoalIdx: idx }),
                                  selectedEditSoalIdx:
                                    this.state.selectedEditSoalIdx,
                                  questions: this.state.questions,
                                  showPreviewButton: true,
                                  showPreview: this.showPreview,
                                  prevBtnRef: this.prevBtnRef,
                                  nextBtnRef: this.nextBtnRef,
                                }}
                              />
                            </div>

                            <div
                              className="flex-column"
                              data-kt-stepper-element="content"
                            >
                              <EditReorderScreen
                                data={{
                                  questions: this.state.questions,
                                  setSelectedEditSoal: (idx) =>
                                    this.setState({ selectedEditSoalIdx: idx }),
                                  showPreview: this.showPreview,
                                  removeQuestion: this.removeQuestion,
                                  showSearch: false,
                                  showAddButton: false,
                                  prevBtnRef: this.prevBtnRef,
                                  nextBtnRef: this.nextBtnRef,
                                }}
                              />
                            </div>

                            <div
                              className="flex-column"
                              data-kt-stepper-element="content"
                            >
                              <DraftPublishScreen
                                data={{
                                  questions: this.state.questions,
                                  selectedLevel: this.state.selectedLevel,
                                  selectedPelatihanInfo:
                                    this.state.selectedPelatihanInfo,
                                  listPelatihanByAkademiTema:
                                    this.state.listPelatihanByAkademiTema,
                                  selectedKategori: this.state.selectedKategori,
                                  mulaiPelaksanaan: this.state.mulaiPelaksanaan,
                                  mulaiPelaksanaanError:
                                    this.state.mulaiPelaksanaanError,
                                  selesaiPelaksanaan:
                                    this.state.selesaiPelaksanaan,
                                  selesaiPelaksanaanError:
                                    this.state.selesaiPelaksanaanError,
                                  jumlahSoal: this.state.jumlahSoal,
                                  jumlahSoalError: this.state.jumlahSoalError,
                                  durasiDetik: this.state.durasiDetik,
                                  durasiDetikError: this.state.durasiDetikError,
                                  passingGrade: this.state.passingGrade,
                                  passingGradeError:
                                    this.state.passingGradeError,
                                  dataStatus: this.state.dataStatus,
                                  selectedStatus: this.state.selectedStatus,
                                  selectedStatusError:
                                    this.state.selectedStatusError,
                                  selectedLevel: this.state.selectedLevel,
                                  dataAkademi: this.state.dataAkademi,
                                  selectedAkademi: this.state.selectedAkademi,
                                  dataTema: this.state.dataTema,
                                  selectedTema: this.state.selectedTema,
                                  updateState: (k, v) => {
                                    this.setState({ [k]: v });
                                  },
                                }}
                              />
                            </div>

                            <div className="text-center border-top pt-10 my-7">
                              {/* <button onClick={this.handleClickBatal} className="btn btn-secondary btn-sm">Daftar Pelatihan</button> */}
                              <button
                                className="btn btn-light btn-md me-3 mr-2"
                                data-kt-stepper-action="previous"
                                ref={this.prevBtnRef}
                                style={{ display: "none" }}
                              >
                                <i className="fa fa-chevron-left"></i>Sebelumnya
                              </button>
                              <button
                                type="submit"
                                className="btn btn-primary btn-md"
                                data-kt-stepper-action="submit"
                                ref={this.submitBtnRef}
                                style={{ display: "none" }}
                              >
                                <i className="fa fa-paper-plane ms-1"></i>Simpan
                              </button>
                              <button
                                type="button"
                                className="btn btn-primary btn-md"
                                data-kt-stepper-action="next"
                                id="nextBtnRef"
                                ref={this.nextBtnRef}
                                style={{ display: "none" }}
                              >
                                <i className="fa fa-chevron-right"></i>Lanjutkan
                              </button>
                              {this.state.step != 1 && (
                                <button
                                  type="button"
                                  className="btn btn-primary btn-md mr-3 me-3"
                                  onClick={(e) => this.handlePrevStep(e)}
                                >
                                  <i className="fa fa-chevron-left"></i>
                                  Sebelumnya
                                </button>
                              )}
                              {this.state.step == 4 && (
                                <button
                                  type="button"
                                  className="btn btn-primary btn-md"
                                  onClick={(e) => this.handleSubmitStep(e)}
                                >
                                  <i className="fa fa-paper-plane ms-1"></i>
                                  Simpan
                                </button>
                              )}
                              {this.state.step < 4 && (
                                <button
                                  type="button"
                                  className="btn btn-primary btn-md"
                                  onClick={(e) => this.handleNextStep(e)}
                                >
                                  <i className="fa fa-chevron-right"></i>
                                  Lanjutkan
                                </button>
                              )}
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <PreviewModal
          questionIdx={this.state.questionsPreviewIdx}
          questions={this.state.questionsPreview}
        />
      </div>
    );
  }
}

const AddTrivia = (props) => {
  const ContentHOC = withExitPrompt(Content);

  return (
    <>
      <SideNav />
      <Header />
      <ContentHOC defaultShowExitPrompt={false} {...props} />
      <Footer />
    </>
  );
};

export default withRouter(AddTrivia);
