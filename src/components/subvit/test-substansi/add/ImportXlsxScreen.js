import "line-awesome/dist/line-awesome/css/line-awesome.min.css";
import { Observer } from "mobx-react-lite";
import React from "react";
import DataTable from "react-data-table-component";
import Swal from "sweetalert2";
import { read, utils } from "xlsx";
import { resizeFile, validateQuestion as validate } from "../components/utils";
import template from "../Template_Soal_Test_Substansi.xlsx";

class ImportXlsxScreen extends React.Component {
  constructor(props) {
    super(props);

    this.btnDialogRef = React.createRef();

    this.state = {
      fileTemplateXlsx: null,
      fileTemplateImages: [],
      errorTriviaQuestions: [],
    };
  }

  onFileTemplateXlsxChange = (v) => {
    this.setState({
      fileTemplateXlsx: v,
    });
  };

  addFileTemplateImages = (v) => {
    this.setState({
      fileTemplateImages: v,
    });
  };

  removeFileTemplateImage = (idx) => {
    let fileTemplateImages = this.state.fileTemplateImages;
    fileTemplateImages.splice(idx, 1);

    this.setState({
      fileTemplateImages: [...fileTemplateImages],
    });
  };

  doImport = async () => {
    const { data } = this.props || {};
    const {
      addQuestions = () => {},
      nextBtnRef,
      onMetodeSoalChange,
      dataTipeSoal,
    } = data || {};

    try {
      const file = this.state.fileTemplateXlsx;
      const images = (
        await Promise.all(
          (this.state.fileTemplateImages || []).map(async (f) => {
            const cf = await resizeFile(f);
            return new Promise((resolve, reject) => {
              var reader = new FileReader();
              reader.onload = () => {
                resolve({ name: f.name, data: reader.result });
              };
              reader.readAsDataURL(cf);
            });
          }),
        )
      ).reduce((obj, { name, data }) => ({ ...obj, [name]: data }), {});

      const templateBin = await new Promise((resolve, reject) => {
        var reader = new FileReader();
        reader.onload = () => {
          resolve(reader.result);
        };
        reader.readAsArrayBuffer(file);
      });

      var result = new Uint8Array(templateBin);
      var wb = read(result, { type: "array" });

      this.dataQuestions = [];

      wb.SheetNames.splice(0, 1).forEach((s) => {
        const [header = [], ...data] = utils.sheet_to_json(wb.Sheets[s], {
          header: 1,
        });

        if (header[3] != "ID Tipe Soal") {
          throw new Error(
            "Template Tidak Sesuai, Pastikan Menggunakan Template Test Substansi",
          );
        }

        // Validate image must be exists in uploaded images
        let notExistingImages = [];
        data.forEach((row, ridx) => {
          header.forEach((cname, cidx) => {
            if (cname.toLowerCase().includes("gambar")) {
              const img = row[cidx];
              if (img) {
                if (!Object.keys(images).includes(img)) {
                  notExistingImages = [...notExistingImages, img];
                }
              }
            }
          });
        });

        // if (notExistingImages.length > 0) throw new Error(`Gambar berikut tidak terdapat di gambar yang di-upload: ${notExistingImages.join(', ')}`);

        let triviaQuestions = [];
        let errorTriviaQuestions = [];
        data.forEach((row, ridx) => {
          const [
            q = "",
            qimg = null,
            skeys = "",
            id_tipe_soal = null,
            status = null,
            ...answs
          ] = header.map((cname, idx) => row[idx]);
          const [key] = skeys
            .split(",")
            .map((k) => k.trim())
            .map((k) => k.toLowerCase());
          const [selectedTipeSoal] = dataTipeSoal.filter(
            ({ value }) => value == id_tipe_soal,
          );
          const { value: selectedTipeSoalId } = selectedTipeSoal || {};

          if (q.trim()) {
            let tas = [];
            const letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];
            for (let aidx = 0; aidx < answs.length; aidx += 2) {
              if (answs[aidx] == null || answs[aidx] == undefined) continue;

              let ta = {
                option: answs[aidx],
                key: letters[aidx / 2],
              };

              if (answs[aidx + 1]) ta.image = images[answs[aidx + 1]];
              tas = [...tas, ta];
            }

            let tq = {
              answer: tas,
              answer_key: key,
              question: q,
              question_image: images[qimg],
              question_type_id: selectedTipeSoalId,
              status: status,
            };

            let errorsss = [];

            try {
              let notExistingImages = [];
              header.forEach((cname, cidx) => {
                if (cname.toLowerCase().includes("gambar")) {
                  const img = row[cidx] || "";
                  if (img) {
                    if (!Object.keys(images).includes(img)) {
                      notExistingImages = [...notExistingImages, img];
                    }
                  }
                }
              });

              if (notExistingImages.length > 0)
                throw new Error(
                  `Gambar ${notExistingImages.join(
                    ", ",
                  )} tidak terdapat pada gambar yang di-upload`,
                );
            } catch (err) {
              errorsss = [...errorsss, err.message];
            }

            const error = validate(tq);
            if (Object.keys(error).length == 0) {
              triviaQuestions = [...triviaQuestions, tq];
            } else {
              try {
                Object.keys(error).forEach((cat) => {
                  if (cat != "answer_error") {
                    throw new Error(error[cat]);
                  } else {
                    error.answer_error.forEach((aerrs) => {
                      Object.keys(aerrs).forEach((kerr) => {
                        throw new Error(aerrs[kerr]);
                      });
                    });
                  }
                });

                throw new Error(error);
              } catch (err) {
                errorsss = [...errorsss, err.message];
              }
            }

            if (errorsss.length > 0) {
              errorTriviaQuestions = [
                ...errorTriviaQuestions,
                { ...tq, rowNum: ridx + 2, error: errorsss.join("; ") },
              ];
            }
          }
        });

        if (errorTriviaQuestions.length) {
          this.setState({ errorTriviaQuestions });
          this.btnDialogRef.current?.click();

          Swal.fire({
            title: "Soal Gagal Diimport",
            text: "Silahkan perbaiki file Excel berdasarkan kesalahan pada dialog yang muncul, kemudian lakukan import kembali!",
            icon: "warning",
            confirmButtonText: "Ok",
          });
        } else {
          addQuestions(triviaQuestions);
          onMetodeSoalChange("entry");
          nextBtnRef?.current?.click();

          Swal.fire({
            title: "Soal Berhasil Diimport",
            icon: "success",
            confirmButtonText: "Ok",
          });
        }
      });
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  render() {
    const { data } = this.props || {};
    const {
      isMetodeSoalEntry,
      isMetodeSoalImport,
      prevBtnRef,
      nextBtnRef,
      addQuestions,
    } = data || {};
    const { showBackButton = false, onBackButtonClick = () => {} } = data || {};

    return (
      <>
        <div className="mx-1">
          <div className="highlight bg-light-primary">
            <div className="col-lg-12 mb-7 fv-row text-primary">
              <h5 className="text-primary fs-5">Panduan</h5>
              <p className="text-primary">
                Sebelum melakukan import soal, mohon untuk membaca panduan
                berikut :
              </p>
              <ul>
                <li>
                  Silahkan unduh template untuk melakukan import pada link
                  berikut{" "}
                  <a
                    target="_blank"
                    href={template}
                    className="btn btn-primary fw-semibold btn-sm py-1 px-2"
                  >
                    <i className="las la-cloud-download-alt fw-semibold me-1" />
                    Download Template
                  </a>
                </li>
                <li>
                  Jika anda ingin membuat soal yang terdapat gambar, pastikan
                  nama file gambar sesuai dengan yang di-input pada template
                </li>
                <li>ID Tipe Soal harus terisi</li>
              </ul>
            </div>
          </div>
          <Observer>
            {() => {
              return (
                <div className="row mt-7">
                  <div className="col-lg-12 mb-7 fv-row">
                    <label className="form-label required">
                      Upload Template
                    </label>
                    <input
                      type="file"
                      className="form-control form-control-sm mb-2"
                      name="upload_silabus"
                      accept=".xlsx"
                      onInput={(e) => {
                        const [f] = [...e.target.files];
                        this.onFileTemplateXlsxChange(f);
                        e.target.value = null;
                      }}
                    />
                    <small className="text-muted">
                      Format File (.xlsx), Max 10240 Kb
                    </small>
                    <br />
                  </div>
                  {this.state.fileTemplateXlsx && (
                    <div className="col-lg-12 mb-7 fv-row">
                      <div className="row">
                        <div className="col-lg-12 mb-7 fv-row">
                          <span>{this.state.fileTemplateXlsx?.name}</span>
                          <div
                            className="btn btn-sm btn-icon btn-light-danger float-end"
                            title="Hapus file"
                            onClick={(e) => this.onFileTemplateXlsxChange(null)}
                          >
                            <span className="las la-trash-alt" />
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              );
            }}
          </Observer>
          <Observer>
            {() => {
              return (
                <div className="row">
                  <div className="col-lg-12 mb-7 fv-row">
                    <label className="form-label">Upload Images</label>
                    <input
                      type="file"
                      className="form-control form-control-sm mb-2"
                      name="upload_silabus"
                      multiple
                      accept="image/png, image/gif, image/jpeg"
                      onInput={(e) => {
                        this.addFileTemplateImages([...e.target.files]);
                        e.target.value = null;
                      }}
                    />
                    <small className="text-muted">
                      Format File (.png, .jpg, .gif), Max 10240 Kb
                    </small>
                    <br />
                  </div>
                  {this.state.fileTemplateImages && (
                    <div className="col-lg-12 mb-7 fv-row">
                      <div className="row">
                        {this.state.fileTemplateImages.map((file, idx) => {
                          return (
                            <div className="col-lg-12 mb-7 fv-row">
                              <span>{file.name}</span>
                              <div
                                className="btn btn-sm btn-icon btn-light-danger float-end"
                                title="Hapus file"
                                onClick={(e) =>
                                  this.removeFileTemplateImage(idx)
                                }
                              >
                                <span className="las la-trash-alt" />
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  )}
                </div>
              );
            }}
          </Observer>
          <div className="row text-center">
            <div className="col-lg-12 text-center mb-3">
              {showBackButton && (
                <a
                  className="btn btn-primary btn-sm me-5"
                  onClick={onBackButtonClick}
                >
                  Kembali
                </a>
              )}
              <Observer>
                {() => (
                  <button
                    className="btn btn-success btn-md"
                    disabled={!this.state.fileTemplateXlsx}
                    onClick={(e) => {
                      e.preventDefault();
                      this.doImport();
                    }}
                  >
                    <i className="bi bi-cloud-download"></i>Import Template
                  </button>
                )}
              </Observer>
            </div>
          </div>
        </div>
        {this.renderErrorDialog()}
      </>
    );
  }

  renderErrorDialog() {
    return (
      <>
        <button
          className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2 d-none"
          data-bs-toggle="modal"
          data-bs-target="#modal_error"
          ref={this.btnDialogRef}
        >
          <i className="bi bi-sliders"></i>
          <span className="d-md-inline d-lg-inline d-xl-inline d-none">
            Show Error Dialog
          </span>
        </button>
        <div className="modal fade" tabindex="-1" id="modal_error">
          <div className="modal-dialog modal-lg">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">
                  <span className="svg-icon svg-icon-5 me-1">
                    <i className="bi bi-sliders text-black"></i>
                  </span>
                  Error Import Tes Substansi
                </h5>
                <div
                  className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <span className="svg-icon svg-icon-2x">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <rect
                        opacity="0.5"
                        x="6"
                        y="17.3137"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(-45 6 17.3137)"
                        fill="currentColor"
                      />
                      <rect
                        x="7.41422"
                        y="6"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(45 7.41422 6)"
                        fill="currentColor"
                      />
                    </svg>
                  </span>
                </div>
              </div>
              <div className="modal-body">
                <DataTable
                  columns={[
                    {
                      name: "Baris",
                      sortable: false,
                      center: true,
                      width: "70px",
                      grow: 1,
                      cell: (row, index) => row?.rowNum,
                    },
                    {
                      name: "Soal",
                      sortable: false,
                      center: true,
                      width: "200px",
                      grow: 1,
                      cell: (row, index) => row?.question,
                    },
                    {
                      name: "Kesalahan",
                      sortable: false,
                      center: true,
                      width: "300px",
                      grow: 1,
                      cell: (row, index) => row?.error,
                    },
                  ]}
                  data={this.state.errorTriviaQuestions}
                  highlightOnHover
                  pointerOnHover
                  pagination
                  customStyles={{
                    headCells: {
                      style: {
                        background: "rgb(243, 246, 249)",
                      },
                    },
                  }}
                  noDataComponent={<div className="mt-5">Tidak Ada Data</div>}
                  persistTableHead={true}
                />
              </div>
              <div className="modal-footer">
                <div className="d-flex justify-content-between">
                  <a className="btn btn-sm btn-primary" data-bs-dismiss="modal">
                    Tutup
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default ImportXlsxScreen;
