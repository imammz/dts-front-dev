import "line-awesome/dist/line-awesome/css/line-awesome.min.css";
import { Observer } from "mobx-react-lite";
import React from "react";
import EntrySoalScreen from "./EntrySoalScreen";
import ImportXlsxScreen from "./ImportXlsxScreen";

class AddEditScreen extends React.Component {
  render() {
    const { data } = this.props || {};
    const { isMetodeSoalEntry, isMetodeSoalImport, prevBtnRef, nextBtnRef } =
      data || {};

    return (
      <>
        <div id="kt_content_container">
          <div className="row mt-7">
            <Observer>
              {() => {
                return isMetodeSoalEntry() ? (
                  <EntrySoalScreen data={data} />
                ) : isMetodeSoalImport() ? (
                  <ImportXlsxScreen data={data} />
                ) : null;
              }}
            </Observer>
          </div>
        </div>
      </>
    );
  }
}

export class Metode extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { data } = this.props || {};
    const { isMetodeSoalEntry, isMetodeSoalImport, onMetodeSoalChange } =
      data || {};

    return (
      <div className="col-lg-12 mb-7 fv-row">
        <label className="form-label required">Metode</label>
        <Observer>
          {() => (
            <div className="d-flex">
              <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                <input
                  className="form-check-input"
                  type="radio"
                  name="metode"
                  value="entry"
                  id="metode1"
                  checked={isMetodeSoalEntry()}
                  onChange={(e) => onMetodeSoalChange(e.target.value)}
                />
                <label className="form-check-label" for="metode1">
                  Entry Soal
                </label>
              </div>
              <div className="form-check form-check-sm form-check-custom form-check-solid">
                <input
                  className="form-check-input"
                  type="radio"
                  name="metode"
                  value="import"
                  id="metode2"
                  checked={isMetodeSoalImport()}
                  onChange={(e) => onMetodeSoalChange(e.target.value)}
                />
                <label className="form-check-label" for="metode2">
                  Import .xlsx
                </label>
              </div>
            </div>
          )}
        </Observer>
        {/* <span style={{ color: "red" }}>{this.state.errors["program_dts"]}</span> */}
      </div>
    );
  }
}

export default AddEditScreen;
