import "line-awesome/dist/line-awesome/css/line-awesome.min.css";
import { Observer } from "mobx-react-lite";
import React from "react";
import DataTable from "react-data-table-component";
import Swal from "sweetalert2";

class TableQuestions extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      initialSelectedQuestionIds: [],
    };
  }

  componentDidUpdate(prevProps) {
    const { initialSelectedQuestionIds } = this.state || {};
    const { data: prevData } = prevProps || {};
    const { data } = this.props || {};
    const { selectedQuestionIds: prevSelectedQuestionIds } = prevData || {};
    const { selectedQuestionIds } = data || {};
    if (
      JSON.stringify(prevSelectedQuestionIds) !=
        JSON.stringify(selectedQuestionIds) &&
      initialSelectedQuestionIds.length == 0
    ) {
      this.setState({ initialSelectedQuestionIds: selectedQuestionIds });
    }
  }

  render() {
    const { data } = this.props || {};
    const {
      questions = [],
      selectedQuestions = [],
      setSelectedEditSoal,
      prevBtnRef,
      nextBtnRef,
      showPreview,
      removeQuestion,
      selectable,
      onSelectedChange = () => {},
      onMetodeSoalChange = () => {},
    } = data || {};
    const { filter, sort = (a, b) => 1 } = data || {};

    const selectedQuestionIds = selectedQuestions.map(({ id }) => id);
    const filteredQuestions = filter ? questions.filter(filter) : questions;

    if (questions.length > 0 && filteredQuestions.length == 0) {
      Swal.fire({
        title: "Soal tidak ditemukan",
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }

    return (
      <Observer>
        {() => {
          const columns = [
            {
              name: "No",
              sortable: false,
              center: true,
              width: "70px",
              grow: 1,
              cell: ({ idx }) => idx + 1,
            },
            {
              name: "Soal",
              className: "min-w-300px mw-300px",
              grow: 6,
              wrap: true,
              allowOverflow: false,
              // sortable: true,
              selector: ({ question = "" }) => {
                return <Observer>{() => question}</Observer>;
              },
            },
            {
              name: "Status",
              className: "min-w-200px mw-200px",
              sortField: "status",
              center: true,
              selector: ({ status }) => (
                <div>
                  <span
                    className={
                      "badge badge-light-" +
                      (status == 1 ? "success" : "danger") +
                      " fs-7 m-1"
                    }
                  >
                    {status == 1 ? "Publish" : "Draft"}
                  </span>
                </div>
              ),
            },
            {
              name: "Aksi",
              center: true,
              width: "180px",
              grow: 3,
              cell: (q) => {
                const { id, idx } = q || {};
                return (
                  <div>
                    <a
                      href="#"
                      title="Preview Soal"
                      className="btn btn-icon btn-bg-primary btn-sm me-1"
                      data-bs-toggle="modal"
                      data-bs-target="#preview_soal"
                      onClick={(e) => showPreview(idx, [q])}
                    >
                      <i className="bi bi-eye-fill text-white"></i>
                    </a>

                    <a
                      href="#"
                      title="Edit"
                      className="btn btn-icon btn-bg-warning btn-sm me-1"
                      onClick={(e) => {
                        e.preventDefault();
                        setSelectedEditSoal(idx);
                        onMetodeSoalChange("entry");
                        prevBtnRef?.current?.click();
                      }}
                    >
                      <i className="bi bi-gear-fill text-white"></i>
                    </a>
                    <a
                      href="#"
                      onClick={(e) => {
                        e.preventDefault();
                        removeQuestion(idx);
                      }}
                      title="Hapus"
                      className="btn btn-icon btn-bg-danger btn-sm me-1"
                    >
                      <i className="bi bi-trash-fill text-white"></i>
                    </a>
                  </div>
                );
              },
            },
          ];

          return (
            <>
              <DataTable
                columns={columns}
                data={filteredQuestions
                  .sort(sort)
                  .map((q, idx) => ({ ...q, idx }))}
                customStyles={{
                  headCells: {
                    style: {
                      background: "rgb(243, 246, 249)",
                    },
                  },
                }}
                persistTableHead={true}
                pagination
                selectableRows={selectable}
                selectableRowSelected={({ id }) =>
                  selectedQuestionIds.length ==
                  this.state.initialSelectedQuestionIds.length
                    ? true
                    : selectedQuestionIds.includes(id)
                }
                onSelectedRowsChange={(selected) => {
                  const { selectedRows = [] } = selected || {};
                  const _selectedQuestionIds = selectedRows.map(({ id }) => id);

                  let tobeKeepIds = filteredQuestions
                    .map(({ id }) => id)
                    .filter((id) => _selectedQuestionIds.includes(id));
                  let tobeRemoveIds = filteredQuestions
                    .map(({ id }) => id)
                    .filter((id) => !_selectedQuestionIds.includes(id));

                  let newSelectedQuestionIds = selectedQuestionIds.filter(
                    (id) => !tobeRemoveIds.includes(id),
                  );
                  newSelectedQuestionIds = [
                    ...new Set([...newSelectedQuestionIds, ...tobeKeepIds]),
                  ];

                  if (
                    JSON.stringify(newSelectedQuestionIds) !=
                    JSON.stringify(selectedQuestionIds)
                  ) {
                    onSelectedChange(
                      questions.filter(({ id }) =>
                        newSelectedQuestionIds.includes(id),
                      ),
                    );
                  }
                }}
                noDataComponent={<div className="mt-5">Tidak Ada Data</div>}
              />
            </>
          );
        }}
      </Observer>
    );
  }
}

class EditReorderScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchKeyword: "",
    };
  }

  componentDidUpdate() {
    [1000, 2000, 3000].forEach((to) => {
      window.setTimeout(() => {
        window.KTMenu.init();
      }, to);
    });
  }

  render() {
    const { data } = this.props || {};
    const {
      questions = [],
      setSelectedEditSoal,
      prevBtnRef,
      nextBtnRef,
      showPreview,
    } = data || {};
    const {
      showAddButton = true,
      showImportButton = false,
      onMetodeSoalChange = () => {},
    } = data || {};
    const {
      selectedQuestions = [],
      showRemoveAllButton = false,
      onRemoveAllItem = () => {},
    } = data || {};
    const { showSearch = true } = data || {};

    return (
      <>
        <div id="kt_content_container">
          <div className="card-header border-0 m-0 p-0">
            <div className="card-title">
              {showSearch && (
                <div className="d-flex align-items-center position-relative my-1 me-2">
                  <span className="svg-icon svg-icon-1 position-absolute ms-6">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <rect
                        opacity="0.5"
                        x="17.0365"
                        y="15.1223"
                        width="8.15546"
                        height="2"
                        rx="1"
                        transform="rotate(45 17.0365 15.1223)"
                        fill="currentColor"
                      ></rect>
                      <path
                        d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                        fill="currentColor"
                      ></path>
                    </svg>
                  </span>
                  <input
                    type="text"
                    data-kt-user-table-filter="search"
                    className="form-control form-control-sm form-control-solid w-250px ps-14"
                    placeholder="Cari Soal"
                    onKeyDown={(e) => {
                      const keyword = e.target.value;
                      if (e.key == "Enter") {
                        this.setState({
                          searchKeyword: keyword,
                        });
                        e.preventDefault();
                      }
                    }}
                    onChange={(e) => {
                      const keyword = e.target.value;
                      if (!keyword) {
                        this.setState({
                          searchKeyword: keyword,
                        });
                      }
                    }}
                  />
                </div>
              )}
            </div>
            <div className="card-toolbar">
              <div className="d-flex align-items-center position-relative my-1 me-2">
                {(showAddButton || showImportButton || showRemoveAllButton) && (
                  <>
                    <a
                      href="#"
                      className="btn btn-light fw-bolder btn-sm me-2"
                      data-kt-menu-trigger="click"
                      data-kt-menu-placement="bottom-end"
                      data-kt-menu-flip="top-end"
                    >
                      <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                        Kelola Soal
                      </span>
                      <i className="bi bi-chevron-down ms-1"></i>
                    </a>
                    <div
                      className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-7 w-auto "
                      data-kt-menu="true"
                    >
                      <div>
                        {showAddButton && (
                          <div className="menu-item">
                            <a
                              href="#"
                              className="menu-link px-5"
                              onClick={() => {
                                setSelectedEditSoal(null);
                                onMetodeSoalChange("entry");
                                prevBtnRef?.current?.click();
                              }}
                            >
                              <i className="bi bi-plus-circle text-dark text-hover-primary me-1"></i>
                              Tambah Soal
                            </a>
                          </div>
                        )}
                        {showImportButton && (
                          <div className="menu-item">
                            <a
                              href="#"
                              className="menu-link px-5"
                              onClick={() => {
                                setSelectedEditSoal(null);
                                onMetodeSoalChange("import");
                                prevBtnRef?.current?.click();
                              }}
                            >
                              <i className="bi bi-cloud-download text-dark text-hover-primary me-1"></i>
                              Import Soal
                            </a>
                          </div>
                        )}

                        {showRemoveAllButton && (
                          <div className="menu-item">
                            <a
                              href="#"
                              className={`menu-link px-5 ${
                                selectedQuestions.length == 0 ? "disabled" : ""
                              }`}
                              onClick={async () => {
                                if (
                                  selectedQuestions.length == questions.length
                                ) {
                                  await Swal.fire({
                                    title:
                                      "Harus terdapat minimal 1 soal tersisa",
                                    icon: "warning",
                                    confirmButtonText: "Ok",
                                  });

                                  return;
                                }

                                onRemoveAllItem();
                              }}
                            >
                              <i className="bi bi-trash text-dark text-hover-primary me-1"></i>
                              Hapus Soal
                            </a>
                          </div>
                        )}
                      </div>
                    </div>
                  </>
                )}
                <a
                  href="#"
                  className="btn btn-primary btn-sm"
                  data-bs-toggle="modal"
                  data-bs-target="#preview_soal"
                  onClick={() => showPreview(null, questions)}
                >
                  <i className="bi bi-eye"></i>Preview
                </a>
              </div>
            </div>
          </div>

          <TableQuestions
            data={{
              ...data,
              filter: ({ question }) => {
                if (!this.state.searchKeyword) return true;
                else {
                  return question
                    .toLowerCase()
                    .includes(this.state.searchKeyword.toLowerCase());
                }
              },
              sort: (a, b) => {
                if (!this.state.searchKeyword) return 1;
                if (a?.question?.toLowerCase() > b?.question?.toLowerCase())
                  return 1;
                return -1;
              },
            }}
          />
        </div>
      </>
    );
  }
}

export default EditReorderScreen;
