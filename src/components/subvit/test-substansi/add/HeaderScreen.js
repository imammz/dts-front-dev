import "line-awesome/dist/line-awesome/css/line-awesome.min.css";
import { Observer } from "mobx-react-lite";
import React from "react";
import Select from "react-select";
import Cookies from "js-cookie";
import { isSuperAdmin } from "../../../AksesHelper";

class TestHeader extends React.Component {
  constructor(props) {
    super(props);
  }

  render(props) {
    let { data } = this.props || {};
    let { nama, setNama, namaError } = data || {};
    let { dataLevel, selectedLevel, onLevelChange, selectedLevelError } =
      data || {};
    let {
      fetchingAkademi,
      dataAkademi,
      selectedAkademi,
      onAkademiChange,
      selectedAkademiError,
    } = data || {};
    let {
      fetchingTema,
      dataTema,
      selectedTema,
      onTemaChange,
      selectedTemaError,
    } = data || {};
    let {
      fetchingPelatihan,
      dataPelatihan,
      selectedPelatihan,
      selectedPelatihanInfo,
      onPelatihanChange,
      selectedPelatihanError,
    } = data || {};
    let {
      dataKategori,
      selectedKategori,
      onKategoriChange,
      selectedKategoriError,
    } = data || {};
    let { isMetodeSoalEntry, isMetodeSoalImport, onMetodeSoalChange } =
      data || {};
    let {
      levelDisabled = false,
      akademiDisabled = false,
      temaDisabled = false,
      pelatihanDisabled = false,
      kategoriDisabled = false,
    } = data || {};
    let { kategoriOptionDisabled } = data || {};

    dataAkademi = dataAkademi.map((p) => {
      let havings = [];
      if (p?.academy_id_testsubtansi) havings = [...havings, "test substansi"];
      if (p?.academy_id_midtest) havings = [...havings, "mid test"];

      return {
        ...p,
        havings,
        label: `${p?.label} ${
          havings.length > 0 ? `(sudah memiliki ${havings.join(" dan ")})` : ""
        }`,
      };
    });

    dataTema = dataTema.map((p) => {
      let havings = [];
      if (p?.theme_id_testsubtansi) havings = [...havings, "test substansi"];
      if (
        (selectedAkademi?.havings || []).includes("test substansi") &&
        !havings.includes("test substansi")
      )
        havings = [...havings, "test substansi"];
      if (p?.theme_id_midtest) havings = [...havings, "mid test"];
      if (
        (selectedAkademi?.havings || []).includes("mid test") &&
        !havings.includes("mid test")
      )
        havings = [...havings, "mid test"];

      return {
        ...p,
        havings,
        label: `${p?.label} ${
          havings.length > 0 ? `(sudah memiliki ${havings.join(" dan ")})` : ""
        }`,
      };
    });

    dataPelatihan = dataPelatihan.map((p) => {
      let havings = [];
      if (p?.training_id_testsubtansi) havings = [...havings, "test substansi"];
      if (
        (selectedTema?.havings || []).includes("test substansi") &&
        !havings.includes("test substansi")
      )
        havings = [...havings, "test substansi"];
      if (p?.training_id_midtest) havings = [...havings, "mid test"];
      if (
        (selectedTema?.havings || []).includes("mid test") &&
        !havings.includes("mid test")
      )
        havings = [...havings, "mid test"];

      return {
        ...p,
        havings,
        label: `${p?.label} ${
          havings.length > 0 ? `(sudah memiliki ${havings.join(" dan ")})` : ""
        }`,
      };
    });

    return (
      <>
        <div className="col-lg-12 mb-7 fv-row">
          <label className="form-label required">Level</label>
          <Observer>
            {() => (
              <>
                <Select
                  id="level"
                  name="level"
                  placeholder="Silahkan pilih"
                  // noOptionsMessage={({ inputValue }) => !inputValue ? this.state.dataxakademi : "Data tidak tersedia"}
                  value={selectedLevel}
                  className="form-select-sm selectpicker p-0"
                  onChange={(v) => onLevelChange(v)}
                  options={dataLevel}
                  isDisabled={levelDisabled}
                  isOptionDisabled={(o) => {
                    if (o?.value == 0 && !isSuperAdmin()) return true;
                    return false;
                  }}
                />
                <span style={{ color: "red" }}>{selectedLevelError}</span>
              </>
            )}
          </Observer>
        </div>
        <div className="col-lg-12 mb-7 fv-row">
          <label className="form-label required">Akademi</label>
          <Observer>
            {() => {
              let { value: level = -1 } = selectedLevel || {};
              return (
                <>
                  <Select
                    name="akademi"
                    placeholder="Silahkan pilih"
                    // noOptionsMessage={({ inputValue }) => !inputValue ? this.state.dataxakademi : "Data tidak tersedia"}
                    value={selectedAkademi}
                    className="form-select-sm selectpicker p-0"
                    options={dataAkademi}
                    onChange={(v) => onAkademiChange(v)}
                    isOptionDisabled={(o) =>
                      (o?.academy_id_testsubtansi || false) &&
                      (o?.academy_id_midtest || false)
                    }
                    isDisabled={fetchingAkademi || level < 0 || akademiDisabled}
                  />
                  <span style={{ color: "red" }}>{selectedAkademiError}</span>
                </>
              );
            }}
          </Observer>
        </div>
        <div className="col-lg-12 mb-7 fv-row">
          <label className="form-label required">Tema</label>
          <Observer>
            {() => {
              let { value: level = -1 } = selectedLevel || {};
              return (
                <>
                  <Select
                    name="tema"
                    placeholder="Silahkan pilih"
                    // noOptionsMessage={({ inputValue }) => !inputValue ? this.state.dataxtema : "Data tidak tersedia"}
                    value={selectedTema}
                    className="form-select-sm selectpicker p-0"
                    options={dataTema}
                    isDisabled={
                      fetchingTema ||
                      level < 1 ||
                      !selectedAkademi ||
                      temaDisabled
                    }
                    isOptionDisabled={(o) =>
                      (o?.theme_id_testsubtansi || false) &&
                      (o?.theme_id_midtest || false)
                    }
                    onChange={(v) => onTemaChange(v)}
                  />
                  <span style={{ color: "red" }}>{selectedTemaError}</span>
                </>
              );
            }}
          </Observer>
        </div>
        <div className="col-lg-12 mb-7 fv-row">
          <label className="form-label required">Pelatihan</label>
          <Observer>
            {() => {
              let { value: level = -1 } = selectedLevel || {};
              return (
                <>
                  <Select
                    name="pelatihan"
                    placeholder="Silahkan pilih"
                    // noOptionsMessage={({ inputValue }) => !inputValue ? this.state.dataxpelatihan : "Data tidak tersedia"}
                    value={selectedPelatihan}
                    className="form-select-sm selectpicker p-0"
                    options={dataPelatihan}
                    isDisabled={
                      fetchingPelatihan ||
                      level < 2 ||
                      !selectedTema ||
                      pelatihanDisabled
                    }
                    isOptionDisabled={(o) =>
                      (o?.training_id_testsubtansi || false) &&
                      (o?.training_id_midtest || false)
                    }
                    onChange={(v) => onPelatihanChange(v)}
                  />
                  <span style={{ color: "red" }}>{selectedPelatihanError}</span>
                </>
              );
            }}
          </Observer>
        </div>
        <div className="col-lg-12 mb-7 fv-row">
          <label className="form-label required">Jenis Test Substansi</label>
          <Observer>
            {() => (
              <>
                <Select
                  placeholder="Silahkan pilih"
                  // noOptionsMessage={({ inputValue }) => !inputValue ? this.state.jenis_survey : "Data tidak tersedia"}
                  className="form-select-sm selectpicker p-0"
                  options={dataKategori}
                  value={selectedKategori}
                  onChange={(v) => onKategoriChange(v)}
                  isDisabled={kategoriDisabled}
                  // isOptionDisabled={kategoriOptionDisabled}
                  isOptionDisabled={({ value }) => {
                    if (selectedLevel?.value == 0) {
                      if (
                        value == 1 &&
                        (selectedAkademi?.havings || []).includes(
                          "test substansi",
                        )
                      )
                        return true;
                      if (
                        value == 2 &&
                        (selectedAkademi?.havings || []).includes("mid test")
                      )
                        return true;
                      return false;
                    }

                    if (selectedLevel?.value == 1) {
                      if (
                        value == 1 &&
                        (selectedTema?.havings || []).includes("test substansi")
                      )
                        return true;
                      if (
                        value == 2 &&
                        (selectedTema?.havings || []).includes("mid test")
                      )
                        return true;
                      return false;
                    }

                    if (selectedLevel?.value == 2) {
                      if (
                        value == 1 &&
                        ((selectedPelatihan?.havings || []).includes(
                          "test substansi",
                        ) ||
                          !selectedPelatihanInfo?.subtansi_mulai)
                      )
                        return true;
                      if (
                        value == 2 &&
                        ((selectedPelatihan?.havings || []).includes(
                          "mid test",
                        ) ||
                          !selectedPelatihanInfo?.midtest_mulai)
                      )
                        return true;
                      return false;
                    }

                    return false;
                  }}
                />
                <span style={{ color: "red" }}>{selectedKategoriError}</span>
              </>
            )}
          </Observer>
        </div>
      </>
    );
  }
}

export default TestHeader;
