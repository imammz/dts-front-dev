import "line-awesome/dist/line-awesome/css/line-awesome.min.css";
import { Observer } from "mobx-react-lite";
import React from "react";
import Select from "react-select";
import Swal from "sweetalert2";
import {
  letters,
  resizeFile,
  validateQuestion as validate,
} from "../components/utils";

class EntrySoalScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      q: null,
      error: {},
    };
  }

  componentDidMount() {
    const { data } = this.props || {};
    const { selectedEditSoalIdx, questions } = data || {};

    this.setState({
      q: selectedEditSoalIdx == null ? {} : questions[selectedEditSoalIdx],
    });
  }

  componentDidUpdate(prevProps) {
    const { data: prevData } = prevProps || {};
    const { selectedEditSoalIdx: prevSelectedEditSoalIdx } = prevData || {};
    const { data } = this.props || {};
    const { selectedEditSoalIdx, questions } = data || {};

    if (prevSelectedEditSoalIdx != selectedEditSoalIdx) {
      this.setState({
        q: selectedEditSoalIdx == null ? {} : questions[selectedEditSoalIdx],
      });
    }
  }

  render() {
    let { data } = this.props || {};
    let {
      isMetodeSoalEntry,
      isMetodeSoalImport,
      prevBtnRef,
      nextBtnRef,
      selectedEditSoalIdx,
      setSelectedEditSoal,
      questions,
      dataTipeSoal = [],
      addQuestions = () => {},
      updateQuestion = () => {},
      onMetodeSoalChange = () => {},
    } = data || {};
    const { showBackButton = false, onBackButtonClick = () => {} } = data || {};
    const { showPreviewButton = false, showPreview = () => {} } = data || {};

    const qIdx =
      selectedEditSoalIdx == null ? questions.length : selectedEditSoalIdx;

    let q = this.state.q || {};
    if ((q?.answer || []).length == 0) q["answer"] = [{}, {}, {}, {}];
    if (q?.status == null) q["status"] = 1;

    let error = this.state.error || {};
    let {
      question = "",
      question_image = null,
      question_type_id,
      tipe_soal = null,
      answer = [],
      answer_key = "",
      status,
    } = q || {};
    let {
      question_error,
      question_type_id_error,
      answer_error = [],
      answer_key_error,
      status_error,
    } = error || {};

    if (!tipe_soal) {
      [tipe_soal = null] = dataTipeSoal.filter(
        ({ value }) => value == question_type_id,
      );
      q = { ...q, tipe_soal };
    }

    return (
      <>
        <div className="card-title">
          <div className="card mb-6">
            <Observer>
              {() => {
                return (
                  <h5 className="me-3 text-dark mr-2">{`Soal ${qIdx + 1}`}</h5>
                );
              }}
            </Observer>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">Pertanyaan</label>
            <Observer>
              {() => (
                <>
                  <input
                    className="form-control form-control-sm"
                    placeholder="Masukkan pertanyaan"
                    name="pertanyaan"
                    value={question}
                    onChange={(e) =>
                      this.setState({ q: { ...q, question: e.target.value } })
                    }
                  />
                  <span style={{ color: "red" }}>{question_error}</span>
                </>
              )}
            </Observer>
          </div>
          <div className="col-lg-12">
            <div className="mb-7 fv-row">
              <label className="form-label">Gambar Pertanyaan (Optional)</label>
              <Observer>
                {() => (
                  <>
                    {question_image && (
                      <div className="mb-3">
                        <div>
                          <img src={question_image} />
                        </div>
                        <div>
                          <button
                            className="btn btn-sm btn-danger"
                            onClick={() =>
                              this.setState({
                                q: { ...q, question_image: null },
                              })
                            }
                          >
                            Ubah Gambar
                          </button>
                        </div>
                      </div>
                    )}
                    {!question_image && (
                      <>
                        <input
                          type="file"
                          className="form-control form-control-sm mb-2"
                          name="upload_silabus"
                          accept="image/png, image/gif, image/jpeg"
                          // value={state.lastQuestion.gambar}
                          onInput={async (e) => {
                            const [f] = [...e.target.files];
                            const cf = await resizeFile(f);

                            var reader = new FileReader();
                            reader.onload = () => {
                              this.setState({
                                q: { ...q, question_image: reader.result },
                              });
                            };
                            reader.readAsDataURL(cf);
                          }}
                        />
                        <small className="text-muted">
                          Format File (.jpg/.jpeg/.png), Max 10240
                        </small>
                        <br />
                      </>
                    )}
                    {/* <span style={{ color: "red" }}>{this.state.errors["upload_silabus"]}</span> */}
                  </>
                )}
              </Observer>
            </div>
          </div>
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">Tipe Soal</label>
            <Select
              placeholder="Silahkan pilih"
              // noOptionsMessage={({ inputValue }) => !inputValue ? this.state.jenis_survey : "Data tidak tersedia"}
              className="form-select-sm selectpicker p-0"
              options={dataTipeSoal}
              value={tipe_soal}
              onChange={(v) =>
                this.setState({
                  q: { ...q, tipe_soal: v, question_type_id: v.value },
                })
              }
            />
            <span style={{ color: "red" }}>{question_type_id_error}</span>
          </div>
          <Observer>
            {() => (
              <>
                {answer.map((j, idx) => {
                  const key = letters[idx];
                  const { option = "", image } = j || {};
                  const { option_error } = answer_error[idx] || {};

                  return (
                    <Observer key={idx}>
                      {() => (
                        <>
                          <div key={idx} className={`col-lg-5 mb-7 fv-row`}>
                            <label className="form-label required">
                              Jawaban {key}
                            </label>
                            <input
                              className="form-control form-control-sm"
                              placeholder={`Masukkan jawaban ${key}`}
                              name="pertanyaan"
                              value={option}
                              onChange={(e) => {
                                let newanswer = [...answer];
                                newanswer[idx] = {
                                  ...newanswer[idx],
                                  ["option"]: e.target.value,
                                };
                                q = { ...q, answer: newanswer };
                                this.setState({ q });
                              }}
                            />
                            <span style={{ color: "red" }}>{option_error}</span>
                          </div>
                          <div className={`col-lg-5 mb-7 fv-row`}>
                            <div className="mb-7 fv-row">
                              <label className="form-label">
                                Gambar Jawaban (Optional)
                              </label>
                              {image && (
                                <div className="mb-3">
                                  <div>
                                    <img src={image} />
                                  </div>
                                  <div>
                                    <button
                                      className="btn btn-sm btn-danger"
                                      onClick={() => {
                                        let newanswer = [...answer];
                                        newanswer[idx] = {
                                          ...newanswer[idx],
                                          ["image"]: null,
                                        };
                                        q = { ...q, answer: newanswer };
                                        this.setState({ q });
                                      }}
                                    >
                                      Ubah Gambar
                                    </button>
                                  </div>
                                </div>
                              )}
                              {!image && (
                                <>
                                  <input
                                    type="file"
                                    className="form-control form-control-sm mb-2"
                                    name="upload_silabus"
                                    accept="image/png, image/gif, image/jpeg"
                                    onChange={async (e) => {
                                      const [f] = [...e.target.files];
                                      const cf = await resizeFile(f);

                                      var reader = new FileReader();
                                      reader.onload = () => {
                                        let newanswer = [...answer];
                                        newanswer[idx] = {
                                          ...newanswer[idx],
                                          ["image"]: reader.result,
                                        };
                                        q = { ...q, answer: newanswer };
                                        this.setState({ q });
                                      };
                                      reader.readAsDataURL(cf);
                                    }}
                                  />
                                  <small className="text-muted">
                                    Format File (.jpg/.jpeg/.png), Max 10240
                                  </small>
                                </>
                              )}
                            </div>
                          </div>
                          <div className={`col-lg-1 mb-7 fv-row`}>
                            <div className="mt-7 fv-row">
                              <a
                                title="Hapus"
                                index="1_0"
                                className={`btn btn-icon btn-danger w-40px h-40px ${
                                  answer.length <= 2 ? "disabled" : ""
                                }`}
                                onClick={(e) => {
                                  e.preventDefault();
                                  let newanswer = [...answer];
                                  newanswer.splice(idx, 1);
                                  q = { ...q, answer: newanswer };
                                  this.setState({ q });
                                }}
                              >
                                <span className="svg-icon svg-icon-3">
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="34"
                                    height="34"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                  >
                                    <path
                                      d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z"
                                      fill="black"
                                    ></path>
                                    <path
                                      opacity="0.5"
                                      d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z"
                                      fill="black"
                                    ></path>
                                    <path
                                      opacity="0.5"
                                      d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z"
                                      fill="black"
                                    ></path>
                                  </svg>
                                </span>
                              </a>
                            </div>
                          </div>
                          <div
                            className={`col-lg-1 fv-row ${
                              idx == 0 ? "" : "mt-7"
                            } text-center`}
                          >
                            <Observer>
                              {() => (
                                <>
                                  {idx == 0 && (
                                    <label className="form-label d-block required mb-3">
                                      Kunci
                                    </label>
                                  )}
                                  <input
                                    className="form-check-input align-content-center"
                                    type="radio"
                                    name="answer_key"
                                    value={key}
                                    id="jenis_pertanyaan1"
                                    checked={
                                      (answer_key || "").toLowerCase() ==
                                      (key || "").toLowerCase()
                                    }
                                    onChange={(e) => {
                                      this.setState({
                                        q: { ...q, answer_key: e.target.value },
                                      });
                                    }}
                                  />
                                  <span style={{ color: "red" }}>
                                    {answer_key_error}
                                  </span>
                                </>
                              )}
                            </Observer>
                          </div>
                        </>
                      )}
                    </Observer>
                  );
                })}
                <div className="row text-center">
                  <div className="col-lg-12 text-center mb-7">
                    <a
                      className="btn btn-sm btn-light text-success d-block btn-block"
                      onClick={(e) => {
                        e.preventDefault();
                        let newanswer = [...answer, {}];
                        q = { ...q, answer: newanswer };
                        this.setState({ q });
                      }}
                    >
                      <i className="bi bi-plus-circle text-success me-1"></i>{" "}
                      Tambah Opsi Jawaban
                    </a>
                  </div>
                </div>

                {/* <div className='row'>
          <div className='col-lg-12 mb-3 mt-3'>
            <h5>Daftar Soal</h5>
            <UrutanSoal sortable={false} />
          </div>
        </div> */}
              </>
            )}
          </Observer>
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">Status</label>
            <div className="d-flex">
              <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                <input
                  className="form-check-input"
                  type="radio"
                  name="status"
                  value="0"
                  id="status_publish_q"
                  checked={status == 0}
                  onChange={(e) => this.setState({ q: { ...q, status: 0 } })}
                />
                <label className="form-check-label" for="status_publish_q">
                  Draft
                </label>
              </div>
              <div className="form-check form-check-sm form-check-custom form-check-solid">
                <input
                  className="form-check-input"
                  type="radio"
                  name="status"
                  value="1"
                  id="status_draft_q"
                  checked={status == 1}
                  onChange={(e) => this.setState({ q: { ...q, status: 1 } })}
                />
                <label className="form-check-label" for="status_draft_q">
                  Publish
                </label>
              </div>
            </div>
            <span style={{ color: "red" }}>{status_error}</span>
          </div>
          <div className="row mt-7">
            <div className="col-lg-12 mb-7 fv-row text-center">
              {selectedEditSoalIdx != null && (
                <a
                  className="btn btn-light btn-md me-5"
                  onClick={(e) => {
                    e.preventDefault();
                    if (setSelectedEditSoal) setSelectedEditSoal(null);
                    if (nextBtnRef?.current) nextBtnRef?.current?.click();
                    if (onBackButtonClick) onBackButtonClick();
                  }}
                >
                  Batal
                </a>
              )}

              {/*{
              showPreviewButton && <a className="btn btn-light btn-md me-3" data-bs-toggle="modal" data-bs-target="#preview_soal" onClick={() => {
                q.answer = [...(q.answer || [])].map((a, idx) => ({ ...a, key: letters[idx] }));
                showPreview(qIdx, [q]);
              }}>Preview</a>
            }*/}
              <Observer>
                {() => (
                  <a
                    className="btn btn-success btn-md"
                    onClick={async (e) => {
                      e.preventDefault();

                      q.answer = [...(q.answer || [])].map((a, idx) => ({
                        ...a,
                        key: letters[idx],
                      }));

                      const error = validate(q) || {};
                      if (Object.keys(error).length == 0) {
                        if (selectedEditSoalIdx == null) {
                          addQuestions([q]);
                        } else {
                          updateQuestion(selectedEditSoalIdx, q);
                          nextBtnRef?.current?.click();
                        }

                        await Swal.fire({
                          title: `Soal berhasil ${
                            selectedEditSoalIdx != null
                              ? `disimpan`
                              : `disimpan`
                          }`,
                          icon: "success",
                          confirmButtonText: "Ok",
                        });

                        onMetodeSoalChange("entry");
                        setSelectedEditSoal(null);

                        this.setState({
                          q: null,
                          error: {},
                        });
                      } else {
                        this.setState({ q: { ...q }, error: { ...error } });

                        Swal.fire({
                          title: "Masih terdapat isian yang belum lengkap",
                          icon: "warning",
                          confirmButtonText: "Ok",
                        });
                      }
                    }}
                  >
                    <i className="bi bi-plus-circle-fill ms-1"></i>
                    {selectedEditSoalIdx != null
                      ? `Simpan`
                      : `Simpan dan Tambah Soal`}
                    {/* {state.lastQuestion.isSaved ? 'Perbarui Soal' : 'Simpan dan Tambah Soal'} */}
                  </a>
                )}
              </Observer>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default EntrySoalScreen;
