import "line-awesome/dist/line-awesome/css/line-awesome.min.css";
import { Observer } from "mobx-react-lite";
import React from "react";
import Select from "react-select";

class HeaderInfoScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { data } = this.props || {};
    const { header } = data || {};
    const {
      category,
      duration,
      id_akademi = 0,
      id_pelatihan,
      id_substansi,
      jml_soal,
      nama_akademi,
      nama_pelatihan,
      nama_substansi,
      nama_tema,
      passing_grade,
      status_pelaksanaan,
      status_substansi = -1,
      tgl_pelaksanaan,
      tgl_selesai_pelaksanaan,
      theme_id = 0,
    } = header || {};

    let badge = "";
    let label = "";
    if (status_substansi == 0) {
      badge = "warning";
      label = "Draft";
    } else if (status_substansi == 1) {
      badge = "success";
      label = "Publish";
    } else if (status_substansi == 2) {
      badge = "danger";
      label = "Batal";
    }

    return (
      <>
        <div className="col-lg-6 mb-7 fv-row">
          <label className="form-label">Nama Test Substansi</label>
          <div className="d-flex">
            <b>{nama_substansi || "-"}</b>
          </div>
        </div>
        <div className="col-lg-6 mb-7 fv-row">
          <label className="form-label">Nama Akademi</label>
          <div className="d-flex">
            <b>{nama_akademi || "-"}</b>
          </div>
        </div>
        <div className="col-lg-6 mb-7 fv-row">
          <label className="form-label">Nama Tema</label>
          <div className="d-flex">
            <b>{nama_tema || "-"}</b>
          </div>
        </div>
        <div className="col-lg-6 mb-7 fv-row">
          <label className="form-label">Nama Pelatihan</label>
          <div className="d-flex">
            <b>{nama_pelatihan || "-"}</b>
          </div>
        </div>
        <div className="col-lg-6 mb-7 fv-row">
          <label className="form-label">Tgl. Mulai Test Substansi</label>
          <div className="d-flex">
            <b>{tgl_pelaksanaan || "-"}</b>
          </div>
        </div>
        <div className="col-lg-6 mb-7 fv-row">
          <label className="form-label">Tgl. Akhir Test Substansi</label>
          <div className="d-flex">
            <b>{tgl_selesai_pelaksanaan || "-"}</b>
          </div>
        </div>
        <div className="col-lg-6 mb-7 fv-row">
          <label className="form-label">Status Test Substansi</label>
          <div className="d-flex">
            <b>
              <span className={"badge badge-light-" + badge + " fs-7 m-1"}>
                {label}
              </span>
            </b>
          </div>
        </div>
        {status_substansi == 1 && (
          <div className="col-lg-6 mb-7 fv-row">
            <label className="form-label">Status Pelaksanaan</label>
            <div className="d-flex">
              <b>
                <span
                  className={
                    "badge badge-light-" +
                    (status_pelaksanaan == "Selesai" ? "success" : "danger") +
                    " fs-7 m-1"
                  }
                >
                  {status_pelaksanaan}
                </span>
              </b>
            </div>
          </div>
        )}
      </>
    );
  }
}

export default HeaderInfoScreen;
