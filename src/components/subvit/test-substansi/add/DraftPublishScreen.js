import { Indonesian } from "flatpickr/dist/l10n/id";
import "line-awesome/dist/line-awesome/css/line-awesome.min.css";
import { Observer } from "mobx-react-lite";
import React from "react";
import DataTable from "react-data-table-component";
import Flatpickr from "react-flatpickr";
import Select from "react-select";
import {
  dateToIsoString,
  validateJumlahSoal,
  validatePassingGrade,
} from "../components/utils";
import _ from "lodash";

class DraftPublishScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidUpdate({ data: prevData }) {
    const { data } = this.props;
    const { jumlahSoal: prevJumlahSoal, passingGrade: prevPassingGrade } =
      prevData || {};
    const { questions, jumlahSoal, passingGrade, updateState } = data || {};

    if (prevJumlahSoal != jumlahSoal) {
      updateState("jumlahSoalError", validateJumlahSoal(jumlahSoal, questions));
    }

    if (prevPassingGrade != passingGrade) {
      updateState("passingGradeError", validatePassingGrade(passingGrade));
    }
  }

  render() {
    const { data } = this.props || {};
    const {
      questions,
      selectedLevel,
      mulaiPelaksanaan,
      mulaiPelaksanaanError,
      selesaiPelaksanaan,
      selesaiPelaksanaanError,
      jumlahSoal,
      jumlahSoalError,
      durasiDetik,
      durasiDetikError,
      passingGrade,
      passingGradeError,
      dataStatus = [],
      selectedStatus,
      selectedStatusError,
      updateState,
    } = data || {};
    const {
      mulaiPelaksanaanDisabled = false,
      selesaiPelaksanaanDisabled = false,
      durasiDetikDisabled = false,
      jumlahSoalDisabled = false,
    } = data || {};

    let {
      selectedPelatihanInfo,
      selectedKategori,
      listPelatihanByAkademiTema,
    } = data || {};
    listPelatihanByAkademiTema = _.uniqBy(listPelatihanByAkademiTema, "id");

    const {
      pendaftaran_mulai,
      pendaftaran_selesai,
      pelatihan_mulai,
      pelatihan_selesai,
      pendaftaran_start,
      pendaftaran_end,
      pelatihan_start,
      pelatihan_end,
      midtest_mulai,
      midtest_selesai,
      subtansi_mulai,
      subtansi_selesai,
    } = selectedPelatihanInfo || {};

    const { value: level = -1 } = selectedLevel || {};
    const { value: kategori = -1 } = selectedKategori || {};

    // let gDataAkademi = {};
    // let gDataTema = {};
    // let dataPelatihan = [];
    // (data?.pelatihan || []).forEach((p) => {
    //   const { akademi, akademi_id, pelatihan, pelatihan_id, pelatihan_mulai, pelatihan_selesai,
    //     status_akademi, status_pelatihan, status_publish, status_tema, tema, tema_id } = p || {};

    //   if (!Object.keys(gDataAkademi).includes(akademi_id)) gDataAkademi[akademi_id] = [];
    //   gDataAkademi[akademi_id] = [...gDataAkademi[akademi_id], {
    //     akademi_id, akademi, status_akademi
    //   }];

    //   if (!Object.keys(gDataTema).includes(tema_id)) gDataTema[tema_id] = [];
    //   gDataTema[tema_id] = [...gDataTema[tema_id], {
    //     tema_id, tema, status_tema
    //   }];

    //   dataPelatihan = [...dataPelatihan, {
    //     pelatihan_id, label: pelatihan, status: status_publish, status_pelatihan, pelatihan_mulai, pelatihan_selesai
    //   }];
    // });

    // let dataAkademi = [];
    // Object.keys(gDataAkademi).forEach((id) => {
    //   const [a] = gDataAkademi[id];
    //   const { akademi_id, akademi, status_akademi } = a || {};
    //   dataAkademi = [...dataAkademi, { akademi_id, label: akademi, status: status_akademi, jumlah: gDataAkademi[id].length }];
    // });

    // let dataTema = [];
    // Object.keys(gDataTema).forEach((id) => {
    //   const [a] = gDataTema[id];
    //   const { tema_id, tema, status_tema } = a || {};
    //   dataTema = [...dataTema, { tema_id, label: tema, status: status_tema, jumlah: gDataTema[id].length }];
    // });

    let columns = [
      {
        name: "No",
        width: "70px",
        center: true,
        cell: ({ idx }) => idx + 1,
      },
      {
        name: "Pelatihan",
        className: "min-w-300px mw-300px",
        selector: ({ name }) => (
          <div>
            <label className="d-flex flex-stack mb- mt-1">
              <span className="d-flex align-items-center me-2">
                <span className="d-flex flex-column">
                  <h6 className="fw-bolder fs-7 mb-0">{name}</h6>
                  {/* <span className="fs-7 text-muted fw-semibold">Nama Akademi disini</span> */}
                </span>
              </span>
            </label>
          </div>
        ),
      },
    ];

    if (kategori == 1) {
      columns = [
        ...columns,
        {
          name: "Pelaksanaan Test",
          width: "300px",
          // sortable: true,
          selector: ({ subtansi_mulai, subtansi_selesai }) => {
            // [subtansi_mulai] = subtansi_mulai.split(' ');
            // [subtansi_selesai] = subtansi_selesai.split(' ');
            return (
              <div>
                <div>
                  {subtansi_mulai} - {subtansi_selesai}
                </div>
              </div>
            );
          },
        },
        {
          name: "Status",
          width: "200px",
          // sortable: true,
          selector: ({
            status_pelaksanaan_substansi,
            subtansi_mulai,
            subtansi_selesai,
          }) => {
            // [subtansi_mulai] = subtansi_mulai.split(' ');
            // [subtansi_selesai] = subtansi_selesai.split(' ');

            let color = "secondary";
            if ((status_pelaksanaan_substansi || "").toLowerCase() == "selesai")
              color = "success";
            else if (
              (status_pelaksanaan_substansi || "").toLowerCase() ==
              "belum dilaksanakan"
            )
              color = "secondary";
            else if (
              (status_pelaksanaan_substansi || "").toLowerCase() ==
              "sedang berlangsung"
            )
              color = "primary";

            return (
              <div>
                {subtansi_mulai && subtansi_selesai && (
                  <div>
                    <span className={`badge badge-${color} fs-7 m-1`}>
                      {status_pelaksanaan_substansi}
                    </span>
                  </div>
                )}
              </div>
            );
          },
        },
      ];
    }

    if (kategori == 2) {
      columns = [
        ...columns,
        {
          name: "Pelaksanaan Test",
          width: "300px",
          // sortable: true,
          selector: ({
            midtest_mulai,
            midtest_selesai,
            status_pelaksanaan_midsubstansi,
          }) => {
            // [midtest_mulai] = midtest_mulai.split(' ');
            // [midtest_selesai] = midtest_selesai.split(' ');
            return (
              <div className="mt-2 text-center">
                <div>
                  {midtest_mulai} - {midtest_selesai}
                </div>
              </div>
            );
          },
        },
        {
          name: "Status",
          width: "150px",
          // sortable: true,
          selector: ({
            midtest_mulai,
            midtest_selesai,
            status_pelaksanaan_midsubstansi,
          }) => {
            // [midtest_mulai] = midtest_mulai.split(' ');
            // [midtest_selesai] = midtest_selesai.split(' ');
            return (
              <div>
                {midtest_mulai && midtest_selesai && (
                  <div>
                    <span className="badge badge-secondary fs-7 m-1">
                      {status_pelaksanaan_midsubstansi}
                    </span>
                  </div>
                )}
              </div>
            );
          },
        },
      ];
    }

    return (
      <>
        <div className="col-lg-12 mb-7 fv-row">
          {level == 0 && (
            <div className="row pt-7">
              <h2 className="fs-5 text-muted mb-3">Target Akademi</h2>
              <div className="col-lg-12">
                <div className="table-responsive">
                  <DataTable
                    columns={[
                      {
                        name: "No",
                        width: "70px",
                        center: true,
                        cell: ({ idx }) => idx + 1,
                      },
                      {
                        name: "Akademi",
                        selector: ({ label }) => (
                          <div className="mt-2">{label}</div>
                        ),
                      },
                      {
                        name: "Pelatihan",
                        width: "300px",
                        center: true,
                        selector: ({ jml_pelatihan }) => {
                          return <span>{jml_pelatihan}</span>;
                        },
                      },
                      {
                        name: "Status",
                        width: "200px",
                        center: true,
                        selector: ({ status }) => (
                          <span
                            className={
                              "badge badge-light-" +
                              ((status || "").toLowerCase() == "publish"
                                ? "success"
                                : "danger") +
                              " fs-7 m-1"
                            }
                          >
                            {(status || "").toLowerCase() == "publish"
                              ? "Publish"
                              : "Unpublish"}
                          </span>
                        ),
                      },
                    ]}
                    data={(data?.dataAkademi || [])
                      .filter((a) => {
                        if (data?.selectedLevel?.value >= 0)
                          return a?.value == data?.selectedAkademi?.value;
                        return true;
                      })
                      .filter(({ label }) => !!label)
                      .map((s, idx) => ({ idx, ...s }))}
                    highlightOnHover
                    pointerOnHover
                    pagination
                    // paginationTotalRows={state.soals.length}
                    // selectableRows
                    // selectableRowSelected={({ id = null } = {}) => state.selectedRows.map(({ id } = {}) => id).includes(id)}
                    // onSelectedRowsChange={({ selectedRows }) => state.setSelectedRows(selectedRows)}
                    customStyles={{
                      headCells: {
                        style: {
                          background: "rgb(243, 246, 249)",
                        },
                      },
                    }}
                    noDataComponent={<div className="mt-5">Tidak Ada Data</div>}
                    persistTableHead={true}
                  />
                </div>
              </div>
            </div>
          )}

          {level == 1 && (
            <div className="row pt-7">
              <h2 className="fs-5 text-muted mb-3">Target Tema</h2>
              <div className="col-lg-12">
                <div className="table-responsive">
                  <DataTable
                    columns={[
                      {
                        name: "No",
                        width: "70px",
                        center: true,
                        cell: ({ idx }) => idx + 1,
                      },
                      {
                        name: "Tema",
                        selector: ({ label }) => (
                          <div className="mt-2">{label}</div>
                        ),
                      },
                      {
                        name: "Pelatihan",
                        width: "300px",
                        center: true,
                        selector: ({ jml_pelatihan }) => {
                          return <span>{jml_pelatihan}</span>;
                        },
                      },
                      {
                        name: "Status",
                        width: "200px",
                        center: true,
                        selector: ({ status }) => (
                          <span
                            className={
                              "badge badge-light-" +
                              (status == 1 ? "success" : "danger") +
                              " fs-7 m-1"
                            }
                          >
                            {status == 1 ? "Publish" : "Unpublish"}
                          </span>
                        ),
                      },
                    ]}
                    data={(data?.dataTema || [])
                      .filter((a) => {
                        if (data?.selectedLevel?.value >= 1)
                          return a?.value == data?.selectedTema?.value;
                        return true;
                      })
                      .filter(({ label }) => !!label)
                      .map((s, idx) => ({ idx, ...s }))}
                    highlightOnHover
                    pointerOnHover
                    pagination
                    // paginationTotalRows={state.soals.length}
                    // selectableRows
                    // selectableRowSelected={({ id = null } = {}) => state.selectedRows.map(({ id } = {}) => id).includes(id)}
                    // onSelectedRowsChange={({ selectedRows }) => state.setSelectedRows(selectedRows)}
                    customStyles={{
                      headCells: {
                        style: {
                          width: "100%",
                          background: "rgb(243, 246, 249)",
                        },
                      },
                    }}
                    noDataComponent={<div className="mt-5">Tidak Ada Data</div>}
                    persistTableHead={true}
                  />
                </div>
              </div>
            </div>
          )}

          {level == 2 && (
            <div className="row pt-7">
              <h2 className="fs-5 text-muted mb-3">Target Pelatihan</h2>
              <div className="col-lg-12">
                <div className="table-responsive">
                  <DataTable
                    columns={columns}
                    data={(listPelatihanByAkademiTema || []).map((p, idx) => ({
                      ...p,
                      idx,
                    }))}
                    highlightOnHover
                    pointerOnHover
                    pagination
                    paginationTotalRows={
                      (listPelatihanByAkademiTema || []).length
                    }
                    customStyles={{
                      headCells: {
                        style: {
                          background: "rgb(243, 246, 249)",
                        },
                      },
                    }}
                    noDataComponent={<div className="mt-5">Tidak Ada Data</div>}
                    persistTableHead={true}
                  />
                </div>
              </div>
            </div>
          )}

          <div>
            <div className="mt-5 border-top mx-0 my-10"></div>
          </div>

          <h2 className="fs-5 mb-5">Ketentuan Test</h2>
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">Jumlah Soal</label>
            <Observer>
              {() => {
                return (
                  <>
                    <input
                      className="form-control form-control-sm"
                      placeholder="Masukkan jumlah soal"
                      name="pertanyaan"
                      value={jumlahSoal}
                      onChange={(e) => {
                        let intVal = parseInt(e.target.value);
                        if (isNaN(intVal)) intVal = 0;
                        updateState("jumlahSoal", intVal);
                      }}
                      disabled={jumlahSoalDisabled}
                    />
                    <span style={{ color: "red" }}>{jumlahSoalError}</span>
                  </>
                );
              }}
            </Observer>
          </div>
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">
              Durasi Pengerjaan (dalam menit)
            </label>
            <Observer>
              {() => (
                <>
                  <div className="input-group">
                    <input
                      type="text"
                      className="form-control form-control-sm"
                      aria-describedby="basic-addon2"
                      min="0"
                      max="300"
                      maxLength="3"
                      placeholder="Masukkan durasi pengerjaan"
                      value={durasiDetik}
                      onChange={(e) => {
                        let intVal = parseInt(e.target.value);
                        if (isNaN(intVal)) intVal = 0;
                        updateState("durasiDetik", intVal);
                      }}
                      disabled={durasiDetikDisabled}
                    />
                    <div className="input-group-append">
                      <span
                        className="input-group-text bg-light"
                        id="basic-addon2"
                      >
                        Menit
                      </span>
                    </div>
                  </div>
                  <span style={{ color: "red" }}>{durasiDetikError}</span>
                </>
              )}
            </Observer>
          </div>
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">
              Passing Grade (0 - 100)
            </label>
            <Observer>
              {() => (
                <>
                  <div className="input-group">
                    <input
                      type="text"
                      className="form-control form-control-sm"
                      aria-describedby="basic-addon2"
                      min="0"
                      max="100"
                      maxLength="3"
                      placeholder="Masukkan passing grade"
                      value={passingGrade}
                      onChange={(e) => {
                        let intVal = parseInt(e.target.value);
                        if (isNaN(intVal)) intVal = 0;
                        updateState("passingGrade", intVal);
                      }}
                    />
                  </div>
                  <span style={{ color: "red" }}>{passingGradeError}</span>
                </>
              )}
            </Observer>
          </div>
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">Status</label>
            <Observer>
              {() => {
                const [iselectedStatus] = dataStatus.filter(
                  ({ value }) => value == selectedStatus,
                );
                return (
                  <>
                    <Select
                      placeholder="Silahkan pilih"
                      // noOptionsMessage={({ inputValue }) => !inputValue ? this.state.dataxakademi : "Data tidak tersedia"}
                      value={iselectedStatus}
                      className="form-select-sm selectpicker p-0"
                      onChange={({ value }) =>
                        updateState("selectedStatus", value)
                      }
                      options={dataStatus}
                      isOptionDisabled={(option) => option.disabled}
                    />
                    <span style={{ color: "red" }}>{selectedStatusError}</span>
                  </>
                );
              }}
            </Observer>
          </div>
        </div>
      </>
    );
  }
}

export default DraftPublishScreen;
