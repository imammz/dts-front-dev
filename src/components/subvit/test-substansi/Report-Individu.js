import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import { capitalizeTheFirstLetterOfEachWord } from "../../publikasi/helper";

export default class ReportIndividu extends React.Component {
  constructor(props) {
    super(props);
    this.handleKembali = this.handleKembaliAction.bind(this);
  }
  state = {
    datax: [],
    loading: true,
    tempLastNumber: 0,
    nama: "",
    nama_test: "",
    nomor_registrasi: "",
    nama_pelatihan: "",
    nama_akademi: "",
    individu: {},
    nilai: "",
  };
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  columns = [
    {
      name: "No",
      sortable: false,
      center: true,
      width: "70px",
      grow: 1,
      cell: (row, index) => this.state.tempLastNumber + index + 1,
    },
    {
      name: "Pertanyaan",
      sortable: false,
      wrap: true,
      allowOverflow: false,
      selector: (row) => (
        <div>
          <h6 className="fw-bolder fs-7 mb-0">{row.soal}</h6>
        </div>
      ),
    },
    {
      name: "Jawaban",
      sortable: false,
      wrap: true,
      allowOverflow: false,
      selector: (row) => (
        <div>
          <h6 className="fw-bolder fs-7 mb-0">{row.jawaban}</h6>
        </div>
      ),
    },
    {
      name: "Kunci Jawaban",
      sortable: false,
      wrap: true,
      allowOverflow: false,
      selector: (row) => (
        <div>
          <h6 className="fw-bolder fs-7 mb-0">{row.jawaban_benar}</h6>
        </div>
      ),
    },
    {
      name: "Keterangan",
      sortable: false,
      wrap: true,
      allowOverflow: false,
      selector: (row) => (
        <div>
          <span
            className={
              row.answer_result == "Benar"
                ? "badge badge-light-success"
                : "badge badge-light-danger"
            }
          >
            {row.answer_result}
          </span>
        </div>
      ),
    },
  ];

  componentDidMount() {
    const temp_storage = localStorage.getItem("dataMenus");
    const individu = localStorage.getItem("individu_substansi");
    const individu_survey = localStorage.getItem("individu_survey");
    this.setState({
      individu: JSON.parse(individu),
    });
    localStorage.clear();
    localStorage.setItem("individu_substansi", individu);
    localStorage.setItem("individu_survey", individu_survey);
    localStorage.setItem("dataMenus", temp_storage);
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    let segment_url = window.location.pathname.split("/");
    let id_substansi = segment_url[4];
    let id_user = segment_url[5];
    let training_id = segment_url[6];

    const data = {
      id_substansi: id_substansi,
      user_id: id_user,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/tessubstansi/report-list-substansi_individu",
        data,
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        //const messagex = res.data.result.Message;
        let nama = "";
        let nomor_registrasi = "";
        let nama_pelatihan = "";
        let nama_akademi = "";
        let nilai = "";
        let status = "";
        const datax = [];
        if (statux) {
          const result_data = res.data.result.Data[0];
          if (result_data) {
            status = result_data.status;
            if (status != "sudah mengerjakan") {
              swal
                .fire({
                  title: "User Belum Mengerjakan",
                  text: "Silahkan Kembali",
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                    window.history.back();
                  }
                });
            }
            nomor_registrasi = result_data.noreg;
            nama = result_data.nama;
            nama_pelatihan = result_data.pelatihan;
            nama_akademi = result_data.akademi;
            nilai = result_data.nilai;
            const id_training = result_data.training_id;
            if (id_training == training_id) {
              const jawaban = result_data.jawaban;
              jawaban.forEach(function (element2) {
                let text_jawaban = "";
                let text_kunci = "";
                const question = {
                  soal: element2.question,
                  answer_result: element2.hasil,
                };
                const list_answer = element2.answer;
                list_answer.forEach(function (element3) {
                  if (element3.key == element2.answer_key) {
                    text_jawaban = element3.key + ". " + element3.option;
                    question.jawaban = text_jawaban;
                  }
                  if (element3.key == element2.jawaban_soal.toUpperCase()) {
                    text_kunci = element3.key + ". " + element3.option;
                    question.jawaban_benar = text_kunci;
                  }
                });
                datax.push(question);
              });
            }
          }

          this.setState({
            loading: false,
            datax: datax,
            nama: nama,
            nomor_registrasi: nomor_registrasi,
            nama_pelatihan,
            nama_akademi,
            nilai: nilai,
          });
          /* const datax = res.data.result.Data[0];
                    this.setState({
                        nama_survey: datax.nama_survey,
                    }); */
        }
      })
      .catch((error) => {
        swal
          .fire({
            title: "Kesalahan",
            text: "Data Peserta Tidak Ditemukan",
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
              window.history.back();
            }
          });
      });
  }

  handleKembaliAction() {
    window.history.back();
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <path
                        opacity="0.3"
                        d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                        fill="#7239ea"
                      ></path>
                      <path
                        d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                        fill="#7239ea"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Subvit
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                    Test Substansi
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Report Individu
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <a
                  onClick={this.handleKembali}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-7"
          id="kt_wrapper"
        >
          <div
            className="d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-5">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Report Individu
                        </h1>
                      </div>
                      <div className="card-toolbar"></div>
                    </div>

                    <div className="card-body">
                      <div className="row mb-5">
                        <div className="col-lg-12 col-12 col-md-12">
                          <div className="d-flex flex-row">
                            <label className="d-flex flex-stack align-items-left">
                              <span className="d-flex align-items-center me-2">
                                <span className="symbol symbol-50px me-6">
                                  <span className="symbol-label bg-light-primary">
                                    <span className="svg-icon svg-icon-1 svg-icon-primary">
                                      <img
                                        src={`${
                                          process.env.REACT_APP_BASE_API_URI
                                        }/download/get-file?path=${
                                          this.state.individu.foto || ""
                                        }`}
                                        alt=""
                                        className="symbol-label"
                                      />
                                    </span>
                                  </span>
                                </span>
                                <span className="d-flex flex-column">
                                  <span className="fs-7 fw-semibold text-muted">
                                    {this.state.nomor_registrasi}
                                  </span>
                                  <span
                                    className="fw-bolder fs-7"
                                    style={{
                                      overflow: "hidden",
                                      whiteSpace: "normal",
                                    }}
                                  >
                                    {capitalizeTheFirstLetterOfEachWord(
                                      this.state.nama,
                                    )}
                                  </span>
                                </span>
                              </span>
                            </label>
                            <label className="d-flex flex-stack align-items-left ms-4">
                              <span className="d-flex align-items-center me-2">
                                <span className="d-flex flex-column">
                                  <h6 className="fw-bolder fs-7 mb-0">
                                    {this.state.nama_pelatihan}
                                  </h6>
                                  <span className="text-muted fs-7 fw-semibold">
                                    {this.state.nama_akademi}
                                  </span>
                                </span>
                              </span>
                            </label>
                            <div className="d-flex ms-4 me-4">
                              <div>
                                <div>
                                  Score: <b>{this.state.nilai}</b>
                                  <br />
                                  Passing Grade:{" "}
                                  <b>{this.state.individu.passing_grade}</b>
                                </div>
                                <div>
                                  <span
                                    className={
                                      this.state.individu.status_eligible ==
                                      "Not Eligible"
                                        ? "badge badge-light-danger"
                                        : "badge badge-light-success"
                                    }
                                  >
                                    {this.state.individu.status_eligible}
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div className="d-flex ms-4 me-4">
                              <div>
                                <div>
                                  <i className="bi bi-files me-1"></i>
                                  {this.state.individu.ptotalpertanyaan} Soal
                                </div>
                                <div>
                                  <i className="bi bi-check-circle-fill text-success me-1"></i>
                                  {this.state.individu.jwabanbenar} Benar
                                </div>
                                <div>
                                  <i className="bi bi-x-circle-fill text-danger me-1"></i>
                                  {this.state.individu.jwabansalah} Salah
                                </div>
                              </div>
                            </div>
                            <div className="d-flex ms-4">
                              <div data-tag="allowRowEvents">
                                <div>
                                  <i className="bi bi-calendar me-1"></i>
                                  {this.state.individu.tgl_pengerjaan}
                                </div>
                                <div>
                                  <i className="bi bi-clock me-1"></i>
                                  {this.state.individu.menit} Menit
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="table-responsive">
                        <DataTable
                          columns={this.columns}
                          data={this.state.datax}
                          progressPending={this.state.loading}
                          highlightOnHover
                          pointerOnHover
                          pagination={false}
                          customStyles={this.customStyles}
                          persistTableHead={true}
                          noDataComponent={
                            <div className="mt-5">Tidak Ada Data</div>
                          }
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
