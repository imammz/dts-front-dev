import Cookies from "js-cookie";
import "line-awesome/dist/line-awesome/css/line-awesome.min.css";
import React from "react";
import DataTable from "react-data-table-component";
import Swal from "sweetalert2";
import Footer from "../../../Footer";
import Header from "../../../Header";
import SideNav from "../../../SideNav";
import { withRouter } from "../components/utils";
import {
  capitalizeFirstLetter,
  capitalizeTheFirstLetterOfEachWord,
  indonesianDateFormat,
} from "../../../publikasi/helper";
import {
  reportTestSubstansi,
  resetPesertaSubstansi,
} from "../components/actions";
import { utils, write, writeXLSX } from "xlsx";
import { saveAs } from "file-saver";
import _ from "lodash";
import Select from "react-select";
import { capitalWord } from "../../../pelatihan/Pelatihan/helper";

function s2ab(s) {
  if (typeof ArrayBuffer !== "undefined") {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xff;
    return buf;
  } else {
    var buf = new Array(s.length);
    for (var i = 0; i != s.length; ++i) buf[i] = s.charCodeAt(i) & 0xff;
    return buf;
  }
}

const sortFields = [
  "_",
  "_",
  "nama",
  "nama_akademi",
  "start_datetime",
  "total_jawab",
  "status",
];

class Content extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentRowsPerPage: 10,
      currentPage: 1,
      sortId: 3,
      sortDir: "asc",

      keyword: "",
      dataStatus: [
        { value: 99, label: "Semua" },
        { value: "Eligible", label: "Eligible" },
        { value: "Not Eligible", label: "Not Eligible" },
      ],
      selectedStatus: 99,
      dataNilai: [
        { value: 1000, label: "Semua" },
        { value: 10, label: "10" },
        { value: 20, label: "20" },
        { value: 30, label: "30" },
        { value: 40, label: "40" },
        { value: 50, label: "50" },
        { value: 60, label: "60" },
        { value: 70, label: "70" },
        { value: 80, label: "80" },
        { value: 90, label: "90" },
        { value: 100, label: "100" },
      ],
      selectedNilai: 1000,

      Dashboard: {},
      JumlahData: 0,
      Data: [],

      selectedTableRows: [],
      clearSelectedRows: false,
    };
  }

  fetch = async (start, rows, sortField, sortDir, keyword = "") => {
    let Dashboard = {},
      JumlahData = 0,
      Data = [];

    if (Cookies.get("token") == null) {
      Swal.fire({
        title: "Unauthenticated.",
        text: "Silahkan login ulang",
        icon: "warning",
        confirmButtonText: "Ok",
      }).then((result) => {
        if (result.isConfirmed) {
          window.location = "/";
        }
      });
    }

    try {
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });

      const { params } = this.props || {};
      const { id } = params || {};

      const {
        dash,
        jml_data,
        Data: data,
      } = await reportTestSubstansi(
        id,
        start,
        rows,
        sortField,
        sortDir,
        keyword,
      );
      Dashboard = dash;
      JumlahData = jml_data;
      Data = data;

      Swal.close();
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }

    this.setState({ Dashboard, JumlahData, Data });
  };

  doFetch = async () => {
    let Dashboard = {},
      JumlahData = 0,
      Data = [];

    if (Cookies.get("token") == null) {
      Swal.fire({
        title: "Unauthenticated.",
        text: "Silahkan login ulang",
        icon: "warning",
        confirmButtonText: "Ok",
      }).then((result) => {
        if (result.isConfirmed) {
          window.location = "/";
        }
      });
    }

    try {
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });

      const { params } = this.props || {};
      const { id } = params || {};

      const {
        dash,
        jml_data,
        Data: data,
      } = await reportTestSubstansi(
        id,
        (this.state.currentPage - 1) * this.state.currentRowsPerPage,
        this.state.currentRowsPerPage,
        sortFields[this.state.sortId],
        this.state.sortDir,
        this.state.keyword,
        this.state.selectedStatus,
        this.state.selectedNilai,
      );
      Dashboard = dash;
      JumlahData = jml_data;
      Data = data;

      Swal.close();
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }

    this.setState({ Dashboard, JumlahData, Data });
  };

  onChangePage = (currentPage) => {
    // this.fetch(
    //   (currentPage - 1) * this.state.currentRowsPerPage,
    //   this.state.currentRowsPerPage,
    //   sortFields[this.state.sortId],
    //   this.state.sortDir
    // );

    this.setState({ currentPage }, () => this.doFetch());
  };

  onChangeRowsPerPage = (currentRowsPerPage, currentPage) => {
    // this.fetch(
    //   (currentPage - 1) * currentRowsPerPage,
    //   currentRowsPerPage,
    //   sortFields[this.state.sortId],
    //   this.state.sortDir
    // );

    this.setState(
      {
        currentRowsPerPage,
        currentPage,
      },
      () => this.doFetch(),
    );
  };

  onSort = ({ id: sortId }, sortDir) => {
    // this.fetch(
    //   (this.state.currentPage - 1) * this.state.currentRowsPerPage,
    //   this.state.currentRowsPerPage,
    //   sortFields[sortId],
    //   sortDir
    // );

    this.setState(
      {
        sortId,
        sortDir,
      },
      () => this.doFetch(),
    );
  };

  search = (keyword) => {
    // this.fetch(
    //   (this.state.currentPage - 1) * this.state.currentRowsPerPage,
    //   this.state.currentRowsPerPage,
    //   sortFields[this.state.sortId],
    //   this.state.sortDir,
    //   keyword
    // );

    this.setState({ keyword }, () => this.doFetch());
  };

  exportReport = async () => {
    const { params } = this.props || {};
    const { id } = params || {};

    const formatDate = (tgl_pengerjaan) => {
      const tanggal_indo = indonesianDateFormat(tgl_pengerjaan);
      const tgl = tanggal_indo.split(" ");
      return tgl[2] + " " + tgl[1] + " " + tgl[0];
    };

    const { dash, jml_data, Data } = await reportTestSubstansi(
      id,
      0,
      this.state.JumlahData,
      "nama_akademi",
      "asc",
      "",
      99,
      1000,
    );
    const pelatihanName = (Data[0] || {})?.pelatihan || "";
    const tglPengerjaan = formatDate((Data[0] || {})?.tgl_pengerjaan);

    const wb = utils.book_new();
    wb.SheetNames.push("Worksheet");
    wb.Sheets["Worksheet"] = utils.json_to_sheet(
      Data.map(
        ({
          nama,
          nomor_registrasi,
          // email,
          // nik,
          akademi,
          pelatihan,
          tgl_pengerjaan,
          menit,
          nilai,
          pstatus,
          jwabanbenar,
          jwabansalah,
          ptotalpertanyaan,
        }) => ({
          "Nama Peserta": nama,
          "Nomor Registrasi": nomor_registrasi,
          //Email: email,
          // NIK: nik,
          Akademi: akademi,
          Pelatihan: pelatihan,
          Pelaksanaan: formatDate(tgl_pengerjaan),
          "Lama Pengerjaan (Menit)": menit,
          "Jawaban Benar": jwabanbenar,
          "Jawaban Salah": jwabansalah,
          "Total Pertanyaan": ptotalpertanyaan,
          "Nilai / Points": nilai,
          Status: pstatus,
        }),
      ),
    );
    const wbout = write(wb, {
      bookType: "xlsx",
      bookSST: true,
      type: "binary",
    });
    saveAs(
      new Blob([s2ab(wbout)], { type: "application/octet-stream" }),
      `Hasil Substansi ${pelatihanName} - ${tglPengerjaan}.xlsx`,
    );
  };

  componentDidMount() {
    // this.fetch(
    //   (this.state.currentPage - 1) * this.state.currentRowsPerPage,
    //   this.state.currentRowsPerPage,
    //   sortFields[this.state.sortId],
    //   this.state.sortDir
    // );

    this.doFetch();
  }

  resetPelaksanaan(rows) {
    (async () => {
      const { isConfirmed = false } = await Swal.fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      });

      if (isConfirmed) {
        try {
          const Message = await resetPesertaSubstansi(rows);

          Swal.close();

          await Swal.fire({
            title: "Data pelaksanaan substansi berhasil direset",
            icon: "success",
            confirmButtonText: "Ok",
          });

          // this.fetch(this.state);
          window.location.reload();
        } catch (err) {
          await Swal.fire({
            title: err,
            icon: "warning",
            confirmButtonText: "Ok",
          });
        }
      }
    })();
  }

  render() {
    const { params } = this.props || {};
    const { id } = params || {};

    const {
      jml_blm_mengerjakan,
      jml_peserta,
      jml_sedang_mengerjakan,
      jml_sudah_mengerjakan,
    } = this.state.Dashboard || {};

    const Checkbox = React.forwardRef(({ onClick, ...rest }, ref) => {
      return (
        <>
          <div className="form-check" style={{ backgroundColor: "" }}>
            <input
              type="checkbox"
              className="form-check-input"
              ref={ref}
              onClick={onClick}
              {...rest}
            />
            <label className="form-check-label" id="booty-check" />
          </div>
        </>
      );
    });

    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <path
                        opacity="0.3"
                        d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                        fill="#7239ea"
                      ></path>
                      <path
                        d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                        fill="#7239ea"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Subvit
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Test Substansi
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={() =>
                    this?.props?.history && this?.props?.history(-1)
                  }
                  className="btn btn-sm btn-light"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
                <button
                  className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder ms-2"
                  data-bs-toggle="modal"
                  data-bs-target="#modal_filter"
                >
                  <i className="bi bi-sliders"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Filter
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-7"
          id="kt_wrapper"
        >
          <div
            className="d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-12">
                    <div className="row">
                      <div className="col-xl-3 mb-3">
                        <div className="card">
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-primary mb-3 mt-1">
                                <span className="svg-icon svg-icon-primary svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <path
                                      opacity="0.3"
                                      d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM12.5 18C12.5 17.4 12.6 17.5 12 17.5H8.5C7.9 17.5 8 17.4 8 18C8 18.6 7.9 18.5 8.5 18.5L12 18C12.6 18 12.5 18.6 12.5 18ZM16.5 13C16.5 12.4 16.6 12.5 16 12.5H8.5C7.9 12.5 8 12.4 8 13C8 13.6 7.9 13.5 8.5 13.5H15.5C16.1 13.5 16.5 13.6 16.5 13ZM12.5 8C12.5 7.4 12.6 7.5 12 7.5H8C7.4 7.5 7.5 7.4 7.5 8C7.5 8.6 7.4 8.5 8 8.5H12C12.6 8.5 12.5 8.6 12.5 8Z"
                                      fill="currentColor"
                                    ></path>
                                    <rect
                                      x="7"
                                      y="17"
                                      width="6"
                                      height="2"
                                      rx="1"
                                      fill="currentColor"
                                    ></rect>
                                    <rect
                                      x="7"
                                      y="12"
                                      width="10"
                                      height="2"
                                      rx="1"
                                      fill="currentColor"
                                    ></rect>
                                    <rect
                                      x="7"
                                      y="7"
                                      width="6"
                                      height="2"
                                      rx="1"
                                      fill="currentColor"
                                    ></rect>
                                    <path
                                      d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z"
                                      fill="currentColor"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {jml_peserta}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Total Peserta
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-xl-3 mb-3">
                        <div className="card">
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-success mb-3 mt-1">
                                <span className="svg-icon svg-icon-success svg-icon-3x">
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24px"
                                    height="24px"
                                    viewBox="0 0 24 24"
                                    className="mh-50px"
                                  >
                                    <path
                                      d="M10.0813 3.7242C10.8849 2.16438 13.1151 2.16438 13.9187 3.7242V3.7242C14.4016 4.66147 15.4909 5.1127 16.4951 4.79139V4.79139C18.1663 4.25668 19.7433 5.83365 19.2086 7.50485V7.50485C18.8873 8.50905 19.3385 9.59842 20.2758 10.0813V10.0813C21.8356 10.8849 21.8356 13.1151 20.2758 13.9187V13.9187C19.3385 14.4016 18.8873 15.491 19.2086 16.4951V16.4951C19.7433 18.1663 18.1663 19.7433 16.4951 19.2086V19.2086C15.491 18.8873 14.4016 19.3385 13.9187 20.2758V20.2758C13.1151 21.8356 10.8849 21.8356 10.0813 20.2758V20.2758C9.59842 19.3385 8.50905 18.8873 7.50485 19.2086V19.2086C5.83365 19.7433 4.25668 18.1663 4.79139 16.4951V16.4951C5.1127 15.491 4.66147 14.4016 3.7242 13.9187V13.9187C2.16438 13.1151 2.16438 10.8849 3.7242 10.0813V10.0813C4.66147 9.59842 5.1127 8.50905 4.79139 7.50485V7.50485C4.25668 5.83365 5.83365 4.25668 7.50485 4.79139V4.79139C8.50905 5.1127 9.59842 4.66147 10.0813 3.7242V3.7242Z"
                                      fill="#50cd89"
                                    ></path>
                                    <path
                                      className="permanent"
                                      d="M14.8563 9.1903C15.0606 8.94984 15.3771 8.9385 15.6175 9.14289C15.858 9.34728 15.8229 9.66433 15.6185 9.9048L11.863 14.6558C11.6554 14.9001 11.2876 14.9258 11.048 14.7128L8.47656 12.4271C8.24068 12.2174 8.21944 11.8563 8.42911 11.6204C8.63877 11.3845 8.99996 11.3633 9.23583 11.5729L11.3706 13.4705L14.8563 9.1903Z"
                                      fill="white"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {jml_sudah_mengerjakan}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Sudah Mengerjakan
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-xl-3 mb-3">
                        <div className="card">
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-warning mb-3 mt-1">
                                <span className="svg-icon svg-icon-warning svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <path
                                      opacity="0.3"
                                      d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                      fill="#ffc700"
                                    ></path>
                                    <path
                                      d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                      fill="#ffc700"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {jml_sedang_mengerjakan}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Sedang Mengerjakan
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="col-xl-3 mb-3">
                        <div className="card">
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-danger mb-3 mt-1">
                                <span className="svg-icon svg-icon-danger svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="#dc3545"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <rect
                                      opacity="0.3"
                                      x="2"
                                      y="2"
                                      width="20"
                                      height="20"
                                      rx="10"
                                      fill="#f1416c"
                                    ></rect>
                                    <rect
                                      x="7"
                                      y="15.3137"
                                      width="12"
                                      height="2"
                                      rx="1"
                                      transform="rotate(-45 7 15.3137)"
                                      fill="#dc3545"
                                    ></rect>
                                    <rect
                                      x="8.41422"
                                      y="7"
                                      width="12"
                                      height="2"
                                      rx="1"
                                      transform="rotate(45 8.41422 7)"
                                      fill="#dc3545"
                                    ></rect>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {jml_blm_mengerjakan}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Belum Mengerjakan
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-lg-12 mt-5">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Report Test Substansi
                        </h1>
                      </div>
                      <div className="card-toolbar">
                        <div className="d-flex align-items-center position-relative my-1 me-2">
                          <span className="svg-icon svg-icon-1 position-absolute ms-6">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                              className="mh-50px"
                            >
                              <rect
                                opacity="0.5"
                                x="17.0365"
                                y="15.1223"
                                width="8.15546"
                                height="2"
                                rx="1"
                                transform="rotate(45 17.0365 15.1223)"
                                fill="currentColor"
                              ></rect>
                              <path
                                d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                fill="currentColor"
                              ></path>
                            </svg>
                          </span>
                          <input
                            type="text"
                            data-kt-user-table-filter="search"
                            className="form-control form-control-sm form-control-solid w-250px ps-14 me-2"
                            placeholder="Cari Peserta"
                            onKeyDown={(e) => {
                              const keyword = e.target.value;
                              if (e.key == "Enter") {
                                this.search(keyword);
                                e.preventDefault();
                              }
                            }}
                            onChange={(e) => {
                              const keyword = e.target.value;
                              if (!keyword) {
                                this.search(keyword);
                                e.preventDefault();
                              }
                            }}
                          />

                          <a
                            href="#"
                            onClick={() => this.exportReport()}
                            className="btn btn-sm btn-flex btn-light fw-bolder my-1 me-2"
                          >
                            <i className="bi bi-cloud-download"></i>
                            <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                              Export
                            </span>
                          </a>
                          {this.state.selectedTableRows.length > 0 && (
                            <button
                              onClick={() =>
                                this.resetPelaksanaan(
                                  this.state.selectedTableRows.map(
                                    ({
                                      training_id,
                                      nomor_registrasi,
                                      nik,
                                    }) => ({
                                      id,
                                      id_pelatihan: training_id,
                                      nomor_registrasi,
                                      nik,
                                    }),
                                  ),
                                )
                              }
                              className="btn btn-sm btn-primary btn-flex btn-light fw-bolder"
                            >
                              <i className="bi bi-x-circle"></i>
                              <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                Reset Pelaksanaan
                              </span>
                            </button>
                          )}
                        </div>
                      </div>
                    </div>

                    <div className="card-body">
                      <div className="table-responsive">
                        <DataTable
                          columns={[
                            {
                              name: "No",
                              width: "70px",
                              center: true,
                              cell: (row, index) =>
                                (this.state.currentPage - 1) *
                                  this.state.currentRowsPerPage +
                                index +
                                1,
                            },
                            {
                              name: "Nama Peserta",
                              sortable: true,
                              className: "min-w-300px mw-300px",
                              width: "300px",
                              grow: 6,
                              center: false,
                              selector: ({
                                nama,
                                email,
                                nomor_registrasi,
                                foto,
                              }) => {
                                return (
                                  <div>
                                    <label className="d-flex flex-stack my-2 cursor-pointer">
                                      <span className="d-flex align-items-center me-2">
                                        <span className="symbol symbol-50px me-6">
                                          <span className="symbol-label bg-light-primary">
                                            <span className="svg-icon svg-icon-1 svg-icon-primary">
                                              <img
                                                src={`${
                                                  process.env
                                                    .REACT_APP_BASE_API_URI
                                                }/download/get-file?path=${
                                                  foto || ""
                                                }`}
                                                alt=""
                                                className="symbol-label"
                                              />
                                            </span>
                                          </span>
                                        </span>
                                        <span className="d-flex flex-column">
                                          <span className="fs-7 fw-semibold text-muted">
                                            {nomor_registrasi}
                                          </span>
                                          <span
                                            className="fw-bolder fs-7"
                                            style={{
                                              overflow: "hidden",
                                              whiteSpace: "wrap",
                                              textOverflow: "ellipses",
                                            }}
                                          >
                                            {capitalizeTheFirstLetterOfEachWord(
                                              nama,
                                            )}
                                          </span>
                                          {/*<span className="text-muted fs-7 fw-semibold">
                                              {email}
                                            </span>*/}
                                        </span>
                                      </span>
                                    </label>
                                  </div>
                                );
                              },
                            },
                            {
                              name: "Pelatihan",
                              sortable: true,
                              className: "min-w-300px mw-300px",
                              width: "300px",
                              grow: 6,
                              center: false,
                              selector: ({ akademi, pelatihan }) => {
                                return (
                                  <div>
                                    <label className="d-flex flex-stack mb- mt-1">
                                      <span className="d-flex align-items-center me-2">
                                        <span className="d-flex flex-column">
                                          <h6 className="fw-bolder fs-7 mb-0">
                                            {capitalizeTheFirstLetterOfEachWord(
                                              pelatihan,
                                            )}
                                          </h6>
                                          <span className="text-muted fs-7 fw-semibold">
                                            {capitalizeTheFirstLetterOfEachWord(
                                              akademi,
                                            )}
                                          </span>
                                        </span>
                                      </span>
                                    </label>
                                  </div>
                                );
                              },
                            },
                            {
                              name: "Nilai",
                              sortable: true,
                              center: false,
                              selector: (row) => {
                                // return "Dijawab : " + total_jawab;
                                // return <>
                                //   <div className='text-center'>{isNaN(nilai) ? nilai : parseFloat(nilai).toFixed(2)}</div>
                                // </>

                                return (
                                  <div>
                                    {/* <br></br> */}
                                    <div>
                                      Score: <b>{row.nilai ?? "-"}</b>
                                      <br />
                                      Passing Grade:{" "}
                                      <b>{row.passing_grade ?? "-"}</b>
                                    </div>
                                    <div>
                                      <span
                                        className={`badge ${
                                          row.status_eligible == "Eligible"
                                            ? "badge-light-success"
                                            : "badge-light-danger"
                                        }`}
                                      >
                                        {capitalWord(
                                          row.status_eligible ?? "-",
                                        )}
                                      </span>
                                    </div>
                                  </div>
                                );
                              },
                            },
                            {
                              name: "Jawaban",
                              sortable: true,
                              width: "180px",
                              center: false,
                              selector: ({
                                total_jawab,
                                ptotalpertanyaan,
                                jwabanbenar,
                                jwabansalah,
                              }) => {
                                // return "Dijawab : " + total_jawab;
                                if (
                                  !(
                                    ptotalpertanyaan ||
                                    jwabanbenar ||
                                    jwabansalah
                                  )
                                )
                                  return "-";
                                return (
                                  <>
                                    <div>
                                      <i className="bi bi-files me-1"></i>
                                      {ptotalpertanyaan ?? "-"} Soal
                                    </div>
                                    <div>
                                      <i className="bi bi-check-circle-fill text-success me-1"></i>
                                      {jwabanbenar ?? "-"} Benar
                                    </div>
                                    <div>
                                      <i className="bi bi-x-circle-fill text-danger me-1"></i>
                                      {jwabansalah ?? "-"} Salah
                                    </div>
                                  </>
                                );
                              },
                            },
                            {
                              name: "Pelaksanaan",
                              sortable: true,
                              width: "200px",
                              center: false,
                              selector: ({ tgl_pengerjaan, menit }) => {
                                if (tgl_pengerjaan) {
                                  const tanggal_indo =
                                    indonesianDateFormat(tgl_pengerjaan);
                                  const tgl = tanggal_indo.split(" ");
                                  return (
                                    <>
                                      <div>
                                        <i className="bi bi-calendar me-1"></i>
                                        {tgl_pengerjaan
                                          ? tgl[2] + " " + tgl[1] + " " + tgl[0]
                                          : "-"}
                                      </div>
                                      <div>
                                        <i className="bi bi-clock me-1"></i>
                                        {menit || "-"} Menit
                                      </div>
                                    </>
                                  );
                                } else {
                                  return "-";
                                }
                              },
                            },
                            {
                              name: "Status",
                              sortable: true,
                              center: true,
                              width: "200px",
                              center: false,
                              cell: ({ pstatus }) => (
                                <div>
                                  <span
                                    className={
                                      "badge badge-light-" +
                                      (pstatus == "sudah mengerjakan"
                                        ? "success"
                                        : "danger") +
                                      " fs-7 m-1"
                                    }
                                  >
                                    {capitalizeTheFirstLetterOfEachWord(
                                      pstatus,
                                    )}
                                  </span>
                                </div>
                              ),
                            },
                            {
                              name: "Aksi",
                              center: true,
                              width: "150px",
                              cell: ({
                                foto,
                                nilai,
                                status_eligible,
                                ptotalpertanyaan,
                                passing_grade,
                                jwabanbenar,
                                jwabansalah,
                                tgl_pengerjaan,
                                menit,
                                training_id,
                                nomor_registrasi,
                                nik,
                                pstatus,
                                user_id,
                              }) => (
                                <div className="row">
                                  <div className="col-6">
                                    <button
                                      title="Report Individu"
                                      className={`btn btn-icon btn-primary btn-sm ${
                                        pstatus == "sudah mengerjakan"
                                          ? ""
                                          : "disabled"
                                      }`}
                                      onClick={() => {
                                        let tanggal_pengerjaan = "-";
                                        if (tgl_pengerjaan) {
                                          const tanggal_indo =
                                            indonesianDateFormat(
                                              tgl_pengerjaan,
                                            );
                                          const tgl = tanggal_indo.split(" ");
                                          tanggal_pengerjaan =
                                            tgl[2] +
                                            " " +
                                            tgl[1] +
                                            " " +
                                            tgl[0];
                                        }
                                        let minutes = "-";
                                        if (menit > 0) {
                                          minutes = menit;
                                        }
                                        const individu = {
                                          foto: foto,
                                          nilai: nilai,
                                          status_eligible: status_eligible,
                                          ptotalpertanyaan: ptotalpertanyaan,
                                          jwabanbenar: jwabanbenar,
                                          jwabansalah: jwabansalah,
                                          tgl_pengerjaan: tanggal_pengerjaan,
                                          menit: minutes,
                                          passing_grade: passing_grade,
                                        };
                                        localStorage.setItem(
                                          "individu_substansi",
                                          JSON.stringify(individu),
                                        );
                                        window.location =
                                          "/subvit/test-substansi/report/" +
                                          id +
                                          "/" +
                                          user_id +
                                          "/" +
                                          training_id;
                                      }}
                                    >
                                      <i className="bi bi-file-earmark-medical"></i>
                                    </button>
                                  </div>
                                  <div className="col-6">
                                    <button
                                      title="Reset Pelaksanaan"
                                      className={`btn btn-icon btn-primary btn-sm ${
                                        pstatus == "sudah mengerjakan"
                                          ? ""
                                          : "disabled"
                                      }`}
                                      onClick={() =>
                                        this.resetPelaksanaan([
                                          {
                                            id,
                                            id_pelatihan: training_id,
                                            nomor_registrasi,
                                            nik,
                                          },
                                        ])
                                      }
                                    >
                                      <i className="bi bi-x-circle"></i>
                                    </button>
                                  </div>
                                </div>
                              ),
                            },
                          ]}
                          data={this.state.Data}
                          // progressPending={this.state.loading}
                          highlightOnHover
                          pointerOnHover
                          pagination
                          paginationServer
                          paginationTotalRows={this.state.JumlahData}
                          paginationComponentOptions={{
                            selectAllRowsItem: true,
                            selectAllRowsItemText: "Semua",
                          }}
                          onChangeRowsPerPage={this.onChangeRowsPerPage}
                          onChangePage={this.onChangePage}
                          // customStyles={this.customStyles}
                          persistTableHead={true}
                          noDataComponent={
                            <div className="mt-5">Tidak Ada Data</div>
                          }
                          onSort={this.onSort}
                          sortServer
                          selectableRows
                          selectableRowsComponent={Checkbox}
                          onSelectedRowsChange={(evt) => {
                            this.setState({
                              selectedTableRows: evt.selectedRows,
                            });
                          }}
                          clearSelectedRows={this.state.clearSelectedRows}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {this.renderFilter()}
      </div>
    );
  }

  renderFilter() {
    return (
      <div className="modal fade" tabindex="-1" id="modal_filter">
        <div className="modal-dialog modal-lg">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">
                <span className="svg-icon svg-icon-5 me-1">
                  <i className="bi bi-sliders text-black"></i>
                </span>
                Filter Report Tes Substansi
              </h5>
              <div
                className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal"
                aria-label="Close"
              >
                <span className="svg-icon svg-icon-2x">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <rect
                      opacity="0.5"
                      x="6"
                      y="17.3137"
                      width="16"
                      height="2"
                      rx="1"
                      transform="rotate(-45 6 17.3137)"
                      fill="currentColor"
                    />
                    <rect
                      x="7.41422"
                      y="6"
                      width="16"
                      height="2"
                      rx="1"
                      transform="rotate(45 7.41422 6)"
                      fill="currentColor"
                    />
                  </svg>
                </span>
              </div>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-lg-12 mb-7 fv-row">
                  <label className="form-label">Status</label>
                  <Select
                    placeholder="Silahkan pilih"
                    className="form-select-sm selectpicker p-0"
                    value={
                      this.state.dataStatus.find(
                        (s) => s?.value == this.state.selectedStatus,
                      ) || null
                    }
                    onChange={({ value }) =>
                      this.setState({ selectedStatus: value })
                    }
                    options={this.state.dataStatus}
                  />
                </div>
              </div>
              {/* <div className="row">
              <div className="col-lg-12 mb-7 fv-row">
                <label className="form-label">Nilai</label>
                <Select
                  placeholder="Silahkan pilih"
                  className="form-select-sm selectpicker p-0"
                  value={this.state.dataNilai.find(s => s?.value == this.state.selectedNilai) || null}
                  onChange={({ value }) =>
                    this.setState({ selectedNilai: value })
                  }
                  options={this.state.dataNilai}
                />
              </div>
            </div> */}
            </div>
            <div className="modal-footer">
              <div className="d-flex justify-content-between">
                <button
                  type="reset"
                  className="btn btn-sm btn-light me-3"
                  onClick={() => {
                    this.setState(
                      {
                        selectedStatus: 0,
                        selectedNilai: 0,
                      },
                      () => this.doFetch(),
                    );
                  }}
                >
                  Reset
                </button>
                <button
                  type="submit"
                  className="btn btn-sm btn-primary"
                  data-bs-dismiss="modal"
                  onClick={() => this.doFetch()}
                >
                  Apply Filter
                </button>
              </div>
            </div>
            {/* </form> */}
          </div>
        </div>
      </div>
    );
  }
}

const AddTrivia = (props) => {
  return (
    <>
      <SideNav />
      <Header />
      <Content {...props} />
      <Footer />
    </>
  );
};

export default withRouter(AddTrivia);
