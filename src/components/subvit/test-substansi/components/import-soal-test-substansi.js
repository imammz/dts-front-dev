import { observable } from "mobx";
import { Observer } from "mobx-react-lite";
import React from "react";
import Select from "react-select";
import imageCompression from "browser-image-compression";
import template from "../Template_Soal_Test_Substansi.xlsx";
import { read, utils } from "xlsx";
import Swal from "sweetalert2";

export const defaultQuestion = {
  question: "",
  question_image: null,
  question_type_id: null,
  status: null,
  answer: [],
  answer_key: "",
};
export const defaultErrorMessage = {
  question: null,
  question_type_id: null,
  status: null,
  answer: [],
  answer_key: null,
  answer_num: null,
};

export const astate = observable({
  fileTemplateXlsx: null,
  fileTemplateImages: [],
  dataQuestions: [],

  question: defaultQuestion,
  error: defaultErrorMessage,
  needSave: false,
});

export const ImportComponent = ({
  question: q = defaultQuestion,
  tipeSoal = { data: [], mapper: (d) => d, findById: (id) => ({}) },
}) => {
  return (
    <>
      <div className="highlight bg-light-primary mt-7">
        <div className="col-lg-12 mb-7 fv-row text-primary">
          <h5 className="text-primary fs-5">Panduan</h5>
          <p className="text-primary">
            Sebelum melakukan import soal, mohon untuk membaca panduan berikut :
          </p>
          <ul>
            <li>
              Silahkan unduh template untuk melakukan import pada link berikut{" "}
              <a
                href={template}
                className="btn btn-primary fw-semibold btn-sm py-1 px-2"
              >
                <i className="las la-cloud-download-alt fw-semibold me-1" />
                Download Template
              </a>
            </li>
            <li>
              Jika anda ingin membuat soal yang terdapat gambar, pastikan nama
              file gambar sesuai dengan yang di-input pada template
            </li>
            <li>ID Tipe Soal harus terisi</li>
          </ul>
        </div>
      </div>
      <Observer>
        {() => {
          return (
            <div className="row mt-7">
              <div className="col-lg-12 mb-7">
                <label className="form-label required">Upload Template</label>
                <input
                  type="file"
                  className="form-control form-control-sm mb-2"
                  name="upload_silabus"
                  accept=".xlsx"
                  onInput={(e) => {
                    const [file] = [...e.target.files];
                    astate.fileTemplateXlsx = file;
                    e.target.value = null;
                  }}
                />
                <small className="text-muted">
                  Format File (.xlsx), Max 10240 Kb
                </small>
                <br />
              </div>
              {astate.fileTemplateXlsx && (
                <div className="col-lg-12 mb-7 fv-row">
                  <div className="row">
                    <div className="col-lg-12 mb-7 fv-row">
                      <span>{astate.fileTemplateXlsx.name}</span>
                      <div
                        className="btn btn-sm btn-icon btn-light-danger float-end"
                        title="Hapus file"
                        onClick={(e) => (astate.fileTemplateXlsx = null)}
                      >
                        <span className="las la-trash-alt" />
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          );
        }}
      </Observer>
      <Observer>
        {() => {
          return (
            <div className="row">
              <div className="col-lg-12 mb-7 fv-row">
                <label className="form-label">Upload Images</label>
                <input
                  type="file"
                  className="form-control form-control-sm mb-2"
                  name="upload_silabus"
                  multiple
                  accept="image/png, image/gif, image/jpeg"
                  onInput={(e) => {
                    const files = [...e.target.files];
                    astate.fileTemplateImages = files;
                    e.target.value = null;
                  }}
                />
                <small className="text-muted">
                  Format File (.png, .jpg, .gif), Max 10240 Kb
                </small>
                <br />
              </div>
              {astate.fileTemplateImages && (
                <div className="col-lg-12 mb-7 fv-row">
                  <div className="row">
                    {astate.fileTemplateImages.map((file, idx) => {
                      return (
                        <div className="col-lg-12 mb-7 fv-row">
                          <span>{file.name}</span>
                          <div
                            className="btn btn-sm btn-icon btn-light-danger float-end"
                            title="Hapus file"
                            onClick={(e) => {
                              let files = astate.fileTemplateImages;
                              files.splice(idx, 1);
                              astate.fileTemplateImages = [...files];
                            }}
                          >
                            <span className="las la-trash-alt" />
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              )}
            </div>
          );
        }}
      </Observer>
    </>
  );
};

export const ImportModal = ({
  id = "import_soal",
  title = "Import Soal",
  onClose = async () => {},
  onSave = async () => {},
  question,
  tipeSoal = { data: [], mapper: (d) => d, findById: (id) => ({}) },
  errorMessage = () => ({}),
}) => {
  const ref = React.useRef();
  const closeBtnRef = React.useRef();

  const resizeFile = async (file) => {
    const options = {
      maxSizeMB: 0.2,
      maxWidthOrHeight: 300,
      useWebWorker: true,
    };

    return imageCompression(file, options);
  };

  const doImport = async () => {
    try {
      const file = astate.fileTemplateXlsx;
      const images = (
        await Promise.all(
          (astate.fileTemplateImages || []).map(async (f) => {
            const cf = await resizeFile(f);
            return new Promise((resolve, reject) => {
              var reader = new FileReader();
              reader.onload = () => {
                resolve({ name: f.name, data: reader.result });
              };
              reader.readAsDataURL(cf);
            });
          }),
        )
      ).reduce((obj, { name, data }) => ({ ...obj, [name]: data }), {});

      const templateBin = await new Promise((resolve, reject) => {
        var reader = new FileReader();
        reader.onload = () => {
          resolve(reader.result);
        };
        reader.readAsArrayBuffer(file);
      });

      var result = new Uint8Array(templateBin);
      var wb = read(result, { type: "array" });

      let triviaQuestions = [];
      wb.SheetNames.splice(0, 1).forEach((s) => {
        const [header = [], ...data] = utils.sheet_to_json(wb.Sheets[s], {
          header: 1,
        });

        // Validate image must be exists in uploaded images
        let notExistingImages = [];
        data.forEach((row) => {
          header.forEach((cname, cidx) => {
            if (cname.toLowerCase().includes("gambar")) {
              const img = row[cidx];
              if (img) {
                if (!Object.keys(images).includes(img)) {
                  notExistingImages = [...notExistingImages, img];
                }
              }
            }
          });
        });

        if (notExistingImages.length > 0)
          throw new Error(
            `Gambar berikut tidak terdapat di gambar yang di-upload: ${notExistingImages.join(
              ", ",
            )}`,
          );

        data.forEach((row) => {
          const [
            q,
            qimg,
            skeys = "",
            id_tipe_soal = null,
            status = null,
            ...answs
          ] = header.map((cname, idx) => row[idx]);
          const [key] = skeys
            .split(",")
            .map((k) => k.trim())
            .map((k) => k.toLowerCase());
          const { value: idts } = tipeSoal.findById(id_tipe_soal) || {};

          const tq = { ...defaultQuestion };
          tq.question = q;
          tq.question_image = qimg;
          tq.question_type_id = idts;
          tq.answer_key = key;
          tq.status = status;

          let tas = [];
          const letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];
          for (let aidx = 0; aidx < answs.length; aidx += 3) {
            if (!answs[aidx]) continue;
            let ta = {};
            ta.option = answs[aidx];
            ta.key = aidx / 3 < letters.length ? letters[aidx / 3] : null;
            ta.image =
              answs[aidx + 1] in images ? images[answs[aidx + 1]] : null;
            tas = [...tas, ta];
          }

          tq.answer = tas;

          const error = errorMessage(tq);
          if (error) throw new Error(error);

          triviaQuestions = [...triviaQuestions, tq];
        });
      });

      await onSave(triviaQuestions);

      // await Swal.fire({
      //   title: "Soal Berhasil Diimport",
      //   icon: "success",
      //   confirmButtonText: "Ok"
      // });

      return true;
    } catch (err) {
      await Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });

      return false;
    }
  };

  React.useEffect(() => {
    ref?.current?.addEventListener("hide.bs.modal", (e) => {
      if (astate.needSave) {
        e.preventDefault();
        doImport().then((noError) => {
          if (noError) {
            astate.fileTemplateXlsx = null;
            astate.fileTemplateImages = [];
            astate.dataQuestions = [];
          }

          astate.needSave = false;
          closeBtnRef?.current?.click();
          onClose(e);
        });

        // astate.error = errorMessage(astate.question);
        // const { answer = [], ...rest } = astate.error || {};

        // if (Object.keys(rest).length > 0 || Object.keys(answer).length > 0) e.preventDefault();
        // else {
        //   await onSave(astate.question);
        // }
      }
      // await onClose(e);
      // astate.question = defaultQuestion;
    });
  }, [ref, astate.needSave]);

  return (
    <div className="modal fade" tabindex="-1" id={id} ref={ref}>
      <div className="modal-dialog modal-lg">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">
              <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                >
                  <path
                    d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                    fill="currentColor"
                  />
                </svg>
              </span>
              {title}
            </h5>
            <div
              className="btn btn-icon btn-sm btn-active-light-primary ms-2"
              data-bs-dismiss="modal"
              aria-label="Close"
            >
              <span className="svg-icon svg-icon-2x">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                >
                  <rect
                    opacity="0.5"
                    x="6"
                    y="17.3137"
                    width="16"
                    height="2"
                    rx="1"
                    transform="rotate(-45 6 17.3137)"
                    fill="currentColor"
                  />
                  <rect
                    x="7.41422"
                    y="6"
                    width="16"
                    height="2"
                    rx="1"
                    transform="rotate(45 7.41422 6)"
                    fill="currentColor"
                  />
                </svg>
              </span>
            </div>
          </div>
          <div className="modal-body">
            <ImportComponent question={question} tipeSoal={tipeSoal} />
          </div>
          <div className="modal-footer">
            <div className="d-flex justify-content-between">
              <button
                ref={closeBtnRef}
                type="submit"
                className="btn btn-sm btn-secondary mr-3 me-5"
                data-bs-dismiss="modal"
                onMouseDown={() => (astate.needSave = false)}
              >
                Tutup
              </button>
              <button
                type="submit"
                className="btn btn-sm btn-primary"
                data-bs-dismiss="modal"
                onMouseDown={() => (astate.needSave = true)}
              >
                Import
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
