import imageCompression from "browser-image-compression";
import React from "react";
import { useParams, useNavigate } from "react-router-dom";

export const letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];

export const resetErrorTestSubstansi = () => {
  return {
    namaError: null,
    selectedLevelError: null,
    selectedAkademiError: null,
    selectedTemaError: null,
    selectedPelatihanError: null,
    selectedKategoriError: null,
    mulaiPelaksanaanError: null,
    selesaiPelaksanaanError: null,
    jumlahSoalError: null,
    durasiDetikError: null,
    passingGradeError: null,
    selectedStatusError: null,
  };
};

export const validateJumlahSoal = (jumlahSoal = 0, questions) => {
  if (!jumlahSoal) {
    return "Jumlah soal tidak boleh kosong";
  } else if (jumlahSoal > (questions || []).length) {
    return "Jumlah soal tidak boleh melebihi bank soal";
  }
  return null;
};

export const validatePassingGrade = (passingGrade = 0) => {
  if (!passingGrade) {
    return "Passing grade tidak boleh kosong";
  } else if (passingGrade < 0 || passingGrade > 100) {
    return "Passing grade harus terisi antara 0 - 100";
  }
  return null;
};

export const resetErrorHeaderScreen = () => {
  return {
    namaError: null,
    selectedLevelError: null,
    selectedAkademiError: null,
    selectedTemaError: null,
    selectedPelatihanError: null,
    selectedKategoriError: null,
    // mulaiPelaksanaanError: null,
    // selesaiPelaksanaanError: null,
    // jumlahSoalError: null,
    // durasiDetikError: null,
    // passingGradeError: null,
    // selectedStatusError: null,
  };
};

export const resetErrorPublishScreen = () => {
  return {
    // namaError: null,
    // selectedLevelError: null,
    // selectedAkademiError: null,
    // selectedTemaError: null,
    // selectedPelatihanError: null,
    // selectedKategoriError: null,
    mulaiPelaksanaanError: null,
    selesaiPelaksanaanError: null,
    jumlahSoalError: null,
    durasiDetikError: null,
    passingGradeError: null,
    selectedStatusError: null,
  };
};

export const validateHeaderScreen = (test) => {
  let {
    nama,
    selectedLevel,
    selectedAkademi,
    selectedTema,
    selectedPelatihan,
    selectedKategori,
    mulaiPelaksanaan,
    selesaiPelaksanaan,
    jumlahSoal,
    durasiDetik,
    passingGrade,
    selectedStatus,
    questions,
    pendaftaran_mulai = "",
    pendaftaran_selesai,
    pelatihan_mulai,
    pelatihan_selesai,
    pendaftaran_start,
    pendaftaran_end,
    pelatihan_start,
    pelatihan_end,
  } = test || {};
  let error = {};

  [pendaftaran_mulai] = pendaftaran_mulai.split(" ");

  const updateState = (k, v) => {
    if (v != null) error[k] = v;
  };

  if (!nama) {
    updateState("namaError", "Nama tidak boleh kosong");
  }

  if (!selectedLevel) {
    updateState("selectedLevelError", "Level harus terpilih salah satu");
  }

  if (!selectedAkademi) {
    updateState("selectedAkademiError", "Akademi harus terpilih salah satu");
  }

  const { value: selectedLevelValue } = selectedLevel || {};
  if (selectedLevelValue > 0 && !selectedTema) {
    updateState("selectedTemaError", "Tema harus terpilih salah satu");
  }

  if (selectedLevelValue > 1 && !selectedPelatihan) {
    updateState(
      "selectedPelatihanError",
      "Pelatihan harus terpilih salah satu",
    );
  }

  if (!selectedKategori) {
    updateState(
      "selectedKategoriError",
      "Kategori soal harus terpilih salah satu",
    );
  }

  return error;
};

export const validatePublishScreen = (test) => {
  let {
    nama,
    selectedLevel,
    selectedAkademi,
    selectedTema,
    selectedPelatihan,
    selectedKategori,
    mulaiPelaksanaan,
    selesaiPelaksanaan,
    jumlahSoal,
    durasiDetik,
    passingGrade,
    selectedStatus,
    questions,
    pendaftaran_mulai = "",
    pendaftaran_selesai,
    pelatihan_mulai,
    pelatihan_selesai,
    pendaftaran_start,
    pendaftaran_end,
    pelatihan_start,
    pelatihan_end,
  } = test || {};
  let error = {};

  [pendaftaran_mulai] = pendaftaran_mulai.split(" ");

  const updateState = (k, v) => {
    if (v != null) error[k] = v;
  };

  updateState("jumlahSoalError", validateJumlahSoal(jumlahSoal, questions));

  if (!durasiDetik) {
    updateState("durasiDetikError", "Durasi pengerjaan tidak boleh kosong");
  }

  updateState("passingGradeError", validatePassingGrade(passingGrade));

  if (selectedStatus == null) {
    updateState("selectedStatusError", "Status harus terpilih salah satu");
  }

  return error;
};

export const validateTestSubstansi = (test) => {
  let {
    nama,
    selectedLevel,
    selectedAkademi,
    selectedTema,
    selectedPelatihan,
    selectedKategori,
    mulaiPelaksanaan,
    selesaiPelaksanaan,
    jumlahSoal,
    durasiDetik,
    passingGrade,
    selectedStatus,
    questions,
    pendaftaran_mulai = "",
    pendaftaran_selesai,
    pelatihan_mulai,
    pelatihan_selesai,
    pendaftaran_start,
    pendaftaran_end,
    pelatihan_start,
    pelatihan_end,
  } = test || {};
  let error = {};

  [pendaftaran_mulai] = pendaftaran_mulai.split(" ");

  const updateState = (k, v) => {
    if (v != null) error[k] = v;
  };

  if (!nama) {
    updateState("namaError", "Nama tidak boleh kosong");
  }

  if (!selectedLevel) {
    updateState("selectedLevelError", "Level harus terpilih salah satu");
  }

  if (!selectedAkademi) {
    updateState("selectedAkademiError", "Akademi harus terpilih salah satu");
  }

  const { value: selectedLevelValue } = selectedLevel || {};
  if (selectedLevelValue > 0 && !selectedTema) {
    updateState("selectedTemaError", "Tema harus terpilih salah satu");
  }

  if (selectedLevelValue > 1 && !selectedPelatihan) {
    updateState(
      "selectedPelatihanError",
      "Pelatihan harus terpilih salah satu",
    );
  }

  if (!selectedKategori) {
    updateState(
      "selectedKategoriError",
      "Kategori soal harus terpilih salah satu",
    );
  }

  // if (!mulaiPelaksanaan) {
  //   updateState('mulaiPelaksanaanError', 'Mulai pelaksanaan tidak boleh kosong');
  // } else if (pendaftaran_mulai && (new Date(mulaiPelaksanaan)).getDate() < (new Date(pendaftaran_mulai)).getDate()) {
  //   updateState('mulaiPelaksanaanError', 'Mulai pelaksanaan tidak boleh sebelum mulai pendaftaran');
  // }

  // if (!selesaiPelaksanaan) {
  //   updateState('selesaiPelaksanaanError', 'Selesai pelaksanaan tidak boleh kosong');
  // }

  // if ((selesaiPelaksanaan || 0) < (mulaiPelaksanaan || 0)) {
  //   updateState('selesaiPelaksanaanError', 'Pelaksanaan sampai harus setelah pelaksanaan dari');
  // }

  updateState("jumlahSoalError", validateJumlahSoal(jumlahSoal, questions));

  // if (!jumlahSoal) {
  //   updateState('jumlahSoalError', 'Jumlah soal tidak boleh kosong');
  // } else if (jumlahSoal > (questions || []).length) {
  //   updateState('jumlahSoalError', 'Jumlah soal tidak boleh melebihi bank soal');
  // }

  if (!durasiDetik) {
    updateState("durasiDetikError", "Durasi pengerjaan tidak boleh kosong");
  }

  updateState("passingGradeError", validatePassingGrade(passingGrade));

  // if (!passingGrade) {
  //   updateState('passingGradeError', 'Passing grade tidak boleh kosong');
  // } else if (passingGrade < 0 || passingGrade > 100) {
  //   updateState('passingGradeError', 'Passing grade harus terisi antara 0 - 100');
  // }

  if (selectedStatus == null) {
    updateState("selectedStatusError", "Status harus terpilih salah satu");
  }

  return error;
};

export const validateQuestion = (tq) => {
  let error = {};

  if (!tq.question) error["question_error"] = "Pertanyaan tidak boleh kosong";
  if (!tq.question_type_id)
    error["question_type_id_error"] = "Tipe soal harus terpilih salah satu";
  if (!tq.answer_key)
    error["answer_key_error"] = "Kunci jawaban tidak boleh kosong";
  if (
    !(tq.answer || [])
      .map((a) => (a.key || "").toLowerCase())
      .includes((tq.answer_key || "").toLowerCase())
  )
    error["answer_key_error"] = "Kunci jawaban tidak ada dalam pilihan jawaban";
  if (![0, 1].includes(Number(tq.status)))
    error["status_error"] = "Status tidak boleh kosong";
  if ((tq.answer || []).length < 2)
    error["num_answer_error"] = "Pilihan jawaban minimal 2 opsi";

  let idx = 0;
  for (const ta of tq.answer || [{}, {}, {}, {}]) {
    if (ta?.option == null || ta?.option == undefined) {
      if (!Object.keys(error).includes("answer_error"))
        error["answer_error"] = [];
      if (!(idx in error["answer_error"])) error["answer_error"][idx] = [];
      error["answer_error"][idx]["option_error"] = "Jawaban tidak boleh kosong";
    }
    idx++;
  }

  return error;
};

export const resizeFile = async (file) => {
  const options = {
    maxSizeMB: 0.2,
    maxWidthOrHeight: 300,
    useWebWorker: true,
  };

  return imageCompression(file, options);
};

export const dateToIsoString = (date) => {
  var d = date,
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [year, month, day].join("-");
};

export const withRouter = (Component) => {
  return (props) => (
    <Component {...props} params={useParams()} history={useNavigate()} />
  );
};
