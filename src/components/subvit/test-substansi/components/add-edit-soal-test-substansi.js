import { observable } from "mobx";
import { Observer } from "mobx-react-lite";
import React from "react";
import Select from "react-select";
import imageCompression from "browser-image-compression";
import { letters } from "./utils";

export const defaultQuestion = {
  question: "",
  question_image: null,
  question_type_id: null,
  status: 1,
  answer: [{ key: "A" }, { key: "B" }, { key: "C" }, { key: "D" }],
  answer_key: "",
};
export const defaultErrorMessage = {
  question: null,
  question_type_id: null,
  status: null,
  answer: [],
  answer_key: null,
  answer_num: null,
};

export const astate = observable({
  question: defaultQuestion,
  error: defaultErrorMessage,
  needSave: false,
});

export const AddEditComponent = ({
  question: q = defaultQuestion,
  tipeSoal = { data: [], mapper: (d) => d, findById: (id) => ({}) },
}) => {
  const setState = (kv) => {
    // _setState({ ...state, ...kv });
    astate.question = { ...astate.question, ...kv };
  };

  // const error = { ...defaultErrorMessage, ...errorMessage };

  React.useEffect(() => {
    astate.question = defaultQuestion;
  }, []);

  React.useEffect(() => {
    if (q) astate.question = q;
    else astate.question = defaultQuestion;
  }, [q]);

  const changeAnswer = (idx, key, val) => {
    let a = astate.question.answer;
    a[idx][key] = val;
    setState({ answer: [...a] });
  };

  const resizeFile = async (file) => {
    const options = {
      maxSizeMB: 0.2,
      maxWidthOrHeight: 300,
      useWebWorker: true,
    };

    return imageCompression(file, options);
  };

  const loadGambar = (f) => {
    return new Promise((resolve, reject) => {
      resizeFile(f).then((cf) => {
        var reader = new FileReader();
        reader.onload = () => {
          resolve(reader.result);
        };
        reader.readAsDataURL(cf);
      });
    });
  };

  return (
    <>
      <div className="row">
        <div className="col-lg-12 mb-7 fv-row">
          <label className="form-label required">Pertanyaan</label>
          <Observer>
            {() => (
              <>
                <input
                  className="form-control form-control-sm"
                  placeholder="Masukkan pertanyaan"
                  name="pertanyaan"
                  value={astate.question.question}
                  onChange={(e) => setState({ question: e.target.value })}
                />
                <span style={{ color: "red" }}>{astate.error.question}</span>
              </>
            )}
          </Observer>
        </div>
        <div className="col-lg-12">
          <div className="mb-7 fv-row">
            <label className="form-label">Gambar Pertanyaan (Optional)</label>
            <Observer>
              {() => (
                <>
                  <input
                    type="file"
                    className="form-control form-control-sm mb-2"
                    name="upload_silabus"
                    accept="image/png, image/gif, image/jpeg"
                    onInput={async (e) => {
                      const [img] = [...e.target.files];
                      const b64img = await loadGambar(img);
                      setState({ question_image: b64img });
                    }}
                  />
                  <small className="text-muted">
                    Format File (.jpg/.jpeg/.png), Max 10240
                  </small>
                  <br />
                  {/* <span style={{ color: "red" }}>{this.state.errors["upload_silabus"]}</span> */}
                </>
              )}
            </Observer>
          </div>
        </div>
        <Observer>
          {() => {
            return (
              astate.question.question_image && (
                <div className="col-lg-12 mb-7 position-relative">
                  <img
                    src={astate.question.question_image}
                    style={{ maxWidth: "100%" }}
                  />
                  <btn
                    className="btn btn-icon btn-active-light-danger w-30px h-30px position-absolute"
                    style={{ top: 10, right: 10 }}
                    onClick={() => setState({ question_image: null })}
                  >
                    <span className="svg-icon svg-icon-3">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width={24}
                        height={24}
                        viewBox="0 0 24 24"
                        fill="none"
                      >
                        <path
                          d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z"
                          fill="black"
                        />
                        <path
                          opacity="0.5"
                          d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z"
                          fill="black"
                        />
                        <path
                          opacity="0.5"
                          d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z"
                          fill="black"
                        />
                      </svg>
                    </span>
                  </btn>
                </div>
              )
            );
          }}
        </Observer>
        <div className="col-lg-12">
          <div className="mb-7 fv-row">
            <label className="form-label required">Tipe Soal</label>
            <Observer>
              {() => {
                return (
                  <>
                    <Select
                      placeholder="Silahkan pilih"
                      className="form-select-sm selectpicker p-0"
                      options={tipeSoal.data.map((d) => tipeSoal.mapper(d))}
                      value={tipeSoal.mapper(
                        tipeSoal.findById(astate.question.question_type_id),
                      )}
                      onChange={({ value }) =>
                        setState({ question_type_id: value })
                      }
                    />
                    <span style={{ color: "red" }}>
                      {astate.error.question_type_id}
                    </span>
                  </>
                );
              }}
            </Observer>
          </div>
        </div>
      </div>
      <Observer>
        {() => (
          <>
            {astate.question.answer.map((j, idx) => {
              const { key = "", option = "", image = null, color } = j || {};
              return (
                <Observer>
                  {() => (
                    <div className="row">
                      <div className={`col-lg-5 mb-7`}>
                        <label className="form-label required">
                          Jawaban {letters[idx]}
                        </label>
                        <input
                          className="form-control form-control-sm"
                          placeholder={`Masukkan jawaban ${key}`}
                          name="pertanyaan"
                          value={option}
                          onChange={(e) =>
                            changeAnswer(idx, "option", e.target.value)
                          }
                        />
                        <span style={{ color: "red" }}>
                          {astate.error?.answer?.length > 0
                            ? astate.error?.answer[idx]?.option
                            : null}
                        </span>
                      </div>
                      <div className={`col-lg-5 mb-7`}>
                        <div className="mb-7 fv-row">
                          <label className="form-label">
                            Gambar Jawaban (Optional)
                          </label>
                          <input
                            type="file"
                            className="form-control form-control-sm mb-2"
                            name="upload_silabus"
                            accept="image/png, image/gif, image/jpeg"
                            onChange={async (e) => {
                              const [img] = [...e.target.files];
                              const b64img = await loadGambar(img);
                              changeAnswer(idx, "image", b64img);
                            }}
                          />
                          <small className="text-muted">
                            Format File (.jpg/.jpeg/.png), Max 10240
                          </small>
                          <br />
                        </div>
                        <Observer>
                          {() => {
                            return (
                              image && (
                                <div className="mb-7 fv-row">
                                  <img
                                    src={image}
                                    style={{ maxWidth: "100%" }}
                                  />
                                  <btn
                                    className="btn btn-icon btn-active-light-danger w-30px h-30px position-absolute"
                                    style={{ top: 10, right: 10 }}
                                    onClick={() =>
                                      changeAnswer(idx, "image", null)
                                    }
                                  >
                                    <span className="svg-icon svg-icon-3">
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width={24}
                                        height={24}
                                        viewBox="0 0 24 24"
                                        fill="none"
                                      >
                                        <path
                                          d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z"
                                          fill="black"
                                        />
                                        <path
                                          opacity="0.5"
                                          d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z"
                                          fill="black"
                                        />
                                        <path
                                          opacity="0.5"
                                          d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z"
                                          fill="black"
                                        />
                                      </svg>
                                    </span>
                                  </btn>
                                </div>
                              )
                            );
                          }}
                        </Observer>
                      </div>
                      <div className={`col-lg-1 mb-7`}>
                        <div className="mt-7 fv-row">
                          <a
                            title="Hapus"
                            index="1_0"
                            className={`btn btn-sm btn-icon btn-danger ${
                              astate.question.answer.length <= 2
                                ? "disabled"
                                : ""
                            }`}
                            onClick={(e) => {
                              e.preventDefault();

                              let newAnswers = [];
                              (astate.question.answer || []).forEach(
                                (a, aidx) => {
                                  if (idx != aidx)
                                    newAnswers = [...newAnswers, a];
                                },
                              );
                              astate.question.answer = newAnswers;

                              // lastQuestion.hapusJawaban(idx);
                            }}
                          >
                            <i className="las la-trash"></i>
                          </a>
                        </div>
                      </div>
                      <div
                        className={`col-lg-1 mb-7 text-center ${
                          idx == 0 ? "" : "mt-7"
                        }`}
                      >
                        {idx == 0 && (
                          <label className="form-label d-block required mb-3">
                            Kunci
                          </label>
                        )}
                        <input
                          className="form-check-input align-content-center"
                          type="radio"
                          name="answer_key"
                          value={letters[idx]}
                          id="jenis_pertanyaan1"
                          checked={astate.question.answer_key == letters[idx]}
                          onChange={(e) =>
                            setState({ answer_key: letters[idx] })
                          }
                        />
                        <span style={{ color: "red" }}>
                          {astate.error.answer_key}
                        </span>
                      </div>
                    </div>
                  )}
                </Observer>
              );
            })}
          </>
        )}
      </Observer>
      <Observer>
        {() => {
          return (
            (astate?.error?.answer_num && (
              <div className="row">
                <div className="col-lg-12 mb-7 fv-row">
                  <Observer>
                    {() => (
                      <span style={{ color: "red" }}>
                        {astate.error.answer_num}
                      </span>
                    )}
                  </Observer>
                </div>
              </div>
            )) ||
            null
          );
        }}
      </Observer>
      <div className="row">
        <div className="col-lg-12 mb-7 fv-row">
          <label className="form-label required">Status</label>
          <Observer>
            {() => (
              <>
                <div className="d-flex">
                  <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="status"
                      value="0"
                      id="status_publish"
                      checked={astate.question.status == 0}
                      onChange={(e) => setState({ status: 0 })}
                    />
                    <label className="form-check-label" for="status_publish">
                      Draft
                    </label>
                  </div>
                  <div className="form-check form-check-sm form-check-custom form-check-solid">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="status"
                      value="1"
                      id="status_draft"
                      checked={astate.question.status == 1}
                      onChange={(e) => setState({ status: 1 })}
                    />
                    <label className="form-check-label" for="status_draft">
                      Publish
                    </label>
                  </div>
                </div>
                <span style={{ color: "red" }}>{astate.error.status}</span>
              </>
            )}
          </Observer>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-12 mb-7 fv-row text-center">
          <a
            className="btn btn-sm btn-light text-success d-block btn-block"
            onClick={() => {
              if (astate.question) {
                const labels = ["A", "B", "C", "D", "E", "F", "G", "H", "I"];
                astate.question.answer = [
                  ...(astate.question.answer || []),
                  {
                    key:
                      astate.question.answer.length < labels.length
                        ? labels[astate.question.answer.length]
                        : "A",
                  },
                ];
              }
            }}
          >
            <i className="bi bi-plus-circle text-success me-1"></i> Tambah Opsi
            Jawaban
          </a>
        </div>
      </div>
    </>
  );
};

export const AddEditModal = ({
  id = "modal_edit_soal",
  title = "Ubah Soal",
  onClose = async () => {},
  onSave = async () => {},
  question,
  tipeSoal = { data: [], mapper: (d) => d, findById: (id) => ({}) },
  errorMessage = () => ({}),
}) => {
  const ref = React.useRef();

  React.useEffect(() => {
    ref?.current?.addEventListener("hide.bs.modal", async (e) => {
      // console.log(astate.needSave)
      if (astate.needSave) {
        astate.error = errorMessage(astate.question);
        const { answer = [], ...rest } = astate.error || {};

        if (Object.keys(rest).length > 0 || Object.keys(answer).length > 0)
          e.preventDefault();
        else {
          await onSave(astate.question);
        }
      }
      await onClose(e);
      astate.question = defaultQuestion;
    });
  }, [astate.needSave]);

  return (
    <div className="modal fade" tabindex="-1" id={id} ref={ref}>
      <div className="modal-dialog modal-lg">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">
              <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                >
                  <path
                    d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                    fill="currentColor"
                  />
                </svg>
              </span>
              {title}
            </h5>
            <div
              className="btn btn-icon btn-sm btn-active-light-primary ms-2"
              data-bs-dismiss="modal"
              aria-label="Close"
            >
              <span className="svg-icon svg-icon-2x">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                >
                  <rect
                    opacity="0.5"
                    x="6"
                    y="17.3137"
                    width="16"
                    height="2"
                    rx="1"
                    transform="rotate(-45 6 17.3137)"
                    fill="currentColor"
                  />
                  <rect
                    x="7.41422"
                    y="6"
                    width="16"
                    height="2"
                    rx="1"
                    transform="rotate(45 7.41422 6)"
                    fill="currentColor"
                  />
                </svg>
              </span>
            </div>
          </div>
          <div className="modal-body">
            <AddEditComponent question={question} tipeSoal={tipeSoal} />
          </div>
          <div className="modal-footer">
            <div className="d-flex justify-content-between">
              <button
                type="submit"
                className="btn btn-sm btn-secondary  mr-3 me-5"
                data-bs-dismiss="modal"
                onMouseDown={() => (astate.needSave = false)}
              >
                Tutup
              </button>
              <button
                type="submit"
                className="btn btn-sm btn-primary"
                data-bs-dismiss="modal"
                onMouseDown={() => (astate.needSave = true)}
              >
                Simpan
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
