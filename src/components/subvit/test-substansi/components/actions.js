import axios from "axios";
import Cookies from "js-cookie";
import moment from "moment";
import { isExceptionRole } from "../../../AksesHelper";
import { letters } from "./utils";

const configs = {
  headers: {
    Authorization: "Bearer " + Cookies.get("token"),
  },
};

export const fetchKategori = async () => {
  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/umum/list-kategori",
        {},
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const { Data = [], Status = false, Message = "" } = result || {};

        if (Status)
          resolve(Data.map(({ id, name }) => ({ value: id, label: name })));
        else reject(Message);
      })
      .catch((err) => {
        const { data } = err.response;
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const fetchAkademi = async () => {
  var formdata = new FormData();
  formdata.append("length", 1000);
  formdata.append("param", "");
  formdata.append("sort", "created_at");
  formdata.append("sort_val", "desc");
  formdata.append("start", 0);
  formdata.append("status", 1);
  formdata.append("tahun", "0");

  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list-akademi-s3",
        formdata,
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const { Data = [], Status = false, Message = "" } = result || {};

        if (Status)
          resolve(
            Data.map(({ id, name, ...rest }) => ({
              value: id,
              label: name,
              ...rest,
            })),
          );
        else reject(Message);
      })
      .catch((err) => {
        const { data } = err.response;
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const fetchTema = async (akademi_id) => {
  return new Promise((resolve, reject) => {
    const dataBody = {
      start: 0,
      rows: 100,
      id_akademi: akademi_id,
      status: "null",
      cari: 0,
      sort: "name",
      sort_val: "DESC",
    };
    axios
      .post(
        isExceptionRole()
          ? process.env.REACT_APP_BASE_API_URI + "/list-tema-filter-substansi"
          : process.env.REACT_APP_BASE_API_URI + "/list-tema-filter-substansi",
        dataBody,
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const { Data = [], Status = false, Message = "" } = result || {};

        if (Status)
          resolve(
            Data.filter((d) => d?.status == 1).map(({ id, name, ...rest }) => ({
              value: id,
              label: name,
              ...rest,
            })),
          );
        else reject(Message);
      })
      .catch((err) => {
        const { data } = err.response;
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const fetchPelatihan = async (akademi_id, theme_id) => {
  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/survey/combo_akademi_pelatihan",
        {
          jns_param: 3,
          akademi_id,
          theme_id,
          pelatihan_id: 0,
        },
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const { Data = [], Status = false, Message = "" } = result || {};

        if (Status)
          resolve(
            Data.map((d) => ({ ...d, ["value"]: d?.pid, ["label"]: d?.nama })),
            // Data.map(
            //   ({ pid, nama, wilayah = "Online", batch = 1, ...rest }) => ({
            //     value: pid,
            //     label: `${nama} Batch ${batch} - ${wilayah}`,
            //     ...rest,
            //   })
            // )
          );
        else reject(Message);
      })
      .catch((err) => {
        const { data } = err.response;
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const fetchStatusPelatihan = async (
  akademi_id,
  theme_id,
  pelatihan_id,
) => {
  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/tessubstansi/combo-akademi-tema-pelatihan-detail",
        {
          akademi_id,
          theme_id,
          pelatihan_id,
        },
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const { Data = [], Status = false, Message = "" } = result || {};
        const [D] = Data || [];

        if (Status) resolve(D);
        else reject(Message);
      })
      .catch((err) => {
        const { data } = err.response;
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const fetchStatusSubstansi = async (
  akademi_id,
  tema_id,
  pelatihan_id,
) => {
  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/tessubstansi/search_exist_pelatihan_v2",
        {
          akademi_id,
          tema_id,
          pelatihan_id,
        },
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const { Data = [], Status = false, Message = "" } = result || {};
        const [D] = Data || [];

        if (Status) resolve(D);
        else reject(Message);
      })
      .catch((err) => {
        const { data } = err.response;
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const fetchListPelatihanByAkademiTema2 = async (
  level,
  akademi_id,
  theme_id,
  pelatihan_id,
) => {
  return new Promise((resolve, reject) => {
    if (level < 0) return resolve([]);

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/tessubstansi/pelatihan-paging-combo",
        {
          jns_param: level + 1,
          akademi_id,
          theme_id,
          pelatihan_id,
          start: "0",
          length: "100",
        },
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const { Data = [], Status = false, Message = "" } = result || {};

        if (Status) resolve(Data);
        else reject(Message);
      })
      .catch((err) => {
        const { data } = err.response;
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const fetchListPelatihanByAkademiTema = async (
  level,
  akademi_id,
  theme_id,
  pelatihan_id,
) => {
  return new Promise((resolve, reject) => {
    if (theme_id === 0) {
      return resolve({ Data: [], Status: false, Message: "" });
    }
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/tessubstansi/list-pelatihan-kategori",
        {
          jns_param: level, // 1=tes,2=mid
          akademi_id: akademi_id,
          theme_id: theme_id,
          pelatihan_id: pelatihan_id,
          start: "0",
          length: "100",
        },
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        let { Data = [], Status = false, Message = "" } = result || {};

        Data = Data.map((d) => ({
          ...d,
          ["nama"]: `${d.nama} Batch ${d.batch || 1} - ${
            d.wilayah || "Online"
          }`,
        }));

        if (Status) resolve(Data);
        else reject(Message);
      })
      .catch((err) => {
        const { data } = err.response;
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const fetchLevel = async () => {
  return new Promise((resolve, reject) => {
    resolve([
      { value: 0, label: "Akademi" },
      { value: 1, label: "Tema" },
      { value: 2, label: "Pelatihan" },
    ]);
  });
};

export const fetchStatus = async () => {
  return new Promise((resolve, reject) => {
    resolve([
      { value: 0, label: "Draft" },
      { value: 1, label: "Publish" },
      { value: 2, label: "Batal" },
    ]);
  });
};

export const fetchStatusPelaksanaan = async () => {
  return new Promise((resolve, reject) => {
    resolve([
      { value: "Belum Dilaksanakan", label: "Belum Dilaksanakan" },
      { value: "Sedang Dilaksanakan", label: "Sedang Dilaksanakan" },
      { value: "Selesai", label: "Selesai" },
    ]);
  });
};

export const fetchListTestSubstansi = async (
  start,
  rows,
  param,
  status,
  sort,
  sort_val,
  level,
  status_pelaksanaan,
) => {
  const params = {
    start,
    rows,
    param,
    status,
    sort,
    sort_val,
    level,
    status_pelaksanaan,
  };

  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/tessubstansi/list-substansi-v2",
        params,
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const {
          Data = [],
          Total = 0,
          Status = false,
          Message = "",
        } = result || {};

        if (Status) resolve({ Total, Data });
        else reject(Message);
      })
      .catch((err) => {
        const { response } = err || {};
        const { data } = response || {};
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const fetchTestSubstansiById = async (id) => {
  var tparams = {
    id,
    start: 0,
    length: 10000,
    status: 99,
    sort: "nama_akademi",
    sort_val: "ASC",
  };

  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list-substansibyid-full",
        tparams,
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const {
          Data = [],
          Detail,
          Status = false,
          Message = "",
        } = result || {};
        const [testSubstansi] = Data || [];
        let { status_substansi, ...restRestSubstansi } = testSubstansi || {};

        if (status_substansi.toLowerCase() == "draft") status_substansi = 0;
        else if (status_substansi.toLowerCase() == "publish")
          status_substansi = 1;
        else if (status_substansi.toLowerCase() == "batal")
          status_substansi = 2;

        if (Status)
          resolve({
            header: { ...restRestSubstansi, status_substansi },
            soals: (Detail || []).map(({ answer, ...rest }) => ({
              answer: typeof answer === "string" ? JSON.parse(answer) : answer,
              ...rest,
            })),
          });
        else reject(Message);
      })
      .catch((err) => {
        const { response } = err || {};
        const { data } = response || {};
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const fetchTestSubstansiHeaderById = async (id) => {
  var tparams = {
    id,
    start: 0,
    length: 10000,
    status: 99,
    sort: "nama_akademi",
    sort_val: "ASC",
  };

  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list-substansibyid-header",
        tparams,
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const { Data = [], Status = false, Message = "" } = result || {};
        const [testSubstansi] = Data || [];
        let { status_substansi, ...restRestSubstansi } = testSubstansi || {};

        if (status_substansi.toLowerCase() == "draft") status_substansi = 0;
        else if (status_substansi.toLowerCase() == "publish")
          status_substansi = 1;
        else if (status_substansi.toLowerCase() == "batal")
          status_substansi = 2;

        if (Status)
          resolve({
            header: { ...restRestSubstansi, status_substansi },
          });
        else reject(Message);
      })
      .catch((err) => {
        const { response } = err || {};
        const { data } = response || {};
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const fetchTestSubstansiByIdV2 = async (id) => {
  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/tessubstansi/substansi-select-byid",
        { id },
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const {
          substansi = [],
          pelatihan = [],
          Detail,
          Status = false,
          Message = "",
        } = result || {};

        // const [testSubstansi] = Data || [];
        // let { status_substansi, ...restRestSubstansi } = testSubstansi || {};
        // status_substansi = status_substansi.toLowerCase() == 'publish' ? 1 : 0;

        if (Status)
          resolve({
            substansi,
            pelatihan,
          });
        else reject(Message);
      })
      .catch((err) => {
        const { response } = err || {};
        const { data } = response || {};
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const removeTestSubstansiById = async (id) => {
  const params = { id };

  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/tessubstansi/softdelete-testsubstansi",
        params,
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const { Status = false, Message = "" } = result || {};

        if (Status) resolve(Message);
        else reject(Message);
      })
      .catch((err) => {
        const { response } = err || {};
        const { data } = response || {};
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const tambahTestSubstansi = async (state) => {
  const {
    nama,
    selectedLevel,
    selectedAkademi,
    selectedTema,
    selectedPelatihan,
    selectedKategori,
    mulaiPelaksanaan,
    selesaiPelaksanaan,
    jumlahSoal,
    durasiDetik,
    passingGrade,
    selectedStatus,
    questions,
    selectedQuestionIds = [],
  } = state || {};

  const { value: levelId } = selectedLevel || {};
  const { value: akademiId } = selectedAkademi || {};
  const { value: temaId = 0 } = selectedTema || {};
  const { value: pelatihanId = 0 } = selectedPelatihan || {};
  const { label: kategoriLabel = "" } = selectedKategori || {};

  const convSoals = questions
    .filter(({ id }) => selectedQuestionIds.includes(id))
    .map((q, idx) => {
      let {
        answer = [],
        answer_key,
        question,
        question_image,
        question_type_id,
        status,
      } = q || {};

      answer = answer.map(({ key, option, image }, aidx) => {
        return {
          key: letters[aidx],
          option: option,
          image: image,
          color: false,
        };
      });

      return {
        nosoal: idx + 1,
        id_tipe_soal: question_type_id,
        pertanyaan: question,
        gambar_pertanyaan: question_image,
        kunci_jawaban: answer_key,
        jawaban: answer,
        status: status,
      };
    });

  var data = [
    {
      level: levelId,
      idakademi: akademiId,
      idtema: temaId,
      idpelatihan: pelatihanId,
      kategori: kategoriLabel,
      tittle: nama,
      // "start_at": mulaiPelaksanaan,
      // "end_at": selesaiPelaksanaan,
      question_to_share: jumlahSoal,
      duration: durasiDetik,
      passing_grade: passingGrade,
      soal_json: convSoals,
      status: selectedStatus,
      id_user: Cookies.get("user_id"),
    },
  ];

  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/tessubstansi/submit_create_substansi_v2",
        data,
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const {
          Data = [],
          Detail,
          Status = false,
          StatusCode = "",
          Message = "",
        } = result || {};
        const StatusCodes = `${StatusCode}`.split(";").map((c) => c.trim());

        if (StatusCodes.includes("200")) {
          resolve();
        } else {
          reject(Message);
        }
      })
      .catch((err) => {
        const { response } = err || {};
        const { data } = response || {};
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const updateTestSubstansi = (id, data) => {
  const {
    nama,
    selectedLevel,
    selectedAkademi,
    selectedTema,
    selectedPelatihan,
    selectedKategori,
    mulaiPelaksanaan,
    selesaiPelaksanaan,
    jumlahSoal,
    durasiDetik,
    passingGrade,
    selectedStatus,
    questions,
  } = data || {};

  const { value: levelId } = selectedLevel || {};
  const { value: akademiId } = selectedAkademi || {};
  const { value: temaId = 0 } = selectedTema || {};
  const { value: pelatihanId = 0 } = selectedPelatihan || {};
  const { label: kategoriLabel = "" } = selectedKategori || {};

  const params = [
    {
      // tittle: nama,
      // level: levelId,
      // akademi_id: levelId > -1 ? akademiId : 0,
      // tema_id: levelId > 0 ? temaId : 0,
      // pelatihan_id: levelId > 1 ? pelatihanId : 0,
      // category: kategoriLabel,
      // start_at: mulaiPelaksanaan,
      // end_at: selesaiPelaksanaan,
      // questions_to_share: jumlahSoal,
      // duration: durasiDetik,
      // passing_grade: passingGrade,
      // status: selectedStatus,
      // id_substansi: id,

      level: levelId,
      idakademi: levelId > -1 ? akademiId : 0,
      idtema: levelId > 0 ? temaId : 0,
      idpelatihan: levelId > 1 ? pelatihanId : 0,
      kategori: kategoriLabel,
      tittle: nama,
      // "start_at": mulaiPelaksanaan,
      // "end_at": selesaiPelaksanaan,
      question_to_share: jumlahSoal,
      duration: durasiDetik,
      passing_grade: passingGrade,
      soal_json: [],
      status: selectedStatus,
      id: id,
      id_user: Cookies.get("user_id"),
    },
  ];

  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/tessubstansi/update-testsubstansi-v3",
        params,
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const {
          Data = [],
          Detail,
          Status = false,
          StatusCode = 200,
          Message = "",
        } = result || {};

        if (StatusCode == 200) resolve();
        else reject(Message);
      })
      .catch((err) => {
        const { response } = err || {};
        const { data } = response || {};
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const updateSoalTestSubstansi = async (rows) => {
  const params = rows.map(
    ({
      id,
      question,
      question_image,
      question_type_id,
      answer_key,
      answer,
      status,
    }) => {
      return {
        soal_id: id,
        id_user: Cookies.get("user_id"),
        // substansi_id: triviaId,
        pertanyaan: question,
        gambar_pertanyaan: question_image,
        id_tipe_soal: question_type_id,
        kunci_jawaban: answer_key,
        jawaban: answer,
        status: status,
      };
    },
  );

  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/tessubstansi/ubah-soal-substansibulk",
        params,
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const { Status = false, Message = "" } = result || {};

        if (Status) resolve(Message);
        else reject(Message);
      })
      .catch((err) => {
        const { response } = err || {};
        const { data } = response || {};
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const removeSoalsTestSubstansi = async (ids) => {
  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/tessubstansi/soalsubstansi-delete-bulk",
        { id: `array[${ids.join(",")}]` },
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const { Status = false, Message = "" } = result || {};

        if (Status) resolve(Message);
        else reject(Message);
      })
      .catch((err) => {
        const { response } = err || {};
        const { data } = response || {};
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

/**
 *
 * @param {*} status 0 = all, 1 = publish, 2 = unpublish
 * @returns
 */
export const fetchTipeSoal = async (
  status = 0,
  sort = null,
  sort_val = null,
) => {
  let param = { start: 0, length: 10000, status };
  if (sort && sort_val) {
    param = { ...param, sort, sort_val };
  }

  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/tessubstansi/list-tipesoal",
        param,
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const { Data = [], Status = false, Message = "" } = result || {};

        if (Status)
          resolve(
            Data.map(({ id, name, value, jml, status }) => ({
              value: id,
              label: `${name} - ${value} Point`,
              nilai: value,
              jml,
              status,
            })),
          );
        else reject(Message);
      })
      .catch((err) => {
        const { data } = err.response;
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

// export const listTipeSoalTestSubstansi = async (sort, sort_val) => {
//   return new Promise((resolve, reject) => {
//     axios.post(process.env.REACT_APP_BASE_API_URI + '/tessubstansi/list-tipesoal', { start: 0, length: 200, sort, sort_val }, configs)
//       .then(res => {
//         const { data } = res || {};
//         const { result } = data;
//         const { Data = [], Detail, Status = false, Message = '' } = result || {};

//         if (Status) resolve(Data);
//         else reject(Message);
//       })
//       .catch(err => {
//         const { response } = err || {};
//         const { data } = response || {};
//         const { result } = data || {};
//         const { Data, Status = false, Message = '' } = result || {};
//         reject(Message);
//       })
//   });
// }

export const getTipeSoalTestSubstansi = async (id) => {
  const params = {
    id,
  };

  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/tessubstansi/cari-tipesoal",
        params,
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const {
          Data = [],
          Detail,
          Status = false,
          Message = "",
        } = result || {};
        const [D] = Data || [];

        if (Status) resolve(D);
        else reject(Message);
      })
      .catch((err) => {
        const { response } = err || {};
        const { data } = response || {};
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const cariTipeSoalTestSubstansi = async (cari, sort, sort_val) => {
  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/tessubstansi/carifull-tipesoal",
        { cari, sort, sort_val },
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const {
          Data = [],
          Detail,
          Status = false,
          Message = "",
        } = result || {};

        if (Status)
          resolve(
            Data.map(({ id, name, value, status }) => ({
              value: id,
              label: name,
              nilai: value,
              status,
            })),
          );
        else reject(Message);
      })
      .catch((err) => {
        const { response } = err || {};
        const { data } = response || {};
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const tambahTipeSoalTestSubstansi = async (name, value, status) => {
  const params = {
    name,
    value,
    status,
  };

  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/tessubstansi/tambah-tipesoal",
        params,
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const {
          Data = [],
          Detail,
          Status = false,
          Message = "",
        } = result || {};

        if (Status) resolve();
        else reject(Message);
      })
      .catch((err) => {
        const { response } = err || {};
        const { data } = response || {};
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const updateTipeSoalTestSubstansi = (id, name, value, status) => {
  const params = {
    id,
    name,
    value,
    status,
  };

  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/tessubstansi/update-tipesoal",
        params,
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const {
          Data = [],
          Detail,
          Status = false,
          Message = "",
        } = result || {};

        if (Status) resolve();
        else reject(Message);
      })
      .catch((err) => {
        const { response } = err || {};
        const { data } = response || {};
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const deleteTipeSoalTestSubstansi = (id) => {
  const params = {
    id,
  };

  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/tessubstansi/softdelete-tipesoal",
        params,
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const {
          Data = [],
          Detail,
          Status = false,
          Message = "",
        } = result || {};

        if (Status) resolve();
        else reject(Message);
      })
      .catch((err) => {
        const { response } = err || {};
        const { data } = response || {};
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const searchTipeSoalTestSubstansi = async (cari) => {
  const params = {
    cari,
  };

  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/tessubstansi/carifull-tipesoal",
        params,
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const {
          Data = [],
          Detail,
          Status = false,
          Message = "",
        } = result || {};

        if (Status) resolve();
        else reject(Message);
      })
      .catch((err) => {
        const { response } = err || {};
        const { data } = response || {};
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const reportTestSubstansi = async (
  id,
  start,
  rows,
  sortField,
  sortDir,
  keyword = "",
  statusPeserta = "99",
  nilai = "",
) => {
  const params = {
    id_substansi: id,
    start,
    rows,
    sort: `${sortField} ${sortDir}`,
    param: keyword,
    status: `99`,
    status_peserta: statusPeserta,
    nilai,
  };

  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/tessubstansi/report-list-substansi",
        params,
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        let {
          dash = [],
          Data = [],
          JumlahPeserta = [],
          Status = false,
          Message = "",
        } = result || {};
        const [d] = dash || [];
        const [j] = JumlahPeserta || [];
        const { jml_data } = j || {};

        Data = (Data || []).map(({ nilai = "", ...d }) => ({
          ...d,
          nilai: `${nilai.replace("-", "").trim() || 0}`,
        }));

        if (Status)
          resolve({
            dash: d,
            jml_data,
            Data,
          });
        else reject(Message);
      })
      .catch((err) => {
        const { response } = err || {};
        const { data } = response || {};
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};

export const resetPesertaSubstansi = async (params) => {
  return new Promise((resolve, reject) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/tessubstansi/reset_jawaban_peserta",
        params,
        configs,
      )
      .then((res) => {
        const { data } = res || {};
        const { result } = data;
        const { Status = false, Message = "" } = result || {};

        if (Status) resolve(Message);
        else reject(Message);
      })
      .catch((err) => {
        const { response } = err || {};
        const { data } = response || {};
        const { result } = data || {};
        const { Data, Status = false, Message = "" } = result || {};
        reject(Message);
      });
  });
};
