import { Observer } from "mobx-react-lite";
import React from "react";
import { letters } from "./utils";

export const PreviewComponent = ({
  questionIdx = null,
  question = "",
  question_image = null,
  answer = [],
  answer_key = "",
}) => {
  return (
    <Observer>
      {() => {
        return (
          <>
            <div className="row">
              <div className="col-lg-12 fv-row mb-7">
                <div>
                  <h5>
                    {questionIdx != null ? `${questionIdx + 1}. ` : ""}
                    {question}
                  </h5>
                  {question_image && (
                    <img
                      className="w-300px rounded mt-3 mb-3"
                      src={question_image}
                    />
                  )}
                </div>
              </div>
              {(answer || []).map((a, idx) => {
                const { image, key = "", option = "" } = a || {};
                return (
                  <div className="col-lg-12 fv-row mb-3">
                    <button
                      className={`btn btn-sm btn-flex btn-light fw-bolder d-flex justify-content-between ${
                        key.toLowerCase() == answer_key.toLowerCase()
                          ? "btn-success"
                          : ""
                      }`}
                      variant="outlined"
                      style={{ width: "100%", border: "1px solid" }}
                    >
                      <div className="text-start">
                        <span className="w-20px">{`${
                          key || letters[idx]
                        }. `}</span>
                        {option != null || option != undefined
                          ? option
                          : image && <img className="w-100px" src={image} />}
                        {!!option && image && (
                          <div>
                            <span className="w-20px"></span>
                            <img className="w-100px" src={image} />
                          </div>
                        )}
                      </div>
                      <div>
                        {key.toLowerCase() == answer_key.toLowerCase() && (
                          <div className="text-white">
                            <span
                              className="las la-key"
                              style={{ fontSize: 25 }}
                            />
                          </div>
                        )}
                      </div>

                      {/* <div>
                  {key && `${key || letters[idx]}. `} {option}
                </div>
                {image && <div><img src={image} /></div>}
                {key.toLowerCase() == answer_key.toLowerCase() && <div className="text-white"><span className="las la-key" /></div>} */}
                    </button>
                  </div>
                );
              })}
            </div>
          </>
        );
      }}
    </Observer>
  );
};

export const PreviewComponents = ({
  questions = [],
  questionIdx = null,
  selectedIdx = 0,
}) => {
  const [idx, setIdx] = React.useState(0);
  const question = questions.length > idx ? questions[idx] : null;

  React.useEffect(() => setIdx(selectedIdx), [questions]);
  React.useEffect(() => setIdx(selectedIdx), [selectedIdx]);

  return (
    <>
      <Observer>
        {() => (
          <PreviewComponent questionIdx={questionIdx || idx} {...question} />
        )}
      </Observer>
      <Observer>
        {() =>
          questionIdx == null && (
            <div className="row">
              <div className="col-lg-12">
                <Observer>
                  {() => {
                    return (
                      <div className="text-end mt-5">
                        <button
                          onClick={() => {
                            if (!(idx == 0)) setIdx(idx - 1);
                          }}
                          className={`btn btn-light btn-sm me-3 mr-2 ${
                            idx == 0 ? "disabled" : ""
                          }`}
                        >
                          <i className="fa fa-chevron-left me-1"></i>Sebelumnya
                        </button>
                        <button
                          onClick={() => {
                            if (idx < questions.length - 1) setIdx(idx + 1);
                          }}
                          className={`btn btn-primary btn-sm ${
                            idx < questions.length - 1 ? "" : "disabled"
                          }`}
                        >
                          Selanjutnya{" "}
                          <i className="fa fa-chevron-right ms-1"></i>
                        </button>
                      </div>
                    );
                  }}
                </Observer>
              </div>
            </div>
          )
        }
      </Observer>
    </>
  );
};

export const PreviewModal = ({
  id = "preview_soal",
  title = "Preview Soal",
  questionIdx = null,
  questions = [],
  onClose = () => {},
}) => {
  const ref = React.useRef();

  React.useEffect(() => {
    ref?.current?.addEventListener("hide.bs.modal", (e) => onClose());
  }, []);

  return (
    <div className="modal fade" tabindex="-1" id={id} ref={ref}>
      <div className="modal-dialog modal-lg">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">{title}</h5>
            <div
              className="btn btn-icon btn-sm btn-active-light-primary ms-2"
              data-bs-dismiss="modal"
              aria-label="Close"
            >
              <span className="svg-icon svg-icon-2x">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                >
                  <rect
                    opacity="0.5"
                    x="6"
                    y="17.3137"
                    width="16"
                    height="2"
                    rx="1"
                    transform="rotate(-45 6 17.3137)"
                    fill="currentColor"
                  />
                  <rect
                    x="7.41422"
                    y="6"
                    width="16"
                    height="2"
                    rx="1"
                    transform="rotate(45 7.41422 6)"
                    fill="currentColor"
                  />
                </svg>
              </span>
            </div>
          </div>
          <div className="modal-body">
            <Observer>
              {() => (
                <PreviewComponents
                  questionIdx={questionIdx}
                  questions={questions}
                />
              )}
            </Observer>
          </div>
          <div className="modal-footer">
            <button className="btn btn-danger btn-sm" data-bs-dismiss="modal">
              Close
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
