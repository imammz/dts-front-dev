import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";

export default class DashboardSubvitContent extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    datax: [],
    loading: false,
    total_substansi: 0,
    total_substansi_midtest: 0,
    total_substansi_test: 0,
    total_survey: 0,
    total_survey_evaluasi: 0,
    total_survey_umum: 0,
    total_trivia: 0,
    survey_evaluasi_batal: 0,
    survey_evaluasi_draft: 0,
    survey_evaluasi_publish: 0,
    survey_umum_batal: 0,
    survey_umum_draft: 0,
    survey_umum_publish: 0,
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    } else {
      console.log("run");
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/survey/subvit-dashboard",
          null,
          this.configs,
        )
        .then((res) => {
          const datax = res.data.result.Data[0];
          const statusx = res.data.result.Status;
          if (statusx) {
            const substansi_midtest_draft = datax.substansi_midtest_draft;
            const substansi_midtest_publish = datax.substansi_midtest_publish;
            const substansi_test_draft = datax.substansi_test_draft;
            const substansi_test_publish = datax.substansi_test_publish;
            const survey_evaluasi_draft = datax.survey_evaluasi_draft;
            const survey_evaluasi_publish = datax.survey_evaluasi_publish;
            const survey_evaluasi_batal = datax.survey_evaluasi_batal;
            const survey_umum_draft = datax.survey_umum_draft;
            const survey_umum_publish = datax.survey_umum_publish;
            const survey_umum_batal = datax.survey_umum_batal;
            const trivia_draft = datax.trivia_draft;
            const trivia_publish = datax.trivia_publish;
            const total_substansi =
              substansi_midtest_draft +
              substansi_midtest_publish +
              substansi_test_draft +
              substansi_test_publish;
            const total_substansi_test =
              substansi_test_draft + substansi_test_publish;
            const total_substansi_midtest =
              substansi_midtest_draft + substansi_midtest_publish;
            const total_survey =
              survey_evaluasi_draft +
              survey_evaluasi_publish +
              survey_umum_draft +
              survey_umum_publish +
              survey_umum_batal +
              survey_evaluasi_batal;
            const total_survey_umum =
              survey_umum_draft + survey_umum_publish + survey_umum_batal;
            const total_survey_evaluasi =
              survey_evaluasi_draft +
              survey_evaluasi_publish +
              survey_evaluasi_batal;
            const total_trivia = trivia_draft + trivia_publish;

            this.setState({
              total_substansi,
              total_substansi_test,
              total_substansi_midtest,
              total_survey,
              total_survey_umum,
              total_survey_evaluasi,
              total_trivia,
              survey_evaluasi_batal,
              survey_evaluasi_draft,
              survey_evaluasi_publish,
              survey_umum_batal,
              survey_umum_draft,
              survey_umum_publish,
            });
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }

  render() {
    return (
      <div style={{ minHeight: "500px" }}>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack"
          >
            <div
              data-kt-swapper="true"
              data-kt-swapper-mode="prepend"
              data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
              className="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0"
            >
              <h1 className="d-flex align-items-center text-dark fw-bolder fs-3 my-1">
                Page
              </h1>
              <span className="h-20px border-gray-200 border-start mx-4" />
              <ul className="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                <li className="breadcrumb-item text-muted">Subvit</li>
                <li className="breadcrumb-item">
                  <span className="bullet bg-gray-200 w-5px h-2px" />
                </li>
                <li className="breadcrumb-item text-muted">Dashboard</li>
              </ul>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-7"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 col-md-12 col-sm-12"></div>
                  <div className="col-lg-4 pt-2">
                    <div className="card border bg-success rounded-xl">
                      <div className="card-body">
                        <div className="row">
                          <div className="col-md-6 text-end">
                            <h1 className="display-2 text-light">
                              {this.state.total_substansi}
                            </h1>
                            <h2 className="fw-bolder text-light-success">
                              Tes Substansi
                            </h2>
                          </div>
                          <div className="col-md-6 text-end">
                            <h3 className="fw-bolder mt-5 text-light-success">
                              Substansi {this.state.total_substansi_test}
                            </h3>
                            <h3 className="fw-bolder mt-5 text-light-success">
                              MidTest {this.state.total_substansi_midtest}
                            </h3>
                          </div>
                        </div>
                      </div>
                    </div>
                    <a
                      href="/subvit/test-substansi"
                      className="btn btn-success mt-5 mb-20"
                      style={{ width: "100%" }}
                    >
                      List Substansi
                    </a>
                  </div>
                  <div className="col-lg-4 pt-2">
                    <div className="card border bg-danger">
                      <div className="card-body">
                        <div className="row">
                          <div className="col-md-6 text-end">
                            <h1 className="display-2 text-light">
                              {this.state.total_survey}
                            </h1>
                            <h2 className="fw-bolder text-light-danger">
                              Survey
                            </h2>
                          </div>
                          <div className="col-md-6 text-end">
                            <h3
                              className="fw-bolder mt-5 text-light-danger"
                              style={{ cursor: "pointer" }}
                              title={
                                "Publish : " +
                                this.state.survey_evaluasi_publish +
                                "\nDraft : " +
                                this.state.survey_evaluasi_draft +
                                "\nBatal : " +
                                this.state.survey_evaluasi_batal
                              }
                            >
                              Evaluasi {this.state.total_survey_evaluasi}
                            </h3>
                            <h3
                              className="fw-bolder mt-5 text-light-danger"
                              style={{ cursor: "pointer" }}
                              title={
                                "Publish : " +
                                this.state.survey_umum_publish +
                                "\nDraft : " +
                                this.state.survey_umum_draft +
                                "\nBatal : " +
                                this.state.survey_umum_batal
                              }
                            >
                              Umum {this.state.total_survey_umum}
                            </h3>
                          </div>
                        </div>
                      </div>
                    </div>
                    <a
                      href="/subvit/survey"
                      className="btn btn-danger mt-5 mb-20"
                      style={{ width: "100%" }}
                    >
                      List Survey
                    </a>
                  </div>
                  <div className="col-lg-4 pt-2">
                    <div className="card border bg-primary">
                      <div className="card-body">
                        <div className="row">
                          <div className="col-md-12 text-end">
                            <h1 className="display-2 text-light">
                              {this.state.total_trivia}
                            </h1>
                            <h1 className="fw-bolder text-light-primary">
                              Trivia
                            </h1>
                          </div>
                        </div>
                      </div>
                    </div>
                    <a
                      href="/subvit/trivia"
                      className="btn btn-primary mt-5 mb-20"
                      style={{ width: "100%" }}
                    >
                      List Trivia
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
