import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import {
  capitalizeTheFirstLetterOfEachWord,
  indonesianDateFormat,
  statusPelaksanaan,
} from "../../publikasi/helper";
import ReviewSoal from "./Review-Soal";

export default class TriviaListSoal extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickDelete = this.handleClickDeleteAction.bind(this);
    this.handleKembali = this.handleKembaliAction.bind(this);
    this.handleChangeSearch = this.handleChangeSearchAction.bind(this);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleClickReviewSoal = this.handleClickReviewSoalAction.bind(this);
    this.handleClickPreviewSingle =
      this.handleClickPreviewSingleAction.bind(this);
    this.nextSoal = this.nextSoalAction.bind(this);
    this.prevSoal = this.prevSoalAction.bind(this);
    this.handleChangeCheckbox = this.handleChangeCheckboxAction.bind(this);
    this.handleDeleteSoal = this.handleDeleteSoalAction.bind(this);
    this.handleDeleteTrivia = this.handleDeleteTriviaAction.bind(this);
  }
  state = {
    datax: [],
    datax_trivia: [],
    datax_all_akademi: [],
    datax_all_tema: [],
    datax_all_pelatihan: [],
    datax_all_pelatihan_filter: [],
    datax_akademi: [],
    datax_tema: [],
    datax_pelatihan: [],
    loading: true,
    tempLastNumber: 0,
    tempLastNumberAkademi: 0,
    tempLastNumberTema: 0,
    tempLastNumberPelatihan: 0,
    newPerPage: 10,
    newPerPageAkademi: 10,
    newPerPageTema: 10,
    newPerPagePelatihan: 10,
    id_trivia: null,
    totalRows: 0,
    totalRowsAkademi: 0,
    totalRowsTema: 0,
    totalRowsPelatihan: 0,
    currentPage: 1,
    currentPageAkademi: 0,
    currentPageTema: 0,
    currentPagePelatihan: 0,
    isSearch: false,
    param: "",
    review_soal: false,
    all_soal: [],
    no_soal: 1,
    arr_id_checked: [],
    update: [],
    idakademi: 0,
    idtema: 0,
    nama_trivia: "",
    idpelatihan: 0,
    start_at: "",
    end_at: "",
    question_to_share: 0,
    duration: 0,
    id_status_peserta: "",
    id_user: 0,
    show_next_prev: true,
    status_trivia: null,
    disabled_button: false,
    disabled_hapus_button: true,
    datax_master: [],
    level: 0,
    status_pelaksanaan: 0,
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  color = ["secondary", "primary", "success"];
  status_text = ["Belum Dilaksanakan", "Sedang Berlangsung", "Selesai"];

  columnsAkademi = [
    {
      name: "No",
      cell: (row, index) => this.state.tempLastNumberAkademi + index + 1,
      width: "70px",
    },
    {
      name: "Akademi",
      sortable: false,
      selector: (row) => row.akademi,
    },
    {
      name: "Pelatihan",
      sortable: false,
      selector: (row) => row.jml_pelatihan,
    },
    {
      name: "Status",
      sortable: true,
      center: true,
      width: "120px",
      selector: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.status == "1" ? "success" : "danger") +
              " fs-7 m-1"
            }
          >
            {row.status == 1 ? "Publish" : "Unpublish"}
          </span>
        </div>
      ),
    },
  ];

  columnsTema = [
    {
      name: "No",
      cell: (row, index) => this.state.tempLastNumberTema + index + 1,
      width: "70px",
    },
    {
      name: "Tema",
      sortable: false,
      selector: (row) => row.tema,
    },
    {
      name: "Pelatihan",
      sortable: false,
      selector: (row) => row.jml_pelatihan,
    },
    {
      name: "Status",
      sortable: true,
      center: true,
      width: "120px",
      selector: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.status == "1" ? "success" : "danger") +
              " fs-7 m-1"
            }
          >
            {row.status == 1 ? "Publish" : "Unpublish"}
          </span>
        </div>
      ),
    },
  ];

  columnsPelatihan = [
    {
      name: "No",
      cell: (row, index) => this.state.tempLastNumberPelatihan + index + 1,
      width: "70px",
    },
    {
      name: "Nama Pelatihan",
      sortable: false,
      width: "300px",
      selector: (row) => row.pelatihan,
    },
    {
      name: "Jadwal Pelatihan",
      sortable: false,
      width: "300px",
      selector: (row) =>
        indonesianDateFormat(row.pelatihan_mulai) +
        " - " +
        indonesianDateFormat(row.pelatihan_selesai),
    },
    {
      name: "Status Pelatihan",
      sortable: false,
      width: "150px",
      selector: (row) => row.status_pelatihan,
    },
    {
      name: "Status",
      sortable: true,
      center: true,
      selector: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.status_publish == "1" ? "success" : "danger") +
              " fs-7 m-1"
            }
          >
            {row.status_publish == 1 ? "Publish" : "Unpublish"}
          </span>
        </div>
      ),
    },
  ];

  columns = [
    {
      center: true,
      cell: (row, index) => this.state.tempLastNumber + index + 1,
      width: "70px",
    },
    {
      name: "Soal",
      className: "min-w-300px mw-300px",
      grow: 6,
      sortable: true,
      sortField: "question",
      wrap: true,
      allowOverflow: false,
      //width: '600px',
      selector: (row) => (
        <>
          <label className="d-flex flex-stack mb- mt-1">
            <span className="d-flex align-items-center me-2">
              <span className="d-flex flex-column">
                <h6 className="fw-bolder fs-7 mb-0">{row.pertanyaan}</h6>
              </span>
            </span>
          </label>
        </>
      ),
    },
    {
      className: "min-w-200px mw-200px",
      sortType: "basic",
      sortable: true,
      name: "Tipe Soal",
      grow: 3,
      //selector: row => capitalizeTheFirstLetterOfEachWord(row.type.split("_").join(" "))
      selector: (row) => "Objective",
    },

    {
      name: "Aksi",
      center: true,
      width: "250px",
      grow: 3,
      cell: (row, index) => (
        <div>
          {/* Review Soal */}
          <a
            href="#"
            id={row.id_trivia}
            nosoal={this.state.tempLastNumber + index + 1}
            data-bs-toggle="modal"
            data-bs-target="#review_soal"
            onClick={this.handleClickPreviewSingle}
            title="Review Soal"
            className="btn btn-icon btn-bg-primary btn-sm me-1"
          >
            <i className="fa fa-eye text-white"></i>
          </a>

          {/* Edit Survei */}
          <a
            href={
              "/subvit/trivia/edit-soal/" +
              row.pidtrivia_detail +
              "/" +
              (this.state.tempLastNumber + index + 1)
            }
            title="Edit"
            className={
              row.is_master || row.isDisabledPublish
                ? "disabled btn btn-icon btn-bg-warning btn-sm me-1"
                : "btn btn-icon btn-bg-warning btn-sm me-1"
            }
          >
            <i className="bi bi-gear-fill text-white"></i>
          </a>
          <a
            href="#"
            id={row.pidtrivia_detail}
            onClick={this.handleClickDelete}
            title="Hapus"
            className={
              row.is_master || row.isDisabledPublish
                ? "disabled btn btn-icon btn-bg-danger btn-sm me-1"
                : "btn btn-icon btn-bg-danger btn-sm me-1"
            }
          >
            <i className="bi bi-trash-fill text-white"></i>
          </a>
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  level = ["Akademi", "Tema", "Pelatihan", "All Level DTS"];

  rowDisabledCriteria = (row) => row.isDisabled;

  componentDidMount() {
    const temp_storage = localStorage.getItem("dataMenus");
    localStorage.clear();
    localStorage.setItem("dataMenus", temp_storage);
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    let segment_url = window.location.pathname.split("/");
    let id_trivia = segment_url[4];

    const dataBody = {
      id: id_trivia,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/trivia_select_byid",
        dataBody,
        this.configs,
      )
      .then((res) => {
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statusx) {
          let datax_trivia = res.data.result.trivia[0];
          const status_pelaksanaan = statusPelaksanaan(
            datax_trivia.start_at,
            datax_trivia.end_at,
          );
          //jika statusnya survei evaluasi dan draft
          this.setState(
            {
              datax_trivia: datax_trivia,
              nama_trivia: datax_trivia.nama_trivia,
              start_at: datax_trivia.start_at,
              end_at: datax_trivia.end_at,
              duration: datax_trivia.duration,
              id_status_peserta: datax_trivia.id_status_peserta,
              id_user: Cookies.get("user_id"),
              status_trivia: datax_trivia.status,
              id_trivia: id_trivia,
              level: datax_trivia.level,
              status_pelaksanaan,
            },
            () => {
              this.loadAllSoal();
              this.handleReload();
            },
          );
          const datax_all_pelatihan = res.data.result.pelatihan;
          const datax_all_pelatihan_filter = datax_all_pelatihan.filter(
            (element) => element.pelatihan_id !== null,
          );
          const datax_all_akademi = [];
          const datax_all_tema = [];
          datax_all_pelatihan.forEach(function (pelatihan, i) {
            if (pelatihan.tema_id != null) {
              const tema = {
                tema: pelatihan.tema,
                tema_id: pelatihan.tema_id,
                status: pelatihan.status_tema,
              };

              let is_tema_push = true;
              datax_all_tema.forEach(function (singleTema, i) {
                if (singleTema.tema_id == pelatihan.tema_id) {
                  is_tema_push = false;
                }
              });
              if (is_tema_push) {
                datax_all_tema.push(tema);
              }
            }

            if (pelatihan.akademi_id != null) {
              const akademi = {
                akademi: pelatihan.akademi,
                akademi_id: pelatihan.akademi_id,
                status: pelatihan.status_akademi,
              };

              let is_akademi_push = true;
              datax_all_akademi.forEach(function (singleAkademi, i) {
                if (singleAkademi.akademi_id == pelatihan.akademi_id) {
                  is_akademi_push = false;
                }
              });
              if (is_akademi_push) {
                datax_all_akademi.push(akademi);
              }
            }
          });
          //hitung pelatihan di akademi
          datax_all_akademi.forEach(function (akademi, i) {
            akademi.jml_pelatihan = 0;
            datax_all_pelatihan.forEach(function (pelatihan, j) {
              if (
                pelatihan.akademi_id == akademi.akademi_id &&
                pelatihan.pelatihan_id != null
              ) {
                akademi.jml_pelatihan = akademi.jml_pelatihan + 1;
              }
            });
          });

          //hitung tema di akademi
          datax_all_tema.forEach(function (tema, i) {
            tema.jml_pelatihan = 0;
            datax_all_pelatihan.forEach(function (pelatihan, j) {
              if (
                pelatihan.tema_id == tema.tema_id &&
                pelatihan.tema_id != null
              ) {
                tema.jml_pelatihan = tema.jml_pelatihan + 1;
              }
            });
          });

          const datax_pelatihan = datax_all_pelatihan_filter.slice(0, 10);
          const datax_tema = datax_all_tema.slice(0, 10);
          const datax_akademi = datax_all_akademi.slice(0, 10);
          const totalRowsAkademi = datax_all_akademi.length;
          const totalRowsTema = datax_all_tema.length;
          const totalRowsPelatihan = datax_all_pelatihan.length;

          this.setState({
            datax_pelatihan,
            datax_tema,
            datax_akademi,
            datax_all_akademi,
            datax_all_tema,
            datax_all_pelatihan,
            datax_all_pelatihan_filter,
            totalRowsAkademi,
            totalRowsTema,
            totalRowsPelatihan,
          });
          swal.close();
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.handleReload();
              }
            });
        }
      })
      .catch((error) => {
        console.log(error);
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  loadAllSoal() {
    //untuk load allsoal yg akan dilempar lagi jika ada swap nomor
    let dataBody = {
      id_trivia: this.state.id_trivia,
      start: 0,
      rows: 100,
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list_soal_trivia_byid",
        dataBody,
        this.configs,
      )
      .then((res) => {
        const datax = res.data.result.Data;
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statusx) {
          datax.forEach(function (element, i) {
            const soal = {
              nosoal: i + 1,
              tipe: element.type,
              pertanyaan: element.pertanyaan,
              gambar_pertanyaan: element.pertanyaan_gambar,
              jawaban: JSON.parse(element.jawaban),
              status: 1,
              id_user: Cookies.get("user_id"),
            };
            localStorage.setItem(i + 1, JSON.stringify(soal));
          });
        } else {
        }
      })
      .catch((error) => {});
  }

  handleReload(page, newPerPage) {
    this.setState({
      datax: [],
      loading: true,
    });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });

    let dataBody = {
      id_trivia: this.state.id_trivia,
      start: start_tmp,
      rows: length_tmp,
      param: this.state.param,
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list_soal_trivia_byid",
        dataBody,
        this.configs,
      )
      .then((res) => {
        this.setState({ loading: false });
        const datax = res.data.result.Data;
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statusx) {
          let disabled_button = false;
          if (this.state.status_trivia == 1) {
            disabled_button = true;
            datax.forEach(function (soal, i) {
              datax[i].isDisabled = true;
              datax[i].isDisabledPublish = true;
            });
          }
          this.setState({
            datax,
            disabled_button,
          });

          this.setState({ totalRows: res.data.result.JumlahData });
          this.setState({ currentPage: page });
          localStorage.setItem("last_number", res.data.result.JumlahData);
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.handleReload();
              }
            });
        }
      })
      .catch((error) => {
        this.setState({ loading: false });
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  handlePageChange = (page) => {
    this.setState({ loading: true });
    this.handleReload(page, this.state.newPerPage);
  };
  handlePerRowsChange = async (newPerPage, page) => {
    this.setState({ loading: true });
    this.setState({ newPerPage: newPerPage });
    this.handleReload(page, newPerPage);
  };

  handlePageChangeAkademi = (page) => {
    this.setState({ loading_akademi: true });
    this.handleReloadAkademi(page, this.state.newPerPageAkademi);
  };
  handlePerRowsChangeAkademi = async (newPerPage, page) => {
    this.setState({ loading_akademi: true });
    this.setState({ newPerPageAkademi: newPerPage });
    this.handleReloadAkademi(page, newPerPage);
  };

  handleReloadAkademi(page, newPerPage) {
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumberAkademi: start_tmp });
    const start = start_tmp;
    const rows = length_tmp + start_tmp;
    const datax_akademi = this.state.datax_all_akademi.slice(start, rows);
    this.setState({
      datax_akademi,
      loading_akademi: false,
    });
  }

  handlePageChangeTema = (page) => {
    this.setState({ loading_tema: true });
    this.handleReloadTema(page, this.state.newPerPageTema);
  };
  handlePerRowsChangeTema = async (newPerPage, page) => {
    this.setState({ loading_tema: true });
    this.setState({ newPerPageTema: newPerPage });
    this.handleReloadTema(page, newPerPage);
  };

  handleReloadTema(page, newPerPage) {
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumberTema: start_tmp });
    const start = start_tmp;
    const rows = length_tmp + start_tmp;
    const datax_tema = this.state.datax_all_tema.slice(start, rows);
    this.setState({
      datax_tema,
      loading_tema: false,
    });
  }

  handlePageChangePelatihan = (page) => {
    this.setState({ loading_pelatihan: true });
    this.handleReloadPelatihan(page, this.state.newPerPagePelatihan);
  };
  handlePerRowsChangePelatihan = async (newPerPage, page) => {
    this.setState({ loading_pelatihan: true });
    this.setState({ newPerPagePelatihan: newPerPage });
    this.handleReloadPelatihan(page, newPerPage);
  };

  handleReloadPelatihan(page, newPerPage) {
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumberPelatihan: start_tmp });
    const start = start_tmp;
    const rows = length_tmp + start_tmp;
    const datax_pelatihan = this.state.datax_all_pelatihan.slice(start, rows);
    this.setState({
      datax_pelatihan,
      loading_pelatihan: false,
    });
  }

  handleChangeSearchAction(e) {
    const searchText = e.currentTarget.value;
    if (searchText == "") {
      this.setState(
        {
          loading: true,
          param: "",
        },
        () => {
          this.handleReload();
        },
      );
    }
  }
  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    if (e.key == "Enter") {
      if (searchText == "") {
        this.setState(
          {
            isSearch: false,
            param: "",
          },
          () => {
            this.handleReload();
          },
        );
      } else {
        this.setState({ loading: true });
        this.setState({ isSearch: true });
        this.setState({ param: searchText }, () => {
          this.handleReload();
        });
      }
    }
  }

  handleClickDeleteAction(e) {
    if (this.state.totalRows == 1) {
      swal
        .fire({
          title: "Minimal Ada 1 Soal di Trivia",
          icon: "warning",
          confirmationButtonText: "Ok",
        })
        .then((result) => {});
    } else {
      const idx = e.currentTarget.id;
      swal
        .fire({
          title: "Apakah anda yakin ?",
          text: "Data ini tidak bisa dikembalikan!",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Ya, hapus!",
          cancelButtonText: "Tidak",
        })
        .then((result) => {
          if (result.isConfirmed) {
            const data = { id: idx };
            axios
              .post(
                process.env.REACT_APP_BASE_API_URI + "/soaltrivia_delete",
                data,
                this.configs,
              )
              .then((res) => {
                const statux = res.data.result.Status;
                const messagex = res.data.result.Message;
                if (statux) {
                  swal
                    .fire({
                      title: messagex,
                      icon: "success",
                      confirmButtonText: "Ok",
                    })
                    .then((result) => {
                      if (result.isConfirmed) {
                        this.handleReload(
                          this.state.currentPage,
                          this.state.newPerPage,
                        );
                      }
                    });
                } else {
                  swal
                    .fire({
                      title: messagex,
                      icon: "warning",
                      confirmationBUttonText: "Ok",
                    })
                    .then((result) => {
                      if (result.isConfirmed) {
                        this.handleReload();
                      }
                    });
                }
              })
              .catch((error) => {
                let statux = error.response.data.result.Status;
                let messagex = error.response.data.result.Message;
                if (!statux) {
                  swal
                    .fire({
                      title: messagex,
                      icon: "warning",
                      confirmButtonText: "Ok",
                    })
                    .then((result) => {
                      if (result.isConfirmed) {
                        this.handleReload();
                      }
                    });
                }
              });
          }
        });
    }
  }

  handleKembaliAction() {
    window.history.back();
  }

  handleClickReviewSoalAction(e) {
    //dibuat false dulu biar reset modal
    this.setState({ review_soal: false });
    const id = e.currentTarget.getAttribute("id");
    this.setState(
      {
        show_next_prev: true,
        no_soal: 1,
      },
      () => this.loadSoalAction(),
    );
  }

  handleClickPreviewSingleAction(e) {
    e.preventDefault();
    this.setState({ review_soal: false });
    const nosoal = e.currentTarget.getAttribute("nosoal");
    this.setState(
      {
        show_next_prev: false,
        no_soal: nosoal,
      },
      () => this.loadSoalAction(),
    );
  }

  nextSoalAction() {
    this.setState(
      {
        review_soal: false,
        show_next_prev: true,
        no_soal: this.state.no_soal + 1,
      },
      () => this.loadSoalAction(),
    );
  }

  prevSoalAction() {
    this.setState(
      {
        review_soal: false,
        show_next_prev: true,
        no_soal: this.state.no_soal - 1,
      },
      () => this.loadSoalAction(),
    );
  }

  loadSoalAction() {
    const data = {
      id_trivia: this.state.id_trivia,
      start: 0,
      rows: 100,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list_soal_trivia_byid",
        data,
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          const datax = res.data.result.Data;
          const soal = (
            <ReviewSoal
              soal={datax[this.state.no_soal - 1]}
              no={this.state.no_soal}
            />
          );
          this.setState({
            review_soal: soal,
            all_soal: datax,
          });
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmationBUttonText: "Ok",
            })
            .then((result) => {
              this.setState({
                show_next_prev: false,
                review_soal: (
                  <div className="text-center">Tidak Ada Data Soal</div>
                ),
              });
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  handleChangeCheckboxAction = ({ selectedRows }) => {
    const arr_id = [];
    selectedRows.forEach(function (element, i) {
      arr_id.push(element.pidtrivia_detail);
    });
    this.setState({ arr_id_checked: arr_id }, () => {
      if (this.state.arr_id_checked.length == 0) {
        this.setState({ disabled_hapus_button: true });
      } else {
        this.setState({ disabled_hapus_button: false });
      }
    });
  };

  handleDeleteSoalAction() {
    if (this.state.arr_id_checked.length == this.state.totalRows) {
      swal
        .fire({
          title: "Anda Tidak Bisa Menghapus Semua Soal",
          icon: "warning",
          confirmationBUttonText: "Ok",
        })
        .then((result) => {});
    } else if (this.state.arr_id_checked.length > 0) {
      const arr_id_checked_temp = this.state.arr_id_checked;
      this.setState({
        arr_id_checked: [],
        disabled_hapus_button: true,
      });
      swal
        .fire({
          title: "Apakah anda yakin ?",
          text: "Data ini tidak bisa dikembalikan!",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Ya, hapus!",
          cancelButtonText: "Tidak",
        })
        .then((result) => {
          if (result.isConfirmed) {
            swal.fire({
              title: "Mohon Tunggu!",
              icon: "info", // add html attribute if you want or remove
              allowOutsideClick: false,
              didOpen: () => {
                swal.showLoading();
              },
            });
            let arr_id_checked = arr_id_checked_temp.join(",");
            const dataBody = {
              id: "array[" + arr_id_checked + "]",
            };
            axios
              .post(
                process.env.REACT_APP_BASE_API_URI + "/soaltrivia_delete_bulk",
                dataBody,
                this.configs,
              )
              .then((res) => {
                const statusx = res.data.result.Status;
                const messagex = res.data.result.Message;
                if (statusx) {
                  swal
                    .fire({
                      title: messagex,
                      icon: "success",
                      confirmButtonText: "Ok",
                    })
                    .then((result) => {
                      if (result.isConfirmed) {
                        this.handleReload();
                      }
                    });
                } else {
                  swal.close();
                  swal
                    .fire({
                      title: messagex,
                      icon: "warning",
                      confirmButtonText: "Ok",
                    })
                    .then((result) => {
                      if (result.isConfirmed) {
                      }
                    });
                }
              })
              .catch((error) => {
                swal.close();
                let statux = error.response.data.result.Status;
                let messagex = error.response.data.result.Message;
                if (!statux) {
                  swal
                    .fire({
                      title: messagex,
                      icon: "warning",
                      confirmButtonText: "Ok",
                    })
                    .then((result) => {
                      if (result.isConfirmed) {
                      }
                    });
                }
              });
          }
        });
    } else {
      swal
        .fire({
          title: "Belum Ada Soal yang Dipilih",
          icon: "warning",
          confirmationBUttonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleDeleteTriviaAction(e) {
    const idx = this.state.id_trivia;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          const data = { id: idx };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/trivia_delete",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                    allowOutsideClick: false,
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      window.location = "/subvit/trivia";
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmationBUttonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            });
        }
      });
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <path
                        opacity="0.3"
                        d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                        fill="#7239ea"
                      ></path>
                      <path
                        d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                        fill="#7239ea"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Subvit
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Trivia
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <a
                  onClick={this.handleKembali}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </a>

                <a
                  href={"/subvit/trivia/edit/" + this.state.id_trivia}
                  className="btn btn-warning fw-bolder btn-sm me-2"
                >
                  <i className="bi bi-gear-fill"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Edit
                  </span>
                </a>
                {this.state.status_trivia != 1 ? (
                  <a
                    href="#"
                    title="Hapus"
                    onClick={this.handleDeleteTrivia}
                    className="btn btn-sm btn-danger btn-active-light-info"
                  >
                    <i className="bi bi-trash-fill text-white"></i>
                    <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                      Hapus
                    </span>
                  </a>
                ) : (
                  <a
                    href="#"
                    title="Hapus"
                    className="disabled btn btn-sm btn-danger btn-active-light-info"
                  >
                    <i className="bi bi-trash-fill text-white"></i>
                    <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                      Hapus
                    </span>
                  </a>
                )}
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div
                    className="stepper stepper-links d-flex flex-column"
                    id="kt_subvit_trivia_list_soal"
                  >
                    <div className="col-lg-12 mt-7">
                      <div className="card border">
                        <div className="card-body mt-5 pt-10 pb-8">
                          <div className="col-12">
                            <h1
                              className="align-items-center text-dark fw-bolder my-1 fs-4"
                              style={{ textTransform: "capitalize" }}
                            >
                              {this.state.nama_trivia}
                            </h1>
                            <p className="text-muted fs-7 mb-0">
                              {this.level[this.state.level]}
                            </p>
                          </div>
                        </div>
                        <div className="card-header">
                          <div className="card-title">
                            <div className="stepper-nav flex-wrap mb-n4">
                              <div
                                className="stepper-item ms-0 my-2 current"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label ms-0">
                                    <h3 className="stepper-title fs-6">
                                      Bank Soal
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Targeting
                                    </h3>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="card-body">
                          <form
                            action="#"
                            onSubmit={this.handleSubmit}
                            id="kt_subvit_trivia_list_soal_form"
                          >
                            {/* MENU 1 */}
                            <div
                              className="flex-column current"
                              data-kt-stepper-element="content"
                            >
                              <div className="row mt-7">
                                <div className="col-lg-6 mb-7">
                                  <label className="form-label">
                                    Tgl. Mulai Trivia
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {indonesianDateFormat(
                                        this.state.datax_trivia.start_at,
                                      )}
                                    </b>
                                  </div>
                                </div>
                                <div className="col-lg-6 mb-7">
                                  <label className="form-label">
                                    Tgl. Akhir Trivia
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {indonesianDateFormat(
                                        this.state.datax_trivia.end_at,
                                      )}
                                    </b>
                                  </div>
                                </div>
                                <div className="col-lg-6 mb-7">
                                  <label className="form-label">
                                    Status Survey
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      <span
                                        className={
                                          "badge badge-light-" +
                                          (this.state.datax_trivia.status == 1
                                            ? "success"
                                            : "danger") +
                                          " fs-7 m-1"
                                        }
                                      >
                                        {this.state.datax_trivia.status == 1
                                          ? "Publish"
                                          : "Draft"}
                                      </span>
                                    </b>
                                  </div>
                                </div>
                                <div className="col-lg-6 mb-7">
                                  <label className="form-label">
                                    Status Pelaksanaan
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      <span
                                        className={
                                          "badge badge-" +
                                          this.color[
                                            this.state.status_pelaksanaan
                                          ] +
                                          " fs-7 m-1"
                                        }
                                      >
                                        {
                                          this.status_text[
                                            this.state.status_pelaksanaan
                                          ]
                                        }
                                      </span>
                                    </b>
                                  </div>
                                </div>
                              </div>
                              <div>
                                <div className="mt-5 border-top mx-0 my-10"></div>
                              </div>

                              <div className="card-header m-0 p-0">
                                <div className="card-title">
                                  <span className="svg-icon svg-icon-1 position-absolute ms-6">
                                    <svg
                                      width="24"
                                      height="24"
                                      viewBox="0 0 24 24"
                                      fill="none"
                                      xmlns="http://www.w3.org/2000/svg"
                                      className="mh-50px"
                                    >
                                      <rect
                                        opacity="0.5"
                                        x="17.0365"
                                        y="15.1223"
                                        width="8.15546"
                                        height="2"
                                        rx="1"
                                        transform="rotate(45 17.0365 15.1223)"
                                        fill="currentColor"
                                      ></rect>
                                      <path
                                        d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                        fill="currentColor"
                                      ></path>
                                    </svg>
                                  </span>
                                  <input
                                    type="text"
                                    data-kt-user-table-filter="search"
                                    className="form-control form-control-sm form-control-solid w-250px ps-14"
                                    placeholder="Cari Soal"
                                    onKeyPress={this.handleKeyPress}
                                    onChange={this.handleChangeSearch}
                                  />
                                </div>
                                <div className="card-toolbar">
                                  <div className="d-flex align-items-center position-relative my-1 me-2">
                                    <button
                                      className="btn btn-light fw-bolder btn-sm ms-2"
                                      data-kt-menu-trigger="click"
                                      data-kt-menu-placement="bottom-end"
                                      data-kt-menu-flip="top-end"
                                    >
                                      <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                        Kelola Soal
                                      </span>
                                      <i className="bi bi-chevron-down ms-1"></i>
                                    </button>
                                    <div
                                      className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-7 w-auto "
                                      data-kt-menu="true"
                                    >
                                      <div>
                                        <div className="menu-item">
                                          <a
                                            href="#"
                                            className="menu-link px-5"
                                            data-bs-toggle="modal"
                                            data-bs-target="#review_soal"
                                            onClick={this.handleClickReviewSoal}
                                          >
                                            <i className="bi bi-eye text-dark  me-1"></i>
                                            Preview Survey
                                          </a>
                                        </div>

                                        {!this.state.disabled_button ? (
                                          <div className="menu-item">
                                            <a
                                              href={
                                                "/subvit/trivia/add-soal/" +
                                                this.state.id_trivia
                                              }
                                              className="menu-link px-5"
                                            >
                                              <i className="bi bi-plus-circle text-dark  me-1"></i>
                                              Tambah Soal
                                            </a>
                                          </div>
                                        ) : (
                                          ""
                                        )}
                                        {!this.state.disabled_button ? (
                                          <div className="menu-item">
                                            <a
                                              href={
                                                "/subvit/trivia/list-soal/" +
                                                this.state.id_trivia +
                                                "/import"
                                              }
                                              className="menu-link px-5"
                                            >
                                              <i className="bi bi-file text-dark me-1"></i>
                                              Import Soal
                                            </a>
                                          </div>
                                        ) : (
                                          ""
                                        )}
                                      </div>
                                    </div>
                                    {!this.state.disabled_button ? (
                                      <a
                                        onClick={this.handleDeleteSoal}
                                        className="btn btn-danger btn-sm ms-2"
                                      >
                                        <i className="bi bi-trash-fill me-1"></i>
                                        Hapus Soal
                                      </a>
                                    ) : (
                                      ""
                                    )}
                                  </div>
                                </div>
                              </div>
                              <DataTable
                                columns={this.columns}
                                data={this.state.datax}
                                progressPending={this.state.loading}
                                highlightOnHover
                                pointerOnHover
                                pagination
                                paginationServer
                                paginationTotalRows={this.state.totalRows}
                                onChangeRowsPerPage={this.handlePerRowsChange}
                                onChangePage={this.handlePageChange}
                                customStyles={this.customStyles}
                                persistTableHead={true}
                                noDataComponent={
                                  <div className="mt-5">Tidak Ada Data</div>
                                }
                                selectableRows
                                onSelectedRowsChange={this.handleChangeCheckbox}
                                selectableRowDisabled={this.rowDisabledCriteria}
                                /* onSort={this.handleSort}
                                                            sortServer */
                              />
                            </div>

                            {/* MENU 2 */}
                            <div
                              className="flex-column"
                              data-kt-stepper-element="content"
                            >
                              <div className="row pt-7">
                                <h2 className="fs-5 text-muted mb-3">
                                  Target Akademi
                                </h2>
                                <div className="col-lg-12">
                                  <div className="table-responsive">
                                    <DataTable
                                      columns={this.columnsAkademi}
                                      data={this.state.datax_akademi}
                                      progressPending={
                                        this.state.loading_akademi
                                      }
                                      highlightOnHover
                                      pointerOnHover
                                      pagination
                                      paginationServer
                                      paginationTotalRows={
                                        this.state.totalRowsAkademi
                                      }
                                      onChangeRowsPerPage={
                                        this.handlePerRowsChangeAkademi
                                      }
                                      onChangePage={
                                        this.handlePageChangeAkademi
                                      }
                                      customStyles={this.customStyles}
                                      persistTableHead={true}
                                      noDataComponent={
                                        <div className="mt-5">
                                          Tidak Ada Data
                                        </div>
                                      }
                                    />
                                  </div>
                                </div>
                              </div>

                              <div className="row pt-7">
                                <h2 className="fs-5 text-muted mb-3">
                                  Target Tema
                                </h2>
                                <div className="col-lg-12">
                                  <div className="table-responsive">
                                    <DataTable
                                      columns={this.columnsTema}
                                      data={this.state.datax_tema}
                                      progressPending={this.state.loading_tema}
                                      highlightOnHover
                                      pointerOnHover
                                      pagination
                                      paginationServer
                                      paginationTotalRows={
                                        this.state.totalRowsTema
                                      }
                                      onChangeRowsPerPage={
                                        this.handlePerRowsChangeTema
                                      }
                                      onChangePage={this.handlePageChangeTema}
                                      customStyles={this.customStyles}
                                      persistTableHead={true}
                                      noDataComponent={
                                        <div className="mt-5">
                                          Tidak Ada Data
                                        </div>
                                      }
                                    />
                                  </div>
                                </div>
                              </div>

                              <div className="row pt-7">
                                <h2 className="fs-5 text-muted mb-3">
                                  Target Pelatihan
                                </h2>
                                <div className="col-lg-12">
                                  <div className="table-responsive">
                                    <DataTable
                                      columns={this.columnsPelatihan}
                                      data={this.state.datax_pelatihan}
                                      progressPending={
                                        this.state.loading_pelatihan
                                      }
                                      highlightOnHover
                                      pointerOnHover
                                      pagination
                                      paginationServer
                                      paginationTotalRows={
                                        this.state.totalRowsPelatihan
                                      }
                                      onChangeRowsPerPage={
                                        this.handlePerRowsChangePelatihan
                                      }
                                      onChangePage={
                                        this.handlePageChangePelatihan
                                      }
                                      customStyles={this.customStyles}
                                      persistTableHead={true}
                                      noDataComponent={
                                        <div className="mt-5">
                                          Tidak Ada Data
                                        </div>
                                      }
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="modal fade" tabIndex="-1" id="review_soal">
          <div className="modal-dialog modal-lg">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Review Soal</h5>
                <div
                  className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <span className="svg-icon svg-icon-2x">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <rect
                        opacity="0.5"
                        x="6"
                        y="17.3137"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(-45 6 17.3137)"
                        fill="currentColor"
                      />
                      <rect
                        x="7.41422"
                        y="6"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(45 7.41422 6)"
                        fill="currentColor"
                      />
                    </svg>
                  </span>
                </div>
              </div>
              <div className="modal-body">
                {this.state.review_soal}
                {this.state.show_next_prev ? (
                  <div className="text-end mt-5">
                    {this.state.no_soal == 1 ? (
                      <button
                        onClick={this.prevSoal}
                        className="btn btn-light btn-sm me-3 mr-2 disabled"
                      >
                        <i className="fa fa-chevron-left me-1"></i>Sebelumnya
                      </button>
                    ) : (
                      <button
                        onClick={this.prevSoal}
                        className="btn btn-light btn-sm me-3 mr-2"
                      >
                        <i className="fa fa-chevron-left me-1"></i>Sebelumnya
                      </button>
                    )}
                    {this.state.no_soal == this.state.all_soal.length ? (
                      <button
                        onClick={this.nextSoal}
                        className="disabled btn btn-primary btn-sm"
                      >
                        Selanjutnya<i className="fa fa-chevron-right ms-1"></i>
                      </button>
                    ) : (
                      <button
                        onClick={this.nextSoal}
                        className="btn btn-primary btn-sm"
                      >
                        Selanjutnya<i className="fa fa-chevron-right ms-1"></i>
                      </button>
                    )}
                  </div>
                ) : (
                  ""
                )}
              </div>
              <div className="modal-footer">
                <button
                  className="btn btn-light btn-sm"
                  data-bs-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
