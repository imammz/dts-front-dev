import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../Trivia-Add-Soal";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const TriviaAddSoal = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default TriviaAddSoal;
