import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../form/Trivia-Add-Soal/ImportForm";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const TriviaListImport = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default TriviaListImport;
