import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../Trivia-Edit-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const TriviaEdit = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default TriviaEdit;
