import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../Trivia-List-Soal";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const SurveyListSoal = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default SurveyListSoal;
