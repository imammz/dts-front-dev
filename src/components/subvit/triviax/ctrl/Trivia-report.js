import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../Trivia-Report-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const TriviaReport = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default TriviaReport;
