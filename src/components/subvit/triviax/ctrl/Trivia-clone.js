import React from "react";
import Header from "../../../Header";
import Content from "../Trivia-Clone";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const Clone = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default Clone;
