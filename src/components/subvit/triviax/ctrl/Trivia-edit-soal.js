import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../Trivia-Edit-Soal";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const TriviaEditSoal = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default TriviaEditSoal;
