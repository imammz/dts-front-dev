import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import ReviewSoal from "../Review-Soal";
import TambahSoalForm from "../../survey/form/Survey-Clone/Tambah-Soal/TambahSoalForm";

export default class TriviaTambahListSoal extends React.Component {
  constructor(props) {
    super(props);
    this.swapNomor = this.swapNomorAction.bind(this);
    this.handleClickDelete = this.handleClickDeleteAction.bind(this);
    this.handleClickEdit = this.handleClickEditAction.bind(this);
    this.handleClickReviewSoal = this.handleClickReviewSoalAction.bind(this);
    this.nextSoal = this.nextSoalAction.bind(this);
    this.prevSoal = this.prevSoalAction.bind(this);
    this.handleClickPreviewSingle =
      this.handleClickPreviewSingleAction.bind(this);

    this.state = {
      datax: [],
      review_soal: false,
      all_soal: [],
      no_soal: 1,
      show_next_prev: true,
    };
  }

  columns = [
    {
      name: "No",
      width: "70px",
      center: true,
      cell: (row, index) => this.state.tempLastNumber + index + 1,
    },
    {
      name: "Soal",
      sortable: true,
      selector: (row) => (
        <>
          <label className="d-flex flex-stack mb- mt-1">
            <span className="d-flex align-items-center me-2">
              <span className="d-flex flex-column">
                <h6 className="fw-bolder fs-7 mb-0">{row.pertanyaan}</h6>
              </span>
            </span>
          </label>
        </>
      ),
    },
    {
      name: "Aksi",
      center: true,
      cell: (row, index) => {
        return (
          <div>
            {/* <a
                        href="#"
                        onClick={this.swapNomor}
                        nosoal={index + 1}
                        targetnumber='up'
                        index={index}
                        title="Naikkan Urutan"
                        className={index == 0 ? "disabled btn btn-icon btn-active-light-primary w-30px h-30px me-3" : "btn btn-icon btn-active-light-danger w-30px h-30px me-3"} >
                        <span className="svg-icon svg-icon-2x">
                            <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24" />
                                    <rect fill="#000000" opacity="0.3" x="11" y="10" width="2" height="10" rx="1" />
                                    <path d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z" fill="#000000" fillRule="nonzero" />
                                </g>
                            </svg>
                        </span>
                    </a>
                    <a
                        href="#"
                        onClick={this.swapNomor}
                        nosoal={index + 1}
                        targetnumber='down'
                        index={index}
                        title="Turunkan Urutan"
                        className={index == (this.state.datax.length - 1) ? "disabled btn btn-icon btn-active-light-primary w-30px h-30px me-3" : "btn btn-icon btn-active-light-danger w-30px h-30px me-3"}>
                        <span className="svg-icon svg-icon-2x">
                            <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24" />
                                    <rect fill="#000000" opacity="0.3" x="11" y="4" width="2" height="10" rx="1" />
                                    <path d="M6.70710678,19.7071068 C6.31658249,20.0976311 5.68341751,20.0976311 5.29289322,19.7071068 C4.90236893,19.3165825 4.90236893,18.6834175 5.29289322,18.2928932 L11.2928932,12.2928932 C11.6714722,11.9143143 12.2810586,11.9010687 12.6757246,12.2628459 L18.6757246,17.7628459 C19.0828436,18.1360383 19.1103465,18.7686056 18.7371541,19.1757246 C18.3639617,19.5828436 17.7313944,19.6103465 17.3242754,19.2371541 L12.0300757,14.3841378 L6.70710678,19.7071068 Z" fill="#000000" fillRule="nonzero" transform="translate(12.000003, 15.999999) scale(1, -1) translate(-12.000003, -15.999999) " />
                                </g>
                            </svg>
                        </span>
                    </a> */}
            {/* Preview Single Soal */}
            <a
              href="#"
              nosoal={index + 1}
              onClick={this.handleClickPreviewSingle}
              data-bs-toggle="modal"
              data-bs-target="#review_soal"
              title="Preview Soal"
              className={"btn btn-icon btn-bg-primary btn-sm me-1"}
            >
              <i className="fa fa-eye text-white"></i>
            </a>
            {/* Edit Trivia */}
            <a
              href="#"
              nosoal={index + 1}
              onClick={this.handleClickEdit}
              title="Edit"
              className={"btn btn-icon btn-bg-warning btn-sm me-1"}
            >
              <i className="bi bi-gear-fill text-white"></i>
            </a>
            <a
              href="#"
              nosoal={index + 1}
              onClick={this.handleClickDelete}
              title="Hapus"
              className={"btn btn-icon btn-bg-danger btn-sm me-1"}
            >
              <i className="bi bi-trash-fill text-white"></i>
            </a>
          </div>
        );
      },
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  componentDidMount() {
    this.handleReload();
  }
  componentDidUpdate(prevProps) {
    if (prevProps.is_add_soal !== this.props.is_add_soal) {
      this.handleReload();
    }
  }
  handleReload(page, newPerPage) {
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });
    const datax = [];
    let totalRows = 0;
    for (let i = 0; i < localStorage.length + 1; i++) {
      const soal = JSON.parse(localStorage.getItem(i));
      if (soal) {
        datax.push(soal);
        totalRows++;
      }
    }

    this.setState({
      datax: datax,
      totalRows: totalRows,
      loading: false,
    });
  }

  handlePageChange = (page) => {
    this.setState({ loading: true });
    this.handleReload(page, this.state.newPerPage);
  };
  handlePerRowsChange = async (newPerPage, page) => {
    this.setState({ loading: true });
    this.setState({ newPerPage: newPerPage });
    this.handleReload(page, newPerPage);
  };

  swapNomorAction(e) {
    const nosoal = e.currentTarget.getAttribute("nosoal");
    const targetNumber = e.currentTarget.getAttribute("targetnumber");
    let notujuan = 0;

    switch (targetNumber) {
      case "up":
        notujuan = parseInt(nosoal) - 1;
        break;
      case "down":
        notujuan = parseInt(nosoal) + 1;
        break;
    }
    const asal = JSON.parse(localStorage.getItem(nosoal));
    asal.nosoal = parseInt(notujuan);
    const tujuan = JSON.parse(localStorage.getItem(notujuan));
    tujuan.nosoal = parseInt(nosoal);
    localStorage.setItem(notujuan, JSON.stringify(asal));
    localStorage.setItem(nosoal, JSON.stringify(tujuan));
    this.handleReload();
  }

  handleClickEditAction(e) {
    const nosoal = e.currentTarget.getAttribute("nosoal");
    const callBackVal = {
      is_edit_soal: true,
      no_soal_edit: nosoal,
    };
    this.props.parentEditCallBack(callBackVal);
  }

  handleClickDeleteAction(e) {
    if (this.state.datax.length == 1) {
      swal
        .fire({
          title: "Soal Harus Tersedia Minimal 1",
          icon: "warning",
          confirmButtonText: "Ok",
          didOpen: () => {
            swal.hideLoading();
          },
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    } else {
      const idx = e.currentTarget.getAttribute("nosoal");

      swal
        .fire({
          title: "Apakah anda yakin ?",
          text: "Data ini tidak bisa dikembalikan!",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Ya, hapus!",
          cancelButtonText: "Tidak",
        })
        .then((result) => {
          if (result.isConfirmed) {
            localStorage.removeItem(idx + "");
            //renumber
            let last_number = 0;
            for (let i = parseInt(idx); i < localStorage.length + 1; i++) {
              let nextLocalStorage = localStorage.getItem(i + 1);
              if (nextLocalStorage) {
                localStorage.setItem(i, nextLocalStorage);
                localStorage.removeItem(i + 1 + "");
                last_number = i + 1;
              }
            }
            const callBackVal = {
              last_number: last_number,
            };
            this.props.parentCallBack(callBackVal);
          }
        });
    }
  }

  handleClickReviewSoalAction(e) {
    e.preventDefault();
    //dibuat false dulu biar reset modal
    this.setState({ review_soal: false });
    const id = e.currentTarget.getAttribute("id");
    this.setState(
      {
        show_next_prev: true,
        no_soal: 1,
      },
      () => this.loadSoalAction(),
    );
  }

  handleClickPreviewSingleAction(e) {
    e.preventDefault();
    this.setState({ review_soal: false });
    const nosoal = e.currentTarget.getAttribute("nosoal");
    this.setState(
      {
        show_next_prev: false,
        no_soal: nosoal,
      },
      () => this.loadSoalAction(),
    );
  }

  loadSoalAction() {
    const datax = [];
    for (let i = 0; i < localStorage.length + 1; i++) {
      const soal = JSON.parse(localStorage.getItem(i));
      if (soal) {
        datax.push(soal);
      }
    }

    if (datax.length != 0) {
      datax[this.state.no_soal - 1].type = datax[this.state.no_soal - 1].tipe;
      //ambil gambar pertanyaan
      const key_gambar_pertanyaan =
        datax[this.state.no_soal - 1].key_gambar_pertanyaan;
      let imagePertanyaanWrapper = document.getElementById(
        key_gambar_pertanyaan,
      );
      let gambar_pertanyaan = null;
      if (imagePertanyaanWrapper) {
        imagePertanyaanWrapper = imagePertanyaanWrapper.value;
        const splitImagePertanyaan = imagePertanyaanWrapper.split("_");
        gambar_pertanyaan = splitImagePertanyaan[1];
      }
      delete datax[this.state.no_soal - 1].key_gambar_pertanyaan;
      datax[this.state.no_soal - 1].pertanyaan_gambar = gambar_pertanyaan;

      //ambil gambar jawaban
      if (datax[this.state.no_soal - 1].jawaban) {
        datax[this.state.no_soal - 1].jawaban.forEach(function (element, i) {
          const key_image = element.key_image;
          let imageJawabanWrapper = document.getElementById(key_image);
          let image = null;
          if (imageJawabanWrapper) {
            imageJawabanWrapper = imageJawabanWrapper.value;
            const splitImageJawaban = imageJawabanWrapper.split("_");
            image = splitImageJawaban[1];
          }
          delete element.key_image;
          element.image = image;
        });
        //jawaban di stringify karena di Review Soal sudah ada parse
        datax[this.state.no_soal - 1].jawaban = JSON.stringify(
          datax[this.state.no_soal - 1].jawaban,
        );
      }

      const current_soal = (
        <ReviewSoal
          soal={datax[this.state.no_soal - 1]}
          no={this.state.no_soal}
        />
      );
      this.setState({
        review_soal: current_soal,
        all_soal: datax,
      });
    } else {
      swal
        .fire({
          title: "Belum Ada Soal",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  nextSoalAction(e) {
    e.preventDefault();
    this.setState(
      {
        review_soal: false,
        show_next_prev: true,
        no_soal: this.state.no_soal + 1,
      },
      () => this.loadSoalAction(),
    );
  }

  prevSoalAction(e) {
    e.preventDefault();
    this.setState(
      {
        review_soal: false,
        show_next_prev: true,
        no_soal: this.state.no_soal - 1,
      },
      () => this.loadSoalAction(),
    );
  }

  render() {
    return (
      <div>
        <div className="col-lg-12 mb-7 fv-row">
          <div className="card-header m-0 p-0">
            <div className="card-title">
              <h5 className="mt-7 mb-5 me-3">Pengaturan Soal</h5>
            </div>
            <div className="card-toolbar">
              <a
                href="#"
                className="btn btn-light btn-sm"
                data-bs-toggle="modal"
                data-bs-target="#review_soal"
                onClick={this.handleClickReviewSoal}
              >
                <i className="bi bi-eye me-1"></i>
                Preview Trivia
              </a>
            </div>
          </div>
          <div className="table-responsive">
            <DataTable
              columns={this.columns}
              data={this.state.datax}
              progressPending={this.state.loading}
              highlightOnHover
              pointerOnHover
              pagination
              paginationServer
              paginationTotalRows={this.state.totalRows}
              onChangeRowsPerPage={this.handlePerRowsChange}
              onChangePage={this.handlePageChange}
              customStyles={this.customStyles}
              persistTableHead={true}
              noDataComponent={<div className="mt-5">Tidak Ada Data</div>}
            />
          </div>
        </div>
        <div className="modal fade" tabIndex="-1" id="review_soal">
          <div className="modal-dialog modal-lg">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Preview Trivia</h5>
                <div
                  className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <span className="svg-icon svg-icon-2x">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <rect
                        opacity="0.5"
                        x="6"
                        y="17.3137"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(-45 6 17.3137)"
                        fill="currentColor"
                      />
                      <rect
                        x="7.41422"
                        y="6"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(45 7.41422 6)"
                        fill="currentColor"
                      />
                    </svg>
                  </span>
                </div>
              </div>
              <div className="modal-body">
                {this.state.review_soal}
                {this.state.show_next_prev ? (
                  <div className="text-end mt-5">
                    {this.state.no_soal == 1 ? (
                      <button
                        onClick={this.prevSoal}
                        className="btn btn-light btn-sm me-3 mr-2 disabled"
                      >
                        <i className="fa fa-chevron-left me-1"></i>Sebelumnya
                      </button>
                    ) : (
                      <button
                        onClick={this.prevSoal}
                        className="btn btn-light btn-sm me-3 mr-2"
                      >
                        <i className="fa fa-chevron-left me-1"></i>Sebelumnya
                      </button>
                    )}
                    {this.state.no_soal == this.state.all_soal.length ? (
                      <button
                        onClick={this.nextSoal}
                        className="disabled btn btn-primary btn-sm"
                      >
                        Selanjutnya<i className="fa fa-chevron-right ms-1"></i>
                      </button>
                    ) : (
                      <button
                        onClick={this.nextSoal}
                        className="btn btn-primary btn-sm"
                      >
                        Selanjutnya<i className="fa fa-chevron-right ms-1"></i>
                      </button>
                    )}
                  </div>
                ) : (
                  ""
                )}
              </div>
              <div className="modal-footer">
                <a
                  href="#"
                  className="btn btn-light btn-sm"
                  data-bs-dismiss="modal"
                >
                  Close
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
