import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";

export default class TriviaCloneList extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickEditSoal = this.handleClickEditSoalAction.bind(this);
    this.handleTambah = this.handleTambahAction.bind(this);
    this.handleImport = this.handleImportAction.bind(this);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleChangeSearch = this.handleChangeSearchAction.bind(this);
    this.onItemCheck = this.onItemCheckAction.bind(this);
    this.rowSelect = this.rowSelectCriteria.bind(this);
    this.state = {
      datax: [],
      loading: false,
      tempLastNumber: 0,
      newPerPage: 200,
      totalRows: 0,
      currentPage: 0,
      isSearch: false,
      akademi_id: this.props.akademi_id,
      theme_id: this.props.tema_id,
      pelatihan_id: this.props.pelatihan_id,
      param: "",
    };
  }

  rowSelectCriteria(row) {
    /* if (row.pidtrivia_detail > 0) {
            return true;
        } */
    return true;
  }

  columns = [
    {
      name: "No",
      width: "70px",
      center: true,
      cell: (row, index) => this.state.tempLastNumber + index + 1,
    },
    {
      name: "Soal",
      className: "min-w-300px mw-300px",
      grow: 6,
      sortable: true,
      sortField: "question",
      wrap: true,
      allowOverflow: false,
      //width: '600px',
      selector: (row) => (
        <>
          <label className="d-flex flex-stack mb- mt-1">
            <span className="d-flex align-items-center me-2">
              <span className="d-flex flex-column">
                <h6 className="fw-bolder fs-7 mb-0">{row.pertanyaan}</h6>
              </span>
            </span>
          </label>
        </>
      ),
    },
    {
      name: "Aksi",
      width: "100px",
      center: true,
      cell: (row, index) => {
        return (
          <div>
            <a
              href="#"
              id_soal={row.pidtrivia_detail}
              id_trivia={row.pid_trivia}
              nomor_soal={this.state.tempLastNumber + index + 1}
              title="Edit"
              onClick={this.handleClickEditSoal}
              className="btn btn-icon btn-warning btn-sm me-1"
            >
              <i className="bi bi-gear-fill text-white"></i>
            </a>
          </div>
        );
      },
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    let elements = document.getElementsByName("select-all-rows");
    elements[0].style.display = "none";
    this.setState(
      {
        datax: [],
        id_trivia: this.props.id_trivia,
      },
      () => {
        this.handleReload();
      },
    );
  }

  componentDidUpdate(prevProps) {
    if (prevProps.is_edit_soal !== this.props.is_edit_soal) {
      this.setState(
        {
          datax: [],
          id_trivia: this.props.id_trivia,
        },
        () => {
          this.handleReload();
        },
      );
    }
  }

  handleReload(page, newPerPage) {
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 200;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });
    const dataBody = {
      id_trivia: this.state.id_trivia,
      start: start_tmp,
      rows: length_tmp,
      param: this.state.param,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list_soal_trivia_byid",
        dataBody,
        this.configs,
      )
      .then((res) => {
        this.setState({ loading: false });
        const datax = res.data.result.Data;
        datax.forEach(function (element, i) {
          let edit = JSON.parse(
            localStorage.getItem("_" + element.pidtrivia_detail),
          );

          if (edit) {
            datax[i] = edit;
            localStorage.setItem(
              datax[i].pidtrivia_detail,
              JSON.stringify(edit),
            );
            //localStorage.removeItem("_" + element.pidtrivia_detail);
          } else {
            if (element.jawaban != "undefined") {
              element.jawaban = JSON.parse(element.jawaban);
            } else {
              element.jawaban = "";
            }
            localStorage.setItem(
              element.pidtrivia_detail,
              JSON.stringify(element),
            );
          }
        });
        let totalRows = res.data.result.JumlahData;
        let newPerPage = this.state.newPerPage;
        const itemTambah = JSON.parse(localStorage.getItem("TAMBAH"));
        if (itemTambah) {
          itemTambah.forEach(function (element, i) {
            const item = JSON.parse(localStorage.getItem(element));
            if (item) {
              datax.push(item);
              if (datax.length > newPerPage) {
                //datax.pop();
              }
              totalRows++;
            }
          });
        }

        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;

        if (statusx) {
          this.setState({ datax });
          this.setState({ loading: false });
          this.setState({ totalRows: totalRows });
          this.setState({ currentPage: page });
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.handleReload();
              }
            });
        }
      })
      .catch((error) => {
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      });
  }

  handlePageChange = (page) => {
    this.setState({ loading: true });
    this.handleReload(page, this.state.newPerPage);
  };
  handlePerRowsChange = async (newPerPage, page) => {
    this.setState({ loading: true });
    this.setState({ newPerPage: newPerPage });
    this.handleReload(page, newPerPage);
  };

  handleClickEditSoalAction(e) {
    const id_soal = e.currentTarget.getAttribute("id_soal");
    const id_trivia = e.currentTarget.getAttribute("id_trivia");
    const nomor_soal = e.currentTarget.getAttribute("nomor_soal");
    const callBackVal = {
      is_edit_soal: true,
      is_tambah_soal: false,
      is_import_soal: false,
      id_soal: id_soal,
      id_trivia: id_trivia,
      nomor_soal: nomor_soal,
    };
    this.props.parentCallBack(callBackVal);
  }

  handleTambahAction(e) {
    const callBackVal = {
      is_tambah_soal: true,
      is_edit_soal: false,
      is_import_soal: false,
      nomor_soal: this.state.totalRows + 1,
    };
    this.props.parentCallBack(callBackVal);
  }

  handleImportAction(e) {
    const callBackVal = {
      is_tambah_soal: false,
      is_edit_soal: false,
      is_import_soal: true,
      nomor_soal: this.state.totalRows + 1,
    };
    this.props.parentCallBack(callBackVal);
  }

  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    //this.setState({param:searchText});
    if (e.key == "Enter") {
      e.preventDefault();
      this.setState({ loading: true });
      this.setState({ param: searchText }, () => {
        this.handleReload();
      });
    }
  }

  handleChangeSearchAction(e) {
    const searchText = e.currentTarget.value;
    if (searchText == "") {
      this.setState(
        {
          loading: true,
          param: "",
        },
        () => {
          this.handleReload();
        },
      );
    }
  }

  onItemCheckAction(sel) {
    const selectedRows = sel.selectedRows;
    const datax = this.state.datax;
    const keyStorage = "uncheck";
    datax.forEach(function (element, i) {
      const d_id = element.pidtrivia_detail;
      let found = false;
      selectedRows.forEach(function (selected, j) {
        const s_id = selected.pidtrivia_detail;
        if (d_id == s_id) {
          found = true;
        }
      });
      if (!found) {
        let old = JSON.parse(localStorage.getItem(keyStorage));
        if (old !== null) {
          if (!old.includes(d_id)) {
            old.push(d_id);
          }
          localStorage.setItem(keyStorage, JSON.stringify(old));
        } else {
          localStorage.setItem(keyStorage, JSON.stringify([d_id]));
        }
      }
    });
    //crosscheck
    selectedRows.forEach(function (selected, j) {
      const s_id = selected.pidtrivia_detail;
      let old = JSON.parse(localStorage.getItem(keyStorage));
      if (old !== null) {
        old.forEach(function (ol, i) {
          if (s_id == ol) {
            const filteredArray = old.filter(function (e) {
              return e !== ol;
            });
            localStorage.setItem(keyStorage, JSON.stringify(filteredArray));
          }
        });
      }
    });
  }

  render() {
    return (
      <div>
        <div className="col-lg-12 mb-7 fv-row">
          <div className="card-header m-0 p-0">
            <div className="card-title">
              <h5 className="mt-7 mb-5 me-3">Pengaturan Soal</h5>
            </div>
            <div className="card-toolbar">
              <div className="d-flex align-items-center position-relative my-1 me-2">
                <div className="dropdown d-inline">
                  <button
                    className="btn btn-light fw-bolder btn-sm dropdown-toggle fs-7 me-2"
                    type="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                      Kelola Soal
                    </span>
                  </button>
                  <ul
                    className="dropdown-menu"
                    style={{ position: "absolute", zIndex: "999999" }}
                  >
                    <li>
                      <a
                        href="#"
                        onClick={this.handleTambah}
                        className="dropdown-item px-5 my-1"
                      >
                        <i className="bi bi-plus-circle text-dark  me-1"></i>
                        Tambah Soal
                      </a>
                    </li>
                    <li>
                      <a
                        href="#"
                        onClick={this.handleImport}
                        className="dropdown-item px-5"
                      >
                        <i className="bi bi-cloud-download text-dark  me-1"></i>
                        Import Soal
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div className="table-responsive">
            <DataTable
              columns={this.columns}
              data={this.state.datax}
              progressPending={this.state.loading}
              highlightOnHover
              pointerOnHover
              pagination={false}
              paginationServer
              paginationTotalRows={this.state.totalRows}
              onChangeRowsPerPage={this.handlePerRowsChange}
              onChangePage={this.handlePageChange}
              customStyles={this.customStyles}
              persistTableHead={true}
              selectableRows
              onSelectedRowsChange={this.onItemCheck}
              selectableRowSelected={this.rowSelect}
              noDataComponent={<div className="mt-5">Tidak Ada Data</div>}
            />
          </div>
        </div>
      </div>
    );
  }
}
