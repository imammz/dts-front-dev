import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import Select from "react-select";
import {
  capitalizeFirstLetter,
  statusPelaksanaan,
} from "../../publikasi/helper";
//import ReviewSoal from './Review-Soal';
import ReviewSoal from "../triviax/Review-Soal";
import { fixBootstrapDropdown } from "../../../utils/commonutil";
import {
  isAdminAkademi,
  isExceptionRole,
  isSuperAdmin,
} from "../../AksesHelper";

export default class TriviaEditContent extends React.Component {
  constructor(props) {
    super(props);
    this.handlePerRowsChange = this.handlePerRowsChange.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleClickDelete = this.handleClickDeleteAction.bind(this);
    this.handleClickReviewSoal = this.handleClickReviewSoalAction.bind(this);
    this.handleChangeStatus = this.handleChangeStatusAction.bind(this);
    this.handleClickFilter = this.handleClickFilterAction.bind(this);
    this.handleClickReset = this.handleClickResetAction.bind(this);
    this.handleSort = this.handleSortAction.bind(this);
    this.prevSoal = this.prevSoalAction.bind(this);
    this.nextSoal = this.nextSoalAction.bind(this);
    this.handleChangeSearch = this.handleChangeSearchAction.bind(this);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleChangeLevel = this.handleChangeLevelAction.bind(this);
  }

  state = {
    datax: [],
    valStatus: [],
    status: 99,
    tempLastNumber: "",
    loading: false,
    totalRows: "",
    isSearch: false,
    newPerPage: 10,
    soalx: [],
    is_filter: false,
    review_soal: false,
    all_soal: [],
    id_trivia: null,
    no_soal: 1,
    column: "id_trivia",
    sortDirection: "DESC",
    show_next_prev: true,
    param: 0,
    level: 99,
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  optionstatus = [
    { value: 99, label: "Semua" },
    { value: 1, label: "Publish" },
    { value: 0, label: "Draft" },
  ];

  level_trivia = [
    { value: 99, label: "Semua" },
    { value: 0, label: "Akademi" },
    { value: 1, label: "Tema" },
    { value: 2, label: "Pelatihan" },
    { value: 3, label: "Seluruh Populasi DTS" },
  ];

  columns = [
    {
      name: "No",
      center: true,
      cell: (row, index) => this.state.tempLastNumber + index + 1,
      width: "70px",
    },
    {
      name: "Nama Trivia",
      className: "min-w-300px mw-300px",
      width: "300px",
      sortable: true,
      grow: 6,
      wrap: true,
      allowOverflow: false,
      selector: (row) => (
        <>
          <label className="d-flex flex-stack mt-1 my-2">
            <span className="d-flex align-items-center me-2">
              <span className="d-flex flex-column">
                <h6 className="fw-bolder fs-7 mb-0">
                  <a
                    href={"/subvit/trivia/list-soal/" + row.id_trivia}
                    className="text-dark"
                  >
                    {capitalizeFirstLetter(row.nama_trivia)}
                  </a>
                </h6>
              </span>
            </span>
          </label>
        </>
      ),
    },
    {
      sortable: true,
      name: "Targeting",
      width: "300px",
      grow: 6,
      wrap: true,
      allowOverflow: false,
      selector: (row) => {
        let level = "Belum Ada Level";
        switch (row.level) {
          case "0":
            level = "Akademi";
            break;
          case "1":
            level = "Tema";
            break;
          case "2":
            level = "Pelatihan";
            break;
          case "3":
            if (row.jenis_survey == 1) {
              level = "Seluruh Populasi DTS";
            } else {
              level = "Akademi";
            }
            break;
        }
        return (
          <div>
            <span>{level}</span>
            <br />
            <span className="text-muted fs-7 fw-semibold">
              {row.nama_level}
            </span>
          </div>
        );
      },
    },
    {
      name: "Jadwal",
      sortable: true,
      className: "min-w-220px mw-220px",
      grow: 5,
      width: "250px",
      selector: (row) => {
        const status = statusPelaksanaan(row.tgl_mulai, row.tgl_selesai);
        const color = ["secondary", "primary", "success"];
        const status_text = [
          "Belum Dilaksanakan",
          "Sedang Berlangsung",
          "Selesai",
        ];
        return (
          <div>
            {row.tgl_mulai} - {row.tgl_selesai}
            <br />
            <span className={"badge badge-" + color[status] + " mt-1"}>
              {status_text[status]}
            </span>
          </div>
        );
      },
    },
    {
      name: "Bank Soal",
      sortable: true,
      grow: 4,
      selector: (row) => {
        return row.jml_soal + " Soal";
      },
    },
    {
      name: "Status",
      sortable: true,
      className: "min-w-220px mw-220px",
      //width: "150px",
      grow: 3,
      selector: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.status_trivia == "1" ? "success" : "danger") +
              " fs-7 m-1"
            }
          >
            {row.status_trivia == "1" ? "Publish" : "Draft"}
          </span>
        </div>
      ),
    },
    {
      name: "Aksi",
      center: false,
      width: "200px",
      cell: (row) => (
        <div>
          <div className="dropdown d-inline">
            <button
              className="btn btn-primary fw-bolder btn-sm dropdown-toggle fs-7 me-2"
              type="button"
              data-bs-toggle="dropdown"
              aria-expanded="false"
            >
              <i className="bi bi-eye-fill text-white"></i>
              <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                Detail
              </span>
            </button>

            <ul
              className="dropdown-menu"
              style={{ position: "absolute", zIndex: "999999" }}
            >
              <li>
                <a
                  href="#"
                  id={row.id_trivia}
                  data-bs-toggle="modal"
                  data-bs-target="#review_soal"
                  onClick={this.handleClickReviewSoal}
                  className="dropdown-item px-5 my-1"
                >
                  <i className="bi bi-eye text-dark text-hover-primary me-1"></i>
                  Preview
                </a>
              </li>

              {row.unit_work_id == Cookies.get("satker_id") ||
              isExceptionRole() ? (
                <>
                  <li>
                    <a
                      href={"/subvit/trivia/list-soal/" + row.id_trivia}
                      title="Detail"
                      className="dropdown-item px-5 my-1"
                    >
                      <i className="bi bi-list-ul text-dark text-hover-primary me-1"></i>
                      Detail
                    </a>
                  </li>
                  <li>
                    <a
                      href={"/subvit/trivia/edit/" + row.id_trivia}
                      title="Edit"
                      className="dropdown-item px-5 my-1"
                    >
                      <i className="bi bi-gear text-dark text-hover-primary me-1"></i>
                      Edit
                    </a>
                  </li>
                </>
              ) : (
                <>
                  <li>
                    <span
                      title="Detail"
                      className="dropdown-item px-5 my-1 text-muted"
                    >
                      <i className="bi bi-list-ul text-muted  me-1"></i>Detail
                    </span>
                  </li>
                  <li>
                    <span
                      title="Edit"
                      className="dropdown-item px-5 my-1 text-muted"
                    >
                      <i className="bi bi-gear text-muted  me-1"></i>Edit
                    </span>
                  </li>
                </>
              )}

              <li>
                {" "}
                <a
                  href={"/subvit/trivia/report/" + row.id_trivia}
                  className="dropdown-item px-5"
                >
                  <i className="bi bi-bar-chart text-dark text-hover-primary me-1"></i>
                  Report
                </a>
              </li>

              <li>
                {" "}
                <a
                  href={"/subvit/trivia/clone/" + row.id_trivia}
                  className="dropdown-item px-5 my-1"
                >
                  <i className="bi bi-files text-dark text-hover-primary me-1"></i>
                  Clone
                </a>
              </li>
            </ul>
          </div>

          {(row.unit_work_id == Cookies.get("satker_id") ||
            isExceptionRole()) &&
          row.status_trivia != 1 ? (
            <>
              <a
                href="#"
                id={row.id_trivia}
                onClick={this.handleClickDelete}
                title="Hapus"
                className="btn btn-icon btn-bg-danger btn-sm me-1"
              >
                <i className="bi bi-trash-fill text-white"></i>
              </a>
            </>
          ) : (
            <>
              <a
                href="#"
                id={row.id_trivia}
                title="Hapus"
                className="disabled btn btn-icon btn-bg-danger btn-sm me-1"
              >
                <i className="bi bi-trash-fill text-white"></i>
              </a>
            </>
          )}
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };
  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    this.handleReload();
  }
  componentDidUpdate() {
    fixBootstrapDropdown();
  }
  handleChangeSearchAction(e) {
    const searchText = e.currentTarget.value;
    if (searchText == "") {
      this.setState(
        {
          loading: true,
          param: 0,
        },
        () => {
          this.handleReload();
        },
      );
    }
  }
  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    if (e.key == "Enter") {
      if (searchText == "") {
        this.setState(
          {
            isSearch: false,
            param: 0,
          },
          () => {
            this.handleReload();
          },
        );
      } else {
        this.setState({ loading: true });
        this.setState({ isSearch: true });
        this.setState({ param: searchText }, () => {
          this.handleReload();
        });
      }
    }
  }

  handleReload(page, newPerPage) {
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });

    let dataBody = {
      /* start: start_tmp,
            rows: length_tmp,
            param: this.state.param,
            status: this.state.status,
            level: this.state.level,
            sort: this.state.column,
            sort_val: this.state.sortDirection.toUpperCase() */
      mulai: start_tmp,
      limit: length_tmp,
      cari: this.state.param,
      status: this.state.status,
      sort: this.state.column,
      sort_val: this.state.sortDirection.toUpperCase(),
      level: 99,
      status_pelaksanaan: 0,
      akademi_id: 0,
      tema_id: 0,
      pelatihan_id: 0,
      user_by: Cookies.get("user_id"),
    };

    if (this.state.is_filter) {
      dataBody = {
        mulai: start_tmp,
        limit: this.state.newPerPage,
        cari: this.state.param,
        status: this.state.status,
        level: this.state.level,
        sort: this.state.column,
        sort_val: this.state.sortDirection.toUpperCase(),
        status_pelaksanaan: 0,
        akademi_id: 0,
        tema_id: 0,
        pelatihan_id: 0,
        user_by: Cookies.get("user_id"),
      };
    }

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/trivia/list_trivia",
        dataBody,
        this.configs,
      )
      .then((res) => {
        this.setState({ loading: false });
        const datax = res.data.result.Data;
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statusx) {
          this.setState({ datax });
          this.setState({ totalRows: res.data.result.JumlahData[0].jml_data });
          this.setState({ currentPage: page });
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      })
      .catch((error) => {
        console.log(error);
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      });
  }
  handlePageChange = (page) => {
    this.setState({ loading: true });
    this.handleReload(page, this.state.newPerPage);
  };
  handlePerRowsChange = async (newPerPage, page) => {
    this.setState({ loading: true });
    this.setState({ newPerPage: newPerPage });
    this.handleReload(page, newPerPage);
  };

  handleClickDeleteAction(e) {
    const idx = e.currentTarget.id;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          const data = { id: idx };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/trivia_delete",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload(
                        this.state.currentPage,
                        this.state.newPerPage,
                      );
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmationBUttonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload();
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload();
                    }
                  });
              }
            });
        }
      });
  }

  handleClickReviewSoalAction(e) {
    //dibuat false dulu biar reset modal
    this.setState({ review_soal: false });
    const id = e.currentTarget.getAttribute("id");
    this.setState(
      {
        id_trivia: id,
        show_next_prev: true,
        no_soal: 1,
      },
      () => this.loadSoalAction(),
    );
  }

  nextSoalAction() {
    this.setState(
      {
        review_soal: false,
        show_next_prev: true,
        no_soal: this.state.no_soal + 1,
      },
      () => this.loadSoalAction(),
    );
  }

  prevSoalAction() {
    this.setState(
      {
        review_soal: false,
        show_next_prev: true,
        no_soal: this.state.no_soal - 1,
      },
      () => this.loadSoalAction(),
    );
  }

  loadSoalAction() {
    const data = {
      id_trivia: this.state.id_trivia,
      start: 0,
      rows: 100,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list_soal_trivia_byid",
        data,
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          const datax = res.data.result.Data;
          const soal = (
            <ReviewSoal
              soal={datax[this.state.no_soal - 1]}
              no={this.state.no_soal}
            />
          );
          this.setState({
            review_soal: soal,
            all_soal: datax,
          });
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmationBUttonText: "Ok",
            })
            .then((result) => {
              this.setState({
                show_next_prev: false,
                review_soal: (
                  <div className="text-center">Tidak Ada Data Soal</div>
                ),
              });
              if (result.isConfirmed) {
              }
            });
        }
      })
      .catch((error) => {
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          this.setState({
            show_next_prev: false,
            review_soal: <div className="text-center">Tidak Ada Data Soal</div>,
          });
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  handleChangeLevelAction = (level) => {
    this.setState({ valLevel: { label: level.label, value: level.value } });
    this.setState({ level: level.value });
  };

  handleChangeStatusAction = (status) => {
    this.setState({ valStatus: { label: status.label, value: status.value } });
    this.setState({ status: status.value });
  };

  handleClickFilterAction(e) {
    e.preventDefault();

    this.setState({ is_filter: true }, () => {
      this.handleReload(1, this.state.newPerPage);
    });
  }

  handleClickResetAction(e) {
    this.setState(
      {
        valStatus: [],
        valLevel: [],
        level: 99,
        status: 99,
        is_filter: false,
      },
      () => this.handleReload(),
    );
  }

  handleSortAction(column, sortDirection) {
    let server_name = "";
    if (column.name == "Nama Trivia") {
      server_name = "nama_trivia";
    } else if (column.name == "Pelaksanaan") {
      server_name = "start_at";
    } else if (column.name == "Jumlah Soal") {
      server_name = "jml_soal";
    } else if (column.name == "Status") {
      server_name = "status_trivia";
    } else if (column.name == "Level") {
      server_name = "level";
    } else if (column.name == "Target") {
      server_name = "jml_pelatihan";
    }

    this.setState(
      {
        column: server_name,
        sortDirection: sortDirection,
      },
      () => {
        this.handleReload(1, this.state.newPerPage);
      },
    );
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <path
                        opacity="0.3"
                        d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                        fill="#7239ea"
                      ></path>
                      <path
                        d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                        fill="#7239ea"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Subvit
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Trivia
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                  data-bs-toggle="modal"
                  data-bs-target="#filter"
                >
                  <i className="bi bi-sliders"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Filter
                  </span>
                </button>

                <a
                  href="/subvit/trivia/tambah"
                  className="btn btn-success btn-sm me-2"
                >
                  <i className="bi bi-plus-circle"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Tambah Trivia
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Daftar Trivia
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <div className="ml-5 d-flex align-items-center position-relative my-1 me-2">
                            <span className="svg-icon svg-icon-1 position-absolute ms-6">
                              <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className="mh-50px"
                              >
                                <rect
                                  opacity="0.5"
                                  x="17.0365"
                                  y="15.1223"
                                  width="8.15546"
                                  height="2"
                                  rx="1"
                                  transform="rotate(45 17.0365 15.1223)"
                                  fill="currentColor"
                                ></rect>
                                <path
                                  d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                  fill="currentColor"
                                ></path>
                              </svg>
                            </span>
                            <input
                              type="text"
                              data-kt-user-table-filter="search"
                              className="form-control form-control-sm form-control-solid w-250px ps-14"
                              placeholder="Cari Trivia"
                              onKeyPress={this.handleKeyPress}
                              onChange={this.handleChangeSearch}
                            />
                          </div>
                          <div className="modal fade" tabIndex="-1" id="filter">
                            <div className="modal-dialog modal-lg">
                              <div className="modal-content">
                                <div className="modal-header">
                                  <h5 className="modal-title">
                                    <span className="svg-icon svg-icon-5 me-1">
                                      <i className="bi bi-sliders text-black"></i>
                                    </span>
                                    Filter Data Trivia
                                  </h5>
                                  <div
                                    className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                    data-bs-dismiss="modal"
                                    aria-label="Close"
                                  >
                                    <span className="svg-icon svg-icon-2x">
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                      >
                                        <rect
                                          opacity="0.5"
                                          x="6"
                                          y="17.3137"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(-45 6 17.3137)"
                                          fill="currentColor"
                                        />
                                        <rect
                                          x="7.41422"
                                          y="6"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(45 7.41422 6)"
                                          fill="currentColor"
                                        />
                                      </svg>
                                    </span>
                                  </div>
                                </div>
                                <form
                                  action="#"
                                  onSubmit={this.handleClickFilter}
                                >
                                  <div className="modal-body">
                                    <div className="row">
                                      <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label">
                                          Level
                                        </label>
                                        <Select
                                          id="status"
                                          name="status"
                                          value={this.state.valLevel}
                                          placeholder="Silahkan pilih Level"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.state.dataxtema
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm selectpicker p-0"
                                          options={this.level_trivia}
                                          onChange={this.handleChangeLevel}
                                        />
                                      </div>
                                    </div>
                                    <div className="row">
                                      <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label">
                                          Status
                                        </label>
                                        <Select
                                          id="status"
                                          name="status"
                                          value={this.state.valStatus}
                                          placeholder="Silahkan pilih Status"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.state.dataxtema
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm selectpicker p-0"
                                          options={this.optionstatus}
                                          onChange={this.handleChangeStatus}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                  <div className="modal-footer">
                                    <div className="d-flex justify-content-between">
                                      <button
                                        type="reset"
                                        className="btn btn-sm btn-light me-3"
                                        onClick={this.handleClickReset}
                                      >
                                        Reset
                                      </button>
                                      <button
                                        type="submit"
                                        className="btn btn-sm btn-primary"
                                        data-bs-dismiss="modal"
                                      >
                                        Apply Filter
                                      </button>
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="table-responsive">
                          <DataTable
                            columns={this.columns}
                            data={this.state.datax}
                            progressPending={this.state.loading}
                            highlightOnHover
                            pointerOnHover
                            pagination
                            paginationServer
                            paginationTotalRows={this.state.totalRows}
                            paginationComponentOptions={{
                              selectAllRowsItem: true,
                              selectAllRowsItemText: "Semua",
                            }}
                            onChangeRowsPerPage={this.handlePerRowsChange}
                            onChangePage={this.handlePageChange}
                            customStyles={this.customStyles}
                            persistTableHead={true}
                            noDataComponent={
                              <div className="mt-5">Tidak Ada Data</div>
                            }
                            onSort={this.handleSort}
                            sortServer
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="modal fade" tabIndex="-1" id="review_soal">
          <div className="modal-dialog modal-lg">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Review Soal</h5>
                <div
                  className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <span className="svg-icon svg-icon-2x">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <rect
                        opacity="0.5"
                        x="6"
                        y="17.3137"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(-45 6 17.3137)"
                        fill="currentColor"
                      />
                      <rect
                        x="7.41422"
                        y="6"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(45 7.41422 6)"
                        fill="currentColor"
                      />
                    </svg>
                  </span>
                </div>
              </div>
              <div className="modal-body">
                {this.state.review_soal}
                {this.state.show_next_prev ? (
                  <div className="text-end mt-5">
                    {this.state.no_soal == 1 ? (
                      <button
                        onClick={this.prevSoal}
                        className="btn btn-light btn-sm me-3 mr-2 disabled"
                      >
                        <i className="fa fa-chevron-left me-1"></i>Sebelumnya
                      </button>
                    ) : (
                      <button
                        onClick={this.prevSoal}
                        className="btn btn-light btn-sm me-3 mr-2"
                      >
                        <i className="fa fa-chevron-left me-1"></i>Sebelumnya
                      </button>
                    )}
                    {this.state.no_soal == this.state.all_soal.length ? (
                      <button
                        onClick={this.nextSoal}
                        className="disabled btn btn-primary btn-sm"
                      >
                        Selanjutnya<i className="fa fa-chevron-right ms-1"></i>
                      </button>
                    ) : (
                      <button
                        onClick={this.nextSoal}
                        className="btn btn-primary btn-sm"
                      >
                        Selanjutnya<i className="fa fa-chevron-right ms-1"></i>
                      </button>
                    )}
                  </div>
                ) : (
                  ""
                )}
              </div>
              <div className="modal-footer">
                <button
                  className="btn btn-light btn-sm"
                  data-bs-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
