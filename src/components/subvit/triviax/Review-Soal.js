import React, { useState } from "react";

export default class ReviewSoal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      soal: props.soal,
      pertanyaan: props.soal.pertanyaan,
      no: props.no,
      jawaban: props.soal.jawaban,
      field_jawaban: false,
      kunci_jawaban: props.soal.kunci_jawaban,
    };
  }

  componentDidMount() {
    this.handleReload();
  }

  handleReload() {
    if (this.state.soal.type == "polling") {
      const field_jawaban = [];
      const jawaban = JSON.parse(this.state.jawaban);
      const context = this.state;
      jawaban.forEach(function (element, i) {
        let is_kunci = false;
        if (element.key == context.kunci_jawaban) {
          is_kunci = true;
        }
        const choices = (
          <MultipleChoice
            key={i}
            abjad={element.key}
            option={element.option}
            image={element.image}
            is_kunci={is_kunci}
          />
        );
        field_jawaban.push(choices);
      });
      this.setState({ field_jawaban });
    }
  }
  render() {
    const styleImage = {
      maxWidth: "400px",
      width: "100%",
      height: "auto",
    };
    return (
      <div>
        <div className="mb-7">
          {this.state.soal.pertanyaan_gambar != null &&
          this.state.soal.pertanyaan_gambar != "null" ? (
            <div>
              <h5>{this.state.no}.</h5>
              <img
                className="w-100 rounded mt-3 mb-3"
                style={styleImage}
                src={this.state.soal.pertanyaan_gambar}
              />
              <br />
              <h5>{this.state.pertanyaan}</h5>
            </div>
          ) : (
            <h5>
              {this.state.no}. {this.state.pertanyaan}
            </h5>
          )}
        </div>
        {this.state.field_jawaban}
      </div>
    );
  }
}

function MultipleChoice(props) {
  const [valKey, setValKey] = useState(props.abjad);
  const [valOption, setValOption] = useState(props.option);
  const [valImage, setValImage] = useState(props.image);
  const [isKunci, setIsKunci] = useState(props.is_kunci);

  return (
    <div className="row">
      <div className="col-lg-12 fv-row mb-3">
        {valImage ? (
          <a
            href="#"
            style={{ width: "100%", border: "solid 1px" }}
            className={
              isKunci
                ? "btn btn-sm btn-flex btn-light fw-bolder d-flex justify-content-between btn-success"
                : "btn btn-sm btn-flex btn-light btn-active-success fw-bolder"
            }
            variant="outlined"
          >
            {valKey}.
            <div className="row">
              <div className="col-lg-12">
                <img src={valImage}></img>
              </div>
              <div className="col-lg-12 text-start ms-8">{valOption}</div>
            </div>
            {isKunci ? (
              <div className="text-white">
                <span
                  className="las la-key"
                  style={{ fontSize: "25px" }}
                ></span>
              </div>
            ) : (
              ""
            )}
          </a>
        ) : (
          <a
            href="#"
            style={{ width: "100%", border: "solid 1px" }}
            className={
              isKunci
                ? "btn btn-sm btn-flex btn-light fw-bolder d-flex justify-content-between btn-success"
                : "btn btn-sm btn-flex btn-light btn-active-success fw-bolder"
            }
            variant="outlined"
          >
            {valKey}. {valOption}
            {isKunci ? (
              <div className="text-white">
                <span
                  className="las la-key"
                  style={{ fontSize: "25px" }}
                ></span>
              </div>
            ) : (
              ""
            )}
          </a>
        )}
      </div>
    </div>
  );
}
