import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import {
  capitalizeTheFirstLetterOfEachWord,
  indonesianDateFormat,
} from "../../publikasi/helper";
import * as FileSaver from "file-saver";
import * as XLSX from "xlsx";
import moment from "moment";

export default class TriviaReport extends React.Component {
  constructor(props) {
    super(props);
    this.handleKembali = this.handleKembaliAction.bind(this);
    this.handleSort = this.handleSortAction.bind(this);
    this.exportReport = this.exportReport.bind(this);
    this.exportIsian = this.exportIsian.bind(this);
    this.handleChangeSearch = this.handleChangeSearchAction.bind(this);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleFilterSudah = this.handleFilterSudahAction.bind(this);
    this.handleFilterSedang = this.handleFilterSedangAction.bind(this);
    this.handleFilterBelum = this.handleFilterBelumAction.bind(this);
  }
  state = {
    datax: [],
    loading: true,
    totalRows: 0,
    newPerPage: 10,
    tempLastNumber: 0,
    currentPage: 0,
    isSearch: false,
    nama_trivia: "",
    sort: "nama_pelatihan asc",
    id_trivia: 0,
    jml_peserta: 0,
    jml_sudah_mengerjakan: 0,
    jml_sedang_mengerjakan: 0,
    jml_blm_mengerjakan: 0,
    param: "",
    status_trivia: -1,
    from_pagination_change: 0,
  };
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  columns = [
    {
      name: "No",
      center: true,
      cell: (row, index) => this.state.tempLastNumber + index + 1,
      width: "70px",
    },
    {
      nname: "Nama Peserta",
      sortable: true,
      className: "min-w-300px mw-300px",
      width: "300px",
      grow: 6,
      wrap: true,
      allowOverflow: false,
      selector: (row) => {
        return (
          <div>
            <label className="d-flex flex-stack my-2 cursor-pointer">
              <span className="d-flex align-items-center me-2">
                <span className="symbol symbol-50px me-6">
                  <span className="symbol-label bg-light-primary">
                    <span className="svg-icon svg-icon-1 svg-icon-primary">
                      {row.foto != null ? (
                        <img
                          src={
                            process.env.REACT_APP_BASE_API_URI +
                            "/download/get-file?path=" +
                            row.foto
                          }
                          alt=""
                          className="symbol-label"
                        />
                      ) : (
                        <span className="svg-icon svg-icon-1 svg-icon-primary text-center">
                          <small style={{ fontSize: "0.675em" }}>
                            Belum
                            <br />
                            Ada
                            <br />
                            Foto
                          </small>
                        </span>
                      )}
                    </span>
                  </span>
                </span>
                <span className="d-flex flex-column">
                  <span className="fs-7 fw-semibold text-muted">
                    {row.pno_registrasi}
                  </span>
                  <span
                    className="fw-bolder fs-7"
                    style={{
                      overflow: "hidden",
                      whiteSpace: "wrap",
                      textOverflow: "ellipses",
                    }}
                  >
                    {capitalizeTheFirstLetterOfEachWord(row.pnama)}
                  </span>
                  {/*<span className="text-muted fs-7 fw-semibold">
                      {row.pemail}
                    </span>*/}
                </span>
              </span>
            </label>
          </div>
        );
      },
    },
    {
      name: "Pelatihan",
      sortable: true,
      selector: (row) => {
        return (
          <div>
            <label className="d-flex flex-stack mb- mt-1">
              <span className="d-flex align-items-center me-2">
                <span className="d-flex flex-column">
                  <h6 className="fw-bolder fs-7 mb-0">
                    {capitalizeTheFirstLetterOfEachWord(row.ppelatihan)}
                  </h6>
                  <span className="text-muted fs-7 fw-semibold">
                    {capitalizeTheFirstLetterOfEachWord(row.pakademi)}
                  </span>
                </span>
              </span>
            </label>
          </div>
        );
      },
    },
    {
      name: "Pelaksanaan",
      sortable: true,
      center: true,
      width: "200px",
      selector: (row) => {
        if (row.ptgl_pengerjaan != "" && row.ptgl_pengerjaan) {
          const tanggal_indo = indonesianDateFormat(row.ptgl_pengerjaan);
          console.log(tanggal_indo);
          const tgl = tanggal_indo.split(" ");
          console.log(tgl);
          return (
            <div>
              <i className="bi bi-calendar me-1"></i>
              {tgl[2]} {tgl[1]} {tgl[0]}
            </div>
          );
        } else {
          return "-";
        }
      },
    },
    {
      name: "Jawaban",
      sortable: true,
      center: true,
      width: "200px",
      selector: (row) => (
        <div>
          <div>
            <i className="bi bi-files me-1"></i>
            {row.total_jawab ? row.total_jawab : "-"} Soal
          </div>
          <div>
            <i className="bi bi-check-circle-fill text-success me-1"></i>
            {row.jumlah_benar ? row.jumlah_benar : "-"} Benar
          </div>
          <div>
            <i className="bi bi-x-circle-fill text-danger me-1"></i>
            {row.jumlah_salah ? row.jumlah_salah : "-"} Salah
          </div>
        </div>
      ),
    },
    {
      name: "Status",
      center: true,
      cell: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.pstatus == "sudah mengerjakan"
                ? "success"
                : row.pstatus == "sedang mengerjakan"
                  ? "primary"
                  : "danger") +
              " fs-7 m-1"
            }
          >
            {capitalizeTheFirstLetterOfEachWord(row.pstatus)}
          </span>
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  componentDidMount() {
    const temp_storage = localStorage.getItem("dataMenus");
    localStorage.clear();
    localStorage.setItem("dataMenus", temp_storage);
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    let segment_url = window.location.pathname.split("/");
    let id_trivia = segment_url[4];
    const data = {
      id: id_trivia,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/trivia_select_byid",
        data,
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          const datax = res.data.result.Data[0];
          this.setState({
            nama_trivia: datax.nama_trivia,
          });
        }
      })
      .catch((error) => {});

    this.setState(
      {
        id_trivia: id_trivia,
      },
      () => {
        this.handleReload();
      },
    );
  }

  handleChangeSearchAction(e) {
    const searchText = e.currentTarget.value;
    if (searchText == "") {
      this.setState(
        {
          loading: true,
          param: "",
        },
        () => {
          this.handleReload();
        },
      );
    }
  }
  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    if (e.key == "Enter") {
      if (searchText == "") {
        this.setState(
          {
            isSearch: false,
            param: "",
          },
          () => {
            this.handleReload();
          },
        );
      } else {
        this.setState({ loading: true });
        this.setState({ isSearch: true });
        this.setState({ param: searchText }, () => {
          this.handleReload();
        });
      }
    }
  }

  handleReload(page, newPerPage) {
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });

    let dataBody = {
      id_trivia: this.state.id_trivia,
      start: start_tmp,
      rows: length_tmp,
      sort: this.state.sort,
      param: this.state.param,
      status: this.state.status_trivia,
      status_peserta: 0,
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/report_trivia",
        dataBody,
        this.configs,
      )
      .then((res) => {
        this.setState({ loading: false });
        const datax = res.data.result.Data;
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (messagex == "Daftar Survey Tidak Di Temukan!") {
          messagex = "Belum Ada Peserta yang Mengisi Trivia";
        }
        if (statusx) {
          this.setState({ datax });
          this.setState({ totalRows: res.data.result.JumlahData });
          this.setState({ currentPage: page });
          this.setState({
            jml_peserta: res.data.result.dash[0].jml_peserta,
            jml_blm_mengerjakan: res.data.result.dash[0].jml_blm_mengerjakan,
            jml_sedang_mengerjakan:
              res.data.result.dash[0].jml_sedang_mengerjakan,
            jml_sudah_mengerjakan:
              res.data.result.dash[0].jml_sudah_mengerjakan,
          });
        } else {
          this.setState({ datax: [] });
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.handleReload();
              }
            });
        }
      })
      .catch((error) => {
        console.log(error);
        this.setState({ datax: [] });
        this.setState({ loading: false });
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  handleKembaliAction() {
    window.history.back();
  }

  handleSortAction(column, sortDirection) {
    let sort = "";
    if (column.name == "Nama Peserta") {
      sort = "nama " + sortDirection;
    } else if (column.name == "Pelatihan") {
      sort = "nama_akademi " + sortDirection;
    } else if (column.name == "Pelaksanaan") {
      sort = "tgl_pengerjaan " + sortDirection;
    } else if (column.name == "Jawaban") {
      sort = "total_jawab " + sortDirection;
    } else if (column.name == "Status") {
      sort = "status " + sortDirection;
    }
    this.setState(
      {
        sort: sort,
      },
      () => {
        this.handleReload();
      },
    );
  }

  fileType =
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
  fileExtension = ".xlsx";

  exportToCSV(apiData, fileName) {
    const ws = XLSX.utils.json_to_sheet(apiData);
    const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
    const excelBuffer = XLSX.write(wb, {
      bookType: "xlsx",
      type: "array",
      Props: {
        Subject: "Trivia",
        Tags: "Trivia Report",
        Title: "Generated From Digitalent Scholarship",
        Comments: `By (${Cookies.get("user_name")} - ${Cookies.get(
          "user_id",
        )}) on ${moment().format("DD/MM/YYYY, HH:mm:ss")}`,
        Name: `From DTS By (${Cookies.get("user_id")}) on ${moment().format(
          "DD/MM/YYYY, HH:mm:ss",
        )}`,
      },
    });
    const data = new Blob([excelBuffer], { type: this.fileType });
    FileSaver.saveAs(data, fileName + this.fileExtension);
  }

  exportReport() {
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    let dataBody = {
      id_trivia: this.state.id_trivia,
      start: 0,
      rows: 1000,
      sort: this.state.sort,
      param: this.state.param,
      status: this.state.status_trivia,
      status_peserta: 0,
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/report_trivia",
        dataBody,
        this.configs,
      )
      .then((res) => {
        this.setState({ loading: false });
        const datax = res.data.result.Data;
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (messagex == "Daftar Survey Tidak Di Temukan!") {
          messagex = "Belum Ada Peserta yang Mengisi Trivia";
        }
        if (statusx) {
          const excel = [];
          this.state.datax.forEach(function (element, i) {
            const rows = {
              Nama: element.pnama,
              //Email: element.pemail,
              //NIK: element.pnik,
              NoRegistrasi: element.pno_registrasi,
              Akademi: element.pakademi,
              Pelatihan: element.ppelatihan,
              Tanggal_Pengerjaan: indonesianDateFormat(element.ptgl_pengerjaan),
              Jumlah_Soal: element.total_jawab,
              Jawaban_Benar: element.jumlah_benar,
              Jawaban_Salah: element.jumlah_salah,
              Status: element.pstatus,
            };
            excel.push(rows);
          });
          let tanggal = Date().toString();
          let split_tanggal = tanggal.split(" ");
          let text_tanggal =
            split_tanggal[1] +
            "_" +
            split_tanggal[2] +
            "_" +
            split_tanggal[3] +
            "_" +
            split_tanggal[4];
          this.exportToCSV(
            excel,
            "Report Trivia " + this.state.nama_trivia + " " + text_tanggal,
          );
          swal.close();
        } else {
          this.setState({ datax: [] });
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.handleReload();
              }
            });
        }
      })
      .catch((error) => {
        this.setState({ datax: [] });
        this.setState({ loading: false });
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  exportIsian() {
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    let dataBody = {
      id_trivia: this.state.id_trivia,
    };
    const excel = [];
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/trivia/export_report_trivia",
        dataBody,
        this.configs,
      )
      .then((res) => {
        const datax = res.data.result.Data;
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;

        if (statusx) {
          swal.hideLoading();
          //for every row
          datax.forEach(function (element, i) {
            let mergedPertanyaanJawaban = {};
            if (element.pjawaban) {
              const jawaban = JSON.parse(element.pjawaban);
              console.log(jawaban);
              //for every jawaban
              jawaban.forEach(function (elementJawaban, j) {
                let pertanyaanJawaban = {};
                if (elementJawaban.type == "polling") {
                  const answer = JSON.parse(elementJawaban.answer);
                  const key = elementJawaban.answer_key;
                  let result = answer.filter((obj) => {
                    return obj.key === key;
                  });
                  if (key) {
                    console.log(result);
                    pertanyaanJawaban = {
                      [j + 1 + ".Pertanyaan"]: elementJawaban.question,
                      [j + 1 + ".Jawaban"]:
                        result[0].key + ". " + result[0].option,
                    };
                  } else {
                    pertanyaanJawaban = {
                      [j + 1 + ".Pertanyaan"]: elementJawaban.question,
                      [j + 1 + ".Jawaban"]: "",
                    };
                  }
                }

                mergedPertanyaanJawaban = {
                  ...mergedPertanyaanJawaban,
                  ...pertanyaanJawaban,
                };
              });
            }

            let rows = {
              Nama: element.pnama,
              //Email: element.pemail,
              //NIK: element.pnik,
              NoRegistrasi: element.pno_registrasi,
              Akademi: element.pakademi,
              Pelatihan: element.ppelatihan,
              Tanggal_Pengerjaan: indonesianDateFormat(element.ptgl_pengerjaan),
              Status: element.pstatus,
              Jumlah_Soal: element.total_jawab,
              Jawaban_Benar: element.jumlah_benar,
              Jawaban_Salah: element.jumlah_salah,
            };

            rows = {
              ...rows,
              ...mergedPertanyaanJawaban,
            };

            excel.push(rows);
          });
          swal.close();
          this.exportToCSV(
            excel,
            "Report Isian Trivia " + this.state.nama_trivia,
          );
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.handleReload();
              }
            });
        }
      })
      .catch((error) => {
        console.log(error);
        this.setState({ loading: false });
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  handlePageChange = (page) => {
    if (this.state.from_pagination_change == 0) {
      this.setState({ loading: true });
      this.handleReload(page, this.state.newPerPage);
    }
  };
  handlePerRowsChange = async (newPerPage, page) => {
    if (this.state.from_pagination_change == 1) {
      this.setState({ loading: true });
      this.setState({ newPerPage: newPerPage }, () => {
        this.handleReload(page, this.state.newPerPage);
      });
    }
  };

  handleFilterBelumAction() {
    this.setState({ status_trivia: 0 }, () => {
      this.handleReload(1, this.state.newPerPage);
    });
  }

  handleFilterSudahAction() {
    this.setState({ status_trivia: 2 }, () => {
      this.handleReload(1, this.state.newPerPage);
    });
  }

  handleFilterSedangAction() {
    this.setState({ status_trivia: 1 }, () => {
      this.handleReload(1, this.state.newPerPage);
    });
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <path
                        opacity="0.3"
                        d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                        fill="#7239ea"
                      ></path>
                      <path
                        d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                        fill="#7239ea"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Subvit
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Trivia
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <a
                  onClick={this.handleKembali}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-7"
          id="kt_wrapper"
        >
          <div
            className="d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-12">
                    <div className="row">
                      <div className="col-xl-3 mb-3">
                        <a
                          href="#"
                          onClick={this.handleClickFilterPublish}
                          className="card hoverable"
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-primary mb-3 mt-1">
                                <span className="svg-icon svg-icon-primary svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <path
                                      opacity="0.3"
                                      d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM12.5 18C12.5 17.4 12.6 17.5 12 17.5H8.5C7.9 17.5 8 17.4 8 18C8 18.6 7.9 18.5 8.5 18.5L12 18C12.6 18 12.5 18.6 12.5 18ZM16.5 13C16.5 12.4 16.6 12.5 16 12.5H8.5C7.9 12.5 8 12.4 8 13C8 13.6 7.9 13.5 8.5 13.5H15.5C16.1 13.5 16.5 13.6 16.5 13ZM12.5 8C12.5 7.4 12.6 7.5 12 7.5H8C7.4 7.5 7.5 7.4 7.5 8C7.5 8.6 7.4 8.5 8 8.5H12C12.6 8.5 12.5 8.6 12.5 8Z"
                                      fill="currentColor"
                                    ></path>
                                    <rect
                                      x="7"
                                      y="17"
                                      width="6"
                                      height="2"
                                      rx="1"
                                      fill="currentColor"
                                    ></rect>
                                    <rect
                                      x="7"
                                      y="12"
                                      width="10"
                                      height="2"
                                      rx="1"
                                      fill="currentColor"
                                    ></rect>
                                    <rect
                                      x="7"
                                      y="7"
                                      width="6"
                                      height="2"
                                      rx="1"
                                      fill="currentColor"
                                    ></rect>
                                    <path
                                      d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z"
                                      fill="currentColor"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.jml_peserta}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Total Peserta
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-xl-3 mb-3">
                        <a
                          href="#"
                          className="card hoverable"
                          onClick={this.handleFilterSudah}
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-success mb-3 mt-1">
                                <span className="svg-icon svg-icon-success svg-icon-3x">
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24px"
                                    height="24px"
                                    viewBox="0 0 24 24"
                                    className="mh-50px"
                                  >
                                    <path
                                      d="M10.0813 3.7242C10.8849 2.16438 13.1151 2.16438 13.9187 3.7242V3.7242C14.4016 4.66147 15.4909 5.1127 16.4951 4.79139V4.79139C18.1663 4.25668 19.7433 5.83365 19.2086 7.50485V7.50485C18.8873 8.50905 19.3385 9.59842 20.2758 10.0813V10.0813C21.8356 10.8849 21.8356 13.1151 20.2758 13.9187V13.9187C19.3385 14.4016 18.8873 15.491 19.2086 16.4951V16.4951C19.7433 18.1663 18.1663 19.7433 16.4951 19.2086V19.2086C15.491 18.8873 14.4016 19.3385 13.9187 20.2758V20.2758C13.1151 21.8356 10.8849 21.8356 10.0813 20.2758V20.2758C9.59842 19.3385 8.50905 18.8873 7.50485 19.2086V19.2086C5.83365 19.7433 4.25668 18.1663 4.79139 16.4951V16.4951C5.1127 15.491 4.66147 14.4016 3.7242 13.9187V13.9187C2.16438 13.1151 2.16438 10.8849 3.7242 10.0813V10.0813C4.66147 9.59842 5.1127 8.50905 4.79139 7.50485V7.50485C4.25668 5.83365 5.83365 4.25668 7.50485 4.79139V4.79139C8.50905 5.1127 9.59842 4.66147 10.0813 3.7242V3.7242Z"
                                      fill="#50cd89"
                                    ></path>
                                    <path
                                      className="permanent"
                                      d="M14.8563 9.1903C15.0606 8.94984 15.3771 8.9385 15.6175 9.14289C15.858 9.34728 15.8229 9.66433 15.6185 9.9048L11.863 14.6558C11.6554 14.9001 11.2876 14.9258 11.048 14.7128L8.47656 12.4271C8.24068 12.2174 8.21944 11.8563 8.42911 11.6204C8.63877 11.3845 8.99996 11.3633 9.23583 11.5729L11.3706 13.4705L14.8563 9.1903Z"
                                      fill="white"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div
                                className="d-flex flex-column text-end mt-n4"
                                onClick={this.handleFilterSudah}
                              >
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.jml_sudah_mengerjakan}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Sudah Mengerjakan
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-xl-3 mb-3">
                        <a
                          href="#"
                          onClick={this.handleFilterSedang}
                          className="card hoverable"
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-warning mb-3 mt-1">
                                <span className="svg-icon svg-icon-warning svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <path
                                      opacity="0.3"
                                      d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                      fill="#ffc700"
                                    ></path>
                                    <path
                                      d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                      fill="#ffc700"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.jml_sedang_mengerjakan}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Sedang Mengerjakan
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-xl-3 mb-3">
                        <a
                          href="#"
                          onClick={this.handleFilterBelum}
                          className="card hoverable"
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-danger mb-3 mt-1">
                                <span className="svg-icon svg-icon-danger svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="#dc3545"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <rect
                                      opacity="0.3"
                                      x="2"
                                      y="2"
                                      width="20"
                                      height="20"
                                      rx="10"
                                      fill="#f1416c"
                                    ></rect>
                                    <rect
                                      x="7"
                                      y="15.3137"
                                      width="12"
                                      height="2"
                                      rx="1"
                                      transform="rotate(-45 7 15.3137)"
                                      fill="#dc3545"
                                    ></rect>
                                    <rect
                                      x="8.41422"
                                      y="7"
                                      width="12"
                                      height="2"
                                      rx="1"
                                      transform="rotate(45 8.41422 7)"
                                      fill="#dc3545"
                                    ></rect>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.jml_blm_mengerjakan}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Belum Mengerjakan
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-12 mt-5">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Report Trivia {this.state.nama_trivia}
                        </h1>
                      </div>
                      <div className="card-toolbar">
                        <div className="d-flex align-items-center position-relative my-1 me-2">
                          <span className="svg-icon svg-icon-1 position-absolute ms-6">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                              className="mh-50px"
                            >
                              <rect
                                opacity="0.5"
                                x="17.0365"
                                y="15.1223"
                                width="8.15546"
                                height="2"
                                rx="1"
                                transform="rotate(45 17.0365 15.1223)"
                                fill="currentColor"
                              ></rect>
                              <path
                                d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                fill="currentColor"
                              ></path>
                            </svg>
                          </span>
                          <input
                            type="text"
                            data-kt-user-table-filter="search"
                            className="form-control form-control-sm form-control-solid w-250px ps-14"
                            placeholder="Cari Peserta"
                            onKeyPress={this.handleKeyPress}
                            onChange={this.handleChangeSearch}
                          />

                          <button
                            className="btn btn-light ms-2 fw-bolder btn-sm"
                            data-kt-menu-trigger="click"
                            data-kt-menu-placement="bottom-end"
                            data-kt-menu-flip="top-end"
                          >
                            <i className="bi bi-cloud-download ms-1"></i>
                            <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                              Export
                            </span>
                          </button>
                          <div
                            className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-7 w-auto "
                            data-kt-menu="true"
                          >
                            <div>
                              <div className="menu-item">
                                <a
                                  href="#"
                                  onClick={this.exportReport}
                                  className="menu-link px-5"
                                >
                                  Export Report
                                </a>
                              </div>
                              <div className="menu-item">
                                <a
                                  href="#"
                                  onClick={this.exportIsian}
                                  className="menu-link px-5"
                                >
                                  Export Isian
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="table-responsive">
                        <DataTable
                          columns={this.columns}
                          data={this.state.datax}
                          progressPending={this.state.loading}
                          highlightOnHover
                          pointerOnHover
                          pagination
                          paginationServer
                          paginationTotalRows={this.state.totalRows}
                          paginationComponentOptions={{
                            selectAllRowsItem: true,
                            selectAllRowsItemText: "Semua",
                          }}
                          onChangeRowsPerPage={(
                            currentRowsPerPage,
                            currentPage,
                          ) => {
                            this.setState(
                              {
                                from_pagination_change: 1,
                              },
                              () => {
                                this.handlePerRowsChange(
                                  currentRowsPerPage,
                                  currentPage,
                                );
                              },
                            );
                          }}
                          onChangePage={(page, totalRows) => {
                            this.setState(
                              {
                                from_pagination_change: 0,
                              },
                              () => {
                                this.handlePageChange(page, totalRows);
                              },
                            );
                          }}
                          customStyles={this.customStyles}
                          persistTableHead={true}
                          noDataComponent={
                            <div className="mt-5">Tidak Ada Data</div>
                          }
                          onSort={this.handleSort}
                          sortServer
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
