import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import {
  capitalizeTheFirstLetterOfEachWord,
  dmyToYmd,
} from "./../../publikasi/helper";
import TriviaForm from "./form/Trivia-Tambah-Bulk/Tambah-Soal/TriviaForm";
import TriviaTambahListSoal from "./list/Trivia-Tambah-List-Soal";
import TriviaFormEdit from "./form/Trivia-Tambah-Bulk/Edit-Soal/TriviaFormEdit";
import ImportForm from "./form/Trivia-Tambah-Bulk/Tambah-Soal/ImportForm";
import DataTable from "react-data-table-component";
import { isAdminAkademi, isSuperAdmin } from "../../AksesHelper";

export default class TriviaTambahContent extends React.Component {
  constructor(props) {
    const temp_storage = localStorage.getItem("dataMenus");
    localStorage.clear();
    localStorage.setItem("dataMenus", temp_storage);
    super(props);
    this.handleChangeLevel = this.handleChangeLevelAction.bind(this);
    this.handleChangeTema = this.handleChangeTemaAction.bind(this);
    this.handleChangePelatihan = this.handleChangePelatihanAction.bind(this);
    this.onChangeMetode = this.onChangeMetodeAction.bind(this);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleSubmit = this.handleSubmitAction.bind(this);
    this.handleChangeAkademi = this.handleChangeAkademiAction.bind(this);
    this.handleKembali = this.handleKembaliAction.bind(this);
    this.handleCallBackTrivia = this.handleCallBackTriviaAction.bind(this);
    this.handleCallBackListSoal = this.handleCallBackListSoalAction.bind(this);
    this.handleCallBackEditSoal = this.handleCallBackEditSoalAction.bind(this);
    this.handleCallBackTriviaEdit =
      this.handleCallBackTriviaEditAction.bind(this);
    this.handleCallBackImport = this.handleCallBackImportAction.bind(this);
    this.handleNextStep = this.handleNextStepAction.bind(this);
    this.handlePreviousStep = this.handlePreviousStepAction.bind(this);
    this.handleChangeJudul = this.handleChangeJudulAction.bind(this);
    this.state = {
      fields: {},
      errors: {},
      errors_message: "",
      dataxakademi: [],
      dataxtema: [],
      dataxpelatihan: [],
      dataxakademi_show: [],
      dataxtema_show: [],
      dataxpelatihan_show: [],
      loading_akademi: false,
      loading_tema: false,
      loading_pelatihan: false,
      totalRowsAkademi: 0,
      totalRowsTema: 0,
      totalRowsPelatihan: 0,
      tempLastNumberAkademi: 0,
      tempLastNumberTema: 0,
      tempLastNumberPelatihan: 0,
      newPerPageAkademi: 10,
      newPerPageTema: 10,
      newPerPagePelatihan: 10,
      selectedOption: null,
      akademi_id: null,
      tema_id: null,
      pelatihan_id: null,
      metode_id: null,
      isDisabled: true,
      isDisabledKab: true,
      level: 0,
      isDisabledTema: true,
      isDisabledPelatihan: true,
      valAkademi: [],
      valTema: [],
      valPelatihan: [],
      levelAkademiDisabled: true,
      levelTemaDisabled: true,
      levelPelatihanDisabled: true,
      noSoalTrivia: 1,
      pilihanLevelTrivia: "",
      level_trivia: null,
      option_role_peserta: [],
      checkedState: [],
      arr_id_checked: [],
      is_add_soal: 0,
      valLevelTrivia: [],
      is_import: false,
      show_role: true,
      is_edit_soal: false,
      no_soal_edit: false,
      datax_evaluasi: [],
      check_all: false,
      show_hide_tingkatan: true,
      show_hide_trivia: true,
      duration: 0,
      question_to_share: 0,
      step: 1,
      text_simpan_lanjutkan: "Lanjutkan",
      judul: "",
      role_id: Cookies.get("role_id_user"),
    };
    this.formDatax = new FormData();
  }

  level_trivia = isSuperAdmin()
    ? [
        { value: 3, label: "Seluruh Populasi DTS" },
        { value: 0, label: "Akademi" },
        { value: 1, label: "Tema" },
        { value: 2, label: "Pelatihan" },
      ]
    : isAdminAkademi()
      ? [
          { value: 1, label: "Tema" },
          { value: 2, label: "Pelatihan" },
        ]
      : [
          { value: 1, label: "Tema" },
          { value: 2, label: "Pelatihan" },
        ];

  optionstatus = [
    { value: "1", label: "Publish" },
    { value: "0", label: "Draft" },
  ];

  optionstatus = [
    { value: "1", label: "Publish" },
    { value: "0", label: "Draft" },
  ];

  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  columnsAkademi = [
    {
      name: "No",
      cell: (row, index) => this.state.tempLastNumberAkademi + index + 1,
      width: "70px",
    },
    {
      name: "Akademi",
      sortable: false,
      selector: (row) => row.label,
    },
  ];

  columnsTema = [
    {
      name: "No",
      cell: (row, index) => this.state.tempLastNumberTema + index + 1,
      width: "70px",
    },
    {
      name: "Tema",
      sortable: false,
      selector: (row) => row.label,
    },
  ];

  columnsPelatihan = [
    {
      name: "No",
      cell: (row, index) => this.state.tempLastNumberPelatihan + index + 1,
      width: "70px",
    },
    {
      name: "Nama Pelatihan",
      sortable: false,
      //width: "300px",
      selector: (row) => row.label,
    },
  ];

  handleReloadAkademiList(page, newPerPage) {
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumberAkademi: start_tmp });
    const start = start_tmp;
    const rows = length_tmp + start_tmp;
    const dataxakademi_show = this.state.dataxakademi.slice(start, rows);
    this.setState({
      dataxakademi_show,
      loading_akademi: false,
    });
  }

  handlePageChangeAkademi = (page) => {
    this.setState({ loading_akademi: true });
    this.handleReloadAkademiList(page, this.state.newPerPageAkademi);
  };
  handlePerRowsChangeAkademi = async (newPerPage, page) => {
    this.setState({ loading_akademi: true });
    this.setState({ newPerPageAkademi: newPerPage });
    this.handleReloadAkademiList(page, newPerPage);
  };

  handleReloadTemaList(page, newPerPage) {
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumberTema: start_tmp });
    const start = start_tmp;
    const rows = length_tmp + start_tmp;
    const dataxtema_show = this.state.dataxtema.slice(start, rows);
    this.setState({
      dataxtema_show,
      loading_tema: false,
    });
  }

  handlePageChangeTema = (page) => {
    this.setState({ loading_tema: true });
    this.handleReloadTemaList(page, this.state.newPerPageTema);
  };
  handlePerRowsChangeTema = async (newPerPage, page) => {
    this.setState({ loading_tema: true });
    this.setState({ newPerPageTema: newPerPage });
    this.handleReloadTemaList(page, newPerPage);
  };

  handleReloadPelatihanList(page, newPerPage) {
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumberPelatihan: start_tmp });
    const start = start_tmp;
    const rows = length_tmp + start_tmp;
    const dataxpelatihan_show = this.state.dataxpelatihan.slice(start, rows);
    this.setState({
      dataxpelatihan_show,
      loading_pelatihan: false,
    });
  }

  handlePageChangePelatihan = (page) => {
    this.setState({ loading_pelatihan: true });
    this.handleReloadPelatihanList(page, this.state.newPerPagePelatihan);
  };
  handlePerRowsChangePelatihan = async (newPerPage, page) => {
    this.setState({ loading_pelatihan: true });
    this.setState({ newPerPagePelatihan: newPerPage });
    this.handleReloadPelatihanList(page, newPerPage);
  };

  handleChangeLevelAction = (selectedLevel) => {
    const level = selectedLevel.value;
    this.setState({
      valLevelTrivia: {
        label: selectedLevel.label,
        value: selectedLevel.value,
      },
    });
    if (level == 0) {
      this.setState({ levelAkademiDisabled: false });
      this.setState({ levelTemaDisabled: true });
      this.setState({ levelPelatihanDisabled: true });
      this.setState({ show_role: true });
      this.setState({ show_hide_tingkatan: true });
      this.clearCheckedRole();
    } else if (level == 1) {
      this.setState({ levelAkademiDisabled: false });
      this.setState({ levelTemaDisabled: false });
      this.setState({ levelPelatihanDisabled: true });
      this.setState({ show_role: true });
      this.setState({ show_hide_tingkatan: true });
      this.clearCheckedRole();
    } else if (level == 2) {
      this.setState({ levelAkademiDisabled: false });
      this.setState({ levelTemaDisabled: false });
      this.setState({ levelPelatihanDisabled: false });
      this.setState({ show_role: true });
      this.setState({ show_hide_tingkatan: true });
      this.clearCheckedRole();
    }
    //tambahan untuk level semua populasi
    else if (level == 3) {
      this.setState({ levelAkademiDisabled: true });
      this.setState({ levelTemaDisabled: true });
      this.setState({ levelPelatihanDisabled: true });
      this.setState({ show_role: false });
      this.setState({ show_hide_tingkatan: false });
      this.checkAllRole();

      this.handleReloadAkademiList();
      //ambil data buat list dibelakang, tema dan pelatihan
      const dataBody = {
        start: 0,
        rows: 100,
        id_akademi: 0,
        status: "null",
        cari: 0,
        sort: "tema",
        sort_val: "ASC",
      };
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/list_tema_filter2",
          dataBody,
          this.configs,
        )
        .then((res) => {
          const optionx = res.data.result.Data;
          const dataxtema = [];
          const totalRowsTema = res.data.result.TotalData;
          optionx.map((data) =>
            dataxtema.push({ value: data.id, label: data.name }),
          );
          this.setState(
            {
              dataxtema,
              totalRowsTema,
            },
            () => this.handleReloadTemaList(),
          );
        })
        .catch((error) => {
          const dataxtema = [];
          const totalRowsTema = 0;
          this.setState(
            {
              dataxtema,
              totalRowsTema,
            },
            () => this.handleReloadTemaList(),
          );
          let messagex = error.response.data.result.Message;
        });

      const date = new Date();
      const y = date.getFullYear();

      const dataBody2 = {
        mulai: 0,
        limit: 100,
        id_penyelenggara: 0,
        id_akademi: 0,
        id_tema: 0,
        status_substansi: 0,
        status_pelatihan: 0,
        status_publish: 99,
        provinsi: 0,
        param: null,
        sort: "id_pelatihan",
        sortval: "DESC",
        tahun: y,
        id_silabus: 0,
      };

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/pelatihan",
          dataBody2,
          this.configs,
        )
        .then((res) => {
          const optionx = res.data.result.Data;
          const dataxpelatihan = [];
          const totalRowsPelatihan = res.data.result.TotalData;
          optionx.map((data) =>
            dataxpelatihan.push({ value: data.pid, label: data.pelatihan }),
          );
          this.setState(
            {
              dataxpelatihan,
              totalRowsPelatihan,
            },
            () => this.handleReloadPelatihanList(),
          );
        })
        .catch((error) => {
          console.log(error);
          const dataxpelatihan = [];
          const totalRowsPelatihan = 0;
          this.setState(
            {
              dataxpelatihan,
              totalRowsPelatihan,
            },
            () => this.handleReloadPelatihanList(),
          );
          let messagex = error.response.data.result.Message;
        });
    }
    let errors = this.state.errors;
    errors["level"] = "";
    this.setState({
      level_trivia: level,
      valAkademi: [],
      valTema: [],
      valPelatihan: [],
      tema_id: null,
      pelatihan_id: null,
      akademi_id: null,
      isDisabledPelatihan: true,
      isDisabledTema: true,
      errors,
    });
  };

  clearCheckedRole() {
    const filled_option = [];
    const arr_id_checked = [];
    this.state.option_role_peserta.forEach(function (element, i) {
      filled_option[i] = false;
    });
    this.setState({
      checkedState: filled_option,
      arr_id_checked: [],
    });
  }

  checkAllRole() {
    const filled_option = [];
    const arr_id_checked = [];
    this.state.option_role_peserta.forEach(function (element, i) {
      filled_option[i] = true;
      arr_id_checked.push(element.id);
    });
    this.setState({
      checkedState: filled_option,
      arr_id_checked: arr_id_checked,
    });
  }

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    //ganti cursor di tab
    const tabs = document.getElementsByClassName("stepper-item");
    tabs.forEach(function (element) {
      element.style.cursor = "default";
    });

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/umum/list-status-peserta",
        null,
        this.configs,
      )
      .then((res) => {
        const option_role_peserta = res.data.result.Data;
        option_role_peserta.forEach(function (element, i) {
          if (
            element.name == "Pembatalan" ||
            element.name == "Mengundurkan Diri"
          ) {
            option_role_peserta.splice(i, 1);
          }
        });
        option_role_peserta.forEach(function (element, i) {
          if (element.name == "Banned") {
            option_role_peserta.splice(i, 1);
          }
        });
        this.setState({
          checkedState: new Array(option_role_peserta.length).fill(false),
        });
        this.setState({ option_role_peserta });
      });

    const date = new Date();
    const y = date.getFullYear();

    const dataAkademik = {
      start: 0,
      length: 100,
      status: "1",
      param: "",
      sort: "created_at",
      sort_val: "desc",
      tahun: y,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list-akademi-s3",
        dataAkademik,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const totalRowsAkademi = res.data.result.Total;
        const dataxakademi = [];
        optionx.map((data) =>
          dataxakademi.push({ value: data.id, label: data.name }),
        );
        this.setState(
          {
            dataxakademi,
            totalRowsAkademi,
          },
          () => {
            this.handleReloadAkademiList();
            swal.close();
          },
        );
      });
  }

  handleClickBatalAction(e) {
    window.location = "/subvit/trivia";
  }

  handleChangeAkademiAction = (akademi_id) => {
    let errors = this.state.errors;
    errors["idakademi"] = "";
    this.setState({
      valTema: [],
      valPelatihan: [],
      tema_id: null,
      pelatihan_id: null,
      isDisabledPelatihan: true,
      errors,
    });

    const dataBody = {
      start: 0,
      rows: 100,
      id_akademi: akademi_id.value,
      status: 1,
      cari: 0,
      sort: "tema",
      sort_val: "ASC",
    };

    if (this.state.level_trivia == 0) {
      const dataxakademi_show = [
        {
          label: akademi_id.label,
        },
      ];

      this.setState({
        dataxakademi_show,
      });
    }

    this.setState({
      valAkademi: { label: akademi_id.label, value: akademi_id.value },
    });
    this.setState({ akademi_id: akademi_id.value });
    this.setState({ pilihanLevelTrivia: akademi_id.label });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list_tema_filter2",
        dataBody,
        this.configs,
      )
      .then((res) => {
        this.setState({ isDisabledTema: false });
        const optionx = res.data.result.Data;
        const dataxtema = [];
        const totalRowsTema = res.data.result.TotalData;
        optionx.map((data) =>
          dataxtema.push({ value: data.id, label: data.name }),
        );
        this.setState(
          {
            dataxtema,
            totalRowsTema,
          },
          () => this.handleReloadTemaList(),
        );
      })
      .catch((error) => {
        const dataxtema = [];
        const totalRowsTema = 0;
        this.setState(
          {
            dataxtema,
            totalRowsTema,
          },
          () => this.handleReloadTemaList(),
        );
        let messagex = error.response.data.result.Message;
        if (this.state.level_trivia != 0) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });

    const date = new Date();
    const y = date.getFullYear();

    const dataBody2 = {
      mulai: 0,
      limit: 100,
      id_penyelenggara: 0,
      id_akademi: akademi_id.value,
      id_tema: 0,
      status_substansi: 0,
      status_pelatihan: 0,
      status_publish: 1,
      provinsi: 0,
      param: null,
      sort: "id_pelatihan",
      sortval: "DESC",
      tahun: y,
      id_silabus: 0,
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/pelatihan",
        dataBody2,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxpelatihan = [];
        const totalRowsPelatihan = res.data.result.TotalData;
        optionx.map((data) =>
          dataxpelatihan.push({ value: data.pid, label: data.pelatihan }),
        );
        this.setState(
          {
            dataxpelatihan,
            totalRowsPelatihan,
          },
          () => this.handleReloadPelatihanList(),
        );
      })
      .catch((error) => {
        console.log(error);
        const dataxpelatihan = [];
        const totalRowsPelatihan = 0;
        this.setState(
          {
            dataxpelatihan,
            totalRowsPelatihan,
          },
          () => this.handleReloadPelatihanList(),
        );
        let messagex = error.response.data.result.Message;
      });
  };

  handleChangeTemaAction = (tema) => {
    let errors = this.state.errors;
    errors["idtema"] = "";
    this.setState({ errors });
    this.setState({ valTema: { label: tema.label, value: tema.value } });
    this.setState({ tema_id: tema.value });
    this.setState({ pilihanLevelTrivia: tema.label });

    if (this.state.level_trivia == 1) {
      const dataxtema_show = [
        {
          label: tema.label,
        },
      ];
      this.setState({
        dataxtema_show,
      });
    }

    /*  const dataBody = {
             jns_param: 3,
             akademi_id: this.state.akademi_id,
             theme_id: tema.value,
             pelatihan_id: 0
         } */

    const date = new Date();
    const y = date.getFullYear();

    const dataBody = {
      mulai: 0,
      limit: 100,
      id_penyelenggara: 0,
      id_akademi: this.state.akademi_id,
      id_tema: tema.value,
      status_substansi: 0,
      status_pelatihan: 0,
      status_publish: 99,
      provinsi: 0,
      param: null,
      sort: "id_pelatihan",
      sortval: "DESC",
      tahun: y,
      id_silabus: 0,
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/pelatihan",
        dataBody,
        this.configs,
      )
      .then((res) => {
        this.setState({ valPelatihan: [] });
        this.setState({ isDisabledPelatihan: false });
        if (this.state.level_trivia == 2) {
          this.setState({ levelPelatihanDisabled: false });
        }
        const optionx = res.data.result.Data;
        const dataxpelatihan = [];
        const totalRowsPelatihan = res.data.result.TotalLength;
        optionx.map((data) => {
          const metode_pelatihan =
            data.metode_pelatihan == "Online"
              ? data.metode_pelatihan.toUpperCase()
              : data.nm_kab + " " + data.nm_prov;
          dataxpelatihan.push({
            value: data.id,
            label:
              "[" +
              data.penyelenggara +
              "] - " +
              data.slug_pelatian_id +
              " - " +
              data.pelatihan +
              " Batch " +
              data.batch +
              " - " +
              metode_pelatihan,
          });
        });
        this.setState(
          {
            dataxpelatihan,
            totalRowsPelatihan,
          },
          () => this.handleReloadPelatihanList(),
        );
      })
      .catch((error) => {
        console.log(error);
        const dataxpelatihan = [];
        const totalRowsPelatihan = 0;
        this.setState(
          {
            dataxpelatihan,
            totalRowsPelatihan,
            valPelatihan: [],
          },
          () => this.handleReloadPelatihanList(),
        );
        let messagex = error.response.data.result.Message;
        if (this.state.level_trivia > 1) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
          this.setState({ levelPelatihanDisabled: true });
        }
      });
  };

  handleChangePelatihanAction = (pelatihan) => {
    let errors = this.state.errors;
    errors["idpelatihan"] = "";
    this.setState({ errors });
    this.setState({
      valPelatihan: { label: pelatihan.label, value: pelatihan.value },
    });
    this.setState({ pelatihan_id: pelatihan.value });
    this.setState({ pilihanLevelTrivia: pelatihan.label });
    if (this.state.level_trivia == 2) {
      const dataxpelatihan_show = [{ label: pelatihan.label }];

      this.setState({
        dataxpelatihan_show,
      });
    }
  };

  onChangeMetodeAction(e) {
    let errors = this.state.errors;
    errors["metode"] = "";
    const metode = e.target.value;
    this.setState({
      metode_id: metode,
      errors,
    });
  }

  handleSubmitAction(e) {
    e.preventDefault();
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    if (this.handleValidation(e)) {
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });
      const bodyJson = this.buildSoalJson(dataForm);
      console.log(bodyJson);
      //jika belum entri soal maka tidak bisa input
      if (bodyJson[0].soal_json.length == 0) {
        swal
          .fire({
            title: "Belum Ada Soal yang di Input",
            icon: "warning",
            confirmButtonText: "Ok",
            didOpen: () => {
              swal.hideLoading();
            },
          })
          .then((result) => {
            if (result.isConfirmed) {
            }
          });
      } else {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/submit_create_trivia",
            bodyJson,
            this.configs,
          )
          .then((res) => {
            const statux = res.data.result.Status;
            const messagex = res.data.result.Message;
            if (statux) {
              swal
                .fire({
                  title: messagex,
                  icon: "success",
                  confirmButtonText: "Ok",
                  allowOutsideClick: false,
                })
                .then((result) => {
                  if (result.isConfirmed) {
                    window.location = "/subvit/trivia";
                  }
                });
            } else {
              swal
                .fire({
                  title: messagex,
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                  }
                });
            }
          })
          .catch((error) => {
            let statux = error.response.data.result.Status;
            let messagex = error.response.data.result.Message;
            if (!statux) {
              swal
                .fire({
                  title: messagex,
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                  }
                });
            }
          });
      }
    } else {
      /* swal.fire({
                title: 'Mohon Periksa Isian',
                icon: 'warning',
                confirmButtonText: 'Ok'
            }).then((result) => {
                if (result.isConfirmed) { }
            }); */
    }
  }

  buildSoalJson(dataForm) {
    const soal_json = [];
    const status = parseInt(dataForm.get("status"));
    const idakademi =
      dataForm.get("idakademi") != null
        ? parseInt(dataForm.get("idakademi"))
        : 0;
    const idtema =
      dataForm.get("idtema") != null ? parseInt(dataForm.get("idtema")) : 0;
    const idpelatihan =
      dataForm.get("idpelatihan") != null
        ? parseInt(dataForm.get("idpelatihan"))
        : 0;
    let duration = 0;
    for (let i = 0; i < localStorage.length + 1; i++) {
      const soal = JSON.parse(localStorage.getItem(i));
      if (soal) {
        soal["status"] = status;
        duration += soal.duration;
        //ambil image yang di tempel di html untuk pertanyaan
        const key_gambar_pertanyaan = soal.key_gambar_pertanyaan;
        let imagePertanyaanWrapper = document.getElementById(
          key_gambar_pertanyaan,
        );
        let gambar_pertanyaan = null;
        if (imagePertanyaanWrapper) {
          imagePertanyaanWrapper = imagePertanyaanWrapper.value;
          const splitImagePertanyaan = imagePertanyaanWrapper.split("_");
          gambar_pertanyaan = splitImagePertanyaan[1];
        }
        delete soal.key_gambar_pertanyaan;
        soal["gambar_pertanyaan"] = gambar_pertanyaan;

        //ambil image yang di tempel di html untuk jawaban
        soal.jawaban.forEach(function (element) {
          let image = null;
          let imageName = null;
          let imageWrapper = document.getElementById(element.key_image);
          if (imageWrapper) {
            imageWrapper = imageWrapper.value;
            const splitImage = imageWrapper.split("_");
            imageName = splitImage[0];
            image = splitImage[1];
          }
          delete element.key_image;
          element.image = image;
          element.imageName = imageName;

          //cek apakah punya sub, sub berarti triggeredquestion
          const is_trigger = element.hasOwnProperty("sub");
          if (is_trigger) {
            element.sub.forEach(function (childQuestion, i) {
              let panjang_sub = element.sub.length;
              if (i < panjang_sub - 1) {
                childQuestion.is_next = true;
              }
              let imageQuestion = null;
              let imageQuestionName = null;
              let imageQuestionWrapper = document.getElementById(
                childQuestion.key_image,
              );
              if (imageQuestionWrapper) {
                imageQuestionWrapper = imageQuestionWrapper.value;
                const splitImageQuestion = imageQuestionWrapper.split("_");
                imageQuestionName = splitImageQuestion[0];
                imageQuestion = splitImageQuestion[1];
              }
              delete childQuestion.key_image;
              childQuestion.image = imageQuestion;
              childQuestion.imageName = imageQuestionName;

              //ambil gambar child jawaban
              childQuestion.answer.forEach(function (childAnswer) {
                let imageChildren = null;
                let imageChildrenName = null;
                let imageChildrenWrapper = document.getElementById(
                  childAnswer.key_image,
                );
                if (imageChildrenWrapper) {
                  imageChildrenWrapper = imageChildrenWrapper.value;
                  const splitImageChildren = imageChildrenWrapper.split("_");
                  imageChildrenName = splitImageChildren[0];
                  imageChildren = splitImageChildren[1];
                }
                delete childAnswer.key_image;
                childAnswer.image = imageChildren;
                childAnswer.imageName = imageChildrenName;
              });
            });
          }
        });

        soal_json.push(soal);
      }
    }

    let start_at = dmyToYmd(dataForm.get("start_at"));
    let end_at = dmyToYmd(dataForm.get("end_at"));

    let question_to_share = dataForm.get("question_to_share");

    const bodyJson = [
      {
        judul: dataForm.get("judul"),
        idakademi: idakademi,
        idtema: idtema,
        idpelatihan: idpelatihan,
        id_user: Cookies.get("user_id"),
        start_at: start_at,
        end_at: end_at,
        questions_to_share: question_to_share,
        id_status_peserta: this.state.arr_id_checked.join(","),
        duration: duration,
        status: status,
        level: this.state.level_trivia,
        soal_json: soal_json,
      },
    ];

    return bodyJson;
  }

  resetError() {
    let errors = {};
    errors[
      ("level",
      "idakademi",
      "idtema",
      "idpelatihan",
      "start_at",
      "end_at",
      "status",
      "judul",
      "role",
      "question_to_share")
    ] = "";
    this.setState({ errors: errors });
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const check = this.checkEmpty(
      dataForm,
      ["start_at", "end_at", "status", "question_to_share"],
      [],
    );
    return check;
  }

  checkEmpty(dataForm, fieldName) {
    const errorMessageEmpty = "Tidak Boleh Kosong";
    let errors = {};
    let formIsValid = true;
    for (const field in fieldName) {
      const nameAttribute = fieldName[field];
      if (dataForm.get(nameAttribute) == "") {
        errors[nameAttribute] = errorMessageEmpty;
        formIsValid = false;
      }
    }

    if (parseInt(dataForm.get("question_to_share")) < 1) {
      errors["question_to_share"] = "Nilai Minimal 1";
      formIsValid = false;
    }

    //check jumlah soal
    let jml_soal = 0;
    for (let i = 0; i < localStorage.length + 1; i++) {
      const soal = JSON.parse(localStorage.getItem(i));
      if (soal) {
        jml_soal++;
      }
    }
    if (parseInt(dataForm.get("question_to_share")) > jml_soal) {
      errors["question_to_share"] =
        "Tidak Boleh Melebihi Jumlah Soal yang Diinput, Jumlah Soal Saat Ini Ada " +
        jml_soal;
      formIsValid = false;
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  handleCheckedChange(position) {
    const updatedCheckedState = this.state.checkedState.map((item, index) =>
      index === position ? !item : item,
    );
    this.setState({ checkedState: updatedCheckedState });

    const id_checked = this.state.option_role_peserta[position].id;
    const arr_id_checked = this.state.arr_id_checked;
    if (arr_id_checked.includes(id_checked)) {
      const index_checked = arr_id_checked.indexOf(id_checked);
      if (index_checked !== -1) {
        arr_id_checked.splice(index_checked, 1);
      }
    } else {
      arr_id_checked.push(id_checked);
    }

    if (arr_id_checked.length == this.state.option_role_peserta.length) {
      this.setState({ check_all: true });
    } else {
      this.setState({ check_all: false });
    }

    this.setState({ arr_id_checked }, () => {
      if (this.state.arr_id_checked.length > 0) {
        let errors = this.state.errors;
        errors["role"] = "";
        this.setState({ errors });
      }
    });
  }

  handleKembaliAction() {
    swal
      .fire({
        title: "Apakah Anda Yakin?",
        icon: "warning",
        confirmButtonText: "Ya",
        showCancelButton: true,
        cancelButtonText: "Tidak",
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
      })
      .then((result) => {
        if (result.isConfirmed) {
          window.history.back();
        }
      });
  }

  handleCallBackTriviaAction(e) {
    this.setState({
      is_add_soal: this.state.is_add_soal + 1,
    });
  }

  handleCallBackEditSoalAction(e) {
    this.setState({
      is_edit_soal: e.is_edit_soal,
      no_soal_edit: e.no_soal_edit,
    });
  }

  handleCallBackListSoalAction(e) {
    const last_number = e.last_number;
    this.setState({
      noSoalTrivia: last_number,
      is_add_soal: this.state.is_add_soal + 1,
    });
  }

  handleCallBackTriviaEditAction(e) {
    this.setState({
      is_edit_soal: e.is_edit_soal,
    });
  }

  handleCallBackImportAction(e) {
    this.setState({
      is_add_soal: this.state.is_add_soal + 1,
    });
  }

  handlePreviousStepAction() {
    if (this.state.step != 1) {
      let prev_step = this.state.step - 1;
      this.setState({
        step: prev_step,
      });

      if (this.state.step != 4) {
        this.setState({
          text_simpan_lanjutkan: "Lanjutkan",
        });
      }

      document.getElementById("prevStep").click();
    }
  }

  handleNextStepAction() {
    const errorMessageEmpty = "Tidak Boleh Kosong";
    let errors = {};
    this.resetError();
    let formIsValid = true;
    if (this.state.step == 1) {
      //validation step 1
      if (this.state.judul == "") {
        errors["judul"] = errorMessageEmpty;
        formIsValid = false;
      }

      if (this.state.level_trivia == null && this.state.level_trivia != 0) {
        errors["level"] = errorMessageEmpty;
        formIsValid = false;
      }
      if (
        (this.state.akademi_id == null || this.state.akademi_id == "") &&
        this.state.level_trivia != 3
      ) {
        errors["idakademi"] = errorMessageEmpty;
        formIsValid = false;
      }
      if (
        (this.state.tema_id == null || this.state.tema_id == "") &&
        (this.state.level_trivia == 1 || this.state.level_trivia == 2)
      ) {
        errors["idtema"] = errorMessageEmpty;
        formIsValid = false;
      }
      if (
        (this.state.tema_id == null || this.state.tema_id == "") &&
        this.state.level_trivia == 2
      ) {
        errors["idpelatihan"] = errorMessageEmpty;
        formIsValid = false;
      }
      if (this.state.metode_id == null) {
        errors["metode"] = errorMessageEmpty;
      }
      if (this.state.levelAkademiDisabled == false) {
        if (this.state.arr_id_checked.length == 0) {
          errors["role"] = errorMessageEmpty;
          formIsValid = false;
        }
      }
      this.setState({ errors: errors });
      if (formIsValid) {
        this.setState({ step: 2 });
        document.getElementById("nextStep").click();
      }
    } else if (this.state.step == 2) {
      formIsValid = false;
      for (let i = 0; i < localStorage.length + 1; i++) {
        const soal = JSON.parse(localStorage.getItem(i));
        if (soal) {
          formIsValid = true;
        }
      }

      if (formIsValid) {
        this.setState({
          step: 3,
          text_simpan_lanjutkan: "Lanjutkan",
        });
        document.getElementById("nextStep").click();
      } else {
        swal
          .fire({
            title: "Belum Ada Soal yang di Input",
            icon: "warning",
            confirmButtonText: "Ok",
            didOpen: () => {
              swal.hideLoading();
            },
          })
          .then((result) => {
            if (result.isConfirmed) {
            }
          });
      }
    } else if (this.state.step == 3) {
      this.setState({
        step: 4,
        text_simpan_lanjutkan: "Simpan",
      });
      document.getElementById("nextStep").click();
    } else if (this.state.step == 4) {
      document.getElementById("simpanStep").click();
    }
  }

  handleChangeJudulAction(e) {
    const judul = e.target.value;
    let errors = this.state.errors;
    errors["judul"] = "";
    this.setState({
      judul,
      errors,
    });
  }

  styles = {
    hide: {
      display: "none",
    },
  };

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <path
                        opacity="0.3"
                        d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                        fill="#7239ea"
                      ></path>
                      <path
                        d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                        fill="#7239ea"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Subvit
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Trivia
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <a
                  onClick={this.handleKembali}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div
                    className="stepper stepper-links d-flex flex-column"
                    id="kt_subvit_trivia"
                  >
                    <div className="col-lg-12 mt-7">
                      <div className="card border">
                        <div className="card-header">
                          <div className="card-title">
                            <div className="stepper-nav flex-wrap mb-n4">
                              <div
                                className="stepper-item ms-0 me-3 my-2 current"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label ms-0">
                                    <h3 className="stepper-title fs-6">
                                      Informasi Trivia
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Bank Soal
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Atur Soal
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Publish
                                    </h3>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="card-body">
                          <div className="highlight bg-light-primary my-5">
                            <div className="col-lg-12 mb-7 fv-row text-primary">
                              <h5 className="text-primary fs-5">Panduan</h5>
                              <p className="text-primary">
                                Sebelum membuat Trivia, mohon untuk membaca
                                panduan berikut :
                              </p>
                              <ul>
                                <li>
                                  Pelaksanaan Level Akademi : Berlaku untuk{" "}
                                  <strong>SEMUA</strong> Pelatihan pada akademi
                                  terpilih, setiap pelatihan pada akademi
                                  tersebut maupun yang baru dibuat kemudian akan
                                  otomatis merujuk kepada Trivia tersebut.
                                </li>
                                <li>
                                  Pelaksanaan Level Tema : Berlaku untuk{" "}
                                  <strong>SEMUA</strong> Pelatihan pada tema
                                  terpilih, setiap pelatihan pada tema tersebut
                                  akan otomatis merujuk kepada Trivia tersebut.
                                </li>
                                <li>
                                  Pelaksanaan Level Pelatihan : Berlaku{" "}
                                  <strong>HANYA</strong> pada pelatihan yang
                                  terpilih
                                </li>
                              </ul>
                            </div>
                          </div>
                          <form
                            action="#"
                            onSubmit={this.handleSubmit}
                            id="kt_subvit_trivia_form"
                          >
                            {/* MENU 1 */}
                            <div
                              className="current"
                              data-kt-stepper-element="content"
                            >
                              <div className="col-lg-12 fv-row">
                                <h5 className="mt-7 me-3 mb-5">
                                  Tambah Trivia
                                </h5>
                                <div className="row">
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Nama Trivia
                                    </label>
                                    <input
                                      className="form-control form-control-sm"
                                      placeholder="Nama Trivia"
                                      name="judul"
                                      id="judul"
                                      onBlur={this.handleChangeJudul}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["judul"]}
                                    </span>
                                  </div>

                                  {this.state.show_hide_trivia ? (
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label required">
                                        Level
                                      </label>
                                      <Select
                                        id="level"
                                        name="level"
                                        placeholder="Silahkan pilih"
                                        noOptionsMessage={({ inputValue }) =>
                                          !inputValue
                                            ? this.state.dataxakademi
                                            : "Data tidak tersedia"
                                        }
                                        className="form-select-sm selectpicker p-0"
                                        onChange={this.handleChangeLevel}
                                        options={this.level_trivia}
                                        value={this.state.valLevelTrivia}
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["level"]}
                                      </span>
                                    </div>
                                  ) : (
                                    ""
                                  )}
                                  {this.state.show_hide_tingkatan ? (
                                    <div>
                                      <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label required">
                                          Akademi
                                        </label>
                                        <Select
                                          id="id_akademi"
                                          name="idakademi"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.state.dataxakademi
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm selectpicker p-0"
                                          isDisabled={
                                            this.state.levelAkademiDisabled
                                          }
                                          onChange={this.handleChangeAkademi}
                                          options={this.state.dataxakademi}
                                          value={this.state.valAkademi}
                                        />
                                        <span style={{ color: "red" }}>
                                          {this.state.errors["idakademi"]}
                                        </span>
                                      </div>
                                      <div className="col-lg-12 mb-7 fv-row hide">
                                        <label className="form-label required">
                                          Tema
                                        </label>
                                        <Select
                                          name="idtema"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.state.dataxtema
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm selectpicker p-0"
                                          options={this.state.dataxtema}
                                          isDisabled={
                                            this.state.isDisabledTema ||
                                            this.state.levelTemaDisabled
                                          }
                                          value={this.state.valTema}
                                          onChange={this.handleChangeTema}
                                        />
                                        <span style={{ color: "red" }}>
                                          {this.state.errors["idtema"]}
                                        </span>
                                      </div>
                                      <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label required">
                                          Pelatihan
                                        </label>
                                        <Select
                                          name="idpelatihan"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.state.dataxtema
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm selectpicker p-0"
                                          options={this.state.dataxpelatihan}
                                          isDisabled={
                                            this.state.isDisabledPelatihan ||
                                            this.state.levelPelatihanDisabled
                                          }
                                          value={this.state.valPelatihan}
                                          onChange={this.handleChangePelatihan}
                                        />
                                        <span style={{ color: "red" }}>
                                          {this.state.errors["idpelatihan"]}
                                        </span>
                                      </div>
                                    </div>
                                  ) : (
                                    <div></div>
                                  )}

                                  {this.state.show_role ? (
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label required">
                                        Status Peserta
                                      </label>
                                      <div className="row">
                                        <div
                                          className="col-lg-3 mb-3 fv-row"
                                          key="custom-checkbox-999"
                                        >
                                          <input
                                            type="checkbox"
                                            id="custom-checkbox-999"
                                            name="all"
                                            checked={this.state.check_all}
                                            onChange={(e) => {
                                              this.setState(
                                                {
                                                  check_all:
                                                    !this.state.check_all,
                                                },
                                                () => {
                                                  if (this.state.check_all) {
                                                    this.checkAllRole();
                                                  } else {
                                                    this.clearCheckedRole();
                                                  }
                                                },
                                              );
                                            }}
                                          />
                                          <label
                                            className="ms-3"
                                            htmlFor="custom-checkbox-999"
                                          >
                                            {capitalizeTheFirstLetterOfEachWord(
                                              "all",
                                            )}
                                          </label>
                                        </div>
                                        {this.state.option_role_peserta.map(
                                          ({ id, name }, index) => {
                                            return (
                                              <div
                                                className="col-lg-3 mb-3 fv-row"
                                                key={`custom-checkbox-${index}`}
                                              >
                                                <input
                                                  type="checkbox"
                                                  id={`custom-checkbox-${index}`}
                                                  name={name}
                                                  value={id}
                                                  checked={
                                                    this.state.checkedState[
                                                      index
                                                    ]
                                                  }
                                                  onChange={() =>
                                                    this.handleCheckedChange(
                                                      index,
                                                    )
                                                  }
                                                />
                                                <label
                                                  className="ms-3"
                                                  htmlFor={`custom-checkbox-${index}`}
                                                >
                                                  {capitalizeTheFirstLetterOfEachWord(
                                                    name,
                                                  )}
                                                </label>
                                              </div>
                                            );
                                          },
                                        )}
                                      </div>
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["role"]}
                                      </span>
                                    </div>
                                  ) : (
                                    ""
                                  )}

                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Metode
                                    </label>
                                    <div
                                      className="d-flex"
                                      onChange={this.onChangeMetode}
                                    >
                                      <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                        <input
                                          className="form-check-input"
                                          type="radio"
                                          name="metode"
                                          value="Entry"
                                          id="metode1"
                                        />
                                        <label
                                          className="form-check-label"
                                          htmlFor="metode1"
                                        >
                                          Entry Soal
                                        </label>
                                      </div>
                                      <div className="form-check form-check-sm form-check-custom form-check-solid">
                                        <input
                                          className="form-check-input"
                                          type="radio"
                                          name="metode"
                                          value="CSV"
                                          id="metode2"
                                        />
                                        <label
                                          className="form-check-label"
                                          htmlFor="metode2"
                                        >
                                          Import .csv/.xls
                                        </label>
                                      </div>
                                    </div>
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["metode"]}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            {/* MENU 2 */}
                            <div data-kt-stepper-element="content">
                              <div className="col-lg-12 fv-row">
                                {this.state.metode_id == "CSV" ? (
                                  <ImportForm
                                    parentCallBack={this.handleCallBackImport}
                                    no_soal={this.state.noSoalTrivia}
                                  />
                                ) : (
                                  <TriviaForm
                                    parentState={this.state}
                                    parentCallBack={this.handleCallBackTrivia}
                                    no_soal={this.state.noSoalTrivia}
                                  />
                                )}
                              </div>
                            </div>
                            {/* MENU 3 */}
                            <div data-kt-stepper-element="content">
                              <div className="col-lg-12 fv-row">
                                {this.state.is_edit_soal ? (
                                  <TriviaFormEdit
                                    id_soal={this.state.no_soal_edit}
                                    parentCallBack={
                                      this.handleCallBackTriviaEdit
                                    }
                                  />
                                ) : (
                                  <TriviaTambahListSoal
                                    parentState={this.state}
                                    is_add_soal={this.state.is_add_soal}
                                    parentCallBack={this.handleCallBackListSoal}
                                    parentEditCallBack={
                                      this.handleCallBackEditSoal
                                    }
                                  />
                                )}
                              </div>
                            </div>
                            <div data-kt-stepper-element="content">
                              <div className="col-lg-12 mb-7 fv-row">
                                {this.state.level_trivia == 3 ? (
                                  <>
                                    <div className="row pt-7">
                                      <h2 className="fs-5 text-muted mb-3">
                                        Target Akademi
                                      </h2>
                                      <div className="col-lg-12">
                                        <div className="table-responsive">
                                          <DataTable
                                            columns={this.columnsAkademi}
                                            data={this.state.dataxakademi_show}
                                            progressPending={
                                              this.state.loading_akademi
                                            }
                                            highlightOnHover
                                            pointerOnHover
                                            pagination
                                            paginationServer
                                            paginationTotalRows={
                                              this.state.totalRowsAkademi
                                            }
                                            onChangeRowsPerPage={
                                              this.handlePerRowsChangeAkademi
                                            }
                                            onChangePage={
                                              this.handlePageChangeAkademi
                                            }
                                            customStyles={this.customStyles}
                                            persistTableHead={true}
                                            noDataComponent={
                                              <div className="mt-5">
                                                Tidak Ada Data
                                              </div>
                                            }
                                          />
                                        </div>
                                      </div>
                                    </div>

                                    <div className="row pt-7">
                                      <h2 className="fs-5 text-muted mb-3">
                                        Target Tema
                                      </h2>
                                      <div className="col-lg-12">
                                        <div className="table-responsive">
                                          <DataTable
                                            columns={this.columnsTema}
                                            data={this.state.dataxtema_show}
                                            progressPending={
                                              this.state.loading_tema
                                            }
                                            highlightOnHover
                                            pointerOnHover
                                            pagination
                                            paginationServer
                                            paginationTotalRows={
                                              this.state.totalRowsTema
                                            }
                                            onChangeRowsPerPage={
                                              this.handlePerRowsChangeTema
                                            }
                                            onChangePage={
                                              this.handlePageChangeTema
                                            }
                                            customStyles={this.customStyles}
                                            persistTableHead={true}
                                            noDataComponent={
                                              <div className="mt-5">
                                                Tidak Ada Data
                                              </div>
                                            }
                                          />
                                        </div>
                                      </div>
                                    </div>

                                    <div className="row pt-7">
                                      <h2 className="fs-5 text-muted mb-3">
                                        Target Pelatihan
                                      </h2>
                                      <div className="col-lg-12">
                                        <div className="table-responsive">
                                          <DataTable
                                            columns={this.columnsPelatihan}
                                            data={
                                              this.state.dataxpelatihan_show
                                            }
                                            progressPending={
                                              this.state.loading_pelatihan
                                            }
                                            highlightOnHover
                                            pointerOnHover
                                            pagination
                                            paginationServer
                                            paginationTotalRows={
                                              this.state.totalRowsPelatihan
                                            }
                                            onChangeRowsPerPage={
                                              this.handlePerRowsChangePelatihan
                                            }
                                            onChangePage={
                                              this.handlePageChangePelatihan
                                            }
                                            customStyles={this.customStyles}
                                            persistTableHead={true}
                                            noDataComponent={
                                              <div className="mt-5">
                                                Tidak Ada Data
                                              </div>
                                            }
                                          />
                                        </div>
                                      </div>
                                    </div>
                                  </>
                                ) : (
                                  ""
                                )}

                                {this.state.level_trivia == 0 ? (
                                  <>
                                    <div className="row pt-7">
                                      <h2 className="fs-5 text-muted mb-3">
                                        Target Akademi
                                      </h2>
                                      <div className="col-lg-12">
                                        <div className="table-responsive">
                                          <DataTable
                                            columns={this.columnsAkademi}
                                            data={this.state.dataxakademi_show}
                                            progressPending={
                                              this.state.loading_akademi
                                            }
                                            highlightOnHover
                                            pointerOnHover
                                            pagination={false}
                                            customStyles={this.customStyles}
                                            persistTableHead={true}
                                            noDataComponent={
                                              <div className="mt-5">
                                                Tidak Ada Data
                                              </div>
                                            }
                                          />
                                        </div>
                                      </div>
                                    </div>
                                    <div className="row pt-7">
                                      <h2 className="fs-5 text-muted mb-3">
                                        Target Tema
                                      </h2>
                                      <div className="col-lg-12">
                                        <div className="table-responsive">
                                          <DataTable
                                            columns={this.columnsTema}
                                            data={this.state.dataxtema_show}
                                            progressPending={
                                              this.state.loading_tema
                                            }
                                            highlightOnHover
                                            pointerOnHover
                                            pagination
                                            paginationServer
                                            paginationTotalRows={
                                              this.state.totalRowsTema
                                            }
                                            onChangeRowsPerPage={
                                              this.handlePerRowsChangeTema
                                            }
                                            onChangePage={
                                              this.handlePageChangeTema
                                            }
                                            customStyles={this.customStyles}
                                            persistTableHead={true}
                                            noDataComponent={
                                              <div className="mt-5">
                                                Tidak Ada Data
                                              </div>
                                            }
                                          />
                                        </div>
                                      </div>
                                    </div>
                                    <div className="row pt-7">
                                      <h2 className="fs-5 text-muted mb-3">
                                        Target Pelatihan
                                      </h2>
                                      <div className="col-lg-12">
                                        <div className="table-responsive">
                                          <DataTable
                                            columns={this.columnsPelatihan}
                                            data={
                                              this.state.dataxpelatihan_show
                                            }
                                            progressPending={
                                              this.state.loading_pelatihan
                                            }
                                            highlightOnHover
                                            pointerOnHover
                                            pagination
                                            paginationServer
                                            paginationTotalRows={
                                              this.state.totalRowsPelatihan
                                            }
                                            onChangeRowsPerPage={
                                              this.handlePerRowsChangePelatihan
                                            }
                                            onChangePage={
                                              this.handlePageChangePelatihan
                                            }
                                            customStyles={this.customStyles}
                                            persistTableHead={true}
                                            noDataComponent={
                                              <div className="mt-5">
                                                Tidak Ada Data
                                              </div>
                                            }
                                          />
                                        </div>
                                      </div>
                                    </div>
                                  </>
                                ) : (
                                  ""
                                )}

                                {this.state.level_trivia == 1 ? (
                                  <>
                                    <div className="row pt-7">
                                      <h2 className="fs-5 text-muted mb-3">
                                        Target Tema
                                      </h2>
                                      <div className="col-lg-12">
                                        <div className="table-responsive">
                                          <DataTable
                                            columns={this.columnsTema}
                                            data={this.state.dataxtema_show}
                                            progressPending={
                                              this.state.loading_tema
                                            }
                                            highlightOnHover
                                            pointerOnHover
                                            pagination={false}
                                            customStyles={this.customStyles}
                                            persistTableHead={true}
                                            noDataComponent={
                                              <div className="mt-5">
                                                Tidak Ada Data
                                              </div>
                                            }
                                          />
                                        </div>
                                      </div>
                                    </div>
                                    <div className="row pt-7">
                                      <h2 className="fs-5 text-muted mb-3">
                                        Target Pelatihan
                                      </h2>
                                      <div className="col-lg-12">
                                        <div className="table-responsive">
                                          <DataTable
                                            columns={this.columnsPelatihan}
                                            data={
                                              this.state.dataxpelatihan_show
                                            }
                                            progressPending={
                                              this.state.loading_pelatihan
                                            }
                                            highlightOnHover
                                            pointerOnHover
                                            pagination
                                            paginationServer
                                            paginationTotalRows={
                                              this.state.totalRowsPelatihan
                                            }
                                            onChangeRowsPerPage={
                                              this.handlePerRowsChangePelatihan
                                            }
                                            onChangePage={
                                              this.handlePageChangePelatihan
                                            }
                                            customStyles={this.customStyles}
                                            persistTableHead={true}
                                            noDataComponent={
                                              <div className="mt-5">
                                                Tidak Ada Data
                                              </div>
                                            }
                                          />
                                        </div>
                                      </div>
                                    </div>
                                  </>
                                ) : (
                                  ""
                                )}

                                {this.state.level_trivia == 2 ? (
                                  <>
                                    <div className="row pt-7">
                                      <h2 className="fs-5 text-muted mb-3">
                                        Target Pelatihan
                                      </h2>
                                      <div className="col-lg-12">
                                        <div className="table-responsive">
                                          <DataTable
                                            columns={this.columnsPelatihan}
                                            data={
                                              this.state.dataxpelatihan_show
                                            }
                                            progressPending={
                                              this.state.loading_pelatihan
                                            }
                                            highlightOnHover
                                            pointerOnHover
                                            pagination={false}
                                            customStyles={this.customStyles}
                                            persistTableHead={true}
                                            noDataComponent={
                                              <div className="mt-5">
                                                Tidak Ada Data
                                              </div>
                                            }
                                          />
                                        </div>
                                      </div>
                                    </div>
                                  </>
                                ) : (
                                  ""
                                )}

                                <div>
                                  <div className="border-top mx-0 pt-10 my-10"></div>
                                </div>
                                <h2 className="fs-5 mb-5">
                                  Pelaksanaan Trivia
                                </h2>
                                <div className="row">
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label required">
                                      Tgl. Mulai Trivia
                                    </label>
                                    <input
                                      className="form-control form-control-sm"
                                      placeholder="Tanggal Mulai Pelaksanaan"
                                      name="start_at"
                                      id="date_trivia_start"
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["start_at"]}
                                    </span>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label required">
                                      Tgl. Akhir Trivia
                                    </label>
                                    <input
                                      className="form-control form-control-sm"
                                      placeholder="Tanggal Selesai Pelaksanaan"
                                      name="end_at"
                                      id="date_trivia_end"
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["end_at"]}
                                    </span>
                                  </div>
                                </div>

                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Jumlah Soal
                                  </label>
                                  <input
                                    className="form-control form-control-sm"
                                    placeholder="Jumlah Soal"
                                    type="number"
                                    min="1"
                                    name="question_to_share"
                                    id="question_to_share"
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["question_to_share"]}
                                  </span>
                                </div>

                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Status
                                  </label>
                                  <Select
                                    name="status"
                                    placeholder="Silahkan pilih"
                                    noOptionsMessage={({ inputValue }) =>
                                      !inputValue
                                        ? this.state.datax
                                        : "Data tidak tersedia"
                                    }
                                    className="form-select-sm selectpicker p-0"
                                    options={this.optionstatus}
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["status"]}
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div className="text-center border-top pt-10 my-7">
                              {this.state.step != 1 ? (
                                <button
                                  type="button"
                                  className="btn btn-light btn-md me-3"
                                  onClick={this.handlePreviousStep}
                                >
                                  <i className="fa fa-chevron-left me-1"></i>
                                  Sebelumnya
                                </button>
                              ) : (
                                ""
                              )}
                              <button
                                type="button"
                                id="prevStep"
                                className="btn btn-secondary btn-md me-3 mr-2"
                                data-kt-stepper-action="previous"
                                style={{ display: "none" }}
                              >
                                <i className="fa fa-chevron-left me-1"></i>
                                Sebelumnya
                              </button>
                              <button
                                type="submit"
                                id="simpanStep"
                                className="btn btn-primary btn-md"
                                data-kt-stepper-action="submit"
                                style={{ display: "none" }}
                              >
                                <i className="fa fa-paper-plane ms-1"></i>Simpan
                              </button>

                              <button
                                type="button"
                                className="btn btn-primary btn-md"
                                onClick={this.handleNextStep}
                              >
                                <i className="fa fa-chevron-right"></i>
                                {this.state.text_simpan_lanjutkan}
                              </button>
                              <button
                                type="button"
                                id="nextStep"
                                className="btn btn-primary btn-md"
                                data-kt-stepper-action="next"
                                style={{ display: "none" }}
                              >
                                <i className="fa fa-chevron-right"></i>Lanjutkan
                              </button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="temp_image"></div>
      </div>
    );
  }
}
