import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import {
  capitalizeTheFirstLetterOfEachWord,
  dmyToYmd,
  ymdToDmy,
} from "../../publikasi/helper";

export default class TriviaEdit extends React.Component {
  constructor(props) {
    super(props);
    this.handleChangeJudul = this.handleChangeJudulAction.bind(this);
    this.handleChangeStatus = this.handleChangeStatusAction.bind(this);
    this.handleSubmit = this.handleSubmitAction.bind(this);
    this.handleKembali = this.handleKembaliAction.bind(this);
    this.handleChangeQuestionToShare =
      this.handleChangeQuestionToShareAction.bind(this);
    this.handleChangeDateEnd = this.handleChangeDateEndAction.bind(this);
    this.handleChangeDateStart = this.handleChangeDateStartAction.bind(this);
    this.handleDeleteTrivia = this.handleDeleteTriviaAction.bind(this);
  }

  state = {
    id_trivia: 0,
    nama_trivia: "",
    datax: [],
    judul: "",
    errors: {},
    valStatus: [],
    option_role_peserta: [],
    checkedState: [],
    arr_id_checked: [],
    start_at: 0,
    end_at: 0,
    question_to_share: 0,
    status: 1,
    status_disabled: 1,
    status_batal: false,
    duration: 0,
    current_status: 0,
    level: "",
    status_trivia: null,
  };

  optionstatus = [
    { value: 0, label: "Draft" },
    { value: 1, label: "Publish" },
  ];

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  level = ["Akademi", "Tema", "Pelatihan", "All Level DTS"];

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    let segment_url = window.location.pathname.split("/");
    let id_trivia = segment_url[4];

    const data = {
      id: id_trivia,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/trivia_select_byid",
        data,
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          const datax = res.data.result.trivia[0];

          const id_status_peserta = datax.id_status_peserta;
          const arr_id_status_peserta = id_status_peserta.split(",");

          this.setState({ arr_id_checked: arr_id_status_peserta });

          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/umum/list-status-peserta",
              null,
              this.configs,
            )
            .then((res) => {
              const option_role_peserta = res.data.result.Data;
              option_role_peserta.forEach(function (element, i) {
                if (
                  element.name == "Pembatalan" ||
                  element.name == "Mengundurkan Diri"
                ) {
                  option_role_peserta.splice(i, 1);
                }
              });
              option_role_peserta.forEach(function (element, i) {
                if (element.name == "Banned") {
                  option_role_peserta.splice(i, 1);
                }
              });
              const filled_option = [];
              option_role_peserta.forEach(function (element, i) {
                filled_option[i] = false;
                arr_id_status_peserta.forEach(function (id_status, j) {
                  if (id_status == element.id) {
                    filled_option[i] = true;
                  }
                });
              });
              this.setState({
                checkedState: filled_option,
              });
              this.setState({ option_role_peserta });
            });

          let valStatus = [];
          let status_disabled = 1;

          valStatus = {
            label: this.optionstatus[datax.status].label,
            value: datax.status,
          };
          status_disabled = datax.status;

          this.setState(
            {
              datax: datax,
              judul: datax.nama_trivia,
              level: datax.level,
              question_to_share: datax.questions_to_share,
              duration: datax.duration,
              valStatus: valStatus,
              status: datax.status,
              current_status: datax.status,
              status_disabled: status_disabled,
              start_at: ymdToDmy(datax.start_at),
              end_at: ymdToDmy(datax.end_at),
              id_trivia: id_trivia,
              nama_trivia: datax.nama_trivia,
              status_trivia: datax.status,
            },
            () => {
              console.log(this.state.status);
            },
          );
          //kalo statusnya batal, disable semuanya
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmationBUttonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
        swal.close();
      })
      .catch((error) => {
        console.log(error);
        swal.close();
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  handleChangeJudulAction(e) {
    let errors = this.state.errors;
    errors["judul"] = "";
    const judul = e.target.value;
    this.setState({
      nama_trivia: judul,
      errors,
    });
  }

  handleChangeDurationAction(e) {
    const duration = e.target.value;
    this.setState({ duration: duration });
  }

  handleChangeStatusAction = (status) => {
    this.setState({ valStatus: { label: status.label, value: status.value } });
    this.setState({ status: status.value });
  };

  handleCheckedChange(position) {
    const updatedCheckedState = this.state.checkedState.map((item, index) =>
      index === position ? !item : item,
    );
    this.setState({ checkedState: updatedCheckedState });

    const id_checked = this.state.option_role_peserta[position].id;
    const arr_id_checked = this.state.arr_id_checked;
    if (arr_id_checked.includes(id_checked + "")) {
      const index_checked = arr_id_checked.indexOf(id_checked + "");
      if (index_checked !== -1) {
        arr_id_checked.splice(index_checked, 1);
      }
    } else {
      arr_id_checked.push(id_checked);
    }
    if (arr_id_checked != null) {
      let errors = this.state.errors;
      errors["role"] = "";
      this.setState({ errors });
    }
    this.setState({ arr_id_checked });
  }

  handleSubmitAction(e) {
    const dataFormX = new FormData(e.currentTarget);
    e.preventDefault();
    this.resetError();
    if (this.handleValidation(e)) {
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });
      const dataForm = new FormData();
      dataForm.append("id", this.state.id_trivia);
      dataForm.append("start_at", dmyToYmd(dataFormX.get("start_at")));
      dataForm.append("end_at", dmyToYmd(dataFormX.get("end_at")));
      dataForm.append("status", this.state.status);
      dataForm.append("id_status_peserta", this.state.arr_id_checked.join(","));
      dataForm.append("nama_trivia", this.state.nama_trivia);
      if (this.state.current_status == 0) {
        dataForm.append(
          "questions_to_share",
          dataFormX.get("question_to_share"),
        );
      } else {
        dataForm.append("questions_to_share", this.state.question_to_share);
      }
      dataForm.append("duration", this.state.duration);

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/trivia_update",
          dataForm,
          this.configs,
        )
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/subvit/trivia";
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const check = this.checkEmpty(
      dataForm,
      ["start_at", "end_at", "status", "judul"],
      [],
    );
    return check;
  }

  checkEmpty(dataForm, fieldName) {
    const errorMessageEmpty = "Tidak Boleh Kosong";
    let errors = {};
    let formIsValid = true;
    for (const field in fieldName) {
      const nameAttribute = fieldName[field];
      if (dataForm.get(nameAttribute) == "") {
        errors[nameAttribute] = errorMessageEmpty;
        formIsValid = false;
      }
    }

    if (this.state.metode_id == null) {
      errors["metode"] = errorMessageEmpty;
    }

    if (this.state.arr_id_checked.length == 0) {
      errors["role"] = errorMessageEmpty;
      formIsValid = false;
    }

    if (this.state.current_status == 0) {
      if (dataForm.get("question_to_share") == "") {
        errors["question_to_share"] = errorMessageEmpty;
        formIsValid = false;
      }
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  resetError() {
    let errors = {};
    errors[("role", "start_at", "end_at", "status", "judul")] = "";
    this.setState({ errors: errors });
  }

  handleKembaliAction() {
    window.history.back();
  }

  handleChangeQuestionToShareAction(e) {
    let errors = this.state.errors;
    errors["question_to_share"] = "";
    const question_to_share = e.target.value;
    this.setState({
      question_to_share: question_to_share,
      errors,
    });
  }

  handleChangeDateEndAction(e) {
    this.setState({
      end_at: e.target.value,
    });
  }
  handleChangeDateStartAction(e) {
    this.setState({
      start_at: e.target.value,
    });
  }
  handleDeleteTriviaAction(e) {
    const idx = this.state.id_trivia;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          const data = { id: idx };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/trivia_delete",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                    allowOutsideClick: false,
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleKembaliAction();
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmationBUttonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            });
        }
      });
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <path
                        opacity="0.3"
                        d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                        fill="#7239ea"
                      ></path>
                      <path
                        d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                        fill="#7239ea"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Subvit
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Trivia
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <a
                  onClick={this.handleKembali}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </a>

                <a
                  href={"/subvit/trivia/list-soal/" + this.state.id_trivia}
                  className="btn btn-sm btn-primary me-2"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-list"></i>
                  </span>
                  List Soal
                </a>

                {this.state.status_trivia != 1 ? (
                  <a
                    href="#"
                    title="Hapus"
                    onClick={this.handleDeleteTrivia}
                    className="btn btn-sm btn-danger btn-active-light-info"
                  >
                    <i className="bi bi-trash-fill text-white"></i>
                    <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                      Hapus
                    </span>
                  </a>
                ) : (
                  <a
                    href="#"
                    title="Hapus"
                    className="disabled btn btn-sm btn-danger btn-active-light-info"
                  >
                    <i className="bi bi-trash-fill text-white"></i>
                    <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                      Hapus
                    </span>
                  </a>
                )}
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div
                    className="stepper stepper-links d-flex flex-column"
                    id="kt_subvit_survey_edit"
                  >
                    <div className="col-lg-12 mt-7">
                      <div className="card border">
                        <div className="card-body mt-5 pt-10 pb-8">
                          <div className="col-12">
                            <h1
                              className="align-items-center text-dark fw-bolder my-1 fs-4"
                              style={{ textTransform: "capitalize" }}
                            >
                              {this.state.nama_trivia}
                            </h1>
                            <p className="text-muted fs-7 mb-0">
                              {this.level[this.state.level]}
                            </p>
                          </div>
                        </div>
                        <div className="card-header">
                          <div className="card-title">
                            <div className="stepper-nav flex-wrap mb-n4">
                              <div
                                className="stepper-item ms-0 my-2 current"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label ms-0">
                                    <h3 className="stepper-title fs-6">
                                      Informasi Trivia
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Targeting
                                    </h3>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="card-body">
                          <form
                            action="#"
                            onSubmit={this.handleSubmit}
                            id="kt_subvit_trivia_form_edit"
                          >
                            {/* MENU 1 */}
                            <div
                              className="current"
                              data-kt-stepper-element="content"
                            >
                              <div className="row mt-7">
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Nama Trivia
                                  </label>
                                  <input
                                    className="form-control form-control-sm"
                                    placeholder="Nama Trivia"
                                    name="judul"
                                    id="judul"
                                    value={this.state.nama_trivia}
                                    //disabled={this.state.status_disabled == 1 ? 'disabled' : ''}
                                    onChange={this.handleChangeJudul}
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["judul"]}
                                  </span>
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Role Peserta
                                  </label>
                                  <div className="row">
                                    {this.state.option_role_peserta.map(
                                      ({ id, name }, index) => {
                                        return (
                                          <div
                                            className="col-lg-3 mb-3 fv-row"
                                            key={`custom-checkbox-${index}`}
                                          >
                                            <input
                                              type="checkbox"
                                              id={`custom-checkbox-${index}`}
                                              name={name}
                                              value={id}
                                              disabled={
                                                this.state.status_disabled == 1
                                                  ? "disabled"
                                                  : ""
                                              }
                                              checked={
                                                this.state.checkedState[index]
                                              }
                                              onChange={() =>
                                                this.handleCheckedChange(index)
                                              }
                                            />
                                            <label
                                              className="ms-3"
                                              htmlFor={`custom-checkbox-${index}`}
                                            >
                                              {capitalizeTheFirstLetterOfEachWord(
                                                name,
                                              )}
                                            </label>
                                          </div>
                                        );
                                      },
                                    )}
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["role"]}
                                  </span>
                                </div>
                              </div>
                            </div>
                            {/* MENU 2 */}
                            <div data-kt-stepper-element="content">
                              <div className="container px-0">
                                <div className="row mt-7">
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label required">
                                      Tgl. Mulai Trivia
                                    </label>
                                    <input
                                      className="form-control form-control-sm"
                                      placeholder="Tanggal Mulai Pelaksanaan"
                                      name="start_at"
                                      id="date_trivia_start"
                                      value={this.state.start_at}
                                      onBlur={this.handleChangeDateStart}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["start_at"]}
                                    </span>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label required">
                                      Tgl. Akhir Trivia
                                    </label>
                                    <input
                                      className="form-control form-control-sm"
                                      placeholder="Tanggal Selesai Pelaksanaan"
                                      name="end_at"
                                      id="date_trivia_end"
                                      value={this.state.end_at}
                                      onBlur={this.handleChangeDateEnd}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["end_at"]}
                                    </span>
                                  </div>
                                </div>

                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Jumlah Soal
                                  </label>
                                  <input
                                    className="form-control form-control-sm"
                                    placeholder="Jumlah Soal"
                                    type="number"
                                    min="1"
                                    name="question_to_share"
                                    id="question_to_share"
                                    value={this.state.question_to_share}
                                    onChange={this.handleChangeQuestionToShare}
                                    disabled={
                                      this.state.status_disabled == 1
                                        ? "disabled"
                                        : ""
                                    }
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["question_to_share"]}
                                  </span>
                                </div>

                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Status
                                  </label>
                                  <Select
                                    name="status"
                                    placeholder="Silahkan pilih"
                                    noOptionsMessage={({ inputValue }) =>
                                      !inputValue
                                        ? this.state.datax
                                        : "Data tidak tersedia"
                                    }
                                    className="form-select-sm selectpicker p-0"
                                    options={this.optionstatus}
                                    value={this.state.valStatus}
                                    onChange={this.handleChangeStatus}
                                    isDisabled={this.state.status_batal}
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["status"]}
                                  </span>
                                </div>
                              </div>
                            </div>

                            <div className="text-center my-7">
                              <button
                                className="btn btn-light btn-md me-3 mr-2"
                                data-kt-stepper-action="previous"
                              >
                                <i className="fa fa-chevron-left me-1"></i>
                                Sebelumnya
                              </button>
                              {
                                //jika trivia batal tidak keluar tombol simpan
                                this.state.datax.status == "2" ? (
                                  <button
                                    type="submit"
                                    className="disabled btn btn-primary btn-md"
                                    data-kt-stepper-action="submit"
                                  >
                                    <i className="fa fa-paper-plane ms-1"></i>
                                    Simpan
                                  </button>
                                ) : (
                                  <button
                                    type="submit"
                                    className="btn btn-primary btn-md"
                                    data-kt-stepper-action="submit"
                                  >
                                    <i className="fa fa-paper-plane ms-1"></i>
                                    Simpan
                                  </button>
                                )
                              }
                              <button
                                type="button"
                                className="btn btn-primary btn-md"
                                data-kt-stepper-action="next"
                              >
                                <i className="fa fa-chevron-right"></i>Lanjutkan
                              </button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
