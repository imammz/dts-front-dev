import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import ObjectiveForm from "./ObjectiveForm";
import imageCompression from "browser-image-compression";

const resizeFile = async (file) => {
  const options = {
    maxSizeMB: 0.2,
    maxWidthOrHeight: 300,
    useWebWorker: true,
  };

  return imageCompression(file, options);
};

export default class TriviaForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      fields: {},
      is_next_soal: true,
      choices: [],
      no_soal: 1,
      tipe: "polling",
      pertanyaan: "",
      key_gambar_pertanyaan: null,
      datax: {},
      id_soal: props.id_soal,
      id_trivia: props.id_trivia,
      nomor_soal: props.nomor_soal,
      kunci_jawaban: "",
      duration: "",
      current_image_name: "",
    };
    this.abjad = ["A", "B", "C", "D", "E", "F", "G"];
    this.handleSimpanKembali = this.handleSimpanKembaliAction.bind(this);
    this.handleKembali = this.handleKembaliAction.bind(this);
    this.handleChangePertanyaan = this.handleChangePertanyaanAction.bind(this);
    this.handleChangeGambar = this.handleChangeGambarAction.bind(this);
    this.handleCallBackObjective =
      this.handleCallBackObjectiveAction.bind(this);
    this.handleChangeDurasi = this.handleChangeDurasiAction.bind(this);
  }

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    //get data yg di local storage
    const datax = JSON.parse(localStorage.getItem(this.state.id_soal));

    if (datax.pertanyaan_gambar != null) {
      let fileName = "Pertanyaan" + this.props.nomor_soal;
      let wrapper = document.getElementById("temp_image");
      const keyImage = datax.id;
      this.setState({ key_gambar_pertanyaan: keyImage });
      let check = document.getElementById(keyImage);
      if (check) {
        check.value = fileName + "_" + datax.pertanyaan_gambar;
      } else {
        const hiddenWrapper = document.createElement("input");
        hiddenWrapper.value = fileName + "_" + datax.pertanyaan_gambar;
        hiddenWrapper.id = keyImage;
        hiddenWrapper.className = "imagePertanyaan_edit";
        hiddenWrapper.type = "hidden";
        wrapper.appendChild(hiddenWrapper);
      }
    }

    let imageName = null;
    let imageWrapper = document.getElementById(datax.id);
    if (imageWrapper) {
      imageWrapper = imageWrapper.value;
      const splitImage = imageWrapper.split("_");
      imageName = splitImage[0];
    }

    if (datax) {
      this.setState({
        datax: datax,
        pertanyaan: datax.pertanyaan,
        no_soal: datax.id,
        kunci_jawaban: datax.kunci_jawaban,
        duration: datax.duration,
        current_image_name: imageName,
      });
    }
  }

  handleKembaliAction() {
    window.location = "/subvit/trivia/list-soal/" + this.state.datax.id_trivia;
  }
  handleSimpanKembaliAction() {
    //check error
    const allJawaban = Array.from(document.getElementsByClassName("jawaban"));
    const arrError = [];

    for (let i = 0; i < allJawaban.length; i++) {
      let valueJawaban = allJawaban[i].getAttribute("value");
      let no_soal = allJawaban[i].getAttribute("no_soal");
      let index = allJawaban[i].getAttribute("index");
      if (valueJawaban == "") {
        const err = {
          no_soal: no_soal,
          index: index,
          message: "Jawaban Tidak Boleh Kosong",
        };
        arrError.push(err);
      }
    }
    let is_error = false;
    const allError = Array.from(document.getElementsByClassName("error"));
    for (let i = 0; i < allError.length; i++) {
      allError[i].textContent = "";
      let no_soal = allError[i].getAttribute("no_soal");
      let index = allError[i].getAttribute("index");
      for (let j = 0; j < arrError.length; j++) {
        if (arrError[j].no_soal == no_soal && arrError[j].index == index) {
          allError[i].textContent = arrError[j].message;
          this.setState({ is_next_soal: false });
          is_error = true;
        }
      }
    }
    let textRadio = "";
    if (this.state.kunci_jawaban == "") {
      textRadio = "Belum Memilih Kunci Jawaban";
      is_error = true;
    }
    const allRadioCheck = Array.from(
      document.getElementsByClassName("error_kunci"),
    );
    for (let i = 0; i < allRadioCheck.length; i++) {
      allRadioCheck[i].textContent = textRadio;
    }

    let errors = [];
    if (this.state.pertanyaan == "") {
      errors["pertanyaan"] = "Tidak Boleh Kosong";
      is_error = true;
    } else {
      errors["pertanyaan"] = "";
    }

    if (this.state.duration == "" || this.state.duration == 0) {
      errors["duration"] = "Tidak Boleh Kosong";
      is_error = true;
    } else {
      errors["duration"] = "";
    }

    this.setState({ errors: errors });
    //build js untuk insert
    //kembalikan kondisi pilihan multiple choice ke kosong
    if (!is_error) {
      const soal = {
        pid_trivia: this.state.id_trivia,
        pidtrivia_detail: this.state.id_soal,
        poin: this.state.datax.poin,
        pertanyaan: this.state.pertanyaan,
        key_gambar_pertanyaan: this.state.key_gambar_pertanyaan,
        type: this.state.tipe,
        status: this.state.datax.status,
        duration: parseInt(this.state.duration),
        kunci_jawaban: isNaN(this.state.kunci_jawaban)
          ? this.state.kunci_jawaban
          : this.abjad[this.state.kunci_jawaban],
        jawaban: [],
      };

      if (this.state.tipe == "polling") {
        const datax = JSON.parse(localStorage.getItem(this.state.tipe));
        const jawaban = [];
        for (let i = 0; i < datax.length; i++) {
          if (datax[i].no == this.state.no_soal) {
            const keyImage = this.state.no_soal + "_" + datax[i].index;

            let image = null;
            let imageName = null;
            let imageWrapper = document.getElementById(keyImage);
            if (imageWrapper) {
              imageWrapper = imageWrapper.value;
              const splitImage = imageWrapper.split("_");
              imageName = splitImage[0];
              image = splitImage[1];
            }

            const option = {
              key: this.abjad[datax[i].index],
              option: datax[i].jawaban,
              image: image,
              color: false,
            };
            jawaban.push(option);
          }
        }
        soal.jawaban = jawaban;
      }

      const key_gambar_pertanyaan = soal.key_gambar_pertanyaan;
      let imagePertanyaanWrapper = document.getElementById(
        key_gambar_pertanyaan,
      );
      let gambar_pertanyaan = null;
      if (imagePertanyaanWrapper) {
        imagePertanyaanWrapper = imagePertanyaanWrapper.value;
        const splitImagePertanyaan = imagePertanyaanWrapper.split("_");
        gambar_pertanyaan = splitImagePertanyaan[1];
      }
      delete soal.key_gambar_pertanyaan;

      const dataForm = new FormData();
      dataForm.append("id", this.state.id_soal);
      dataForm.append("id_trivia", soal.pid_trivia);
      dataForm.append("pertanyaan", soal.pertanyaan);
      dataForm.append("pertanyaan_gambar", gambar_pertanyaan);
      dataForm.append("type", soal.type);
      dataForm.append("kunci_jawaban", soal.kunci_jawaban);
      dataForm.append("jawaban", JSON.stringify(soal.jawaban));
      dataForm.append("status", soal.status);
      dataForm.append("duration", soal.duration);
      dataForm.append("update_by", Cookies.get("user_id"));

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/soaltrivia_update",
          dataForm,
          this.configs,
        )
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location =
                    "/subvit/trivia/list-soal/" + this.state.id_trivia;
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    }
  }

  handleChangePertanyaanAction(e) {
    e.preventDefault();
    let errors = this.state.errors;
    errors["pertanyaan"] = "";
    this.setState({
      pertanyaan: e.currentTarget.value,
      errors,
    });
  }

  handleChangeDurasiAction(e) {
    e.preventDefault();
    let errors = this.state.errors;
    errors["duration"] = "";
    let duration = e.currentTarget.value;
    if (duration < 0) {
      duration = 0;
    }
    this.setState({
      duration: duration,
      errors,
    });
  }

  handleChangeGambarAction(e) {
    const imageFile = e.target.files[0];
    const reader = new FileReader();
    const keyImage = this.state.no_soal;
    this.setState({ key_gambar_pertanyaan: keyImage });
    resizeFile(imageFile).then((cf) => {
      reader.readAsDataURL(cf);
      reader.onload = function () {
        let result = reader.result;
        let fileName = imageFile.name;
        let wrapper = document.getElementById("temp_image");
        let check = document.getElementById(keyImage);
        if (check) {
          check.value = fileName + "_" + result;
        } else {
          const hiddenWrapper = document.createElement("input");
          hiddenWrapper.value = fileName + "_" + result;
          hiddenWrapper.id = keyImage;
          hiddenWrapper.className = "imagePertanyaan";
          hiddenWrapper.type = "hidden";
          wrapper.appendChild(hiddenWrapper);
        }
      };
    });
  }

  handleCallBackObjectiveAction(e) {
    this.setState(
      {
        kunci_jawaban: e.kunci_jawaban,
      },
      () => {},
    );
  }

  render() {
    return (
      <>
        <h5 className="mt-7 mb-5 me-3">Soal {this.state.nomor_soal}</h5>
        <div className="row">
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">Pertanyaan</label>
            <input
              className="form-control form-control-sm"
              placeholder="Masukkan pertanyaan"
              name="pertanyaan"
              value={this.state.pertanyaan}
              onChange={this.handleChangePertanyaan}
            />
            <span style={{ color: "red" }}>
              {this.state.errors["pertanyaan"]}
            </span>
          </div>
          <div className="col-lg-12">
            <div className="mb-7 fv-row">
              <label className="form-label">
                {this.state.current_image_name != null &&
                this.state.current_image_name != "" ? (
                  <div>
                    Ganti Gambar{" "}
                    <span className="text-danger">
                      {this.state.current_image_name}
                    </span>
                  </div>
                ) : (
                  "Gambar Jawaban (Optional)"
                )}
              </label>
              <input
                type="file"
                className="form-control form-control-sm mb-2"
                name="upload_silabus"
                accept="image/*"
                onChange={this.handleChangeGambar}
                value={this.state.gambar}
              />
              <small className="text-muted">
                Format File (.jpg/.jpeg/.png/.svg)
              </small>
              <br />
              <span style={{ color: "red" }}>
                {this.state.errors["upload_silabus"]}
              </span>
            </div>
          </div>

          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">Durasi (Detik)</label>
            <input
              className="form-control form-control-sm"
              placeholder="Masukkan Durasi"
              type="number"
              min={0}
              name="duration"
              value={this.state.duration}
              onChange={this.handleChangeDurasi}
            />
            <span style={{ color: "red" }}>
              {this.state.errors["duration"]}
            </span>
          </div>

          <ObjectiveForm
            id_soal={this.state.id_soal}
            choices={this.state.choices}
            no_soal={this.state.no_soal}
            parentCallBack={this.handleCallBackObjective}
          />

          <div className="text-center my-7">
            <a
              onClick={this.handleKembali}
              className="btn btn-light btn-md me-6"
            >
              Batal
            </a>
            <a
              onClick={this.handleSimpanKembali}
              className="btn btn-primary btn-md "
            >
              <i className="fa fa-paper-plane ms-1"></i>Simpan
            </a>
          </div>
        </div>
        <div id="temp_image"></div>
      </>
    );
  }
}
