import React, { useState } from "react";
import imageCompression from "browser-image-compression";
import { read, utils } from "xlsx";
import swal from "sweetalert2";
import ImportReport from "../../../../survey/Import-Report";

const resizeFile = async (file) => {
  const options = {
    maxSizeMB: 0.2,
    maxWidthOrHeight: 300,
    useWebWorker: true,
  };

  return imageCompression(file, options);
};

export default class ImportForm extends React.Component {
  constructor(props) {
    super(props);
    this.onChangeTemplate = this.onChangeTemplateAction.bind(this);
    this.onChangeImages = this.onChangeImagesAction.bind(this);
    this.importTemplate = this.importTemplateAction.bind(this);
    this.kembali = this.kembaliAction.bind(this);
    this.handleCallBackDelete = this.handleCallBackDeleteAction.bind(this);
    this.state = {
      dataTemplate: [],
      fileTemplateImages: [],
      dataChildTrigger: [],
      noSoalTrivia: this.props.nomor_soal,
      tipe: "polling",
      id_trivia: props.id_trivia,
      is_uploaded: false,
      errors: {},
      success: {},
      failed: [],
    };
    this.abjad = ["A", "B", "C", "D", "E", "F", "G"];
  }

  onChangeImagesAction(e) {
    const tempImage = [];
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    const length_file = e.target.files.length;
    e.target.files.forEach(
      function (element, i) {
        resizeFile(element).then((cf) => {
          if (length_file - 1 == i) {
            swal.close();
          }
          let reader = new FileReader();
          reader.readAsDataURL(cf);
          const context = this;
          reader.onload = function () {
            let result = reader.result;
            let fileName = element.name;
            let wrapperHTML = document.getElementById("temp_image");

            let check = document.getElementById(fileName);
            if (check) {
              check.value = fileName + "_" + result;
            } else {
              const wrapper = (
                <Gambar
                  fileName={fileName}
                  key={i}
                  index={i}
                  onClickDelete={context.handleCallBackDelete}
                />
              );
              tempImage.push(wrapper);

              const hiddenWrapper = document.createElement("input");
              hiddenWrapper.value = fileName + "_" + result;
              hiddenWrapper.imageName = fileName;
              hiddenWrapper.id = fileName;
              hiddenWrapper.className = "imageImport";
              hiddenWrapper.type = "hidden";
              wrapperHTML.appendChild(hiddenWrapper);
            }
            context.setState({ fileTemplateImages: tempImage });
          };
        });
      }.bind(this),
    );
  }

  handleCallBackDeleteAction(e) {
    const index = e;
    const newList = this.state.fileTemplateImages.filter(
      (item) => item.key !== index,
    );
    this.setState({ fileTemplateImages: newList });
  }

  onChangeTemplateAction(e) {
    const [file] = e.target.files;
    const reader = new FileReader();

    const errors = this.state.errors;
    errors["template"] = "";
    this.setState({
      errors,
    });
    reader.onload = (evt) => {
      const bstr = evt.target.result;
      const wb = read(bstr, { type: "binary" });
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      const data = utils.sheet_to_json(ws, { header: 1 });
      this.setState({
        dataTemplate: data,
        is_uploaded: true,
      });
    };

    reader.readAsBinaryString(file);
  }

  buildLocalStorage(datax, context) {
    let is_error = false;
    datax.forEach(function (element, i) {
      //skip index ke 0 karena header
      if (i != 0 && element[0] != "" && element[1] != "" && element[2] != "") {
        const soal = {
          nosoal: element[0] + (context.state.noSoalTrivia - 1),
          pid_trivia: context.state.id_trivia,
          pidtrivia_detail: 0,
          pertanyaan: element[1],
          key_gambar_pertanyaan: element[2],
          duration: element[3],
          kunci_jawaban: element[4],
          type: context.state.tipe,
        };
        let jawaban = [];

        jawaban = [
          context.setOption("A", element[5], element[6]),
          context.setOption("B", element[7], element[8]),
          context.setOption("C", element[9], element[10]),
          context.setOption("D", element[11], element[12]),
          context.setOption("E", element[13], element[14]),
          context.setOption("F", element[15], element[16]),
        ];

        const removeJawabanFalse = jawaban.filter(function (obj) {
          return obj !== false;
        });

        if (!soal.nosoal) {
          const err = {
            no: "",
            soal: soal.pertanyaan,
            kesalahan: "Nomor Belum Terisi",
          };
          const failed = context.state.failed;
          failed.push(err);
          context.setState({ failed });
          is_error = true;
        }

        if (!soal.pertanyaan) {
          const err = {
            no: soal.nosoal,
            soal: "",
            kesalahan: "Soal Belum Terisi",
          };
          const failed = context.state.failed;
          failed.push(err);
          context.setState({ failed });
          is_error = true;
        }

        if (!soal.duration) {
          const err = {
            no: soal.nosoal,
            soal: soal.pertanyaan,
            kesalahan: "Durasi Belum Terisi",
          };
          const failed = context.state.failed;
          failed.push(err);
          context.setState({ failed });
          is_error = true;
        }

        if (!soal.kunci_jawaban) {
          const err = {
            no: soal.nosoal,
            soal: soal.pertanyaan,
            kesalahan: "Kunci Jawaban Belum Terisi",
          };
          const failed = context.state.failed;
          failed.push(err);
          context.setState({ failed });
          is_error = true;
        }

        if (removeJawabanFalse.length < 2) {
          const err = {
            no: soal.nosoal,
            soal: soal.pertanyaan,
            kesalahan: "Pilihan Objective Minimal 2",
          };
          const failed = context.state.failed;
          failed.push(err);
          context.setState({ failed });
          is_error = true;
        }

        jawaban = jawaban.filter(function (e) {
          return e !== false;
        });

        let key_is_valid = false;
        for (let i = 0; i < jawaban.length; i++) {
          if (jawaban[i].key == soal.kunci_jawaban) {
            key_is_valid = true;
          }
        }

        if (!key_is_valid) {
          const err = {
            no: soal.nosoal,
            soal: soal.pertanyaan,
            kesalahan: 'Kunci Jawaban "' + soal.kunci_jawaban + '" Tidak Valid',
          };
          const failed = context.state.failed;
          failed.push(err);
          context.setState({ failed });
          is_error = true;
        }

        soal.jawaban = jawaban;
        let old = JSON.parse(localStorage.getItem("TAMBAH"));
        if (old !== null) {
          soal.pidtrivia_detail = "_" + soal.nosoal;
          old.push("_" + soal.nosoal);
          localStorage.setItem("_" + soal.nosoal, JSON.stringify(soal));
          localStorage.setItem("TAMBAH", JSON.stringify(old));
        } else {
          soal.pidtrivia_detail = "_" + soal.nosoal;
          localStorage.setItem("_" + soal.nosoal, JSON.stringify(soal));
          localStorage.setItem("TAMBAH", JSON.stringify(["_" + soal.nosoal]));
        }
      }
    });
    if (is_error) {
      let failed = this.state.failed;
      let old = JSON.parse(localStorage.getItem("TAMBAH"));
      for (let i = 0; i < failed.length; i++) {
        localStorage.removeItem("_" + failed[i].no);
        old = old.filter(function (e) {
          return e !== "_" + failed[i].no;
        });
      }
      localStorage.setItem("TAMBAH", JSON.stringify(old));
      this.setState({ is_uploaded: false });
      return false;
    } else {
      return true;
    }
  }

  checkTemplate(datax) {
    //check template sesuai tidak
    if (
      datax[0][0] != "No" ||
      datax[0][1] != "Pertanyaan" ||
      datax[0][2] != "Nama Gambar Pertanyaan" ||
      datax[0][3] != "Durasi" ||
      datax[0][4] != "Kunci Jawaban"
    ) {
      return false;
    } else {
      return true;
    }
  }

  importTemplateAction(e) {
    e.preventDefault();
    const errors = this.state.errors;
    if (this.state.is_uploaded) {
      errors["template"] = "";
      this.setState({
        errors,
      });

      if (!this.checkTemplate(this.state.dataTemplate)) {
        const errors = this.state.errors;
        swal
          .fire({
            title:
              "Template Tidak Sesuai, Pastikan Menggunakan Template Trivia",
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            errors["template"] = "Template Tidak Sesuai";
            this.setState({
              errors,
            });
          });
      } else if (this.buildLocalStorage(this.state.dataTemplate, this)) {
        swal
          .fire({
            title: "Soal Berhasil Disimpan",
            icon: "success",
            confirmButtonText: "Ok",
          })
          .then((result) => {});

        const callBackVal = {
          is_add_soal: true,
        };
        this.props.parentCallBack(callBackVal);
      } else {
        swal
          .fire({
            title: "Ada Kesalahan, Mohon Periksa File Template",
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {});
        var modal = document.getElementById("exampleModal");
        document.getElementById("backdrop").style.display = "block";
        document.getElementById("exampleModal").style.display = "block";
        document.getElementById("exampleModal").classList.add("show");
      }
    } else {
      swal
        .fire({
          title: "Belum Ada Template yang Diimport",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          errors["template"] = "File Belum Diupload";
          this.setState({
            errors,
          });
        });
    }
  }

  kembaliAction(e) {
    e.preventDefault();
    const callBackVal = {
      is_add_soal: false,
      is_tambah_soal: false,
      is_import_soal: false,
    };
    this.props.parentCallBack(callBackVal);
  }

  setOption(key, jawaban, key_gambar_jawaban) {
    if (typeof jawaban !== "undefined") {
      const optionJawaban = {
        key: key,
        option: jawaban,
        key_image: key_gambar_jawaban,
        color: false,
      };
      return optionJawaban;
    } else {
      return false;
    }
  }

  render() {
    return (
      <div>
        <div className="highlight bg-light-primary mt-7">
          <div className="col-lg-12 mb-7 fv-row text-primary">
            <h5 className="text-primary fs-5">Panduan</h5>
            <p className="text-primary">
              Sebelum melakukan import soal, mohon untuk membaca panduan berikut
              :
            </p>
            <ul>
              <li>
                Silahkan unduh template untuk melakukan import pada link berikut{" "}
                <a
                  href={
                    process.env.REACT_APP_BASE_API_URI +
                    "/download/get-file?path=template_soal_trivia.xlsx&disk=dts-storage-sitemanagement"
                  }
                  className="btn btn-primary fw-semibold btn-sm py-1 px-2"
                >
                  <i className="las la-cloud-download-alt fw-semibold me-1" />
                  Download Template
                </a>
              </li>
              <li>
                Jika anda ingin membuat soal yang terdapat gambar, pastikan nama
                file gambar sesuai dengan yang di-input pada template
              </li>
              <li>ID Tipe Soal harus terisi</li>
            </ul>
          </div>
        </div>
        <div className="row mt-7">
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">Upload Template</label>
            <input
              type="file"
              className="form-control form-control-sm mb-2"
              name="upload_template"
              accept=".xlsx"
              onChange={this.onChangeTemplate}
            />
            <small className="text-muted">
              Format File (.xlsx), Max 10240 Kb
            </small>
            <br />
            <span style={{ color: "red" }}>
              {this.state.errors["template"]}
            </span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label">Upload Images</label>
            <input
              type="file"
              className="form-control form-control-sm mb-2"
              name="upload gambar"
              multiple
              accept="image/png, image/gif, image/jpeg"
              onChange={this.onChangeImages}
            />
            <small className="text-muted">
              Format File (.png, .jpg, .gif), Max 10240 Kb
            </small>
            <br />
          </div>
        </div>
        <div className="row">{this.state.fileTemplateImages}</div>

        <div className="text-center my-7">
          <a onClick={this.kembali} className="btn btn-light btn-md me-6">
            <i className="bi bi-list me-1"></i>List Soal
          </a>
          <a onClick={this.importTemplate} className="btn btn-success btn-md">
            <i className="bi bi-cloud-download me-1"></i>Import Template
          </a>
        </div>
        <div
          class="modal fade"
          id="exampleModal"
          tabIndex="-1"
          aria-labelledby="exampleModalLabel"
          aria-modal="true"
          role="dialog"
        >
          <div class="modal-dialog modal-lg" role="document">
            <ImportReport failed={this.state.failed} />
          </div>
        </div>
        <div
          class="modal-backdrop fade show"
          id="backdrop"
          style={{ display: "none" }}
        ></div>
      </div>
    );
  }
}

function Gambar(props) {
  const [valFileName, setFileName] = useState(props.fileName);
  const [valKey, setKey] = useState(props.index);

  function handleClickDelete(event) {
    props.onClickDelete(event.currentTarget.getAttribute("index"));
  }

  return (
    <div key={valKey} className="col-lg-12 mb-7 fv-row">
      <div
        className="btn btn-sm btn-icon btn-light-danger"
        title="Hapus file"
        onClick={handleClickDelete}
        index={valKey}
      >
        <span className="las la-trash-alt" />
      </div>
      <span>{valFileName}</span>
    </div>
  );
}
