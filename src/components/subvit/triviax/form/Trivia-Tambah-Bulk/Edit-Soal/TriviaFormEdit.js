import React from "react";
import ObjectiveForm from "./ObjectiveForm";
import swal from "sweetalert2";
import imageCompression from "browser-image-compression";

const resizeFile = async (file) => {
  const options = {
    maxSizeMB: 0.2,
    maxWidthOrHeight: 300,
    useWebWorker: true,
  };

  return imageCompression(file, options);
};

export default class TriviaFormEdit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      fields: {},
      is_next_soal: true,
      choices: [],
      no_soal: props.id_soal,
      tipe: "polling",
      pertanyaan: "",
      key_gambar_pertanyaan: null,
      datax: {},
      id_soal: props.id_soal,
      kunci_jawaban: "",
      duration: "",
      current_image_name: "",
    };
    this.abjad = ["A", "B", "C", "D", "E", "F", "G"];
    this.handleSimpanKembali = this.handleSimpanKembaliAction.bind(this);
    this.handleKembali = this.handleKembaliAction.bind(this);
    this.handleChangePertanyaan = this.handleChangePertanyaanAction.bind(this);
    this.handleChangeGambar = this.handleChangeGambarAction.bind(this);
    this.handleCallBackObjective =
      this.handleCallBackObjectiveAction.bind(this);
    this.handleChangeDurasi = this.handleChangeDurasiAction.bind(this);
  }

  componentDidMount() {
    const datax = JSON.parse(localStorage.getItem(this.state.id_soal));

    let imageName = null;
    let imageWrapper = document.getElementById(datax.key_gambar_pertanyaan);
    if (imageWrapper) {
      imageWrapper = imageWrapper.value;
      const splitImage = imageWrapper.split("_");
      imageName = splitImage[0];
    }

    if (datax) {
      this.setState({
        datax: datax,
        pertanyaan: datax.pertanyaan,
        no_soal: datax.nosoal,
        kunci_jawaban: datax.kunci_jawaban,
        duration: datax.duration,
        current_image_name: imageName,
      });
    }
  }

  handleKembaliAction() {
    const callBackVal = {
      is_edit_soal: false,
    };
    this.props.parentCallBack(callBackVal);
  }

  handleSimpanKembaliAction() {
    //check error
    const allJawaban = Array.from(
      document.getElementsByClassName("jawaban_edit"),
    );
    const arrError = [];

    for (let i = 0; i < allJawaban.length; i++) {
      let valueJawaban = allJawaban[i].getAttribute("value");
      let no_soal = allJawaban[i].getAttribute("no_soal");
      let index = allJawaban[i].getAttribute("index");
      if (valueJawaban == "") {
        const err = {
          no_soal: no_soal,
          index: index,
          message: "Jawaban Tidak Boleh Kosong",
        };
        arrError.push(err);
      }
    }
    let is_error = false;
    const allError = Array.from(document.getElementsByClassName("error_edit"));
    for (let i = 0; i < allError.length; i++) {
      allError[i].textContent = "";
      let no_soal = allError[i].getAttribute("no_soal");
      let index = allError[i].getAttribute("index");
      for (let j = 0; j < arrError.length; j++) {
        if (arrError[j].no_soal == no_soal && arrError[j].index == index) {
          allError[i].textContent = arrError[j].message;
          this.setState({ is_next_soal: false });
          is_error = true;
        }
      }
    }
    let textRadio = "";
    if (this.state.kunci_jawaban == "") {
      textRadio = "Belum Memilih Kunci Jawaban";
      is_error = true;
    }
    const allRadioCheck = Array.from(
      document.getElementsByClassName("error_kunci"),
    );
    for (let i = 0; i < allRadioCheck.length; i++) {
      allRadioCheck[i].textContent = textRadio;
    }

    let errors = [];
    if (this.state.pertanyaan == "") {
      errors["pertanyaan"] = "Tidak Boleh Kosong";
      is_error = true;
    } else {
      errors["pertanyaan"] = "";
    }

    if (this.state.duration == "" || this.state.duration == 0) {
      errors["duration"] = "Tidak Boleh Kosong";
      is_error = true;
    } else {
      errors["duration"] = "";
    }

    this.setState({ errors: errors });

    //build js untuk insert
    //kembalikan kondisi pilihan multiple choice ke kosong
    if (!is_error) {
      const soal = {
        nosoal: this.state.no_soal,
        pertanyaan: this.state.pertanyaan,
        key_gambar_pertanyaan: this.state.no_soal,
        tipe: this.state.tipe,
        duration: parseInt(this.state.duration),
        kunci_jawaban: isNaN(this.state.kunci_jawaban)
          ? this.state.kunci_jawaban
          : this.abjad[this.state.kunci_jawaban],
        jawaban: [],
      };
      if (this.state.tipe == "polling") {
        const datax = JSON.parse(localStorage.getItem(this.state.tipe + "_"));
        const jawaban = [];
        for (let i = 0; i < datax.length; i++) {
          if (datax[i].no == this.state.no_soal) {
            const keyImage = this.state.no_soal + "_" + datax[i].index;
            const option = {
              key: this.abjad[datax[i].index],
              option: datax[i].jawaban,
              key_image: keyImage,
              color: false,
            };
            jawaban.push(option);
          }
        }
        soal.jawaban = jawaban;
      }

      localStorage.setItem(this.state.no_soal, JSON.stringify(soal));
      const callBackVal = {
        is_edit_soal: false,
        is_tambah_soal: false,
      };
      swal
        .fire({
          title: "Soal Berhasil Disimpan",
          icon: "success",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          this.props.parentCallBack(callBackVal);
        });
    }
  }

  handleChangePertanyaanAction(e) {
    e.preventDefault();
    let errors = this.state.errors;
    errors["pertanyaan"] = "";
    this.setState({
      pertanyaan: e.currentTarget.value,
      errors,
    });
  }

  handleChangeDurasiAction(e) {
    e.preventDefault();
    let errors = this.state.errors;
    errors["duration"] = "";
    let duration = e.currentTarget.value;
    console.log(duration);
    if (duration < 0) {
      duration = 0;
    }
    this.setState({
      duration: duration,
      errors,
    });
  }

  handleChangeGambarAction(e) {
    const imageFile = e.target.files[0];
    const reader = new FileReader();
    const keyImage = this.state.no_soal;
    this.setState({ key_gambar_pertanyaan: keyImage });
    resizeFile(imageFile).then((cf) => {
      reader.readAsDataURL(cf);
      reader.onload = function () {
        let result = reader.result;
        let fileName = imageFile.name;
        let wrapper = document.getElementById("temp_image");
        let check = document.getElementById(keyImage);
        if (check) {
          check.value = fileName + "_" + result;
        } else {
          const hiddenWrapper = document.createElement("input");
          hiddenWrapper.value = fileName + "_" + result;
          hiddenWrapper.id = keyImage;
          hiddenWrapper.className = "imagePertanyaan_edit";
          hiddenWrapper.type = "hidden";
          wrapper.appendChild(hiddenWrapper);
        }
      };
    });
  }

  handleCallBackObjectiveAction(e) {
    this.setState(
      {
        kunci_jawaban: e.kunci_jawaban,
      },
      () => {
        console.log(e.kunci_jawaban);
      },
    );
  }

  render() {
    return (
      <>
        <h5 className="mt-7 mb-5 me-3">Soal {this.state.no_soal}</h5>
        <div className="row">
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">Pertanyaan</label>
            <input
              className="form-control form-control-sm"
              placeholder="Masukkan pertanyaan"
              name="pertanyaan"
              value={this.state.pertanyaan}
              onChange={this.handleChangePertanyaan}
            />
            <span style={{ color: "red" }}>
              {this.state.errors["pertanyaan"]}
            </span>
          </div>
          <div className="col-lg-12">
            <div className="mb-7 fv-row">
              <label className="form-label">
                {this.state.current_image_name != null &&
                this.state.current_image_name != "" ? (
                  <div>
                    Ganti Gambar{" "}
                    <span className="text-danger">
                      {this.state.current_image_name}
                    </span>
                  </div>
                ) : (
                  "Gambar Jawaban (Optional)"
                )}
              </label>
              <input
                id="upload_image_form"
                type="file"
                className="form-control form-control-sm mb-2"
                name="upload_silabus"
                accept="image/*"
                onChange={this.handleChangeGambar}
                value={this.state.gambar}
              />
              <small className="text-muted">
                Format File (.jpg/.jpeg/.png/.svg)
              </small>
              <br />
              <span style={{ color: "red" }}>
                {this.state.errors["upload_silabus"]}
              </span>
            </div>
          </div>

          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">Durasi (Detik)</label>
            <input
              className="form-control form-control-sm"
              placeholder="Masukkan Durasi"
              type="number"
              min={0}
              name="duration"
              value={this.state.duration}
              onChange={this.handleChangeDurasi}
            />
            <span style={{ color: "red" }}>
              {this.state.errors["duration"]}
            </span>
          </div>

          <ObjectiveForm
            id_soal={this.state.id_soal}
            choices={this.state.choices}
            no_soal={this.state.no_soal}
            parentCallBack={this.handleCallBackObjective}
          />

          <div className="text-center my-7">
            <a
              onClick={this.handleKembali}
              className="btn btn-light btn-md me-6"
            >
              Batal
            </a>
            <a
              onClick={this.handleSimpanKembali}
              className="btn btn-success btn-md "
            >
              <i className="fa fa-paper-plane ms-1"></i>Simpan Soal
            </a>
          </div>
        </div>
      </>
    );
  }
}
