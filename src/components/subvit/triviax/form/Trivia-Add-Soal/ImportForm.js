import React, { useState } from "react";
import imageCompression from "browser-image-compression";
import { read, utils } from "xlsx";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import axios from "axios";
import ImportReport from "../../../survey/Import-Report";

const resizeFile = async (file) => {
  const options = {
    maxSizeMB: 0.2,
    maxWidthOrHeight: 300,
    useWebWorker: true,
  };

  return imageCompression(file, options);
};

export default class ImportForm extends React.Component {
  constructor(props) {
    super(props);
    this.onChangeTemplate = this.onChangeTemplateAction.bind(this);
    this.onChangeImages = this.onChangeImagesAction.bind(this);
    this.handleKembali = this.handleKembaliAction.bind(this);
    this.importTemplate = this.importTemplateAction.bind(this);
    this.handleCallBackDelete = this.handleCallBackDeleteAction.bind(this);
    this.state = {
      dataTemplate: [],
      fileTemplateImages: [],
      tipe: "polling",
      id_trivia: 0,
      last_number: 0,
      datax: [],
      level: "",
      status_trivia: "",
      is_uploaded: false,
      errors: {},
      success: {},
      failed: [],
    };
    this.abjad = ["A", "B", "C", "D", "E", "F", "G"];
  }

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    const temp_storage = localStorage.getItem("dataMenus");
    localStorage.clear();
    localStorage.setItem("dataMenus", temp_storage);
    let segment_url = window.location.pathname.split("/");
    let id_trivia = segment_url[4];
    this.setState({ id_trivia });

    //ambil data current trivia untuk dikirim lagi

    let dataBody = {
      id_trivia: id_trivia,
      start: 0,
      rows: 100,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list_soal_trivia_byid",
        dataBody,
        this.configs,
      )
      .then((res) => {
        this.setState({ loading: false });
        const datax = res.data.result.Data;
        this.setState({ datax });
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statusx) {
          let last_number = 0;
          datax.forEach(function (element, i) {
            const soal = {
              nosoal: i + 1,
              tipe: element.type,
              pertanyaan: element.pertanyaan,
              gambar_pertanyaan: element.pertanyaan_gambar,
              jawaban: JSON.parse(element.jawaban),
              kunci_jawaban: element.kunci_jawaban,
              duration: element.duration,
              status: 1,
              id_user: Cookies.get("user_id"),
            };
            localStorage.setItem(i + 1, JSON.stringify(soal));
            last_number = i + 1;
          });
          this.setState({ last_number });
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.handleReload();
              }
            });
        }
      })
      .catch((error) => {
        console.log(error);
        this.setState({ loading: false });
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  onChangeImagesAction(e) {
    const tempImage = [];
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    const length_file = e.target.files.length;
    e.target.files.forEach(
      function (element, i) {
        resizeFile(element).then((cf) => {
          if (length_file - 1 == i) {
            swal.close();
          }
          let reader = new FileReader();
          reader.readAsDataURL(cf);
          const context = this;
          reader.onload = function () {
            let result = reader.result;
            let fileName = element.name;
            let wrapperHTML = document.getElementById("temp_image");

            let check = document.getElementById(fileName);
            if (check) {
              check.value = fileName + "_" + result;
            } else {
              const wrapper = (
                <Gambar
                  fileName={fileName}
                  key={i}
                  index={i}
                  onClickDelete={context.handleCallBackDelete}
                />
              );
              tempImage.push(wrapper);

              const hiddenWrapper = document.createElement("input");
              hiddenWrapper.value = fileName + "_" + result;
              hiddenWrapper.imageName = fileName;
              hiddenWrapper.id = fileName;
              hiddenWrapper.className = "imageImport";
              hiddenWrapper.type = "hidden";
              wrapperHTML.appendChild(hiddenWrapper);
            }
            context.setState({ fileTemplateImages: tempImage });
          };
        });
      }.bind(this),
    );
  }

  handleCallBackDeleteAction(e) {
    const index = e;
    const newList = this.state.fileTemplateImages.filter(
      (item) => item.key !== index,
    );
    this.setState({ fileTemplateImages: newList });
  }

  onChangeTemplateAction(e) {
    const errors = this.state.errors;
    errors["template"] = "";
    this.setState({
      errors,
    });
    const [file] = e.target.files;
    const reader = new FileReader();

    reader.onload = (evt) => {
      const bstr = evt.target.result;
      const wb = read(bstr, { type: "binary" });
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      const data = utils.sheet_to_json(ws, { header: 1 });
      this.setState({
        dataTemplate: data,
        is_uploaded: true,
      });
    };

    reader.readAsBinaryString(file);
  }

  buildLocalStorage(datax, context) {
    let is_error = false;
    let temp_i = 0;
    let arr_soal = [];
    datax.forEach(function (element, i) {
      //skip index ke 0 karena header
      if (i != 0 && element[0] != "" && element[1] != "" && element[2] != "") {
        const soal = {
          nosoal: i,
          pertanyaan: element[1],
          key_gambar_pertanyaan: element[2],
          duration: element[3],
          kunci_jawaban: element[4],
          tipe: context.state.tipe,
        };

        let jawaban = [];

        jawaban = [
          context.setOption("A", element[5], element[6]),
          context.setOption("B", element[7], element[8]),
          context.setOption("C", element[9], element[10]),
          context.setOption("D", element[11], element[12]),
          context.setOption("E", element[13], element[14]),
          context.setOption("F", element[15], element[16]),
        ];

        const removeJawabanFalse = jawaban.filter(function (obj) {
          return obj !== false;
        });

        jawaban = jawaban.filter(function (e) {
          return e !== false;
        });

        if (!soal.nosoal) {
          const err = {
            no: "",
            soal: soal.pertanyaan,
            kesalahan: "Nomor Belum Terisi",
          };
          const failed = context.state.failed;
          failed.push(err);
          context.setState({ failed });
          is_error = true;
        }

        if (!soal.pertanyaan) {
          const err = {
            no: soal.nosoal,
            soal: "",
            kesalahan: "Soal Belum Terisi",
          };
          const failed = context.state.failed;
          failed.push(err);
          context.setState({ failed });
          is_error = true;
        }

        if (!soal.duration) {
          const err = {
            no: soal.nosoal,
            soal: soal.pertanyaan,
            kesalahan: "Durasi Belum Terisi",
          };
          const failed = context.state.failed;
          failed.push(err);
          context.setState({ failed });
          is_error = true;
        }

        if (!soal.kunci_jawaban) {
          const err = {
            no: soal.nosoal,
            soal: soal.pertanyaan,
            kesalahan: "Kunci Jawaban Belum Terisi",
          };
          const failed = context.state.failed;
          failed.push(err);
          context.setState({ failed });
          is_error = true;
        }

        if (removeJawabanFalse.length < 2) {
          const err = {
            no: soal.nosoal,
            soal: soal.pertanyaan,
            kesalahan: "Pilihan Objective Minimal 2",
          };
          const failed = context.state.failed;
          failed.push(err);
          context.setState({ failed });
          is_error = true;
        }

        let key_is_valid = false;
        for (let i = 0; i < jawaban.length; i++) {
          if (jawaban[i].key == soal.kunci_jawaban) {
            key_is_valid = true;
          }
        }

        if (!key_is_valid) {
          const err = {
            no: soal.nosoal,
            soal: soal.pertanyaan,
            kesalahan: 'Kunci Jawaban "' + soal.kunci_jawaban + '" Tidak Valid',
          };
          const failed = context.state.failed;
          failed.push(err);
          context.setState({ failed });
          is_error = true;
        }

        soal.jawaban = jawaban;
        localStorage.setItem(soal.nosoal, JSON.stringify(soal));
        arr_soal.push(soal);
      }
    });
    if (is_error) {
      for (let i = 0; i < arr_soal.length; i++) {
        localStorage.removeItem(arr_soal[i].nosoal);
      }
      this.setState({ is_uploaded: false });
      return false;
    } else {
      return true;
    }
  }

  checkTemplate(datax) {
    //check template sesuai tidak
    if (
      datax[0][0] != "No" ||
      datax[0][1] != "Pertanyaan" ||
      datax[0][2] != "Nama Gambar Pertanyaan" ||
      datax[0][3] != "Durasi" ||
      datax[0][4] != "Kunci Jawaban"
    ) {
      return false;
    } else {
      return true;
    }
  }

  importTemplateAction(e) {
    e.preventDefault();
    const errors = this.state.errors;

    if (this.state.is_uploaded) {
      errors["template"] = "";
      this.setState({
        errors,
      });

      if (!this.checkTemplate(this.state.dataTemplate)) {
        const errors = this.state.errors;
        swal
          .fire({
            title:
              "Template Tidak Sesuai, Pastikan Menggunakan Template Trivia",
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            errors["template"] = "Template Tidak Sesuai";
            this.setState({
              errors,
            });
          });
      } else if (this.buildLocalStorage(this.state.dataTemplate, this)) {
        const data = {
          id: this.state.id_trivia,
        };
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/trivia_select_byid",
            data,
            this.configs,
          )
          .then((res) => {
            const statux = res.data.result.Status;
            const messagex = res.data.result.Message;
            if (statux) {
              const datax = res.data.result.trivia[0];
              const nama_trivia = datax.nama_trivia;
              const question_to_share = datax.jml_soal;
              const duration = datax.duration;
              const id_status_peserta = datax.status;
              const id_user = Cookies.get("user_id");
              const start_at = datax.start_at;
              const end_at = datax.end_at;
              const soal_json = this.buildSoalJson();
              const update = [
                {
                  id: this.state.id_trivia,
                  start_at: start_at,
                  end_at: end_at,
                  level: datax.level,
                  questions_to_share: question_to_share,
                  duration: duration,
                  id_status_peserta: id_status_peserta,
                  nama_trivia: nama_trivia,
                  status: datax.status,
                  id_user: id_user,
                  soal_json: soal_json,
                },
              ];

              axios
                .post(
                  process.env.REACT_APP_BASE_API_URI + "/submit_update_trivia",
                  JSON.stringify(update),
                  this.configs,
                )
                .then((res) => {
                  const statux = res.data.result.Status;
                  const messagex = res.data.result.Message;
                  if (statux) {
                    swal
                      .fire({
                        title: messagex,
                        icon: "success",
                        confirmButtonText: "Ok",
                        allowOutsideClick: false,
                      })
                      .then((result) => {
                        if (result.isConfirmed) {
                          window.location =
                            "/subvit/trivia/list-soal/" + this.state.id_trivia;
                        }
                      });
                  } else {
                    swal
                      .fire({
                        title: messagex,
                        icon: "warning",
                        confirmButtonText: "Ok",
                      })
                      .then((result) => {
                        if (result.isConfirmed) {
                        }
                      });
                  }
                })
                .catch((error) => {
                  let statux = error.response.data.result.Status;
                  let messagex = error.response.data.result.Message;
                  if (!statux) {
                    swal
                      .fire({
                        title: messagex,
                        icon: "warning",
                        confirmButtonText: "Ok",
                      })
                      .then((result) => {
                        if (result.isConfirmed) {
                        }
                      });
                  }
                });
            } else {
              swal
                .fire({
                  title: messagex,
                  icon: "warning",
                  confirmationButtonText: "Ok",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                  }
                });
            }
            swal.close();
          })
          .catch((error) => {
            console.log(error);
            swal.close();
            let statux = error.response.data.result.Status;
            let messagex = error.response.data.result.Message;
            if (!statux) {
              swal
                .fire({
                  title: messagex,
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                  }
                });
            }
          });

        swal
          .fire({
            title: "Soal Berhasil Disimpan",
            icon: "success",
            confirmButtonText: "Ok",
          })
          .then((result) => {});
      } else {
        //console.log(this.state.failed);
        swal
          .fire({
            title: "Ada Kesalahan, Mohon Periksa File Template",
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {});
        var modal = document.getElementById("exampleModal");
        document.getElementById("backdrop").style.display = "block";
        document.getElementById("exampleModal").style.display = "block";
        document.getElementById("exampleModal").classList.add("show");
      }
    } else {
      swal
        .fire({
          title: "Belum Ada Template yang Diimport",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          errors["template"] = "File Belum Diupload";
          this.setState({
            errors,
          });
        });
    }
  }

  buildSoalJson() {
    const soal_json = [];
    const status = 1;
    for (let i = 0; i < localStorage.length + 1; i++) {
      const soal = JSON.parse(localStorage.getItem(i));
      if (soal) {
        soal["status"] = status;
        soal["nosoal"] = i + 1;
        soal["id_user"] = Cookies.get("user_id");
        this.setState({
          last_number: soal["nosoal"],
        });
        //ambil image yang di tempel di html untuk pertanyaan
        const key_gambar_pertanyaan = soal.key_gambar_pertanyaan;
        let imagePertanyaanWrapper = document.getElementById(
          key_gambar_pertanyaan,
        );
        let gambar_pertanyaan = null;
        if (imagePertanyaanWrapper) {
          imagePertanyaanWrapper = imagePertanyaanWrapper.value;
          const splitImagePertanyaan = imagePertanyaanWrapper.split("_");
          gambar_pertanyaan = splitImagePertanyaan[1];
        }
        delete soal.key_gambar_pertanyaan;
        soal["gambar_pertanyaan"] = gambar_pertanyaan;

        //ambil image yang di tempel di html untuk jawaban
        if (soal.tipe != "pertanyaan_terbuka") {
          soal.jawaban.forEach(function (element) {
            let image = null;
            let imageName = null;
            let imageWrapper = document.getElementById(element.key_image);
            if (imageWrapper) {
              imageWrapper = imageWrapper.value;
              const splitImage = imageWrapper.split("_");
              imageName = splitImage[0];
              image = splitImage[1];
            }
            delete element.key_image;
            element.image = image;
            element.imageName = imageName;
          });
        }
        soal_json.push(soal);
      }
    }
    return soal_json;
  }

  setOption(key, jawaban, key_gambar_jawaban) {
    if (typeof jawaban !== "undefined") {
      const optionJawaban = {
        key: key,
        option: jawaban,
        key_image: key_gambar_jawaban,
        color: false,
      };
      return optionJawaban;
    } else {
      return false;
    }
  }

  handleKembaliAction() {
    window.location = "/subvit/trivia/list-soal/" + this.state.id_trivia;
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <path
                        opacity="0.3"
                        d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                        fill="#7239ea"
                      ></path>
                      <path
                        d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                        fill="#7239ea"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Subvit
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Trivia
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <a
                  onClick={this.handleKembali}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Import Soal Trivia
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <div>
                        <div className="highlight bg-light-primary mt-7">
                          <div className="col-lg-12 mb-7 fv-row text-primary">
                            <h5 className="text-primary fs-5">Panduan</h5>
                            <p className="text-primary">
                              Sebelum melakukan import soal, mohon untuk membaca
                              panduan berikut :
                            </p>
                            <ul>
                              <li>
                                Silahkan unduh template untuk melakukan import
                                pada link berikut{" "}
                                <a
                                  href={
                                    process.env.REACT_APP_BASE_API_URI +
                                    "/download/get-file?path=template_soal_trivia.xlsx&disk=dts-storage-sitemanagement"
                                  }
                                  className="btn btn-primary fw-semibold btn-sm py-1 px-2"
                                >
                                  <i className="las la-cloud-download-alt fw-semibold me-1" />
                                  Download Template
                                </a>
                              </li>
                              <li>
                                Jika anda ingin membuat soal yang terdapat
                                gambar, pastikan nama file gambar sesuai dengan
                                yang di-input pada template
                              </li>
                              <li>ID Tipe Soal harus terisi</li>
                            </ul>
                          </div>
                        </div>

                        <div className="row mt-7">
                          <div className="col-lg-12 mb-7 fv-row">
                            <label className="form-label required">
                              Upload Template
                            </label>
                            <input
                              type="file"
                              className="form-control form-control-sm mb-2"
                              name="upload_template"
                              accept=".xlsx"
                              onChange={this.onChangeTemplate}
                            />
                            <small className="text-muted">
                              Format File (.xlsx), Max 10240 Kb
                            </small>
                            <br />
                            <span style={{ color: "red" }}>
                              {this.state.errors["template"]}
                            </span>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-lg-12 mb-7 fv-row">
                            <label className="form-label">Upload Images</label>
                            <input
                              type="file"
                              className="form-control form-control-sm mb-2"
                              name="upload gambar"
                              multiple
                              accept="image/png, image/gif, image/jpeg"
                              onChange={this.onChangeImages}
                            />
                            <small className="text-muted">
                              Format File (.png, .jpg, .gif), Max 10240 Kb
                            </small>
                            <br />
                          </div>
                        </div>
                        <div className="row">
                          {this.state.fileTemplateImages}
                        </div>
                        <div className="text-center my-7">
                          <a
                            onClick={this.handleKembali}
                            className="btn btn-light btn-md me-6"
                          >
                            Batal
                          </a>
                          <a
                            onClick={this.importTemplate}
                            className="btn btn-success btn-md"
                          >
                            <i className="bi bi-cloud-download me-1"></i>Import
                            Template
                          </a>
                        </div>
                      </div>
                    </div>
                    <div id="temp_image"></div>
                    <div
                      class="modal fade"
                      id="exampleModal"
                      tabIndex="-1"
                      aria-labelledby="exampleModalLabel"
                      aria-modal="true"
                      role="dialog"
                    >
                      <div class="modal-dialog modal-lg" role="document">
                        <ImportReport failed={this.state.failed} />
                      </div>
                    </div>
                    <div
                      class="modal-backdrop fade show"
                      id="backdrop"
                      style={{ display: "none" }}
                    ></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function Gambar(props) {
  const [valFileName, setFileName] = useState(props.fileName);
  const [valKey, setKey] = useState(props.index);
  function handleClickDelete(event) {
    props.onClickDelete(event.currentTarget.getAttribute("index"));
  }
  return (
    <div key={valKey} className="col-lg-12 mb-7 fv-row">
      <div
        className="btn btn-sm btn-icon btn-light-danger"
        title="Hapus file"
        onClick={handleClickDelete}
        index={valKey}
      >
        <span className="las la-trash-alt" />
      </div>
      <span>{valFileName}</span>
    </div>
  );
}
