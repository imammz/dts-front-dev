import React from "react";
import Header from "../../../../components/Header";
import Content from "../Survey-Clone";
import SideNav from "../../../../components/SideNav";
import Footer from "../../../../components/Footer";

const Clone = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default Clone;
