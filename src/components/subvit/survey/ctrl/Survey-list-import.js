import React from "react";
import Header from "../../../../components/Header";
import Breadcumb from "../../../../components/Breadcumb";
import Content from "../form/Survey-Add-Soal/ImportForm";
import SideNav from "../../../../components/SideNav";
import Footer from "../../../../components/Footer";

const SurveyListImport = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default SurveyListImport;
