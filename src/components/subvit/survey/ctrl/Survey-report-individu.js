import React from "react";
import Header from "../../../../components/Header";
import Breadcumb from "../../../../components/Breadcumb";
import Content from "../Report-Individu";
import SideNav from "../../../../components/SideNav";
import Footer from "../../../../components/Footer";

const SurveyReportIndividu = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default SurveyReportIndividu;
