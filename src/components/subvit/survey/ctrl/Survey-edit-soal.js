import React from "react";
import Header from "../../../../components/Header";
import Breadcumb from "../../../../components/Breadcumb";
import Content from "../Survey-Edit-Soal";
import SideNav from "../../../../components/SideNav";
import Footer from "../../../../components/Footer";

const SurveyEditSoal = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default SurveyEditSoal;
