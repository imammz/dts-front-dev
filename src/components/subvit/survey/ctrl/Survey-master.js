import React from "react";
import Header from "../../../../components/Header";
import Breadcumb from "../../../../components/Breadcumb";
import Content from "../Survey-Master-Content";
import SideNav from "../../../../components/SideNav";
import Footer from "../../../../components/Footer";

const SurveyMaster = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default SurveyMaster;
