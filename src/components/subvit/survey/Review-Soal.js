import React, { useState } from "react";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";

export default class ReviewSoal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      soal: props.soal,
      pertanyaan: props.soal.pertanyaan,
      no: props.no,
      jawaban: props.soal.jawaban,
      field_jawaban: false,
    };
  }

  componentDidMount() {
    this.handleReload();
  }

  handleReload() {
    if (this.state.soal.type == "objective") {
      const field_jawaban = [];
      const jawaban = JSON.parse(this.state.jawaban);

      jawaban.forEach(function (element, i) {
        const choices = (
          <MultipleChoice
            key={i}
            abjad={element.key}
            option={element.option}
            image={element.image}
          />
        );
        field_jawaban.push(choices);
      });
      this.setState({ field_jawaban });
    } else if (this.state.soal.type == "multiple_choice") {
      const field_jawaban = [];
      const jawaban = JSON.parse(this.state.jawaban);

      jawaban.forEach(function (element, i) {
        const choices = (
          <MultipleCheck
            key={i}
            abjad={element.key}
            option={element.option}
            image={element.image}
          />
        );
        field_jawaban.push(choices);
      });
      this.setState({ field_jawaban });
    } else if (this.state.soal.type == "pertanyaan_terbuka") {
      const field_jawaban = <Terbuka />;
      this.setState({ field_jawaban });
    } else if (this.state.soal.type == "triggered_question") {
      const field_jawaban = [];
      const jawaban = JSON.parse(this.state.jawaban);
      console.log(jawaban);
      jawaban.forEach(function (element, i) {
        const choices = (
          <MultipleChoiceTrigger
            sub={element.sub ? element.sub : []}
            indexSub={0}
            key={i}
            abjad={element.key}
            image={element.image}
            option={element.option}
          />
        );
        field_jawaban.push(choices);
      });
      this.setState({ field_jawaban });
    } else if (this.state.soal.type == "skala") {
      const field_jawaban = [];
      const jawaban = JSON.parse(this.state.jawaban);
      const num_choices = jawaban.length;
      const valueAbjad = [
        "0",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10",
        "11",
        "12",
        "13",
        "14",
        "15",
      ];
      const choices = [];
      for (let i = 0; i < num_choices; i++) {
        choices.push(<Skala abjad={valueAbjad[i]} key={i} />);
      }
      this.setState({ field_jawaban: choices });
    }
  }

  render() {
    return (
      <div>
        <div className="mb-7">
          {this.state.soal.pertanyaan_gambar != null &&
          this.state.soal.pertanyaan_gambar != "null" ? (
            <div>
              <h5>{this.state.no}.</h5>
              <img
                className="w-100 rounded mt-3 mb-3"
                src={this.state.soal.pertanyaan_gambar}
              />
              <br />
              <h5>{this.state.pertanyaan}</h5>
            </div>
          ) : (
            <h5>
              {this.state.no}. {this.state.pertanyaan}
            </h5>
          )}
        </div>
        {this.state.soal.type == "skala" ? (
          <div>
            {this.state.field_jawaban}
            <div className="col-lg-12 row">
              <div className="col-lg-6 text-start">Tidak Merekomendasikan</div>
              <div className="col-lg-6 text-end">Sangat Merekomendasikan</div>
            </div>
          </div>
        ) : (
          <div>{this.state.field_jawaban}</div>
        )}
      </div>
    );
  }
}

function MultipleChoiceTrigger(props) {
  const [valKey, setValKey] = useState(props.abjad);
  const [child, setChild] = useState(false);
  const [valOption, setValOption] = useState(props.option);
  const [sub, setSub] = useState(props.sub);
  const [pertanyaan, setPertanyaan] = useState("");
  let [indexSub, setIndexSub] = useState(props.indexSub);
  const [noChild, setNoChild] = useState(false);
  const [fieldJawaban, setFieldJawaban] = useState([]);
  const [fieldChildTrigger, setChildTrigger] = useState([]);
  const [nextChild, setNextChild] = useState([]);
  const [valImage, setValImage] = useState(props.image);

  function handleClick() {
    setChild(true);
    if (sub && sub[indexSub].answer) {
      const field_jawaban = [];
      const jawaban = sub[indexSub].answer;
      jawaban.forEach(function (element, i) {
        const choices = (
          <MultipleChoiceChild
            key={i}
            abjad={element.key}
            option={element.option}
            onClick={handleNext}
            image={element.image}
          />
        );
        field_jawaban.push(choices);
      });
      setChildTrigger([
        <ChildTrigger
          key={sub[indexSub].key}
          no={sub[indexSub].key}
          pertanyaan={sub[indexSub].question}
          fieldJawaban={field_jawaban}
          image={sub[indexSub].image}
        />,
      ]);
      setIndexSub(indexSub++);
    }
  }

  /* useEffect(() => {
        setChildTrigger([...fieldChildTrigger, );             
    }, [fieldJawaban]) */

  function handleNext() {
    if (indexSub < sub.length) {
      const field_jawaban = [];
      const jawaban = sub[indexSub].answer;
      jawaban.forEach(function (element, i) {
        const choices = (
          <MultipleChoiceChild
            key={i}
            abjad={element.key}
            option={element.option}
            onClick={handleNext}
          />
        );
        field_jawaban.push(choices);
      });
      setChildTrigger([
        ...fieldChildTrigger,
        <ChildTrigger
          key={sub[indexSub].key}
          no={sub[indexSub].key}
          image={sub[indexSub].image}
          pertanyaan={sub[indexSub].question}
          fieldJawaban={field_jawaban}
        />,
      ]);
      setIndexSub(indexSub++);
    }
  }

  return (
    <div className="row">
      <div className="col-lg-12 fv-row mb-3">
        {valImage != null ? (
          <a
            href="#"
            onClick={handleClick}
            style={{ width: "100%", border: "solid 1px" }}
            className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder"
            variant="outlined"
          >
            {valKey}.
            <div className="row">
              <div className="col-lg-12">
                <img src={valImage}></img>
              </div>
              <div className="col-lg-12 text-start ms-8">{valOption}</div>
            </div>
          </a>
        ) : (
          <a
            href="#"
            onClick={handleClick}
            style={{ width: "100%", border: "solid 1px" }}
            className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder"
            variant="outlined"
          >
            {valKey}. {valOption}
          </a>
        )}
        {child ? <div>{fieldChildTrigger}</div> : ""}
      </div>
      {nextChild}
    </div>
  );
}

function ChildTrigger(props) {
  const [no, setNo] = useState(props.no);
  const [pertanyaan, setPertanyaan] = useState(props.pertanyaan);
  const [fieldJawaban, setFieldJawaban] = useState(props.fieldJawaban);
  const [valImage, setValImage] = useState(props.image);
  const [indexSub, setIndexSub] = useState(props.indexSub);
  console.log(valImage);
  return (
    <div className="col-lg-12 p-5 m-5 bg-gray-200 rounded-sm">
      <div className="col-lg-12">
        <div className="row mb-3">
          {valImage != null ? (
            <div>
              <h5>{no}.</h5>
              <img className="w-100 rounded mt-3 mb-3" src={valImage} />
              <br />
              <h5>{pertanyaan}</h5>
            </div>
          ) : (
            <h5>
              {no}. {pertanyaan}
            </h5>
          )}
        </div>
        {fieldJawaban}
      </div>
    </div>
  );
}

function MultipleChoiceChild(props) {
  const [valKey, setValKey] = useState(props.abjad);
  const [valOption, setValOption] = useState(props.option);
  const [valImage, setValImage] = useState(props.image);

  function handleClick() {
    props.onClick(true);
  }

  return (
    <div className="row">
      <div className="col-lg-12 fv-row mb-3">
        {valImage ? (
          <a
            href="#"
            onClick={handleClick}
            style={{ width: "100%", border: "solid 1px" }}
            className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder"
            variant="outlined"
          >
            {valKey}.
            <div className="row">
              <div className="col-lg-12">
                <img src={valImage}></img>
              </div>
              <div className="col-lg-12 text-start ms-8">{valOption}</div>
            </div>
          </a>
        ) : (
          <a
            href="#"
            onClick={handleClick}
            style={{ width: "100%", border: "solid 1px" }}
            className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder"
            variant="outlined"
          >
            {valKey}. {valOption}
          </a>
        )}
      </div>
    </div>
  );
}

function MultipleChoice(props) {
  const [valKey, setValKey] = useState(props.abjad);
  const [valOption, setValOption] = useState(props.option);
  const [valImage, setValImage] = useState(props.image);

  return (
    <div className="row">
      <div className="col-lg-12 fv-row mb-3">
        {valImage ? (
          <a
            href="#"
            style={{ width: "100%", border: "solid 1px" }}
            className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder"
            variant="outlined"
          >
            {valKey}.
            <div className="row">
              <div className="col-lg-12">
                <img src={valImage}></img>
              </div>
              <div className="col-lg-12 text-start ms-8">{valOption}</div>
            </div>
          </a>
        ) : (
          <a
            href="#"
            style={{ width: "100%", border: "solid 1px" }}
            className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder"
            variant="outlined"
          >
            {valKey}. {valOption}
          </a>
        )}
      </div>
    </div>
  );
}

function MultipleCheck(props) {
  const [valKey, setValKey] = useState(props.abjad);
  const [valOption, setValOption] = useState(props.option);
  let [checkVal, setCheckVal] = useState(false);
  const [valImage, setValImage] = useState(props.image);

  function handleChange(prev) {
    if (prev.currentTarget.checkVal !== checkVal) {
      setCheckVal(!checkVal);
    }
  }

  return (
    <div className="row">
      <div className="col-lg-12 fv-row mb-3">
        {valImage ? (
          <div className="bg-gray-200 row mb-4 pb-4">
            <div className="col-lg-12">
              <FormControlLabel
                control={
                  <Checkbox
                    checked={checkVal}
                    onChange={handleChange}
                    value={valOption}
                  />
                }
                label={valOption}
              />
            </div>
            <div className="col-lg-12">
              <img src={valImage}></img>
            </div>
          </div>
        ) : (
          <FormControlLabel
            control={
              <Checkbox
                checked={checkVal}
                onChange={handleChange}
                value={valOption}
              />
            }
            label={valOption}
          />
        )}
      </div>
    </div>
  );
}

function Terbuka(props) {
  return (
    <div className="row">
      <textarea name="Text1" cols="40" rows="5"></textarea>
    </div>
  );
}

function Skala(props) {
  return (
    <button className="jawaban btn btn-secondary form-control-sm m-1">
      {props.abjad}
    </button>
  );
}
