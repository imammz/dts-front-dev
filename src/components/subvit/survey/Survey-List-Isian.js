import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import {
  capitalizeTheFirstLetterOfEachWord,
  indonesianDateFormat,
} from "../../publikasi/helper";

export default class SurveyListIsian extends React.Component {
  constructor(props) {
    super(props);
    this.handleKembali = this.handleKembaliAction.bind(this);
  }
  state = {
    datax: [],
    loading: true,
    totalRows: 0,
    newPerPage: 10,
    tempLastNumber: 0,
    currentPage: 0,
    isSearch: false,
    nama_survey: "",
    sort: "c.name asc",
    id_survey: 0,
    columns: [],
    param: "",
  };
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  componentDidMount() {
    const temp_storage = localStorage.getItem("dataMenus");
    localStorage.clear();
    localStorage.setItem("dataMenus", temp_storage);
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    let segment_url = window.location.pathname.split("/");
    let id_survey = segment_url[4];
    const data = {
      id: id_survey,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/survey/survey_select_byid",
        data,
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          const datax = res.data.result.Data[0];
          this.setState({
            nama_survey: datax.nama_survey,
          });
        }
      });

    this.setState(
      {
        id_survey: id_survey,
      },
      () => {
        this.handleReload();
      },
    );
  }

  handleReload() {
    let dataBody = {
      id_survey: this.state.id_survey,
    };
    const excel = [];
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/survey/export_report",
        dataBody,
        this.configs,
      )
      .then((res) => {
        const datax = res.data.result.Data;
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;

        if (statusx) {
          swal.hideLoading();
          //for every row
          datax.forEach(function (element, i) {
            let mergedPertanyaanJawaban = {};
            if (element.pjawaban) {
              const jawaban = JSON.parse(element.pjawaban);
              //for every jawaban
              jawaban.forEach(function (elementJawaban, j) {
                let pertanyaanJawaban = {};
                if (elementJawaban.type == "pertanyaan_terbuka") {
                  pertanyaanJawaban = {
                    [j + 1 + ".Pertanyaan"]: elementJawaban.question,
                    [j + 1 + ".Jawaban"]: elementJawaban.answer,
                  };
                } else if (
                  elementJawaban.type == "objective" ||
                  elementJawaban.type == "multiple_choice"
                ) {
                  const answer = JSON.parse(elementJawaban.answer);
                  const key = elementJawaban.participant_answer;
                  let result = answer.filter((obj) => {
                    return obj.key === key;
                  });

                  pertanyaanJawaban = {
                    [j + 1 + ".Pertanyaan"]: elementJawaban.question,
                    [j + 1 + ".Jawaban"]:
                      result[0].key + ". " + result[0].option,
                  };
                } else if (elementJawaban.type == "triggered_question") {
                  const answer = JSON.parse(elementJawaban.answer);
                  const participant_answer = JSON.parse(
                    elementJawaban.participant_answer,
                  );
                  let result = [];
                  if (participant_answer != null) {
                    const key = participant_answer[0].key;
                    result = answer.filter((obj) => {
                      return obj.key === key;
                    });
                    if (result.length != 0) {
                      pertanyaanJawaban = {
                        [j + 1 + ".Pertanyaan"]: elementJawaban.question,
                        [j + 1 + ".Jawaban"]:
                          result[0].key + ". " + result[0].option,
                      };
                    } else {
                      pertanyaanJawaban = {
                        [j + 1 + ".Pertanyaan"]: elementJawaban.question,
                        [j + 1 + ".Jawaban"]: "Tidak Menjawab",
                      };
                    }
                  } else {
                    pertanyaanJawaban = {
                      [j + 1 + ".Pertanyaan"]: elementJawaban.question,
                      [j + 1 + ".Jawaban"]: "Tidak Menjawab",
                    };
                  }
                }
                mergedPertanyaanJawaban = {
                  ...mergedPertanyaanJawaban,
                  ...pertanyaanJawaban,
                };
              });
            }

            let rows = {
              Nama: element.pnama,
              Email: element.pemail,
              NIK: element.pnik,
              Akademi: element.pakademi,
              Pelatihan: element.ppelatihan,
              Tanggal_Pengerjaan: indonesianDateFormat(element.ptgl_pengerjaan),
              Status: element.pstatus,
            };

            rows = {
              ...rows,
              ...mergedPertanyaanJawaban,
            };

            excel.push(rows);
          });

          //create column data table
          const column = [];
          excel.forEach(function (element) {
            for (const key in element) {
              if (column.indexOf(key) === -1) {
                column.push(key);
              }
            }
          });

          const column_datatable = [];

          column.forEach(function (element, i) {
            const col = {
              name: element,
              sortable: true,
              width: "180px",
              selector: (row) => row[column[i]],
            };
            column_datatable.push(col);
          });

          this.setState(
            {
              columns: column_datatable,
              datax: excel,
            },
            () => {
              console.log(this.state.columns);
              console.log(this.state.datax);
            },
          );
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.handleReload();
              }
            });
        }
      })
      .catch((error) => {
        console.log(error);
        this.setState({ loading: false });
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  handleKembaliAction() {
    window.location = "/subvit/survey/report/" + this.state.id_survey;
  }

  render() {
    return (
      <div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 col-md-12 col-sm-12"></div>
                </div>
                <div className="col-lg-12 mt-7">
                  <div className="card card-flush border">
                    <div className="card-header pt-6">
                      <div className="card-title">
                        <div className="card mb-12 mb-xl-8">
                          <h2 className="me-3 mr-2">
                            Report Survey {this.state.nama_survey}
                          </h2>
                        </div>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="table-responsive">
                        <DataTable
                          columns={this.state.columns}
                          data={this.state.datax}
                          progressPending={this.state.loading}
                          highlightOnHover
                          pointerOnHover
                          pagination
                          paginationServer
                          paginationTotalRows={this.state.totalRows}
                          onChangeRowsPerPage={this.handlePerRowsChange}
                          onChangePage={this.handlePageChange}
                          customStyles={this.customStyles}
                          persistTableHead={true}
                          noDataComponent={
                            <div className="mt-5">Tidak Ada Data</div>
                          }
                          onSort={this.handleSort}
                          sortServer
                        />
                      </div>
                      <button
                        type="button"
                        onClick={this.handleKembali}
                        className="btn btn-danger btn-sm me-3 mr-2"
                      >
                        Kembali
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
