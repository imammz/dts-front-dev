import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import SurveyCloneList from "./list/Survey-Clone-List";
import SurveyForm from "./form/Survey-Clone/Edit-Soal/SurveyForm";
import TambahSoalForm from "./form/Survey-Clone/Tambah-Soal/TambahSoalForm";
import {
  capitalizeTheFirstLetterOfEachWord,
  dmyToYmd,
  indonesianDateFormat,
} from "./../../publikasi/helper";
import ImportForm from "./form/Survey-Clone/Tambah-Soal/ImportForm";
import DataTable from "react-data-table-component";
import { fixBootstrapDropdown } from "../../../utils/commonutil";
import { isAdminAkademi, isSuperAdmin } from "../../AksesHelper";

export default class SurveyContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleChangeLevelTujuan =
      this.handleChangeLevelTujuanAction.bind(this);
    this.handleChangeAkademiTujuan = this.handleChangeAkademiTujuan.bind(this);
    this.handleChangeTemaTujuan = this.handleChangeTemaTujuanAction.bind(this);
    this.handleChangePelatihanTujuan =
      this.handleChangePelatihanTujuanAction.bind(this);
    this.handleChangeJenisSurvey =
      this.handleChangeJenisSurveyAction.bind(this);
    this.handleChangeBehavior = this.handleChangeBehaviorAction.bind(this);
    this.handleCallBackList = this.handleCallBackListAction.bind(this);
    this.handleCallBackSurvey = this.handleCallBackSurveyAction.bind(this);
    this.handleCallBackTambah = this.handleCallBackTambahAction.bind(this);
    this.handleCallBackImport = this.handleCallBackImportAction.bind(this);
    this.handleSubmit = this.handleSubmitAction.bind(this);
    this.handleKembali = this.handleKembaliAction.bind(this);
    this.state = {
      errors: {},
      errors_message: "",
      dataxakademi: [],
      //dataxakademitujuan: [],
      dataxtematujuan: [],
      dataxpelatihantujuan: [],
      dataxakademi_show: [],
      dataxtema_show: [],
      dataxpelatihan_show: [],
      loading_akademi: false,
      loading_tema: false,
      loading_pelatihan: false,
      totalRowsAkademi: 0,
      totalRowsTema: 0,
      totalRowsPelatihan: 0,
      tempLastNumberAkademi: 0,
      tempLastNumberTema: 0,
      tempLastNumberPelatihan: 0,
      newPerPageAkademi: 10,
      newPerPageTema: 10,
      newPerPagePelatihan: 10,
      isDisabledTemaTujuan: true,
      isDisabledPelatihanTujuan: true,
      valAkademiTujuan: [],
      valTemaTujuan: [],
      valPelatihanTujuan: [],
      valLevel: [],
      levelAkademiTujuanDisabled: true,
      levelTemaTujuanDisabled: true,
      levelPelatihanTujuanDisabled: true,
      pilihanLevelSurveyTujuan: "",
      level_survey_tujuan: "",
      tema_id_tujuan: null,
      pelatihan_id_tujuan: null,
      akademi_id_tujuan: null,
      is_update_table: 0,
      is_edit_soal: false,
      is_tambah_soal: false,
      id_soal_callback: 0,
      id_survey_callback: 0,
      option_role_peserta: [],
      checkedState: [],
      arr_id_checked: [],
      id_survey: window.location.pathname.split("/")[4],
      nama_survey: "",
      jenis_survey: null,
      behavior_survey: null,
      valJenisSurvey: [],
      valBehavior: [],
      show_role: true,
      show_hide_tingkatan: true,
      show_hide_survey: true,
      show_hide_tanggal: true,
      datax_survey: [],
      text_jenis_survey: "",
      text_level_survey: "",
      nomor_soal: "",
      check_all: false,
      simpan_tambah: false,
      is_import_soal: false,
      option_akademi: [],
      checkedStateAkademi: [],
      arr_id_checked_akademi: [],
      survey_exists: [],
      role_id: Cookies.get("role_id_user"),
      nama_survey_asal: "",
      level_survey_asal: "",
      jenis_survey_asal: "",
    };
  }

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  level = ["Akademi", "Tema", "Pelatihan", "Seluruh Populasi DTS"];

  jenis_survey_label = [
    "Evaluasi",
    "Umum",
    "Evaluasi Online",
    "Evaluasi Offline",
    "Evaluasi Online & Offline",
  ];

  level_survey = isSuperAdmin()
    ? [
        { value: 3, label: "Seluruh Populasi DTS" },
        { value: 0, label: "Akademi" },
        { value: 1, label: "Tema" },
        { value: 2, label: "Pelatihan" },
      ]
    : isAdminAkademi()
      ? [
          { value: 0, label: "Akademi" },
          { value: 1, label: "Tema" },
          { value: 2, label: "Pelatihan" },
        ]
      : [
          { value: 1, label: "Tema" },
          { value: 2, label: "Pelatihan" },
        ];

  level_survey_general = [
    { value: 1, label: "Tema" },
    { value: 2, label: "Pelatihan" },
  ];

  optionstatus = [
    { value: "1", label: "Publish" },
    { value: "0", label: "Draft" },
  ];

  jenis_survey = [
    { value: 0, label: "Evaluasi" },
    { value: 1, label: "Umum" },
    { value: 2, label: "Evaluasi Online" },
    { value: 3, label: "Evaluasi Offline" },
    { value: 4, label: "Evaluasi Online & Offline" },
  ];

  behavior_survey = [
    { value: 0, label: "Optional" },
    { value: 1, label: "Mandatory" },
  ];

  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  componentDidUpdate() {
    fixBootstrapDropdown();
  }

  columnsAkademi = [
    {
      name: "No",
      cell: (row, index) => this.state.tempLastNumberAkademi + index + 1,
      width: "70px",
    },
    {
      name: "Akademi",
      sortable: false,
      selector: (row) => row.label,
    },
  ];

  columnsTema = [
    {
      name: "No",
      cell: (row, index) => this.state.tempLastNumberTema + index + 1,
      width: "70px",
    },
    {
      name: "Tema",
      sortable: false,
      selector: (row) => row.label,
    },
  ];

  columnsPelatihan = [
    {
      name: "No",
      cell: (row, index) => this.state.tempLastNumberPelatihan + index + 1,
      width: "70px",
    },
    {
      name: "Nama Pelatihan",
      sortable: false,
      //width: "300px",
      selector: (row) => row.label,
    },
  ];

  handleReloadAkademiList(page, newPerPage) {
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumberAkademi: start_tmp });
    const start = start_tmp;
    const rows = length_tmp + start_tmp;
    const dataxakademi_show = this.state.dataxakademi.slice(start, rows);
    this.setState({
      dataxakademi_show,
      loading_akademi: false,
    });
  }

  handlePageChangeAkademi = (page) => {
    this.setState({ loading_akademi: true });
    this.handleReloadAkademiList(page, this.state.newPerPageAkademi);
  };
  handlePerRowsChangeAkademi = async (newPerPage, page) => {
    this.setState({ loading_akademi: true });
    this.setState({ newPerPageAkademi: newPerPage });
    this.handleReloadAkademiList(page, newPerPage);
  };

  handleReloadTemaList(page, newPerPage) {
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumberTema: start_tmp });
    const start = start_tmp;
    const rows = length_tmp + start_tmp;
    const dataxtema_show = this.state.dataxtematujuan.slice(start, rows);
    this.setState({
      dataxtema_show,
      loading_tema: false,
    });
  }

  handlePageChangeTema = (page) => {
    this.setState({ loading_tema: true });
    this.handleReloadTemaList(page, this.state.newPerPageTema);
  };
  handlePerRowsChangeTema = async (newPerPage, page) => {
    this.setState({ loading_tema: true });
    this.setState({ newPerPageTema: newPerPage });
    this.handleReloadTemaList(page, newPerPage);
  };

  handleReloadPelatihanList(page, newPerPage) {
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumberPelatihan: start_tmp });
    const start = start_tmp;
    const rows = length_tmp + start_tmp;
    const dataxpelatihan_show = this.state.dataxpelatihantujuan.slice(
      start,
      rows,
    );
    this.setState({
      dataxpelatihan_show,
      loading_pelatihan: false,
    });
  }

  handlePageChangePelatihan = (page) => {
    this.setState({ loading_pelatihan: true });
    this.handleReloadPelatihanList(page, this.state.newPerPagePelatihan);
  };
  handlePerRowsChangePelatihan = async (newPerPage, page) => {
    this.setState({ loading_pelatihan: true });
    this.setState({ newPerPagePelatihan: newPerPage });
    this.handleReloadPelatihanList(page, newPerPage);
  };

  componentDidMount() {
    const temp_storage = localStorage.getItem("dataMenus");
    localStorage.clear();
    localStorage.setItem("dataMenus", temp_storage);
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    const data = {
      id: this.state.id_survey,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/survey/survey_select_byid",
        data,
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        if (statux) {
          const datax = res.data.result.survey[0];

          if (datax.jenis_survey > 1) {
            window.history.back();
            this.setState({
              show_behavior: false,
              show_hide_tingkatan: false,
              show_hide_survey: false,
              show_hide_tanggal: false,
              show_role: false,
              level_survey_tujuan: 3,
            });
            this.setState({
              valLevelSurvey: {
                label: this.level_survey[0].label,
                value: this.level_survey[0].value,
              },
            });
          } else {
            this.setState({
              show_behavior: true,
              show_hide_tingkatan: true,
              show_hide_survey: true,
              show_hide_tanggal: false,
              show_role: true,
            });
            if (datax.mandatory == null) {
              datax.mandatory = 0;
            }
            if (datax.jenis_survey == null) {
              datax.jenis_survey = 1;
            }
            this.setState({
              //valBehavior: { label: this.behavior_survey[datax.mandatory].label, value: datax.mandatory }
            });
          }
          let text_jenis_survey = "Evaluasi";
          if (datax.jenis_survey == 1) {
            text_jenis_survey = "Umum";
          } else if (datax.jenis_survey == 2) {
            text_jenis_survey = "Evaluasi Online";
          } else if (datax.jenis_survey == 2) {
            text_jenis_survey = "Evaluasi Offline";
          } else if (datax.jenis_survey == 2) {
            text_jenis_survey = "Evaluasi Hybrid";
          }

          let text_level_survey = "Akademi";
          if (datax.level_survey == 1) {
            text_level_survey = "Tema";
          } else if (datax.level_survey == 2) {
            text_level_survey = "Pelatihan";
          } else if (datax.level_survey == 3) {
            text_level_survey = "All Level DTS";
          }

          const nama_survey_asal = datax.nama_survey;
          const level_survey_asal = datax.level;
          const jenis_survey_asal = datax.jenis_survey;

          this.setState({
            nama_survey_asal,
            level_survey_asal,
            jenis_survey_asal,
            datax_survey: datax,
            text_jenis_survey: text_jenis_survey,
            text_level_survey: text_level_survey,
            valJenisSurvey: {
              label: this.jenis_survey[datax.jenis_survey].label,
              value: datax.jenis_survey,
            },
            jenis_survey: datax.jenis_survey,
            behavior_survey: datax.mandatory,
          });
        }
      });

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/umum/list-status-peserta",
        null,
        this.configs,
      )
      .then((res) => {
        const option_role_peserta = res.data.result.Data;
        option_role_peserta.forEach(function (element, i) {
          if (
            element.name == "Pembatalan" ||
            element.name == "Mengundurkan Diri"
          ) {
            option_role_peserta.splice(i, 1);
          }
        });
        option_role_peserta.forEach(function (element, i) {
          if (element.name == "Banned") {
            option_role_peserta.splice(i, 1);
          }
        });
        this.setState({
          checkedState: new Array(option_role_peserta.length).fill(false),
        });
        this.setState({ option_role_peserta }, () => {
          if (this.state.jenis_survey > 1) {
            this.checkAllRole();
          }
        });
      });

    const dataAkademik = { start: 0, length: 100 };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/akademi/list_akademi_survey",
        dataAkademik,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const totalRowsAkademi = res.data.result.Total;
        const dataxakademi = [];
        const option_akademi = optionx;
        optionx.map((data) =>
          dataxakademi.push({ value: data.id, label: data.name }),
        );
        this.setState(
          {
            dataxakademi,
            totalRowsAkademi,
            option_akademi,
          },
          () => {
            this.handleReloadAkademiList();
            swal.close();
          },
        );
      });

    axios
      .get(
        process.env.REACT_APP_BASE_API_URI + "/cek-akademi-evaluasi-survey",
        this.configs,
      )
      .then((res) => {
        const datax = res.data.result.Data;
        for (let i = 0; i < datax.length; i++) {
          document.getElementById(
            "custom-checkbox-akademi-" + datax[i].akademi_id,
          ).disabled = true;
          document.getElementById(
            "custom-checkbox-akademi-" + datax[i].akademi_id,
          ).title = "Sudah Memiliki Survey Evaluasi";
          document.getElementById(
            "custom-checkbox-akademi-" + datax[i].akademi_id,
          ).checked = false;
          document.getElementById(
            "custom-checkbox-text-akademi-" + datax[i].akademi_id,
          ).disabled = true;
          document.getElementById(
            "custom-checkbox-text-akademi-" + datax[i].akademi_id,
          ).title = "Sudah Memiliki Survey Evaluasi";
        }
        this.setState({
          survey_exists: datax,
        });
      });
  }

  handleChangeLevelTujuanAction = (selectedLevel) => {
    const level = selectedLevel.value;
    if (level == 0) {
      this.setState({ levelAkademiTujuanDisabled: false });
      this.setState({ levelTemaTujuanDisabled: true });
      this.setState({ levelPelatihanTujuanDisabled: true });
      this.setState({ show_role: true });
      this.setState({ show_hide_tingkatan: true });
      this.clearCheckedRole();
    } else if (level == 1) {
      this.setState({ levelAkademiTujuanDisabled: false });
      this.setState({ levelTemaTujuanDisabled: false });
      this.setState({ levelPelatihanTujuanDisabled: true });
      this.setState({ show_role: true });
      this.setState({ show_hide_tingkatan: true });
      this.clearCheckedRole();
    } else if (level == 2) {
      this.setState({ levelAkademiTujuanDisabled: false });
      this.setState({ levelTemaTujuanDisabled: false });
      this.setState({ levelPelatihanTujuanDisabled: false });
      this.setState({ show_role: true });
      this.setState({ show_hide_tingkatan: true });
      this.clearCheckedRole();
    } else if (level == 3) {
      this.setState({ levelAkademiTujuanDisabled: true });
      this.setState({ levelTemaTujuanDisabled: true });
      this.setState({ levelPelatihanTujuanDisabled: true });
      this.setState({ show_role: false });
      this.setState({ show_hide_tingkatan: false });
      this.checkAllRole();

      this.handleReloadAkademiList();
      //ambil data buat list dibelakang, tema dan pelatihan
      const dataBody = {
        start: 0,
        rows: 100,
        id_akademi: 0,
        status: "null",
        cari: 0,
        sort: "tema",
        sort_val: "ASC",
      };
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/list_tema_filter2",
          dataBody,
          this.configs,
        )
        .then((res) => {
          const optionx = res.data.result.Data;
          const dataxtematujuan = [];
          const totalRowsTema = res.data.result.TotalData;
          optionx.map((data) =>
            dataxtematujuan.push({ value: data.id, label: data.name }),
          );
          this.setState(
            {
              dataxtematujuan,
              totalRowsTema,
            },
            () => this.handleReloadTemaList(),
          );
        })
        .catch((error) => {
          const dataxtematujuan = [];
          const totalRowsTema = 0;
          this.setState(
            {
              dataxtematujuan,
              totalRowsTema,
            },
            () => this.handleReloadTemaList(),
          );
          let messagex = error.response.data.result.Message;
        });

      const date = new Date();
      const y = date.getFullYear();

      const dataBody2 = {
        mulai: 0,
        limit: 100,
        id_penyelenggara: 0,
        id_akademi: 0,
        id_tema: 0,
        status_substansi: 0,
        status_pelatihan: 0,
        status_publish: 99,
        provinsi: 0,
        param: null,
        sort: "id_pelatihan",
        sortval: "DESC",
        tahun: y,
        id_silabus: 0,
      };

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/pelatihan",
          dataBody2,
          this.configs,
        )
        .then((res) => {
          const optionx = res.data.result.Data;
          const dataxpelatihantujuan = [];
          const totalRowsPelatihan = res.data.result.TotalData;
          optionx.map((data) =>
            dataxpelatihantujuan.push({
              value: data.id_pelatihan,
              label: data.pelatihan,
            }),
          );
          this.setState(
            {
              dataxpelatihantujuan,
              totalRowsPelatihan,
            },
            () => this.handleReloadPelatihanList(),
          );
        })
        .catch((error) => {
          console.log(error);
          const dataxpelatihantujuan = [];
          const totalRowsPelatihan = 0;
          this.setState(
            {
              dataxpelatihantujuan,
              totalRowsPelatihan,
            },
            () => this.handleReloadPelatihanList(),
          );
          let messagex = error.response.data.result.Message;
        });
    }
    this.setState({
      level_survey_tujuan: level,
      valAkademiTujuan: [],
      valTemaTujuan: [],
      valPelatihanTujuan: [],
      tema_id_tujuan: null,
      pelatihan_id_tujuan: null,
      akademi_id_tujuan: null,
      isDisabledPelatihanTujuan: true,
      isDisabledTemaTujuan: true,
    });
  };

  clearCheckedRole() {
    const filled_option = [];
    const arr_id_checked = [];
    this.state.option_role_peserta.forEach(function (element, i) {
      filled_option[i] = false;
    });
    this.setState({
      checkedState: filled_option,
      arr_id_checked: [],
    });
  }

  checkAllRole() {
    const filled_option = [];
    const arr_id_checked = [];
    this.state.option_role_peserta.forEach(function (element, i) {
      filled_option[i] = true;
      arr_id_checked.push(element.id);
    });
    this.setState(
      {
        checkedState: filled_option,
        arr_id_checked: arr_id_checked,
      },
      () => {
        console.log(this.state.arr_id_checked);
      },
    );
  }

  handleChangeAkademiTujuan = (akademi_id) => {
    this.setState({
      valTemaTujuan: [],
      valPelatihanTujuan: [],
      tema_id_tujuan: null,
      pelatihan_id_tujuan: null,
      isDisabledPelatihanTujuan: true,
    });

    const dataBody = {
      start: 0,
      rows: 100,
      id_akademi: akademi_id.value,
      status: "null",
      cari: 0,
      sort: "tema",
      sort_val: "ASC",
    };

    if (this.state.level_survey_tujuan == 0 && this.state.jenis_survey == 1) {
      const dataxakademi_show = [
        {
          label: akademi_id.label,
        },
      ];

      this.setState({
        dataxakademi_show,
      });
    }

    this.setState({
      valAkademiTujuan: { label: akademi_id.label, value: akademi_id.value },
    });
    this.setState({ akademi_id_tujuan: akademi_id.value });
    this.setState({ pilihanLevelSurveyTujuan: akademi_id.label });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list_tema_filter2",
        dataBody,
        this.configs,
      )
      .then((res) => {
        this.setState({ isDisabledTemaTujuan: false });
        const optionx = res.data.result.Data;
        const dataxtema = [];
        const totalRowsTema = res.data.result.TotalData;
        optionx.map((data) =>
          dataxtema.push({ value: data.id, label: data.name }),
        );
        this.setState(
          {
            dataxtematujuan: dataxtema,
            totalRowsTema,
          },
          () => this.handleReloadTemaList(),
        );
      })
      .catch((error) => {
        const dataxtema = [];
        const totalRowsTema = 0;
        this.setState(
          {
            dataxtematujuan: dataxtema,
            totalRowsTema,
          },
          () => this.handleReloadTemaList(),
        );
        let messagex = error.response.data.result.Message;
        if (this.state.level_survey_tujuan != 0) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });

    const date = new Date();
    const y = date.getFullYear();

    const dataBody2 = {
      mulai: 0,
      limit: 100,
      id_penyelenggara: 0,
      id_akademi: akademi_id.value,
      id_tema: 0,
      status_substansi: 0,
      status_pelatihan: 0,
      status_publish: 99,
      provinsi: 0,
      param: null,
      sort: "id_pelatihan",
      sortval: "DESC",
      tahun: y,
      id_silabus: 0,
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/pelatihan",
        dataBody2,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxpelatihantujuan = [];
        const totalRowsPelatihan = res.data.result.TotalData;
        optionx.map((data) =>
          dataxpelatihantujuan.push({ value: data.pid, label: data.pelatihan }),
        );
        this.setState(
          {
            dataxpelatihantujuan,
            totalRowsPelatihan,
          },
          () => this.handleReloadPelatihanList(),
        );
      })
      .catch((error) => {
        console.log(error);
        const dataxpelatihantujuan = [];
        const totalRowsPelatihan = 0;
        this.setState(
          {
            dataxpelatihantujuan,
            totalRowsPelatihan,
          },
          () => this.handleReloadPelatihanList(),
        );
        let messagex = error.response.data.result.Message;
      });
  };

  handleChangeTemaTujuanAction = (tema) => {
    this.setState({ valTemaTujuan: { label: tema.label, value: tema.value } });
    this.setState({ tema_id_tujuan: tema.value });
    this.setState({ pilihanLevelSurveyTujuan: tema.label });

    if (this.state.level_survey_tujuan == 1 && this.state.jenis_survey == 1) {
      const dataxtema_show = [
        {
          label: tema.label,
        },
      ];
      this.setState({
        dataxtema_show,
      });
    }

    /* const dataBody = {
			jns_param: 3,
			akademi_id: this.state.akademi_id_tujuan,
			theme_id: tema.value,
			pelatihan_id: 0,
		}; */
    const date = new Date();
    const y = date.getFullYear();

    const dataBody = {
      mulai: 0,
      limit: 100,
      id_penyelenggara: 0,
      id_akademi: this.state.akademi_id_tujuan,
      id_tema: tema.value,
      status_substansi: 0,
      status_pelatihan: 0,
      status_publish: 99,
      provinsi: 0,
      param: null,
      sort: "id_pelatihan",
      sortval: "DESC",
      tahun: y,
      id_silabus: 0,
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/pelatihan",
        dataBody,
        this.configs,
      )
      .then((res) => {
        this.setState({ isDisabledPelatihanTujuan: false });
        const optionx = res.data.result.Data;
        const dataxpelatihan = [];
        const totalRowsPelatihan = res.data.result.TotalLength;
        optionx.map((data) => {
          const metode_pelatihan =
            data.metode_pelatihan == "Online"
              ? data.metode_pelatihan.toUpperCase()
              : data.nm_kab + " " + data.nm_prov;
          dataxpelatihan.push({
            value: data.id,
            label:
              "[" +
              data.penyelenggara +
              "] - " +
              data.slug_pelatian_id +
              " - " +
              data.pelatihan +
              " Batch " +
              data.batch +
              " - " +
              metode_pelatihan,
          });
        });
        this.setState(
          {
            dataxpelatihantujuan: dataxpelatihan,
            totalRowsPelatihan,
          },
          () => this.handleReloadPelatihanList(),
        );
      })
      .catch((error) => {
        const dataxpelatihan = [];
        const totalRowsPelatihan = 0;
        this.setState({
          dataxpelatihantujuan: dataxpelatihan,
          totalRowsPelatihan,
        });
        let messagex = error.response.data.result.Message;
        if (this.state.level_survey_tujuan > 1) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  };

  handleChangePelatihanTujuanAction = (pelatihan) => {
    this.setState({
      valPelatihanTujuan: { label: pelatihan.label, value: pelatihan.value },
    });
    this.setState({ pelatihan_id_tujuan: pelatihan.value });
    this.setState({ pilihanLevelSurveyTujuan: pelatihan.label });

    if (this.state.level_survey_tujuan == 2 && this.state.jenis_survey == 1) {
      const dataxpelatihan_show = [{ label: pelatihan.label }];

      this.setState({
        dataxpelatihan_show,
      });
    }
  };

  handleCallBackListAction(e) {
    if (e.is_tambah_soal || e.is_import_soal) {
      this.setState({
        is_edit_soal: e.is_edit_soal,
        is_tambah_soal: e.is_tambah_soal,
        is_import_soal: e.is_import_soal,
        simpan_tambah: false,
        nomor_soal: e.nomor_soal,
      });
    } else {
      this.setState({
        is_tambah_soal: e.is_tambah_soal,
        is_edit_soal: e.is_edit_soal,
        is_import_soal: e.is_import_soal,
        id_soal_callback: e.id_soal,
        id_survey_callback: e.id_survey,
        simpan_tambah: false,
        nomor_soal: e.nomor_soal,
      });
    }
  }

  handleCallBackSurveyAction(e) {
    this.setState({
      is_edit_soal: e.is_edit_soal,
      is_tambah_soal: e.is_tambah_soal,
      is_import_soal: false,
      simpan_tambah: false,
    });
  }

  handleCallBackTambahAction(e) {
    this.setState({
      is_edit_soal: e.is_edit_soal,
      is_tambah_soal: e.is_tambah_soal,
      is_import_soal: false,
      simpan_tambah: e.simpan_tambah,
    });
  }

  handleCallBackImportAction(e) {
    this.setState({
      is_edit_soal: e.is_edit_soal,
      is_tambah_soal: e.is_tambah_soal,
      is_import_soal: e.is_import_soal,
      simpan_tambah: false,
    });
  }

  handleSubmitAction(e) {
    e.preventDefault();
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    if (this.handleValidation(e)) {
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });
      const soal_json = [];
      const status = parseInt(dataForm.get("status"));
      const idtema_tujuan =
        dataForm.get("idtema_tujuan") != null
          ? parseInt(dataForm.get("idtema_tujuan"))
          : 0;
      const idpelatihan_tujuan =
        dataForm.get("idpelatihan_tujuan") != null
          ? parseInt(dataForm.get("idpelatihan_tujuan"))
          : 0;
      let idakademi_tujuan = null;
      if (this.state.jenis_survey == 1) {
        idakademi_tujuan =
          dataForm.get("idakademi_tujuan") != null
            ? parseInt(dataForm.get("idakademi_tujuan"))
            : 0;
      } else {
        const arr_id_checked_akademi = this.state.arr_id_checked_akademi;
        const idakademis = [];
        for (let i = 0; i < arr_id_checked_akademi.length; i++) {
          const akademi = { idakademi: arr_id_checked_akademi[i] };
          idakademis.push(akademi);
        }
        idakademi_tujuan = idakademis;
        idakademi_tujuan = JSON.stringify(idakademi_tujuan);
      }
      //ambil dulu dari server semuanya, karena yg disimpan di local storage hanya yg ada pagination
      const dataBody = {
        start: 0,
        rows: 200,
        param: "",
        id_survey: this.state.id_survey,
      };
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/survey/list_soal_survey_byid",
          dataBody,
          this.configs,
        )
        .then((res) => {
          const datax = res.data.result.Data;
          const saveDatax = [];
          //uncheck
          let uncheck = JSON.parse(localStorage.getItem("uncheck"));
          //check tambah
          let itemTambah = JSON.parse(localStorage.getItem("TAMBAH"));
          if (itemTambah) {
            itemTambah.forEach(function (element, i) {
              const item = JSON.parse(localStorage.getItem(element));
              if (item && uncheck != null) {
                if (!uncheck.includes(item.pidsurvey_detail)) {
                  datax.push(item);
                }
              } else if (item && uncheck == null) {
                datax.push(item);
              }
            });
          }
          console.log(datax);
          datax.forEach(function (element, i) {
            //check edit
            let edit = JSON.parse(
              localStorage.getItem("_" + element.pidsurvey_detail),
            );
            if (edit) {
              if (
                uncheck == null ||
                uncheck == false ||
                (uncheck && !uncheck.includes(edit.pidsurvey_detail))
              ) {
                const key_gambar_pertanyaan = edit.key_gambar_pertanyaan;
                let imagePertanyaanWrapper = document.getElementById(
                  key_gambar_pertanyaan,
                );
                let gambar_pertanyaan = null;
                if (imagePertanyaanWrapper) {
                  imagePertanyaanWrapper = imagePertanyaanWrapper.value;
                  const splitImagePertanyaan =
                    imagePertanyaanWrapper.split("_");
                  gambar_pertanyaan = splitImagePertanyaan[1];
                }
                delete edit.key_gambar_pertanyaan;
                edit.pertanyaan_gambar = gambar_pertanyaan;

                if (edit.type != "pertanyaan_terbuka") {
                  edit.jawaban.forEach(function (elementJawaban) {
                    let image = null;
                    let imageName = null;
                    let imageWrapper = document.getElementById(
                      elementJawaban.key_image,
                    );
                    if (imageWrapper) {
                      imageWrapper = imageWrapper.value;
                      const splitImage = imageWrapper.split("_");
                      imageName = splitImage[0];
                      image = splitImage[1];
                    }
                    delete elementJawaban.key_image;
                    elementJawaban.image = image;
                    elementJawaban.imageName = imageName;

                    if (elementJawaban.sub && elementJawaban.sub.length != 0) {
                      elementJawaban.sub.forEach(function (elementSub) {
                        let imagePertanyaanSub = null;
                        let imagePertanyaanSubName = null;
                        let imageWrapperPertanyaanSub = document.getElementById(
                          elementSub.key_image,
                        );
                        if (imageWrapperPertanyaanSub) {
                          imageWrapperPertanyaanSub =
                            imageWrapperPertanyaanSub.value;
                          const splitImage =
                            imageWrapperPertanyaanSub.split("_");
                          imagePertanyaanSubName = splitImage[0];
                          imagePertanyaanSub = splitImage[1];
                        }
                        delete elementSub.key_image;
                        elementSub.image = imagePertanyaanSub;
                        elementSub.imageName = imagePertanyaanSubName;

                        elementSub.answer.forEach(function (elementAnswerSub) {
                          let imageAnswerSub = null;
                          let imageAnswerSubName = null;
                          let imageAnswerSubWrapper = document.getElementById(
                            elementAnswerSub.key_image,
                          );
                          if (imageAnswerSubWrapper) {
                            imageAnswerSubWrapper = imageAnswerSubWrapper.value;
                            const splitImage = imageAnswerSubWrapper.split("_");
                            imageAnswerSubName = splitImage[0];
                            imageAnswerSub = splitImage[1];
                          }
                          delete elementAnswerSub.key_image;
                          elementAnswerSub.image = imageAnswerSub;
                          elementAnswerSub.imageName = imageAnswerSubName;
                        });
                      });
                    }
                  });
                }

                element = edit;
              }
            } else {
              try {
                element.jawaban = JSON.parse(element.jawaban);
              } catch (error) {}
            }
            element.nosoal = i + 1;
            element.idsurvey = parseInt(element.pidsurvey);
            if (isNaN(element.pidsurvey_detail)) {
              element.pidsurvey_detail = 0;
            }

            if (element.pidsurvey_detail != null) {
              element.idsurvey_detail = element.pidsurvey_detail;
            } else {
              element.idsurvey_detail = 0;
            }

            element.tipe = element.type;
            //element.gambar_pertanyaan = element.pertanyaan_gambar;
            element.status = element.status == "publish" ? 1 : 0;
            delete element.pidsurvey;
            delete element.pidsurvey_detail;
            delete element.type;
            //delete element.pertanyaan_gambar;
            //check delete
            const id = element.idsurvey_detail;
            if (uncheck) {
              console.log("includes", id);
              if (!uncheck.includes(id)) {
                saveDatax.push(element);
              }
            } else {
              saveDatax.push(element);
            }
          });

          console.log(saveDatax);

          const param = {
            categoryOptItems: saveDatax,
          };

          if (param.categoryOptItems.length == 0) {
            swal
              .fire({
                title: "Belum Ada Soal yang diInput",
                icon: "warning",
                confirmButtonText: "Ok",
                didOpen: () => {
                  swal.hideLoading();
                },
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          } else {
            const date = new Date();
            const y = date.getFullYear();
            let start_at = y + "-01-01";
            let end_at = y + "-12-31";

            const dataFormSubmit = new FormData();
            dataFormSubmit.append("akademi_id", idakademi_tujuan);
            dataFormSubmit.append("nama_survey", dataForm.get("judul"));
            dataFormSubmit.append("theme_id", idtema_tujuan);
            dataFormSubmit.append("pelatihan_id", idpelatihan_tujuan);
            dataFormSubmit.append(
              "id_status_peserta",
              this.state.arr_id_checked.join(","),
            );
            dataFormSubmit.append("duration", 1);
            if (dataForm.get("start_at") != null) {
              dataFormSubmit.append("start_at", start_at);
              dataFormSubmit.append("end_at", end_at);
            } else {
              dataFormSubmit.append("start_at", start_at);
              dataFormSubmit.append("end_at", end_at);
            }
            dataFormSubmit.append("param", JSON.stringify(param));
            dataFormSubmit.append("jenis_survey", this.state.jenis_survey);
            let behavior = null;
            if (this.state.jenis_survey == 1) {
              behavior = this.state.behavior_survey;
            }
            dataFormSubmit.append("behaviour", behavior);
            dataFormSubmit.append("level", this.state.level_survey_tujuan);
            console.log(param);

            if (this.state.jenis_survey == 1) {
              axios
                .post(
                  process.env.REACT_APP_BASE_API_URI +
                    "/survey/submit_clone_survey",
                  dataFormSubmit,
                  this.configs,
                )
                .then((res) => {
                  const statux = res.data.result.Status;
                  const messagex = res.data.result.Message;
                  if (statux) {
                    swal
                      .fire({
                        title: messagex,
                        icon: "success",
                        confirmButtonText: "Ok",
                      })
                      .then((result) => {
                        if (result.isConfirmed) {
                          window.location = "/subvit/survey";
                        }
                      });
                  } else {
                    swal
                      .fire({
                        title: messagex,
                        icon: "warning",
                        confirmButtonText: "Ok",
                      })
                      .then((result) => {
                        if (result.isConfirmed) {
                        }
                      });
                  }
                })
                .catch((error) => {
                  let statux = error.response.data.result.Status;
                  let messagex = error.response.data.result.Message;
                  if (!statux) {
                    swal
                      .fire({
                        title: messagex,
                        icon: "warning",
                        confirmButtonText: "Ok",
                      })
                      .then((result) => {
                        if (result.isConfirmed) {
                        }
                      });
                  }
                });
            } else {
              axios
                .post(
                  process.env.REACT_APP_BASE_API_URI +
                    "/survey/submit_clone_survey_eval",
                  dataFormSubmit,
                  this.configs,
                )
                .then((res) => {
                  const statux = res.data.result.Status;
                  const messagex = res.data.result.Message;
                  if (statux) {
                    swal
                      .fire({
                        title: messagex,
                        icon: "success",
                        confirmButtonText: "Ok",
                      })
                      .then((result) => {
                        if (result.isConfirmed) {
                          window.location = "/subvit/survey";
                        }
                      });
                  } else {
                    swal
                      .fire({
                        title: messagex,
                        icon: "warning",
                        confirmButtonText: "Ok",
                      })
                      .then((result) => {
                        if (result.isConfirmed) {
                        }
                      });
                  }
                })
                .catch((error) => {
                  let statux = error.response.data.result.Status;
                  let messagex = error.response.data.result.Message;
                  if (!statux) {
                    swal
                      .fire({
                        title: messagex,
                        icon: "warning",
                        confirmButtonText: "Ok",
                      })
                      .then((result) => {
                        if (result.isConfirmed) {
                        }
                      });
                  }
                });
            }
          }
        });
    } else {
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  resetError() {
    let errors = {};
    errors[
      ("judul",
      "level_tujuan",
      "idakademi_tujuan",
      "idtema_tujuan",
      "idpelatihan_asal",
      "idpelatihan_tujuan",
      "start_at",
      "end_at",
      "status")
    ] = "";
    this.setState({ errors: errors });
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const check = this.checkEmpty(
      dataForm,
      ["judul", "level_tujuan", "start_at", "end_at", "status"],
      [],
    );
    return check;
  }

  checkEmpty(dataForm, fieldName) {
    const errorMessageEmpty = "Tidak Boleh Kosong";
    let errors = {};
    let formIsValid = true;
    for (const field in fieldName) {
      const nameAttribute = fieldName[field];
      if (dataForm.get(nameAttribute) == "") {
        errors[nameAttribute] = errorMessageEmpty;
        formIsValid = false;
      }
    }
    if (
      dataForm.get("idakademi") == "" &&
      this.state.level_survey_tujuan != 3
    ) {
      errors["idakademi"] = errorMessageEmpty;
      formIsValid = false;
    }

    if (
      dataForm.get("idtema_tujuan") == "" &&
      (this.state.level_survey_tujuan == 1 ||
        this.state.level_survey_tujuan == 2)
    ) {
      errors["idtema_tujuan"] = errorMessageEmpty;
      formIsValid = false;
    }
    if (
      dataForm.get("idpelatihan_tujuan") == "" &&
      this.state.level_survey_tujuan == 2
    ) {
      errors["idpelatihan_tujuan"] = errorMessageEmpty;
      formIsValid = false;
    }

    if (this.state.jenis_survey == 1) {
      if (this.state.arr_id_checked.length == 0) {
        errors["role"] = errorMessageEmpty;
        formIsValid = false;
      }
    }

    if (this.state.jenis_survey == null) {
      errors["jenis_survey"] = errorMessageEmpty;
      this.setState({ jenis_survey: 1 });
    }
    console.log(this.state.behavior_survey);
    if (this.state.jenis_survey == 1 && this.state.behavior_survey == null) {
      errors["behavior"] = errorMessageEmpty;
      formIsValid = false;
    }
    console.log("error", errors);
    this.setState({ errors: errors });
    return formIsValid;
  }

  handleCheckedChange(position) {
    const updatedCheckedState = this.state.checkedState.map((item, index) =>
      index === position ? !item : item,
    );
    this.setState({ checkedState: updatedCheckedState });

    const id_checked = this.state.option_role_peserta[position].id;
    const arr_id_checked = this.state.arr_id_checked;
    if (arr_id_checked.includes(id_checked)) {
      const index_checked = arr_id_checked.indexOf(id_checked);
      if (index_checked !== -1) {
        arr_id_checked.splice(index_checked, 1);
      }
    } else {
      arr_id_checked.push(id_checked);
    }

    if (arr_id_checked.length == this.state.option_role_peserta.length) {
      this.setState({ check_all: true });
    } else {
      this.setState({ check_all: false });
    }

    this.setState({ arr_id_checked });
  }

  handleChangeJenisSurveyAction = (jenis) => {
    this.setState({
      valJenisSurvey: { label: jenis.label, value: jenis.value },
    });
    this.setState({ jenis_survey: jenis.value });

    if (
      this.state.role_id == 109 ||
      this.state.role_id == 1 ||
      this.state.role_id == 116
    ) {
      this.setState({
        valLevelSurvey: {
          label: this.level_survey[0].label,
          value: this.level_survey[0].value,
        },
      });
    } else {
      this.setState({
        valLevelSurvey: {
          label: this.level_survey_general[0].label,
          value: this.level_survey_general[0].value,
        },
      });
    }
  };

  handleChangeBehaviorAction = (behavior) => {
    this.setState({
      valBehavior: { label: behavior.label, value: behavior.value },
    });
    this.setState({ behavior_survey: behavior.value });
  };

  handleCheckedChangeAkademi(position) {
    const updatedCheckedState = this.state.checkedStateAkademi.map(
      (item, index) => (index === position ? !item : item),
    );
    this.setState({ checkedStateAkademi: updatedCheckedState });

    const id_checked = this.state.option_akademi[position].id;
    const arr_id_checked_akademi = this.state.arr_id_checked_akademi;
    if (arr_id_checked_akademi.includes(id_checked)) {
      const index_checked = arr_id_checked_akademi.indexOf(id_checked);
      if (index_checked !== -1) {
        arr_id_checked_akademi.splice(index_checked, 1);
      }
    } else {
      arr_id_checked_akademi.push(id_checked);
    }

    this.setState({ arr_id_checked_akademi });
  }

  handleKembaliAction() {
    swal
      .fire({
        title: "Apakah Anda Yakin?",
        icon: "warning",
        confirmButtonText: "Ya",
        showCancelButton: true,
        cancelButtonText: "Tidak",
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
      })
      .then((result) => {
        if (result.isConfirmed) {
          window.history.back();
        }
      });
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <path
                        opacity="0.3"
                        d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                        fill="#7239ea"
                      ></path>
                      <path
                        d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                        fill="#7239ea"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Subvit
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Survey
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <a
                  onClick={this.handleKembali}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div
                    className="stepper stepper-links d-flex flex-column"
                    id="kt_subvit_clone_survey"
                  >
                    <div className="col-lg-12 mt-7">
                      <div className="card border">
                        <div className="card-body mt-5 pt-10 pb-8">
                          <div className="col-12">
                            <h1
                              className="align-items-center text-dark fw-bolder my-1 fs-4"
                              style={{ textTransform: "capitalize" }}
                            >
                              {this.state.nama_survey_asal}
                            </h1>
                            <p className="text-dark fs-7 mb-0">
                              {this.level[this.state.level_survey_asal]}
                              <span className="text-muted fs-7 mb-0">
                                {" "}
                                - Survey{" "}
                                {
                                  this.jenis_survey_label[
                                    this.state.jenis_survey_asal
                                  ]
                                }
                              </span>
                            </p>
                          </div>
                        </div>
                        <div className="card-header">
                          <div className="card-title">
                            <div className="stepper-nav flex-wrap mb-n4">
                              <div
                                className="stepper-item ms-0 me-3 my-2 current"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label ms-0">
                                    <h3 className="stepper-title fs-6">
                                      Clone Survey
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Bank Soal
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Publish
                                    </h3>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="card-body">
                          <div className="highlight bg-light-primary my-5">
                            <div className="col-lg-12 mb-7 fv-row text-primary">
                              <h5 className="text-primary fs-5">Panduan</h5>
                              <p className="text-primary">
                                Sebelum membuat Survey, mohon untuk membaca
                                panduan berikut :
                              </p>
                              <ul>
                                <li>
                                  Pelaksanaan Level Tema : Berlaku untuk{" "}
                                  <strong>SEMUA</strong> Pelatihan pada tema
                                  terpilih, setiap pelatihan pada tema tersebut
                                  maupun yang baru dibuat kemudian akan otomatis
                                  merujuk kepada Survey tersebut.
                                </li>
                                <li>
                                  Pelaksanaan Level Pelatihan : Berlaku{" "}
                                  <strong>HANYA</strong> pada pelatihan yang
                                  terpilih
                                </li>
                              </ul>
                            </div>
                          </div>
                          <form
                            action="#"
                            onSubmit={this.handleSubmit}
                            id="kt_subvit_clone_survey_form"
                          >
                            {/* MENU 1 */}
                            <div
                              className="current"
                              data-kt-stepper-element="content"
                            >
                              <div className="col-lg-12 fv-row">
                                <h5 className="mt-7 text-muted me-3 mb-5">
                                  Asal Clone Survey
                                </h5>
                                <div className="row">
                                  <div className="col-lg-6 mb-7">
                                    Nama Survey
                                    <br />
                                    <strong>
                                      {this.state.datax_survey.nama_survey}
                                    </strong>
                                  </div>
                                  <div className="col-lg-6 mb-7">
                                    Jenis Survey
                                    <br />
                                    <strong>
                                      {this.state.text_jenis_survey}
                                    </strong>
                                  </div>
                                  {this.state.datax_survey.jenis_survey == 1 ? (
                                    <div className="col-lg-6 mb-7">
                                      Tgl. Mulai Survey
                                      <br />
                                      <strong>
                                        {indonesianDateFormat(
                                          this.state.datax_survey.start_at,
                                        )}
                                      </strong>
                                    </div>
                                  ) : (
                                    ""
                                  )}
                                  {this.state.datax_survey.jenis_survey == 1 ? (
                                    <div className="col-lg-6 mb-7">
                                      Tgl. Akhir Survey
                                      <br />
                                      <strong>
                                        {indonesianDateFormat(
                                          this.state.datax_survey.end_at,
                                        )}
                                      </strong>
                                    </div>
                                  ) : (
                                    ""
                                  )}

                                  <div className="col-lg-6 mb-7">
                                    Level
                                    <br />
                                    <strong>
                                      {this.state.text_level_survey}
                                    </strong>
                                  </div>
                                  <div className="col-lg-6 mb-7">
                                    Status
                                    <br />
                                    <span
                                      className={
                                        "badge badge-light-" +
                                        (this.state.datax_survey.status == 1
                                          ? "success"
                                          : "danger") +
                                        " fs-7"
                                      }
                                    >
                                      {this.state.datax_survey.status == 1
                                        ? "Publish"
                                        : "Draft"}
                                    </span>
                                  </div>
                                </div>
                                <div>
                                  <div className="mt-5 border-top mx-0 my-10"></div>
                                </div>
                                <h5 className="mt-7 text-muted me-3 mb-5">
                                  Tujuan Clone
                                </h5>
                                <div className="row">
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Nama Survey
                                    </label>
                                    <input
                                      className="form-control form-control-sm"
                                      placeholder="Nama Survey"
                                      name="judul"
                                      id="judul"
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["judul"]}
                                    </span>
                                  </div>

                                  {this.state.jenis_survey != 1 ? (
                                    <div className="col-lg-12 mb-7 mt-3 fv-row">
                                      <label className="form-label required">
                                        Target Akademi Evaluasi
                                      </label>
                                      <div className="row mb-5 mt-3">
                                        {this.state.option_akademi.map(
                                          ({ id, name }, index) => {
                                            return (
                                              <div
                                                className="col-lg-4 mb-3 fv-row"
                                                key={`custom-checkbox-akademi-${index}`}
                                              >
                                                <input
                                                  type="checkbox"
                                                  id={`custom-checkbox-akademi-${id}`}
                                                  name={name}
                                                  value={id}
                                                  checked={
                                                    this.state
                                                      .checkedStateAkademi[
                                                      index
                                                    ]
                                                  }
                                                  onChange={() =>
                                                    this.handleCheckedChangeAkademi(
                                                      index,
                                                    )
                                                  }
                                                />
                                                <label
                                                  className="ms-3"
                                                  id={`custom-checkbox-text-akademi-${id}`}
                                                  htmlFor={`custom-checkbox-akademi-${id}`}
                                                >
                                                  {capitalizeTheFirstLetterOfEachWord(
                                                    name,
                                                  )}
                                                </label>
                                              </div>
                                            );
                                          },
                                        )}
                                      </div>
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["idakademi"]}
                                      </span>
                                    </div>
                                  ) : (
                                    ""
                                  )}

                                  <div
                                    className="col-lg-12 mb-7 fv-row "
                                    style={{ display: "none" }}
                                  >
                                    <label className="form-label required">
                                      Jenis Survey
                                    </label>
                                    <Select
                                      name="jenis_survey"
                                      placeholder="Silahkan pilih"
                                      noOptionsMessage={({ inputValue }) =>
                                        !inputValue
                                          ? this.state.jenis_survey
                                          : "Data tidak tersedia"
                                      }
                                      className="form-select-sm selectpicker p-0"
                                      options={this.jenis_survey}
                                      value={this.state.valJenisSurvey}
                                      isDisabled={true}
                                      onChange={this.handleChangeJenisSurvey}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["jenis_survey"]}
                                    </span>
                                  </div>
                                  {this.state.show_behavior ? (
                                    <div
                                      className="col-lg-12 mb-7 fv-row"
                                      id="behavior"
                                    >
                                      <label className="form-label required">
                                        Behavior Survey
                                      </label>
                                      <Select
                                        name="behavior"
                                        placeholder="Silahkan pilih"
                                        noOptionsMessage={({ inputValue }) =>
                                          !inputValue
                                            ? this.state.behavior_survey
                                            : "Data tidak tersedia"
                                        }
                                        className="form-select-sm selectpicker p-0"
                                        options={this.behavior_survey}
                                        value={this.state.valBehavior}
                                        isDisabled={
                                          this.state.status_disabled == 1
                                            ? true
                                            : false
                                        }
                                        onChange={this.handleChangeBehavior}
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["behavior"]}
                                      </span>
                                    </div>
                                  ) : (
                                    ""
                                  )}
                                  {this.state.show_hide_survey ? (
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label required">
                                        Level
                                      </label>
                                      <Select
                                        id="level_tujuan"
                                        name="level_tujuan"
                                        placeholder="Silahkan pilih"
                                        noOptionsMessage={({ inputValue }) =>
                                          !inputValue
                                            ? this.state.dataxakademi
                                            : "Data tidak tersedia"
                                        }
                                        className="form-select-sm selectpicker p-0"
                                        onChange={this.handleChangeLevelTujuan}
                                        options={
                                          this.state.role_id == 109 ||
                                          this.state.role_id == 1 ||
                                          this.state.role_id == 116
                                            ? this.level_survey
                                            : this.level_survey_general
                                        }
                                      />
                                    </div>
                                  ) : (
                                    ""
                                  )}
                                  {this.state.show_hide_tingkatan ? (
                                    <div>
                                      <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label required">
                                          Akademi
                                        </label>
                                        <Select
                                          id="id_akademi_tujuan"
                                          name="idakademi_tujuan"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.state.dataxakademi
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm selectpicker p-0"
                                          isDisabled={
                                            this.state
                                              .levelAkademiTujuanDisabled
                                          }
                                          onChange={
                                            this.handleChangeAkademiTujuan
                                          }
                                          options={this.state.dataxakademi}
                                          value={this.state.valAkademiTujuan}
                                        />
                                        <span style={{ color: "red" }}>
                                          {
                                            this.state.errors[
                                              "idakademi_tujuan"
                                            ]
                                          }
                                        </span>
                                      </div>
                                      <div className="col-lg-12 mb-7 fv-row hide">
                                        <label className="form-label required">
                                          Tema
                                        </label>
                                        <Select
                                          name="idtema_tujuan"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.state.dataxtema
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm selectpicker p-0"
                                          options={this.state.dataxtematujuan}
                                          isDisabled={
                                            this.state.isDisabledTemaTujuan ||
                                            this.state.levelTemaTujuanDisabled
                                          }
                                          value={this.state.valTemaTujuan}
                                          onChange={this.handleChangeTemaTujuan}
                                        />
                                        <span style={{ color: "red" }}>
                                          {this.state.errors["idtema_tujuan"]}
                                        </span>
                                      </div>
                                      <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label required">
                                          Pelatihan
                                        </label>
                                        <Select
                                          name="idpelatihan_tujuan"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.state.dataxtema
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm selectpicker p-0"
                                          options={
                                            this.state.dataxpelatihantujuan
                                          }
                                          isDisabled={
                                            this.state
                                              .isDisabledPelatihanTujuan ||
                                            this.state
                                              .levelPelatihanTujuanDisabled
                                          }
                                          value={this.state.valPelatihanTujuan}
                                          onChange={
                                            this.handleChangePelatihanTujuan
                                          }
                                        />
                                        <span style={{ color: "red" }}>
                                          {
                                            this.state.errors[
                                              "idpelatihan_tujuan"
                                            ]
                                          }
                                        </span>
                                      </div>
                                    </div>
                                  ) : (
                                    ""
                                  )}

                                  {this.state.show_role ? (
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label required">
                                        Role Peserta
                                      </label>
                                      <div className="row">
                                        <div
                                          className="col-lg-3 mb-3 fv-row"
                                          key="custom-checkbox-999"
                                        >
                                          <input
                                            type="checkbox"
                                            id="custom-checkbox-999"
                                            name="all"
                                            checked={this.state.check_all}
                                            onChange={(e) => {
                                              this.setState(
                                                {
                                                  check_all:
                                                    !this.state.check_all,
                                                },
                                                () => {
                                                  if (this.state.check_all) {
                                                    this.checkAllRole();
                                                  } else {
                                                    this.clearCheckedRole();
                                                  }
                                                },
                                              );
                                            }}
                                          />
                                          <label
                                            className="ms-3"
                                            htmlFor="custom-checkbox-999"
                                          >
                                            {capitalizeTheFirstLetterOfEachWord(
                                              "all",
                                            )}
                                          </label>
                                        </div>
                                        {this.state.option_role_peserta.map(
                                          ({ id, name }, index) => {
                                            return (
                                              <div
                                                className="col-lg-3 mb-3 fv-row"
                                                key={`custom-checkbox-${index}`}
                                              >
                                                <input
                                                  type="checkbox"
                                                  id={`custom-checkbox-${index}`}
                                                  name={name}
                                                  value={id}
                                                  checked={
                                                    this.state.checkedState[
                                                      index
                                                    ]
                                                  }
                                                  onChange={() =>
                                                    this.handleCheckedChange(
                                                      index,
                                                    )
                                                  }
                                                />
                                                <label
                                                  className="ms-3"
                                                  htmlFor={`custom-checkbox-${index}`}
                                                >
                                                  {capitalizeTheFirstLetterOfEachWord(
                                                    name,
                                                  )}
                                                </label>
                                              </div>
                                            );
                                          },
                                        )}
                                      </div>
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["role"]}
                                      </span>
                                    </div>
                                  ) : (
                                    ""
                                  )}
                                </div>
                              </div>
                            </div>
                            {/* MENU 2 */}
                            <div data-kt-stepper-element="content">
                              <div className="col-lg-12 fv-row">
                                {this.state.is_edit_soal ? (
                                  <SurveyForm
                                    id_soal={this.state.id_soal_callback}
                                    id_survey={this.state.id_survey_callback}
                                    nomor_soal={this.state.nomor_soal}
                                    parentCallBack={this.handleCallBackSurvey}
                                  />
                                ) : this.state.is_tambah_soal ? (
                                  <TambahSoalForm
                                    id_survey={this.state.id_survey}
                                    nomor_soal={this.state.nomor_soal}
                                    parentCallBack={this.handleCallBackTambah}
                                  />
                                ) : this.state.is_import_soal ? (
                                  <ImportForm
                                    id_survey={this.state.id_survey}
                                    nomor_soal={this.state.nomor_soal}
                                    parentCallBack={this.handleCallBackImport}
                                  />
                                ) : (
                                  <SurveyCloneList
                                    id_survey={this.state.id_survey}
                                    is_edit_soal={this.state.is_edit_soal}
                                    parentCallBack={this.handleCallBackList}
                                    simpan_tambah={this.state.simpan_tambah}
                                  />
                                )}
                              </div>
                            </div>
                            {/* MENU 3 */}
                            <div data-kt-stepper-element="content">
                              <div className="col-lg-12 fv-row">
                                <div className="row">
                                  {/* <div className="col-lg-12 mb-7 fv-row">
                                                                            <label className="form-label">Tanggal Pendaftaran : 18-04-2022  s.d. 18-04-2022</label><br />
                                                                            <label className="form-label">Tanggal Pelatihan : 19-04-2022  s.d.  19-04-2022</label>
                                                                        </div> */}

                                  {this.state.level_survey_tujuan == 3 &&
                                  this.state.jenis_survey == 1 ? (
                                    <>
                                      <div className="row pt-7">
                                        <h2 className="fs-5 text-muted mb-3">
                                          Target Akademi
                                        </h2>
                                        <div className="col-lg-12">
                                          <div className="table-responsive">
                                            <DataTable
                                              columns={this.columnsAkademi}
                                              data={
                                                this.state.dataxakademi_show
                                              }
                                              progressPending={
                                                this.state.loading_akademi
                                              }
                                              highlightOnHover
                                              pointerOnHover
                                              pagination
                                              paginationServer
                                              paginationTotalRows={
                                                this.state.totalRowsAkademi
                                              }
                                              onChangeRowsPerPage={
                                                this.handlePerRowsChangeAkademi
                                              }
                                              onChangePage={
                                                this.handlePageChangeAkademi
                                              }
                                              customStyles={this.customStyles}
                                              persistTableHead={true}
                                              noDataComponent={
                                                <div className="mt-5">
                                                  Tidak Ada Data
                                                </div>
                                              }
                                            />
                                          </div>
                                        </div>
                                      </div>
                                      <div className="row pt-7">
                                        <h2 className="fs-5 text-muted mb-3">
                                          Target Tema
                                        </h2>
                                        <div className="col-lg-12">
                                          <div className="table-responsive">
                                            <DataTable
                                              columns={this.columnsTema}
                                              data={this.state.dataxtema_show}
                                              progressPending={
                                                this.state.loading_tema
                                              }
                                              highlightOnHover
                                              pointerOnHover
                                              pagination
                                              paginationServer
                                              paginationTotalRows={
                                                this.state.totalRowsTema
                                              }
                                              onChangeRowsPerPage={
                                                this.handlePerRowsChangeTema
                                              }
                                              onChangePage={
                                                this.handlePageChangeTema
                                              }
                                              customStyles={this.customStyles}
                                              persistTableHead={true}
                                              noDataComponent={
                                                <div className="mt-5">
                                                  Tidak Ada Data
                                                </div>
                                              }
                                            />
                                          </div>
                                        </div>
                                      </div>
                                      <div className="row pt-7">
                                        <h2 className="fs-5 text-muted mb-3">
                                          Target Pelatihan
                                        </h2>
                                        <div className="col-lg-12">
                                          <div className="table-responsive">
                                            <DataTable
                                              columns={this.columnsPelatihan}
                                              data={
                                                this.state.dataxpelatihan_show
                                              }
                                              progressPending={
                                                this.state.loading_pelatihan
                                              }
                                              highlightOnHover
                                              pointerOnHover
                                              pagination
                                              paginationServer
                                              paginationTotalRows={
                                                this.state.totalRowsPelatihan
                                              }
                                              onChangeRowsPerPage={
                                                this
                                                  .handlePerRowsChangePelatihan
                                              }
                                              onChangePage={
                                                this.handlePageChangePelatihan
                                              }
                                              customStyles={this.customStyles}
                                              persistTableHead={true}
                                              noDataComponent={
                                                <div className="mt-5">
                                                  Tidak Ada Data
                                                </div>
                                              }
                                            />
                                          </div>
                                        </div>
                                      </div>
                                    </>
                                  ) : (
                                    ""
                                  )}

                                  {this.state.level_survey_tujuan == 0 &&
                                  this.state.jenis_survey == 1 ? (
                                    <>
                                      <div className="row pt-7">
                                        <h2 className="fs-5 text-muted mb-3">
                                          Target Akademi
                                        </h2>
                                        <div className="col-lg-12">
                                          <div className="table-responsive">
                                            <DataTable
                                              columns={this.columnsAkademi}
                                              data={
                                                this.state.dataxakademi_show
                                              }
                                              progressPending={
                                                this.state.loading_akademi
                                              }
                                              highlightOnHover
                                              pointerOnHover
                                              pagination={false}
                                              customStyles={this.customStyles}
                                              persistTableHead={true}
                                              noDataComponent={
                                                <div className="mt-5">
                                                  Tidak Ada Data
                                                </div>
                                              }
                                            />
                                          </div>
                                        </div>
                                      </div>
                                      <div className="row pt-7">
                                        <h2 className="fs-5 text-muted mb-3">
                                          Target Tema
                                        </h2>
                                        <div className="col-lg-12">
                                          <div className="table-responsive">
                                            <DataTable
                                              columns={this.columnsTema}
                                              data={this.state.dataxtema_show}
                                              progressPending={
                                                this.state.loading_tema
                                              }
                                              highlightOnHover
                                              pointerOnHover
                                              pagination
                                              paginationServer
                                              paginationTotalRows={
                                                this.state.totalRowsTema
                                              }
                                              onChangeRowsPerPage={
                                                this.handlePerRowsChangeTema
                                              }
                                              onChangePage={
                                                this.handlePageChangeTema
                                              }
                                              customStyles={this.customStyles}
                                              persistTableHead={true}
                                              noDataComponent={
                                                <div className="mt-5">
                                                  Tidak Ada Data
                                                </div>
                                              }
                                            />
                                          </div>
                                        </div>
                                      </div>
                                      <div className="row pt-7">
                                        <h2 className="fs-5 text-muted mb-3">
                                          Target Pelatihan{" "}
                                        </h2>
                                        <div className="col-lg-12">
                                          <div className="table-responsive">
                                            <DataTable
                                              columns={this.columnsPelatihan}
                                              data={
                                                this.state.dataxpelatihan_show
                                              }
                                              progressPending={
                                                this.state.loading_pelatihan
                                              }
                                              highlightOnHover
                                              pointerOnHover
                                              pagination
                                              paginationServer
                                              paginationTotalRows={
                                                this.state.totalRowsPelatihan
                                              }
                                              onChangeRowsPerPage={
                                                this
                                                  .handlePerRowsChangePelatihan
                                              }
                                              onChangePage={
                                                this.handlePageChangePelatihan
                                              }
                                              customStyles={this.customStyles}
                                              persistTableHead={true}
                                              noDataComponent={
                                                <div className="mt-5">
                                                  Tidak Ada Data
                                                </div>
                                              }
                                            />
                                          </div>
                                        </div>
                                      </div>
                                    </>
                                  ) : (
                                    ""
                                  )}

                                  {this.state.level_survey_tujuan == 1 &&
                                  this.state.jenis_survey == 1 ? (
                                    <>
                                      <div className="row pt-7">
                                        <h2 className="fs-5 text-muted mb-3">
                                          Target Tema
                                        </h2>
                                        <div className="col-lg-12">
                                          <div className="table-responsive">
                                            <DataTable
                                              columns={this.columnsTema}
                                              data={this.state.dataxtema_show}
                                              progressPending={
                                                this.state.loading_tema
                                              }
                                              highlightOnHover
                                              pointerOnHover
                                              pagination={false}
                                              customStyles={this.customStyles}
                                              persistTableHead={true}
                                              noDataComponent={
                                                <div className="mt-5">
                                                  Tidak Ada Data
                                                </div>
                                              }
                                            />
                                          </div>
                                        </div>
                                      </div>
                                      <div className="row pt-7">
                                        <h2 className="fs-5 text-muted mb-3">
                                          Target Pelatihan
                                        </h2>
                                        <div className="col-lg-12">
                                          <div className="table-responsive">
                                            <DataTable
                                              columns={this.columnsPelatihan}
                                              data={
                                                this.state.dataxpelatihan_show
                                              }
                                              progressPending={
                                                this.state.loading_pelatihan
                                              }
                                              highlightOnHover
                                              pointerOnHover
                                              pagination
                                              paginationServer
                                              paginationTotalRows={
                                                this.state.totalRowsPelatihan
                                              }
                                              onChangeRowsPerPage={
                                                this
                                                  .handlePerRowsChangePelatihan
                                              }
                                              onChangePage={
                                                this.handlePageChangePelatihan
                                              }
                                              customStyles={this.customStyles}
                                              persistTableHead={true}
                                              noDataComponent={
                                                <div className="mt-5">
                                                  Tidak Ada Data
                                                </div>
                                              }
                                            />
                                          </div>
                                        </div>
                                      </div>
                                    </>
                                  ) : (
                                    ""
                                  )}
                                  {this.state.level_survey_tujuan == 2 &&
                                  this.state.jenis_survey == 1 ? (
                                    <>
                                      <div className="row pt-7">
                                        <h2 className="fs-5 text-muted mb-3">
                                          Target Pelatihan
                                        </h2>
                                        <div className="col-lg-12">
                                          <div className="table-responsive">
                                            <DataTable
                                              columns={this.columnsPelatihan}
                                              data={
                                                this.state.dataxpelatihan_show
                                              }
                                              progressPending={
                                                this.state.loading_pelatihan
                                              }
                                              highlightOnHover
                                              pointerOnHover
                                              pagination={false}
                                              customStyles={this.customStyles}
                                              persistTableHead={true}
                                              noDataComponent={
                                                <div className="mt-5">
                                                  Tidak Ada Data
                                                </div>
                                              }
                                            />
                                          </div>
                                        </div>
                                      </div>
                                    </>
                                  ) : (
                                    ""
                                  )}
                                  <div>
                                    <div className="mt-5 border-top mx-0 my-10"></div>
                                  </div>
                                  <h2 className="fs-5 mb-5">
                                    Pelaksanaan Survey
                                  </h2>
                                  {
                                    //jika survey evaluasi tidak memilih tanggal
                                    this.state.show_hide_tanggal ? (
                                      <div className="row">
                                        <div className="col-lg-6 mb-7">
                                          <label className="form-label required">
                                            Tgl. Mulai Survey
                                          </label>
                                          <input
                                            className="form-control form-control-sm"
                                            placeholder="Tanggal Mulai Pelaksanaan"
                                            name="start_at"
                                            id="date_survey_start"
                                          />
                                          <span style={{ color: "red" }}>
                                            {this.state.errors["start_at"]}
                                          </span>
                                        </div>
                                        <div className="col-lg-6 mb-7">
                                          <label className="form-label required">
                                            Tgl. Akhir Survey
                                          </label>
                                          <input
                                            className="form-control form-control-sm"
                                            placeholder="Tanggal Selesai Pelaksanaan"
                                            name="end_at"
                                            id="date_survey_end"
                                          />
                                          <span style={{ color: "red" }}>
                                            {this.state.errors["end_at"]}
                                          </span>
                                        </div>
                                      </div>
                                    ) : (
                                      <div></div>
                                    )
                                  }
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Status
                                    </label>
                                    <Select
                                      name="status"
                                      placeholder="Silahkan pilih"
                                      noOptionsMessage={({ inputValue }) =>
                                        !inputValue
                                          ? this.state.datax
                                          : "Data tidak tersedia"
                                      }
                                      className="form-select-sm selectpicker p-0"
                                      options={this.optionstatus}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["status"]}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="text-center border-top pt-10 my-7">
                              <button
                                className="btn btn-light btn-md me-3 mr-2"
                                data-kt-stepper-action="previous"
                              >
                                <i className="fa fa-chevron-left me-1"></i>
                                Sebelumnya
                              </button>
                              <button
                                type="submit"
                                className="btn btn-primary btn-md"
                                data-kt-stepper-action="submit"
                              >
                                <i className="fa fa-paper-plane ms-1"></i>Simpan
                              </button>
                              <button
                                type="button"
                                className="btn btn-primary btn-md"
                                data-kt-stepper-action="next"
                              >
                                <i className="fa fa-chevron-right"></i>Lanjutkan
                              </button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="temp_image"></div>
      </div>
    );
  }
}
