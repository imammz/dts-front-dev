import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import {
  capitalizeTheFirstLetterOfEachWord,
  indonesianDateFormat,
} from "../../publikasi/helper";

export default class ReportIndividu extends React.Component {
  constructor(props) {
    super(props);
    this.handleKembali = this.handleKembaliAction.bind(this);
  }
  state = {
    datax: [],
    loading: true,
    tempLastNumber: 0,
    nama: "",
    nama_survey: "",
    nomor_registrasi: "",
    nama_pelatihan: "",
    nama_akademi: "",
    tgl_pengerjaan: "",
    menit: "",
    individu: {},
  };
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  columns = [
    {
      name: "No",
      sortable: false,
      center: true,
      width: "70px",
      grow: 1,
      cell: (row, index) => this.state.tempLastNumber + index + 1,
    },
    {
      name: "Pertanyaan",
      sortable: false,
      wrap: true,
      allowOverflow: false,
      width: "600px",
      selector: (row) => (
        <div>
          <div dangerouslySetInnerHTML={{ __html: row.soal }}></div>
        </div>
      ),
    },
    {
      name: "Jawaban",
      sortable: false,
      wrap: true,
      allowOverflow: false,
      selector: (row) => (
        <div>
          <div dangerouslySetInnerHTML={{ __html: row.jawaban }}></div>
        </div>
      ),
    },
    {
      name: "Tipe Pertanyaan",
      sortable: false,
      wrap: true,
      allowOverflow: false,
      selector: (row) => (
        <div>
          <span className={"badge badge-light-success"}>{row.type}</span>
        </div>
      ),
    },
  ];

  componentDidMount() {
    const temp_storage = localStorage.getItem("dataMenus");
    const individu = localStorage.getItem("individu_survey");
    const individu_substansi = localStorage.getItem("individu_substansi");
    this.setState({
      individu: JSON.parse(individu),
    });
    localStorage.clear();
    localStorage.setItem("individu_survey", individu);
    localStorage.setItem("individu_substansi", individu_substansi);
    localStorage.setItem("dataMenus", temp_storage);
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    let segment_url = window.location.pathname.split("/");
    let id_survey = segment_url[4];
    let id_user = segment_url[5];
    let id_training = segment_url[6];

    const data = {
      id_survey: id_survey,
      user_id: id_user,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/survey/export_report_individu",
        data,
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        let nama = "";
        let nomor_registrasi = "";
        let nama_pelatihan = "";
        let nama_akademi = "";
        let tgl_pengerjaan = "";
        let menit = "";
        const datax = [];
        if (statux) {
          const result_data = res.data.result.Data;
          result_data.forEach(function (element, i) {
            if (
              element.ptraining_id == id_training &&
              element.status == "sudah mengerjakan"
            ) {
              const jawaban = JSON.parse(element.pjawaban);
              nama = element.pnama;
              nomor_registrasi = element.pnoreg;
              nama_pelatihan = element.ppelatihan;
              nama_akademi = element.pakademi;
              tgl_pengerjaan = element.ptgl_pengerjaan;
              menit = element.pmenit;
              jawaban.forEach(function (element2, j) {
                const type = element2.type;
                let jawaban = "";
                const question = {
                  soal: element2.question,
                };
                if (type == "objective") {
                  const participant_answer = element2.participant_answer;
                  const list_answer = JSON.parse(element2.answer);
                  list_answer.forEach(function (element3, k) {
                    if (element3.key == participant_answer) {
                      jawaban = element3.key + ". " + element3.option;
                      question.jawaban = jawaban;
                      question.type = "Objective";
                    }
                  });
                  datax.push(question);
                } else if (type == "skala") {
                  const participant_answer = element2.participant_answer;
                  const list_answer = JSON.parse(element2.answer);
                  list_answer.forEach(function (element3, k) {
                    if (element3.key == participant_answer) {
                      jawaban = element3.option;
                      question.jawaban = jawaban;
                      question.type = "Skala";
                    }
                  });
                  datax.push(question);
                } else if (type == "pertanyaan_terbuka") {
                  const participant_answer = element2.participant_answer;
                  jawaban = participant_answer;
                  question.jawaban = jawaban;
                  question.type = "Pertanyaan Terbuka";
                  datax.push(question);
                } else if (type == "multiple_choice") {
                  const participant_answer = element2.participant_answer;
                  const list_answer = JSON.parse(element2.answer);
                  const arr_jawaban = [];
                  participant_answer.forEach(function (element3, k) {
                    list_answer.forEach(function (element4, k) {
                      if (element3 == element4.key) {
                        arr_jawaban.push(element4.key + ". " + element4.option);
                      }
                    });
                  });
                  question.jawaban = arr_jawaban.join(",");
                  question.type = "Multiple Choice";
                  datax.push(question);
                } else if (type == "triggered_question") {
                  const participant_answer = element2.participant_answer;
                  const list_answer = JSON.parse(element2.answer);
                  console.log(list_answer);
                  list_answer.forEach(function (element3, k) {
                    if (element3.key == participant_answer) {
                      jawaban = element3.key + ". " + element3.option;
                      const arr_soal = [];
                      const arr_jawaban = [];
                      arr_soal.push("<li>" + question.soal + "</li>");
                      arr_jawaban.push("<li>" + jawaban + "</li>");
                      /* question.jawaban = jawaban; */
                      question.type = "Triggered Question";
                      //datax.push(question);
                      const sub = element3.sub;
                      if (sub) {
                        sub.forEach(function (subelement, l) {
                          /* const question_sub = {
                            soal: subelement.question,
                          }; */
                          arr_soal.push("<li>" + subelement.question + "</li>");
                          const sub_answer = subelement.participant_answer;
                          const list_sub_answer = subelement.answer;
                          list_sub_answer.forEach(function (sanswer, m) {
                            if (sanswer.key == sub_answer) {
                              /*  question_sub.jawaban =
                                 sanswer.key + ". " + sanswer.option;
                               question_sub.type = "Triggered Question Child";
                               datax.push(question_sub); */
                              arr_jawaban.push(
                                "<li>" +
                                  sanswer.key +
                                  ". " +
                                  sanswer.option +
                                  "</li>",
                              );
                            }
                          });
                          if (!sub_answer) {
                            arr_jawaban.push("<li>-</li>");
                          }
                        });
                      }
                      const join_soal = arr_soal.join("");
                      const join_jawaban = arr_jawaban.join("");
                      question.soal = "<ul>" + join_soal + "</ul>";
                      question.jawaban = "<ul>" + join_jawaban + "</ul>";
                      datax.push(question);
                    }
                  });
                }
              });
            }
          });
          const tanggal_indo = indonesianDateFormat(tgl_pengerjaan);
          const tgl = tanggal_indo.split(" ");
          tgl_pengerjaan = tgl[2] + " " + tgl[1] + " " + tgl[0];
          this.setState({
            loading: false,
            datax: datax,
            nama: nama,
            nomor_registrasi,
            nama_akademi,
            nama_pelatihan,
            tgl_pengerjaan,
            menit,
          });
          console.log(individu);

          /* const datax = res.data.result.Data[0];
                    this.setState({
                        nama_survey: datax.nama_survey,
                    }); */
        }
      })
      .catch((error) => {});
  }

  handleKembaliAction() {
    window.history.back();
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <path
                        opacity="0.3"
                        d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                        fill="#7239ea"
                      ></path>
                      <path
                        d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                        fill="#7239ea"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Subvit
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                    Survey
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Report Individu
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <a
                  onClick={this.handleKembali}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-7"
          id="kt_wrapper"
        >
          <div
            className="d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-5">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Report Individu
                        </h1>
                      </div>
                      <div className="card-toolbar"></div>
                    </div>
                    <div className="card-body">
                      <div className="row mb-5">
                        <div className="col-lg-12 col-12 col-md-12">
                          <div className="d-flex flex-row">
                            <label className="d-flex flex-stack align-items-left">
                              <span className="d-flex align-items-center me-2">
                                <span className="symbol symbol-50px me-6">
                                  <span className="symbol-label bg-light-primary">
                                    <span className="svg-icon svg-icon-1 svg-icon-primary">
                                      <img
                                        src={`${
                                          process.env.REACT_APP_BASE_API_URI
                                        }/download/get-file?path=${
                                          this.state.individu.foto || ""
                                        }`}
                                        alt=""
                                        className="symbol-label"
                                      />
                                    </span>
                                  </span>
                                </span>
                                <span className="d-flex flex-column">
                                  <span className="fs-7 fw-semibold text-muted">
                                    {this.state.nomor_registrasi}
                                  </span>
                                  <span
                                    className="fw-bolder fs-7"
                                    style={{
                                      overflow: "hidden",
                                      whiteSpace: "normal",
                                    }}
                                  >
                                    {capitalizeTheFirstLetterOfEachWord(
                                      this.state.nama,
                                    )}
                                  </span>
                                </span>
                              </span>
                            </label>
                            <label className="d-flex flex-stack align-items-left ms-4">
                              <span className="d-flex align-items-center me-2">
                                <span className="d-flex flex-column">
                                  <h6 className="fw-bolder fs-7 mb-0">
                                    {this.state.nama_pelatihan}
                                  </h6>
                                  <span className="text-muted fs-7 fw-semibold">
                                    {this.state.nama_akademi}
                                  </span>
                                </span>
                              </span>
                            </label>

                            <div className="d-flex ms-4">
                              <div data-tag="allowRowEvents">
                                <div>
                                  <i className="bi bi-calendar me-1"></i>
                                  {this.state.tgl_pengerjaan}
                                </div>
                                <div>
                                  <i className="bi bi-clock me-1"></i>
                                  {this.state.menit} Menit
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="table-responsive">
                        <DataTable
                          columns={this.columns}
                          data={this.state.datax}
                          progressPending={this.state.loading}
                          highlightOnHover
                          pointerOnHover
                          pagination={false}
                          customStyles={this.customStyles}
                          persistTableHead={true}
                          noDataComponent={
                            <div className="mt-5">Tidak Ada Data</div>
                          }
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
