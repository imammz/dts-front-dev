import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import { capitalizeTheFirstLetterOfEachWord } from "../../../publikasi/helper";
import { fixBootstrapDropdown } from "../../../../utils/commonutil";

export default class SurveyCloneList extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickEditSoal = this.handleClickEditSoalAction.bind(this);
    this.handleTambah = this.handleTambahAction.bind(this);
    this.handleImport = this.handleImportAction.bind(this);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleChangeSearch = this.handleChangeSearchAction.bind(this);
    this.onItemCheck = this.onItemCheckAction.bind(this);
    this.rowSelect = this.rowSelectCriteria.bind(this);
    this.state = {
      datax: [],
      loading: false,
      tempLastNumber: 0,
      newPerPage: 100,
      totalRows: 0,
      currentPage: 0,
      isSearch: false,
      akademi_id: this.props.akademi_id,
      theme_id: this.props.tema_id,
      pelatihan_id: this.props.pelatihan_id,
      param: "",
    };
  }

  rowSelectCriteria(row) {
    /* if (row.pidsurvey_detail > 0) {
            return true;
        } */
    return true;
  }

  columns = [
    {
      name: "No",
      center: true,
      width: "70px",
      cell: (row, index) => this.state.tempLastNumber + index + 1,
    },
    {
      name: "Soal",
      className: "min-w-300px mw-300px",
      grow: 6,
      sortable: true,
      sortField: "question",
      wrap: true,
      allowOverflow: false,
      selector: (row) => (
        <>
          <label className="d-flex flex-stack mb- mt-1">
            <span className="d-flex align-items-center me-2">
              <span className="d-flex flex-column">
                <h6 className="fw-bolder fs-7 mb-0">{row.pertanyaan}</h6>
              </span>
            </span>
          </label>
        </>
      ),
    },
    {
      className: "min-w-200px mw-200px",
      sortType: "basic",
      sortable: true,
      name: "Tipe Soal",
      grow: 3,
      selector: (row) =>
        capitalizeTheFirstLetterOfEachWord(row.type.split("_").join(" ")),
    },
    {
      name: "Aksi",
      center: true,
      width: "250px",
      grow: 3,
      cell: (row, index) => {
        return (
          <div>
            <a
              href="#"
              id_soal={row.pidsurvey_detail}
              id_survey={row.pidsurvey}
              nomor_soal={this.state.tempLastNumber + index + 1}
              title="Edit"
              onClick={this.handleClickEditSoal}
              className="btn btn-icon btn-bg-warning btn-sm me-1"
            >
              <i className="bi bi-gear-fill text-white"></i>
            </a>
          </div>
        );
      },
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    let elements = document.getElementsByName("select-all-rows");
    elements[0].style.display = "none";
    this.setState(
      {
        datax: [],
        id_survey: this.props.id_survey,
      },
      () => {
        this.handleReload();
      },
    );
  }

  componentDidUpdate(prevProps) {
    if (prevProps.is_edit_soal !== this.props.is_edit_soal) {
      this.setState(
        {
          datax: [],
          id_survey: this.props.id_survey,
        },
        () => {
          this.handleReload();
        },
      );
    }
  }

  handleReload(page, newPerPage) {
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 200;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }

    if (this.props.simpan_tambah) {
      console.log("from tambah");
      //check total Rows
      const dataBodyCheck = {
        id_survey: this.state.id_survey,
        start: 0,
        rows: 1,
      };
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/survey/list_soal_survey_byid",
          dataBodyCheck,
          this.configs,
        )
        .then((res) => {
          const jml_data = res.data.result.JumlahData;
          const last_page = Math.floor(jml_data / this.state.newPerPage);
          const start = last_page == 0 ? 0 : last_page * this.state.newPerPage;
          start_tmp = start;
          this.setState({ tempLastNumber: start_tmp });
          this.loadData(start_tmp, length_tmp, page, true);
        });
    } else {
      console.log("normal");
      this.setState({ tempLastNumber: start_tmp });
      this.loadData(start_tmp, length_tmp, page, false);
    }
  }

  loadData(start_tmp, length_tmp, page, is_tambah) {
    const dataBody = {
      id_survey: this.state.id_survey,
      start: start_tmp,
      rows: length_tmp,
      param: this.state.param,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/survey/list_soal_survey_byid",
        dataBody,
        this.configs,
      )
      .then((res) => {
        this.setState({ loading: false });
        const datax = res.data.result.Data;
        datax.forEach(function (element, i) {
          let edit = JSON.parse(
            localStorage.getItem("_" + element.pidsurvey_detail),
          );

          if (edit) {
            datax[i] = edit;
            localStorage.setItem(
              datax[i].pidsurvey_detail,
              JSON.stringify(edit),
            );
            //localStorage.removeItem("_" + element.pidsurvey_detail);
          } else {
            if (element.jawaban != "undefined") {
              element.jawaban = JSON.parse(element.jawaban);
            } else {
              element.jawaban = "";
            }
            localStorage.setItem(
              element.pidsurvey_detail,
              JSON.stringify(element),
            );
          }
        });
        let totalRows = res.data.result.JumlahData;
        let newPerPage = this.state.newPerPage;
        const itemTambah = JSON.parse(localStorage.getItem("TAMBAH"));
        if (itemTambah) {
          itemTambah.forEach(function (element, i) {
            const item = JSON.parse(localStorage.getItem(element));
            if (item) {
              datax.push(item);
              if (datax.length > newPerPage) {
                //datax.pop();
                //page = page==undefined?2:(page + 1);
              }
              totalRows++;
            }
          });
        }

        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;

        if (statusx) {
          this.setState({ datax });
          this.setState({ loading: false });
          this.setState({ totalRows: totalRows });
          this.setState({ currentPage: page });
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.handleReload();
              }
            });
        }
      })
      .catch((error) => {
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      });
  }

  handlePageChange = (page) => {
    this.setState({ loading: true });
    this.handleReload(page, this.state.newPerPage);
  };
  handlePerRowsChange = async (newPerPage, page) => {
    this.setState({ loading: true });
    this.setState({ newPerPage: newPerPage });
    this.handleReload(page, newPerPage);
  };

  handleClickEditSoalAction(e) {
    const id_soal = e.currentTarget.getAttribute("id_soal");
    const id_survey = e.currentTarget.getAttribute("id_survey");
    const nomor_soal = e.currentTarget.getAttribute("nomor_soal");

    const callBackVal = {
      is_edit_soal: true,
      is_tambah_soal: false,
      is_import_soal: false,
      id_soal: id_soal,
      id_survey: id_survey,
      nomor_soal: nomor_soal,
    };
    this.props.parentCallBack(callBackVal);
  }

  handleTambahAction(e) {
    const callBackVal = {
      is_tambah_soal: true,
      is_edit_soal: false,
      is_import_soal: false,
      nomor_soal: this.state.totalRows + 1,
    };
    this.props.parentCallBack(callBackVal);
  }

  handleImportAction(e) {
    const callBackVal = {
      is_tambah_soal: false,
      is_edit_soal: false,
      is_import_soal: true,
      nomor_soal: this.state.totalRows + 1,
    };
    this.props.parentCallBack(callBackVal);
  }

  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    //this.setState({param:searchText});
    if (e.key == "Enter") {
      e.preventDefault();
      this.setState({ loading: true });
      this.setState({ param: searchText }, () => {
        this.handleReload();
      });
    }
  }

  handleChangeSearchAction(e) {
    const searchText = e.currentTarget.value;
    if (searchText == "") {
      this.setState(
        {
          loading: true,
          param: "",
        },
        () => {
          this.handleReload();
        },
      );
    }
  }

  onItemCheckAction(sel) {
    const selectedRows = sel.selectedRows;
    const datax = this.state.datax;
    const keyStorage = "uncheck";
    datax.forEach(function (element, i) {
      const d_id = element.pidsurvey_detail;
      let found = false;
      selectedRows.forEach(function (selected, j) {
        const s_id = selected.pidsurvey_detail;
        if (d_id == s_id) {
          found = true;
        }
      });
      if (!found) {
        let old = JSON.parse(localStorage.getItem(keyStorage));
        if (old !== null) {
          if (!old.includes(d_id)) {
            old.push(d_id);
          }
          localStorage.setItem(keyStorage, JSON.stringify(old));
        } else {
          localStorage.setItem(keyStorage, JSON.stringify([d_id]));
        }
      }
    });
    //crosscheck
    selectedRows.forEach(function (selected, j) {
      const s_id = selected.pidsurvey_detail;
      let old = JSON.parse(localStorage.getItem(keyStorage));
      if (old !== null) {
        old.forEach(function (ol, i) {
          if (s_id == ol) {
            const filteredArray = old.filter(function (e) {
              return e !== ol;
            });
            localStorage.setItem(keyStorage, JSON.stringify(filteredArray));
          }
        });
      }
    });
  }

  render() {
    return (
      <div>
        <div className="card-header m-0 p-0">
          <div className="card-title">
            <span className="svg-icon svg-icon-1 position-absolute ms-6">
              <svg
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
                className="mh-50px"
              >
                <rect
                  opacity="0.5"
                  x="17.0365"
                  y="15.1223"
                  width="8.15546"
                  height="2"
                  rx="1"
                  transform="rotate(45 17.0365 15.1223)"
                  fill="currentColor"
                ></rect>
                <path
                  d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                  fill="currentColor"
                ></path>
              </svg>
            </span>
            <input
              type="text"
              data-kt-user-table-filter="search"
              className="form-control form-control-sm form-control-solid w-250px ps-14"
              placeholder="Cari Soal"
              onKeyPress={this.handleKeyPress}
              onChange={this.handleChangeSearch}
            />
          </div>
          <div className="card-toolbar">
            <div className="d-flex align-items-center position-relative my-1 me-2">
              <div className="dropdown d-inline">
                <button
                  className="btn btn-light fw-bolder btn-sm dropdown-toggle fs-7 me-2"
                  type="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kelola Soal
                  </span>
                </button>
                <ul
                  className="dropdown-menu"
                  style={{ position: "absolute", zIndex: "999999" }}
                >
                  <li>
                    <a
                      href="#"
                      onClick={this.handleTambah}
                      className="dropdown-item px-5 my-1"
                    >
                      <i className="bi bi-plus-circle text-dark  me-1"></i>
                      Tambah Soal
                    </a>
                  </li>
                  <li>
                    <a
                      href="#"
                      onClick={this.handleImport}
                      className="dropdown-item px-5"
                    >
                      <i className="bi bi-cloud-download text-dark  me-1"></i>
                      Import Soal
                    </a>
                  </li>
                </ul>
              </div>

              {/* <a href="#" className="btn btn-light fw-bolder btn-sm ms-2" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-flip="top-end">
                                <span className="d-md-inline d-lg-inline d-xl-inline d-none">Kelola Soal</span>
                                <i className="bi bi-chevron-down ms-1"></i>
                            </a>
                            <div className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-7 w-auto " data-kt-menu="true">
                                <div>

                                    <div className="menu-item">
                                        <a href="#" onClick={this.handleTambah} className="menu-link px-5">
                                            <i className="bi bi-plus-circle text-dark  me-1"></i>
                                            Tambah Soal
                                        </a></div>

                                    <div className="menu-item">
                                        <a href="#" onClick={this.handleImport} className="menu-link px-5">
                                            <i className="bi bi-cloud-download text-dark  me-1"></i>
                                            Import Soal
                                        </a></div>

                                </div>
                            </div> */}
            </div>
          </div>
        </div>
        <div className="table-responsive">
          <DataTable
            columns={this.columns}
            data={this.state.datax}
            progressPending={this.state.loading}
            highlightOnHover
            pointerOnHover
            pagination={false}
            paginationServer
            paginationTotalRows={this.state.totalRows}
            onChangeRowsPerPage={this.handlePerRowsChange}
            onChangePage={this.handlePageChange}
            customStyles={this.customStyles}
            paginationPerPage={200}
            persistTableHead={true}
            selectableRows
            onSelectedRowsChange={this.onItemCheck}
            selectableRowSelected={this.rowSelect}
            noDataComponent={<div className="mt-5">Tidak Ada Data</div>}
          />
        </div>
      </div>
    );
  }
}
