import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import {
  capitalizeTheFirstLetterOfEachWord,
  dmyToYmd,
  indonesianDateFormat,
  ymdToDmy,
} from "./../../publikasi/helper";
import DataTable from "react-data-table-component";

export default class SurveyEdit extends React.Component {
  constructor(props) {
    super(props);
    this.handleChangeJudul = this.handleChangeJudulAction.bind(this);
    this.handleChangeStatus = this.handleChangeStatusAction.bind(this);
    this.handleChangeJenisSurvey =
      this.handleChangeJenisSurveyAction.bind(this);
    this.handleChangeBehavior = this.handleChangeBehaviorAction.bind(this);
    this.handleSubmit = this.handleSubmitAction.bind(this);
    this.handleKembali = this.handleKembaliAction.bind(this);
    this.handleHapus = this.handleHapusAction.bind(this);
  }

  state = {
    id_survey: 0,
    nama_survey: "",
    datax: [],
    judul: "",
    errors: {},
    valStatus: [],
    isDisabledJenisSurvey: true,
    isDisabledBehavior: false,
    option_role_peserta: [],
    checkedState: [],
    arr_id_checked: [],
    start_at: 0,
    end_at: 0,
    question_to_share: 0,
    status: 1,
    status_disabled: 1,
    status_batal: false,
    duration: 0,
    jenis_survey: 1,
    behavior_survey: 1,
    valJenisSurvey: [],
    valBehavior: [],
    show_behavior: true,
    show_hide_tanggal: true,
    kategori_master: [],
    valKategoriMaster: [],
    option_akademi: [],
    checkedStateAkademi: [],
    arr_id_checked_akademi: [],
    datax_pelatihan: [],
    datax_tema: [],
    datax_akademi: [],
    datax_all_akademi: [],
    datax_all_tema: [],
    datax_all_pelatihan: [],
    datax_all_pelatihan_filter: [],
    totalRowsAkademi: 0,
    totalRowsTema: 0,
    totalRowsPelatihan: 0,
    tempLastNumberAkademi: 0,
    tempLastNumberTema: 0,
    tempLastNumberPelatihan: 0,
    newPerPageAkademi: 10,
    newPerPageTema: 10,
    newPerPagePelatihan: 10,
    currentPageAkademi: 0,
    currentPageTema: 0,
    currentPagePelatihan: 0,
    status_survey: null,
  };

  level_survey = ["Akademi", "Tema", "Pelatihan", "Seluruh Populasi DTS"];

  jenis_survey_label = [
    "Evaluasi",
    "Umum",
    "Evaluasi Online",
    "Evaluasi Offline",
    "Evaluasi Online & Offline",
  ];

  optionstatus = [
    { value: 0, label: "Draft" },
    { value: 1, label: "Publish" },
  ];

  optionstatusevaluasi = [
    { value: 1, label: "Publish" },
    { value: 2, label: "Batal" },
  ];

  jenis_survey = [
    { value: 0, label: "Evaluasi" },
    { value: 1, label: "Umum" },
    { value: 2, label: "Evaluasi Online" },
    { value: 3, label: "Evaluasi Offline" },
    { value: 4, label: "Evaluasi Hybrid" },
  ];
  behavior_survey = [
    { value: 0, label: "Optional" },
    { value: 1, label: "Mandatory" },
  ];

  columnsAkademi = [
    {
      name: "No",
      cell: (row, index) => this.state.tempLastNumberAkademi + index + 1,
      width: "70px",
    },
    {
      name: "Akademi",
      sortable: false,
      selector: (row) => row.akademi,
    },
    {
      name: "Pelatihan",
      sortable: false,
      selector: (row) => row.jml_pelatihan,
    },
    {
      name: "Status",
      sortable: true,
      center: true,
      width: "100px",
      selector: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.status == "1" ? "success" : "danger") +
              " fs-7 m-1"
            }
          >
            {row.status == 1 ? "Publish" : "Unpublish"}
          </span>
        </div>
      ),
    },
  ];

  columnsTema = [
    {
      name: "No",
      cell: (row, index) => this.state.tempLastNumberTema + index + 1,
      width: "70px",
    },
    {
      name: "Tema",
      sortable: false,
      selector: (row) => row.tema,
    },
    {
      name: "Pelatihan",
      sortable: false,
      selector: (row) => row.jml_pelatihan,
    },
    {
      name: "Status",
      sortable: true,
      center: true,
      width: "100px",
      selector: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.status == "1" ? "success" : "danger") +
              " fs-7 m-1"
            }
          >
            {row.status == 1 ? "Publish" : "Unpublish"}
          </span>
        </div>
      ),
    },
  ];

  columnsPelatihan = [
    {
      name: "No",
      cell: (row, index) => this.state.tempLastNumberPelatihan + index + 1,
      width: "70px",
    },
    {
      name: "Nama Pelatihan",
      sortable: false,
      width: "300px",
      selector: (row) => (
        <div className="mt-2">
          <label className="d-flex flex-stack mb- mt-1">
            <span className="d-flex align-items-center me-2">
              <span className="d-flex flex-column">
                <h6 className="fw-bolder fs-7 mb-0">{row.pelatihan}</h6>
                <span className="fs-7 text-muted fw-semibold">
                  {row.akademi}
                </span>
              </span>
            </span>
          </label>
        </div>
      ),
    },
    {
      name: "Jadwal Pelatihan",
      sortable: false,
      width: "300px",
      selector: (row) =>
        indonesianDateFormat(row.pelatihan_mulai) +
        " - " +
        indonesianDateFormat(row.pelatihan_selesai),
    },
    {
      name: "Status Pelatihan",
      sortable: false,
      width: "150px",
      selector: (row) => row.status_pelatihan,
    },
    {
      name: "Jumlah Peserta",
      sortable: false,
      width: "150px",
      selector: (row) => row.jml_peserta,
    },
    {
      name: "Status",
      sortable: true,
      center: true,
      selector: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.status_publish == "1" ? "success" : "danger") +
              " fs-7 m-1"
            }
          >
            {row.status_publish == 1 ? "Publish" : "Unpublish"}
          </span>
        </div>
      ),
    },
  ];

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    let segment_url = window.location.pathname.split("/");
    let id_survey = segment_url[4];
    this.setState({
      id_survey,
    });

    const data = {
      id: id_survey,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/survey/survey_select_byid",
        data,
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          //showing akademi, tema, pelatihan (20/12/2022)
          const datax_all_pelatihan = res.data.result.pelatihan;
          const datax_all_pelatihan_filter = datax_all_pelatihan.filter(
            (element) => element.pelatihan_id !== null,
          );
          const datax_all_akademi = [];
          const datax_all_tema = [];
          datax_all_pelatihan.forEach(function (pelatihan, i) {
            if (pelatihan.tema_id != null) {
              const tema = {
                tema: pelatihan.tema,
                tema_id: pelatihan.tema_id,
                status: pelatihan.status_tema,
              };

              let is_tema_push = true;
              datax_all_tema.forEach(function (singleTema, i) {
                if (singleTema.tema_id == pelatihan.tema_id) {
                  is_tema_push = false;
                }
              });
              if (is_tema_push) {
                datax_all_tema.push(tema);
              }
            }

            if (pelatihan.akademi_id != null) {
              const akademi = {
                akademi: pelatihan.akademi,
                akademi_id: pelatihan.akademi_id,
                status: pelatihan.status_akademi,
              };

              let is_akademi_push = true;
              datax_all_akademi.forEach(function (singleAkademi, i) {
                if (singleAkademi.akademi_id == pelatihan.akademi_id) {
                  is_akademi_push = false;
                }
              });
              if (is_akademi_push) {
                datax_all_akademi.push(akademi);
              }
            }
          });
          //hitung pelatihan di akademi
          datax_all_akademi.forEach(function (akademi, i) {
            akademi.jml_pelatihan = 0;
            datax_all_pelatihan.forEach(function (pelatihan, j) {
              if (
                pelatihan.akademi_id == akademi.akademi_id &&
                pelatihan.pelatihan_id != null
              ) {
                akademi.jml_pelatihan = akademi.jml_pelatihan + 1;
              }
            });
          });

          //hitung tema di akademi
          datax_all_tema.forEach(function (tema, i) {
            tema.jml_pelatihan = 0;
            datax_all_pelatihan.forEach(function (pelatihan, j) {
              if (
                pelatihan.tema_id == tema.tema_id &&
                pelatihan.tema_id != null
              ) {
                tema.jml_pelatihan = tema.jml_pelatihan + 1;
              }
            });
          });

          const datax_pelatihan = datax_all_pelatihan_filter.slice(0, 10);
          const datax_tema = datax_all_tema.slice(0, 10);
          const datax_akademi = datax_all_akademi.slice(0, 10);
          const totalRowsAkademi = datax_all_akademi.length;
          const totalRowsTema = datax_all_tema.length;
          const totalRowsPelatihan = datax_all_pelatihan.length;

          this.setState({
            datax_pelatihan,
            datax_tema,
            datax_akademi,
            datax_all_akademi,
            datax_all_tema,
            datax_all_pelatihan,
            datax_all_pelatihan_filter,
            totalRowsAkademi,
            totalRowsTema,
            totalRowsPelatihan,
          });

          const datax = res.data.result.survey[0];
          const pelatihan = res.data.result.pelatihan;

          const id_status_peserta = datax.id_status_peserta;
          const arr_id_status_peserta = id_status_peserta.split(",");

          this.setState({ arr_id_checked: arr_id_status_peserta });

          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/umum/list-status-peserta",
              null,
              this.configs,
            )
            .then((res) => {
              const option_role_peserta = res.data.result.Data;
              option_role_peserta.forEach(function (element, i) {
                if (
                  element.name == "Pembatalan" ||
                  element.name == "Mengundurkan Diri"
                ) {
                  option_role_peserta.splice(i, 1);
                }
              });
              option_role_peserta.forEach(function (element, i) {
                if (element.name == "Banned") {
                  option_role_peserta.splice(i, 1);
                }
              });
              const filled_option = [];
              option_role_peserta.forEach(function (element, i) {
                filled_option[i] = false;
                arr_id_status_peserta.forEach(function (id_status, j) {
                  if (id_status == element.id) {
                    filled_option[i] = true;
                  }
                });
              });
              this.setState({
                checkedState: filled_option,
              });
              this.setState({ option_role_peserta });
            });

          let arr_id_checked_akademi = [];
          pelatihan.forEach(function (element) {
            arr_id_checked_akademi.push(element.akademi_id);
          });

          arr_id_checked_akademi = arr_id_checked_akademi.filter(
            function (item, index, inputArray) {
              return inputArray.indexOf(item) == index;
            },
          );

          const dataAkademik = { start: 0, length: 100 };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI +
                "/akademi/list_akademi_survey",
              dataAkademik,
              this.configs,
            )
            .then((res) => {
              const option_akademi = res.data.result.Data;
              const filled_option_akademi = [];
              option_akademi.forEach(function (element, i) {
                filled_option_akademi[i] = false;
                arr_id_checked_akademi.forEach(function (id_status, j) {
                  if (id_status == element.id) {
                    filled_option_akademi[i] = true;
                  }
                });
              });
              this.setState({
                checkedStateAkademi: filled_option_akademi,
              });
              this.setState(
                {
                  option_akademi,
                },
                () => {
                  swal.close();
                },
              );
            });

          if (datax.jenis_survey != 1) {
            this.setState({
              show_behavior: false,
              show_hide_tanggal: false,
            });
          } else {
            this.setState({
              show_behavior: true,
              show_hide_tanggal: false,
            });
            this.setState({
              valBehavior: {
                label: this.behavior_survey[datax.behaviour].label,
                value: datax.behaviour,
              },
            });
          }
          let valStatus = [];
          let status_disabled = 1;
          if (datax.status == 2) {
            valStatus = { label: "Batal", value: datax.status };
            this.setState({
              status_batal: true,
            });
          } else {
            valStatus = {
              label: this.optionstatus[datax.status].label,
              value: datax.status,
            };
            status_disabled = datax.status;
          }

          this.setState(
            {
              datax: datax,
              judul: datax.nama_survey,
              question_to_share: datax.jml_soal,
              duration: datax.duration,
              valStatus: valStatus,
              status: datax.status,
              status_disabled: status_disabled,
              start_at: ymdToDmy(datax.start_at),
              end_at: ymdToDmy(datax.end_at),
              id_survey: id_survey,
              nama_survey: datax.nama_survey,
              jenis_survey: datax.jenis_survey,
              behavior_survey: datax.behaviour,
              valJenisSurvey: {
                label: this.jenis_survey[datax.jenis_survey].label,
                value: datax.jenis_survey,
              },
              status_survey: datax.status,
            },
            () => {
              console.log(this.state.status);
              console.log(this.state.nama_survey);
            },
          );
          //kalo statusnya batal, disable semuanya
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmationBUttonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
        swal.close();
      })
      .catch((error) => {
        console.log(error);
        swal.close();
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  handleChangeJudulAction(e) {
    const judul = e.target.value;
    this.setState({ nama_survey: judul });
  }

  handleChangeDurationAction(e) {
    const duration = e.target.value;
    this.setState({ duration: duration });
  }

  handleChangeJumlahSoalAction(e) {
    const question_to_share = e.target.value;
    this.setState({ question_to_share: question_to_share });
  }

  handleChangeJenisSurveyAction = (jenis) => {
    this.setState({
      valJenisSurvey: { label: jenis.label, value: jenis.value },
    });
    this.setState({ jenis_survey: jenis.value });
    if (jenis.value == 0) {
      this.setState({ show_behavior: false });
    } else {
      this.setState({ show_behavior: true });
    }
  };

  handleChangeBehaviorAction = (behavior) => {
    this.setState({
      valBehavior: { label: behavior.label, value: behavior.value },
    });
    this.setState({ behavior_survey: behavior.value });
  };

  handleChangeStatusAction = (status) => {
    this.setState({ valStatus: { label: status.label, value: status.value } });
    this.setState({ status: status.value });
  };

  handleCheckedChange(position) {
    const updatedCheckedState = this.state.checkedState.map((item, index) =>
      index === position ? !item : item,
    );
    this.setState({ checkedState: updatedCheckedState });

    const id_checked = this.state.option_role_peserta[position].id;
    const arr_id_checked = this.state.arr_id_checked;
    for (let i = 0; i < arr_id_checked.length; i++) {
      arr_id_checked[i] = parseInt(arr_id_checked[i]);
    }
    if (arr_id_checked.includes(id_checked)) {
      const index_checked = arr_id_checked.indexOf(id_checked);
      if (index_checked !== -1) {
        arr_id_checked.splice(index_checked, 1);
      }
    } else {
      arr_id_checked.push(id_checked);
    }
    console.log(arr_id_checked);

    this.setState({ arr_id_checked });
  }

  handleSubmitAction(e) {
    const dataFormX = new FormData(e.currentTarget);
    e.preventDefault();
    this.resetError();
    if (this.handleValidation(e)) {
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });
      const dataForm = new FormData();
      dataForm.append("id", this.state.id_survey);
      dataForm.append("start_at", dmyToYmd(this.state.start_at));
      dataForm.append("end_at", dmyToYmd(this.state.end_at));
      /* if (this.state.jenis_survey != 1) {
                dataForm.append("start_at", dmyToYmd(this.state.start_at));
                dataForm.append("end_at", dmyToYmd(this.state.end_at));
            } else {
                this.setState({
                    start_at: dmyToYmd(dataFormX.get("start_at")),
                    end_at: dmyToYmd(dataFormX.get("end_at"))
                });
                dataForm.append("start_at", dmyToYmd(dataFormX.get("start_at")));
                dataForm.append("end_at", dmyToYmd(dataFormX.get("end_at")));
            } */
      dataForm.append("duration", 1);
      dataForm.append("jns_survey", this.state.jenis_survey);
      /* let mandatory = 1;
            if (this.state.jenis_survey == 1) {
                mandatory = this.state.behavior_survey
            } */
      let mandatory = this.state.behavior_survey;
      if (this.state.jenis_survey != 1) {
        mandatory = 1;
      }
      dataForm.append("mandatory", mandatory);
      dataForm.append("jml_soal", 1);
      dataForm.append("status", this.state.status);
      dataForm.append("nama_survey", this.state.nama_survey);
      dataForm.append("id_status_peserta", this.state.arr_id_checked.join(","));

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/survey/survey_update",
          dataForm,
          this.configs,
        )
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/subvit/survey";
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const check = this.checkEmpty(
      dataForm,
      ["start_at", "end_at", "status", "judul"],
      [],
    );
    return check;
  }

  checkEmpty(dataForm, fieldName) {
    const errorMessageEmpty = "Tidak Boleh Kosong";
    let errors = {};
    let formIsValid = true;
    for (const field in fieldName) {
      const nameAttribute = fieldName[field];
      if (dataForm.get(nameAttribute) == "") {
        errors[nameAttribute] = errorMessageEmpty;
        formIsValid = false;
      }
    }

    if (this.state.metode_id == null) {
      errors["metode"] = errorMessageEmpty;
    }

    if (this.state.arr_id_checked.length == 0) {
      errors["role"] = errorMessageEmpty;
      formIsValid = false;
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  resetError() {
    let errors = {};
    errors[("role", "start_at", "end_at", "status", "judul")] = "";
    this.setState({ errors: errors });
  }

  handleKembaliAction() {
    swal
      .fire({
        title: "Apakah Anda Yakin?",
        icon: "warning",
        confirmButtonText: "Ya",
        showCancelButton: true,
        cancelButtonText: "Tidak",
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
      })
      .then((result) => {
        if (result.isConfirmed) {
          window.history.back();
        }
      });
  }

  handlePageChangeAkademi = (page) => {
    this.setState({ loading_akademi: true });
    this.handleReloadAkademi(page, this.state.newPerPageAkademi);
  };
  handlePerRowsChangeAkademi = async (newPerPage, page) => {
    this.setState({ loading_akademi: true });
    this.setState({ newPerPageAkademi: newPerPage });
    this.handleReloadAkademi(page, newPerPage);
  };

  handleReloadAkademi(page, newPerPage) {
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumberAkademi: start_tmp });
    const start = start_tmp;
    const rows = length_tmp + start_tmp;
    const datax_akademi = this.state.datax_all_akademi.slice(start, rows);
    this.setState({
      datax_akademi,
      loading_akademi: false,
    });
  }

  handlePageChangeTema = (page) => {
    this.setState({ loading_tema: true });
    this.handleReloadTema(page, this.state.newPerPageTema);
  };
  handlePerRowsChangeTema = async (newPerPage, page) => {
    this.setState({ loading_tema: true });
    this.setState({ newPerPageTema: newPerPage });
    this.handleReloadTema(page, newPerPage);
  };

  handleReloadTema(page, newPerPage) {
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumberTema: start_tmp });
    const start = start_tmp;
    const rows = length_tmp + start_tmp;
    const datax_tema = this.state.datax_all_tema.slice(start, rows);
    this.setState({
      datax_tema,
      loading_tema: false,
    });
  }

  handlePageChangePelatihan = (page) => {
    this.setState({ loading_pelatihan: true });
    this.handleReloadPelatihan(page, this.state.newPerPagePelatihan);
  };
  handlePerRowsChangePelatihan = async (newPerPage, page) => {
    this.setState({ loading_pelatihan: true });
    this.setState({ newPerPagePelatihan: newPerPage });
    this.handleReloadPelatihan(page, newPerPage);
  };

  handleReloadPelatihan(page, newPerPage) {
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumberPelatihan: start_tmp });
    const start = start_tmp;
    const rows = length_tmp + start_tmp;
    const datax_pelatihan = this.state.datax_all_pelatihan.slice(start, rows);
    this.setState({
      datax_pelatihan,
      loading_pelatihan: false,
    });
  }

  handleHapusAction() {
    const idx = this.state.id_survey;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          const data = { id: idx };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/survey/survey_delete",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const status_code = res.data.result.StatusCode;
              const messagex = res.data.result.Message;
              if (statux && status_code == "200") {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                    allowOutsideClick: false,
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      window.history.back();
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmationBUttonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            });
        }
      });
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <path
                        opacity="0.3"
                        d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                        fill="#7239ea"
                      ></path>
                      <path
                        d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                        fill="#7239ea"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Subvit
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Survey
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <a
                  onClick={this.handleKembali}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </a>

                <a
                  href={"/subvit/survey/list-soal/" + this.state.id_survey}
                  className="btn btn-sm btn-primary me-2"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-list"></i>
                  </span>
                  List Soal
                </a>

                {(Cookies.get("role_id_user") == 1 ||
                  Cookies.get("role_id_user") == 109) &&
                this.state.jenis_survey != 1 &&
                this.state.status_survey != 1 &&
                this.state.status_survey != 2 ? (
                  <a
                    href="#"
                    onClick={this.handleHapus}
                    title="Hapus"
                    className="btn btn-sm btn-danger btn-active-light-info"
                  >
                    <i className="bi bi-trash-fill text-white"></i>
                    <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                      Hapus
                    </span>
                  </a>
                ) : this.state.jenis_survey == 1 &&
                  this.state.status_survey != 1 ? (
                  <a
                    href="#"
                    onClick={this.handleHapus}
                    title="Hapus"
                    className="btn btn-sm btn-danger btn-active-light-info"
                  >
                    <i className="bi bi-trash-fill text-white"></i>
                    <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                      Hapus
                    </span>
                  </a>
                ) : (
                  <a
                    className="disabled btn btn-sm btn-danger btn-active-light-info"
                    href="#"
                    title="Hapus"
                  >
                    <i className="bi bi-trash-fill text-white"></i>
                    <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                      Hapus
                    </span>
                  </a>
                )}
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div
                    className="stepper stepper-links d-flex flex-column"
                    id="kt_subvit_survey_edit"
                  >
                    <div className="col-lg-12 mt-7">
                      <div className="card border">
                        <div className="card-body mt-5 pt-10 pb-8">
                          <div className="col-12">
                            <h1
                              className="align-items-center text-dark fw-bolder my-1 fs-4"
                              style={{ textTransform: "capitalize" }}
                            >
                              {this.state.nama_survey}
                            </h1>
                            <p className="text-dark fs-7 mb-0">
                              {this.level_survey[this.state.datax.level]}
                              <span className="text-muted fw-semibold fs-7 mb-0">
                                {" "}
                                - Survey{" "}
                                {
                                  this.jenis_survey_label[
                                    this.state.datax.jenis_survey
                                  ]
                                }
                              </span>
                            </p>
                          </div>
                        </div>
                        <div className="card-header">
                          <div className="card-title">
                            <div className="stepper-nav flex-wrap mb-n4">
                              <div
                                className="stepper-item ms-0 my-2 current"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label ms-0">
                                    <h3 className="stepper-title fs-6">
                                      Informasi Survey
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Targeting
                                    </h3>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="card-body">
                          <form
                            action="#"
                            onSubmit={this.handleSubmit}
                            id="kt_subvit_survey_form_edit"
                          >
                            {/* MENU 1 */}
                            <div
                              className="current"
                              data-kt-stepper-element="content"
                            >
                              <div className="row mt-7">
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Nama Survey
                                  </label>
                                  <input
                                    className="form-control form-control-sm"
                                    placeholder="Nama Survey"
                                    name="judul"
                                    id="judul"
                                    value={this.state.nama_survey}
                                    //disabled={this.state.status_disabled == 1 ? 'disabled' : ''}
                                    onChange={this.handleChangeJudul}
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["judul"]}
                                  </span>
                                </div>

                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Jenis Survey
                                  </label>
                                  <Select
                                    name="jenis_survey"
                                    placeholder="Silahkan pilih"
                                    noOptionsMessage={({ inputValue }) =>
                                      !inputValue
                                        ? this.state.jenis_survey
                                        : "Data tidak tersedia"
                                    }
                                    className="form-select-sm selectpicker p-0"
                                    options={this.jenis_survey}
                                    value={this.state.valJenisSurvey}
                                    isDisabled={true}
                                    onChange={this.handleChangeJenisSurvey}
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["jenis_survey"]}
                                  </span>
                                </div>
                                {this.state.jenis_survey != 1 ? (
                                  <div>
                                    <div className="col-lg-12 mb-7 mt-3 fv-row">
                                      <label className="form-label required">
                                        Target Akademi Evaluasi
                                      </label>
                                      <div className="row mb-5 mt-3">
                                        {this.state.option_akademi.map(
                                          ({ id, name }, index) => {
                                            return (
                                              <div
                                                className="col-lg-4 mb-3 fv-row"
                                                key={`custom-checkbox-akademi-${index}`}
                                              >
                                                <input
                                                  type="checkbox"
                                                  id={`custom-checkbox-akademi-${id}`}
                                                  name={name}
                                                  value={id}
                                                  checked={
                                                    this.state
                                                      .checkedStateAkademi[
                                                      index
                                                    ]
                                                  }
                                                  disabled={true}
                                                  onChange={() =>
                                                    this.handleCheckedChangeAkademi(
                                                      index,
                                                    )
                                                  }
                                                  className="custom-checkbox-akademi"
                                                />
                                                <label
                                                  className="ms-3"
                                                  id={`custom-checkbox-text-akademi-${id}`}
                                                  htmlFor={`custom-checkbox-akademi-${id}`}
                                                >
                                                  {capitalizeTheFirstLetterOfEachWord(
                                                    name,
                                                  )}
                                                </label>
                                              </div>
                                            );
                                          },
                                        )}
                                      </div>
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["targetakademi"]}
                                      </span>
                                    </div>
                                  </div>
                                ) : (
                                  ""
                                )}
                                {this.state.show_behavior ? (
                                  <div
                                    className="col-lg-12 mb-7 fv-row"
                                    id="behavior"
                                  >
                                    <label className="form-label required">
                                      Behavior Survey
                                    </label>
                                    <Select
                                      name="behavior"
                                      placeholder="Silahkan pilih"
                                      noOptionsMessage={({ inputValue }) =>
                                        !inputValue
                                          ? this.state.behavior_survey
                                          : "Data tidak tersedia"
                                      }
                                      className="form-select-sm selectpicker p-0"
                                      options={this.behavior_survey}
                                      value={this.state.valBehavior}
                                      isDisabled={
                                        this.state.status_disabled == 1
                                          ? true
                                          : false
                                      }
                                      onChange={this.handleChangeBehavior}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["behavior"]}
                                    </span>
                                  </div>
                                ) : (
                                  ""
                                )}
                                {this.state.jenis_survey == 1 ? (
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Role Peserta
                                    </label>
                                    <div className="row">
                                      {this.state.option_role_peserta.map(
                                        ({ id, name }, index) => {
                                          return (
                                            <div
                                              className="col-lg-3 mb-3 fv-row"
                                              key={`custom-checkbox-${index}`}
                                            >
                                              <input
                                                type="checkbox"
                                                id={`custom-checkbox-${index}`}
                                                name={name}
                                                value={id}
                                                disabled={
                                                  this.state.status_disabled ==
                                                  1
                                                    ? "disabled"
                                                    : ""
                                                }
                                                checked={
                                                  this.state.checkedState[index]
                                                }
                                                onChange={() =>
                                                  this.handleCheckedChange(
                                                    index,
                                                  )
                                                }
                                              />
                                              <label
                                                className="ms-3"
                                                htmlFor={`custom-checkbox-${index}`}
                                              >
                                                {capitalizeTheFirstLetterOfEachWord(
                                                  name,
                                                )}
                                              </label>
                                            </div>
                                          );
                                        },
                                      )}
                                    </div>
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["role"]}
                                    </span>
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>
                            </div>
                            {/* MENU 2 */}
                            <div data-kt-stepper-element="content">
                              <div className="container px-0">
                                <div className="row pt-7">
                                  <h2 className="fs-5 text-muted mb-3">
                                    Target Akademi
                                  </h2>
                                  <div className="col-lg-12">
                                    <div className="table-responsive">
                                      <DataTable
                                        columns={this.columnsAkademi}
                                        data={this.state.datax_akademi}
                                        progressPending={
                                          this.state.loading_akademi
                                        }
                                        highlightOnHover
                                        pointerOnHover
                                        pagination
                                        paginationServer
                                        paginationTotalRows={
                                          this.state.totalRowsAkademi
                                        }
                                        onChangeRowsPerPage={
                                          this.handlePerRowsChangeAkademi
                                        }
                                        onChangePage={
                                          this.handlePageChangeAkademi
                                        }
                                        customStyles={this.customStyles}
                                        persistTableHead={true}
                                        noDataComponent={
                                          <div className="mt-5">
                                            Tidak Ada Data
                                          </div>
                                        }
                                      />
                                    </div>
                                  </div>
                                </div>

                                <div className="row pt-7">
                                  <h2 className="fs-5 text-muted mb-3">
                                    Target Tema
                                  </h2>
                                  <div className="col-lg-12">
                                    <div className="table-responsive">
                                      <DataTable
                                        columns={this.columnsTema}
                                        data={this.state.datax_tema}
                                        progressPending={
                                          this.state.loading_tema
                                        }
                                        highlightOnHover
                                        pointerOnHover
                                        pagination
                                        paginationServer
                                        paginationTotalRows={
                                          this.state.totalRowsTema
                                        }
                                        onChangeRowsPerPage={
                                          this.handlePerRowsChangeTema
                                        }
                                        onChangePage={this.handlePageChangeTema}
                                        customStyles={this.customStyles}
                                        persistTableHead={true}
                                        noDataComponent={
                                          <div className="mt-5">
                                            Tidak Ada Data
                                          </div>
                                        }
                                      />
                                    </div>
                                  </div>
                                </div>

                                <div className="row pt-7">
                                  <h2 className="fs-5 text-muted mb-3">
                                    Target Pelatihan
                                  </h2>
                                  <div className="col-lg-12">
                                    <div className="table-responsive">
                                      <DataTable
                                        columns={this.columnsPelatihan}
                                        data={this.state.datax_pelatihan}
                                        progressPending={
                                          this.state.loading_pelatihan
                                        }
                                        highlightOnHover
                                        pointerOnHover
                                        pagination
                                        paginationServer
                                        paginationTotalRows={
                                          this.state.totalRowsPelatihan
                                        }
                                        onChangeRowsPerPage={
                                          this.handlePerRowsChangePelatihan
                                        }
                                        onChangePage={
                                          this.handlePageChangePelatihan
                                        }
                                        customStyles={this.customStyles}
                                        persistTableHead={true}
                                        noDataComponent={
                                          <div className="mt-5">
                                            Tidak Ada Data
                                          </div>
                                        }
                                      />
                                    </div>
                                  </div>
                                </div>
                                {this.state.show_hide_tanggal ? (
                                  <div className="row mt-7">
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label required">
                                        Tgl. Mulai Survey
                                      </label>
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Tanggal Mulai Pelaksanaan"
                                        name="start_at"
                                        id="date_survey_start"
                                        value={this.state.start_at}
                                        //disabled={this.state.status_disabled == 1 ? 'disabled' : ''}
                                        /* style={
                                                                                this.state.status_disabled ?
                                                                                    {
                                                                                        backgroundColor: '#f0f0f0',
                                                                                        cursor: 'pointer',
                                                                                    }
                                                                                    :
                                                                                    {}
                                                                            } */
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["start_at"]}
                                      </span>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label required">
                                        Tgl. Akhir Survey
                                      </label>
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Tanggal Selesai Pelaksanaan"
                                        name="end_at"
                                        id="date_survey_end"
                                        value={this.state.end_at}
                                        //disabled={this.state.status_disabled == 1 ? 'disabled' : ''}
                                        /* style={
                                                                                this.state.status_disabled ?
                                                                                    {
                                                                                        backgroundColor: '#f0f0f0',
                                                                                        cursor: 'pointer',
                                                                                    }
                                                                                    :
                                                                                    {}
                                                                            } */
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["end_at"]}
                                      </span>
                                    </div>
                                  </div>
                                ) : (
                                  ""
                                )}

                                <div className="col-lg-12 mt-7 mb-7 fv-row">
                                  <label className="form-label required">
                                    Status
                                  </label>
                                  <Select
                                    name="status"
                                    placeholder="Silahkan pilih"
                                    noOptionsMessage={({ inputValue }) =>
                                      !inputValue
                                        ? this.state.datax
                                        : "Data tidak tersedia"
                                    }
                                    className="form-select-sm selectpicker p-0"
                                    options={
                                      this.state.jenis_survey == 1
                                        ? this.optionstatus
                                        : this.optionstatusevaluasi
                                    }
                                    value={this.state.valStatus}
                                    onChange={this.handleChangeStatus}
                                    isDisabled={this.state.status_batal}
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["status"]}
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div className="text-center my-7">
                              <button
                                className="btn btn-light btn-md me-3 mr-2"
                                data-kt-stepper-action="previous"
                              >
                                <i className="fa fa-chevron-left me-1"></i>
                                Sebelumnya
                              </button>
                              {
                                //jika survey batal tidak keluar tombol simpan
                                this.state.datax.status == "2" ? (
                                  <button
                                    type="submit"
                                    className="disabled btn btn-primary btn-md"
                                    data-kt-stepper-action="submit"
                                  >
                                    <i className="fa fa-paper-plane ms-1"></i>
                                    Simpan
                                  </button>
                                ) : (
                                  <button
                                    type="submit"
                                    className="btn btn-primary btn-md"
                                    data-kt-stepper-action="submit"
                                  >
                                    <i className="fa fa-paper-plane ms-1"></i>
                                    Simpan
                                  </button>
                                )
                              }
                              <button
                                type="button"
                                className="btn btn-primary btn-md"
                                data-kt-stepper-action="next"
                              >
                                <i className="fa fa-chevron-right"></i>Lanjutkan
                              </button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
