import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import MultipleForm from "./MultipleForm";
import ObjectiveForm from "./ObjectiveForm";
import TerbukaForm from "./TerbukaForm";
import TriggerForm from "./TriggerForm";
import imageCompression from "browser-image-compression";

const resizeFile = async (file) => {
  const options = {
    maxSizeMB: 0.2,
    maxWidthOrHeight: 300,
    useWebWorker: true,
  };

  return imageCompression(file, options);
};

export default class SurveyForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      fields: {},
      is_next_soal: true,
      choices: [],
      no_soal: 1,
      show_objective: true,
      show_multiple: false,
      show_terbuka: false,
      show_trigger: false,
      tipe: "objective",
      pertanyaan: "",
      key_gambar_pertanyaan: null,
      id_survey: this.props.id_survey,
    };
    this.abjad = ["A", "B", "C", "D", "E", "F", "G"];
    this.handleSimpanKembali = this.handleSimpanKembaliAction.bind(this);
    this.handleKembali = this.handleKembaliAction.bind(this);
    this.handleChangeTipe = this.handleChangeTipeAction.bind(this);
    this.handleChangePertanyaan = this.handleChangePertanyaanAction.bind(this);
    this.handleChangeGambar = this.handleChangeGambarAction.bind(this);
  }

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    const radio1 = document.getElementById("jenis_pertanyaan1");
    radio1.checked = true;
  }

  componentDidUpdate(prevProps) {
    if (prevProps.id_survey !== this.props.id_survey) {
      this.setState(
        {
          id_survey: this.props.id_survey,
        },
        () => {
          console.log(this.state.id_survey);
        },
      );
    }
  }

  handleKembaliAction() {
    window.location = "/subvit/survey/list-soal/" + this.props.id_survey;
  }

  handleSimpanKembaliAction() {
    //check error
    const allJawaban = Array.from(document.getElementsByClassName("jawaban"));
    const arrError = [];

    for (let i = 0; i < allJawaban.length; i++) {
      let valueJawaban = allJawaban[i].getAttribute("value");
      let no_soal = allJawaban[i].getAttribute("no_soal");
      let index = allJawaban[i].getAttribute("index");
      if (valueJawaban == "") {
        const err = {
          no_soal: no_soal,
          index: index,
          message: "Jawaban Tidak Boleh Kosong",
        };
        arrError.push(err);
      }
    }
    let is_error = false;
    const allError = Array.from(document.getElementsByClassName("error"));
    for (let i = 0; i < allError.length; i++) {
      allError[i].textContent = "";
      let no_soal = allError[i].getAttribute("no_soal");
      let index = allError[i].getAttribute("index");
      for (let j = 0; j < arrError.length; j++) {
        if (arrError[j].no_soal == no_soal && arrError[j].index == index) {
          allError[i].textContent = arrError[j].message;
          this.setState({ is_next_soal: false });
          is_error = true;
        }
      }
    }
    let errors = [];
    if (this.state.pertanyaan == "") {
      errors["pertanyaan"] = "Tidak Boleh Kosong";
      is_error = true;
    } else {
      errors["pertanyaan"] = "";
    }
    this.setState({ errors: errors });
    //build js untuk insert
    //kembalikan kondisi pilihan multiple choice ke kosong
    if (!is_error) {
      const soal = {
        nosoal: this.state.no_soal,
        pertanyaan: this.state.pertanyaan,
        key_gambar_pertanyaan: this.state.key_gambar_pertanyaan,
        tipe: this.state.tipe,
        jawaban: [],
      };
      if (
        this.state.tipe == "objective" ||
        this.state.tipe == "multiple_choice"
      ) {
        const datax = JSON.parse(localStorage.getItem(this.state.tipe));
        const jawaban = [];
        for (let i = 0; i < datax.length; i++) {
          if (datax[i].no == this.state.no_soal) {
            const keyImage = this.state.no_soal + "_" + datax[i].index;

            let imageJawaban = null;
            let imageNameJawaban = null;
            let imageWrapperJawaban = document.getElementById(keyImage);
            if (imageWrapperJawaban) {
              imageWrapperJawaban = imageWrapperJawaban.value;
              const splitImage = imageWrapperJawaban.split("_");
              imageNameJawaban = splitImage[0];
              imageJawaban = splitImage[1];
            }

            const option = {
              key: this.abjad[datax[i].index],
              option: datax[i].jawaban,
              image: imageJawaban,
              color: false,
            };
            jawaban.push(option);
          }
        }
        soal.jawaban = jawaban;
      } else if (this.state.tipe == "triggered_question") {
        //build parent nya dulu
        const datax = JSON.parse(localStorage.getItem(this.state.tipe));
        const jawaban = [];
        for (let i = 0; i < datax.length; i++) {
          if (datax[i].no == this.state.no_soal) {
            //cek apakah ada child
            const allPertanyaan = JSON.parse(
              localStorage.getItem("pertanyaanChild"),
            );
            let key = 1;
            let is_next = false;
            const sub = [];
            if (allPertanyaan) {
              for (let j = 0; j < allPertanyaan.length; j++) {
                if (
                  allPertanyaan[j].no == datax[i].no &&
                  allPertanyaan[j].index == datax[i].index
                ) {
                  //cek jawaban dari child
                  const answer = [];
                  let index_abjad = 0;
                  const allJawabanChild = JSON.parse(
                    localStorage.getItem("childTrigger"),
                  );
                  for (let k = 0; k < allJawabanChild.length; k++) {
                    if (
                      allJawabanChild[k].no == allPertanyaan[j].no &&
                      allJawabanChild[k].num_child == allPertanyaan[j].index &&
                      allJawabanChild[k].no_soal_child ==
                        allPertanyaan[j].no_soal_child
                    ) {
                      const keyImageChildAnswer =
                        allJawabanChild[k].no +
                        "_" +
                        allJawabanChild[k].num_child +
                        "_" +
                        allJawabanChild[k].no_soal_child +
                        "_" +
                        allJawabanChild[k].index;

                      let imageJawaban = null;
                      let imageNameJawaban = null;
                      let imageWrapperJawaban =
                        document.getElementById(keyImageChildAnswer);
                      if (imageWrapperJawaban) {
                        imageWrapperJawaban = imageWrapperJawaban.value;
                        const splitImage = imageWrapperJawaban.split("_");
                        imageNameJawaban = splitImage[0];
                        imageJawaban = splitImage[1];
                      }

                      const answerContent = {
                        key: this.abjad[index_abjad],
                        option: allJawabanChild[k].jawaban,
                        //key_image: keyImageChildAnswer,
                        image: imageJawaban,
                        type: "choose",
                      };
                      answer.push(answerContent);
                      index_abjad++;
                    }
                  }
                  is_next = true;
                  let key_image =
                    "p" +
                    allPertanyaan[j].no +
                    "_" +
                    allPertanyaan[j].index +
                    "_" +
                    allPertanyaan[j].no_soal_child;
                  let imageChildPertanyaan = null;
                  let imageChildNamePertanyaan = null;
                  let imageWrapperChildPertanyaan =
                    document.getElementById(key_image);
                  if (imageWrapperChildPertanyaan) {
                    imageWrapperChildPertanyaan =
                      imageWrapperChildPertanyaan.value;
                    const splitImage = imageWrapperChildPertanyaan.split("_");
                    imageChildNamePertanyaan = splitImage[0];
                    imageChildPertanyaan = splitImage[1];
                  }
                  const subContent = {
                    key: key,
                    question: allPertanyaan[j].pertanyaan,
                    //key_image: key_image,
                    image: imageChildPertanyaan,
                    is_next: is_next,
                    answer: answer,
                  };
                  sub.push(subContent);
                }
                key++;
              }
            }
            const keyImage = this.state.no_soal + "_" + datax[i].index;
            let imageOption = null;
            let imageNameOption = null;
            let imageWrapperOption = document.getElementById(keyImage);
            if (imageWrapperOption) {
              imageWrapperOption = imageWrapperOption.value;
              const splitImage = imageWrapperOption.split("_");
              imageNameOption = splitImage[0];
              imageOption = splitImage[1];
            }

            const option = {
              key: this.abjad[datax[i].index],
              option: datax[i].jawaban,
              //key_image: keyImage,
              image: imageOption,
              color: false,
              is_next: is_next,
              sub: sub,
            };
            jawaban.push(option);
          }
        }
        soal.jawaban = jawaban;
      } else if (this.state.tipe == "pertanyaan_terbuka") {
        delete soal.jawaban;
      }

      const dataForm = new FormData();
      dataForm.append("id_survey", this.state.id_survey);
      dataForm.append("type", soal.tipe);
      dataForm.append("pertanyaan", soal.pertanyaan);

      //ambil image yang di tempel di html untuk pertanyaan
      const key_gambar_pertanyaan = soal.key_gambar_pertanyaan;
      let imagePertanyaanWrapper = document.getElementById(
        key_gambar_pertanyaan,
      );
      let gambar_pertanyaan = null;
      if (imagePertanyaanWrapper) {
        imagePertanyaanWrapper = imagePertanyaanWrapper.value;
        const splitImagePertanyaan = imagePertanyaanWrapper.split("_");
        gambar_pertanyaan = splitImagePertanyaan[1];
      }
      dataForm.append("pertanyaan_gambar", gambar_pertanyaan);
      dataForm.append("status", 1);
      dataForm.append("jawaban", JSON.stringify(soal.jawaban));

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "survey/soalsurvey_add",
          dataForm,
          this.configs,
        )
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location =
                    "/subvit/survey/list-soal/" + this.state.id_survey;
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    }
  }

  handleChangeTipeAction(e) {
    const tipe = e.target.value;
    this.setState(
      {
        tipe: tipe,
        show_objective: false,
        show_multiple: false,
        show_terbuka: false,
        show_trigger: false,
      },
      () => {
        switch (tipe) {
          case "objective":
            this.setState({ show_objective: true });
            break;
          case "multiple_choice":
            this.setState({ show_multiple: true });
            break;
          case "pertanyaan_terbuka":
            this.setState({ show_terbuka: true });
            break;
          case "triggered_question":
            this.setState({ show_trigger: true });
            break;
        }
      },
    );
  }

  handleChangePertanyaanAction(e) {
    e.preventDefault();
    this.setState({ pertanyaan: e.currentTarget.value });
  }

  handleChangeGambarAction(e) {
    const imageFile = e.target.files[0];
    const reader = new FileReader();
    const keyImage = this.state.no_soal;
    this.setState({ key_gambar_pertanyaan: keyImage });
    resizeFile(imageFile).then((cf) => {
      reader.readAsDataURL(cf);
      reader.onload = function () {
        let result = reader.result;
        let fileName = imageFile.name;
        let wrapper = document.getElementById("temp_image");
        let check = document.getElementById(keyImage);
        if (check) {
          check.value = fileName + "_" + result;
        } else {
          const hiddenWrapper = document.createElement("input");
          hiddenWrapper.value = fileName + "_" + result;
          hiddenWrapper.id = keyImage;
          hiddenWrapper.className = "imagePertanyaan";
          hiddenWrapper.type = "hidden";
          wrapper.appendChild(hiddenWrapper);
        }
      };
    });
  }

  render() {
    return (
      <div className="card-body">
        <div className="row">
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">Pertanyaan</label>
            <input
              className="form-control form-control-sm"
              placeholder="Masukkan pertanyaan"
              name="pertanyaan"
              value={this.state.pertanyaan}
              onChange={this.handleChangePertanyaan}
            />
            <span style={{ color: "red" }}>
              {this.state.errors["pertanyaan"]}
            </span>
          </div>
          <div className="col-lg-12 ">
            <div className="mb-7 fv-row">
              <label className="form-label">Gambar Pertanyaan (Optional)</label>
              <input
                type="file"
                className="form-control form-control-sm mb-2"
                name="upload_silabus"
                accept="image/*"
                onChange={this.handleChangeGambar}
                value={this.state.gambar}
              />
              <small className="text-muted">
                Format File (.jpg/.jpeg/.png/.svg)
              </small>
              <br />
              <span style={{ color: "red" }}>
                {this.state.errors["upload_silabus"]}
              </span>
            </div>
          </div>
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">Jenis Pertanyaan</label>
            <div className="d-flex">
              <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                <input
                  className="form-check-input"
                  onChange={this.handleChangeTipe}
                  type="radio"
                  name="jenis_pertanyaan"
                  value="objective"
                  id="jenis_pertanyaan1"
                />
                <label className="form-check-label" htmlFor="jenis_pertanyaan1">
                  Objective
                </label>
              </div>
              <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                <input
                  className="form-check-input"
                  onChange={this.handleChangeTipe}
                  type="radio"
                  name="jenis_pertanyaan"
                  value="multiple_choice"
                  id="jenis_pertanyaan2"
                />
                <label className="form-check-label" htmlFor="jenis_pertanyaan2">
                  Multiple Choice
                </label>
              </div>
              <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                <input
                  className="form-check-input"
                  onChange={this.handleChangeTipe}
                  type="radio"
                  name="jenis_pertanyaan"
                  value="pertanyaan_terbuka"
                  id="jenis_pertanyaan3"
                />
                <label className="form-check-label" htmlFor="jenis_pertanyaan3">
                  Pertanyaan Terbuka
                </label>
              </div>
              <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                <input
                  className="form-check-input"
                  onChange={this.handleChangeTipe}
                  type="radio"
                  name="jenis_pertanyaan"
                  value="triggered_question"
                  id="jenis_pertanyaan4"
                />
                <label className="form-check-label" htmlFor="jenis_pertanyaan4">
                  Triggered Question
                </label>
              </div>
            </div>
            <span style={{ color: "red" }}>
              {this.state.errors["program_dts"]}
            </span>
          </div>

          {this.state.show_objective ? (
            <ObjectiveForm
              choices={this.state.choices}
              no_soal={this.state.no_soal}
            />
          ) : (
            ""
          )}
          {this.state.show_multiple ? (
            <MultipleForm
              choices={this.state.choices}
              no_soal={this.state.no_soal}
            />
          ) : (
            ""
          )}
          {this.state.show_terbuka ? <TerbukaForm /> : ""}
          {this.state.show_trigger ? (
            <TriggerForm
              choices={this.state.choices}
              no_soal={this.state.no_soal}
            />
          ) : (
            ""
          )}

          <div className="text-center my-7">
            <a
              onClick={this.handleKembali}
              className="btn btn-light btn-md me-6"
            >
              Batal
            </a>
            <a
              onClick={this.handleSimpanKembali}
              className="btn btn-primary btn-md"
            >
              <i className="fa fa-paper-plane ms-1"></i>Simpan
            </a>
          </div>
        </div>
        <div id="temp_image"></div>
      </div>
    );
  }
}
