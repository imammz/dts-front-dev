import React, { useState, useEffect } from "react";
import swal from "sweetalert2";
import Switch from "@material-ui/core/Switch";
import imageCompression from "browser-image-compression";

const resizeFile = async (file) => {
  const options = {
    maxSizeMB: 0.2,
    maxWidthOrHeight: 300,
    useWebWorker: true,
  };

  return imageCompression(file, options);
};

export default class TriggerChildForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      fields: {},
      choices: [],
      choices_last_index: 0,
      max_choices: 7,
      no_soal: props.no_soal,
      num_choices: 0,
      default_num_choices: 3,
      pertanyaan: "",
      checked: props.is_next,
      num_child: props.num_child,
      no_soal_child: props.no_soal_child,
      index: props.index,
    };

    this.abjad = ["A", "B", "C", "D", "E", "F", "G"];

    this.handleTambah = this.handleTambahAction.bind(this);
    this.handleDelete = this.handleDeleteAction.bind(this);
    this.handleChangeChecked = this.handleChangeCheckedAction.bind(this);
    this.handleChangePertanyaan = this.handleChangePertanyaanAction.bind(this);
    this.handleChangeImagePertanyaan =
      this.handleChangeImagePertanyaanAction.bind(this);
    this.formName = "childTrigger";
  }
  componentDidMount() {
    if (this.props.sub) {
      const question = this.props.sub.question;
      this.setState({ pertanyaan: question });
    }
    this.loadMultipleChoice();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.choices !== this.props.choices) {
      this.setState(
        {
          choices: this.props.choices,
          no_soal: this.state.no_soal + 1,
        },
        () => {
          this.loadMultipleChoice();
        },
      );
    }
  }

  loadMultipleChoice() {
    let temp;
    const choices = [];
    let num_choices = this.state.num_choices;
    const datax = this.props.sub;
    const pidsurvey_detail = this.state.no_soal;
    let answer = null;
    if (datax) {
      answer = datax.answer;
    }

    const storeAllJawaban = [];
    //loop length menyesuaikan apakah dari local storage atau tidak
    const loopLength = answer ? answer.length : this.state.default_num_choices;
    for (let i = 0; i < loopLength; i++) {
      let answerType = "0";
      if (answer) {
        if (answer[i].type == "choose" || answer[i].type == "text") {
          answerType = answer[i].type;
        }
      }
      if (answerType == "choose" || answerType == "0") {
        choices.push(
          <MultipleChoice
            no_soal={this.state.no_soal}
            abjad={this.abjad[i]}
            index={i}
            key={
              this.state.no_soal +
              "_" +
              this.state.num_child +
              "_" +
              this.state.no_soal_child +
              "_" +
              i
            }
            num_child={this.state.num_child}
            onClick={this.handleDelete}
            no_soal_child={this.state.no_soal_child}
            option={answer ? answer[i].option : ""}
          />,
        );
      }
      /* //simpan localStorage
            const storeJawaban = {
                "no": this.state.no_soal,
                "num_child": this.state.num_child,
                "no_soal_child": this.state.no_soal_child,
                "index": i,
                "jawaban": answer ? answer[i].option : '',
                "type": answer ? answer[i].type : 'choose',
            }
            storeAllJawaban.push(storeJawaban); */
      temp = i;
      num_choices++;
    }
    /* let allJawaban = JSON.parse(localStorage.getItem(this.formName));
        if (allJawaban) {
            const newJawaban = allJawaban.concat(storeAllJawaban);
            localStorage.setItem(this.formName, JSON.stringify(newJawaban));
        } else {
            localStorage.setItem(this.formName, JSON.stringify(storeAllJawaban));
        } */

    this.setState({ num_choices });
    this.setState({ choices_last_index: temp });
    this.setState({ choices });
  }

  handleTambahAction() {
    if (this.state.num_choices < this.state.max_choices) {
      const choices_last_index = this.state.choices_last_index + 1;
      this.setState({
        choices: [
          ...this.state.choices,
          <MultipleChoice
            no_soal={this.state.no_soal}
            abjad={this.abjad[this.state.num_choices]}
            index={choices_last_index}
            key={
              this.state.no_soal +
              "_" +
              this.state.num_child +
              "_" +
              this.state.no_soal_child +
              "_" +
              choices_last_index
            }
            num_child={this.state.num_child}
            no_soal_child={this.state.no_soal_child}
            onClick={this.handleDelete}
          />,
        ],
        num_choices: this.state.num_choices + 1,
      });
      this.setState({ choices_last_index: choices_last_index });
      this.setState({ num_choices: this.state.num_choices + 1 });
    } else {
      swal
        .fire({
          title: "Maksimal Pilihan hanya " + this.state.max_choices,
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleDeleteAction(index) {
    const newList = this.state.choices.filter((item) => item.key !== index);
    this.setState({ choices: newList }, () => {
      const allAbjad = Array.from(
        document.getElementsByClassName("abjad_child"),
      );
      let inc1 = 0;
      let inc2 = 0;
      for (let i = 0; i < allAbjad.length; i++) {
        const indexsplit = index.split("_");
        const newIndex =
          indexsplit[0] + "_" + indexsplit[1] + "_" + indexsplit[2];
        if (allAbjad[i].getAttribute("index") == newIndex) {
          allAbjad[i].textContent = this.abjad[inc1];
          inc1++;
        }
      }
      const allPlaceholder = Array.from(
        document.getElementsByClassName("jawaban_child"),
      );
      for (let i = 0; i < allPlaceholder.length; i++) {
        const indexsplit = index.split("_");
        const newIndex =
          indexsplit[0] + "_" + indexsplit[1] + "_" + indexsplit[2];
        if (allAbjad[i].getAttribute("index") == newIndex) {
          allPlaceholder[i].placeholder =
            "Masukkan Jawaban " + this.abjad[inc2];
          inc2++;
        }
      }

      //hapus localStorage
      const datax = JSON.parse(localStorage.getItem(this.formName));
      if (datax) {
        const newDatax = datax.filter(
          (item) => item.no + "_" + item.num_child + "_" + item.index !== index,
        );
        localStorage.setItem(this.formName, JSON.stringify(newDatax));
      }
    });
    this.setState({ num_choices: this.state.num_choices - 1 });
  }

  handleNextSoalAction() {
    this.setState({ no_soal: this.state.no_soal + 1 });
    this.setState({ choices: [] });
    this.setState({ num_choices: 0 });
    this.loadMultipleChoice();
  }

  handleChangeCheckedAction(e) {
    this.setState({ checked: !this.state.checked });
    const callBackVal = {
      checked: e.target.checked,
      index: e.target.value,
      no_soal_child: this.state.no_soal_child + 1,
    };
    this.props.parentCallBack(callBackVal);
  }

  handleChangePertanyaanAction(event) {
    event.preventDefault();
    const storageName = "pertanyaanChild";
    const pertanyaan = event.currentTarget.value;
    const storePertanyaan = {
      no: this.state.no_soal,
      index: this.state.index,
      pertanyaan: pertanyaan,
      no_soal_child: this.state.no_soal_child,
    };
    const old = JSON.parse(localStorage.getItem(storageName));
    if (old !== null) {
      let filtered = old.filter(function (obj) {
        return (
          obj.no + "_" + obj.index + "_" + obj.no_soal_child !==
          event.currentTarget.getAttribute("index")
        );
      });
      filtered.push(storePertanyaan);
      //sort
      filtered.sort((a, b) => (a.index > b.index ? 1 : -1));
      filtered.sort((a, b) => {
        if (a.index == b.index) {
          return a.no_soal_child < b.no_soal_child ? -1 : 1;
        } else {
          return a.index < b.index ? -1 : 1;
        }
      });
      localStorage.setItem(storageName, JSON.stringify(filtered));
    } else {
      localStorage.setItem(storageName, JSON.stringify([storePertanyaan]));
    }
  }

  handleChangeImagePertanyaanAction(e) {
    const imageFile = e.target.files[0];
    const reader = new FileReader();
    const keyImage =
      "p" +
      this.state.no_soal +
      "_" +
      this.state.index +
      "_" +
      this.state.no_soal_child;
    resizeFile(imageFile).then((cf) => {
      reader.readAsDataURL(cf);
      reader.onload = function () {
        let result = reader.result;
        let fileName = imageFile.name;
        let wrapper = document.getElementById("temp_image");

        let check = document.getElementById(keyImage);
        if (check) {
          check.value = fileName + "_" + result;
        } else {
          const hiddenWrapper = document.createElement("input");
          hiddenWrapper.value = fileName + "_" + result;
          hiddenWrapper.imageName = fileName;
          hiddenWrapper.id = keyImage;
          hiddenWrapper.className = "imagePertanyaanChild";
          hiddenWrapper.type = "hidden";
          wrapper.appendChild(hiddenWrapper);
        }
      };
    });
  }

  render() {
    return (
      <div className="col-lg-12 p-5 m-5 bg-gray-200 rounded-sm">
        <div className="col-lg-12">
          <div className="row">
            <div className="col-lg-4 mb-7 ms-5 fv-row">
              <label className="form-label required">Pertanyaan</label>
              {
                <input
                  className="form-control form-control-sm jawaban"
                  onChange={(e) => {
                    this.setState({ pertanyaan: e.currentTarget.value });
                  }}
                  placeholder="Pertanyaan"
                  name={
                    "pertanyaan_" +
                    this.state.no_soal +
                    "_" +
                    this.state.num_child
                  }
                  no_soal={this.state.no_soal}
                  no_soal_child={this.state.no_soal_child}
                  index={
                    this.state.no_soal +
                    "_" +
                    this.state.num_child +
                    "_" +
                    this.state.no_soal_child
                  }
                  onBlur={this.handleChangePertanyaan}
                  value={this.state.pertanyaan}
                />
              }
              <span className="error" style={{ color: "red" }}></span>
            </div>
            <div className="col-lg-4">
              <div className="mb-7 fv-row">
                <label className="form-label">
                  Gambar Pertanyaan (Optional)
                </label>
                <input
                  type="file"
                  className="form-control form-control-sm mb-2"
                  name="upload_gambar"
                  accept="image/*"
                  onChange={this.handleChangeImagePertanyaan}
                />
                <small className="text-muted">
                  Format File (.jpg/.jpeg/.png/.svg)
                </small>
                <br />
                <span style={{ color: "red" }}></span>
              </div>
            </div>
            <div className="col-lg-3">
              {this.props.canAdd ? (
                <div className="mt-7 fv-row">
                  <Switch
                    checked={this.state.checked}
                    onChange={this.handleChangeChecked}
                    value={this.state.num_child}
                    inputProps={{ "aria-label": "secondary checkbox" }}
                  />
                </div>
              ) : (
                ""
              )}
              {this.state.checked ? (
                <div className="text-danger">Ada Pertanyaan Selanjutnya?</div>
              ) : (
                ""
              )}
            </div>
          </div>
        </div>
        {this.state.choices}
        <div className="row">
          <div className="col-lg-12 mb-7 fv-row text-center">
            <a
              onClick={this.handleTambah}
              className="btn btn-sm btn-light text-success d-block btn-block"
            >
              <i className="bi bi-plus-circle text-success me-1"></i> Tambah
              Opsi Jawaban
            </a>
          </div>
        </div>
      </div>
    );
  }
}

function MultipleChoice(props) {
  const formName = "childTrigger";
  const placeholder = "Masukkan Jawaban " + props.abjad;
  const [valjawab, setValjawab] = useState(props.option);
  const [checked, setChecked] = useState(false);
  const [childForm, setChild] = useState();
  const [pertanyaan, setValPertanyaan] = useState("");

  function handleClickDelete(event) {
    props.onClick(event.currentTarget.getAttribute("index"));
  }

  function handleChangeInput(event) {
    event.preventDefault();
    const jawaban = event.currentTarget.value;
    //build untuk localStorage
    const storeJawaban = {
      no: props.no_soal,
      num_child: props.num_child,
      no_soal_child: props.no_soal_child,
      index: props.index,
      jawaban: jawaban,
      type: "choose",
    };
    let old = JSON.parse(localStorage.getItem(formName));
    if (old !== null) {
      let filtered = old.filter(function (obj) {
        return (
          obj.no +
            "_" +
            obj.num_child +
            "_" +
            obj.no_soal_child +
            "_" +
            obj.index !==
          props.no_soal +
            "_" +
            props.num_child +
            "_" +
            props.no_soal_child +
            "_" +
            props.index
        );
      });
      filtered.push(storeJawaban);
      //sort
      filtered.sort((a, b) => (a.index > b.index ? 1 : -1));
      localStorage.setItem(formName, JSON.stringify(filtered));
    } else {
      localStorage.setItem(formName, JSON.stringify([storeJawaban]));
    }
  }

  function handleChangeChecked(prev) {
    if (prev.currentTarget.checked !== checked) {
      setChecked(!checked);
    }
  }

  function handleChangeImageJawaban(e) {
    const imageFile = e.target.files[0];
    const reader = new FileReader();
    resizeFile(imageFile).then((cf) => {
      reader.readAsDataURL(cf);
      reader.onload = function () {
        let result = reader.result;
        let fileName = imageFile.name;
        const keyImage =
          props.no_soal +
          "_" +
          props.num_child +
          "_" +
          props.no_soal_child +
          "_" +
          props.index;
        let wrapper = document.getElementById("temp_image");

        let check = document.getElementById(keyImage);
        if (check) {
          check.value = fileName + "_" + result;
        } else {
          const hiddenWrapper = document.createElement("input");
          hiddenWrapper.value = fileName + "_" + result;
          hiddenWrapper.imageName = fileName;
          hiddenWrapper.id = keyImage;
          hiddenWrapper.className = "imageJawabanChildren";
          hiddenWrapper.type = "hidden";
          wrapper.appendChild(hiddenWrapper);
        }
      };
    });
  }

  return (
    <div className="row ms-10">
      <div className="col-lg-4 mb-7 fv-row">
        <label className="form-label required">
          Jawaban
          <span
            className="abjad_child ms-1"
            index={
              props.no_soal + "_" + props.num_child + "_" + props.no_soal_child
            }
          >
            {props.abjad}
          </span>
        </label>
        {
          <input
            className="form-control form-control-sm jawaban_child"
            onChange={(e) => setValjawab(e.currentTarget.value)}
            value={valjawab}
            placeholder={placeholder}
            name={"jawaban_" + props.no_soal + "_" + props.index}
            no_soal={props.no_soal}
            no_soal_child={props.no_soal_child}
            index={
              props.no_soal + "_" + props.num_child + "_" + props.no_soal_child
            }
            num_child={props.num_child}
            onBlur={handleChangeInput}
          />
        }
        <span
          className="error"
          style={{ color: "red" }}
          no_soal={props.no_soal}
          index={props.index}
        ></span>
      </div>
      <div className="col-lg-4">
        <div className="mb-7 fv-row">
          <label className="form-label">Gambar Jawaban (Optional)</label>
          <input
            type="file"
            className="form-control form-control-sm mb-2"
            name="upload_gambar"
            accept="image/*"
            index={props.index}
            onChange={handleChangeImageJawaban}
          />
          <small className="text-muted">
            Format File (.jpg/.jpeg/.png/.svg)
          </small>
          <br />
          <span style={{ color: "red" }}></span>
        </div>
      </div>
      <div className="col-lg-2">
        <div className="mt-7 fv-row">
          <a
            title="Hapus"
            onClick={handleClickDelete}
            index={
              props.no_soal +
              "_" +
              props.num_child +
              "_" +
              props.no_soal_child +
              "_" +
              props.index
            }
            className="btn btn-icon btn-danger w-40px h-40px"
          >
            <span className="svg-icon svg-icon-3">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width={34}
                height={34}
                viewBox="0 0 24 24"
                fill="none"
              >
                <path
                  d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z"
                  fill="black"
                />
                <path
                  opacity="0.5"
                  d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z"
                  fill="black"
                />
                <path
                  opacity="0.5"
                  d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z"
                  fill="black"
                />
              </svg>
            </span>
          </a>
        </div>
      </div>
    </div>
  );
}
