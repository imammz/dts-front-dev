import React, { useState } from "react";
import swal from "sweetalert2";
import Switch from "@material-ui/core/Switch";
import TriggerChildForm from "./TriggerChildForm";
import imageCompression from "browser-image-compression";

const resizeFile = async (file) => {
  const options = {
    maxSizeMB: 0.2,
    maxWidthOrHeight: 300,
    useWebWorker: true,
  };

  return imageCompression(file, options);
};

export default class TriggerForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      fields: {},
      choices: props.choices,
      choices_last_index: 0,
      max_choices: 7,
      no_soal: parseInt(props.id_soal),
      num_choices: 0,
      default_num_choices: 4,
      id_soal: props.id_soal,
    };

    this.abjad = ["A", "B", "C", "D", "E", "F", "G"];

    this.handleTambah = this.handleTambahAction.bind(this);
    this.handleDelete = this.handleDeleteAction.bind(this);
    this.handleNextSoal = this.handleNextSoalAction.bind(this);
    this.formName = "triggered_question";
  }
  componentDidMount() {
    localStorage.setItem(this.formName, null);
    localStorage.setItem("childTrigger", null);
    localStorage.setItem("pertanyaanChild", null);
    this.loadMultipleChoice();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.choices !== this.props.choices) {
      this.setState(
        {
          choices: this.props.choices,
          no_soal: this.state.no_soal + 1,
        },
        () => {
          this.loadMultipleChoice();
        },
      );
    }
  }

  loadMultipleChoice() {
    let temp;
    const choices = [];
    let num_choices = this.state.num_choices;
    const datax = JSON.parse(localStorage.getItem(this.state.id_soal));
    const pidsurvey_detail = datax.id;
    //check jika dia dari pertanyaan terbuka
    let answer = false;
    if (datax.type != "pertanyaan_terbuka") {
      answer = JSON.parse(datax.jawaban);
    }
    const storeAllJawaban = [];
    const storeAllPertanyaanChild = [];
    const storeAllChildTrigger = [];
    //loop length menyesuaikan apakah dari local storage atau tidak
    const loopLength = answer ? answer.length : this.state.default_num_choices;
    for (let i = 0; i < loopLength; i++) {
      choices.push(
        <MultipleChoice
          no_soal={this.state.no_soal}
          abjad={this.abjad[i]}
          index={i}
          key={this.state.no_soal + "_" + i}
          onClick={this.handleDelete}
          storage=""
          option={answer ? answer[i].option : ""}
          is_next={answer ? answer[i].is_next : false}
          sub={answer ? answer[i].sub : []}
        />,
      );
      let ns = this.state.no_soal;
      if (answer && answer[i].sub && answer[i].sub.length > 0) {
        answer[i].sub.forEach(function (pc, j) {
          const storePertanyaanChild = {
            no: ns,
            index: i,
            pertanyaan: pc.question,
            no_soal_child: j,
          };
          storeAllPertanyaanChild.push(storePertanyaanChild);
          pc.answer.forEach(function (ans, k) {
            const storeChildTrigger = {
              no: ns,
              num_child: i,
              no_soal_child: j,
              index: k,
              jawaban: ans.option,
              type: "choose",
            };
            storeAllChildTrigger.push(storeChildTrigger);
          });
        });
      }
      //simpan localStorage
      const storeJawaban = {
        no: pidsurvey_detail,
        index: i,
        jawaban: answer ? answer[i].option : "",
        type: answer ? answer[i].type : "choose",
      };
      storeAllJawaban.push(storeJawaban);
      temp = i;
      num_choices++;
    }

    localStorage.setItem(this.formName, JSON.stringify(storeAllJawaban));
    localStorage.setItem(
      "pertanyaanChild",
      JSON.stringify(storeAllPertanyaanChild),
    );
    localStorage.setItem("childTrigger", JSON.stringify(storeAllChildTrigger));
    this.setState({ num_choices });
    this.setState({ choices_last_index: temp });
    this.setState({ choices });
  }

  handleTambahAction() {
    if (this.state.num_choices < this.state.max_choices) {
      const choices_last_index = this.state.choices_last_index + 1;
      this.setState({
        choices: [
          ...this.state.choices,
          <MultipleChoice
            no_soal={this.state.no_soal}
            abjad={this.abjad[this.state.num_choices]}
            key={this.state.no_soal + "_" + choices_last_index}
            index={choices_last_index}
            onClick={this.handleDelete}
          />,
        ],
        num_choices: this.state.num_choices + 1,
      });
      this.setState({ choices_last_index: choices_last_index });
    } else {
      swal
        .fire({
          title: "Maksimal Pilihan hanya " + this.state.max_choices,
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleDeleteAction(index) {
    if (this.state.num_choices > 2) {
      const newList = this.state.choices.filter((item) => item.key !== index);
      this.setState({ choices: newList }, () => {
        const allAbjad = Array.from(document.getElementsByClassName("abjad"));
        for (let i = 0; i < allAbjad.length; i++) {
          allAbjad[i].textContent = this.abjad[i];
        }
        const allPlaceholder = Array.from(
          document.getElementsByClassName("jawaban"),
        );
        for (let i = 0; i < allPlaceholder.length; i++) {
          allPlaceholder[i].placeholder = "Masukkan Jawaban " + this.abjad[i];
        }

        //hapus localStorage
        const datax = JSON.parse(localStorage.getItem(this.formName));
        if (datax) {
          const newDatax = datax.filter(
            (item) => item.no + "_" + item.index !== index,
          );
          localStorage.setItem(this.formName, JSON.stringify(newDatax));
        }
      });
      this.setState({ num_choices: this.state.num_choices - 1 });
    } else {
      swal
        .fire({
          title: "Minimal Pilihan Adalah 2",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleNextSoalAction() {
    this.setState({ no_soal: this.state.no_soal + 1 });
    this.setState({ choices: [] });
    this.setState({ num_choices: 0 });
    this.loadMultipleChoice();
  }

  render() {
    return (
      <div>
        {this.state.choices}
        <div className="row">
          <div className="col-lg-12 mb-7 fv-row text-center">
            <a
              onClick={this.handleTambah}
              className="btn btn-sm btn-light text-success d-block btn-block"
            >
              <i className="bi bi-plus-circle text-success me-1"></i> Tambah
              Opsi Jawaban
            </a>
          </div>
        </div>
      </div>
    );
  }
}

function MultipleChoice(props) {
  const formName = "triggered_question";
  const maxChild = 3;
  const placeholder = "Masukkan Jawaban " + props.abjad;
  const [valjawab, setValjawab] = useState(props.option);
  const [checked, setChecked] = useState(
    props.sub ? (props.sub.length > 0 ? true : false) : false,
  );
  const [childForm, setChild] = useState(
    props.sub ? (props.sub.length > 0 ? true : false) : false,
  );
  const initTriggerChild = [];
  const initPertanyaanChild = [];
  if (props.sub && props.sub.length > 0) {
    props.sub.forEach(function (element, i) {
      let ischecked = false;

      if (i < props.sub.length - 1) {
        ischecked = true;
      }
      const triggerchild = (
        <TriggerChildForm
          checked={ischecked}
          sub={element}
          key={props.no_soal + "_child_" + i}
          parentCallBack={handleCallBack}
          canAdd={true}
          index={props.index}
          no_soal={props.no_soal}
          num_child={props.index}
          no_soal_child={i}
        />
      );
      initTriggerChild.push(triggerchild);
    });
  } else {
    initTriggerChild.push(
      <TriggerChildForm
        checked={false}
        key={props.no_soal + "_child_1"}
        parentCallBack={handleCallBack}
        canAdd={true}
        index={props.index}
        no_soal={props.no_soal}
        num_child={props.index}
        no_soal_child={1}
      />,
    );
  }

  const [triggerComponent, setTriggerComponent] = useState(initTriggerChild);
  let [numChild, setNumChild] = useState(1);

  function handleClickDelete(event) {
    props.onClick(event.currentTarget.getAttribute("index"));
  }

  function handleCallBack(childData) {
    const isChecked = childData.checked;
    const indexClicked = childData.index;
    const no_soal_child = childData.no_soal_child;
    if (isChecked && numChild < maxChild - 1) {
      setTriggerComponent((triggerComponent) => [
        ...triggerComponent,
        <TriggerChildForm
          key={props.no_soal + "_" + props.indexClicked + "_" + no_soal_child}
          parentCallBack={handleCallBack}
          canAdd={true}
          index={indexClicked}
          no_soal={props.no_soal}
          num_child={indexClicked}
          no_soal_child={no_soal_child}
        />,
      ]);
      setNumChild(numChild++);
    } else if (isChecked && numChild < maxChild) {
      setTriggerComponent((triggerComponent) => [
        ...triggerComponent,
        <TriggerChildForm
          key={props.no_soal + "_" + props.indexClicked + "_" + no_soal_child}
          parentCallBack={handleCallBack}
          canAdd={false}
          index={indexClicked}
          no_soal={props.no_soal}
          num_child={indexClicked}
          no_soal_child={no_soal_child}
        />,
      ]);
      setNumChild(numChild++);
    } else if (!isChecked) {
      let indexDelete = 1 - indexClicked;
      if (indexDelete == 0) {
        indexDelete = 1;
      }
      setTriggerComponent((triggerComponent) =>
        triggerComponent.slice(0, indexClicked),
      );
      setNumChild(numChild--);
    }
  }

  function handleChangeInput(event) {
    event.preventDefault();
    const jawaban = event.currentTarget.value;
    //build untuk localStorage
    const storeJawaban = {
      no: props.no_soal,
      index: props.index,
      jawaban: jawaban,
      type: "choose",
    };
    let old = JSON.parse(localStorage.getItem(formName));
    if (old !== null) {
      let filtered = old.filter(function (obj) {
        return obj.index !== props.index;
      });
      filtered.push(storeJawaban);
      //sort
      filtered.sort((a, b) => (a.index > b.index ? 1 : -1));
      localStorage.setItem(formName, JSON.stringify(filtered));
    } else {
      localStorage.setItem(formName, JSON.stringify([storeJawaban]));
    }
  }

  function handleChangeImageJawaban(e) {
    const imageFile = e.target.files[0];
    const reader = new FileReader();
    resizeFile(imageFile).then((cf) => {
      reader.readAsDataURL(cf);
      reader.onload = function () {
        let result = reader.result;
        let fileName = imageFile.name;
        const keyImage = props.no_soal + "_" + props.index;
        let wrapper = document.getElementById("temp_image");

        let check = document.getElementById(keyImage);
        if (check) {
          check.value = fileName + "_" + result;
        } else {
          const hiddenWrapper = document.createElement("input");
          hiddenWrapper.value = fileName + "_" + result;
          hiddenWrapper.imageName = fileName;
          hiddenWrapper.id = keyImage;
          hiddenWrapper.className = "imageJawaban";
          hiddenWrapper.type = "hidden";
          wrapper.appendChild(hiddenWrapper);
        }
      };
    });
  }

  function handleChangeChecked(prev) {
    if (prev.currentTarget.checked !== checked) {
      setChecked(!checked);
      setChild(prev.currentTarget.checked);
    }
  }

  return (
    <div className="row mb-3">
      <div className="col-lg-4 mb-7 fv-row">
        <label className="form-label required">
          Jawaban <span className="abjad">{props.abjad}</span>
        </label>
        <input
          className="form-control form-control-sm jawaban"
          onChange={(e) => setValjawab(e.currentTarget.value)}
          value={valjawab}
          placeholder={placeholder}
          name={"jawaban_" + props.no_soal + "_" + props.index}
          no_soal={props.no_soal}
          index={props.index}
          onBlur={handleChangeInput}
        />
        <span
          className="error"
          style={{ color: "red" }}
          no_soal={props.no_soal}
          index={props.index}
        ></span>
      </div>
      <div className="col-lg-4">
        <div className="mb-7 fv-row">
          <label className="form-label">Gambar Jawaban (Optional)</label>
          <input
            type="file"
            className="form-control form-control-sm mb-2"
            name="upload_gambar"
            accept="image/*"
            index={props.index}
            onChange={handleChangeImageJawaban}
          />
          <small className="text-muted">
            Format File (.jpg/.jpeg/.png/.svg)
          </small>
          <br />
          <span style={{ color: "red" }}></span>
        </div>
      </div>
      <div className="col-lg-1">
        <div className="mt-7 fv-row">
          <a
            title="Hapus"
            onClick={handleClickDelete}
            index={props.no_soal + "_" + props.index}
            className="btn btn-icon btn-danger w-40px h-40px"
          >
            <span className="svg-icon svg-icon-3">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width={34}
                height={34}
                viewBox="0 0 24 24"
                fill="none"
              >
                <path
                  d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z"
                  fill="black"
                />
                <path
                  opacity="0.5"
                  d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z"
                  fill="black"
                />
                <path
                  opacity="0.5"
                  d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z"
                  fill="black"
                />
              </svg>
            </span>
          </a>
        </div>
      </div>
      <div className="col-lg-3">
        <div className="mt-7 fv-row">
          <Switch
            checked={checked}
            onChange={handleChangeChecked}
            value="checkedA"
            inputProps={{ "aria-label": "secondary checkbox" }}
          />
          {checked ? (
            <div className="text-danger">Ada Pertanyaan Selanjutnya?</div>
          ) : (
            ""
          )}
        </div>
      </div>
      {childForm ? <div>{triggerComponent}</div> : ""}
    </div>
  );
}
