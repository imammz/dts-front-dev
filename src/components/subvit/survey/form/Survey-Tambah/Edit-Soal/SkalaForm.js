import React, { useState } from "react";
import swal from "sweetalert2";
import imageCompression from "browser-image-compression";

const resizeFile = async (file) => {
  const options = {
    maxSizeMB: 0.2,
    maxWidthOrHeight: 300,
    useWebWorker: true,
  };

  return imageCompression(file, options);
};

export default class SkalaForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      fields: {},
      choices: props.choices,
      choices_last_index: 0,
      max_choices: 15,
      no_soal: props.no_soal,
      num_choices: 0,
      default_num_choices: 10,
      id_soal: props.id_soal,
    };

    this.abjad = [
      "0",
      "1",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7",
      "8",
      "9",
      "10",
      "11",
      "12",
      "13",
      "14",
      "15",
    ];
    this.handleNextSoal = this.handleNextSoalAction.bind(this);
    this.handleChangeJumlahSkala =
      this.handleChangeJumlahSkalaAction.bind(this);
    this.formName = "skala_";
  }
  componentDidMount() {
    localStorage.setItem(this.formName, null);
    const datax = JSON.parse(localStorage.getItem(this.state.id_soal));
    console.log(datax);
    const jawaban = datax.jawaban;
    const num_choices = jawaban.length - 1;
    this.setState({ default_num_choices: num_choices }, () => {
      this.loadSkala();
    });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.choices !== this.props.choices) {
      this.setState(
        {
          choices: this.props.choices,
          no_soal: this.state.no_soal + 1,
        },
        () => {
          this.loadSkala();
        },
      );
    }
  }

  loadSkala() {
    let temp;
    const choices = [];

    for (let i = 0; i <= this.state.default_num_choices; i++) {
      choices.push(
        <Skala
          no_soal={this.state.no_soal}
          abjad={this.abjad[i]}
          index={i}
          key={this.state.no_soal + "_" + i}
          storage=""
        />,
      );
      temp = i;
    }

    this.setState({ choices_last_index: temp });
    this.setState({ choices });
    const storeJawaban = {
      no: this.state.no_soal,
      skala: this.state.default_num_choices,
    };
    localStorage.setItem(this.formName, JSON.stringify([storeJawaban]));
  }

  handleChangeJumlahSkalaAction(e) {
    const skala = e.currentTarget.value;
    if (skala < 1 || skala > 15) {
      let errors = [];
      errors["skala"] = "Jumlah Skala Minimal 1 dan Maksimal 15";
      this.setState({
        errors: errors,
      });
    } else {
      this.setState(
        {
          errors: [],
          default_num_choices: skala,
        },
        () => {
          this.loadSkala();
        },
      );
    }
  }

  handleNextSoalAction() {
    this.setState({ no_soal: this.state.no_soal + 1 });
    this.setState({ choices: [] });
    this.setState({ num_choices: 0 });
    this.loadMultipleChoice();
  }

  render() {
    return (
      <div>
        <div className="col-lg-12 mb-7 fv-row">
          <label className="form-label required">Jumlah Skala</label>
          <input
            className="form-control form-control-sm"
            placeholder="Jumlah Skala"
            name="jumlah_skala"
            value={this.state.default_num_choices}
            onChange={this.handleChangeJumlahSkala}
            type="number"
            max={15}
            min={1}
          />
          <span style={{ color: "red" }}>{this.state.errors["skala"]}</span>
        </div>

        <div className="btn-group" role="group" aria-label="Basic example">
          <div>
            {this.state.choices}
            <div className="col-lg-12 row">
              <div className="col-lg-6 text-start">Tidak Merekomendasikan</div>
              <div className="col-lg-6 text-end">Sangat Merekomendasikan</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function Skala(props) {
  return (
    <button className="jawaban btn btn-secondary form-control-sm m-1">
      {props.abjad}
    </button>
  );
}
