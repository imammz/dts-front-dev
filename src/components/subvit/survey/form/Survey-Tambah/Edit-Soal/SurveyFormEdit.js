import React from "react";
import MultipleForm from "./MultipleForm";
import ObjectiveForm from "./ObjectiveForm";
import TerbukaForm from "./TerbukaForm";
import TriggerForm from "./TriggerForm";
import swal from "sweetalert2";
import SkalaForm from "./SkalaForm";

export default class SurveyForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      fields: {},
      is_next_soal: true,
      choices: [],
      no_soal: props.id_soal,
      show_objective: false,
      show_multiple: false,
      show_terbuka: false,
      show_trigger: false,
      show_skala: false,
      tipe: "objective",
      pertanyaan: "",
      key_gambar_pertanyaan: null,
      datax: {},
      id_soal: props.id_soal,
      current_image_name: null,
    };
    this.abjad = ["A", "B", "C", "D", "E", "F", "G"];
    this.handleSimpanKembali = this.handleSimpanKembaliAction.bind(this);
    this.handleKembali = this.handleKembaliAction.bind(this);
    this.handleChangeTipe = this.handleChangeTipeAction.bind(this);
    this.handleChangePertanyaan = this.handleChangePertanyaanAction.bind(this);
    this.handleChangeGambar = this.handleChangeGambarAction.bind(this);
  }

  componentDidMount() {
    //get data yg di local storage
    const datax = JSON.parse(localStorage.getItem(this.state.id_soal));
    //check ada gambar tidak
    const gambar = document.getElementById(this.state.id_soal);
    let current_image_name = "";
    if (gambar) {
      const explode_gambar = gambar.value.split("_");
      current_image_name = explode_gambar[0];
    }

    if (datax) {
      this.setState({
        datax: datax,
        pertanyaan: datax.pertanyaan,
        no_soal: datax.nosoal,
        current_image_name,
      });
      this.changeTipe(datax.tipe);
      document.getElementById(datax.tipe + "").checked = true;

      /* const arr_jawaban = [];
            datax.jawaban.forEach(function (element, i) {
                arr_jawaban.push({
                    index: i,
                    jawaban: element.option
                });
            }); */

      /* if(datax.tipe=="objective"||datax.tipe=="multiple")
            {
                localStorage.setItem(datax.tipe+"_", JSON.stringify(arr_jawaban));
            }else{

            } */
    }
  }
  handleKembaliAction() {
    const callBackVal = {
      is_edit_soal: false,
    };
    this.props.parentCallBack(callBackVal);
  }
  handleSimpanKembaliAction() {
    //check error
    const allJawaban = Array.from(
      document.getElementsByClassName("jawaban_edit"),
    );
    const arrError = [];

    for (let i = 0; i < allJawaban.length; i++) {
      let valueJawaban = allJawaban[i].getAttribute("value");
      let no_soal = allJawaban[i].getAttribute("no_soal");
      let index = allJawaban[i].getAttribute("index");
      if (valueJawaban == "") {
        const err = {
          no_soal: no_soal,
          index: index,
          message: "Jawaban Tidak Boleh Kosong",
        };
        arrError.push(err);
      }
    }

    let is_error = false;
    const allError = Array.from(document.getElementsByClassName("error_edit"));

    for (let i = 0; i < allError.length; i++) {
      allError[i].textContent = "";
      let no_soal = allError[i].getAttribute("no_soal");
      let index = allError[i].getAttribute("index");
      for (let j = 0; j < arrError.length; j++) {
        if (arrError[j].no_soal == no_soal && arrError[j].index == index) {
          allError[i].textContent = arrError[j].message;
          this.setState({ is_next_soal: false });
          is_error = true;
        }
      }
    }
    let errors = [];
    if (this.state.pertanyaan == "") {
      errors["pertanyaan"] = "Tidak Boleh Kosong";
      is_error = true;
    }
    this.setState({ errors: errors });

    //build js untuk insert
    //kembalikan kondisi pilihan multiple choice ke kosong
    if (!is_error) {
      const soal = {
        nosoal: this.state.id_soal,
        pertanyaan: this.state.pertanyaan,
        key_gambar_pertanyaan: this.state.key_gambar_pertanyaan,
        tipe: this.state.tipe,
        jawaban: [],
      };
      if (
        this.state.tipe == "objective" ||
        this.state.tipe == "multiple_choice"
      ) {
        const datax = JSON.parse(localStorage.getItem(this.state.tipe + "_"));
        const jawaban = [];
        for (let i = 0; i < datax.length; i++) {
          if (datax[i].no == this.state.no_soal) {
            const keyImage = this.state.no_soal + "_" + datax[i].index;
            const option = {
              key: this.abjad[datax[i].index],
              option: datax[i].jawaban,
              key_image: keyImage,
              color: false,
            };
            jawaban.push(option);
          }
        }
        soal.jawaban = jawaban;
      } else if (this.state.tipe == "skala") {
        const datax = JSON.parse(localStorage.getItem(this.state.tipe + "_"));
        const skala = datax[0].skala;
        const jawaban = [];
        const valueSkala = [
          "0",
          "1",
          "2",
          "3",
          "4",
          "5",
          "6",
          "7",
          "8",
          "9",
          "10",
          "11",
          "12",
          "13",
          "14",
          "15",
        ];
        const valueAbjad = [
          "A",
          "B",
          "C",
          "D",
          "F",
          "G",
          "H",
          "I",
          "J",
          "K",
          "L",
          "M",
          "N",
          "O",
          "P",
          "Q",
          "R",
          "S",
        ];
        for (let i = 0; i <= skala; i++) {
          if (datax[0].no == this.state.no_soal) {
            const option = {
              key: valueAbjad[i],
              option: valueSkala[i],
              color: false,
            };
            jawaban.push(option);
          }
        }
        soal.jawaban = jawaban;
      } else if (this.state.tipe == "triggered_question") {
        //build parent nya dulu
        const datax = JSON.parse(localStorage.getItem(this.state.tipe + "_"));
        const jawaban = [];
        for (let i = 0; i < datax.length; i++) {
          if (datax[i].no == this.state.no_soal) {
            //cek apakah ada child
            const allPertanyaan = JSON.parse(
              localStorage.getItem("pertanyaanChild_"),
            );
            let key = 1;
            let is_next = false;
            const sub = [];
            if (allPertanyaan) {
              for (let j = 0; j < allPertanyaan.length; j++) {
                if (
                  allPertanyaan[j].no == datax[i].no &&
                  allPertanyaan[j].index == datax[i].index
                ) {
                  //cek jawaban dari child
                  const answer = [];
                  let index_abjad = 0;
                  const allJawabanChild = JSON.parse(
                    localStorage.getItem("childTrigger_"),
                  );
                  for (let k = 0; k < allJawabanChild.length; k++) {
                    if (
                      allJawabanChild[k].no == allPertanyaan[j].no &&
                      allJawabanChild[k].num_child == allPertanyaan[j].index &&
                      allJawabanChild[k].no_soal_child ==
                        allPertanyaan[j].no_soal_child
                    ) {
                      const keyImageChildAnswer =
                        allJawabanChild[k].no +
                        "_" +
                        allJawabanChild[k].num_child +
                        "_" +
                        allJawabanChild[k].no_soal_child +
                        "_" +
                        allJawabanChild[k].index;
                      const answerContent = {
                        key: this.abjad[index_abjad],
                        option: allJawabanChild[k].jawaban,
                        key_image: keyImageChildAnswer,
                        type: "choose",
                      };
                      answer.push(answerContent);
                      index_abjad++;
                    }
                  }
                  is_next = true;
                  const subContent = {
                    key: key,
                    question: allPertanyaan[j].pertanyaan,
                    key_image:
                      "p" +
                      allPertanyaan[j].no +
                      "_" +
                      allPertanyaan[j].index +
                      "_" +
                      allPertanyaan[j].no_soal_child,
                    is_next: is_next,
                    answer: answer,
                  };
                  sub.push(subContent);
                }
                key++;
              }
            }
            const keyImage = this.state.no_soal + "_" + datax[i].index;
            const option = {
              key: this.abjad[datax[i].index],
              option: datax[i].jawaban,
              key_image: keyImage,
              color: false,
              is_next: is_next,
              sub: sub,
            };
            jawaban.push(option);
          }
        }
        soal.jawaban = jawaban;
      } else if (this.state.tipe == "pertanyaan_terbuka") {
        delete soal.jawaban;
      }
      localStorage.setItem(this.state.no_soal, JSON.stringify(soal));
      const callBackVal = {
        is_edit_soal: false,
        is_tambah_soal: false,
      };
      swal
        .fire({
          title: "Soal Berhasil Disimpan",
          icon: "success",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          this.props.parentCallBack(callBackVal);
        });
    }
  }

  handleChangeTipeAction(e) {
    const tipe = e.target.value;
    this.changeTipe(tipe);
  }

  changeTipe(tipe) {
    this.setState(
      {
        tipe: tipe,
        show_objective: false,
        show_multiple: false,
        show_terbuka: false,
        show_trigger: false,
        show_skala: false,
      },
      () => {
        switch (tipe) {
          case "objective":
            this.setState({ show_objective: true });
            break;
          case "multiple_choice":
            this.setState({ show_multiple: true });
            break;
          case "pertanyaan_terbuka":
            this.setState({ show_terbuka: true });
            break;
          case "triggered_question":
            this.setState({ show_trigger: true });
            break;
          case "skala":
            this.setState({ show_skala: true });
            break;
        }
      },
    );
  }

  handleChangePertanyaanAction(e) {
    e.preventDefault();
    this.setState({ pertanyaan: e.currentTarget.value });
  }

  handleChangeGambarAction(e) {
    const imageFile = e.target.files[0];
    const reader = new FileReader();
    const keyImage = this.state.no_soal;
    this.setState({ key_gambar_pertanyaan: keyImage });
    reader.readAsDataURL(imageFile);
    reader.onload = function () {
      let result = reader.result;
      let fileName = imageFile.name;
      let wrapper = document.getElementById("temp_image");
      let check = document.getElementById(keyImage);
      if (check) {
        check.value = fileName + "_" + result;
      } else {
        const hiddenWrapper = document.createElement("input");
        hiddenWrapper.value = fileName + "_" + result;
        hiddenWrapper.id = keyImage;
        hiddenWrapper.className = "imagePertanyaan_edit";
        hiddenWrapper.type = "hidden";
        wrapper.appendChild(hiddenWrapper);
      }
    };
  }

  render() {
    return (
      <>
        <h5 className="mt-7 mb-5 me-3">Soal {this.state.no_soal}</h5>
        <div className="row">
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">Pertanyaan</label>
            <input
              className="form-control form-control-sm"
              placeholder="Masukkan pertanyaan"
              name="pertanyaan"
              value={this.state.pertanyaan}
              onChange={this.handleChangePertanyaan}
            />
            <span style={{ color: "red" }}>
              {this.state.errors["pertanyaan"]}
            </span>
          </div>
          <div className="col-lg-12">
            <div className="mb-7 fv-row">
              <label className="form-label">
                {this.state.current_image_name != null &&
                this.state.current_image_name != "" ? (
                  <div>
                    Ganti Gambar{" "}
                    <span className="text-danger">
                      {this.state.current_image_name}
                    </span>
                  </div>
                ) : (
                  "Gambar Pertanyaan (Optional)"
                )}
              </label>
              <input
                type="file"
                className="form-control form-control-sm mb-2"
                name="upload_silabus"
                accept="image/*"
                onChange={this.handleChangeGambar}
                value={this.state.gambar}
              />
              <small className="text-muted">
                Format File (.jpg/.jpeg/.png/.svg)
              </small>
              <br />
              <span style={{ color: "red" }}>
                {this.state.errors["upload_silabus"]}
              </span>
            </div>
          </div>
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">Jenis Pertanyaan</label>
            <div className="d-flex">
              <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                <input
                  className="form-check-input"
                  onChange={this.handleChangeTipe}
                  type="radio"
                  name="jenis_pertanyaan"
                  value="objective"
                  id="objective"
                />
                <label className="form-check-label" htmlFor="jenis_pertanyaan1">
                  Objective
                </label>
              </div>
              <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                <input
                  className="form-check-input"
                  onChange={this.handleChangeTipe}
                  type="radio"
                  name="jenis_pertanyaan"
                  value="multiple_choice"
                  id="multiple_choice"
                />
                <label className="form-check-label" htmlFor="jenis_pertanyaan2">
                  Multiple Choice
                </label>
              </div>
              <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                <input
                  className="form-check-input"
                  onChange={this.handleChangeTipe}
                  type="radio"
                  name="jenis_pertanyaan"
                  value="pertanyaan_terbuka"
                  id="pertanyaan_terbuka"
                />
                <label className="form-check-label" htmlFor="jenis_pertanyaan3">
                  Pertanyaan Terbuka
                </label>
              </div>
              <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                <input
                  className="form-check-input"
                  onChange={this.handleChangeTipe}
                  type="radio"
                  name="jenis_pertanyaan"
                  value="triggered_question"
                  id="triggered_question"
                />
                <label className="form-check-label" htmlFor="jenis_pertanyaan4">
                  Triggered Question
                </label>
              </div>
              <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                <input
                  className="form-check-input"
                  onChange={this.handleChangeTipe}
                  type="radio"
                  name="jenis_pertanyaan"
                  value="skala"
                  id="skala"
                />
                <label className="form-check-label" htmlFor="jenis_pertanyaan5">
                  Skala
                </label>
              </div>
            </div>
            <span style={{ color: "red" }}>
              {this.state.errors["program_dts"]}
            </span>
          </div>

          {this.state.show_objective ? (
            <ObjectiveForm
              id_soal={this.state.id_soal}
              choices={this.state.choices}
              no_soal={this.state.no_soal}
            />
          ) : (
            ""
          )}
          {this.state.show_multiple ? (
            <MultipleForm
              id_soal={this.state.id_soal}
              choices={this.state.choices}
              no_soal={this.state.no_soal}
            />
          ) : (
            ""
          )}
          {this.state.show_terbuka ? (
            <TerbukaForm id_soal={this.state.id_soal} />
          ) : (
            ""
          )}
          {this.state.show_trigger ? (
            <TriggerForm
              id_soal={this.state.id_soal}
              choices={this.state.choices}
              no_soal={this.state.no_soal}
            />
          ) : (
            ""
          )}
          {this.state.show_skala ? (
            <SkalaForm
              id_soal={this.state.id_soal}
              choices={this.state.choices}
              no_soal={this.state.no_soal}
            />
          ) : (
            ""
          )}

          <div className="text-center my-7">
            <a
              onClick={this.handleKembali}
              className="btn btn-light btn-md me-6"
            >
              Batal
            </a>
            <a
              onClick={this.handleSimpanKembali}
              className="btn btn-success btn-md"
            >
              <i className="bi bi-plus-circle me-1"></i>Simpan Soal
            </a>
          </div>
        </div>
        <div id="temp_image"></div>
      </>
    );
  }
}
