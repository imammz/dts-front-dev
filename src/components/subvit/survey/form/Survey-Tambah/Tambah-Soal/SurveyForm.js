import React from "react";
import MultipleForm from "./MultipleForm";
import ObjectiveForm from "./ObjectiveForm";
import TerbukaForm from "./TerbukaForm";
import TriggerForm from "./TriggerForm";
import swal from "sweetalert2";
import imageCompression from "browser-image-compression";
import SkalaForm from "./SkalaForm";

const resizeFile = async (file) => {
  const options = {
    maxSizeMB: 0.2,
    maxWidthOrHeight: 300,
    useWebWorker: true,
  };

  return imageCompression(file, options);
};

export default class SurveyForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      fields: {},
      is_next_soal: true,
      choices: [],
      no_soal: props.no_soal,
      show_objective: true,
      show_multiple: false,
      show_terbuka: false,
      show_trigger: false,
      show_skala: false,
      tipe: "objective",
      pertanyaan: "",
      key_gambar_pertanyaan: null,
      jenis_survey: props.jenis_survey,
    };
    this.abjad = ["A", "B", "C", "D", "E", "F", "G"];
    this.handleNextSoal = this.handleNextSoalAction.bind(this);
    this.handleChangeTipe = this.handleChangeTipeAction.bind(this);
    this.handleChangePertanyaan = this.handleChangePertanyaanAction.bind(this);
    this.handleChangeGambar = this.handleChangeGambarAction.bind(this);
  }

  componentDidMount() {
    const radio1 = document.getElementById("jenis_pertanyaan1");
    radio1.checked = true;
  }

  componentDidUpdate(prevProps) {
    if (prevProps.no_soal !== this.props.no_soal) {
      this.setState({
        no_soal: this.props.no_soal,
      });
    }
  }

  handleNextSoalAction() {
    //check error
    const allJawaban = Array.from(document.getElementsByClassName("jawaban"));
    const arrError = [];

    for (let i = 0; i < allJawaban.length; i++) {
      let valueJawaban = allJawaban[i].getAttribute("value");
      let no_soal = allJawaban[i].getAttribute("no_soal");
      let index = allJawaban[i].getAttribute("index");
      if (valueJawaban == "") {
        const err = {
          no_soal: no_soal,
          index: index,
          message: "Jawaban Tidak Boleh Kosong",
        };
        arrError.push(err);
      }
    }
    let is_error = false;
    const allError = Array.from(document.getElementsByClassName("error"));
    for (let i = 0; i < allError.length; i++) {
      allError[i].textContent = "";
      let no_soal = allError[i].getAttribute("no_soal");
      let index = allError[i].getAttribute("index");
      for (let j = 0; j < arrError.length; j++) {
        if (arrError[j].no_soal == no_soal && arrError[j].index == index) {
          allError[i].textContent = arrError[j].message;
          this.setState({ is_next_soal: false });
          is_error = true;
        }
      }
    }

    const allJawabanTrigger = Array.from(
      document.getElementsByClassName("jawaban_child"),
    );
    for (let i = 0; i < allJawabanTrigger.length; i++) {
      if (allJawabanTrigger[i].value == "") {
        allJawabanTrigger[i].nextSibling.innerText =
          "Jawaban Tidak Boleh Kosong";
        is_error = true;
      }
    }

    let errors = [];
    if (this.state.pertanyaan == "") {
      errors["pertanyaan"] = "Tidak Boleh Kosong";
      is_error = true;
    } else {
      errors["pertanyaan"] = "";
    }
    this.setState({ errors: errors });
    //build js untuk insert
    //kembalikan kondisi pilihan multiple choice ke kosong
    if (!is_error) {
      document.getElementById("upload_image_form").value = "";
      const soal = {
        nosoal: this.state.no_soal,
        pertanyaan: this.state.pertanyaan,
        key_gambar_pertanyaan: this.state.no_soal,
        tipe: this.state.tipe,
        jawaban: [],
      };
      if (
        this.state.tipe == "objective" ||
        this.state.tipe == "multiple_choice"
      ) {
        const datax = JSON.parse(localStorage.getItem(this.state.tipe));
        const jawaban = [];
        for (let i = 0; i < datax.length; i++) {
          if (datax[i].no == this.state.no_soal) {
            const keyImage = this.state.no_soal + "_" + datax[i].index;
            const option = {
              key: this.abjad[datax[i].index],
              option: datax[i].jawaban,
              key_image: keyImage,
              color: false,
            };
            jawaban.push(option);
          }
        }
        soal.jawaban = jawaban;
      } else if (this.state.tipe == "skala") {
        const datax = JSON.parse(localStorage.getItem(this.state.tipe));
        const skala = datax[0].skala;
        const jawaban = [];
        const valueSkala = [
          "0",
          "1",
          "2",
          "3",
          "4",
          "5",
          "6",
          "7",
          "8",
          "9",
          "10",
          "11",
          "12",
          "13",
          "14",
          "15",
        ];
        const valueAbjad = [
          "A",
          "B",
          "C",
          "D",
          "F",
          "G",
          "H",
          "I",
          "J",
          "K",
          "L",
          "M",
          "N",
          "O",
          "P",
          "Q",
          "R",
          "S",
        ];
        for (let i = 0; i <= skala; i++) {
          if (datax[0].no == this.state.no_soal) {
            const option = {
              key: valueAbjad[i],
              option: valueSkala[i],
              color: false,
            };
            jawaban.push(option);
          }
        }
        soal.jawaban = jawaban;
      } else if (this.state.tipe == "triggered_question") {
        //build parent nya dulu
        const datax = JSON.parse(localStorage.getItem(this.state.tipe));
        const jawaban = [];
        for (let i = 0; i < datax.length; i++) {
          if (datax[i].no == this.state.no_soal) {
            //cek apakah ada child
            const allPertanyaan = JSON.parse(
              localStorage.getItem("pertanyaanChild"),
            );
            let key = 1;
            let is_next = false;
            const sub = [];
            if (allPertanyaan) {
              for (let j = 0; j < allPertanyaan.length; j++) {
                if (
                  allPertanyaan[j].no == datax[i].no &&
                  allPertanyaan[j].index == datax[i].index
                ) {
                  //cek jawaban dari child
                  const answer = [];
                  let index_abjad = 0;
                  const allJawabanChild = JSON.parse(
                    localStorage.getItem("childTrigger"),
                  );
                  for (let k = 0; k < allJawabanChild.length; k++) {
                    if (
                      allJawabanChild[k].no == allPertanyaan[j].no &&
                      allJawabanChild[k].num_child == allPertanyaan[j].index &&
                      allJawabanChild[k].no_soal_child ==
                        allPertanyaan[j].no_soal_child
                    ) {
                      const keyImageChildAnswer =
                        allJawabanChild[k].no +
                        "_" +
                        allJawabanChild[k].num_child +
                        "_" +
                        allJawabanChild[k].no_soal_child +
                        "_" +
                        allJawabanChild[k].index;
                      const answerContent = {
                        key: this.abjad[index_abjad],
                        option: allJawabanChild[k].jawaban,
                        key_image: keyImageChildAnswer,
                        type: "choose",
                      };
                      answer.push(answerContent);
                      index_abjad++;
                    }
                  }
                  is_next = true;
                  const subContent = {
                    key: key,
                    question: allPertanyaan[j].pertanyaan,
                    key_image:
                      "p" +
                      allPertanyaan[j].no +
                      "_" +
                      allPertanyaan[j].index +
                      "_" +
                      allPertanyaan[j].no_soal_child,
                    is_next: is_next,
                    answer: answer,
                  };
                  sub.push(subContent);
                }
                key++;
              }
            }
            const keyImage = this.state.no_soal + "_" + datax[i].index;
            const option = {
              key: this.abjad[datax[i].index],
              option: datax[i].jawaban,
              key_image: keyImage,
              color: false,
              is_next: is_next,
              sub: sub,
            };
            jawaban.push(option);
          }
        }
        soal.jawaban = jawaban;
      } else if (this.state.tipe == "pertanyaan_terbuka") {
        delete soal.jawaban;
      }
      localStorage.setItem(this.state.no_soal, JSON.stringify(soal));
      this.setState({
        choices: [],
        pertanyaan: "",
        no_soal: this.state.no_soal + 1,
      });

      const callBackVal = {
        is_add_soal: true,
      };
      this.props.parentCallBack(callBackVal);

      swal
        .fire({
          title: "Soal Berhasil Disimpan",
          icon: "success",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          window.scrollTo(0, 0);
        });
    } else {
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleChangeTipeAction(e) {
    const tipe = e.target.value;
    this.setState(
      {
        tipe: tipe,
        show_objective: false,
        show_multiple: false,
        show_terbuka: false,
        show_trigger: false,
        show_skala: false,
      },
      () => {
        switch (tipe) {
          case "objective":
            this.setState({ show_objective: true });
            break;
          case "multiple_choice":
            this.setState({ show_multiple: true });
            break;
          case "pertanyaan_terbuka":
            this.setState({ show_terbuka: true });
            break;
          case "triggered_question":
            this.setState({ show_trigger: true });
            break;
          case "skala":
            this.setState({ show_skala: true });
            break;
        }
      },
    );
  }

  handleChangePertanyaanAction(e) {
    e.preventDefault();
    this.setState({ pertanyaan: e.currentTarget.value });
  }

  handleChangeGambarAction(e) {
    const imageFile = e.target.files[0];
    const reader = new FileReader();
    const keyImage = this.state.no_soal;
    this.setState({ key_gambar_pertanyaan: keyImage });
    resizeFile(imageFile).then((cf) => {
      reader.readAsDataURL(cf);
      reader.onload = function () {
        let result = reader.result;
        let fileName = imageFile.name;
        let wrapper = document.getElementById("temp_image");
        let check = document.getElementById(keyImage);
        if (check) {
          check.value = fileName + "_" + result;
        } else {
          const hiddenWrapper = document.createElement("input");
          hiddenWrapper.value = fileName + "_" + result;
          hiddenWrapper.id = keyImage;
          hiddenWrapper.className = "imagePertanyaan";
          hiddenWrapper.type = "hidden";
          wrapper.appendChild(hiddenWrapper);
        }
      };
    });
  }

  render() {
    return (
      <div>
        <h5 className="mt-7 mb-5 me-3">Soal {this.state.no_soal}</h5>
        <div className="row">
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">Pertanyaan</label>
            <input
              className="form-control form-control-sm"
              placeholder="Masukkan pertanyaan"
              name="pertanyaan"
              value={this.state.pertanyaan}
              onChange={this.handleChangePertanyaan}
            />
            <span style={{ color: "red" }}>
              {this.state.errors["pertanyaan"]}
            </span>
          </div>
          <div className="col-lg-12">
            <div className="mb-7 fv-row">
              <label className="form-label">Gambar Pertanyaan (Optional)</label>
              <input
                id="upload_image_form"
                type="file"
                className="form-control form-control-sm mb-2"
                name="upload_silabus"
                accept="image/*"
                onChange={this.handleChangeGambar}
                value={this.state.gambar}
              />
              <small className="text-muted">
                Format File (.jpg/.jpeg/.png/.svg)
              </small>
              <br />
              <span style={{ color: "red" }}>
                {this.state.errors["upload_silabus"]}
              </span>
            </div>
          </div>
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">Jenis Pertanyaan</label>
            <div className="d-flex">
              <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                <input
                  className="form-check-input"
                  onChange={this.handleChangeTipe}
                  type="radio"
                  name="jenis_pertanyaan"
                  value="objective"
                  id="jenis_pertanyaan1"
                />
                <label className="form-check-label" htmlFor="jenis_pertanyaan1">
                  Objective
                </label>
              </div>
              <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                <input
                  className="form-check-input"
                  onChange={this.handleChangeTipe}
                  type="radio"
                  name="jenis_pertanyaan"
                  value="multiple_choice"
                  id="jenis_pertanyaan2"
                />
                <label className="form-check-label" htmlFor="jenis_pertanyaan2">
                  Multiple Choice
                </label>
              </div>
              <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                <input
                  className="form-check-input"
                  onChange={this.handleChangeTipe}
                  type="radio"
                  name="jenis_pertanyaan"
                  value="pertanyaan_terbuka"
                  id="jenis_pertanyaan3"
                />
                <label className="form-check-label" htmlFor="jenis_pertanyaan3">
                  Pertanyaan Terbuka
                </label>
              </div>
              <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                <input
                  className="form-check-input"
                  onChange={this.handleChangeTipe}
                  type="radio"
                  name="jenis_pertanyaan"
                  value="triggered_question"
                  id="jenis_pertanyaan4"
                />
                <label className="form-check-label" htmlFor="jenis_pertanyaan4">
                  Triggered Question
                </label>
              </div>
              {this.props.jenis_survey != 1 ? (
                <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                  <input
                    className="form-check-input"
                    onChange={this.handleChangeTipe}
                    type="radio"
                    name="jenis_pertanyaan"
                    value="skala"
                    id="jenis_pertanyaan5"
                  />
                  <label
                    className="form-check-label"
                    htmlFor="jenis_pertanyaan5"
                  >
                    Skala
                  </label>
                </div>
              ) : (
                ""
              )}
            </div>
            <span style={{ color: "red" }}>
              {this.state.errors["program_dts"]}
            </span>
          </div>

          {this.state.show_objective ? (
            <ObjectiveForm
              choices={this.state.choices}
              no_soal={this.state.no_soal}
            />
          ) : (
            ""
          )}
          {this.state.show_multiple ? (
            <MultipleForm
              choices={this.state.choices}
              no_soal={this.state.no_soal}
            />
          ) : (
            ""
          )}
          {this.state.show_terbuka ? <TerbukaForm /> : ""}
          {this.state.show_trigger ? (
            <TriggerForm
              choices={this.state.choices}
              no_soal={this.state.no_soal}
            />
          ) : (
            ""
          )}

          {this.state.show_skala ? (
            <SkalaForm
              choices={this.state.choices}
              no_soal={this.state.no_soal}
            />
          ) : (
            ""
          )}

          <div className="col-lg-12 my-7 text-center">
            <a
              onClick={this.handleNextSoal}
              className="btn btn-success btn-md "
            >
              <i className="bi bi-plus-circle me-1"></i>Simpan dan Tambah Soal
            </a>
          </div>
        </div>
      </div>
    );
  }
}
