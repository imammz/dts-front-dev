import React, { useState } from "react";
import DataTable from "react-data-table-component";

export default class ImportReport extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickDismiss = this.onHandleClickDismiss.bind(this);
    this.state = {
      datax: [],
      loading: false,
      tempLastNumber: 0,
      newPerPage: 10,
      totalRows: 0,
      currentPage: 1,
      param: "",
      datax: this.props.failed,
    };
  }

  handlePageChange = (page) => {
    this.setState({ loading: true });
    //this.handleReloadTema(page, this.state.newPerPage);
  };
  handlePerRowsChange = async (newPerPage, page) => {
    this.setState({ loading: true });
    this.setState({ newPerPage: newPerPage });
    //this.handleReloadTema(page, newPerPage);
  };

  columns = [
    {
      name: "Nomor",
      cell: (row, index) => row.no,
      width: "70px",
    },
    {
      name: "Soal",
      sortable: false,
      selector: (row) => row.soal,
    },
    {
      name: "Kesalahan",
      sortable: false,
      selector: (row) => row.kesalahan,
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  componentDidMount() {
    console.log(this.state.datax);
  }

  onHandleClickDismiss(e) {
    var modal = document.getElementById("exampleModal");
    document.getElementById("backdrop").style.display = "none";
    document.getElementById("exampleModal").style.display = "none";
    document.getElementById("exampleModal").classList.remove("show");
  }

  render() {
    return (
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title">
            <span className="svg-icon svg-icon-5 me-1">
              <i className="bi bi-sliders text-black"></i>
            </span>
            Error Import
          </h5>
          <div
            className="btn btn-icon btn-sm btn-active-light-primary ms-2"
            data-bs-dismiss="modal"
            aria-label="Close"
            onClick={this.handleClickDismiss}
          >
            <span className="svg-icon svg-icon-2x">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
              >
                <rect
                  opacity="0.5"
                  x="6"
                  y="17.3137"
                  width="16"
                  height="2"
                  rx="1"
                  transform="rotate(-45 6 17.3137)"
                  fill="currentColor"
                ></rect>
                <rect
                  x="7.41422"
                  y="6"
                  width="16"
                  height="2"
                  rx="1"
                  transform="rotate(45 7.41422 6)"
                  fill="currentColor"
                ></rect>
              </svg>
            </span>
          </div>
        </div>
        <div className="modal-body">
          <DataTable
            columns={this.columns}
            data={this.state.datax}
            progressPending={this.state.loading}
            highlightOnHover
            pointerOnHover
            pagination={false}
            paginationServer
            paginationTotalRows={this.state.totalRows}
            onChangeRowsPerPage={this.handlePerRowsChange}
            onChangePage={this.handlePageChange}
            customStyles={this.customStyles}
            persistTableHead={true}
            noDataComponent={<div className="mt-5">Tidak Ada Data</div>}
          />
        </div>
        <div className="modal-footer">
          <div className="d-flex justify-content-between">
            <a
              className="btn btn-sm btn-primary"
              href="#"
              onClick={this.handleClickDismiss}
              data-bs-dismiss="modal"
            >
              Tutup
            </a>
          </div>
        </div>
      </div>
    );
  }
}
