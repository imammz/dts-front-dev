import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import DataTable from "react-data-table-component";
import {
  capitalizeFirstLetter,
  indonesianDateFormatWithoutTime,
} from "../../publikasi/helper";

export default class ExportDataContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickDelete = this.handleClickDeleteAction.bind(this);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleChangeSearch = this.handleChangeSearchAction.bind(this);
    this.handleSort = this.handleSortAction.bind(this);
    this.handleDownloadData = this.handleDownloadDataAction.bind(this);
  }

  state = {
    datax: [],
    loading: true,
    tempLastNumber: 0,
    totalRows: 0,
    newPerPage: 10,
    numberrow: 1,
    searchText: null,
    sort_by: "id",
    sort_val: "DESC",
    user_id_filter: null,
    status: null,
    status_finish: null,
  };
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  columns = [
    {
      name: "No",
      center: true,
      width: "70px",
      cell: (row, index) => (
        <div>
          <span>{this.state.tempLastNumber + index + 1}</span>
          <br />
        </div>
      ),
    },
    {
      name: "Author",
      sortable: true,
      width: "250px",
      selector: (row) => (
        <div>
          <span className="d-flex flex-column my-2">
            <a
              href={"/site-management/export-data/detail/" + row.id}
              className="text-dark"
            >
              <span
                className="fw-bolder fs-7"
                style={{
                  overflow: "hidden",
                  whiteSpace: "wrap",
                  textOverflow: "ellipses",
                }}
              >
                {capitalizeFirstLetter(row.user_name)}
              </span>
            </a>
          </span>
        </div>
      ),
    },
    {
      name: "Tgl. Export Data",
      sortable: true,
      selector: (row) => indonesianDateFormatWithoutTime(row.created_at),
      width: "200px",
    },
    {
      name: "Status",
      sortable: true,
      center: true,
      selector: (row) => (
        <div>
          {row.status == "Waiting" ? (
            <span className="badge badge-warning mt-2 mb-2 ms-2 me-2">
              {row.status}
            </span>
          ) : row.status == "Done" ? (
            <span className="badge badge-success mt-2 mb-2 ms-2 me-2">
              {row.status}
            </span>
          ) : (
            <span className="badge badge-danger mt-2 mb-2 ms-2 me-2">
              {row.status}
            </span>
          )}
        </div>
      ),
    },
    {
      name: "Aksi",
      center: true,
      width: "200px",
      cell: (row) => (
        <div className="row">
          <a
            href={"/site-management/export-data/detail/" + row.id}
            title="View"
            className="btn btn-icon btn-bg-primary btn-sm me-1"
          >
            <i className="bi bi-eye-fill text-white"></i>
          </a>
          {row.status_finish == 1 ? (
            <a
              href="#"
              url={
                process.env.REACT_APP_BASE_API_URI +
                "/get-file-export?path=" +
                row.file
              }
              file={row.file}
              onClick={this.handleDownloadData}
              title="Download" /* target="_blank" */
              className="btn btn-icon btn-bg-success btn-sm me-1"
            >
              <i className="bi bi-cloud-download text-white"></i>
            </a>
          ) : (
            ""
          )}
          <a
            href="#"
            id={row.id}
            onClick={this.handleClickDelete}
            title="Hapus"
            className="btn btn-icon btn-bg-danger btn-sm me-1"
          >
            <i className="bi bi-trash-fill text-white"></i>
          </a>
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };
  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    this.handleReload(this.state.from, this.state.param, 1, 10);
  }
  handleReload(page, newPerPage) {
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });

    let dataBody = {
      start: start_tmp,
      length: length_tmp,
      user_id: this.state.user_id_filter,
      status: this.state.status,
      status_finish: this.state.status_finish,
      search: this.state.searchText,
      sort_by: this.state.sort_by,
      sort_val: this.state.sort_val,
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/exportData/API_List_Export_Data",
        dataBody,
        this.configs,
      )
      .then((res) => {
        this.setState({ loading: false });
        const datax = res.data.result.Data;
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        const total = res.data.result.TotalLength;
        if (statusx) {
          this.setState({ datax });
          this.setState({ totalRows: total });
          this.setState({ currentPage: page });
        } else {
          this.setState({ datax: [] });
          this.setState({ loading: false });
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      })
      .catch((error) => {
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        this.setState({ datax: [] });
        this.setState({ loading: false });
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  handlePageChange = (page) => {
    this.setState({ loading: true });
    this.handleReload(page, this.state.newPerPage);
  };
  handlePerRowsChange = async (newPerPage, page) => {
    this.setState({ loading: true });
    this.setState({ newPerPage: newPerPage });
    this.handleReload(page, newPerPage);
  };

  handleChangeSearchAction(e) {
    const searchText = e.currentTarget.value;
    if (searchText == "") {
      this.setState(
        {
          loading: true,
          searchText: "",
        },
        () => {
          this.handleReload(1, this.state.newPerPage);
        },
      );
    }
  }

  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    if (e.key == "Enter") {
      if (searchText == "") {
        this.setState(
          {
            isSearch: false,
            param: "",
          },
          () => {
            this.handleReload(1, this.state.newPerPage);
          },
        );
      } else {
        this.setState({ loading: true });
        this.setState({ isSearch: true });
        this.setState({ searchText: searchText }, () => {
          this.handleReload(1, this.state.newPerPage);
        });
      }
    }
  }

  handleClickDeleteAction(e) {
    const idx = e.currentTarget.id;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          const data = { id: idx };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/API_Delete_Export_Data",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload(
                        this.state.currentPage,
                        this.state.newPerPage,
                      );
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmationBUttonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload();
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload();
                    }
                  });
              }
            });
        }
      });
  }

  handleSortAction(column, sortDirection) {
    let server_name = "";
    if (column.name == "History Filter Data") {
      server_name = "user_name";
    } else if (column.name == "Tanggal Filter Data") {
      server_name = "created_at";
    } else if (column.name == "Status") {
      server_name = "status";
    }

    this.setState(
      {
        sort_by: server_name,
        sort_val: sortDirection,
      },
      () => {
        this.handleReload(1, this.state.newPerPage);
      },
    );
  }

  handleDownloadDataAction(e) {
    const url = e.currentTarget.getAttribute("url");
    const nama = e.currentTarget.getAttribute("file");

    const configs = {
      headers: {
        Authorization: "Bearer " + Cookies.get("token"),
      },
      responseType: "arraybuffer",
    };
    swal.fire({
      title: "Memproses Download File, Mohon Tunggu..",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios.get(url, configs).then((response) => {
      var fileURL = window.URL.createObjectURL(new Blob([response.data]));
      var fileLink = document.createElement("a");
      fileLink.href = fileURL;
      fileLink.setAttribute("download", nama);
      document.body.appendChild(fileLink);
      fileLink.click();
      swal.close();
      window.location.reload();
    });
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Export Data
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <a
                  href={"/site-management/export-data/tambah"}
                  className="btn btn-success fw-bolder btn-sm"
                >
                  <i className="bi bi-plus-circle"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Export Data
                  </span>
                </a>
              </div>
              {/* <a href="https://back.dev.sdmdigital.id/api/report-pelatihan" className="btn btn-primary btn-sm me-2">
                <i className="las la-file-export"></i>
                Export
              </a>
              <a href="/pelatihan/tambah-pelatihan" className="btn btn-primary btn-sm">
                <i className="las la-plus"></i>
                Tambah Pelatihan
              </a> */}
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            List Export Data
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <div className="d-flex align-items-center position-relative my-1 me-2">
                            <span className="svg-icon svg-icon-1 position-absolute ms-6">
                              <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className="mh-50px"
                              >
                                <rect
                                  opacity="0.5"
                                  x="17.0365"
                                  y="15.1223"
                                  width="8.15546"
                                  height="2"
                                  rx="1"
                                  transform="rotate(45 17.0365 15.1223)"
                                  fill="currentColor"
                                ></rect>
                                <path
                                  d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                  fill="currentColor"
                                ></path>
                              </svg>
                            </span>
                            <input
                              type="text"
                              data-kt-user-table-filter="search"
                              className="form-control form-control-sm form-control-solid w-250px ps-14"
                              placeholder="Cari Data"
                              onKeyPress={this.handleKeyPress}
                              onChange={this.handleChangeSearch}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="table-responsive">
                          <DataTable
                            columns={this.columns}
                            data={this.state.datax}
                            progressPending={this.state.loading}
                            highlightOnHover
                            pointerOnHover
                            pagination
                            paginationServer
                            paginationTotalRows={this.state.totalRows}
                            paginationComponentOptions={{
                              selectAllRowsItem: true,
                              selectAllRowsItemText: "Semua",
                            }}
                            onChangeRowsPerPage={this.handlePerRowsChange}
                            onChangePage={this.handlePageChange}
                            customStyles={this.customStyles}
                            persistTableHead={true}
                            noDataComponent={
                              <div className="mt-5">Tidak Ada Data</div>
                            }
                            onSort={this.handleSort}
                            sortServer
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
