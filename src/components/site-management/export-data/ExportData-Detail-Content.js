import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import { indonesianDateFormat } from "../../publikasi/helper";

export default class ExportDataDetailContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleSort = this.handleSortAction.bind(this);
    this.kembali = this.kembaliAction.bind(this);
  }

  state = {
    datax: [],
    loading: true,
    totalRows: "",
    tempLastNumber: 0,
    newPerPage: 10,
    column: "",
    sortDirection: "DESC",
    sort: "id",
    searchText: "",
    id_export: null,
    akademi: "-",
    tema: "-",
    pelatihan: "-",
    penyelenggara: "-",
    mitra: "-",
    kelamin: "-",
    pelatihan_provinsi: "-",
    pelatihan_kabkot: "-",
    domisili_provinsi: "-",
    domisili_kabkot: "-",
    domisili_kecamatan: "-",
    domisili_desa: "-",
    filter: [],
  };
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  columns = [
    {
      name: "No",
      center: true,
      width: "70px",
      cell: (row, index) => (
        <div>
          <span>{this.state.tempLastNumber + index + 1}</span>
          <br />
        </div>
      ),
    },
    {
      name: "Nama Peserta",
      sortable: true,
      selector: (row) => row.user_name,
      width: "250px",
    },
    {
      name: "Pelatihan",
      sortable: true,
      selector: (row) => row.pelatihan_name,
      //width: '250px',
    },
    {
      name: "Tanggal Pelatihan",
      sortable: true,
      center: true,
      selector: (row) => (
        <div>
          {indonesianDateFormat(row.pelatihan_mulai)} -{" "}
          {indonesianDateFormat(row.pelatihan_selesai)}
        </div>
      ),
      //width: '250px',
    },
  ];

  kembaliAction() {
    window.history.back();
  }

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    } else {
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });

      let segment_url = window.location.pathname.split("/");
      let id_export = segment_url[4];
      this.setState({ id_export: id_export });

      const data = { id: id_export };

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/API_Detail_Export_Data",
          data,
          this.configs,
        )
        .then((res) => {
          const statusx = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statusx) {
            const datax = res.data.result.Data[0];
            const filter = JSON.parse(datax.filter);
            const akademi = datax.akademi_name;
            const tema = datax.tema_name;
            const pelatihan = datax.pelatihan_name;
            const penyelenggara = datax.penyelenggara_name;
            const mitra = datax.mitra_name;
            const kelamin = datax.kelamin_name;
            const pelatihan_provinsi = datax.lokasi_pelatihan_provinsi_name;
            const pelatihan_kabkot = datax.lokasi_pelatihan_kabkot_name;
            const domisili_provinsi = datax.lokasi_domisili_provinsi_name;
            const domisili_kabkot = datax.lokasi_domisili_kabkot_name;
            const domisili_kecamatan = datax.lokasi_domisili_kecamatan_name;
            const domisili_desa = datax.lokasi_domisili_desa_name;

            this.setState({
              akademi,
              tema,
              pelatihan,
              penyelenggara,
              mitra,
              kelamin,
              pelatihan_provinsi,
              pelatihan_kabkot,
              domisili_provinsi,
              domisili_kabkot,
              domisili_kecamatan,
              domisili_desa,
              filter,
            });
            console.log(filter);
            this.handleReload();
          }
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    }
  }

  handleReload(page, newPerPage) {
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });

    const currentData = this.state.filter;
    const filter = {
      start: start_tmp,
      length: length_tmp,
      akademi: currentData.akademi,
      tema: currentData.tema,
      pelatihan: currentData.pelatihan,
      penyelenggara: currentData.penyelenggara,
      mitra: currentData.mitra,
      kelamin: currentData.kelamin,
      status_seleksi: currentData.status_seleksi,
      status_sertifikasi: currentData.status_sertifikasi,
      lokasi_pelatihan_provinsi: currentData.lokasi_pelatihan_provinsi,
      lokasi_pelatihan_kabkot: currentData.lokasi_pelatihan_kabkot,
      lokasi_domisili_provinsi: currentData.lokasi_domisili_provinsi,
      lokasi_domisili_kabkot: currentData.lokasi_domisili_kabkot,
      lokasi_domisili_kecamatan: currentData.lokasi_domisili_kecamatan,
      lokasi_domisili_desa: currentData.lokasi_domisili_desa,
      jenis_output: currentData.jenis_output,
      search: this.state.searchText,
      sort_by: this.state.sort,
      sort_val: this.state.sortDirection,
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/exportData/API_Search_Export_Data",
        filter,
        this.configs,
      )
      .then((res) => {
        this.setState({ loading: false });
        const datax = res.data.result.Data;
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        const total = res.data.result.TotalLength;
        if (statusx) {
          swal.close();
          this.setState({ datax });
          this.setState({ totalRows: total });
          this.setState({ currentPage: page });
        } else {
          swal
            .fire({
              title: "Data Tidak Ditemukan",
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      })
      .catch((error) => {
        swal
          .fire({
            title: "Data Tidak Ditemukan",
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
              this.setState({ datax: [] });
              this.setState({ loading: false });
            }
          });
      });
  }

  handlePageChange = (page) => {
    this.setState({ loading: true });
    this.handleReload(page, this.state.newPerPage);
  };
  handlePerRowsChange = async (newPerPage, page) => {
    this.setState({ loading: true });
    this.setState({ newPerPage: newPerPage });
    this.handleReload(page, newPerPage);
  };

  handleSortAction(column, sortDirection) {
    let server_name = "";
    if (column.name == "Nama Peserta") {
      server_name = "user_name";
    } else if (column.name == "Pelatihan") {
      server_name = "pelatihan_name";
    } else if (column.name == "Tanggal Pelatihan") {
      server_name = "pelatihan_mulai";
    }

    this.setState(
      {
        sort: server_name,
        sortDirection: sortDirection,
      },
      () => {
        this.handleReload(1, this.state.newPerPage);
      },
    );
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Export Data
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.kembali}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
              {/* <a href="https://back.dev.sdmdigital.id/api/report-pelatihan" className="btn btn-primary btn-sm me-2">
                <i className="las la-file-export"></i>
                Export
              </a>
              <a href="/pelatihan/tambah-pelatihan" className="btn btn-primary btn-sm">
                <i className="las la-plus"></i>
                Tambah Pelatihan
              </a> */}
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Parameter Export Data
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="row">
                        <div className="col-md-6 col-lg-6 col-xs-6 mt-3 mb-3">
                          <strong>Akademi</strong>
                          <div>
                            {this.state.akademi ? this.state.akademi : "-"}
                          </div>
                        </div>
                        <div className="col-md-6 col-lg-6 col-xs-6 mt-3 mb-3">
                          <strong>Tema</strong>
                          <div>{this.state.tema ? this.state.tema : "-"}</div>
                        </div>
                        <div className="col-lg-12 mt-3 mb-3">
                          <strong>Pelatihan</strong>
                          <div>
                            {this.state.pelatihan ? this.state.pelatihan : "-"}
                          </div>
                        </div>
                        <div className="col-md-6 col-lg-6 col-xs-6 mt-3 mb-3">
                          <strong>Mitra</strong>
                          <div>{this.state.mitra ? this.state.mitra : "-"}</div>
                        </div>
                        <div className="col-md-6 col-lg-6 col-xs-6 mt-3 mb-3">
                          <strong>Penyelenggara</strong>
                          <div>
                            {this.state.penyelenggara
                              ? this.state.penyelenggara
                              : "-"}
                          </div>
                        </div>
                        <div className="col-md-6 col-lg-6 col-xs-6 mt-3 mb-3">
                          <strong>Jenis Kelamin Peserta</strong>
                          <div>
                            {this.state.kelamin ? this.state.kelamin : "-"}
                          </div>
                        </div>
                        <h5 className="mt-7 text-muted">Lokasi Pelatihan</h5>
                        <div className="col-md-6 col-lg-6 col-xs-6 mt-3 mb-3">
                          <strong>Provinsi Pelatihan</strong>
                          <div>
                            {this.state.pelatihan_provinsi
                              ? this.state.pelatihan_provinsi
                              : "-"}
                          </div>
                        </div>
                        <div className="col-md-6 col-lg-6 col-xs-6 mt-3 mb-3">
                          <strong>Kab/Kota Pelatihan</strong>
                          <div>
                            {this.state.pelatihan_kabkot
                              ? this.state.pelatihan_kabkot
                              : "-"}
                          </div>
                        </div>
                        <h5 className="mt-7 text-muted">Domisili Peserta</h5>
                        <div className="col-md-6 col-lg-6 col-xs-6 mt-3 mb-3">
                          <strong>Provinsi Domisili</strong>
                          <div>
                            {this.state.domisili_provinsi
                              ? this.state.domisili_provinsi
                              : "-"}
                          </div>
                        </div>
                        <div className="col-md-6 col-lg-6 col-xs-6 mt-3 mb-3">
                          <strong>Kab/Kota Domisili</strong>
                          <div>
                            {this.state.domisili_kabkot
                              ? this.state.domisili_kabkot
                              : "-"}
                          </div>
                        </div>
                        <div className="col-md-6 col-lg-6 col-xs-6 mt-3 mb-3">
                          <strong>Kecamatan Domisili</strong>
                          <div>
                            {this.state.domisili_kecamatan
                              ? this.state.domisili_kecamatan
                              : "-"}
                          </div>
                        </div>
                        <div className="col-md-6 col-lg-6 col-xs-6 mt-3 mb-3">
                          <strong>Desa Domisili</strong>
                          <div>
                            {this.state.domisili_desa
                              ? this.state.domisili_desa
                              : "-"}
                          </div>
                        </div>

                        <div className="mt-10">
                          <div className="card-title">
                            <h5 className="me-3 mr-2">Data</h5>
                          </div>
                        </div>
                        <div className="table-responsive mt-5">
                          <DataTable
                            columns={this.columns}
                            data={this.state.datax}
                            progressPending={this.state.loading}
                            highlightOnHover
                            pointerOnHover
                            pagination
                            paginationServer
                            paginationTotalRows={this.state.totalRows}
                            onChangeRowsPerPage={this.handlePerRowsChange}
                            onChangePage={this.handlePageChange}
                            customStyles={this.customStyles}
                            persistTableHead={true}
                            noDataComponent={
                              <div className="mt-5">Tidak Ada Data</div>
                            }
                            onSort={this.handleSort}
                            sortServer
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
