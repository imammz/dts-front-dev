import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import DataTable from "react-data-table-component";
import { indonesianDateFormat } from "../../publikasi/helper";
import ShowUUPdp from "../../pelatihan/Rekappendaftaran/showUUPdp";

export default class ExportDataTambah extends React.Component {
  constructor(props) {
    super(props);
    this.handlePreview = this.handlePreviewAction.bind(this);
    this.kembali = this.kembaliAction.bind(this);
    this.handleSubmit = this.handleSubmitAction.bind(this);
    this.handleChangeAkademi = this.handleChangeAkademiAction.bind(this);
    this.handleChangeTema = this.handleChangeTemaAction.bind(this);
    this.handleChangePelatihan = this.handleChangePelatihanAction.bind(this);
    this.handleChangeMitra = this.handleChangeMitraAction.bind(this);
    this.handleChangeKelamin = this.handleChangeKelaminAction.bind(this);
    this.handleChangeSertifikasi =
      this.handleChangeSertifikasiAction.bind(this);
    this.handleChangePenyelenggara =
      this.handleChangePenyelenggaraAction.bind(this);
    this.handleChangePelatihanProvinsi =
      this.handleChangePelatihanProvinsiAction.bind(this);
    this.handleChangePelatihanKabkot =
      this.handleChangePelatihanKabkotAction.bind(this);
    this.handleChangePesertaProvinsi =
      this.handleChangePesertaProvinsiAction.bind(this);
    this.handleChangePesertaKabkot =
      this.handleChangePesertaKabkotAction.bind(this);
    this.handleChangePesertaKecamatan =
      this.handleChangePesertaKecamatanAction.bind(this);
    this.handleChangePesertaDesa =
      this.handleChangePesertaDesaAction.bind(this);
    this.handleChangeOutput = this.handleChangeOutputAction.bind(this);
    this.handleSort = this.handleSortAction.bind(this);
    this.handleChangeStatus = this.handleChangeStatusAction.bind(this);
  }

  state = {
    datax: [],
    loading: false,
    isLoading: false,
    totalRows: "",
    tempLastNumber: 0,
    newPerPage: 10,
    column: "",
    numberrow: 1,
    errors: {},
    searchText: "",
    valStatus: [],
    status: false,
    akademi_id: null,
    tema_id: null,
    pelatihan_id: null,
    penyelenggara_id: null,
    mitra_id: null,
    kelamin_id: null,
    status_id: null,
    sertifikasi_id: null,
    valAkademi: [],
    pelatihan_provinsi_id: null,
    pelatihan_kabkot_id: null,
    peserta_provinsi_id: null,
    peserta_kabkot_id: null,
    peserta_kecamatan_id: null,
    peserta_desa_id: null,
    valTema: [],
    valPelatihan: [],
    valPenyelenggara: [],
    valKelamin: [],
    valMitra: [],
    valSertifikasi: [],
    valPelatihanProvinsi: [],
    valPelatihanKabkot: [],
    valPesertaProvinsi: [],
    valPesertaKabkot: [],
    valPesertaKecamatan: [],
    valPesertaDesa: [],
    datax_akademi: [],
    datax_tema: [],
    datax_pelatihan: [],
    datax_penyeleggara: [],
    datax_mitra: [],
    datax_status: [],
    datax_sertifikasi: [],
    datax_pelatihan_provinsi: [],
    datax_pelatihan_kabkot: [],
    datax_peserta_provinsi: [],
    datax_peserta_kabkot: [],
    datax_peserta_kecamatan: [],
    datax_peserta_desa: [],
    tema_disabled: true,
    pelatihan_disabled: true,
    pelatihan_kabkot_disabled: true,
    peserta_kabkot_disabled: true,
    peserta_kecamatan_disabled: true,
    peserta_desa_disabled: true,
    output: null,
    is_preview: false,
    sortDirection: "DESC",
    sort: "roles_id",
    showPdpModal: false,
  };

  optionstatus = [
    { value: "1", label: "Publish" },
    { value: "0", label: "Unpublish" },
  ];

  datax_kelamin = [
    { value: "x", label: "Semua" },
    { value: "0", label: "Perempuan" },
    { value: "1", label: "Laki-Laki" },
  ];

  option_output = [
    { value: "0", label: "Rekap + Lampiran" },
    { value: "1", label: "Rekap" },
  ];

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  columns = [
    {
      name: "No",
      center: true,
      width: "70px",
      cell: (row, index) => (
        <div>
          <span>{this.state.tempLastNumber + index + 1}</span>
          <br />
        </div>
      ),
    },
    {
      name: "Nama Peserta",
      sortable: true,
      selector: (row) => row.user_name,
      width: "250px",
    },
    {
      name: "Pelatihan",
      sortable: true,
      selector: (row) => row.pelatihan_name,
      //width: '250px',
    },
    {
      name: "Tanggal Pelatihan",
      sortable: true,
      center: true,
      selector: (row) => (
        <div>
          {indonesianDateFormat(row.pelatihan_mulai)} -{" "}
          {indonesianDateFormat(row.pelatihan_selesai)}
        </div>
      ),
      //width: '250px',
    },
  ];

  handleSortAction(column, sortDirection) {
    let server_name = "";
    if (column.name == "Nama Peserta") {
      server_name = "user_name";
    } else if (column.name == "Pelatihan") {
      server_name = "pelatihan_name";
    } else if (column.name == "Tanggal Pelatihan") {
      server_name = "pelatihan_mulai";
    }

    this.setState(
      {
        sort: server_name,
        sortDirection: sortDirection,
      },
      () => {
        this.handleReload(1, this.state.newPerPage);
      },
    );
  }

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    } else {
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });

      const date = new Date();
      const y = date.getFullYear();
      //akademi
      const dataAkademik = { start: 0, length: 200, status: "publish" };
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/akademi/list-akademi",
          dataAkademik,
          this.configs,
        )
        .then((res) => {
          swal.close();
          const optionx = res.data.result.Data;
          const datax_akademi = [];
          datax_akademi.push({ value: 0, label: "Semua" });
          optionx.map((data) =>
            datax_akademi.push({ value: data.id, label: data.name }),
          );
          this.setState({
            datax_akademi,
          });
        });

      //penyelenggara
      const dataSatker = {
        mulai: 0,
        limit: 200,
        cari: "",
        sort: "id asc",
      };

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/list_satker",
          dataSatker,
          this.configs,
        )
        .then((res) => {
          swal.close();
          const optionx = res.data.result.Data;
          const datax_penyeleggara = [];
          datax_penyeleggara.push({ value: 0, label: "Semua" });
          optionx.map((data) =>
            datax_penyeleggara.push({ value: data.id, label: data.name }),
          );
          this.setState({
            datax_penyeleggara,
          });
        });

      //mitra
      const dataMitra = {
        start: 0,
        length: 200,
        cari: "",
        sort: "id",
        sort_val: "desc",
        param: "",
        status: 1,
        tahun: y,
      };
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/mitra/list-mitra-s3",
          dataMitra,
          this.configs,
        )
        .then((res) => {
          swal.close();
          const optionx = res.data.result.Data;
          const datax_mitra = [];
          datax_mitra.push({ value: 0, label: "Semua" });
          optionx.map((data) =>
            datax_mitra.push({ value: data.id, label: data.nama_mitra }),
          );
          this.setState({
            datax_mitra,
          });
        });

      //seleksi
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/umum/list-status-peserta",
          null,
          this.configs,
        )
        .then((res) => {
          swal.close();
          const optionx = res.data.result.Data;
          const datax_status = [];
          datax_status.push({ value: 0, label: "Semua" });
          optionx.map((data) =>
            datax_status.push({ value: data.id, label: data.name }),
          );
          this.setState({
            datax_status,
          });
        });

      //sertifikasi
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/pelatihanz/sertifikasi_list",
          null,
          this.configs,
        )
        .then((res) => {
          swal.close();
          const optionx = res.data.result.Data;
          const datax_sertifikasi = [];
          datax_sertifikasi.push({ value: 0, label: "Semua" });
          optionx.map((data) =>
            datax_sertifikasi.push({ value: data.id, label: data.name }),
          );
          this.setState({
            datax_sertifikasi,
          });
        });

      //pelatihan_provinsi
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/provinsi",
          null,
          this.configs,
        )
        .then((res) => {
          swal.close();
          const optionx = res.data.result.Data;
          const datax_pelatihan_provinsi = [];
          datax_pelatihan_provinsi.push({ value: 0, label: "Semua" });
          optionx.map((data) =>
            datax_pelatihan_provinsi.push({ value: data.id, label: data.name }),
          );
          this.setState({
            datax_pelatihan_provinsi,
            datax_peserta_provinsi: datax_pelatihan_provinsi,
          });
        });
    }
  }

  kembaliAction() {
    window.history.back();
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const check = this.checkEmpty(dataForm, ["output"], [""]);
    return check;
  }

  resetError() {
    let errors = {};
    errors["output"] = "";
    this.setState({ errors: errors });
  }

  checkEmpty(dataForm, fieldName, fileFieldName) {
    let errors = {};
    let formIsValid = true;
    for (const field in fieldName) {
      const nameAttribute = fieldName[field];
      if (dataForm.get(nameAttribute) == "") {
        errors[nameAttribute] = "Tidak Boleh Kosong";
        formIsValid = false;
      }
    }

    if (this.state.output == "" || this.state.output == null) {
      errors["output"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (
      this.state.akademi_id == null &&
      this.state.tema_id == null &&
      this.state.pelatihan_id == null &&
      this.state.penyelenggara_id == null &&
      this.state.mitra_id == null &&
      this.state.kelamin_id == null &&
      this.state.status_id == null &&
      this.state.sertifikasi_id == null &&
      this.state.pelatihan_provinsi_id == null &&
      this.state.pelatihan_kabkot_id == null &&
      this.state.pelatihan_kabkot_id == null &&
      this.state.peserta_provinsi_id == null &&
      this.state.peserta_kabkot_id == null &&
      this.state.peserta_kecamatan_id == null &&
      this.state.peserta_desa_id == null
    ) {
      errors["filters"] = "Parameter harus ditentukan";
      formIsValid = false;
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  handlePreviewAction(e) {
    e.preventDefault();
    this.resetError();
    const dataForm = new FormData();
    if (this.handleValidation(dataForm)) {
      this.setState({
        is_preview: true,
      });
      this.handleReload();
    } else {
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleReload(page, newPerPage) {
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });

    const filter = {
      start: start_tmp,
      length: length_tmp,
      akademi: this.state.akademi_id,
      tema: this.state.tema_id,
      pelatihan: this.state.pelatihan_id,
      penyelenggara: this.state.penyelenggara_id,
      mitra: this.state.mitra_id,
      kelamin: this.state.kelamin_id,
      status_seleksi: this.state.status_id,
      status_sertifikasi: this.state.sertifikasi_id,
      lokasi_pelatihan_provinsi: this.state.pelatihan_provinsi_id,
      lokasi_pelatihan_kabkot: this.state.pelatihan_kabkot_id,
      lokasi_domisili_provinsi: this.state.pelatihan_provinsi_id,
      lokasi_domisili_kabkot: this.state.peserta_kabkot_id,
      lokasi_domisili_kecamatan: this.state.peserta_kecamatan_id,
      lokasi_domisili_desa: this.state.peserta_desa_id,
      jenis_output: this.state.output,
      search: this.state.searchText,
      sort_by: this.state.sort,
      sort_val: this.state.sortDirection,
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/exportData/API_Search_Export_Data",
        filter,
        this.configs,
      )
      .then((res) => {
        this.setState({ loading: false });
        const datax = res.data.result.Data;
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statusx) {
          swal.close();
          const total = res.data.result.TotalLength;
          this.setState({ datax });
          this.setState({ totalRows: total });
          this.setState({ currentPage: page });
        } else {
          swal
            .fire({
              title: "Data Tidak Ditemukan",
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      })
      .catch((error) => {
        console.log(error);
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: "Data Tidak Ditemukan",
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      });
  }

  handleSubmitAction(e) {
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      this.setState({
        showPdpModal: true,
      });
    } else {
      this.setState({ isLoading: false });
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleProcess() {
    this.setState({ isLoading: true });
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    const filter = {
      akademi: this.state.akademi_id,
      tema: this.state.tema_id,
      pelatihan: this.state.pelatihan_id,
      penyelenggara: this.state.penyelenggara_id,
      mitra: this.state.mitra_id,
      kelamin: this.state.kelamin_id,
      status_seleksi: this.state.status_id,
      status_sertifikasi: this.state.sertifikasi_id,
      lokasi_pelatihan_provinsi: this.state.pelatihan_provinsi_id,
      lokasi_pelatihan_kabkot: this.state.pelatihan_kabkot_id,
      lokasi_domisili_provinsi: this.state.peserta_provinsi_id,
      lokasi_domisili_kabkot: this.state.peserta_kabkot_id,
      lokasi_domisili_kecamatan: this.state.peserta_kecamatan_id,
      lokasi_domisili_desa: this.state.peserta_desa_id,
      jenis_output: this.state.output,
    };

    const bodyJson = {
      filter: JSON.stringify(filter),
      status: "Sedang Diproses",
      file: null,
      status_finish: null,
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/exportData/API_Submit_Export_Data",
        bodyJson,
        this.configs,
      )
      .then((res) => {
        this.setState({ isLoading: false });
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          swal
            .fire({
              title: messagex,
              icon: "success",
              confirmButtonText: "Ok",
              allowOutsideClick: false,
            })
            .then((result) => {
              if (result.isConfirmed) {
                window.location = "/site-management/export-data/";
              }
            });
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      })
      .catch((error) => {
        this.setState({ isLoading: false });
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  handlePageChange = (page) => {
    this.setState({ loading: true });
    this.handleReload(page, this.state.newPerPage);
  };
  handlePerRowsChange = async (newPerPage, page) => {
    this.setState({ loading: true });
    this.setState({ newPerPage: newPerPage });
    this.handleReload(page, newPerPage);
  };

  handleSortAction(column, sortDirection) {
    let server_name = "";
    if (column.name == "Nama Peserta") {
      server_name = "user_name";
    } else if (column.name == "Pelatihan") {
      server_name = "pelatihan_name";
    } else if (column.name == "Tanggal Pelatihan") {
      server_name = "pelatihan_mulai";
    }

    this.setState(
      {
        sort: server_name,
        sortDirection: sortDirection,
      },
      () => {
        this.handleReload(1, this.state.newPerPage);
      },
    );
  }

  handleChangeAkademiAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["filters"] = "";
    this.setState({ errors });

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    this.setState({
      valTema: [],
      valPelatihan: [],
      tema_id: null,
      pelatihan_id: null,
      pelatihan_disabled: true,
    });

    let akademi_id = selectedOption.value;
    if (selectedOption.value == 0) {
      akademi_id = "semua";
    }

    this.setState({
      akademi_id: akademi_id,
      valAkademi: { value: selectedOption.value, label: selectedOption.label },
    });

    if (selectedOption.value == 0) {
      swal.close();
      const datax_tema = [];
      const datax_pelatihan = [];
      this.setState({
        pelatihan_id: null,
        tema_id: null,
        datax_tema,
        datax_pelatihan,
        tema_disabled: true,
        pelatihan_disabled: true,
      });
    } else {
      const dataBody = {
        start: 0,
        rows: 100,
        id_akademi: selectedOption.value,
        status: "null",
        cari: 0,
        sort: "tema",
        sort_val: "ASC",
      };

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/list_tema_filter2",
          dataBody,
          this.configs,
        )
        .then((res) => {
          swal.close();
          const tema_disabled = false;
          const optionx = res.data.result.Data;
          const datax_tema = [];
          datax_tema.push({ value: 0, label: "Semua" });
          optionx.map((data) =>
            datax_tema.push({ value: data.id, label: data.name }),
          );
          this.setState({
            tema_disabled,
            datax_tema,
          });
        })
        .catch((error) => {
          const datax_tema = [];
          this.setState({
            datax_tema,
            tema_disabled: true,
          });
          let messagex = error.response.data.result.Message;
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        });
    }
  };

  handleChangeTemaAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["filters"] = "";
    this.setState({ errors });

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    this.setState({
      valPelatihan: [],
      pelatihan_id: null,
    });

    let tema_id = selectedOption.value;
    if (selectedOption.value == 0) {
      tema_id = "semua";
    }

    this.setState({
      tema_id: tema_id,
      valTema: { value: selectedOption.value, label: selectedOption.label },
    });

    if (selectedOption.value == 0) {
      swal.close();
      const datax_pelatihan = [];
      this.setState({
        pelatihan_id: null,
        datax_pelatihan,
        pelatihan_disabled: true,
      });
    } else {
      const dataBody = {
        jns_param: 3,
        akademi_id: this.state.akademi_id,
        theme_id: selectedOption.value,
        pelatihan_id: 0,
      };

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI +
            "/survey/combo_akademi_pelatihan",
          dataBody,
          this.configs,
        )
        .then((res) => {
          swal.close();
          const pelatihan_disabled = false;
          this.setState({
            valPelatihan: [],
            pelatihan_disabled,
          });

          const optionx = res.data.result.Data;
          const datax_pelatihan = [];
          datax_pelatihan.push({ value: 0, label: "Semua" });
          optionx.map((data) =>
            datax_pelatihan.push({ value: data.pid, label: data.nama }),
          );
          this.setState({
            datax_pelatihan,
          });
        })
        .catch((error) => {
          console.log(error);
          const datax_pelatihan = [];
          this.setState({
            datax_pelatihan,
            valPelatihan: [],
          });
          let messagex = error.response.data.result.Message;

          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
          this.setState({ pelatihan_disabled: true });
        });
    }
  };

  handleChangePelatihanAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["filters"] = "";
    this.setState({ errors });

    let pelatihan_id = selectedOption.value;
    if (selectedOption.value == 0) {
      pelatihan_id = "semua";
    }

    this.setState({
      pelatihan_id: pelatihan_id,
      valPelatihan: {
        value: selectedOption.value,
        label: selectedOption.label,
      },
    });
  };

  handleChangeMitraAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["filters"] = "";
    this.setState({ errors });

    let mitra_id = selectedOption.value;
    if (selectedOption.value == 0) {
      mitra_id = "semua";
    }

    this.setState({
      mitra_id: mitra_id,
      valMitra: { value: selectedOption.value, label: selectedOption.label },
    });
  };

  handleChangePenyelenggaraAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["filters"] = "";
    this.setState({ errors });

    let penyelenggara_id = selectedOption.value;
    if (selectedOption.value == 0) {
      penyelenggara_id = "semua";
    }

    this.setState({
      penyelenggara_id: penyelenggara_id,
      valPenyelenggara: {
        value: selectedOption.value,
        label: selectedOption.label,
      },
    });
  };

  handleChangeKelaminAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["filters"] = "";
    this.setState({ errors });

    let kelamin_id = selectedOption.value;
    if (selectedOption.value == "x") {
      kelamin_id = "semua";
    }

    this.setState({
      kelamin_id: kelamin_id,
      valKelamin: { value: selectedOption.value, label: selectedOption.label },
    });
  };

  handleChangeStatusAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["filters"] = "";
    this.setState({ errors });

    let status_id = selectedOption.value;
    if (selectedOption.value == 0) {
      status_id = "semua";
    }

    this.setState({
      status_id: status_id,
      valStatus: { value: selectedOption.value, label: selectedOption.label },
    });
  };

  handleChangeSertifikasiAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["filters"] = "";
    this.setState({ errors });

    let sertifikasi_id = selectedOption.value;
    if (selectedOption.value == 0) {
      sertifikasi_id = "semua";
    }

    this.setState({
      sertifikasi_id: sertifikasi_id,
      valSertifikasi: {
        value: selectedOption.value,
        label: selectedOption.label,
      },
    });
  };

  handleChangePelatihanProvinsiAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["filters"] = "";
    this.setState({ errors });

    this.setState({
      valPelatihanKabkot: [],
      pelatihan_kabkot_id: null,
      pelatihan_kabkot_disabled: true,
    });

    let pelatihan_provinsi_id = selectedOption.value;
    if (selectedOption.value == 0) {
      pelatihan_provinsi_id = "semua";
    }

    this.setState({
      pelatihan_provinsi_id: pelatihan_provinsi_id,
      valPelatihanProvinsi: {
        value: selectedOption.value,
        label: selectedOption.label,
      },
    });

    if (selectedOption.value != 0) {
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });

      const dataBody = {
        kdprop: selectedOption.value,
      };

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/kabupaten",
          dataBody,
          this.configs,
        )
        .then((res) => {
          swal.close();
          const pelatihan_kabkot_disabled = false;
          const optionx = res.data.result.Data;
          const datax_pelatihan_kabkot = [];
          datax_pelatihan_kabkot.push({ value: 0, label: "Semua" });
          optionx.map((data) =>
            datax_pelatihan_kabkot.push({ value: data.id, label: data.name }),
          );
          this.setState({
            pelatihan_kabkot_disabled,
            datax_pelatihan_kabkot,
          });
        })
        .catch((error) => {
          const datax_pelatihan_kabkot = [];
          this.setState({
            datax_pelatihan_kabkot,
            pelatihan_kabkot_disabled: true,
          });
          let messagex = error.response.data.result.Message;
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        });
    } else {
      this.setState({
        pelatihan_kabkot_id: 0,
      });
    }
  };

  handleChangePelatihanKabkotAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["filters"] = "";
    this.setState({ errors });

    let pelatihan_kabkot_id = selectedOption.value;
    if (selectedOption.value == 0) {
      pelatihan_kabkot_id = "semua";
    }

    this.setState({
      pelatihan_kabkot_id: pelatihan_kabkot_id,
      valPelatihanKabkot: {
        value: selectedOption.value,
        label: selectedOption.label,
      },
    });
  };

  handleChangePesertaProvinsiAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["filters"] = "";
    this.setState({ errors });

    this.setState({
      valPesertaKabkot: [],
      valPesertaKecamatan: [],
      valPesertaDesa: [],
      peserta_kabkot_id: null,
      peserta_kabkot_disabled: true,
      peserta_kecamatan_id: null,
      peserta_kecamatan_disabled: true,
      peserta_desa_id: null,
      peserta_desa_disabled: true,
    });

    let peserta_provinsi_id = selectedOption.value;
    if (selectedOption.value == 0) {
      peserta_provinsi_id = "semua";
    }

    this.setState({
      peserta_provinsi_id: peserta_provinsi_id,
      valPesertaProvinsi: {
        value: selectedOption.value,
        label: selectedOption.label,
      },
    });

    if (selectedOption.value != 0) {
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });

      const dataBody = {
        kdprop: selectedOption.value,
      };

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/kabupaten",
          dataBody,
          this.configs,
        )
        .then((res) => {
          swal.close();
          const peserta_kabkot_disabled = false;
          const optionx = res.data.result.Data;
          const datax_peserta_kabkot = [];
          datax_peserta_kabkot.push({ value: 0, label: "Semua" });
          optionx.map((data) =>
            datax_peserta_kabkot.push({ value: data.id, label: data.name }),
          );
          this.setState({
            peserta_kabkot_disabled,
            datax_peserta_kabkot,
          });
        })
        .catch((error) => {
          const datax_peserta_kabkot = [];
          this.setState({
            datax_peserta_kabkot,
            peserta_kabkot_disabled: true,
          });
          let messagex = error.response.data.result.Message;
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        });
    } else {
      this.setState({
        peserta_kabkot_id: 0,
        peserta_kecamatan_id: 0,
        peserta_desa_id: 0,
      });
    }
  };

  handleChangePesertaKabkotAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["filters"] = "";
    this.setState({ errors });

    this.setState({
      valPesertaKecamatan: [],
      valPesertaDesa: [],
      peserta_kecamatan_id: null,
      peserta_kecamatan_disabled: true,
      peserta_desa_id: null,
      peserta_desa_disabled: true,
    });

    let peserta_kabkot_id = selectedOption.value;
    if (selectedOption.value == 0) {
      peserta_kabkot_id = "semua";
    }

    this.setState({
      peserta_kabkot_id: peserta_kabkot_id,
      valPesertaKabkot: {
        value: selectedOption.value,
        label: selectedOption.label,
      },
    });

    if (selectedOption.value != 0) {
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });

      const dataBody = {
        kdprop: this.state.peserta_provinsi_id,
        kdkab: selectedOption.value,
      };

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/kecamatan",
          dataBody,
          this.configs,
        )
        .then((res) => {
          swal.close();
          const peserta_kecamatan_disabled = false;
          const optionx = res.data.result.Data;
          const datax_peserta_kecamatan = [];
          datax_peserta_kecamatan.push({ value: 0, label: "Semua" });
          optionx.map((data) =>
            datax_peserta_kecamatan.push({ value: data.id, label: data.name }),
          );
          this.setState({
            peserta_kecamatan_disabled,
            datax_peserta_kecamatan,
          });
        })
        .catch((error) => {
          const datax_peserta_kecamatan = [];
          this.setState({
            datax_peserta_kecamatan,
            peserta_kecamatan_disabled: true,
          });
          let messagex = error.response.data.result.Message;
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        });
    } else {
      this.setState({
        peserta_kecamatan_id: 0,
        peserta_desa_id: 0,
      });
    }
  };

  handleChangePesertaKecamatanAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["filters"] = "";
    this.setState({ errors });

    this.setState({
      valPesertaDesa: [],
      peserta_desa_id: null,
      peserta_desa_disabled: true,
    });

    let peserta_kecamatan_id = selectedOption.value;
    if (selectedOption.value == 0) {
      peserta_kecamatan_id = "semua";
    }

    this.setState({
      peserta_kecamatan_id: peserta_kecamatan_id,
      valPesertaKecamatan: {
        value: selectedOption.value,
        label: selectedOption.label,
      },
    });

    if (selectedOption.value != 0) {
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });

      const dataBody = {
        kdprop: this.state.peserta_provinsi_id,
        kdkab: this.state.peserta_kabkot_id,
        kdkec: selectedOption.value,
      };

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/desa",
          dataBody,
          this.configs,
        )
        .then((res) => {
          swal.close();
          const peserta_desa_disabled = false;
          const optionx = res.data.result.Data;
          const datax_peserta_desa = [];
          datax_peserta_desa.push({ value: 0, label: "Semua" });
          optionx.map((data) =>
            datax_peserta_desa.push({ value: data.id, label: data.name }),
          );
          this.setState({
            peserta_desa_disabled,
            datax_peserta_desa,
          });
        })
        .catch((error) => {
          const datax_peserta_desa = [];
          this.setState({
            datax_peserta_desa,
            peserta_desa_disabled: true,
          });
          let messagex = error.response.data.result.Message;
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        });
    } else {
      this.setState({
        peserta_desa_id: 0,
      });
    }
  };

  handleChangePesertaDesaAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["filters"] = "";
    this.setState({ errors });

    let peserta_desa_id = selectedOption.value;
    if (selectedOption.value == 0) {
      peserta_desa_id = "semua";
    }

    this.setState({
      peserta_desa_id: peserta_desa_id,
      valPesertaDesa: {
        value: selectedOption.value,
        label: selectedOption.label,
      },
    });
  };

  handleChangeOutputAction(e) {
    const errors = this.state.errors;
    errors["output"] = "";
    const output = e.currentTarget.value;

    this.setState({
      output,
      errors,
    });
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Export Data
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.kembali}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
              {/* <a href="https://back.dev.sdmdigital.id/api/report-pelatihan" className="btn btn-primary btn-sm me-2">
                <i className="las la-file-export"></i>
                Export
              </a>
              <a href="/pelatihan/tambah-pelatihan" className="btn btn-primary btn-sm">
                <i className="las la-plus"></i>
                Tambah Pelatihan
              </a> */}
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Parameter Export Data
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <form
                        className="form"
                        action="#"
                        onSubmit={this.handleSubmit}
                      >
                        <input
                          type="hidden"
                          name="csrf-token"
                          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                        />
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label ">Akademi</label>
                          <Select
                            id="id_akademi"
                            name="idakademi"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax_akademi
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            onChange={this.handleChangeAkademi}
                            options={this.state.datax_akademi}
                            value={this.state.valAkademi}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["filters"]}
                          </span>
                        </div>
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label ">Tema</label>
                          <Select
                            id="id_tema"
                            name="tema"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax_tema
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            isDisabled={this.state.tema_disabled}
                            onChange={this.handleChangeTema}
                            options={this.state.datax_tema}
                            value={this.state.valTema}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["filters"]}
                          </span>
                        </div>
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label ">Penyeleggara</label>
                          <Select
                            id="id_tema"
                            name="tema"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax_penyeleggara
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            onChange={this.handleChangePenyelenggara}
                            options={this.state.datax_penyeleggara}
                            value={this.state.valPenyelenggara}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["filters"]}
                          </span>
                        </div>
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label ">Pelatihan</label>
                          <Select
                            id="id_pelatihan"
                            name="pelatihan"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax_pelatihan
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            isDisabled={this.state.pelatihan_disabled}
                            onChange={this.handleChangePelatihan}
                            options={this.state.datax_pelatihan}
                            value={this.state.valPelatihan}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["filters"]}
                          </span>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label ">Mitra</label>
                          <Select
                            id="id_mitra"
                            name="mitra"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax_mitra
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            onChange={this.handleChangeMitra}
                            options={this.state.datax_mitra}
                            value={this.state.valMitra}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["filters"]}
                          </span>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label ">Jenis Kelamin</label>
                          <Select
                            id="id_mitra"
                            name="mitra"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.datax_kelamin
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            onChange={this.handleChangeKelamin}
                            options={this.datax_kelamin}
                            value={this.state.valKelamin}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["filters"]}
                          </span>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label ">Status Seleksi</label>
                          <Select
                            id="id_status"
                            name="status"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax_status
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            onChange={this.handleChangeStatus}
                            options={this.state.datax_status}
                            value={this.state.valStatus}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["filters"]}
                          </span>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label ">
                            Status Sertifikasi
                          </label>
                          <Select
                            id="id_sertifikasi"
                            name="sertifikasi"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax_sertifikasi
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            onChange={this.handleChangeSertifikasi}
                            options={this.state.datax_sertifikasi}
                            value={this.state.valSertifikasi}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["filters"]}
                          </span>
                        </div>

                        <h5>Lokasi Pelatihan</h5>
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label ">Provinsi</label>
                          <Select
                            id="id_pelatihan_provinsi"
                            name="pelatihan_provinsi"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax_pelatihan_provinsi
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            onChange={this.handleChangePelatihanProvinsi}
                            options={this.state.datax_pelatihan_provinsi}
                            value={this.state.valPelatihanProvinsi}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["filters"]}
                          </span>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label ">Kabupaten/Kota</label>
                          <Select
                            id="id_pelatihan_kabkot"
                            name="pelatihan_kabkot"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax_pelatihan_kabkot
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            isDisabled={this.state.pelatihan_kabkot_disabled}
                            onChange={this.handleChangePelatihanKabkot}
                            options={this.state.datax_pelatihan_kabkot}
                            value={this.state.valPelatihanKabkot}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["filters"]}
                          </span>
                        </div>

                        <h5>Domisili Peserta</h5>
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label ">Provinsi</label>
                          <Select
                            id="id_peserta_provinsi"
                            name="peserta_provinsi"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax_peserta_provinsi
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            onChange={this.handleChangePesertaProvinsi}
                            options={this.state.datax_peserta_provinsi}
                            value={this.state.valPesertaProvinsi}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["filters"]}
                          </span>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label ">Kabupaten/Kota</label>
                          <Select
                            id="id_peserta_kabkot"
                            name="peserta_kabkot"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax_peserta_kabkot
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            isDisabled={this.state.peserta_kabkot_disabled}
                            onChange={this.handleChangePesertaKabkot}
                            options={this.state.datax_peserta_kabkot}
                            value={this.state.valPesertaKabkot}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["filters"]}
                          </span>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label ">Kecamatan</label>
                          <Select
                            id="id_peserta_kecamatan"
                            name="peserta_kecamatan"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax_peserta_kecamatan
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            isDisabled={this.state.peserta_kecamatan_disabled}
                            onChange={this.handleChangePesertaKecamatan}
                            options={this.state.datax_peserta_kecamatan}
                            value={this.state.valPesertaKecamatan}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["filters"]}
                          </span>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label ">Desa</label>
                          <Select
                            id="id_peserta_desa"
                            name="peserta_desa"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax_peserta_kecamatan
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            isDisabled={this.state.peserta_desa_disabled}
                            onChange={this.handleChangePesertaDesa}
                            options={this.state.datax_peserta_desa}
                            value={this.state.valPesertaDesa}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["filters"]}
                          </span>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">
                            Output Export
                          </label>
                          <div className="d-flex">
                            {this.option_output.map(
                              ({ value, label }, index) => {
                                return (
                                  <div
                                    className="form-check form-check-sm form-check-custom form-check-solid me-5"
                                    key={value}
                                  >
                                    <input
                                      className="form-check-input"
                                      onChange={this.handleChangeOutput}
                                      type="radio"
                                      name="output"
                                      value={value}
                                    />
                                    <label
                                      className="form-check-label"
                                      htmlFor="jenis_pertanyaan1"
                                    >
                                      {label}
                                    </label>
                                  </div>
                                );
                              },
                            )}
                          </div>
                          <span style={{ color: "red" }}>
                            {this.state.errors["output"]}
                          </span>
                        </div>

                        <div className="text-center mt-5 mb-5">
                          <a
                            href="#"
                            onClick={this.handlePreview}
                            className="btn btn-primary btn-md"
                            disabled={this.state.isLoading}
                          >
                            {this.state.isLoading ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm me-2"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Loading...</span>
                                Loading...
                              </>
                            ) : (
                              <>
                                <i className="fa fa-paper-plane me-1"></i>Proses
                              </>
                            )}
                          </a>
                        </div>

                        {this.state.is_preview ? (
                          <div className="table-responsive">
                            <DataTable
                              columns={this.columns}
                              data={this.state.datax}
                              progressPending={this.state.loading}
                              highlightOnHover
                              pointerOnHover
                              pagination
                              paginationServer
                              paginationTotalRows={this.state.totalRows}
                              onChangeRowsPerPage={this.handlePerRowsChange}
                              onChangePage={this.handlePageChange}
                              customStyles={this.customStyles}
                              persistTableHead={true}
                              noDataComponent={
                                <div className="mt-5">Tidak Ada Data</div>
                              }
                              onSort={this.handleSort}
                              sortServer
                            />
                          </div>
                        ) : (
                          ""
                        )}

                        <div className="text-center my-7">
                          {this.state.datax.length > 0 ? (
                            <button
                              type="submit"
                              className="btn btn-primary btn-md"
                              id="submitQuestion1"
                              disabled={this.state.isLoading}
                            >
                              {this.state.isLoading ? (
                                <>
                                  <span
                                    className="spinner-border spinner-border-sm me-2"
                                    role="status"
                                    aria-hidden="true"
                                  ></span>
                                  <span className="sr-only">Loading...</span>
                                  Loading...
                                </>
                              ) : (
                                <>
                                  <i className="fa fa-paper-plane me-1"></i>
                                  Export Data
                                </>
                              )}
                            </button>
                          ) : (
                            ""
                          )}
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <ShowUUPdp
          file_id={1}
          jenis={"export data halaman site management admin"}
          show={this.state.showPdpModal}
          onResolve={() => {
            this.setState({ showPdpModal: false }, () => {
              this.handleProcess();
            });
          }}
          onReject={() => {
            this.setState({ showPdpModal: false });
          }}
        />
      </div>
    );
  }
}
