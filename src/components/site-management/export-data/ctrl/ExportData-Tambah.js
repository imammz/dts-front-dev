import React from "react";
import Header from "../../../../components/Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../ExportData-Tambah-Content";
import SideNav from "../../../../components/SideNav";
import Footer from "../../../../components/Footer";

const ExportDataTambah = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default ExportDataTambah;
