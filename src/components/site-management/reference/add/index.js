import React from "react";
import Header from "../../../Header";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";
import Select from "react-select";
import DataTable from "react-data-table-component";
import {
  fetchDetailZonasi,
  fetchListKotaKab,
  fetchListProvinsi,
  fetchReferences,
  fetchStatusAktif,
  fetchZonasi,
  hasError,
  insertReferenceNoRelation,
  insertReferenceWithRelation,
  resetErrorReferenceNoRelation,
  resetErrorReferenceWithRelation,
  validateReferenceNoRelation,
  validateReferenceWithRelation,
} from "../actions";
import Swal from "sweetalert2";
import { capitalizeFirstLetter } from "../../../publikasi/helper";
import { withRouter } from "../../../RouterUtil";
import AddEditForm from "./AddEditForm";
import Cookies from "js-cookie";

class Content extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      currentRowsPerPage: 10,
      currentPage: 1,
      sortBy: 0,
      sortDir: "asc",

      dataReferences: [],
      dataStatusAktif: [],
      dataProvinsi: [],

      loading: false,
      selectedProvinsis: [],

      name: "",
      nameError: null,
      status: null,
      statusError: null,
      selectedReference: null,
      selectedReferenceError: null,
      selectedReferenceValues: [],
      selectedReferenceLoading: false,
      // values: [{}],
      // valuesError: [null],
      // relations: [{}],
      // relationsError: [null],
      // relationsValues: [[]],
      // relationsValuesError: [[]],
      values: [],
      valuesError: [],
      relations: [],
      relationsError: [],
      relationsValues: [],
      relationsValuesError: [],
    };
  }

  fetch = async () => {
    const { id } = this.props.params || {};

    try {
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info",
        allowOutsideClick: false,
        didOpen: () => Swal.showLoading(),
      });

      const [
        dataStatusAktif = [],
        dataProvinsi = [],
        { data: dataReferences = [] },
      ] = await Promise.all([
        fetchStatusAktif(),
        fetchListProvinsi(),
        fetchReferences({
          start: 0,
          length: 1000,
          search: "",
          orderBy: "id",
          orderDir: "desc",
        }),
      ]);

      this.setState({
        dataStatusAktif,
        dataProvinsi,
        dataReferences: dataReferences.map(({ id, name }) => ({
          value: id,
          label: name,
        })),
      });

      Swal.close();
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  saveReferenceNoRelation = async () => {
    try {
      this.setState({ isLoading: true });
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info",
        allowOutsideClick: false,
        didOpen: () => Swal.showLoading(),
      });

      const payload = {
        name: this.state.name,
        status: this.state.status,
        user_by: parseInt(Cookies.get("user_id")),
        value: JSON.stringify(
          this.state?.values?.map(({ value = null, label }, idx) => ({
            id: value,
            value: label,
            status: "insert",
          })),
        ),
      };

      // console.log("payload", JSON.stringify(payload, null, 2));
      // return;
      const message = await insertReferenceNoRelation(payload);

      Swal.close();
      this.setState({ isLoading: false });
      await Swal.fire({
        title: message || "Referensi berhasil disimpan",
        icon: "success",
        confirmButtonText: "Ok",
      });

      window.location.href = "/site-management/reference";
    } catch (err) {
      this.setState({ isLoading: false });
      Swal.close();
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  saveReferenceWithRelation = async () => {
    try {
      this.setState({ isLoading: true });
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info",
        allowOutsideClick: false,
        didOpen: () => Swal.showLoading(),
      });
      const payload = {
        name: this.state.name,
        status: this.state.status,
        user_by: parseInt(Cookies.get("user_id")),
        reference_id: parseInt(this?.state?.selectedReference),
        value: JSON.stringify(
          this?.state?.relations?.map((rel, idx) => ({
            data_references_id: parseInt(this?.state?.selectedReference),
            relasi_id: rel?.value,
            // value: [{ value: "asdf" }],
            value: [{ value: this?.state?.relations[idx]?.label }],
          })),
        ),
      };

      const message = await insertReferenceWithRelation(payload);

      Swal.close();
      this.setState({ isLoading: false });
      await Swal.fire({
        title: message || "Referensi berhasil disimpan",
        icon: "success",
        confirmButtonText: "Ok",
      });

      window.location.href = "/site-management/reference";
    } catch (err) {
      this.setState({ isLoading: false });
      Swal.close();
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  checkAndSave = () => {
    if (!this?.state?.selectedReference) {
      const errors = validateReferenceNoRelation(this.state);
      if (!hasError(errors)) {
        this.saveReferenceNoRelation();
      } else {
        this.setState({ ...resetErrorReferenceNoRelation(), ...errors });

        Swal.fire({
          title: "Masih terdapat isian yang belum lengkap",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      }
    } else {
      const errors = validateReferenceWithRelation(this.state);
      if (!hasError(errors)) {
        this.saveReferenceWithRelation();
      } else {
        this.setState({ ...resetErrorReferenceWithRelation(), ...errors });

        Swal.fire({
          title: "Masih terdapat isian yang belum lengkap",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      }
    }
  };

  componentDidMount() {
    this.fetch();
  }

  render() {
    return (
      <AddEditForm
        label={"Tambah Reference"}
        {...this.props}
        {...this.state}
        onChange={(k, v, cb) => this.setState({ [k]: v }, cb)}
        onSave={() => this.checkAndSave()}
      />
    );
  }
}

const ViewZonasi = (props) => {
  return (
    <div>
      <SideNav />
      <Header />
      <Content {...props} />
      <Footer />
    </div>
  );
};

export default withRouter(ViewZonasi);
