import { saveAs } from "file-saver";
import React from "react";
import DataTable from "react-data-table-component";
import Select from "react-select";
import * as XLSX from "xlsx";

function s2ab(s) {
  if (typeof ArrayBuffer !== "undefined") {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xff;
    return buf;
  } else {
    var buf = new Array(s.length);
    for (var i = 0; i != s.length; ++i) buf[i] = s.charCodeAt(i) & 0xff;
    return buf;
  }
}

class AddDialog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: null,
      label: null,
    };
  }

  onClose = () => {
    this.setState({ value: "" });
  };

  render() {
    const {
      ref,
      id = "modal_add",
      onChange = () => null,
      relationLabel,
      selectedReferenceValues,
      selectedReferenceLoading,
      relations = [],
      relationsError = [],
    } = this.props || {};

    return (
      <div className="modal fade" tabindex="-1" id={id} ref={ref}>
        <div className="modal-dialog modal-lg">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Tambah Relasi Data</h5>
              <div
                className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal"
                aria-label="Close"
              >
                <span className="svg-icon svg-icon-2x">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <rect
                      opacity="0.5"
                      x="6"
                      y="17.3137"
                      width="16"
                      height="2"
                      rx="1"
                      transform="rotate(-45 6 17.3137)"
                      fill="currentColor"
                    />
                    <rect
                      x="7.41422"
                      y="6"
                      width="16"
                      height="2"
                      rx="1"
                      transform="rotate(45 7.41422 6)"
                      fill="currentColor"
                    />
                  </svg>
                </span>
              </div>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-lg-12 mb-7 fv-row">
                  <label className="form-label required">
                    Value {relations.length + 1}
                  </label>
                  <Select
                    placeholder={`Silahkan Pilih ${relationLabel}`}
                    className="form-select-sm selectpicker p-0"
                    value={
                      this.state.value
                        ? { value: this.state.value, label: this.state.label }
                        : null
                    }
                    onChange={({ value, label }) => {
                      this.setState({ value, label });
                    }}
                    options={selectedReferenceValues}
                    isOptionDisabled={(option) =>
                      relations
                        .map(({ value }) => `${value}`)
                        .includes(`${option?.value}`)
                    }
                    isLoading={selectedReferenceLoading}
                  />
                  <span style={{ color: "red" }}>
                    {relationsError[relations.length]}
                  </span>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <div className="d-flex justify-content-between">
                <button
                  type="reset"
                  className="btn btn-sm btn-light me-3"
                  data-bs-dismiss="modal"
                  onClick={this.onClose}
                >
                  Batal
                </button>
                <button
                  type="submit"
                  className="btn btn-sm btn-primary"
                  data-bs-dismiss="modal"
                  onClick={() => {
                    relationsError[relations.length] = null;
                    onChange("relationsError", relationsError);

                    relations[relations.length] = {
                      value: this.state.value,
                      label: this.state.label,
                      values: [],
                    };
                    onChange("relations", relations);

                    this.onClose();
                  }}
                >
                  Simpan
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

class ImportDialog extends React.Component {
  constructor(props) {
    super(props);
    this.filePesertaRef = React.createRef();

    this.state = {
      fileTemplateXlsx: null,
      dataPreview: [],
    };
  }

  downloadFileTemplateXlsx = () => {
    var relationLabel = this.props?.relationLabel || "";
    var selectedReferenceValues = this.props?.selectedReferenceValues || [];

    var ws = XLSX.utils.json_to_sheet([{ Value: null }]);
    var dictws = XLSX.utils.json_to_sheet(selectedReferenceValues);

    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Reference");
    XLSX.utils.book_append_sheet(wb, dictws, `Kamus ${relationLabel}`);
    var wbout = XLSX.write(wb, {
      bookType: "xlsx",
      bookSST: true,
      type: "binary",
    });
    saveAs(
      new Blob([s2ab(wbout)], { type: "application/octet-stream" }),
      "Template Data Reference Dengan Relasi.xlsx",
    );
  };

  onFileTemplateXlsxChange = (f) => {
    this.setState({ fileTemplateXlsx: f }, () => this.readTemplateContent());
  };

  readTemplateContent = async () => {
    if (!this.state.fileTemplateXlsx) this.setState({ dataPreview: [] });

    const templateBin = await new Promise((resolve, reject) => {
      var reader = new FileReader();
      reader.onload = () => {
        resolve(reader.result);
      };
      reader.readAsArrayBuffer(this.state.fileTemplateXlsx);
    });

    var wb = XLSX.read(new Uint8Array(templateBin), { type: "array" });
    var dataPreview = XLSX.utils.sheet_to_json(wb.Sheets["Reference"], {
      header: 2,
    });

    this.setState({ dataPreview: dataPreview });
  };

  onClose = () => {
    this.setState({ fileTemplateXlsx: null, dataPreview: [] });
  };

  render() {
    var selectedReferenceValues = this.props?.selectedReferenceValues || [];
    const { ref, id = "import", onImport = () => null } = this.props || {};

    return (
      <div className="modal fade" tabindex="-1" id={id} ref={ref}>
        <div className="modal-dialog modal-lg">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">
                <i className="bi bi-info me-1"></i>
                Import Relasi Data
              </h5>
              <div
                className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal"
                aria-label="Close"
              >
                <span className="svg-icon svg-icon-2x">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <rect
                      opacity="0.5"
                      x="6"
                      y="17.3137"
                      width="16"
                      height="2"
                      rx="1"
                      transform="rotate(-45 6 17.3137)"
                      fill="currentColor"
                    />
                    <rect
                      x="7.41422"
                      y="6"
                      width="16"
                      height="2"
                      rx="1"
                      transform="rotate(45 7.41422 6)"
                      fill="currentColor"
                    />
                  </svg>
                </span>
              </div>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-lg-12 mb-7 fv-row">
                  <div className="highlight bg-light-primary">
                    <div className="col-lg-12 mb-7 fv-row text-primary">
                      <h5 className="text-primary fs-5">Panduan</h5>
                      <p className="text-primary">
                        Sebelum melakukan import, mohon untuk membaca panduan
                        berikut :
                      </p>
                      <ul>
                        <li>
                          Silahkan unduh template untuk melakukan import pada
                          link berikut{" "}
                          <a
                            href="#"
                            onClick={() => this.downloadFileTemplateXlsx()}
                            className="btn btn-primary fw-semibold btn-sm py-1 px-2"
                          >
                            <i className="las la-cloud-download-alt fw-semibold me-1" />
                            Download Template
                          </a>
                        </li>
                        <li>Lengkapi isian pada kolom value</li>
                        <li>Upload kembali template yang telah dilengkapi</li>
                        <li>Simpan</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12 mb-7 fv-row">
                  <label className="form-label required">
                    Upload Template Reference
                  </label>
                  <input
                    ref={this.filePesertaRef}
                    type="file"
                    className="form-control form-control-sm mb-2"
                    name="upload_silabus"
                    accept=".xlsx"
                    onInput={(e) => {
                      const [f] = [...e.target.files];
                      this.onFileTemplateXlsxChange(f);
                      e.target.value = null;
                    }}
                  />
                  <small className="text-muted">
                    Format File (.xlsx), Max 10240 Kb
                  </small>
                  <br />
                </div>
                {this.state.fileTemplateXlsx && (
                  <div className="col-lg-12 mb-7 fv-row">
                    <div className="row">
                      <div className="col-lg-12 mb-7 fv-row">
                        <span>{this.state.fileTemplateXlsx?.name}</span>
                        <div
                          className="btn btn-sm btn-icon btn-light-danger float-end"
                          title="Hapus file"
                          onClick={(e) => this.onFileTemplateXlsxChange(null)}
                        >
                          <span className="las la-trash-alt" />
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </div>

              {this.state.fileTemplateXlsx && (
                <div className="row">
                  <div className="col-lg-12 mb-7 fv-row">
                    <label className="form-label">Preview</label>
                    <DataTable
                      data={(this.state.dataPreview || []).map((row, idx) => ({
                        idx,
                        ...row,
                      }))}
                      columns={[
                        {
                          name: "No",
                          center: true,
                          width: "70px",
                          cell: (row, index) => row?.idx + 1,
                        },
                        {
                          name: "Value",
                          // width: '225px',
                          // sortable: true,
                          cell: ({ Value }) => Value,
                        },
                        {
                          name: "Label",
                          cell: ({ Value }) => {
                            const { label = "" } =
                              selectedReferenceValues.find(
                                ({ value }) => value == Value,
                              ) || {};
                            return label;
                          },
                        },
                      ]}
                    />
                  </div>
                </div>
              )}
            </div>
            <div className="modal-footer">
              <div className="d-flex justify-content-between">
                <button
                  type="reset"
                  className="btn btn-sm btn-light me-3"
                  data-bs-dismiss="modal"
                  onClick={this.onClose}
                >
                  Batal
                </button>
                <button
                  type="submit"
                  className="btn btn-sm btn-primary"
                  data-bs-dismiss="modal"
                  onClick={() => {
                    onImport(
                      [...this.state.dataPreview].map(({ Value }) => Value),
                    );
                    this.onClose();
                  }}
                >
                  Import
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

class DenganRelasiForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      rowsPerPage: 10,
      keyword: "",
    };
  }

  render() {
    let {
      relations = [],
      relationsError = [],
      relationsValuesError = [[]],
      onChange = (k, v) => {},
      selectedReference,
      selectedReferenceLoading = false,
      selectedReferenceValues = [],
      selectedReferenceSelectedValues = [],
      dataReferences = [],
    } = this.props || {};
    relations.forEach((_, idx) => (relations[idx]["idx"] = idx + 1));

    const [{ label: relationLabel = "" }] = dataReferences.filter(
      (s) => s.value == selectedReference,
    );
    let relationsClone = [...relations].filter(({ label }) => {
      if (this.state.keyword)
        return label
          ?.toLowerCase()
          ?.includes(this.state.keyword?.toLowerCase());
      else return true;
    });
    let lastPage =
      Math.floor(relationsClone.length / this.state.rowsPerPage) + 1;

    relationsClone.reverse();

    return (
      <div className="card border mt-4">
        <div className="card-header">
          <div className="card-title">
            <h5 className="required">Relasi Data : {relationLabel}</h5>
          </div>
          {relationsClone.length > 0 && (
            <div className="card-toolbar">
              <div className="d-flex align-items-center position-relative my-1 me-2">
                <span className="svg-icon svg-icon-1 position-absolute ms-6">
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    className="mh-50px"
                  >
                    <rect
                      opacity="0.5"
                      x="17.0365"
                      y="15.1223"
                      width="8.15546"
                      height="2"
                      rx="1"
                      transform="rotate(45 17.0365 15.1223)"
                      fill="currentColor"
                    ></rect>
                    <path
                      d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                      fill="currentColor"
                    ></path>
                  </svg>
                </span>
                <input
                  type="text"
                  data-kt-user-table-filter="search"
                  className="form-control form-control-sm form-control-solid w-250px ps-14"
                  placeholder={`Cari Relasi ${relationLabel}`}
                  onKeyDown={(e) => {
                    if (e.key == "Enter")
                      this.setState({ keyword: e.target.value, page: 1 });
                  }}
                  onChange={(e) => {
                    if (!e.target.value)
                      this.setState({ keyword: e.target.value, page: 1 });
                  }}
                />
              </div>
            </div>
          )}
        </div>

        <div className="card-body">
          <div className="row">
            <div className="col-lg-12 mb-7 fv-row">
              {relationsClone.length == 0 && (
                <div className="d-flex justify-content-center m-10">
                  Data tidak tersedia
                </div>
              )}
              {[...relationsClone]
                .splice(
                  (this.state.page - 1) * this.state.rowsPerPage,
                  this.state.rowsPerPage,
                )
                .map(({ value, label, values, idx: _idx }, idx) => {
                  idx = (this.state.page - 1) * this.state.rowsPerPage + idx;
                  relationsValuesError[idx] = relationsValuesError[idx] || [];

                  return (
                    <div className="row">
                      <div className="col-lg-12 mb-7 fv-row">
                        <label className="form-label required">
                          Value {_idx}
                        </label>
                        <div className="row">
                          <div className="col-lg-11 mb-7 fv-row">
                            <Select
                              placeholder={`Silahkan Pilih ${relationLabel}`}
                              // noOptionsMessage={({ inputValue }) => !inputValue ? this.state.dataxakademi : "Data tidak tersedia"}
                              value={value ? { value, label } : null}
                              className="form-select-sm selectpicker p-0"
                              onChange={({ value, label }) => {
                                relationsError[idx] = null;
                                onChange("relationsError", relationsError);

                                relations[idx] = { value, label, values: [] };
                                onChange("relations", relations);
                              }}
                              options={selectedReferenceValues}
                              isOptionDisabled={(option) =>
                                relations
                                  .map(({ value }) => `${value}`)
                                  .includes(`${option?.value}`)
                              }
                              isLoading={selectedReferenceLoading}
                            />
                            <span style={{ color: "red" }}>
                              {relationsError[relations.length - 1 - idx]}
                            </span>
                          </div>
                          <div className="col-lg-1">
                            <btn
                              title="Hapus"
                              className={`btn btn-icon btn-active-light-danger w-30px h-30px me-3 ${
                                (relations || []).length <= 1 ? `disabled` : ``
                              }`}
                              onClick={(e) => {
                                let relations = this?.props?.relations;
                                let relationsValues =
                                  this?.props?.relationsValues;

                                relations.splice(relations.length - 1 - idx, 1);
                                relationsValues.splice(
                                  relationsValues.length - 1 - idx,
                                  1,
                                );

                                onChange("relations", relations);
                                onChange("relationsValues", relationsValues);
                              }}
                            >
                              <span className="svg-icon svg-icon-3">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  width={24}
                                  height={24}
                                  viewBox="0 0 24 24"
                                  fill="none"
                                >
                                  <path
                                    d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z"
                                    fill="black"
                                  />
                                  <path
                                    opacity="0.5"
                                    d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z"
                                    fill="black"
                                  />
                                  <path
                                    opacity="0.5"
                                    d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z"
                                    fill="black"
                                  />
                                </svg>
                              </span>
                            </btn>
                          </div>
                        </div>
                        {/* <span style={{ color: "red" }}>{relationsError[(relations.length - 1) - idx]}</span> */}
                      </div>
                    </div>
                  );
                })}
            </div>
            {relationsClone.length > 0 && (
              <div className="col-lg-12 mb-7 fv-row d-flex justify-content-center">
                <div className="d-flex justify-content-between align-items-center flex-wrap">
                  <div className="d-flex flex-wrap py-2 mr-3">
                    <select
                      class="d-flex form-select form-select-sm me-2 my-1 w-100px"
                      onChange={(e) =>
                        this.setState({ rowsPerPage: e.target.value })
                      }
                    >
                      <option value="10">10</option>
                      <option value="15">15</option>
                      <option value="20" selected>
                        20
                      </option>
                      <option value="25">25</option>
                      <option value="30">30</option>
                    </select>

                    <div className="d-flex align-items-center">
                      <div className="me-2">
                        {(this.state.page - 1) * this.state.rowsPerPage + 1}-
                        {relationsClone.length <= this.state.rowsPerPage
                          ? relationsClone.length
                          : Math.min(
                              (this.state.page - 1) * this.state.rowsPerPage +
                                this.state.rowsPerPage,
                              relationsClone.length,
                            )}{" "}
                        of {relationsClone.length}
                      </div>
                    </div>

                    <btn
                      onClick={() => this.setState({ page: 1 })}
                      className={`btn btn-icon btn-sm btn-light me-2 my-1 ${
                        this.state.page == 1 ? `disabled` : ``
                      }`}
                    >
                      <i className="las la-angle-double-left icon-xs"></i>
                    </btn>
                    <btn
                      onClick={() =>
                        this.setState({ page: this.state.page - 1 })
                      }
                      className={`btn btn-icon btn-sm btn-light me-2 my-1 ${
                        this.state.page == 1 ? `disabled` : ``
                      }`}
                    >
                      <i className="las la-angle-left icon-xs"></i>
                    </btn>
                    <btn
                      onClick={() =>
                        this.setState({ page: this.state.page + 1 })
                      }
                      className={`btn btn-icon btn-sm btn-light me-2 my-1 ${
                        this.state.page >= lastPage ? `disabled` : ``
                      }`}
                    >
                      <i className="las la-angle-right icon-xs"></i>
                    </btn>
                    <btn
                      onClick={() => this.setState({ page: lastPage })}
                      className={`btn btn-icon btn-sm btn-light me-2 my-1 ${
                        this.state.page >= lastPage ? `disabled` : ``
                      }`}
                    >
                      <i className="las la-angle-double-right icon-xs"></i>
                    </btn>
                  </div>
                </div>
              </div>
            )}
            <div className="row my-7">
              <div className="col-6">
                <a
                  className="btn btn-sm btn-light text-success d-block btn-block"
                  data-bs-toggle="modal"
                  data-bs-target="#modal_add"
                  onClick={() => {
                    // onChange('relations', [...this?.props?.relations, {}]);
                    // onChange('relationsValues', [...this?.props?.relationsValues, [null]])
                  }}
                >
                  <i className="bi bi-plus-circle text-success me-1"></i>Tambah
                  Relasi Data
                </a>
              </div>
              <div className="col-6">
                <a
                  className="btn btn-sm btn-info d-block btn-block"
                  data-bs-toggle="modal"
                  data-bs-target="#import"
                >
                  <i className="bi bi-cloud-download text-white me-1"></i>Import
                  Relasi Data
                </a>
              </div>
            </div>

            <ImportDialog
              relationLabel={relationLabel}
              selectedReferenceValues={selectedReferenceValues}
              onImport={(_values) => {
                relationsError = [
                  ...relationsError,
                  ..._values.map(() => null),
                ];
                onChange("relationsError", relationsError);

                relations = [
                  ...relations,
                  ..._values.map((Value) => {
                    const { label = "" } =
                      selectedReferenceValues.find(
                        ({ value }) => value == Value,
                      ) || {};
                    return { value: Value, label };
                  }),
                ];
                onChange("relations", relations);
              }}
            />

            <AddDialog
              relationLabel={relationLabel}
              selectedReferenceValues={selectedReferenceValues}
              selectedReferenceLoading={selectedReferenceLoading}
              relations={relations}
              relationsError={relationsError}
              onChange={onChange}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default DenganRelasiForm;
