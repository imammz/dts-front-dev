import React from "react";
import Select from "react-select";
import Swal from "sweetalert2";
import {
  deleteReference,
  fetchDetailReference,
  fetchListKotaKab,
} from "../actions";
import TanpaRelasiForm from "./TanpaRelasiForm";
import DenganRelasiForm from "./DenganRelasiForm";
import _ from "lodash";
import Cookies from "js-cookie";

class AddEditForm extends React.Component {
  constructor(props) {
    super(props);
  }

  remove = async (id) => {
    try {
      const { isConfirmed = false } = await Swal.fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      });

      if (isConfirmed) {
        Swal.fire({
          title: "Mohon Tunggu!",
          icon: "info",
          allowOutsideClick: false,
          didOpen: () => Swal.showLoading(),
        });

        const message = await deleteReference({
          id,
          user_by: parseInt(Cookies.get("user_id")),
        });

        Swal.close();

        await Swal.fire({
          title: message || "Reference berhasil dihapus",
          icon: "success",
          confirmButtonText: "Ok",
        });

        this.back();
      }

      // throw new Error("API delete belum tersedia");
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  back = () => {
    window.location.href = "/site-management/reference";
  };

  render() {
    console.log(this.props.isLoading);
    const { id } = this.props.params || {};

    let {
      header,
      label = "",

      name = "",
      nameError = null,
      status,
      statusError = null,
      dataStatusAktif = [],
      selectedReference,
      selectedReferenceError = null,
      dataReferences = [],
      values = [],

      dataProvinsi = [],
      selectedProvinsis = [],
      selectedProvinsisError = null,
      onChange = (k, v) => {},
      onSave = () => {},
    } = this.props || {};

    const jml_form_repo = (this.props?.formRepos || []).length;

    let _dataReferences = [
      { value: null, label: "Tanpa Reference" },
      ...(dataReferences || []),
    ];

    const [oStatus] = dataStatusAktif.filter((s) => s.value == status);
    const [oSelectedReference] = _dataReferences.filter(
      (s) => s.value == selectedReference,
    );

    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Data Reference
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                  onClick={() =>
                    (window.location.href = "/site-management/reference")
                  }
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
                <button
                  className={`btn btn-sm btn-danger btn-active-light-info ${
                    jml_form_repo > 0 ? "disabled" : ""
                  }`}
                  onClick={(e) => this.remove(id)}
                >
                  <i className="bi bi-trash-fill text-white pe-1"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </button>
              </div>
              {/* <a href="https://back.dev.sdmdigital.id/api/report-pelatihan" className="btn btn-primary btn-sm me-2">
                <i className="las la-file-export"></i>
                Export
              </a>
              <a href="/pelatihan/tambah-pelatihan" className="btn btn-primary btn-sm">
                <i className="las la-plus"></i>
                Tambah Pelatihan
              </a> */}
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >{`${id ? "Edit" : "Tambah"} Data Reference`}</h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="row">
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">
                            Nama Referensi
                          </label>
                          <input
                            className="form-control form-control-sm"
                            placeholder="Masukkan Nama Referensi"
                            value={name}
                            onChange={(e) => {
                              onChange("nameError", "");
                              onChange("name", e.target.value);
                            }}
                          />
                          <span style={{ color: "red" }}>{nameError}</span>
                        </div>
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">Status</label>
                          <Select
                            placeholder="Silahkan Pilih Status"
                            // noOptionsMessage={({ inputValue }) => !inputValue ? this.state.dataxakademi : "Data tidak tersedia"}
                            value={oStatus}
                            className="form-select-sm selectpicker p-0"
                            onChange={({ value }) => {
                              onChange("statusError", "");
                              onChange("status", value);
                            }}
                            options={dataStatusAktif}
                            // isOptionDisabled={(option) => option.disabled}
                          />
                          <span style={{ color: "red" }}>{statusError}</span>
                        </div>
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">Relasi</label>
                          <Select
                            placeholder="Silahkan Pilih Data Reference"
                            // noOptionsMessage={({ inputValue }) => !inputValue ? this.state.dataxakademi : "Data tidak tersedia"}
                            value={oSelectedReference}
                            className="form-select-sm selectpicker p-0"
                            onChange={({ value }) => {
                              onChange("selectedReference", value, async () => {
                                onChange("selectedReferenceLoading", true);
                                onChange("selectedReferenceValues", []);

                                const { header } = await fetchDetailReference({
                                  id: this?.props?.selectedReference,
                                  limit: 1,
                                });

                                let bidx = 0,
                                  pageSize = 500,
                                  batchSize = 10,
                                  data = [];
                                for (const batch of _.chunk(
                                  _.chunk(
                                    Array(header?.total || 0).fill(null),
                                    pageSize,
                                  ),
                                  batchSize,
                                )) {
                                  const _data = await Promise.all(
                                    batch.map(async (b, idx) => {
                                      let page = bidx * batchSize + idx;
                                      let { data = [] } =
                                        await fetchDetailReference({
                                          id: this?.props?.selectedReference,
                                          mulai: page * pageSize,
                                          limit: pageSize,
                                        });
                                      return data;
                                    }),
                                  );

                                  for (const _d of _data) {
                                    data = [
                                      ...data,
                                      ..._d.map(({ label, ...rest }) => ({
                                        ...rest,
                                        label: label || "",
                                      })),
                                    ];
                                  }

                                  bidx += 1;
                                }

                                onChange("values", []);
                                onChange("relations", []);
                                onChange("relationsValues", [[]]);
                                onChange("relationsValuesError", [[]]);
                                onChange("selectedReferenceValues", data);
                                onChange("selectedReferenceLoading", false);
                              });
                            }}
                            options={_dataReferences}
                            // isOptionDisabled={(option) => option.disabled}
                          />
                          <span style={{ color: "red" }}>
                            {selectedReferenceError}
                          </span>
                        </div>
                        <div className="col-lg-12 mb-7 fv-row">
                          {selectedReference ? (
                            <DenganRelasiForm {...this.props} {...this.state} />
                          ) : (
                            <TanpaRelasiForm {...this.props} {...this.state} />
                          )}
                        </div>
                      </div>
                      <div className="row my-7">
                        <div className="col-lg-12 text-center">
                          <button
                            className="btn btn-md btn-light me-3"
                            onClick={() => this.back()}
                          >
                            Batal
                          </button>
                          <button
                            className={`btn btn-md btn-primary`}
                            onClick={() => onSave()}
                            disabled={this.props.isLoading}
                          >
                            {this.props.isLoading ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm me-2"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Loading...</span>
                                Loading...
                              </>
                            ) : (
                              <>
                                <i className="fa fa-paper-plane me-1"></i>Simpan
                              </>
                            )}
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AddEditForm;
