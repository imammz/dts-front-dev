import React from "react";
import Header from "../../../Header";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";
import Select from "react-select";
import DataTable from "react-data-table-component";
import {
  deleteReference,
  fetchDetailReference,
  fetchListProvinsi,
  fetchStatusAktif,
  fetchZonasi,
} from "../actions";
import Swal from "sweetalert2";
import {
  capitalizeFirstLetter,
  capitalizeTheFirstLetterOfEachWord,
} from "../../../publikasi/helper";
import { withRouter } from "../../../RouterUtil";
import Cookies from "js-cookie";

class Content extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentRowsPerPage: 10,
      currentPage: 1,
      sortBy: 0,
      sortDir: "asc",

      loading: false,
      totalData: 0,
      header: null,
      data: [],
      dataReference: [],
      formRepos: [],
    };
  }

  onChangeRowsPerPage = (currentRowsPerPage, currentPage) => {
    this.setState({
      currentRowsPerPage,
      currentPage,
    });
  };

  onChangePage = (currentPage, totalRows) => {
    this.setState({
      currentPage: currentPage,
    });
  };

  onSort = ({ id }, direction, rows) => {
    this.setState({
      sortBy: id,
      sortDir: direction,
    });
  };

  fetch = async () => {
    const { id } = this.props.params || {};

    try {
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info",
        allowOutsideClick: false,
        didOpen: () => Swal.showLoading(),
      });

      let [{ header, data, referenceId, formRepos }, dataStatusAktif] =
        await Promise.all([fetchDetailReference({ id }), fetchStatusAktif()]);

      let dataReference = [];
      if (referenceId) {
        let { data: dataR = [] } = await fetchDetailReference({
          id: referenceId,
        });
        dataReference = dataR;

        let gData = {};
        (data || []).forEach((r) => {
          if (!gData[r?.relasi_id]) gData[r?.relasi_id] = [];
          gData[r?.relasi_id] = [...gData[r?.relasi_id], r];
        });

        let gAData = [];
        Object.keys(gData).forEach((relasi_id) => {
          gAData = [...gAData, { relasi_id, data: gData[relasi_id] }];
        });

        data = gAData;
      }

      const [{ label }] = dataStatusAktif.filter(
        ({ value }) => value == header?.status,
      );
      header.status = label;

      this.setState({
        header,
        data,
        dataStatusAktif,
        dataReference,
        formRepos,
      });

      Swal.close();
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  remove = async (id) => {
    try {
      const { isConfirmed = false } = await Swal.fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      });

      if (isConfirmed) {
        Swal.fire({
          title: "Mohon Tunggu!",
          icon: "info",
          allowOutsideClick: false,
          didOpen: () => Swal.showLoading(),
        });

        const message = await deleteReference({
          id,
          user_by: parseInt(Cookies.get("user_id")),
        });

        Swal.close();

        await Swal.fire({
          title: message || "Reference berhasil dihapus",
          icon: "success",
          confirmButtonText: "Ok",
        });

        this.back();
      }

      // throw new Error("API delete belum tersedia");
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  back = () => {
    window.location.href = "/site-management/reference";
  };

  componentDidMount() {
    this.fetch();
  }

  render() {
    const { id } = this.props.params || {};

    let columns = [
      {
        name: "No",
        width: "70px",
        center: true,
        cell: ({ idx }) => idx + 1,
      },
      {
        name: "Value",
        selector: ({ label, value, relasi_id }) => {
          if ((this?.state?.dataReference || []).length > 0 && relasi_id) {
            const { label: refLabel } =
              this?.state?.dataReference.find(
                ({ value: refValue }) => relasi_id == refValue,
              ) || {};
            label = refLabel;
          }

          return <div className="mt-2">{label}</div>;
        },
      },
    ];

    let columnsRepo = [
      {
        name: "No",
        width: "70px",
        center: true,
        cell: ({ idx }) => idx + 1,
      },
      {
        name: "Nama Form Element",
        selector: ({ judul_form }) => <div className="mt-2">{judul_form}</div>,
      },
    ];

    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Data Reference
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={() => this.back()}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500">
                    <i className="fa fa-chevron-left pe-1"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>

                <a
                  href={`/site-management/reference/edit/${id}`}
                  className="btn btn-warning fw-bolder btn-sm me-2"
                >
                  <i className="bi bi-gear-fill"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Edit
                  </span>
                </a>

                <button
                  className={`btn btn-sm btn-danger btn-active-light-info ${
                    this.state.formRepos.length > 0 ? "disabled" : ""
                  }`}
                  onClick={(e) => this.remove(id)}
                >
                  <i className="bi bi-trash-fill text-white pe-1"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Detail Data Reference
                          </h1>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="mb-7">
                          <div className="d-flex flex-wrap py-3">
                            <div className="flex-grow-1 mb-3">
                              <div className="d-flex justify-content-between align-items-start flex-wrap">
                                <div className="d-flex flex-column">
                                  <div className="d-flex align-items-center mb-0">
                                    <h5 className="fw-bolder mb-0 fs-5">
                                      {this.state.header?.name}
                                    </h5>
                                  </div>
                                  <div className="d-flex flex-wrap fw-bold fs-6 pe-2">
                                    <span
                                      className={
                                        "badge badge-light-" +
                                        (this.state.header?.status.toLowerCase() ==
                                        "aktif"
                                          ? "success"
                                          : "danger") +
                                        " fs-7"
                                      }
                                    >
                                      {capitalizeFirstLetter(
                                        this.state.header?.status,
                                      )}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div className="d-flex border-bottom p-0">
                          <ul
                            className="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap"
                            style={{
                              overflowX: "auto",
                              overflowY: "hidden",
                              display: "flex",
                              whiteSpace: "nowrap",
                              marginBottom: 0,
                            }}
                          >
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary active"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_1"
                              >
                                Informasi
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary false"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_2"
                              >
                                Penggunaan
                              </a>
                            </li>
                          </ul>
                        </div>

                        <div className="tab-content" id="detail-account-tab">
                          {/* tab 1 */}
                          <div
                            className="tab-pane fade show active"
                            id="kt_tab_pane_1"
                            role="tabpanel"
                          >
                            <div className="mt-7">
                              <div className="row pt-7">
                                <label className="form-label">Value</label>
                                <div className="table-responsive">
                                  <DataTable
                                    columns={columns}
                                    data={this?.state?.data?.map((s, idx) => ({
                                      idx,
                                      ...s,
                                    }))}
                                    highlightOnHover
                                    pointerOnHover
                                    pagination
                                    // paginationTotalRows={state.soals.length}
                                    // selectableRows
                                    // selectableRowSelected={({ id = null } = {}) => state.selectedRows.map(({ id } = {}) => id).includes(id)}
                                    // onSelectedRowsChange={({ selectedRows }) => state.setSelectedRows(selectedRows)}
                                    customStyles={{
                                      headCells: {
                                        style: {
                                          background: "rgb(243, 246, 249)",
                                        },
                                      },
                                    }}
                                    noDataComponent={
                                      <div className="mt-5">Tidak Ada Data</div>
                                    }
                                    persistTableHead={true}
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div
                            className="tab-pane fade"
                            id="kt_tab_pane_2"
                            role="tabpanel"
                          >
                            <div className="mt-7">
                              <div className="row pt-7">
                                <div className="col-lg-12">
                                  <div className="table-responsive">
                                    <DataTable
                                      columns={columnsRepo}
                                      data={this?.state?.formRepos?.map(
                                        (s, idx) => ({ idx, ...s }),
                                      )}
                                      highlightOnHover
                                      pointerOnHover
                                      pagination
                                      // paginationTotalRows={state.soals.length}
                                      // selectableRows
                                      // selectableRowSelected={({ id = null } = {}) => state.selectedRows.map(({ id } = {}) => id).includes(id)}
                                      // onSelectedRowsChange={({ selectedRows }) => state.setSelectedRows(selectedRows)}
                                      customStyles={{
                                        headCells: {
                                          style: {
                                            background: "rgb(243, 246, 249)",
                                          },
                                        },
                                      }}
                                      noDataComponent={
                                        <div className="mt-5">
                                          Tidak Ada Data
                                        </div>
                                      }
                                      persistTableHead={true}
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const ViewZonasi = (props) => {
  return (
    <div>
      <SideNav />
      <Header />
      <Content {...props} />
      <Footer />
    </div>
  );
};

export default withRouter(ViewZonasi);
