import React from "react";
import Header from "../../../Header";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";
import Select from "react-select";
import DataTable from "react-data-table-component";
import {
  fetchDetailReference,
  fetchListKotaKab,
  fetchListProvinsi,
  fetchReferences,
  fetchStatusAktif,
  fetchZonasi,
  hasError,
  insertReferenceNoRelation,
  insertReferenceWithRelation,
  resetErrorReferenceNoRelation,
  resetErrorReferenceWithRelation,
  updateReferenceNoRelation,
  updateReferenceWithRelation,
  validateReferenceNoRelation,
  validateReferenceWithRelation,
} from "../actions";
import Swal from "sweetalert2";
import { capitalizeFirstLetter } from "../../../publikasi/helper";
import { withRouter } from "../../../RouterUtil";
import AddEditForm from "../add/AddEditForm";
import Cookies from "js-cookie";

class Content extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      currentRowsPerPage: 10,
      currentPage: 1,
      sortBy: 0,
      sortDir: "asc",

      dataReferences: [],
      dataStatusAktif: [],
      dataProvinsi: [],

      loading: false,
      selectedProvinsis: [],

      name: "",
      nameError: null,
      status: null,
      statusError: null,
      selectedReference: null,
      selectedReferenceError: null,
      selectedReferenceValues: [],
      selectedReferenceLoading: false,
      values: [{}],
      originalValues: [{}],
      valuesError: [null],
      relations: [{}],
      relationsValues: [[]],
    };
  }

  onChange = (k, v, cb) => this.setState({ [k]: v }, cb);

  fetch = async () => {
    const { id } = this.props.params || {};

    try {
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info",
        allowOutsideClick: false,
        didOpen: () => Swal.showLoading(),
      });

      let [
        { header, data = [], referenceId, formRepos },
        dataStatusAktif = [],
        { data: dataReferences = [] },
      ] = await Promise.all([
        fetchDetailReference({ id }),
        fetchStatusAktif(),
        fetchReferences({
          start: 0,
          length: 1000,
          search: "",
          orderBy: "id",
          orderDir: "desc",
        }),
      ]);

      this.setState(
        {
          name: header?.name,
          status: header?.status,
          formRepos,
          dataStatusAktif,
          dataReferences: dataReferences.map(({ id, name }) => ({
            value: id,
            label: name,
          })),
          selectedReference: referenceId,
        },
        async () => {
          if (this?.state?.selectedReference) {
            this?.onChange("selectedReferenceLoading", true);
            this?.onChange("selectedReferenceValues", []);
            const { data: dataReference = [] } = await fetchDetailReference({
              id: this?.state?.selectedReference,
            });
            this?.onChange("values", [{}]);
            this?.onChange("relations", [{}]);
            this?.onChange("relationsValues", [[null]]);
            this?.onChange("selectedReferenceValues", dataReference);
            this?.onChange("selectedReferenceLoading", false);

            let gData = {};
            (data || []).forEach((r) => {
              if (!gData[r?.relasi_id]) gData[r?.relasi_id] = [];
              gData[r?.relasi_id] = [...gData[r?.relasi_id], r];
            });

            let gAData = [];
            Object.keys(gData).forEach((relasi_id) => {
              gAData = [...gAData, { relasi_id, data: gData[relasi_id] }];
            });

            data = gAData;

            let relations = [];
            let relationsValues = [];
            data.forEach(
              (
                { value, label: _relationValues, relasi_id, data = [] },
                idx,
              ) => {
                const { label: refLabel } = dataReference.find(
                  ({ value: refValue }) => relasi_id == refValue,
                );

                relations = [
                  ...relations,
                  { value: relasi_id, label: refLabel },
                ];
                relationsValues[idx] = data.map(({ label }) => label);
              },
            );

            this?.onChange("relationsValues", relationsValues);
            this?.onChange("relations", relations);
          } else {
            this?.onChange("values", data);
            this?.onChange("originalValues", [...data]);
          }
        },
      );

      Swal.close();
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  saveReferenceNoRelation = async () => {
    const { id } = this.props.params || {};

    try {
      this.setState({ isLoading: true });
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info",
        allowOutsideClick: false,
        didOpen: () => Swal.showLoading(),
      });

      console.log(
        "this.state?.originalValues",
        this.state?.originalValues,
        this.state?.values,
      );

      let values = [];
      this.state?.values?.forEach(({ value = null, label }, idx) => {
        values = [
          ...values,
          {
            id_value: value,
            nama_value: label,
            status: value ? "update" : "insert",
          },
        ];
      });

      this.state?.originalValues
        .filter(
          ({ value }) =>
            !this.state?.values?.map(({ value }) => value).includes(value),
        )
        .forEach(({ value = null, label }, idx) => {
          values = [
            ...values,
            {
              id_value: value,
              nama_value: label,
              status: "delete",
            },
          ];
        });

      const payload = {
        id,
        name: this.state.name,
        status: this.state.status,
        user_by: parseInt(Cookies.get("user_id")),
        value: JSON.stringify(values),
      };

      const message = await updateReferenceNoRelation(payload);

      Swal.close();
      this.setState({ isLoading: false });
      await Swal.fire({
        title: message || "Satuan kerja berhasil diupdate",
        icon: "success",
        confirmButtonText: "Ok",
      });

      window.location.href = "/site-management/reference";
    } catch (err) {
      this.setState({ isLoading: false });
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  saveReferenceWithRelation = async () => {
    const { id } = this.props.params || {};

    try {
      this.setState({ isLoading: true });
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info",
        allowOutsideClick: false,
        didOpen: () => Swal.showLoading(),
      });

      const payload = {
        id,
        name: this.state.name,
        status: this.state.status,
        user_by: parseInt(Cookies.get("user_id")),
        reference_id: parseInt(this?.state?.selectedReference),
        value: JSON.stringify(
          this?.state?.relations?.map((rel, idx) => ({
            data_references_id: parseInt(this?.state?.selectedReference),
            relasi_id: rel?.value,
            value: this?.state?.relationsValues[idx]?.map((value) => ({
              value,
            })),
          })),
        ),
      };

      const message = await updateReferenceWithRelation(payload);

      Swal.close();
      this.setState({ isLoading: false });
      await Swal.fire({
        title: message || "Referensi berhasil disimpan",
        icon: "success",
        confirmButtonText: "Ok",
      });

      window.location.href = "/site-management/reference";
    } catch (err) {
      this.setState({ isLoading: false });
      Swal.close();
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  checkAndSave = () => {
    if (!this?.state?.selectedReference) {
      const errors = validateReferenceNoRelation(this.state);
      if (!hasError(errors)) {
        this.saveReferenceNoRelation();
      } else {
        this.setState({ ...resetErrorReferenceNoRelation(), ...errors });

        Swal.fire({
          title: "Masih terdapat isian yang belum lengkap",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      }
    } else {
      const errors = validateReferenceWithRelation(this.state);
      if (!hasError(errors)) {
        this.saveReferenceWithRelation();
      } else {
        this.setState({ ...resetErrorReferenceWithRelation(), ...errors });

        Swal.fire({
          title: "Masih terdapat isian yang belum lengkap",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      }
    }
  };

  componentDidMount() {
    this.fetch();
  }

  render() {
    return (
      <AddEditForm
        label={"Edit Reference"}
        {...this.props}
        {...this.state}
        onChange={this?.onChange}
        onSave={() => this.checkAndSave()}
      />
    );
  }
}

const ViewZonasi = (props) => {
  return (
    <div>
      <SideNav />
      <Header />
      <Content {...props} />
      <Footer />
    </div>
  );
};

export default withRouter(ViewZonasi);
