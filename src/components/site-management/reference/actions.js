import axios from "axios";
import Cookies from "js-cookie";
import Swal from "sweetalert2";

const configs = {
  headers: {
    Authorization: "Bearer " + Cookies.get("token"),
  },
};

export const resetErrorReferenceNoRelation = () => {
  return {
    nameError: null,
    statusError: null,
    valuesError: null,
  };
};

export const validateReferenceNoRelation = ({
  name,
  status,
  selectedReference,
  values = [],
}) => {
  let error = {};
  let valuesError = [];

  if (!name) error["nameError"] = "Nama referensi tidak boleh kosong";
  if (status == null)
    error["statusError"] = "Status referensi tidak boleh kosong";
  values.forEach(({ label }, idx) => {
    if (!label) valuesError[idx] = `Value ${idx + 1} tidak boleh kosong`;
  });
  error["valuesError"] = valuesError;

  return error;
};

export const resetErrorReferenceWithRelation = () => {
  return {
    nameError: null,
    statusError: null,
    valuesError: null,
  };
};

export const validateReferenceWithRelation = ({
  name,
  status,
  selectedReference,
  dataReferences = [],
  relations = [],
  relationsValues = [[]],
}) => {
  const [{ label: relationLabel = "" }] = dataReferences.filter(
    (s) => s.value == selectedReference,
  );
  let error = {};
  let relationsError = [];
  let relationsValuesError = [];

  if (!name) error["nameError"] = "Nama referensi tidak boleh kosong";
  if (status == null)
    error["statusError"] = "Status referensi tidak boleh kosong";

  relations.forEach(({ label }, idx) => {
    if (!label) relationsError[idx] = `Value ${idx + 1} tidak boleh kosong`;
  });
  error["relationsError"] = relationsError;

  // relationsValues.forEach((values, idx) => {
  //   values.forEach((value, idx2) => {
  //     relationsValuesError[idx] = relationsValuesError[idx] || [];
  //     if (value == null) relationsValuesError[idx][idx2] = `Value ${relationLabel} tidak boleh kosong`;
  //   })
  // })
  // error['relationsValuesError'] = relationsValuesError;

  return error;
};

export const hasError = (error = {}) => {
  let _hasError = false;
  Object.keys(error).forEach((k) => {
    if (!error[k]) {
    } else if (Array.isArray(error[k]) || typeof error[k] == "object") {
      _hasError = _hasError || hasError(error[k]);
    } else {
      _hasError = _hasError || true;
    }
  });
  return _hasError;
};

const checkToken = () => {
  return new Promise(async (resolve, reject) => {
    if (Cookies.get("token") == null) {
      const { isConfirmed } = await Swal.fire({
        title: "Unauthenticated.",
        text: "Silahkan login ulang",
        icon: "warning",
        confirmButtonText: "Ok",
      });

      if (isConfirmed) return reject();
    }

    resolve();
  });
};

export const fetchReferences = async ({
  start,
  length,
  search = "",
  orderBy = "name",
  orderDir = "desc",
}) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/listreferensi",
            {
              mulai: start,
              limit: length,
              cari: search,
              sort: `${orderBy} ${orderDir}`,
            },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let {
              TotalLength,
              Data = [],
              Status = false,
              Message = "",
            } = result || {};
            [TotalLength] = TotalLength || [];
            TotalLength = TotalLength?.jml_data;

            if (Status)
              resolve({
                total: TotalLength,
                data: Data.map(
                  ({ id, nama_referensi, status_aktif, jml_form_repo }) => ({
                    id,
                    name: nama_referensi,
                    status: status_aktif,
                    jml_form_repo,
                  }),
                ),
              });
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchDetailReference = async ({
  id,
  mulai = 0,
  limit = 10000000,
  cari = "",
  sort = "id desc",
}) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/detailreferensi",
            { id, mulai, limit, cari, sort },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let {
              Judul,
              TotalData,
              Data = [],
              ID_Relasi,
              Target_Form_Repo = [],
              Status = false,
              Message = "",
            } = result || {};

            // if (Status) resolve(Data.map(({ id_value, nilai_referensi }) => ({ id: id_value, value: nilai_referensi })));
            if (Status)
              resolve({
                header: {
                  id,
                  name: Judul,
                  status: 1,
                  total: TotalData || Data.length || 10000000,
                },
                data: Data.map(({ id_value, nilai_referensi, relasi_id }) => ({
                  value: id_value,
                  label: nilai_referensi,
                  relasi_id,
                })),
                referenceId: ID_Relasi,
                formRepos: Target_Form_Repo,
              });
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchStatusAktif = async () => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        resolve([
          { value: 1, label: "Aktif" },
          { value: 0, label: "Tidak Aktif" },
        ]);
      })
      .catch(reject);
  });
};

export const fetchListProvinsi = async () => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(process.env.REACT_APP_BASE_API_URI + "/provinsi", {}, configs)
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            Data = Data.map(({ id, name, meta }) => ({
              value: id,
              label: name,
            }));

            if (Status) resolve(Data);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchListKotaKab = async ({ prov_id }) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/kabupaten",
            { kdprop: prov_id },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            Data = Data.map(({ id, name, meta }) => ({
              value: id,
              label: name,
            }));

            if (Status) resolve(Data);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const insertReferenceNoRelation = async (payload) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/add_referensi",
            payload,
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Message);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const insertReferenceWithRelation = async (payload) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/add_referensi_relasi",
            payload,
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Message);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const updateReferenceNoRelation = async (payload) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/edit_referensi",
            payload,
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Message);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const updateReferenceWithRelation = async (payload) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/edit_referensi_relasi",
            payload,
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Message);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const deleteReference = async (payload) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/delete_referensi",
            payload,
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Message);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};
