import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import Select from "react-select";
import {
  capitalizeFirstLetter,
  capitalizeTheFirstLetterOfEachWord,
} from "../../publikasi/helper";
import moment from "moment";

export default class UserAdminView extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickDelete = this.handleClickDeleteAction.bind(this);
    this.handleSort = this.handleSortAction.bind(this);
    this.handleChangeAkademi = this.handleChangeAkademiAction.bind(this);
    this.handleChangePelatihan = this.handleChangePelatihanAction.bind(this);
    this.handleChangeStatus = this.handleChangeStatusAction.bind(this);
    this.handleChangeRole = this.handleChangeRoleAction.bind(this);
    this.handleChangeSatuanKerja =
      this.handleChangeSatuanKerjaAction.bind(this);
    this.handleChangeSearch = this.handleChangeSearchAction.bind(this);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleClickFilter = this.handleClickFilterAction.bind(this);
    this.handleClickReset = this.handleClickResetAction.bind(this);
  }
  state = {
    datax: [],
    loading: true,
    datax_akademi: [],
    datax_pelatihan: [],
    totalRows: 0,
    newPerPage: 10,
    valAkademi: [],
    valPelatihan: [],
    akademi_id: "",
    pelatihan_id: "",
    tempLastNumber: 0,
    currentPage: 0,
    valStatus: [],
    valRoleAdmin: [],
    valSatuanKerja: [],
    column: "id",
    sortDirection: "desc",
    searchText: "",
    sort_by: "user_id",
    sort_val: "DESC",
    status: "",
    roleAdmin: [],
    satuanKerja: [],
    role_id: "",
    satuan_kerja_id: "",
    status_id: "",
    show_akademi: true,
    init: true,
    from_pagination_change: "",
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  status = [
    { value: "", label: "Semua" },
    { value: 1, label: "Aktif" },
    { value: 0, label: "Tidak Aktif" },
  ];

  columns = [
    {
      name: "No",
      cell: (row, index) => this.state.tempLastNumber + index + 1,
      width: "70px",
      center: true,
    },
    {
      name: "Nama Lengkap",
      className: "min-w-300px mw-300px",
      width: "300px",
      sortable: true,
      grow: 6,
      wrap: true,
      allowOverflow: false,
      accessor: "",
      selector: (row) => this.capitalWord(row.nama_akademi),
      cell: (row) => (
        <span className="d-flex flex-column my-2">
          <a
            href={"/site-management/administrator/preview/" + row.user_id}
            className="text-dark"
          >
            <span
              className="fw-bolder fs-7"
              style={{
                overflow: "hidden",
                whiteSpace: "wrap",
                textOverflow: "ellipses",
              }}
            >
              {capitalizeTheFirstLetterOfEachWord(row.name)}
            </span>
          </a>
        </span>
      ),
    },
    {
      name: "Email",
      sortable: true,
      wrap: true,
      width: "250px",
      allowOverflow: false,
      selector: (row) => row.email,
      cell: (row, index) => {
        return (
          <label className="d-flex flex-stack mb- mt-1">
            <span className="d-flex align-items-center me-2">
              <span className="d-flex flex-column">
                <span className="mb-0 fs-7">
                  <p className="mb-0">{row.email}</p>
                </span>
              </span>
            </span>
          </label>
        );
      },
    },
    {
      name: "Satker",
      center: false,
      sortable: false,
      wrap: true,
      width: "250px",
      allowOverflow: false,
      cell: (row) => {
        if (row.unit_work) {
          const roles = JSON.parse(row.unit_work);
          const print_role = [];
          const count_roles = roles.length;
          const max_roles = 5;
          roles.forEach(function (element, i) {
            if (i < max_roles) {
              let name = element.name;
              if (name.length > 15) {
                name = name.slice(0, 15) + "...";
              }
              print_role.push(
                '<div title="' +
                  element.name +
                  '" class="fs-7 mt-2 mb-2 ms-2 me-2">' +
                  name +
                  "</div>",
              );
            }
          });
          if (count_roles > max_roles) {
            const count_sisa = count_roles - max_roles;
            print_role.push(
              '<a href="/site-management/administrator/preview/' +
                row.user_id +
                '" class="fs-7 mt-2 mb-2 ms-2 me-2">' +
                count_sisa +
                " more satker...</a>",
            );
          }
          const join = print_role.join("");
          return (
            <div
              className="text-center"
              dangerouslySetInnerHTML={{ __html: join }}
            />
          );
        } else {
          return "-";
        }
      },
    },
    {
      name: "Role",
      center: false,
      sortable: false,
      width: "200px",
      //className: "min-w-100px",
      cell: (row) => {
        if (row.role_name) {
          const roles = JSON.parse(row.role_name);
          const print_role = [];
          const count_roles = roles.length;
          const max_roles = 5;
          roles.forEach(function (element, i) {
            if (i < max_roles) {
              let name = element.name;
              if (name.length > 15) {
                name = name.slice(0, 15) + "...";
              }
              print_role.push(
                '<div title="' +
                  element.name +
                  '" class="badge badge-light-primary mt-2 mb-2 ms-2 me-2">' +
                  name +
                  "</div>",
              );
            }
          });
          if (count_roles > max_roles) {
            const count_sisa = count_roles - max_roles;
            print_role.push(
              '<a href="/site-management/administrator/preview/' +
                row.user_id +
                '" class="badge badge-primary mt-2 mb-2 ms-2 me-2">' +
                count_sisa +
                " more roles...</a>",
            );
          }
          const join = print_role.join("");
          return (
            <div className="" dangerouslySetInnerHTML={{ __html: join }} />
          );
        } else {
          return "-";
        }
      },
    },
    {
      name: "Status",
      center: false,
      sortable: true,
      cell: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.is_active ? "success" : "danger") +
              " fs-7 m-1"
            }
          >
            {row.is_active == 1 ? "Aktif" : "Tidak Aktif"}
          </span>
        </div>
      ),
    },
    {
      name: "Last Login",
      sortable: true,
      wrap: true,
      width: "200px",
      allowOverflow: false,
      selector: (row) => row.last_login,
      cell: (row, index) => {
        const d = moment(row.last_login).locale("id");
        const last_login = d.isValid() ? d.format("D MMM YYYY, HH:mm:ss") : "-";
        return (
          <label className="d-flex flex-stack mb- mt-1">
            <span className="d-flex align-items-center me-2">
              <span className="d-flex flex-column">
                <span className="mb-0 fs-7">
                  <p className="mb-0">{last_login}</p>
                </span>
              </span>
            </span>
          </label>
        );
      },
    },
    {
      name: "Aksi",
      center: false,
      width: "150px",
      cell: (row) => (
        <div>
          <a
            href={"/site-management/administrator/preview/" + row.user_id}
            title="Detail"
            className="btn btn-icon btn-bg-primary btn-sm me-1"
          >
            <i className="bi bi-eye-fill text-white"></i>
          </a>
          <a
            href={"/site-management/administrator/edit/" + row.user_id}
            id={row.user_id}
            title="Edit"
            className="btn btn-icon btn-bg-warning btn-sm me-1"
            alt="Edit"
          >
            <i className="bi bi-gear-fill text-white"></i>
          </a>
          <a
            href="#"
            id={row.user_id}
            onClick={this.handleClickDelete}
            title="Status Aktif"
            className="btn btn-icon btn-bg-danger btn-sm me-1"
          >
            <i className="bi bi-trash-fill text-white"></i>
          </a>
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    } else {
      this.handleReload();

      const dataAkademik = { start: 0, length: 200, status: "publish" };
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/akademi/list-akademi",
          dataAkademik,
          this.configs,
        )
        .then((res) => {
          const optionx = res.data.result.Data;
          const datax_akademi = [];
          datax_akademi.push({ value: 0, label: "Semua" });
          optionx.map((data) =>
            datax_akademi.push({ value: data.id, label: data.name }),
          );
          this.setState({ datax_akademi });
        });

      /* //pelatihan
            const dataBodyPelatihan = {
                id_akademi: 0,
                id_penyelenggara: 0,
                id_silabus: 0,
                id_tema: 0,
                limit: 1000,
                mulai: 0,
                param: '',
                provinsi: 0,
                sort: this.state.sortBy,
                sortVal: this.state.sortDirection.toUpperCase(),
                status_pelatihan: 0,
                status_publish: 1,
                status_substansi: 0,
                tahun: 2022,
            }

            axios.post(process.env.REACT_APP_BASE_API_URI + '/pelatihan', dataBodyPelatihan, this.configs)
                .then(res => {
                    const optionx = res.data.result.Data;
                    const datax_pelatihan = [];
                    datax_pelatihan.push({ "value": 0, "label": 'Semua' });
                    optionx.map(data =>
                        datax_pelatihan.push({ "value": data.id, "label": data.pelatihan })
                    )
                    this.setState({ datax_pelatihan });
                }); */

      let dataBody = {
        mulai: 0,
        limit: 100,
        cari: "",
        sort: "id DESC",
      };

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/list-role-v2",
          dataBody,
          this.configs,
        )
        .then((res) => {
          const optionx = res.data.result.Data;
          const roleAdmin = [];
          roleAdmin.push({ value: 0, label: "Semua" });
          optionx.map((data) =>
            roleAdmin.push({ value: data.id, label: data.nama }),
          );
          this.setState({ roleAdmin });
        });

      //penyelenggara
      const dataSatker = {
        mulai: 0,
        limit: 200,
        cari: "",
        sort: "id asc",
      };

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/list_satker",
          dataSatker,
          this.configs,
        )
        .then((res) => {
          swal.close();
          const datax_satker = res.data.result.Data;
          const satuanKerja = [];
          satuanKerja.push({ value: 0, label: "Semua" });
          datax_satker.map((data) =>
            satuanKerja.push({ value: data.id, label: data.name }),
          );
          this.setState({
            satuanKerja,
          });
        });
    }
  }

  handleClickResetAction() {
    this.setState({ valStatusFilter: [] });
    this.setState(
      {
        statusPublish: "",
      },
      () => {
        this.handleReload();
      },
    );
  }

  handleClickDeleteAction(e) {
    const idx = e.currentTarget.id;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          //const data = { id_role: idx }
          const dataFormPost = new FormData();
          dataFormPost.append("id", idx);
          dataFormPost.append("deleted_by", Cookies.get("user_id"));

          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/profile/hapus-user-admin",
              dataFormPost,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload(
                        this.state.currentPage,
                        this.state.newPerPage,
                      );
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmationButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload();
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload();
                    }
                  });
              }
            });
        }
      });
  }

  handleReload(page, newPerPage) {
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });
    const dataBody = {
      start: start_tmp,
      length: length_tmp,
      id_academy: this.state.akademi_id,
      id_training: this.state.pelatihan_id,
      role_id: this.state.role_id,
      id_unit_work: this.state.satuan_kerja_id,
      status: this.state.status_id,
      search: this.state.searchText,
      sort_by: this.state.sort_by,
      sort_val: this.state.sort_val.toUpperCase(),
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/filter-user-admin",
        dataBody,
        this.configs,
      )
      .then((res) => {
        this.setState({ loading: false });
        const datax = res.data.result.Data;
        const statusx = res.data.result.Status;
        let messagex = res.data.result.Message;
        if (statusx) {
          this.setState({ datax });
          this.setState({ loading: false });
          this.setState({ totalRows: res.data.result.Jumlahdata });
          this.setState({ currentPage: page });
        } else {
          this.setState({ datax: [] });
          //overwrite dulu sambil nunggu BE
          if (messagex == "Daftar Peserta Digitalent Tidak Di Temukan!") {
            messagex = "User Administrator Tidak Di Temukan!";
          } else if (messagex == "Undefined array key 0") {
            messagex = "User Administrator Tidak Di Temukan!";
          }
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ loading: false });
              }
            });
        }
      })
      .catch((error) => {
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        this.setState({ datax: [] });
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ loading: false });
              }
            });
        }
      });
  }

  handlePageChange = (page) => {
    if (this.state.from_pagination_change == 0) {
      this.setState({ loading: true });
      this.handleReload(page, this.state.newPerPage);
    }
  };
  handlePerRowsChange = async (newPerPage, page) => {
    if (this.state.from_pagination_change == 1) {
      this.setState({ loading: true });
      this.setState({ newPerPage: newPerPage }, () => {
        this.handleReload(page, this.state.newPerPage);
      });
    }
  };

  handleSortAction(column, sortDirection) {
    let server_name = "";
    if (column.name == "Nama Lengkap") {
      server_name = "name";
    } else if (column.name == "NIK") {
      server_name = "nik";
    } else if (column.name == "Email") {
      server_name = "email";
    } else if (column.name == "Status") {
      server_name = "is_active";
    } else if (column.name == "Last Login") {
      server_name = "last_login";
    }

    this.setState(
      {
        sort_by: server_name,
        sort_val: sortDirection,
      },
      () => {
        this.handleReload(1, this.state.newPerPage);
      },
    );
  }

  handleChangeSearchAction(e) {
    const searchText = e.currentTarget.value;
    if (searchText == "") {
      this.setState(
        {
          loading: true,
          searchText: "",
        },
        () => {
          this.handleReload(1, this.state.newPerPage);
        },
      );
    }
  }

  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    if (e.key == "Enter") {
      if (searchText == "") {
        this.setState(
          {
            isSearch: false,
            searchText: "",
          },
          () => {
            this.handleReload(1, this.state.newPerPage);
          },
        );
      } else {
        this.setState({ loading: true });
        this.setState({ isSearch: true });
        this.setState({ searchText: searchText }, () => {
          this.handleReload(1, this.state.newPerPage);
        });
      }
    }
  }

  handleChangeAkademiAction = (selectedOption) => {
    this.setState(
      {
        akademi_id: selectedOption.value,
        valAkademi: {
          value: selectedOption.value,
          label: selectedOption.label,
        },
        pelatihan_id: "",
        valPelatihan: [],
        datax_pelatihan: [],
      },
      () => {
        swal.fire({
          title: "Mohon Tunggu!",
          icon: "info", // add html attribute if you want or remove
          allowOutsideClick: false,
          didOpen: () => {
            swal.showLoading();
          },
        });
        //pelatihan
        /* const dataBodyPelatihan = {
                id_akademi: selectedOption.value,
                id_penyelenggara: 0,
                id_silabus: 0,
                id_tema: 0,
                limit: 1000,
                mulai: 0,
                param: '',
                provinsi: 0,
                sort: this.state.sortBy,
                sortVal: this.state.sortDirection.toUpperCase(),
                status_pelatihan: 0,
                status_publish: 1,
                status_substansi: 0,
                tahun: 2022,
            } */
        const dataBody = [{ akademi_id: selectedOption.value }];

        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/useradmin/get_pelatihan_info_byjson",
            JSON.stringify(dataBody),
            this.configs,
          )
          .then((res) => {
            swal.close();
            const optionx = res.data.result.Data;
            const totalRowsPelatihan = res.data.result.JmlData;
            const dataxpelatihan = [];
            /* const dataxpelatihan = optionx.reduce((r, { tema: label, ...object }) => {
                        var temp = r.find(o => o.label === label);
                        if (!temp) r.push(temp = { label, options: [] });
                        temp.options.push({ value: object.id_peatihan, label: object.nama_pelatihan });
                        return r;
                    }, []); */
            optionx.map((data) =>
              dataxpelatihan.push({
                value: data.id_peatihan,
                label: data.nama_pelatihan,
              }),
            );
            console.log(dataxpelatihan);
            this.setState({
              dataxpelatihan,
              totalRowsPelatihan,
            });
          })
          .catch((error) => {
            swal.close();
            console.log(error);
            const dataxpelatihan = [];
            const totalRowsPelatihan = 0;
            this.setState({
              dataxpelatihan,
              totalRowsPelatihan,
            });
            let messagex = error.response.data.result.Message;
          });

        /* axios.post(process.env.REACT_APP_BASE_API_URI + '/pelatihan', dataBodyPelatihan, this.configs)
                .then(res => {
                    swal.close();
                    const optionx = res.data.result.Data;
                    const statusx = res.data.result.Status;
                    if (statusx) {
                        const datax_pelatihan = [];
                        datax_pelatihan.push({ "value": 0, "label": 'Semua' });
                        optionx.map(data =>
                            datax_pelatihan.push({ "value": data.id, "label": data.pelatihan })
                        )
                        this.setState({ datax_pelatihan });
                        if (datax_pelatihan.length == 0) {
                            swal.fire({
                                title: "Data Pelatihan Tidak Ditemukan",
                                icon: "warning",
                                confirmationButtonText: "Ok"
                            }).then((result) => {
                                if (result.isConfirmed) {

                                }
                            })
                        }
                    } else {
                        swal.fire({
                            title: "Data Pelatihan Tidak Ditemukan",
                            icon: "warning",
                            confirmationButtonText: "Ok"
                        }).then((result) => {
                            if (result.isConfirmed) {

                            }
                        })
                    }

                }).catch((error) => {
                    swal.fire({
                        title: "Data Pelatihan Tidak Ditemukan",
                        icon: "warning",
                        confirmationButtonText: "Ok"
                    }).then((result) => {
                        if (result.isConfirmed) {

                        }
                    })
                }); */
      },
    );
  };

  handleChangePelatihanAction = (selectedOption) => {
    console.log(selectedOption);
    this.setState({
      pelatihan_id: selectedOption.value,
      akademi_id: 0,
      valPelatihan: {
        value: selectedOption.value,
        label: selectedOption.label,
      },
    });
  };

  handleChangeStatusAction = (selectedOption) => {
    this.setState({
      status_id: selectedOption.value,
      valStatus: { value: selectedOption.value, label: selectedOption.label },
    });
  };

  handleChangeRoleAction = (selectedOption) => {
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    let akses_akademi = false;

    const dataBody = {
      mulai: 0,
      limit: 100,
      sort: "role_id asc",
      id_role: selectedOption.value,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/detail-role",
        dataBody,
        this.configs,
      )
      .then((res) => {
        swal.close();
        const detail = res.data.result.Detail;
        detail.forEach(function (elementDetail) {
          if (
            elementDetail.permissions_id == 200 &&
            (elementDetail.manage_role == 1 ||
              elementDetail.publish_role == 1 ||
              elementDetail.view_role == 1)
          ) {
            akses_akademi = true;
          }
        });
        this.setState({
          role_id: selectedOption.value,
          valRoleAdmin: {
            value: selectedOption.value,
            label: selectedOption.label,
          },
          show_akademi: akses_akademi,
        });
      });
  };

  handleChangeSatuanKerjaAction = (selectedOption) => {
    this.setState({
      satuan_kerja_id: selectedOption.value,
      valSatuanKerja: {
        value: selectedOption.value,
        label: selectedOption.label,
      },
    });
  };

  handleClickResetAction(e) {
    e.preventDefault();
    const akademi_id = "";
    const pelatihan_id = "";
    const role_id = "";
    const satuan_kerja_id = "";
    const status_id = "";
    const valAkademi = [];
    const valPelatihan = [];
    const valStatus = [];
    const valRoleAdmin = [];
    const valSatuanKerja = [];
    this.setState(
      {
        akademi_id,
        pelatihan_id,
        role_id,
        satuan_kerja_id,
        status_id,
        valAkademi,
        valPelatihan,
        valStatus,
        valRoleAdmin,
        valSatuanKerja,
      },
      () => {
        this.handleReload(1, this.state.newPerPage);
      },
    );
  }

  handleClickFilterAction(e) {
    e.preventDefault();
    this.handleReload(1, this.state.newPerPage);
  }

  render() {
    let rowCounter = 1;
    const styleImg = {
      width: "40px",
      height: "30px",
    };
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Administrator
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                  data-bs-toggle="modal"
                  data-bs-target="#filter"
                >
                  <i className="bi bi-sliders"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Filter
                  </span>
                </button>

                <a
                  href={"/site-management/administrator/tambah"}
                  className="btn btn-success fw-bolder btn-sm"
                >
                  <i className="bi bi-plus-circle"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Tambah Administrator
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Daftar Administrator
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <div className="d-flex align-items-center position-relative my-1 me-2">
                            <span className="svg-icon svg-icon-1 position-absolute ms-6">
                              <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className="mh-50px"
                              >
                                <rect
                                  opacity="0.5"
                                  x="17.0365"
                                  y="15.1223"
                                  width="8.15546"
                                  height="2"
                                  rx="1"
                                  transform="rotate(45 17.0365 15.1223)"
                                  fill="currentColor"
                                ></rect>
                                <path
                                  d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                  fill="currentColor"
                                ></path>
                              </svg>
                            </span>
                            <input
                              type="text"
                              data-kt-user-table-filter="search"
                              className="form-control form-control-sm form-control-solid w-250px ps-14"
                              placeholder="Cari Administrator"
                              onKeyPress={this.handleKeyPress}
                              onChange={this.handleChangeSearch}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="table-responsive">
                          <DataTable
                            columns={this.columns}
                            data={this.state.datax}
                            progressPending={this.state.loading}
                            highlightOnHover
                            pointerOnHover
                            pagination
                            paginationServer
                            paginationTotalRows={this.state.totalRows}
                            paginationDefaultPage={this.state.currentPage}
                            onChangeRowsPerPage={(
                              currentRowsPerPage,
                              currentPage,
                            ) => {
                              this.setState(
                                {
                                  from_pagination_change: 1,
                                },
                                () => {
                                  this.handlePerRowsChange(
                                    currentRowsPerPage,
                                    currentPage,
                                  );
                                },
                              );
                            }}
                            onChangePage={(page, totalRows) => {
                              //biar ga 2 x ke trigger
                              if (this.state.init) {
                                this.setState({
                                  init: false,
                                });
                              } else {
                                this.setState(
                                  {
                                    from_pagination_change: 0,
                                  },
                                  () => {
                                    this.handlePageChange(page, totalRows);
                                  },
                                );
                              }
                            }}
                            customStyles={this.customStyles}
                            persistTableHead={true}
                            onSort={this.handleSort}
                            noDataComponent={
                              <div className="mt-5">Tidak Ada Data</div>
                            }
                            sortServer
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Filter */}
        <div className="modal modal-lg fade" tabIndex="-1" id="filter">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">
                  <span className="svg-icon svg-icon-5 me-1">
                    <i className="bi bi-sliders text-black"></i>
                  </span>
                  Filter Data Administrator
                </h5>
                <div
                  className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <span className="svg-icon svg-icon-2x">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <rect
                        opacity="0.5"
                        x="6"
                        y="17.3137"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(-45 6 17.3137)"
                        fill="currentColor"
                      />
                      <rect
                        x="7.41422"
                        y="6"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(45 7.41422 6)"
                        fill="currentColor"
                      />
                    </svg>
                  </span>
                </div>
              </div>
              <form action="#" onSubmit={this.handleClickFilter}>
                <input
                  type="hidden"
                  name="csrf-token"
                  value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                />
                <div className="modal-body">
                  <div className="col-lg-12  fv-row form-group mb-7">
                    <label className="form-label">Status</label>
                    <Select
                      id="status"
                      name="status"
                      placeholder="Silahkan pilih Status"
                      noOptionsMessage={({ inputValue }) =>
                        !inputValue ? this.state.status : "Data tidak tersedia"
                      }
                      className="form-select-sm selectpicker p-0"
                      options={this.status}
                      onChange={this.handleChangeStatus}
                      value={this.state.valStatus}
                    />
                  </div>
                  <div className="col-lg-12  fv-row form-group mb-7">
                    <label className="form-label">Role Admin</label>
                    <Select
                      id="roleAdmin"
                      name="roleAdmin"
                      placeholder="Silahkan pilih Status"
                      noOptionsMessage={({ inputValue }) =>
                        !inputValue
                          ? this.state.roleAdmin
                          : "Data tidak tersedia"
                      }
                      className="form-select-sm selectpicker p-0"
                      options={this.state.roleAdmin}
                      onChange={this.handleChangeRole}
                      value={this.state.valRoleAdmin}
                    />
                  </div>

                  <div className="col-lg-12  fv-row form-group mb-7">
                    <label className="form-label">Satuan Kerja</label>
                    <Select
                      id="roleAdmin"
                      name="roleAdmin"
                      placeholder="Silahkan pilih Status"
                      noOptionsMessage={({ inputValue }) =>
                        !inputValue
                          ? this.state.valSatuanKerja
                          : "Data tidak tersedia"
                      }
                      className="form-select-sm selectpicker p-0"
                      options={this.state.satuanKerja}
                      onChange={this.handleChangeSatuanKerja}
                      value={this.state.valSatuanKerja}
                    />
                  </div>
                  {this.state.show_akademi ? (
                    <div className="col-lg-12  fv-row form-group mb-7 mt-10">
                      <h4>Akses Akademi/Pelatihan</h4>

                      <div className="mb-7 fv-row">
                        <label className="form-label">Akademi</label>
                        <Select
                          name="akademi_id"
                          placeholder="Silahkan pilih Akademi"
                          noOptionsMessage={({ inputValue }) =>
                            !inputValue
                              ? this.state.dataxakademi
                              : "Data tidak tersedia"
                          }
                          className="form-select-sm form-select-solid selectpicker p-0"
                          options={this.state.datax_akademi}
                          onChange={this.handleChangeAkademi}
                          value={this.state.valAkademi}
                        />
                      </div>

                      <div className="mb-7 fv-row">
                        <label className="form-label">Pelatihan</label>
                        <Select
                          name="pelatihan_id"
                          placeholder="Silahkan pilih Pelatihan"
                          noOptionsMessage={({ inputValue }) =>
                            !inputValue
                              ? this.state.dataxpelatihan
                              : "Data tidak tersedia"
                          }
                          className="form-select-sm form-select-solid selectpicker p-0"
                          options={this.state.dataxpelatihan}
                          onChange={this.handleChangePelatihan}
                          value={this.state.valPelatihan}
                        />
                      </div>
                    </div>
                  ) : (
                    <div></div>
                  )}
                </div>
                <div className="modal-footer">
                  <div className="d-flex justify-content-between">
                    <button
                      type="reset"
                      className="btn btn-sm btn-light me-3"
                      onClick={this.handleClickReset}
                    >
                      Reset
                    </button>
                    <button
                      type="submit"
                      className="btn btn-sm btn-primary"
                      data-bs-dismiss="modal"
                    >
                      Apply Filter
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
