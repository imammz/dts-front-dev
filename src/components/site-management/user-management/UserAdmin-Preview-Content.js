import React, { useMemo, useState } from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import {
  handleFormatDate,
  capitalWord,
} from "./../../pelatihan/Pelatihan/helper";
import moment from "moment";

const configTableRoleAkses = {
  columns: [
    {
      name: "Nama Role",
      // center: true,
      sortable: true,
      selector: (row) => row.role_name,
    },
    {
      name: "Dibuat",
      // center: true,
      sortable: true,
      selector: (row) => {
        const d = moment(row.created_at).locale("id");
        return d.isValid() ? d.format("D MMM YYYY, HH:mm:ss") : "-";
      },
    },
  ],
  data: [],
};

const configTableSakter = {
  columns: [
    {
      name: "Nama Akademi",
      // center: false,
      sortable: true,
      selector: (row) => row.unit_work_name,
    },
    {
      name: "Dibuat",
      // center: true,
      sortable: true,
      selector: (row) => {
        const d = moment(row.created_at).locale("id");
        return d.isValid() ? d.format("D MMM YYYY, HH:mm:ss") : "-";
      },
    },
  ],
  data: [],
};

const configTableAkademi = {
  columns: [
    {
      name: "Nama Akademi",
      // center: false,
      sortable: true,
      selector: (row) => row.nama_akademi,
    },
    {
      name: "Dibuat",
      sortable: true,
      selector: (row) => {
        const d = moment(row.created_at).locale("id");
        return d.isValid() ? d.format("D MMM YYYY, HH:mm:ss") : "-";
      },
    },
  ],
  data: [],
};

const configTablePelatihan = {
  columns: [
    {
      name: "Nama Pelatihan",
      // center: false,
      sortable: true,
      selector: (row) => row.nama_pelatihan,
    },
    {
      name: "Nama Akademi",
      // center: false,
      sortable: true,
      selector: (row) => row.nama_akademi,
    },
    {
      name: "Nama Tema",
      // center: false,
      sortable: true,
      selector: (row) => row.nama_tema,
    },
    {
      name: "Dibuat",
      // center: true,
      sortable: true,
      selector: (row) => {
        const d = moment(row.created_at).locale("id");
        return d.isValid() ? d.format("D MMM YYYY, HH:mm:ss") : "-";
      },
    },
  ],
  data: [],
};

const customLoader = () => (
  <div style={{ padding: "24px" }}>
    <img src="/assets/media/loader/loader-biru.gif" />
  </div>
);

const CustomList = ({ columns, data, mode = "table", chipPropName = "" }) => {
  // const [activeColumns,setActiveColumns] = useState()
  const [activePaging, setActivePaging] = useState({ page: 1, perPage: 10 });

  const activeColumns = useMemo(
    () => [
      {
        name: "No",
        center: true,
        width: "70px",
        cell: (row, index) => (
          <div>
            <span>
              {index + 1 + (activePaging.page - 1) * activePaging.perPage}
            </span>
          </div>
        ),
      },
      ...columns,
    ],
    [columns, activePaging],
  );

  const handlePageChange = (page) => {
    // console.log(page)
    setActivePaging((p) => ({ ...p, page: page }));
  };

  const handlePerRowChange = (perPage) => {
    // console.log(page)
    setActivePaging((p) => ({ ...p, perPage: perPage }));
  };

  return (
    <>
      {mode == "table" ? (
        <DataTable
          columns={activeColumns}
          data={data}
          progressComponent={customLoader}
          highlightOnHover
          pointerOnHover
          pagination
          onChangeRowsPerPage={handlePerRowChange}
          onChangePage={handlePageChange}
          defaultSortAsc={true}
          // persistTableHead={true}
          paginationPerPage={activePaging.perPage}
          // sortServer
          // paginationServer
          // paginationTotalRows={totalData}
          noDataComponent={<div className="mt-5">Tidak Ada Data</div>}
        ></DataTable>
      ) : mode == "chip" ? (
        <>
          {data?.length !== 0 ? (
            data.map((element, i) => (
              <span key={i} className="badge badge-secondary me-1 mb-1 ms-1">
                {element[chipPropName]}
              </span>
            ))
          ) : (
            <></>
          )}
        </>
      ) : (
        <></>
      )}
    </>
  );
};

export default class UserAdminPreviewContent extends React.Component {
  constructor(props) {
    super(props);
    this.back = this.back.bind(this);
    this.handleSort = this.handleSortAction.bind(this);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleDelete = this.handleDeleteAction.bind(this);
  }

  state = {
    datax: [],
    dataxpdp: [],
    user_id: null,
    data_user: [],
    data_role: [],
    data_satker: [],
    data_akademi: [],
    data_pelatihan: [],
    roles: [],
    satkers: [],
    pelatihans: [],
    akademis: [],
    loading: false,
    totalRows: 0,
    newPerPage: 10,
    tempLastNumber: 0,
    currentPage: 0,
    column: "id",
    sortDirection: "asc",
    isRowChangeRef: false,
    searchText: "",
    last_login: "-",
  };
  customLoader = (
    <div style={{ padding: "24px" }}>
      <img src="/assets/media/loader/loader-biru.gif" />
    </div>
  );
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };
  columns = [
    {
      name: "No",
      cell: (row, index) => this.state.tempLastNumber + index + 1,
      width: "70px",
      center: true,
    },
    {
      name: "Nama File",
      width: "300px",
      sortable: true,
      selector: (row) => row.filename,
    },
    {
      name: "Deskripsi",
      sortable: false,
      selector: (row) => capitalWord(row.jenis),
      width: "300px",
    },
    {
      name: "Created At",
      sortable: true,
      center: true,
      selector: (row) => (
        <span className="">
          {handleFormatDate(
            row.created_at,
            "DD MMMM YYYY, HH:mm:ss",
            "YYYY-MM-DD, HH:mm:ss",
          )}
        </span>
      ),
    },
    {
      name: "Aksi",
      center: true,
      width: "100px",
      cell: (row) => (
        <div>
          <a
            href="#"
            title="Daftar Peserta"
            className="btn btn-icon btn-bg-success btn-sm me-1"
            onClick={() => {
              this.downloadFilePdp(row.filename);
            }}
          >
            <i class="bi bi-file-earmark-check-fill text-white"></i>
          </a>
        </div>
      ),
    },
  ];
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  handlePerRowsChange = async (arg1, arg2, srcEvent) => {
    if (srcEvent == "page-change") {
      this.setState({ loading: true }, () => {
        if (!this.state.isRowChangeRef) {
          this.handleReload(arg1, this.state.newPerPage);
        }
      });
    } else if (srcEvent == "row-change") {
      this.setState({ isRowChangeRef: true }, () => {
        this.handleReload(arg2, arg1);
      });
      this.setState({ loading: true, newPerPage: arg1 }, () => {
        this.setState({ isRowChangeRef: false });
      });
    }
  };
  handleSortAction(column, sortDirection) {
    let server_name = "";
    if (column.name == "Nama File") {
      server_name = "filename";
    } else if (column.name == "Created At") {
      server_name = "created_at";
    }

    this.setState(
      {
        column: server_name,
        sortDirection: sortDirection,
      },
      () => {
        this.handleReload(1, this.state.newPerPage);
      },
    );
  }
  downloadFilePdp(filename) {
    axios
      .get(process.env.REACT_APP_BASE_API_URI + "/rekappendaftaran/unduh-pdp", {
        params: {
          filename: filename,
        },
        responseType: "blob",
      })
      .then((resp) => {
        // create file link in browser's memory
        const href = URL.createObjectURL(resp.data);

        // create "a" HTML element with href to file & click
        const link = document.createElement("a");
        link.href = href;
        link.setAttribute("download", filename); //or any other extension
        document.body.appendChild(link);
        link.click();

        // clean up "a" element & remove ObjectURL
        document.body.removeChild(link);
        URL.revokeObjectURL(href);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    if (e.key == "Enter") {
      if (searchText == "") {
        this.handleReload();
      } else {
        this.setState({ loading: true, searchText: searchText }, () => {
          this.handleReload(1, this.state.newPerPage);
        });
      }
    }
  }
  back() {
    window.history.back();
  }

  handleReload(page, newPerPage) {
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });
    const userViewId = window?.location?.pathname?.split("/")[4];
    const dataBody = {
      user_id: userViewId,
      mulai: start_tmp,
      limit: length_tmp,
      param: this.state.searchText,
      sort: this.state.column,
      sort_val: this.state.sortDirection.toUpperCase(),
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/rekappendaftaran/list-unduh-ttd",
        dataBody,
        this.configs,
      )
      .then((res) => {
        const status = res.data.result.Status;
        const messagex = res.data.result.Message;
        console.log(res.data.result);
        if (status) {
          const dataxpdp =
            res.data.result.Data && res.data.result.Data[0] == null
              ? []
              : res.data.result.result;
          this.setState({ dataxpdp });
          this.setState({ loading: false });
          this.setState({ totalRows: res.data.result.TotalLength });
          this.setState({ currentPage: page });
        } else {
          this.setState({ dataxpdp: [], loading: false });
          // swal
          //   .fire({
          //     title: messagex,
          //     icon: "warning",
          //     confirmButtonText: "Ok",
          //   })
          //   .then((result) => {
          //     if (result.isConfirmed) {
          //       // this.handleClickResetAction();
          //     }
          //   });
        }
      })
      .catch((error) => {
        console.log(error);
        let messagex = error.response?.data?.result?.Message ?? error.message;
        swal.fire({
          title: messagex ?? "Terjadi Kesalahan!",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      });
  }

  handleDeleteAction() {
    // const self=this
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          //const data = { id_role: idx }
          const dataFormPost = new FormData();
          dataFormPost.append("id", this.state.user_id);
          dataFormPost.append("deleted_by", Cookies.get("user_id"));

          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/profile/hapus-user-admin",
              dataFormPost,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.back();
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmationButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.back();
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.back();
                    }
                  });
              }
            });
        }
      });
  }

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    } else {
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });

      let segment_url = window.location.pathname.split("/");
      let user_id = segment_url[4];
      this.setState({ user_id: user_id });

      const data = { id: user_id };
      this.handleReload();
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/get-user-admin",
          data,
          this.configs,
        )
        .then((res) => {
          swal.close();
          const statusx = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statusx) {
            const data_user = res.data.result.DataUser[0];
            const data_role = res.data.result.DataRelasiUser;
            const data_satker = res.data.result.DataSatker;
            const data_akademi = res.data.result.DataAkademi;
            const data_pelatihan = res.data.result.DataPelatihan;
            const d = moment(data_user.last_login).locale("id");
            const last_login = d.isValid()
              ? d.format("D MMM YYYY, HH:mm:ss")
              : "-";

            const roles = [...data_role];

            // data_role.forEach(function (element, i) {
            //   const role = (
            //     <span key={i} className="badge badge-secondary me-1 ms-1 mb-1">
            //       {element.role_name}
            //     </span>
            //   );
            //   roles.push(role);
            // });

            const satkers = [...data_satker];

            // data_satker.forEach(function (element, i) {
            //   const satker = (
            //     <span key={i} className="fs-7 text-dark">
            //       {element.unit_work_name}
            //     </span>
            //   );
            //   satkers.push(satker);
            // });

            const akademis = [...data_akademi];

            // data_akademi.forEach(function (element, i) {
            //   const akademi = (
            //     <span key={i} className="badge badge-secondary me-1 mb-1 ms-1">
            //       {element.nama_akademi}
            //     </span>
            //   );
            //   akademis.push(akademi);
            // });

            const pelatihans = [...data_pelatihan];

            // data_pelatihan.forEach(function (element, i) {
            //   const pelatihan = (
            //     <span key={i} className="badge badge-secondary me-1 mb-1 ms-1">
            //       {element.nama_pelatihan}
            //     </span>
            //   );
            //   pelatihans.push(pelatihan);
            // });

            this.setState({
              data_user,
              data_role,
              data_satker,
              data_akademi,
              data_pelatihan,
              roles,
              satkers,
              akademis,
              pelatihans,
              last_login,
            });
          }
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    }
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Administrator
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.back}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500">
                    <i className="fa fa-chevron-left pe-1"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>

                <a
                  href={
                    "/site-management/administrator/edit/" + this.state.user_id
                  }
                  className="btn btn-warning fw-bolder btn-sm me-2"
                >
                  <i className="bi bi-gear-fill"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Edit
                  </span>
                </a>

                <button
                  onClick={this.handleDelete}
                  className="btn btn-sm btn-danger btn-active-light-info"
                >
                  <i className="bi bi-trash-fill text-white pe-1"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Detail Administrator
                          </h1>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="mb-7">
                          <div className="col-12 mt-5 mb-2">
                            {this.state.data_user.is_active ? (
                              <span className="badge badge-success badge-sm">
                                Aktif
                              </span>
                            ) : (
                              <span className="badge badge-danger badge-sm">
                                Tidak Aktif
                              </span>
                            )}
                            <h1 className="align-items-center text-dark fw-bolder my-1 fs-4">
                              {this.state.data_user.name
                                ? this.state.data_user.name
                                : "-"}
                            </h1>
                          </div>
                          <div className="d-flex flex-wrap fw-bold fs-6 mb-4 pe-2">
                            <span className="d-flex align-items-center fs-7 fw-semibold text-muted me-5 mb-3">
                              <span className="svg-icon svg-icon-4 me-1">
                                <i className="bi bi-building me-1"></i>
                                {this.state.satkers[0]?.unit_work_name || "-"}
                              </span>
                            </span>
                            {/*<span
															className="d-flex align-items-center fs-7 text-muted fw-semibold me-5 mb-3"><span
																className="svg-icon svg-icon-4 me-1"><i className="bi bi-person-vcard me-1"></i>{this.state.data_user.nik
																	? this.state.data_user.nik
																	: "-"}</span></span>*/}
                            <span className="d-flex align-items-center fs-7 text-muted fw-semibold me-5 mb-3">
                              <span className="svg-icon svg-icon-4 me-1">
                                <i className="bi bi-envelope-at me-1"></i>
                                {this.state.data_user.email
                                  ? this.state.data_user.email
                                  : "-"}
                              </span>
                            </span>
                          </div>
                          <span className="d-flex align-items-center fs-7 text-muted me-5 mb-3">
                            Last Login : {this.state.last_login}
                          </span>
                        </div>
                        <div className="mt-4">
                          {/* <CustomList {...configTableSakter} data={this.state.satkers} ></CustomList> */}
                        </div>

                        <div className="d-flex border-bottom p-0">
                          <ul
                            className="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap"
                            style={{
                              overflowX: "auto",
                              overflowY: "hidden",
                              display: "flex",
                              whiteSpace: "nowrap",
                              marginBottom: 0,
                            }}
                          >
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary active"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_1"
                              >
                                Akses
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary false"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_2"
                              >
                                Log Export data
                              </a>
                            </li>
                          </ul>
                        </div>
                        <div className="tab-content" id="detail-account-tab">
                          {/* tab 1 */}
                          <div
                            className="tab-pane fade show active"
                            id="kt_tab_pane_1"
                            role="tabpanel"
                          >
                            <div className="row mt-7">
                              <div className="col-lg-12 mb-7 fv-row">
                                <label className="form-label">Akses Role</label>
                                {/* <div>{this.state.roles}</div> */}
                                <div>
                                  <CustomList
                                    {...configTableRoleAkses}
                                    data={this.state.roles}
                                    mode="table"
                                    chipPropName="role_name"
                                  ></CustomList>
                                </div>
                              </div>
                              {/* {this.state.akademis.length != 0 ? (
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label">Akses Akademi</label>
                                  <div>{this.state.akademis}</div>
                                </div>
                              ) : (
                                ""
                              )} */}
                              {this.state.akademis.length != 0 ? (
                                <>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label">
                                      Akses Akademi
                                    </label>
                                    <div>
                                      <CustomList
                                        {...configTableAkademi}
                                        data={this.state.akademis}
                                        mode="table"
                                        chipPropName="nama_akademi"
                                      ></CustomList>
                                    </div>
                                  </div>
                                </>
                              ) : (
                                <></>
                              )}

                              {this.state.pelatihans.length != 0 ? (
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label">
                                    Akses Pelatihan
                                  </label>
                                  <CustomList
                                    {...configTablePelatihan}
                                    data={this.state.pelatihans}
                                    mode="table"
                                    chipPropName="nama_pelatihan"
                                  ></CustomList>
                                </div>
                              ) : (
                                ""
                              )}
                            </div>
                          </div>
                          <div
                            className="tab-pane fade"
                            id="kt_tab_pane_2"
                            role="tabpanel"
                          >
                            <div className="row">
                              <div className="card-toolbar">
                                <div className="d-flex align-items-center position-relative float-end mt-7 mb-5 me-2">
                                  <span className="svg-icon svg-icon-1 position-absolute ms-6">
                                    <svg
                                      width="24"
                                      height="24"
                                      viewBox="0 0 24 24"
                                      fill="none"
                                      xmlns="http://www.w3.org/2000/svg"
                                      className="mh-50px"
                                    >
                                      <rect
                                        opacity="0.5"
                                        x="17.0365"
                                        y="15.1223"
                                        width="8.15546"
                                        height="2"
                                        rx="1"
                                        transform="rotate(45 17.0365 15.1223)"
                                        fill="currentColor"
                                      ></rect>
                                      <path
                                        d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                        fill="currentColor"
                                      ></path>
                                    </svg>
                                  </span>
                                  <input
                                    type="text"
                                    data-kt-user-table-filter="search"
                                    className="form-control form-control-sm form-control-solid w-250px ps-14"
                                    placeholder="Cari Log"
                                    onKeyPress={this.handleKeyPress}
                                    onChange={(e) => {
                                      if (e.target.value == "") {
                                        this.setState(
                                          { searchText: "" },
                                          () => {
                                            this.handleReload();
                                          },
                                        );
                                      }
                                    }}
                                  />
                                </div>
                              </div>
                              <DataTable
                                columns={this.columns}
                                data={this.state.dataxpdp}
                                progressPending={this.state.loading}
                                progressComponent={this.customLoader}
                                highlightOnHover
                                pointerOnHover
                                pagination
                                paginationServer
                                paginationTotalRows={this.state.totalRows}
                                paginationDefaultPage={this.state.currentPage}
                                onChangeRowsPerPage={(
                                  currentRowsPerPage,
                                  currentPage,
                                ) => {
                                  this.handlePerRowsChange(
                                    currentRowsPerPage,
                                    currentPage,
                                    "row-change",
                                  );
                                }}
                                onChangePage={(page, totalRows) => {
                                  this.handlePerRowsChange(
                                    page,
                                    totalRows,
                                    "page-change",
                                  );
                                }}
                                customStyles={this.customStyles}
                                persistTableHead={true}
                                sortServer
                                onSort={this.handleSort}
                                noDataComponent={
                                  <div className="mt-5">Tidak Ada Data</div>
                                }
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
