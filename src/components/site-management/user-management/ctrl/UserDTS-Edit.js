import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";
import Content from "../UserDTS-Edit-Content";

const UserDTSEdit = () => {
  return (
    <div>
      <SideNav />
      <Header />
      <Content />
      <Footer />
    </div>
  );
};

export default UserDTSEdit;
