import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";
import Content from "../UserAdmin-Edit-Content";

const UserAdminEdit = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default UserAdminEdit;
