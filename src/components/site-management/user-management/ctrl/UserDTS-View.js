import React, { useEffect } from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";
import Content from "../UserDTS-View-Content";
import { cekPermition, logout } from "../../../AksesHelper";
import Swal from "sweetalert2";

const UserDTS = () => {
  useEffect(() => {
    if (cekPermition().view !== 1) {
      Swal.fire({
        title: "Akses Tidak Diizinkan",
        html: "<i> Anda akan kembali kehalaman login </i>",
        icon: "warning",
      }).then(() => {
        logout();
      });
    }
  }, []);

  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default UserDTS;
