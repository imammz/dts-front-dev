import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";
import Content from "../UserMitra-Edit-Content";

const UserMitraEdit = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default UserMitraEdit;
