import React from "react";
import { Card, CardMedia } from "@material-ui/core";

export default class ImageCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      image: this.props.image,
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.image !== this.props.image) {
      this.setState({
        image: this.props.image,
      });
    }
  }

  render() {
    return (
      <div>
        <Card className="mt-3 mb-3">
          <div style={{ position: "relative" }}>
            {this.state.image ? (
              <CardMedia
                className="shadow-none"
                component="img"
                style={{
                  height: 120,
                  boxShadow: "0",
                }}
                src={
                  process.env.REACT_APP_BASE_API_URI +
                  "/download/get-file?path=" +
                  this.state.image
                }
                title="Image"
              />
            ) : (
              <div
                className="text-center pt-20"
                style={{
                  height: 120,
                  boxShadow: "0",
                }}
              >
                <h3
                  style={{
                    color: "#bdc3c7",
                  }}
                >
                  No photo
                </h3>
              </div>
            )}
          </div>
        </Card>
      </div>
    );
  }
}
