import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import { useState } from "react";

const loginUserId = parseInt(Cookies.get("user_id"));

export default function ButtonSetAdmin({
  isAdmin = false,
  userId = "",
  userNama = "",
  axiosConfig = {},
  onRequestStart = () => {},
  onRequestEnd = () => {},
  onSuccess = () => {},
}) {
  // const [showPreModal, setShowPreModal] = useState(false)

  const preMakeAdmin = () => {
    swal
      .fire({
        title: "Perhatian",
        text: `Apakah anda yakin ${
          isAdmin ? "menghapus" : "menjadikan"
        } pengguna ${userNama} ${isAdmin ? "dari" : "sebagai"}  admin ?`,
        icon: "question",
        showCancelButton: true,
        confirmButtonColor: "primary",
        confirmButtonText: `${isAdmin ? "Hapus dari" : "Jadikan sebgai"} admin`,
        cancelButtonText: "Kembali",
        customClass: {
          confirmButton: `${
            isAdmin ? "btn btn-danger btn-sm" : "btn btn-info btn-sm"
          }`,
          cancelButton:
            "btn btn-sm btn-bg-secondary btn-color-gray-100 text-black",
        },
        buttonsStyling: false,
      })
      .then((result) => {
        if (result.isConfirmed) {
          makeAdmin();
          // console.log("okkkk")
        }
      });
  };

  const makeAdmin = () => {
    console.log(onRequestStart);
    // const user = this.state.datax_user

    if (typeof onRequestStart == "function") {
      onRequestStart();
    }

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/tambah-user-admin-peserta",
        { user_id: userId, created_by: loginUserId },
        axiosConfig,
      )
      .then((res) => {
        // console.log(res)
        const response = res.data?.result;

        if (response.Status) {
          if (response?.Data[0]) {
            const { status: resStatus, message: resMessage } = response.Data[0];
            if (resStatus == 200) {
              if (typeof onSuccess == "function") onSuccess();
              swal
                .fire({
                  title: resMessage,
                  icon: "success",
                  confirmButtonText: isAdmin
                    ? "Kembali"
                    : "Lihat user dimenu admin",
                  cancelButtonText: "Kembali",
                  customClass: {
                    confirmButton: `btn btn-info btn-sm`,
                    cancelButton:
                      "btn btn-sm btn-bg-secondary btn-color-gray-100 text-black",
                  },
                  buttonsStyling: false,
                })
                .then((result) => {
                  if (result.isConfirmed) {
                    if (!isAdmin)
                      window.location.href =
                        "/site-management/administrator/edit/" + userId;
                  }
                });
            } else {
              swal
                .fire({
                  title: resMessage,
                  icon: "warning",
                  confirmButtonText: "Lihat user dimenu admin",
                  cancelButtonText: "Kembali",
                  showCancelButton: true,
                  customClass: {
                    confirmButton: `${
                      isAdmin ? "btn btn-danger btn-sm" : "btn btn-info btn-sm"
                    }`,
                    cancelButton:
                      "btn btn-sm btn-bg-secondary btn-color-gray-100 text-black",
                  },
                  buttonsStyling: false,
                })
                .then((result) => {
                  if (result.isConfirmed) {
                    window.location.href =
                      "/site-management/administrator/edit/" + userId;
                  }
                });
            }
          }
        } else {
          swal
            .fire({
              title: "Terjadi kesalahan",
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      })
      .catch((err) => {})
      .finally(() => {
        if (typeof onRequestEnd == "function") onRequestEnd();
      });
    // console.log(user)
  };
  return !isAdmin ? (
    <a
      title="Set Admin"
      className="btn btn-sm bg-info text-white"
      onClick={preMakeAdmin}
    >
      <i className="bi bi-person-fill-add fs-5 text-white me-1"></i>
      Set Admin
    </a>
  ) : (
    <a
      title="Batalkan Sebagai Admin"
      className="btn btn-sm bg-danger text-white"
      onClick={preMakeAdmin}
    >
      <i className="bi bi-person-fill-x fs-5 text-white me-1"></i>
      Batalkan Sebagai Admin
    </a>
  );
}
