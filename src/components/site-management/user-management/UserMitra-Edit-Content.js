import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import Select from "react-select";

export default class UserMitraEditContent extends React.Component {
  constructor(props) {
    super(props);
    this.back = this.back.bind(this);
    this.handleSubmit = this.handleSubmitAction.bind(this);
    this.handleChangeStatus = this.handleChangeStatusAction.bind(this);
    this.handleChangeNama = this.handleChangeNamaAction.bind(this);
    this.handleChangeEmail = this.handleChangeEmailAction.bind(this);
    this.handleChangePassword = this.handleChangePasswordAction.bind(this);
    this.handleChangeConfirmPassword =
      this.handleChangeConfirmPasswordAction.bind(this);
    this.handleChangeNik = this.handleChangeNikAction.bind(this);
    this.showPassword = this.showPassword.bind(this);
    this.showConfirmPassword = this.showConfirmPassword.bind(this);
    this.handleChangeMitra = this.handleChangeMitraAction.bind(this);
    this.handleDelete = this.handleDeleteAction.bind(this);
  }

  state = {
    datax: [],
    datax_current: [],
    valMitra: [],
    isLoading: false,
    column: "",
    loading: false,
    totalRows: 0,
    currentPage: 1,
    mitra_id: "",
    newPerPage: 10,
    profile: [],
    tempLastNumber: 0,
    currentPage: 0,
    valStatus: [],
    pratinjau: [],
    sortBy: "id",
    sortDirection: "desc",
    searchText: "",
    errors: {},
    nama: "",
    email: "",
    password: "",
    confirmPassword: "",
    status_id: false,
    searchText: "",
    nik: "",
    typePassword: "password",
    classEye: "fa fa-eye",
    typeConfirmPassword: "password",
    classConfirmEye: "fa fa-eye",
    user_id: "",
    created_by: "",
  };

  option_status = [
    { value: 1, label: "Aktif" },
    { value: 2, label: "Tidak Aktif" },
  ];

  validURL(str) {
    var pattern = new RegExp(
      "^(https?:\\/\\/)?" + // protocol
        "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
        "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
        "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
        "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
        "(\\#[-a-z\\d_]*)?$",
      "i",
    ); // fragment locator
    return !!pattern.test(str);
  }

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    } else {
      let segment_url = window.location.pathname.split("/");
      let user_id = segment_url[4];
      this.setState({ user_id: user_id });

      const date = new Date();
      const dataMitra = {
        start: 0,
        length: 2000,
        sort: "nama_mitra",
        sort_val: "asc",
        param: "",
        status: 1,
        tahun: date.getFullYear(), //ganti ke current year
      };

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/mitra/list-mitra-s3",
          dataMitra,
          this.configs,
        )
        .then((res) => {
          const datax_mitra = res.data.result.Data;
          const datax = [];
          datax_mitra.map((data) =>
            datax.push({
              value: data.id,
              label: data.nama_mitra + " (" + data.website + ")",
            }),
          );
          this.setState(
            {
              datax,
            },
            () => {
              this.loadData();
            },
          );
        })
        .catch((error) => {});
    }
  }

  loadData() {
    const dataBody = { id: this.state.user_id };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/get-user-mitra-v2",
        dataBody,
        this.configs,
      )
      .then((res) => {
        swal.close();
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statusx) {
          const datax_current = res.data.result.Data[0];
          const nama = datax_current.name;
          const nik = datax_current.nik;
          const email = datax_current.email;
          let valStatus = { value: 2, label: "Tidak Aktif" };
          let status_id = 2;
          if (datax_current.is_active) {
            status_id = 1;
            valStatus = { value: 1, label: "Aktif" };
          }
          const created_by = datax_current.user_id;

          const valMitra = {
            value: datax_current.id_mitra,
            label: datax_current.nama_mitra,
          };
          const mitra_id = datax_current.id_mitra;

          this.setState({
            nama,
            nik,
            email,
            valStatus,
            status_id,
            valMitra,
            created_by,
            mitra_id,
          });
        }
      })
      .catch((error) => {
        console.log(error);
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  back() {
    window.history.back();
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const check = this.checkEmpty(dataForm, ["nama", "nik", "email"], [""]);
    return check;
  }

  resetError() {
    let errors = {};
    errors[("nama", "nik", "email", "status", "password", "confirmPassword")] =
      "";
    this.setState({ errors: errors });
  }

  stringContainsNumber(_string) {
    return /\d/.test(_string);
  }

  containsSpecialChars(str) {
    const specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
    return specialChars.test(str);
  }

  checkEmpty(dataForm, fieldName, fileFieldName) {
    let errors = {};
    let formIsValid = true;
    for (const field in fieldName) {
      const nameAttribute = fieldName[field];
      if (dataForm.get(nameAttribute) == "") {
        errors[nameAttribute] = "Tidak Boleh Kosong";
        formIsValid = false;
      }
    }

    if (dataForm.get("nik").length != 16) {
      errors["nik"] = "NIK Harus 16 Digit";
      formIsValid = false;
    }

    if (this.state.status_id == false) {
      errors["status"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.password.length > 0 && this.state.password.length < 8) {
      errors["password"] = "Password Minimal 8 Digit";
      formIsValid = false;
    }

    if (
      this.state.password.length > 0 &&
      this.state.password == this.state.password.toLowerCase()
    ) {
      console.log("1");
      errors["password"] =
        "Password Harus Memiliki Minimal 1 Huruf Kapital, Angka, Symbol dan Minimal 8 Digit";
      formIsValid = false;
    }

    if (
      this.state.password.length > 0 &&
      this.stringContainsNumber(this.state.password) == false
    ) {
      errors["password"] =
        "Password Harus Memiliki Minimal 1 Huruf Kapital, Angka, Symbol dan Minimal 8 Digit";
      console.log("2");
      formIsValid = false;
    }

    if (
      this.state.password.length > 0 &&
      this.containsSpecialChars(this.state.password) == false
    ) {
      console.log("3");
      errors["password"] =
        "Password Harus Memiliki Minimal 1 Huruf Kapital, Angka, Symbol dan Minimal 8 Digit";
      formIsValid = false;
    }

    if (
      this.state.password.length > 0 &&
      this.state.password != this.state.confirmPassword
    ) {
      errors["confirmPassword"] = "Harus Sama Dengan Password";
      formIsValid = false;
    }

    if (this.state.mitra_id == "") {
      errors["mitra"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    let validRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (this.state.email.match(validRegex) == null) {
      errors["email"] = "Email Tidak Valid";
      formIsValid = false;
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  handleSubmitAction(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });

      const dataFormPost = new FormData();
      dataFormPost.append("created_by", this.state.created_by);
      dataFormPost.append("nama_pic", dataForm.get("nama"));
      dataFormPost.append("password", this.state.password);
      dataFormPost.append("nik", this.state.nik);
      dataFormPost.append("email_pic", dataForm.get("email").toLowerCase());
      let status = false;
      if (this.state.status_id == 1) {
        status = true;
      }
      dataFormPost.append("id_mitra", this.state.mitra_id);
      dataFormPost.append("status", status);
      dataFormPost.append("id_user", this.state.user_id);

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/ubah-user-mitra-v2",
          dataFormPost,
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
                allowOutsideClick: false,
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/site-management/mitra";
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      this.setState({ isLoading: false });
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleChangeStatusAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["status"] = "";
    this.setState({ errors });

    this.setState({
      status_id: selectedOption.value,
      valStatus: { value: selectedOption.value, label: selectedOption.label },
    });
  };

  handleChangeNamaAction(e) {
    const errors = this.state.errors;
    errors["nama"] = "";
    const nama = e.currentTarget.value;
    this.setState({
      nama,
      errors,
    });
  }

  handleChangeEmailAction(e) {
    const errors = this.state.errors;
    errors["email"] = "";
    const email = e.currentTarget.value;
    this.setState({
      email,
      errors,
    });
  }

  handleChangePasswordAction(e) {
    const errors = this.state.errors;
    errors["password"] = "";
    const password = e.currentTarget.value;
    this.setState({
      password,
      errors,
    });
  }

  handleChangeConfirmPasswordAction(e) {
    const errors = this.state.errors;
    errors["confirmPassword"] = "";
    const confirmPassword = e.currentTarget.value;
    this.setState({
      confirmPassword,
      errors,
    });
  }

  handleChangeNikAction(e) {
    const errors = this.state.errors;
    errors["nik"] = "";
    const nik = e.currentTarget.value;
    if (nik.length <= 16) {
      this.setState({
        nik,
      });
    }
    this.setState({
      errors,
    });
  }

  showPassword() {
    if (this.state.typePassword == "password") {
      this.setState({
        typePassword: "text",
        classEye: "fa fa-eye-slash",
      });
    } else {
      this.setState({
        typePassword: "password",
        classEye: "fa fa-eye",
      });
    }
  }

  showConfirmPassword() {
    if (this.state.typeConfirmPassword == "password") {
      this.setState({
        typeConfirmPassword: "text",
        classConfirmEye: "fa fa-eye-slash",
      });
    } else {
      this.setState({
        typeConfirmPassword: "password",
        classConfirmEye: "fa fa-eye",
      });
    }
  }

  handleChangeMitraAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["mitra"] = "";
    this.setState({ errors });

    this.setState({
      mitra_id: selectedOption.value,
      valMitra: selectedOption,
    });
  };

  handleDeleteAction(e) {
    e.preventDefault();
    const idx = this.state.user_id;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          //const data = { id_role: idx }
          const dataFormPost = new FormData();
          dataFormPost.append("id_user", idx);

          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/profile/hapus-user-mitra",
              dataFormPost,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                let icon = "success";
                if (res.data.result.StatusCode == "404") {
                  icon = "warning";
                }
                swal
                  .fire({
                    title: messagex,
                    icon: icon,
                    confirmButtonText: "Ok",
                    allowOutsideClick: false,
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      window.history.back();
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmationButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            });
        }
      });
  }

  render() {
    let rowCounter = 1;
    const styleImg = {
      width: "40px",
      height: "30px",
    };
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  User Mitra
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.back}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>

                <a
                  href="#"
                  title="Hapus"
                  onClick={this.handleDelete}
                  className="btn btn-sm btn-danger btn-active-light-info"
                >
                  <i className="bi bi-trash-fill text-white"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Edit User Mitra
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <form
                        className="form"
                        action="#"
                        onSubmit={this.handleSubmit}
                      >
                        <div className="row my-5">
                          <div className="col-lg-12">
                            <div className="col-lg-12 mb-7 fv-row">
                              <div className="form-group">
                                <div className="row align-items-end">
                                  <div className="col-md mb-md-0">
                                    <label className="form-label required">
                                      Nama Lengkap
                                    </label>
                                    <input
                                      className="form-control form-control-sm"
                                      placeholder="Isi Nama Sesuai KTP"
                                      name="nama"
                                      value={this.state.nama}
                                      onChange={this.handleChangeNama}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["nama"]}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <div className="form-group">
                                <div className="row align-items-end">
                                  <div className="col-md mb-md-0">
                                    <label className="form-label required">
                                      NIK
                                    </label>
                                    <input
                                      className="form-control form-control-sm"
                                      placeholder="Isi NIK Sesuai KTP"
                                      type="number"
                                      name="nik"
                                      value={this.state.nik}
                                      onChange={this.handleChangeNik}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["nik"]}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <div className="form-group">
                                <div className="row align-items-end">
                                  <div className="col-md mb-md-0">
                                    <label className="form-label required">
                                      Email
                                    </label>
                                    <input
                                      autoComplete="off"
                                      className="form-control form-control-sm"
                                      placeholder="Isi Email"
                                      name="email"
                                      value={this.state.email}
                                      onChange={this.handleChangeEmail}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["email"]}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <div className="form-group">
                                <div className="row align-items-end">
                                  <div className="col-md mb-md-0">
                                    <label className="form-label">
                                      Password
                                    </label>
                                    <div className="input-group">
                                      <input
                                        autoComplete="new-password"
                                        className="form-control form-control-sm"
                                        placeholder="Isi Password"
                                        name="password"
                                        type={this.state.typePassword}
                                        value={this.state.password}
                                        onChange={this.handleChangePassword}
                                      />
                                      <span
                                        title="show/hide password"
                                        style={{ cursor: "pointer" }}
                                        className="input-group-text"
                                        id="basic-addon2"
                                        onClick={this.showPassword}
                                      >
                                        <i className={this.state.classEye}></i>
                                      </span>
                                    </div>

                                    <span style={{ color: "red" }}>
                                      {this.state.errors["password"]}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <div className="form-group">
                                <div className="row align-items-end">
                                  <div className="col-md mb-md-0">
                                    <label className="form-label">
                                      Confirm Password
                                    </label>
                                    <div className="input-group">
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Ulangi Password"
                                        name="confirmPassword"
                                        type={this.state.typeConfirmPassword}
                                        value={this.state.confirmPassword}
                                        onChange={
                                          this.handleChangeConfirmPassword
                                        }
                                      />
                                      <span
                                        title="show/hide password"
                                        style={{ cursor: "pointer" }}
                                        className="input-group-text"
                                        id="basic-addon2"
                                        onClick={this.showConfirmPassword}
                                      >
                                        <i
                                          className={this.state.classConfirmEye}
                                        ></i>
                                      </span>
                                    </div>
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["confirmPassword"]}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Pilih Mitra
                              </label>
                              <Select
                                name="mitra"
                                placeholder="Silahkan pilih"
                                noOptionsMessage={({ inputValue }) =>
                                  !inputValue
                                    ? this.state.datax
                                    : "Data tidak tersedia"
                                }
                                className="form-select-sm selectpicker p-0"
                                options={this.state.datax}
                                value={this.state.valMitra}
                                onChange={this.handleChangeMitra}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["mitra"]}
                              </span>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <div className="form-group">
                                <div className="row align-items-end">
                                  <div className="col-md mb-md-0">
                                    <label className="form-label required">
                                      Status
                                    </label>
                                    <Select
                                      name="status"
                                      placeholder="Silahkan pilih"
                                      noOptionsMessage={({ inputValue }) =>
                                        !inputValue
                                          ? this.state.datax
                                          : "Data tidak tersedia"
                                      }
                                      className="form-select-sm selectpicker p-0"
                                      options={this.option_status}
                                      value={this.state.valStatus}
                                      onChange={this.handleChangeStatus}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["status"]}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="text-center my-7">
                          <a
                            href="#"
                            className="btn btn-ms btn-light me-3"
                            title="Batal"
                            onClick={this.back}
                          >
                            {" "}
                            Batal{" "}
                          </a>
                          <button
                            type="submit"
                            className="btn btn-primary btn-md"
                            disabled={this.state.isLoading}
                          >
                            {this.state.isLoading ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm me-2"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Loading...</span>
                                Loading...
                              </>
                            ) : (
                              <>
                                <i className="fa fa-paper-plane me-1"></i>Simpan
                              </>
                            )}
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
