import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import Select from "react-select";

export default class UserAdminTambahContent extends React.Component {
  constructor(props) {
    super(props);
    this.back = this.back.bind(this);
    this.handleSubmit = this.handleSubmitAction.bind(this);
    this.handleChangeStatus = this.handleChangeStatusAction.bind(this);
    this.handleChangeRole = this.handleChangeRoleAction.bind(this);
    this.handleChangeSatker = this.handleChangeSatkerAction.bind(this);
    this.handleChangeNama = this.handleChangeNamaAction.bind(this);
    this.handleChangeEmail = this.handleChangeEmailAction.bind(this);
    this.handleChangePassword = this.handleChangePasswordAction.bind(this);
    this.handleChangeConfirmPassword =
      this.handleChangeConfirmPasswordAction.bind(this);
    this.handleChangeNik = this.handleChangeNikAction.bind(this);
    this.showPassword = this.showPassword.bind(this);
    this.showConfirmPassword = this.showConfirmPassword.bind(this);
    this.handleChangeLevel = this.handleChangeLevelAction.bind(this);
    this.handleChangeAkademi = this.handleChangeAkademiAction.bind(this);
    this.handleChangePelatihan = this.handleChangePelatihanAction.bind(this);
    this.verifikasiNik = this.verifikasiNikAction.bind(this);
  }

  state = {
    datax: [],
    datax_role: [],
    datax_satker: [],
    column: "",
    isLoading: false,
    loading: false,
    totalRows: 0,
    currentPage: 1,
    newPerPage: 10,
    dataxakademivalue: "",
    dataxtemavalue: [],
    profile: [],
    tempLastNumber: 0,
    currentPage: 0,
    valStatus: [],
    pratinjau: [],
    sortBy: "id_pelatihan",
    sortDirection: "DESC",
    searchText: "",
    errors: {},
    nama: "",
    email: "",
    password: "",
    confirmPassword: "",
    option_role: [],
    valRole: [],
    option_satker: [],
    valSatker: [],
    status_id: false,
    datax_akademi: [],
    valAkademi: [],
    option_akademi: [],
    akademi_id: [],
    nik: "",
    typePassword: "password",
    classEye: "fa fa-eye",
    typeConfirmPassword: "password",
    classConfirmEye: "fa fa-eye",
    valLevel: [],
    level: 1,
    valPelatihan: [],
    dataxpelatihan: [],
    totalRowsPelatihan: 0,
    show_akademi: false,
    nik_verified: false,
  };

  option_status = [
    { value: 1, label: "Aktif" },
    { value: 2, label: "Tidak Aktif" },
  ];

  option_level = [
    { value: 1, label: "Akademi" },
    { value: 2, label: "Pelatihan" },
  ];

  validURL(str) {
    var pattern = new RegExp(
      "^(https?:\\/\\/)?" + // protocol
        "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
        "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
        "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
        "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
        "(\\#[-a-z\\d_]*)?$",
      "i",
    ); // fragment locator
    return !!pattern.test(str);
  }

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    } else {
      let dataBody = {
        mulai: 0,
        limit: 100,
        cari: "",
        sort: "id DESC",
      };

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/list-role-v2",
          dataBody,
          this.configs,
        )
        .then((res) => {
          this.setState({ loading: false });
          const datax_role = res.data.result.Data;
          const statusx = res.data.result.Status;
          const messagex = res.data.result.Message;
          const total = res.data.result.JmlData;
          if (statusx && total > 0) {
            const option_role = [];
            datax_role.forEach(function (element, i) {
              if (element.id != 86) {
                const valOption = { value: element.id, label: element.nama };
                option_role.push(valOption);
              }
            });

            this.setState({
              datax_role,
              option_role,
            });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  this.setState({ datax_role: [] });
                }
              });
          }
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  this.setState({ datax_role: [] });
                }
              });
          }
        });

      //penyelenggara
      const dataSatker = {
        mulai: 0,
        limit: 200,
        cari: "",
        sort: "id asc",
      };

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/list_satker",
          dataSatker,
          this.configs,
        )
        .then((res) => {
          swal.close();
          const datax_satker = res.data.result.Data;
          const option_satker = [];
          //option_satker.push({ "value": 0, "label": "Semua" });
          datax_satker.map((data) =>
            option_satker.push({ value: data.id, label: data.name }),
          );
          this.setState({
            datax_satker,
            option_satker,
          });
        });

      //akademi
      const dataAkademik = { start: 0, length: 200, status: "publish" };
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/akademi/list-akademi",
          dataAkademik,
          this.configs,
        )
        .then((res) => {
          swal.close();
          const datax_akademi = res.data.result.Data;
          const option_akademi = [];
          option_akademi.push({ value: 0, label: "Semua" });
          datax_akademi.map((data) =>
            option_akademi.push({ value: data.id, label: data.name }),
          );
          this.setState({
            datax_akademi,
            option_akademi,
          });
        });

      this.setState({
        email: "",
        password: "",
      });
    }
  }

  back() {
    window.history.back();
  }

  handleValidation(e) {
    const check = this.checkEmpty();
    return check;
  }

  resetError() {
    let errors = {};
    errors[
      ("nama",
      "nik",
      "email",
      "status",
      "password",
      "confirmPassword",
      "role",
      "satker",
      "byAkademi",
      "byPelatihan",
      "level")
    ] = "";
    this.setState({ errors: errors });
  }

  stringContainsNumber(_string) {
    return /\d/.test(_string);
  }

  containsSpecialChars(str) {
    const specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
    return specialChars.test(str);
  }

  checkEmpty() {
    let errors = {};
    let formIsValid = true;

    if (this.state.nama == "") {
      errors["nama"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }
    if (this.state.nik == "") {
      errors["nik"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }
    if (this.state.email == "") {
      errors["email"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.nik.length != 16) {
      errors["nik"] = "NIK Harus 16 Digit";
      formIsValid = false;
    }

    if (this.state.status_id == false) {
      errors["status"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.valRole.length == 0) {
      errors["role"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.valSatker.length == 0) {
      errors["satker"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.password.length < 8) {
      errors["password"] = "Password Minimal 8 Digit";
      formIsValid = false;
    }

    if (this.state.password == this.state.password.toLowerCase()) {
      console.log("1");
      errors["password"] =
        "Password Harus Memiliki Minimal 1 Huruf Kapital, Angka, Symbol dan Minimal 8 Digit";
      formIsValid = false;
    }

    if (this.stringContainsNumber(this.state.password) == false) {
      errors["password"] =
        "Password Harus Memiliki Minimal 1 Huruf Kapital, Angka, Symbol dan Minimal 8 Digit";
      console.log("2");
      formIsValid = false;
    }

    if (this.containsSpecialChars(this.state.password) == false) {
      console.log("3");
      errors["password"] =
        "Password Harus Memiliki Minimal 1 Huruf Kapital, Angka, Symbol dan Minimal 8 Digit";
      formIsValid = false;
    }

    if (this.state.password != this.state.confirmPassword) {
      errors["confirmPassword"] = "Harus Sama Dengan Password";
      formIsValid = false;
    }

    if (this.state.show_akademi) {
      if (this.state.valLevel.length == 0) {
        errors["level"] = "Tidak Boleh Kosong";
        formIsValid = false;
      }

      if (this.state.level == 1 && this.state.valAkademi.length == 0) {
        errors["byAkademi"] = "Tidak Boleh Kosong";
        formIsValid = false;
      }

      if (this.state.level == 2 && this.state.valAkademi.length == 0) {
        errors["byAkademi"] = "Tidak Boleh Kosong";
        formIsValid = false;
      }

      if (this.state.level == 2 && this.state.valPelatihan.length == 0) {
        errors["byPelatihan"] = "Tidak Boleh Kosong";
        formIsValid = false;
      }
    }

    let validRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (this.state.email.match(validRegex) == null) {
      errors["email"] = "Email Tidak Valid";
      formIsValid = false;
    }

    console.log(errors);

    this.setState({ errors: errors });
    return formIsValid;
  }

  handleSubmitAction(e) {
    e.preventDefault();
    this.resetError();
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });

      const roles = [];
      this.state.valRole.forEach(function (element) {
        const role = {
          roles_user_id: element.value,
        };
        roles.push(role);
      });

      let satkers = [];
      const context = this.state;
      [this.state.valSatker].forEach(function (element) {
        if (element.value == 0) {
          context.option_satker.map((data) => {
            if (data.value != 0) {
              const satker = {
                unit_rowk_id: data.value,
              };
              satkers.push(satker);
            }
          });
        } else {
          const satker = {
            unit_rowk_id: element.value,
          };
          satkers.push(satker);
        }
      });

      satkers = [
        ...new Map(
          satkers.map((item) => [item["unit_rowk_id"], item]),
        ).values(),
      ];

      const byAkademis = [];
      const byPelatihans = [];
      if (this.state.level == 1) {
        this.state.valAkademi.forEach(function (element) {
          const akademi = {
            id_akademi: element.value,
          };
          byAkademis.push(akademi);
        });
      } else if (this.state.level == 2) {
        this.state.valPelatihan.forEach(function (element) {
          const pelatihan = {
            id_pelatihan: element.value,
          };
          byPelatihans.push(pelatihan);
        });
      }

      const dataFormPost = new FormData();
      dataFormPost.append("name", this.state.nama);
      dataFormPost.append("email", this.state.email.toLowerCase());
      dataFormPost.append("nik", this.state.nik);
      let is_active = false;
      if (this.state.status_id == 1) {
        is_active = true;
      }
      dataFormPost.append("is_active", is_active);
      dataFormPost.append("password", this.state.password);
      dataFormPost.append("role_id", JSON.stringify(roles));
      dataFormPost.append("satuan_kerja", JSON.stringify(satkers));
      dataFormPost.append(
        "hak_akses_pelatihan_by_akademi",
        JSON.stringify(byAkademis),
      );
      dataFormPost.append(
        "hak_akses_pelatihan_by_pelatihan",
        JSON.stringify(byPelatihans),
      );
      dataFormPost.append("created_by", Cookies.get("user_id"));

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/tambah-user-admin",
          dataFormPost,
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
                allowOutsideClick: false,
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/site-management/administrator";
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleChangeStatusAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["status"] = "";
    this.setState({ errors });

    this.setState({
      status_id: selectedOption.value,
      valStatus: { value: selectedOption.value, label: selectedOption.label },
    });
  };

  handleChangeRoleAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["role"] = "";
    this.setState({ errors });

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    let akses_akademi = false;
    let arr_request = [];
    const context = this;
    selectedOption.forEach(function (element) {
      const id_role = element.value;
      const dataBody = {
        mulai: 0,
        limit: 100,
        sort: "role_id asc",
        id_role: id_role,
      };
      const request = axios.post(
        process.env.REACT_APP_BASE_API_URI + "/detail-role",
        dataBody,
        context.configs,
      );
      arr_request.push(request);
    });

    axios.all(arr_request).then(
      axios.spread((...responses) => {
        responses.forEach(function (element) {
          const detail = element.data.result.Detail;
          detail.forEach(function (elementDetail) {
            if (
              elementDetail.permissions_id == 200 &&
              (elementDetail.manage_role == 1 ||
                elementDetail.publish_role == 1 ||
                elementDetail.view_role == 1)
            ) {
              akses_akademi = true;
            }
          });
        });
        context.setState(
          {
            show_akademi: akses_akademi,
          },
          () => {
            swal.close();
          },
        );
      }),
    );

    this.setState({
      valRole: selectedOption,
    });
  };

  handleChangeSatkerAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["satker"] = "";
    this.setState({ errors });

    this.setState({
      valSatker: selectedOption,
    });
  };

  handleChangeNamaAction(e) {
    const errors = this.state.errors;
    errors["nama"] = "";
    let nama = e.currentTarget.value;
    nama = nama.toUpperCase();
    this.setState({
      nama,
      errors,
    });
  }

  handleChangeEmailAction(e) {
    const errors = this.state.errors;
    errors["email"] = "";
    const email = e.currentTarget.value;
    this.setState({
      email,
      errors,
    });
  }

  handleChangePasswordAction(e) {
    const errors = this.state.errors;
    errors["password"] = "";
    const password = e.currentTarget.value;
    this.setState({
      password,
      errors,
    });
  }

  handleChangeConfirmPasswordAction(e) {
    const errors = this.state.errors;
    errors["confirmPassword"] = "";
    const confirmPassword = e.currentTarget.value;
    this.setState({
      confirmPassword,
      errors,
    });
  }

  handleChangeNikAction(e) {
    const errors = this.state.errors;
    errors["nik"] = "";
    const nik = e.currentTarget.value;
    if (nik.length <= 16) {
      this.setState({
        nik,
      });
    }
    this.setState({
      errors,
    });
  }

  showPassword() {
    if (this.state.typePassword == "password") {
      this.setState({
        typePassword: "text",
        classEye: "fa fa-eye-slash",
      });
    } else {
      this.setState({
        typePassword: "password",
        classEye: "fa fa-eye",
      });
    }
  }

  showConfirmPassword() {
    if (this.state.typeConfirmPassword == "password") {
      this.setState({
        typeConfirmPassword: "text",
        classConfirmEye: "fa fa-eye-slash",
      });
    } else {
      this.setState({
        typeConfirmPassword: "password",
        classConfirmEye: "fa fa-eye",
      });
    }
  }

  handleChangeLevelAction = (selectedLevel) => {
    const errors = this.state.errors;
    errors["level"] = "";
    this.setState({ errors });

    const level = selectedLevel.value;
    this.setState({
      valLevel: { label: selectedLevel.label, value: selectedLevel.value },
      level: level,
      valAkademi: [],
      valPelatihan: [],
    });
  };

  handleChangeAkademiAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["byAkademi"] = "";
    this.setState({ errors });

    this.setState(
      {
        valPelatihan: [],
        pelatihan_id: null,
        valAkademi: selectedOption,
        //akademi_id: akademi_id.value,
      },
      () => {
        if (this.state.level != 1) {
          swal.fire({
            title: "Memuat Data Pelatihan",
            icon: "info", // add html attribute if you want or remove
            allowOutsideClick: false,
            didOpen: () => {
              swal.showLoading();
            },
          });
          let dataBody = [];
          let is_all = false;
          this.state.valAkademi.forEach(function (element, i) {
            dataBody.push({ akademi_id: element.value });
            if (element.value == 0) {
              is_all = true;
            }
          });

          if (is_all) {
            dataBody = [];
            this.state.option_akademi.forEach(function (element, i) {
              dataBody.push({ akademi_id: element.value });
            });
          }

          axios
            .post(
              process.env.REACT_APP_BASE_API_URI +
                "/useradmin/get_pelatihan_info_byjson",
              JSON.stringify(dataBody),
              this.configs,
            )
            .then((res) => {
              swal.close();
              const optionx = res.data.result.Data;
              const totalRowsPelatihan = res.data.result.JmlData;
              const dataxpelatihan = optionx.reduce(
                (r, { tema: label, ...object }) => {
                  var temp = r.find((o) => o.label === label);
                  if (!temp) r.push((temp = { label, options: [] }));
                  temp.options.push({
                    value: object.id_peatihan,
                    label:
                      "[" +
                      object.penyelenggara +
                      "] - " +
                      object.slug +
                      object.id_pelatihan +
                      " - " +
                      object.nama_pelatihan +
                      " Batch " +
                      object.batch +
                      " - " +
                      object.lokasi,
                  });
                  return r;
                },
                [],
              );
              this.setState({
                dataxpelatihan,
                totalRowsPelatihan,
              });
            })
            .catch((error) => {
              swal.close();
              console.log(error);
              const dataxpelatihan = [];
              const totalRowsPelatihan = 0;
              this.setState({
                dataxpelatihan,
                totalRowsPelatihan,
              });
              let messagex = error.response.data.result.Message;
            });
        }
      },
    );
  };

  handleChangePelatihanAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["byPelatihan"] = "";
    this.setState({ errors });

    this.setState(
      {
        valPelatihan: selectedOption,
      },
      () => {
        console.log(this.state.valPelatihan);
      },
    );
  };

  verifikasiNikAction() {
    let errors = {};
    let formIsValid = true;

    if (this.state.nik == "") {
      errors["nik"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.nama == "") {
      errors["nama"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.nik.length != 16) {
      errors["nik"] = "NIK Harus 16 Digit";
      formIsValid = false;
    }

    this.setState({ errors: errors });
    if (!formIsValid) {
      return;
    }

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    const bodyJson = {
      NIK: this.state.nik,
      NAMA_LGKP: this.state.nama,
      NO_KK: "",
      JENIS_KLMIN: "",
      TMPT_LHR: "",
      TGL_LHR: "",
      STATUS_KAWIN: "",
      JENIS_PKRJN: "",
      NAMA_LGKP_IBU: "",
      ALAMAT: "",
      NO_PROP: "",
      NO_KAB: "",
      NO_KEC: "",
      NO_KEL: "",
      PROP_NAME: "",
      KAB_NAME: "",
      KEC_NAME: "",
      KEL_NAME: "",
      NO_RT: "",
      NO_RW: "",
      TRESHOLD: "100",
    };

    axios
      .post(
        "https://dukapi.sdmdigital.id/hitnik",
        JSON.stringify(bodyJson),
        this.configs,
      )
      .then((res) => {
        const datax = res.data.content[0];
        const NAMA_LGKP = datax.NAMA_LGKP;
        if (NAMA_LGKP == "Sesuai (100)") {
          console.log("nik sesuai");
          this.setState({ nik_verified: true });
          swal
            .fire({
              title: "Nik dan Nama Terverifikasi",
              icon: "success",
              confirmButtonText: "Ok",
              allowOutsideClick: false,
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        } else {
          console.log("salah");
          this.setState({ nik_verified: false });
          swal
            .fire({
              title: "Nik dan Nama Tidak Sesuai",
              icon: "warning",
              confirmButtonText: "Ok",
              allowOutsideClick: false,
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      })
      .catch((error) => {});
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Administrator
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.back}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Tambah Administrator
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <form
                        className="form"
                        action="#"
                        onSubmit={this.handleSubmit}
                      >
                        <div className="row">
                          <div className="col-12">
                            <div className="col-lg-12 mb-7 fv-row">
                              <div className="form-group">
                                <div className="row align-items-end">
                                  <div className="col-md mb-md-0">
                                    <label className="form-label required">
                                      Nama Lengkap
                                    </label>
                                    <input
                                      className="form-control form-control-sm"
                                      placeholder="Isi Nama Sesuai KTP"
                                      name="nama"
                                      value={this.state.nama}
                                      onChange={this.handleChangeNama}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["nama"]}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-lg-12 mb-2 fv-row">
                              <div className="form-group">
                                <div className="row align-items-end">
                                  <div className="col-md mb-md-0">
                                    <label className="form-label required">
                                      NIK
                                    </label>
                                    <input
                                      className="form-control form-control-sm"
                                      placeholder="Isi NIK Sesuai KTP"
                                      type="number"
                                      name="nik"
                                      value={this.state.nik}
                                      onChange={this.handleChangeNik}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["nik"]}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row2">
                              <div className="form-group mb-3">
                                <button
                                  type="button"
                                  className="btn btn-primary btn-sm fw-semibold form-control"
                                  onClick={this.verifikasiNik}
                                >
                                  Validasi NIK
                                </button>
                              </div>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <div className="form-group">
                                <div className="row align-items-end">
                                  <div className="col-md mb-md-0">
                                    <label className="form-label required">
                                      Email
                                    </label>
                                    <input
                                      autoComplete="off"
                                      className="form-control form-control-sm"
                                      placeholder="Isi Email"
                                      name="email"
                                      value={this.state.email}
                                      onChange={this.handleChangeEmail}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["email"]}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <div className="form-group">
                                <div className="row align-items-end">
                                  <div className="col-md mb-md-0">
                                    <label className="form-label required">
                                      Status
                                    </label>
                                    <Select
                                      name="status"
                                      placeholder="Silahkan pilih"
                                      noOptionsMessage={({ inputValue }) =>
                                        !inputValue
                                          ? this.state.datax
                                          : "Data tidak tersedia"
                                      }
                                      className="form-select-sm selectpicker p-0"
                                      options={this.option_status}
                                      value={this.state.valStatus}
                                      onChange={this.handleChangeStatus}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["status"]}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <div className="form-group">
                                <div className="row align-items-end">
                                  <div className="col-md mb-md-0">
                                    <label className="form-label required">
                                      Password
                                    </label>
                                    <div className="input-group">
                                      <input
                                        autoComplete="new-password"
                                        className="form-control form-control-sm"
                                        placeholder="Isi Password"
                                        name="password"
                                        type={this.state.typePassword}
                                        value={this.state.password}
                                        onChange={this.handleChangePassword}
                                      />
                                      <span
                                        title="show/hide password"
                                        style={{ cursor: "pointer" }}
                                        className="input-group-text"
                                        id="basic-addon2"
                                        onClick={this.showPassword}
                                      >
                                        <i className={this.state.classEye}></i>
                                      </span>
                                    </div>

                                    <span style={{ color: "red" }}>
                                      {this.state.errors["password"]}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <div className="form-group">
                                <div className="row align-items-end">
                                  <div className="col-md mb-md-0">
                                    <label className="form-label required">
                                      Confirm Password
                                    </label>
                                    <div className="input-group">
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Ulangi Password"
                                        name="confirmPassword"
                                        type={this.state.typeConfirmPassword}
                                        value={this.state.confirmPassword}
                                        onChange={
                                          this.handleChangeConfirmPassword
                                        }
                                      />
                                      <span
                                        title="show/hide password"
                                        style={{ cursor: "pointer" }}
                                        className="input-group-text"
                                        id="basic-addon2"
                                        onClick={this.showConfirmPassword}
                                      >
                                        <i
                                          className={this.state.classConfirmEye}
                                        ></i>
                                      </span>
                                    </div>
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["confirmPassword"]}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <div className="form-group">
                                <div className="row align-items-end">
                                  <div className="col-md mb-md-0">
                                    <label className="form-label required">
                                      Role
                                    </label>
                                    <Select
                                      name="role"
                                      placeholder="Silahkan pilih"
                                      noOptionsMessage={({ inputValue }) =>
                                        !inputValue
                                          ? this.state.datax
                                          : "Data tidak tersedia"
                                      }
                                      className="form-select-sm selectpicker p-0"
                                      options={this.state.option_role}
                                      value={this.state.valRole}
                                      onChange={this.handleChangeRole}
                                      isMulti={true}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["role"]}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              {this.state.show_akademi ? (
                                <div>
                                  <div className="col-lg-12 mb-7 fv-row my-4">
                                    <label className="form-label required">
                                      Level Admin
                                    </label>
                                    <Select
                                      id="level"
                                      name="level"
                                      placeholder="Silahkan pilih"
                                      noOptionsMessage={({ inputValue }) =>
                                        !inputValue
                                          ? this.state.dataxakademi
                                          : "Data tidak tersedia"
                                      }
                                      className="form-select-sm selectpicker p-0"
                                      onChange={this.handleChangeLevel}
                                      options={this.option_level}
                                      value={this.state.valLevel}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["level"]}
                                    </span>
                                  </div>
                                  {this.state.valLevel.value == 1 ||
                                  this.state.valLevel.value == 2 ? (
                                    <div>
                                      <div className="col-lg-12 mb-7 fv-row my-1">
                                        <label className="form-label required">
                                          Akademi
                                        </label>
                                        <Select
                                          id="akademi"
                                          name="akademi"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.state.dataxakademi
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm selectpicker p-0"
                                          onChange={this.handleChangeAkademi}
                                          options={this.state.option_akademi}
                                          value={this.state.valAkademi}
                                          isMulti={true}
                                        />
                                        <span style={{ color: "red" }}>
                                          {this.state.errors["byAkademi"]}
                                        </span>
                                      </div>
                                      {this.state.valLevel.value == 2 ? (
                                        <div className="col-lg-12 mb-7 fv-row my-1">
                                          <label className="form-label required">
                                            Pelatihan
                                          </label>
                                          <Select
                                            id="pelatihan"
                                            name="pelatihan"
                                            placeholder="Silahkan pilih"
                                            noOptionsMessage={({
                                              inputValue,
                                            }) =>
                                              !inputValue
                                                ? this.state.dataxakademi
                                                : "Data tidak tersedia"
                                            }
                                            className="form-select-sm selectpicker p-0"
                                            onChange={
                                              this.handleChangePelatihan
                                            }
                                            options={this.state.dataxpelatihan}
                                            value={this.state.valPelatihan}
                                            isMulti={true}
                                          />
                                          <span style={{ color: "red" }}>
                                            {this.state.errors["byPelatihan"]}
                                          </span>
                                        </div>
                                      ) : (
                                        ""
                                      )}
                                    </div>
                                  ) : (
                                    ""
                                  )}
                                </div>
                              ) : (
                                ""
                              )}
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <div className="form-group">
                                <div className="row align-items-end">
                                  <div className="col-md mb-md-0">
                                    <label className="form-label required">
                                      Satuan Kerja
                                    </label>
                                    <Select
                                      name="satker"
                                      placeholder="Silahkan pilih"
                                      noOptionsMessage={({ inputValue }) =>
                                        !inputValue
                                          ? this.state.datax
                                          : "Data tidak tersedia"
                                      }
                                      className="form-select-sm selectpicker p-0"
                                      options={this.state.option_satker}
                                      value={this.state.valSatker}
                                      onChange={this.handleChangeSatker}
                                      isMulti={false}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["satker"]}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="text-center my-7">
                            <a
                              href="#"
                              className="btn btn-md btn-light me-3"
                              title="kembali"
                              onClick={this.back}
                            >
                              {" "}
                              Batal{" "}
                            </a>
                            <button
                              type="button"
                              onClick={(e) => {
                                this.handleSubmit(e);
                              }}
                              className="btn btn-primary btn-md"
                              disabled={
                                !this.state.nik_verified || this.state.isLoading
                              }
                              title={
                                !this.state.nik_verified
                                  ? "Mohon Verifikasi Nik Terlebih Dahulu"
                                  : ""
                              }
                            >
                              {this.state.isLoading ? (
                                <>
                                  <span
                                    className="spinner-border spinner-border-sm me-2"
                                    role="status"
                                    aria-hidden="true"
                                  ></span>
                                  <span className="sr-only">Loading...</span>
                                  Loading...
                                </>
                              ) : (
                                <>
                                  <i className="fa fa-paper-plane me-1"></i>
                                  Simpan
                                </>
                              )}
                            </button>
                            <br />
                            {!this.state.nik_verified ? (
                              <small className="text text-danger">
                                Silakan Verifikasi NIK untuk Melakukan Submit
                              </small>
                            ) : (
                              ""
                            )}
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
