import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import Select from "react-select";
import {
  capitalizeFirstLetter,
  colorStatusPelaksanaan,
  getColorClassStatusPendaftaran,
  indonesianDateFormat,
  indonesianDateFormatWithoutTime,
  statusPelaksanaanWithLabel,
} from "../../publikasi/helper";
import ButtonSetAdmin from "./components/ButtonSetAdmin";
import { capitalWord, dateRange } from "../../pelatihan/Pelatihan/helper";
import moment from "moment";

export default class UserDTSEditContent extends React.Component {
  constructor(props) {
    super(props);
    this.back = this.back.bind(this);
    this.handleSubmit = this.handleSubmitAction.bind(this);
    this.showPassword = this.showPassword.bind(this);
    this.showConfirmPassword = this.showConfirmPassword.bind(this);
    this.handleChangePassword = this.handleChangePasswordAction.bind(this);
    this.handleChangeConfirmPassword =
      this.handleChangeConfirmPasswordAction.bind(this);
    this.handleChangeNama = this.handleChangeNamaAction.bind(this);
    this.handleChangeEmail = this.handleChangeEmailAction.bind(this);
    this.handleChangeNik = this.handleChangeNikAction.bind(this);
    this.handleChangeKelamin = this.handleChangeKelaminAction.bind(this);
    this.handleChangeHandphone = this.handleChangeHandphoneAction.bind(this);
    this.handleChangePendidikan = this.handleChangePendidikanAction.bind(this);
    this.handleChangeTempatLahir =
      this.handleChangeTempatLahirAction.bind(this);
    this.handleChangeTanggalLahir =
      this.handleChangeTanggalLahirAction.bind(this);
    this.handleChangeKontakDarurat =
      this.handleChangeKontakDaruratAction.bind(this);
    this.handleChangeHandphoneDarurat =
      this.handleChangeHandphoneDaruratAction.bind(this);
    this.handleChangeAlamat = this.handleChangeAlamatAction.bind(this);
    this.handleChangeProvinsi_1 = this.handleChangeProvinsi_1Action.bind(this);
    this.handleChangeKabupaten_1 =
      this.handleChangeKabupaten_1Action.bind(this);
    this.handleChangeKecamatan_1 =
      this.handleChangeKecamatan_1Action.bind(this);
    this.handleChangeDesa_1 = this.handleChangeDesa_1Action.bind(this);
    this.handleChangeFoto = this.handleChangeFotoAction.bind(this);
    this.handleChangeSearch = this.handleChangeSearchAction.bind(this);
    this.handleSubmitUbahPelatihan =
      this.handleSubmitUbahPelatihanAction.bind(this);
    this.handleChangeDetailPelatihan =
      this.handleChangeDetailPelatihanAction.bind(this);
    this.handleChangeSelectPelatihan =
      this.handleChangeSelectPelatihanAction.bind(this);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleSort = this.handleSortAction.bind(this);
    this.handleMakeAdmin = this.handleMakeAdminAction.bind(this);
    this.handleDelete = this.handleDeleteAction.bind(this);
    this.handleChangeStatusPekerjaan =
      this.handleChangeStatusPekerjaanAction.bind(this);
    this.handleChangePekerjaan = this.handleChangePekerjaanAction.bind(this);
    this.handleChangeInstitusi = this.handleChangeInstitusiAction.bind(this);
    this.handleChangeHubungan = this.handleChangeHubunganAction.bind(this);
    this.pratinjauTriggerRef = React.createRef(null);
  }

  state = {
    datax_user: [],
    datax: [],
    totalRows: 0,
    isLoading: false,
    newPerPage: 10,
    tempLastNumber: 0,
    loading: false,
    errors: {},
    hover: 1,
    selected: 1,
    foto: "",
    ktp: "",
    nama: "",
    nik: "",
    email: "",
    nomor_handphone: "",
    tempat_lahir: "",
    tanggal_lahir: "",
    jenis_kelamin: "",
    jenjang_id: "",
    jenjang_nama: "",
    address: "",
    provinsi_id: "",
    provinsi_nama: "",
    kota_id: "",
    kota_nama: "",
    kecamatan_id: "",
    kecamatan_nama: "",
    kelurahan_id: "",
    kelurahan_nama: "",
    status_pekerjaan_id: "",
    status_pekerjaan_nama: "",
    pekerjaan_id: "",
    pekerjaan_nama: "",
    institusi: "",
    nama_kontak_darurat: "",
    hubungan: "",
    nomor_handphone_darurat: "",
    kode_pos: "",
    ijazah: "",
    datax_provinsi_1: [],
    valProvinsi_1: [],
    provinsi_id_1: "",
    datax_provinsi_2: [],
    valProvinsi_2: [],
    provinsi_id_2: "",
    datax_kabupaten_1: [],
    valKabupaten_1: [],
    kabupaten_id_1: "",
    datax_kabupaten_2: [],
    valKabupaten_2: [],
    kabupaten_id_2: "",
    datax_kecamatan_1: [],
    valKecamatan_1: [],
    kecamatan_id_1: "",
    datax_kecamatan_2: [],
    valKecamatan_2: [],
    kecamatan_id_2: "",
    datax_desa_1: [],
    valDesa_1: [],
    desa_id_1: "",
    datax_desa_2: [],
    valDesa_2: [],
    desa_id_2: "",
    option_pendidikan: [],
    password: "",
    confirmPassword: "",
    typePassword: "password",
    classEye: "fa fa-eye",
    typeConfirmPassword: "password",
    classConfirmEye: "fa fa-eye",
    searchText: "",
    selected_data: 0,
    main: 1,
    detail_nama_pelatihan: "",
    detail_id_pelatihan: "",
    detail_status_pelatihan: "",
    ubah_data_status: 0,
    detail_all_pelatihan: [],
    detailPelatihanVal: [],
    pelatihan_to: "",
    pelatihan_from: "",
    detail_akademi: "",
    detail_tema: "",
    detail_penyelenggara: "",
    detail_provinsi: "",
    detail_pelatihan_mulai: "",
    detail_pelatihan_selesai: "",
    datax_select_pelatihan: [],
    valSelectPelatihan: {},
    from_pagination_change: 0,
    pelatihan_id: "",
    sort_by: "id",
    sort_val: "DESC",
    user_data_temp: {},
    from_edit_photo: false,
    totalLulus: 0,
    totalTidakLulus: 0,
    option_status_pekerjaan: [],
    valStatusPekerjaan: [],
    option_list_pekerjaan: [],
    valPekerjaan: [],
    option_kontak_darurat: [],
    valHubungan: [],
    isadmin: false,
    statusBekerja: true,
    penyelenggara: "",
    pelatihan_id_slug: "",
    nama_mitra: "",
    slug_pelatian_id: "",
    nama_mitra: "",
    metode_pelatihan: "",
    alamat_detail: "",
    provinsi_detail: "",
    kabupaten_detail: "",
    tanggal_pelatihan_detail: "",
    zonasi: "",
    zonasi: "",
    status_pelaksanaan: "",
    color_status_pelaksanaan: "",
    status_peserta: "",
  };

  option_status = [
    { value: 1, label: "Aktif" },
    { value: 2, label: "Tidak Aktif" },
  ];

  option_kelamin = [
    { value: 1, label: "Pria" },
    { value: 2, label: "Wanita" },
  ];

  option_pendidikan = [
    { value: "Tidak Sekolah", label: "Tidak Sekolah" },
    { value: "TK", label: "TK" },
    { value: "SD/Sederajat", label: "SD/Sederajat" },
    { value: "SMP/Sederajat", label: "SMP/Sederajat" },
    { value: "SMA/Sederajat", label: "SMA/Sederajat" },
    { value: "D1", label: "D1" },
    { value: "D2", label: "D2" },
    { value: "D3", label: "D3" },
    { value: "D4", label: "D4" },
    { value: "S1", label: "S1" },
    { value: "S2", label: "S2" },
    { value: "S3", label: "S3" },
  ];

  option_kontak_darurat = [
    { value: "Orang Tua", label: "Orang Tua" },
    { value: "Saudara", label: "Saudara" },
    { value: "Pasangan", label: "Pasangan" },
    { value: "Kerabat", label: "Kerabat" },
  ];

  validURL(str) {
    var pattern = new RegExp(
      "^(https?:\\/\\/)?" + // protocol
        "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
        "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
        "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
        "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
        "(\\#[-a-z\\d_]*)?$",
      "i",
    ); // fragment locator
    return !!pattern.test(str);
  }

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  style = {
    selectedSide: {
      padding: "8px",
      backgroundColor: "#ecf5fc",
      marginBottom: "4px",
      borderRadius: "8px",
      //fontSize: '16px',
      fontWeight: "700",
      width: "95%",
    },
    normalSide: {
      padding: "8px",
      //backgroundColor: '#ecf5fc',
      marginBottom: "4px",
      borderRadius: "8px",
      //fontSize: '16px',
      fontWeight: "700",
      width: "95%",
    },
  };

  columns = [
    {
      name: "No",
      center: true,
      cell: (row, index) => this.state.tempLastNumber + index + 1,
      width: "70px",
    },
    {
      name: "ID Pelatihan",
      sortable: true,
      center: false,
      width: "150px",
      //width: '180px',
      selector: (row) => <div>{row.pelatian_id_slug}</div>,
    },
    {
      name: "Nama Pelatihan",
      className: "min-w-300px mw-300px",
      width: "300px",
      sortable: true,
      grow: 6,
      wrap: true,
      allowOverflow: false,
      selector: (row) => (
        <div>
          <span className="d-flex flex-column my-2">
            <a
              href="#"
              onClick={() => {
                this.setState({ selected_data: 1, main: 0 });
                this.loadDataPelatihan(row.pelatian_id);
                const valSelectPelatihan = {
                  value: row.pelatian_id,
                  label: row.nama_pelatihan,
                };
                this.setState({
                  valSelectPelatihan,
                });
              }}
              title="Detail"
              className="text-dark"
            >
              <span
                className="fw-bolder fs-7"
                style={{
                  overflow: "hidden",
                  whiteSpace: "wrap",
                  textOverflow: "ellipses",
                }}
              >
                {capitalizeFirstLetter(row.nama_pelatihan)}
              </span>
              <h6 className="text-muted fs-7 fw-semibold mb-1">
                {row.penyelenggara}
              </h6>
              <span className="text-muted fs-7 fw-semibold">
                {row.metode_pelatihan}
              </span>
            </a>
          </span>
        </div>
      ),
    },
    {
      name: "Tgl. Pendaftaran",
      sortable: true,
      center: false,
      width: "180px",
      selector: (row) => (
        <span className="fs-7">
          {dateRange(
            row.pendaftaran_mulai,
            row.pendaftaran_selesai,
            "YYYY-MM-DD HH:mm:ss",
            "DD MMM YYYY",
          )}
        </span>
      ),
    },
    {
      name: "Tgl. Pelatihan",
      sortable: true,
      center: false,
      width: "180px",
      selector: (row) => (
        <span className="fs-7">
          {dateRange(
            row.pelatihan_mulai,
            row.pelatihan_selesai,
            "YYYY-MM-DD HH:mm:ss",
            "DD MMM YYYY",
          )}
        </span>
      ),
    },
    {
      name: "Lokasi",
      sortable: true,
      cell: (row) => (
        <div>
          <span className="fs-7">
            {row.kabupaten ? row.kabupaten : "Online"}
          </span>
        </div>
      ),
    },
    {
      name: "Status",
      sortable: true,
      center: true,
      width: "200px",
      selector: (row) => (
        <span className="fs-7">
          <span
            className={
              "badge badge-" +
              getColorClassStatusPendaftaran(row.status_peserta)
            }
          >
            {row.status_peserta}
          </span>
        </span>
      ),
    },
    {
      name: "Nilai",
      sortable: false,
      //width: "150px",
      cell: (row) => (
        <div>
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              this.getNilaiPeserta(row.user_id, row.pelatian_id);
            }}
            id={row.id}
            title="Lihat Nilai"
            className="btn btn-icon btn-success btn-sm me-1"
          >
            <i class="bi bi-journal-check text-white"></i>
          </a>
        </div>
      ),
    },
    {
      name: "Aksi",
      center: true,
      width: "150px",
      cell: (row) => (
        <div>
          <a
            href="#"
            onClick={() => {
              this.setState({ selected_data: 1, main: 0 });
              this.loadDataPelatihan(row.pelatian_id);
              const valSelectPelatihan = {
                value: row.pelatian_id,
                label: row.nama_pelatihan,
              };
              const status_peserta = row.status_peserta;

              this.setState({
                valSelectPelatihan,
                status_peserta,
              });
            }}
            title="Detail"
            className="btn btn-icon btn-bg-primary btn-sm me-1"
          >
            <i className="bi bi-eye-fill text-white"></i>
          </a>

          {/* Edit Page */}
          {row.show_edit ? (
            <a
              href="#"
              onClick={() => {
                this.setState({ selected_data: 2, main: 0 });
                this.loadDataPelatihan(row.pelatian_id);
              }}
              title="Edit"
              className="btn btn-icon btn-bg-warning btn-sm me-1"
              alt="Edit"
            >
              <i className="bi bi-gear-fill text-white"></i>
            </a>
          ) : (
            ""
          )}
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    } else {
      let segment_url = window.location.pathname.split("/");
      let user_id = segment_url[4];
      this.setState({ user_id: user_id }, () => this.handleReload());

      let dataBodyUser = {
        userid: user_id,
      };

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/provinsi",
          null,
          this.configs,
        )
        .then((res) => {
          swal.close();
          const optionx = res.data.result.Data;
          const datax_provinsi = [];

          optionx.map((data) =>
            datax_provinsi.push({ value: data.id, label: data.name }),
          );
          this.setState(
            {
              datax_provinsi_1: datax_provinsi,
              datax_provinsi_2: datax_provinsi,
            },
            () => {},
          );
        });

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/user/pendidikan",
          null,
          this.configs,
        )
        .then((res) => {
          const optionx = res.data.result.Data;
          const option_pendidikan = [];

          optionx.map((data) =>
            option_pendidikan.push({ value: data.id, label: data.nama }),
          );
          this.setState({
            option_pendidikan,
          });
        });

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/admin-status-pekerjaan",
          null,
          this.configs,
        )
        .then((res) => {
          swal.close();
          const optionx = res.data.result.Data;
          const option_status_pekerjaan = [];
          optionx.map((data) =>
            option_status_pekerjaan.push({ value: data.id, label: data.nama }),
          );
          this.setState({
            option_status_pekerjaan,
          });
        })
        .catch((error) => {
          const option_status_pekerjaan = [];
          this.setState({
            option_status_pekerjaan,
          });
          let messagex = error.response.data.result.Message;
        });

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/admin-bidang-pekerjaan",
          null,
          this.configs,
        )
        .then((res) => {
          swal.close();
          const optionx = res.data.result.Data;
          const option_list_pekerjaan = [];
          optionx.map((data) =>
            option_list_pekerjaan.push({ value: data.id, label: data.nama }),
          );
          this.setState({
            option_list_pekerjaan,
          });
        })
        .catch((error) => {
          const option_list_pekerjaan = [];
          this.setState({
            option_list_pekerjaan,
          });
          let messagex = error.response.data.result.Message;
        });

      //const axios_pelatihan = axios.post(process.env.REACT_APP_BASE_API_URI + '/list_admin_user_pelatihan', dataBodyPelatihan, this.configs);

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/get_user_byid",
          dataBodyUser,
          this.configs,
        )
        .then((res) => {
          const datax_user = res.data.result.data[0];
          const statusx = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statusx) {
            const foto = datax_user.foto;
            const ktp = datax_user.ktp;
            const nama = datax_user.nama;
            const nik = datax_user.nik;
            const email = datax_user.email;
            const nomor_handphone = datax_user.nomor_handphone
              ? datax_user.nomor_handphone.replace("-", "")
              : "";
            let tempat_lahir = "-";
            if (datax_user.tempat_lahir && datax_user.tempat_lahir != "null") {
              tempat_lahir = datax_user.tempat_lahir;
            }
            let tanggal_lahir = "";
            if (datax_user.tanggal_lahir) {
              tanggal_lahir = indonesianDateFormat(datax_user.tanggal_lahir);
            }
            const jenis_kelamin = datax_user.jenis_kelamin;
            const jenjang_id = datax_user.jenjang_id
              ? datax_user.jenjang_id
              : "-";
            const jenjang_nama = datax_user.jenjang_nama;
            const address = datax_user.address;
            const provinsi_id = datax_user.provinsi_id;
            const provinsi_nama = datax_user.provinsi_nama;
            const kota_id = datax_user.kota_id;
            const kota_nama = datax_user.kota_nama;
            const kecamatan_id = datax_user.kecamatan_id;
            const kecamatan_nama = datax_user.kecamatan_nama;
            const kelurahan_id = datax_user.kelurahan_id;
            const kelurahan_nama = datax_user.kelurahan_nama;
            const status_pekerjaan_id = datax_user.status_pekerjaan_id;
            const status_pekerjaan_nama = datax_user.status_pekerjaan_nama;
            const pekerjaan_id = datax_user.pekerjaan_id;
            const pekerjaan_nama = datax_user.pekerjaan_nama;
            const perusahaan = datax_user.perusahaan;
            const nama_kontak_darurat = datax_user.nama_kontak_darurat;
            const hubungan = datax_user.hubungan;
            const nomor_handphone_darurat = datax_user.nomor_handphone_darurat
              ? datax_user.nomor_handphone_darurat
              : "-";
            const ijazah = datax_user.ijazah;

            let statusBekerja = false;
            if (status_pekerjaan_nama == "Bekerja") {
              statusBekerja = true;
            }

            let valKelamin = [];
            if (jenis_kelamin == "Pria" || jenis_kelamin == "1") {
              valKelamin = { value: 1, label: "Pria" };
            } else if (jenis_kelamin == "Wanita" || jenis_kelamin == "2") {
              valKelamin = { value: 2, label: "Wanita" };
            }

            let valPendidikan = [];
            if (jenjang_id) {
              valPendidikan = { value: jenjang_id, label: jenjang_nama };
            }

            let valProvinsi_1 = [];
            if (provinsi_id) {
              valProvinsi_1 = { value: provinsi_id, label: provinsi_nama };
              this.loadKabupaten(provinsi_id);
            }

            let valKabupaten_1 = [];
            if (provinsi_id && kota_id) {
              valKabupaten_1 = { value: kota_id, label: kota_nama };
              this.loadKecamatan(provinsi_id, kota_id);
            }

            let valKecamatan_1 = [];
            if (provinsi_id && kota_id && kecamatan_id) {
              valKecamatan_1 = { value: kecamatan_id, label: kecamatan_nama };
              this.loadDesa(provinsi_id, kota_id, kecamatan_id);
            }

            let valDesa_1 = [];
            if (kelurahan_id) {
              valDesa_1 = { value: kelurahan_id, label: kelurahan_nama };
            }

            let valStatusPekerjaan = [];
            if (status_pekerjaan_id) {
              valStatusPekerjaan = {
                value: status_pekerjaan_id,
                label: status_pekerjaan_nama,
              };
            }

            let valPekerjaan = [];
            if (pekerjaan_id) {
              valPekerjaan = { value: pekerjaan_id, label: pekerjaan_nama };
            }

            let valHubungan = [];
            if (hubungan) {
              valHubungan = { value: hubungan, label: hubungan };
            }

            this.setState({
              datax_user,
              foto,
              image: foto,
              ktp,
              nama,
              nik,
              email,
              nomor_handphone,
              tempat_lahir,
              tanggal_lahir,
              jenis_kelamin,
              jenjang_id,
              jenjang_nama,
              address,
              provinsi_id,
              provinsi_nama,
              kota_id,
              kota_nama,
              kecamatan_id,
              kecamatan_nama,
              kelurahan_id,
              kelurahan_nama,
              status_pekerjaan_id,
              status_pekerjaan_nama,
              pekerjaan_id,
              pekerjaan_nama,
              institusi: perusahaan,
              nama_kontak_darurat,
              hubungan,
              nomor_handphone_darurat,
              ijazah,
              valKelamin,
              valProvinsi_1,
              valKabupaten_1,
              valKecamatan_1,
              valDesa_1,
              valPendidikan,
              provinsi_id_1: provinsi_id,
              kabupaten_id_1: kota_id,
              kecamatan_id_1: kecamatan_id,
              desa_id_1: kelurahan_id,
              valStatusPekerjaan,
              valPekerjaan,
              isadmin: datax_user.isadmin,
              valHubungan,
              statusBekerja,
            });
          }
        });
    }
  }

  handleReload(page, newPerPage) {
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });
    let dataBodyPelatihan = {
      userid: this.state.user_id,
      start: start_tmp,
      rows: length_tmp,
      cari: this.state.searchText,
      sort: this.state.sort_by,
      sort_val: this.state.sort_val,
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/user/list_admin_user_pelatihan",
        dataBodyPelatihan,
        this.configs,
      )
      .then((res) => {
        const datax = res.data.result.Data;
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        let totalLulus = 0;
        let totalTidakLulus = 0;
        for (let i = 0; i < datax.length; i++) {
          if (
            datax[i].status_peserta == "Lulus Pelatihan - Nilai" ||
            datax[i].status_peserta == "Lulus Pelatihan - Kehadiran"
          ) {
            totalLulus += 1;
          }

          if (datax[i].status_peserta.includes("Tidak Lulus")) {
            totalTidakLulus += 1;
          }
        }
        if (statusx) {
          const now = new Date().getTime();
          datax.forEach(function (data, i) {
            const awal = new Date(datax[i].pendaftaran_mulai);
            const selesai = new Date(datax[i].pendaftaran_selesai);
            const is_between = now <= selesai && now >= awal ? true : false;
            if (i == 0 && start_tmp == 0 && is_between) {
              datax[i].show_edit = true;
            } else {
              datax[i].show_edit = false;
            }
          });

          this.setState({
            datax: datax,
            loading: false,
            totalRows: res.data.result.TotalLength[0].jml_data,
            totalLulus,
            totalTidakLulus,
          });

          const datax_select_pelatihan = [];
          datax.map((data) => {
            datax_select_pelatihan.push({
              value: data.pelatian_id,
              label: data.nama_pelatihan,
            });
          });

          const pelatihan_id = datax[0].pelatian_id;

          this.setState(
            {
              datax_select_pelatihan,
              pelatihan_id,
            },
            () => {
              const bodyPelatihan = {
                pelatihan_id: pelatihan_id,
              };
              axios
                .post(
                  process.env.REACT_APP_BASE_API_URI +
                    "/list_admin_pindah_pelatihan_peserta",
                  bodyPelatihan,
                  this.configs,
                )
                .then((res) => {
                  const optionx = res.data.result.Data;
                  const detail_all_pelatihan = [];

                  optionx.map((data) => {
                    detail_all_pelatihan.push({
                      value: data.id,
                      label: data.nama_pelatihan,
                    });
                  });

                  this.setState({
                    detail_all_pelatihan,
                  });
                });
            },
          );
        } else {
          this.setState({
            datax: [],
            loading: false,
            totalRows: 0,
          });
          let messagex = "Data Tidak Ditemukan";
          if (this.state.selected != 1) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        }
      })
      .catch((error) => {
        this.setState({
          datax: [],
          loading: false,
          totalRows: 0,
        });
        //let messagex = error.response.data.result.Message;
        let messagex = "Data Tidak Ditemukan";
        if (this.state.selected != 1) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  loadKabupaten(kdprop) {
    const dataBody = {
      kdprop,
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/kabupaten",
        dataBody,
        this.configs,
      )
      .then((res) => {
        swal.close();
        const optionx = res.data.result.Data;
        const datax_pelatihan_kabkot = [];
        optionx.map((data) =>
          datax_pelatihan_kabkot.push({ value: data.id, label: data.name }),
        );
        this.setState({
          datax_kabupaten_1: datax_pelatihan_kabkot,
        });
      })
      .catch((error) => {
        const datax_pelatihan_kabkot = [];
        this.setState({
          datax_kabupaten_1: datax_pelatihan_kabkot,
        });
        let messagex = error.response.data.result.Message;
        swal
          .fire({
            title: messagex,
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
            }
          });
      });
  }

  loadKecamatan(kdprop, kdkab) {
    const dataBody = {
      kdprop,
      kdkab,
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/kecamatan",
        dataBody,
        this.configs,
      )
      .then((res) => {
        swal.close();
        const optionx = res.data.result.Data;
        const datax_kecamatan = [];
        optionx.map((data) =>
          datax_kecamatan.push({ value: data.id, label: data.name }),
        );
        this.setState({
          datax_kecamatan_1: datax_kecamatan,
        });
      })
      .catch((error) => {
        const datax_kecamatan = [];
        this.setState({
          datax_kecamatan_1: datax_kecamatan,
        });
        let messagex = error.response.data.result.Message;
        swal
          .fire({
            title: messagex,
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
            }
          });
      });
  }

  loadDesa(kdprop, kdkab, kdkec) {
    const dataBody = {
      kdprop,
      kdkab,
      kdkec,
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/desa",
        dataBody,
        this.configs,
      )
      .then((res) => {
        swal.close();
        const optionx = res.data.result.Data;
        const datax_desa = [];
        optionx.map((data) =>
          datax_desa.push({ value: data.id, label: data.name }),
        );
        this.setState({
          datax_desa_1: datax_desa,
        });
      })
      .catch((error) => {
        const datax_desa = [];
        this.setState({
          datax_desa_1: datax_desa,
        });
        let messagex = error.response.data.result.Message;
        swal
          .fire({
            title: messagex,
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
            }
          });
      });
  }

  back() {
    window.history.back();
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const check = this.checkEmpty(dataForm, ["nama"], [""]);
    return check;
  }

  resetError() {
    let errors = {};
    errors[
      ("email",
      "status",
      "password",
      "confirmPassword",
      "kelamin",
      //"pendidikan",
      //"tempat_lahir",
      "tanggal_lahir",
      "kontak_darurat",
      "handphone_darurat",
      "provinsi",
      "kota",
      "kecamatan",
      "kelurahan",
      "institusi")
    ] = "";
    this.setState({ errors: errors });
  }

  stringContainsNumber(_string) {
    return /\d/.test(_string);
  }

  containsSpecialChars(str) {
    const specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
    return specialChars.test(str);
  }

  checkEmpty(dataForm, fieldName, fileFieldName) {
    let errors = {};
    let formIsValid = true;
    for (const field in fieldName) {
      const nameAttribute = fieldName[field];
      if (dataForm.get(nameAttribute) == "") {
        errors[nameAttribute] = "Tidak Boleh Kosong";
        formIsValid = false;
      }
    }

    if (this.state.password.length != 0) {
      if (this.state.password.length < 8) {
        errors["password"] = "Password Minimal 8 Digit";
        formIsValid = false;
      }

      if (this.state.password == this.state.password.toLowerCase()) {
        console.log("1");
        errors["password"] =
          "Password Harus Memiliki Minimal 1 Huruf Kapital, Angka dan Symbol";
        formIsValid = false;
      }

      if (this.stringContainsNumber(this.state.password) == false) {
        errors["password"] =
          "Password Harus Memiliki Minimal 1 Huruf Kapital, Angka dan Symbol";
        console.log("2");
        formIsValid = false;
      }

      if (this.containsSpecialChars(this.state.password) == false) {
        console.log("3");
        errors["password"] =
          "Password Harus Memiliki Minimal 1 Huruf Kapital, Angka dan Symbol";
        formIsValid = false;
      }

      if (this.state.password != this.state.confirmPassword) {
        errors["confirmPassword"] = "Harus Sama Dengan Password";
        formIsValid = false;
      }
    }

    if (this.state.jenis_kelamin == "" || this.state.jenis_kelamin == null) {
      errors["kelamin"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    /* if (this.state.jenjang_id == "" || this.state.jenjang_id == null) {
			errors["pendidikan"] = "Tidak Boleh Kosong";
			formIsValid = false;
		} */

    /* if (this.state.tempat_lahir == "" || this.state.tempat_lahir == null) {
			errors["tempat_lahir"] = "Tidak Boleh Kosong";
			formIsValid = false;
		} */

    if (this.state.tanggal_lahir == "" || this.state.tanggal_lahir == null) {
      errors["tanggal_lahir"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    /* if (
			this.state.nama_kontak_darurat == "" ||
			this.state.nama_kontak_darurat == null
		) {
			errors["kontak_darurat"] = "Tidak Boleh Kosong";
			formIsValid = false;
		}

		if (
			this.state.nomor_handphone_darurat == "" ||
			this.state.nomor_handphone_darurat == null
		) {
			errors["handphone_darurat"] = "Tidak Boleh Kosong";
			formIsValid = false;
		} */

    if (this.state.address == "" || this.state.address == null) {
      errors["alamat"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.provinsi_id_1 == "" || this.state.provinsi_id_1 == null) {
      errors["provinsi"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.kabupaten_id_1 == "" || this.state.kabupaten_id_1 == null) {
      errors["kota"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.kecamatan_id_1 == "" || this.state.kecamatan_id_1 == null) {
      errors["kecamatan"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.desa_id_1 == "" || this.state.desa_id_1 == null) {
      errors["kelurahan"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (
      this.state.status_pekerjaan_id == "" ||
      this.state.status_pekerjaan_id == null
    ) {
      errors["status_pekerjaan"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    /* if (this.state.pekerjaan_id == '' || this.state.pekerjaan_id == null) {
				errors['pekerjaan'] = 'Tidak Boleh Kosong';
				formIsValid = false;
			} */

    /* if (this.state.hubungan == "" || this.state.hubungan == null) {
			errors["hubungan"] = "Tidak Boleh Kosong";
			formIsValid = false;
		} */

    if (
      this.state.status_pekerjaan_nama == "Bekerja" &&
      (this.state.pekerjaan_id == "" || this.state.pekerjaan_id == null)
    ) {
      errors["pekerjaan"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (
      this.state.status_pekerjaan_nama == "Bekerja" &&
      (this.state.institusi == "" || this.state.institusi == null)
    ) {
      errors["institusi"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }
    console.log(errors);
    this.setState({ errors: errors });
    return formIsValid;
  }

  handleDate(dateString) {
    moment.locale("id");
    if (moment(dateString, "DD MMMM YYYY").isValid()) {
      return moment(dateString, "DD MMMM YYYY").format("YYYY-MM-DD");
    }
    return dateString;
  }

  handleSubmitAction(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.state.selected == 1) {
      if (this.handleValidation(e)) {
        this.setState({ isLoading: true });
        swal.fire({
          title: "Mohon Tunggu!",
          icon: "info", // add html attribute if you want or remove
          allowOutsideClick: false,
          didOpen: () => {
            swal.showLoading();
          },
        });

        const dataFormPost = new FormData();
        dataFormPost.append("id", this.state.user_id);
        dataFormPost.append("name", this.state.nama);
        dataFormPost.append("email", this.state.email);
        dataFormPost.append("nik", this.state.nik);
        dataFormPost.append("nomor_hp", this.state.nomor_handphone);
        dataFormPost.append("tempat_lahir", this.state.tempat_lahir);
        let tanggal = this.handleDate(this.state.tanggal_lahir);
        dataFormPost.append("tanggal_lahir", tanggal);
        dataFormPost.append("jenis_kelamin", this.state.jenis_kelamin);
        dataFormPost.append("jenjang_id", this.state.jenjang_id);
        dataFormPost.append("address", this.state.address);
        dataFormPost.append("provinsi_id", this.state.provinsi_id_1);
        dataFormPost.append("kota_id", this.state.kabupaten_id_1);
        dataFormPost.append("kecamatan_id", this.state.kecamatan_id_1);
        dataFormPost.append("kelurahan_id", this.state.desa_id_1);
        let status_pekerjaan_id = 0;
        if (
          this.state.status_pekerjaan_id != "" &&
          this.state.status_pekerjaan_id != null
        ) {
          status_pekerjaan_id = this.state.status_pekerjaan_id;
        }
        let institusi = 0;
        if (this.state.institusi != "" && this.state.institusi != null) {
          institusi = this.state.institusi;
        }
        let pekerjaan_id = 0;
        if (this.state.pekerjaan_id != "" && this.state.pekerjaan_id != null) {
          pekerjaan_id = this.state.pekerjaan_id;
        }
        dataFormPost.append("status_pekerjaan_id", status_pekerjaan_id);
        dataFormPost.append("perusahaan", institusi);
        dataFormPost.append("pekerjaan", pekerjaan_id);
        dataFormPost.append(
          "nama_kontak_darurat",
          this.state.nama_kontak_darurat,
        );
        dataFormPost.append(
          "nomor_handphone_darurat",
          this.state.nomor_handphone_darurat,
        );
        dataFormPost.append("hubungan", this.state.hubungan);
        let password = this.state.password;
        if (this.state.password == "") {
          password = 0;
        }

        dataFormPost.append("pass_baru", password);

        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/user/profilUpdateUser",
            dataFormPost,
            this.configs,
          )
          .then((res) => {
            this.setState({ isLoading: false });
            const statux = res.data.result.Status;
            const messagex = res.data.result.Message;
            if (statux) {
              swal
                .fire({
                  title: "Data Berhasil Disimpan",
                  icon: "success",
                  confirmButtonText: "Ok",
                  allowOutsideClick: false,
                })
                .then((result) => {
                  if (result.isConfirmed) {
                    window.location =
                      "/site-management/user-dts/preview/" + this.state.user_id;
                  }
                });
            } else {
              this.setState({ isLoading: false });
              swal
                .fire({
                  title: messagex,
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                  }
                });
            }
          })
          .catch((error) => {
            this.setState({ isLoading: false });
            console.log(error.response);
            let messagex = error.response.data.result;

            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          });
      } else {
        this.setState({ isLoading: false });
        swal
          .fire({
            title: "Mohon Periksa Isian",
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
            }
          });
      }
    }
  }

  showPassword() {
    if (this.state.typePassword == "password") {
      this.setState({
        typePassword: "text",
        classEye: "fa fa-eye-slash",
      });
    } else {
      this.setState({
        typePassword: "password",
        classEye: "fa fa-eye",
      });
    }
  }

  showConfirmPassword() {
    if (this.state.typeConfirmPassword == "password") {
      this.setState({
        typeConfirmPassword: "text",
        classConfirmEye: "fa fa-eye-slash",
      });
    } else {
      this.setState({
        typeConfirmPassword: "password",
        classConfirmEye: "fa fa-eye",
      });
    }
  }

  handleChangePasswordAction(e) {
    const errors = this.state.errors;
    errors["password"] = "";
    const password = e.currentTarget.value;
    this.setState({
      password,
      errors,
    });
  }

  handleChangeConfirmPasswordAction(e) {
    const errors = this.state.errors;
    errors["confirmPassword"] = "";
    const confirmPassword = e.currentTarget.value;
    this.setState({
      confirmPassword,
      errors,
    });
  }

  handleChangeNamaAction(e) {
    const errors = this.state.errors;
    errors["nama"] = "";

    const nama = e.currentTarget.value;
    this.setState({
      errors,
      nama,
    });
  }

  handleChangeEmailAction(e) {
    const errors = this.state.errors;
    errors["email"] = "";

    const email = e.currentTarget.value;
    this.setState({
      errors,
      email,
    });
  }

  handleChangeNikAction(e) {
    const errors = this.state.errors;
    errors["nik"] = "";

    const nik = e.currentTarget.value;
    this.setState({
      errors,
      nik,
    });
  }

  handleChangeKelaminAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["kelamin"] = "";
    this.setState({ errors });

    this.setState({
      jenis_kelamin: selectedOption.value,
      valKelamin: selectedOption,
    });
  };

  handleChangeHandphoneAction(e) {
    const errors = this.state.errors;
    errors["nomor_handphone"] = "";

    const nomor_handphone = e.currentTarget.value;
    this.setState({
      errors,
      nomor_handphone,
    });
  }

  handleChangePendidikanAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["pendidikan"] = "";
    this.setState({ errors });

    this.setState({
      jenjang_id: selectedOption.value,
      valPendidikan: selectedOption,
    });
  };

  handleChangeTempatLahirAction(e) {
    const errors = this.state.errors;
    errors["tempat_lahir"] = "";

    const tempat_lahir = e.currentTarget.value;
    this.setState({
      errors,
      tempat_lahir,
    });
  }

  handleChangeTanggalLahirAction(e) {
    const errors = this.state.errors;
    errors["tanggal_lahir"] = "";

    const tanggal_lahir = e.currentTarget.value;

    this.setState({
      errors,
      tanggal_lahir,
    });
  }

  handleChangeKontakDaruratAction(e) {
    const errors = this.state.errors;
    errors["kontak_darurat"] = "";

    const nama_kontak_darurat = e.currentTarget.value;
    this.setState({
      errors,
      nama_kontak_darurat,
    });
  }

  handleChangeHandphoneDaruratAction(e) {
    const errors = this.state.errors;
    errors["handphone_darurat"] = "";
    const nomor_handphone_darurat = e.currentTarget.value;

    if (nomor_handphone_darurat.length <= 15) {
      this.setState({
        nomor_handphone_darurat,
      });
    }
    this.setState({
      errors,
    });
  }

  handleChangeAlamatAction(e) {
    const errors = this.state.errors;
    errors["alamat"] = "";

    const address = e.currentTarget.value;
    this.setState({
      errors,
      address,
    });
  }

  handleChangeProvinsi_1Action = (selectedOption) => {
    this.setState({
      kabupaten_id_1: "",
      valKabupaten_1: [],
      kecamatan_id_1: "",
      valKecamatan_1: [],
      desa_id_1: "",
      valDesa_1: [],
    });
    const errors = this.state.errors;
    errors["provinsi"] = "";
    this.setState({ errors });

    this.loadKabupaten(selectedOption.value);

    this.setState({
      provinsi_id_1: selectedOption.value,
      valProvinsi_1: selectedOption,
    });
  };

  handleChangeKabupaten_1Action = (selectedOption) => {
    this.setState({
      kecamatan_id_1: "",
      valKecamatan_1: [],
      desa_id_1: "",
      valDesa_1: [],
    });
    const errors = this.state.errors;
    errors["kota"] = "";
    this.setState({ errors });

    this.loadKecamatan(this.state.provinsi_id_1, selectedOption.value);

    this.setState({
      kabupaten_id_1: selectedOption.value,
      valKabupaten_1: selectedOption,
    });
  };

  handleChangeKecamatan_1Action = (selectedOption) => {
    this.setState({
      desa_id_1: "",
      valDesa_1: [],
    });

    const errors = this.state.errors;
    errors["kecamatan"] = "";
    this.setState({ errors });

    this.loadDesa(
      this.state.provinsi_id_1,
      this.state.kabupaten_id_1,
      selectedOption.value,
    );

    this.setState({
      kecamatan_id_1: selectedOption.value,
      valKecamatan_1: selectedOption,
    });
  };

  handleChangeDesa_1Action = (selectedOption) => {
    const errors = this.state.errors;
    errors["kelurahan"] = "";
    this.setState({ errors });

    this.setState({
      desa_id_1: selectedOption.value,
      valDesa_1: selectedOption,
    });
  };

  handleChangeFotoAction(e) {
    const logofile = e.target.files[0];
    const formDatax = new FormData();
    formDatax.append("userid", this.state.user_id);
    formDatax.append("foto_profil", logofile);
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/user/fotoUserPesertaUpdate",
        formDatax,
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        let dataBodyUser = {
          userid: this.state.user_id,
        };
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/get_user_byid",
            dataBodyUser,
            this.configs,
          )
          .then((res) => {
            const datax_user2 = res.data.result.data[0];
            const statusx2 = res.data.result.Status;
            if (statusx2) {
              const foto = datax_user2.foto;
              this.setState({ image: foto });
            }
          });
        if (statux) {
          swal
            .fire({
              title: "Foto Berhasil Disimpan",
              icon: "success",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                //window.location = '/site-management/user-dts/edit/' + this.state.user_id;
              }
            });
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      })
      .catch((error) => {
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  handleChangeSearchAction(e) {
    const searchText = e.currentTarget.value;
    this.setState({
      searchText: searchText,
    });
    if (searchText == "") {
      this.setState(
        {
          searchText: searchText,
        },
        () => {
          this.handleReload();
        },
      );
    }
  }

  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    this.setState({ searchText });
    if (e.key == "Enter") {
      if (searchText == "") {
        this.setState(
          {
            isSearch: false,
            searchText: "",
          },
          () => {
            this.handleReload();
          },
        );
      } else {
        this.setState({ loading: true });
        this.setState({ isSearch: true });
        this.setState({ searchText: searchText }, () => {
          this.handleReload();
        });
      }
    }
  }

  loadDataPelatihan(pelatihan_id) {
    this.setState({
      pelatihan_from: pelatihan_id,
    });
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    let dataBody = {
      id: pelatihan_id,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/pelatihanz/findpelatihan2",
        dataBody,
        this.configs,
      )
      .then((res) => {
        swal.close();
        const datax_pelatihan = res.data.result.Data[0];
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statusx) {
          console.log(datax_pelatihan);
          const detail_nama_pelatihan = datax_pelatihan.pelatihan;
          const detail_id_pelatihan = pelatihan_id;
          const detail_status_pelatihan = datax_pelatihan.status_pelatihan;
          const detail_akademi = datax_pelatihan.akademi;
          const detail_tema = datax_pelatihan.tema;
          const detail_penyelenggara = datax_pelatihan.penyelenggara;
          const detail_provinsi = datax_pelatihan.nm_prov;
          const detail_pelatihan_mulai = indonesianDateFormatWithoutTime(
            datax_pelatihan.pelatihan_mulai,
          );
          const detail_pelatihan_selesai = indonesianDateFormatWithoutTime(
            datax_pelatihan.pelatihan_selesai,
          );
          const slug_pelatian_id = datax_pelatihan.slug_pelatian_id;
          const nama_mitra = datax_pelatihan.mitra_name;
          const metode_pelatihan = datax_pelatihan.metode_pelatihan;
          const alamat_detail = datax_pelatihan.alamat;
          const provinsi_detail = datax_pelatihan.provinsi;
          const kabupaten_detail = datax_pelatihan.kabupaten;
          const tanggal_pelatihan_detail = dateRange(
            datax_pelatihan.pelatihan_start,
            datax_pelatihan.pelatihan_end,
            "DD-MM-YYYY HH:mm:ss",
            "DD MMM YYYY",
          );

          const pelatihan_start_reform =
            datax_pelatihan.pelatihan_start.split(" ")[0];
          const pelatihan_end_reform =
            datax_pelatihan.pelatihan_end.split(" ")[0];

          const pelatihan_start_reform_2 = pelatihan_start_reform.split("-");
          const pelatihan_end_reform_2 = pelatihan_end_reform.split("-");

          const pelatihan_start_new =
            pelatihan_start_reform_2[2] +
            "-" +
            pelatihan_start_reform_2[1] +
            "-" +
            pelatihan_start_reform_2[0];
          const pelatihan_end_new =
            pelatihan_end_reform_2[2] +
            "-" +
            pelatihan_end_reform_2[1] +
            "-" +
            pelatihan_end_reform_2[0];

          const status_pelaksanaan = statusPelaksanaanWithLabel(
            pelatihan_start_new,
            pelatihan_end_new,
          );
          const zonasi = datax_pelatihan.zonasi;

          const color_status_pelaksanaan = colorStatusPelaksanaan(
            pelatihan_start_new,
            pelatihan_end_new,
          );

          this.setState({
            detail_nama_pelatihan,
            detail_id_pelatihan,
            detail_status_pelatihan,
            detail_akademi,
            detail_tema,
            detail_penyelenggara,
            detail_provinsi,
            detail_pelatihan_mulai,
            detail_pelatihan_selesai,
            slug_pelatian_id,
            nama_mitra,
            metode_pelatihan,
            alamat_detail,
            provinsi_detail,
            kabupaten_detail,
            tanggal_pelatihan_detail,
            zonasi,
            status_pelaksanaan,
            color_status_pelaksanaan,
          });
        }
      });
  }

  handleChangeDetailPelatihanAction = (selectedOption) => {
    /* const errors = this.state.errors;
			errors['jenis_kelamin'] = '';
			this.setState({ errors });
	 */
    this.setState({
      pelatihan_to: selectedOption.value,
      detailPelatihanVal: selectedOption,
    });
  };

  handleChangeSelectPelatihanAction = (selectedOption) => {
    /* const errors = this.state.errors;
			errors['jenis_kelamin'] = '';
			this.setState({ errors }); */

    this.setState(
      {
        valSelectPelatihan: selectedOption,
      },
      () => {
        this.loadDataPelatihan(selectedOption.value);
      },
    );
  };

  handleSubmitUbahPelatihanAction(e) {
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    const bodyJson = [
      {
        pelatian_id_fr: this.state.pelatihan_from,
        pelatian_id_to: this.state.pelatihan_to,
        created_by: Cookies.get("user_id"),
        users: [
          {
            userid: this.state.user_id,
          },
        ],
      },
    ];

    e.preventDefault();

    const formData = new FormData();
    formData.append("param", JSON.stringify(bodyJson));

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/simpan_pindah_pelatihan_peserta",
        formData,
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        const data = res.data.result.Data[0];
        if (statux) {
          swal
            .fire({
              title: "Data Berhasil Disimpan",
              icon: "success",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                window.location =
                  "/site-management/user-dts/edit/" + this.state.user_id;
              }
            });
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      })
      .catch((error) => {
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Data;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  handlePageChange = (page) => {
    if (this.state.from_pagination_change == 0) {
      this.setState({ loading: true });
      this.handleReload(page, this.state.newPerPage);
    }
  };
  handlePerRowsChange = async (newPerPage, page) => {
    if (this.state.from_pagination_change == 1) {
      this.setState({ loading: true });
      this.setState({ newPerPage: newPerPage }, () => {
        this.handleReload(page, this.state.newPerPage);
      });
    }
  };

  handleSortAction(column, sortDirection) {
    let server_name = "";
    if (column.name == "Nama Pelatihan") {
      server_name = "nama";
    } else if (column.name == "ID Pelatihan") {
      server_name = "id";
    } else if (column.name == "Status") {
      server_name = "status";
    }

    this.setState(
      {
        sort_by: server_name,
        sort_val: sortDirection.toUpperCase(),
      },
      () => {
        this.handleReload(1, this.state.newPerPage);
      },
    );
  }

  handleMakeAdminAction() {
    const user = this.state.datax_user;
    this.setState({ loading: true });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/tambah-user-admin-peserta",
        { user_id: user.id, created_by: parseInt(Cookies.get("user_id")) },
        this.configs,
      )
      .then((res) => {
        // console.log(res)
        const response = res.data?.result;

        if (response.Status) {
          if (response?.Data[0]) {
            const { status: resStatus, message: resMessage } = response.Data[0];
            if (resStatus == 200) {
              swal
                .fire({
                  title: resMessage,
                  icon: "success",
                  confirmButtonText: "Lihat user dimenu admin",
                  cancelButtonText: "Kembali",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                    window.location.href =
                      "/site-management/administrator/edit/" + user.id;
                  }
                });
            } else {
              swal
                .fire({
                  title: resMessage,
                  icon: "warning",
                  confirmButtonText: "Lihat user dimenu admin",
                  cancelButtonText: "Kembali",
                  showCancelButton: true,
                })
                .then((result) => {
                  if (result.isConfirmed) {
                    window.location.href =
                      "/site-management/administrator/edit/" + user.id;
                  }
                });
            }
          }
        } else {
          swal
            .fire({
              title: "Terjadi kesalahan",
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      })
      .catch((err) => {})
      .finally(() => {
        this.setState({ loading: false });
      });
    console.log(user);
  }

  handleDeleteAction(e) {
    e.preventDefault();
    const idx = this.state.user_id;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          //const data = { id_role: idx }
          const dataFormPost = new FormData();
          dataFormPost.append("userid", idx);

          axios
            .post(
              process.env.REACT_APP_BASE_API_URI +
                "/user/delete_pelatihan_user",
              dataFormPost,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                let icon = "success";
                if (res.data.result.StatusCode == "404") {
                  icon = "warning";
                }
                swal
                  .fire({
                    title: messagex,
                    icon: icon,
                    confirmButtonText: "Ok",
                    allowOutsideClick: false,
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      window.location = "/site-management/user-dts";
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmationButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            });
        }
      });
  }

  handleChangeStatusPekerjaanAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["status_pekerjaan"] = "";
    this.setState({ errors });

    let statusBekerja = false;
    if (selectedOption.label == "Bekerja") {
      statusBekerja = true;
    }

    this.setState({
      status_pekerjaan_id: selectedOption.value,
      status_pekerjaan_nama: selectedOption.label,
      statusBekerja,
      valStatusPekerjaan: {
        value: selectedOption.value,
        label: selectedOption.label,
      },
    });
  };

  handleChangePekerjaanAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["pekerjaan"] = "";
    this.setState({ errors });

    this.setState({
      pekerjaan_id: selectedOption.value,
      pekerjaan_nama: selectedOption.label,
      valPekerjaan: {
        value: selectedOption.value,
        label: selectedOption.label,
      },
    });
  };

  handleChangeInstitusiAction(e) {
    const errors = this.state.errors;
    errors["institusi"] = "";

    const institusi = e.currentTarget.value;
    this.setState({
      errors,
      institusi,
    });
  }

  handleChangeHubunganAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["hubungan"] = "";
    this.setState({ errors });

    this.setState({
      hubungan: selectedOption.value,
      valHubungan: { value: selectedOption.value, label: selectedOption.label },
    });
  };

  getNilaiPeserta(idUser, idPelatihan) {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/rekappendaftaran/list-user-silabus",
        {
          pelatihan_id: idPelatihan,
          user_id: idUser,
        },
        this.configs,
      )
      .then((result) => {
        this.setState({ pratinjau: result.data.result }, () => {
          this.pratinjauTriggerRef.current.click();
        });
      })
      .catch((err) => {
        let statux = err.response.data.result.Status;
        let messagex = err.response.data.result.Message;
        swal
          .fire({
            title:
              messagex ?? "Terjadi kesalahan saat berkomunikasi dengan server.",
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
            }
          });
      });
  }

  render() {
    return (
      <div>
        <a
          href="#"
          hidden
          data-bs-toggle="modal"
          data-bs-target="#pratinjau-nilai"
          ref={this.pratinjauTriggerRef}
        ></a>

        <div className="modal fade" tabindex="-1" id="pratinjau-nilai">
          <div className="modal-dialog modal-md">
            <div className="modal-content">
              <div className="modal-header pe-0 pb-0">
                <div className="row w-100">
                  <div className="col-12 d-flex w-100 justify-content-between">
                    <h5 className="modal-title">Nilai Peserta</h5>
                    <div
                      className="btn btn-icon btn-sm btn-active-light-primary"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                    >
                      <span className="svg-icon svg-icon-2x">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                          fill="none"
                        >
                          <rect
                            opacity="0.5"
                            x="6"
                            y="17.3137"
                            width="16"
                            height="2"
                            rx="1"
                            transform="rotate(-45 6 17.3137)"
                            fill="currentColor"
                          />
                          <rect
                            x="7.41422"
                            y="6"
                            width="16"
                            height="2"
                            rx="1"
                            transform="rotate(45 7.41422 6)"
                            fill="currentColor"
                          />
                        </svg>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              {this.state.pratinjau && (
                <div className="modal-body">
                  <div className="card">
                    <div className="row justify-content-start">
                      <div className="col-12">
                        <h4 className="fw-bolder">
                          {capitalWord(this.state.pratinjau[0]?.nama_silabus)}
                        </h4>
                        <span className="text-muted">Nilai Peserta</span>
                      </div>
                      <div className="col-12">
                        <ul className="list-group">
                          {this.state.pratinjau.map((elem) => (
                            <div
                              className="d-flex align-items-center bg-light-primary rounded p-5 mb-2"
                              key={`detail_${elem.silabus_header_id}`}
                            >
                              <span className="svg-icon svg-icon-primary me-5">
                                <span className="svg-icon svg-icon-1">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <path
                                      opacity="0.3"
                                      d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                                      fill="currentColor"
                                    ></path>
                                    <path
                                      d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                                      fill="currentColor"
                                    ></path>
                                  </svg>
                                </span>
                              </span>
                              <div className="flex-grow-1 me-2">
                                <a
                                  href="#"
                                  className="fw-bold text-gray-800 text-hover-primary fs-6"
                                >
                                  {elem.silabus_detail ?? "Tidak ada data."}
                                </a>
                              </div>
                              <span className="fw-bold text-primary py-1">
                                {elem.nilai ?? ""}
                              </span>
                            </div>
                          ))}
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              )}

              <div className="modal-footer">
                <button
                  className="btn btn-light btn-sm"
                  data-bs-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>

        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  User DTS
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.back}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>

                <a
                  href="#"
                  title="Hapus"
                  onClick={this.handleDelete}
                  className="btn btn-sm btn-danger btn-active-light-info"
                >
                  <i className="bi bi-trash-fill text-white"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Detail User DTS
                          </h1>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="mb-7">
                          <div className="d-flex flex-wrap py-3">
                            <div className="flex-grow-1 mb-3">
                              <div className="d-flex flex-wrap flex-sm-nowrap mb-3">
                                <div className="me-7 mb-4">
                                  {this.state.image == null ? (
                                    <span className="symbol symbol-100px symbol-lg-150px me-6">
                                      <span className="symbol-label bg-light-primary">
                                        <span className="svg-icon svg-icon-1 svg-icon-primary text-center">
                                          <small style={{ fontSize: "2em" }}>
                                            Belum
                                            <br />
                                            Ada
                                            <br />
                                            Gambar
                                          </small>
                                        </span>
                                      </span>
                                    </span>
                                  ) : (
                                    <div className="symbol symbol-100px symbol-lg-150px symbol-fixed position-relative">
                                      <img
                                        src={
                                          process.env.REACT_APP_BASE_API_URI +
                                          "/download/get-file?path=" +
                                          this.state.image
                                        }
                                      />
                                    </div>
                                  )}
                                </div>
                                <div className="flex-grow-1">
                                  <div className="d-flex justify-content-between align-items-start flex-wrap mb-2">
                                    <div className="d-flex flex-column">
                                      <div className="d-flex align-items-center mb-2">
                                        <h2 className="fw-bolder me-1 mb-0">
                                          {this.state.nama
                                            ? this.state.nama
                                            : "-"}
                                        </h2>
                                      </div>
                                      <div className="d-flex flex-wrap fw-bold fs-6 mb-4 pe-2">
                                        <span className="d-flex align-items-center fs-7 text-muted fw-semibold me-5 mb-3">
                                          <span className="svg-icon svg-icon-4 me-1">
                                            <i className="bi bi-person-vcard me-1"></i>
                                            {this.state.nik
                                              ? this.state.nik
                                              : "-"}
                                          </span>
                                        </span>
                                        <span className="d-flex align-items-center fs-7 fw-semibold text-muted me-5 mb-3">
                                          <span className="svg-icon svg-icon-4 me-1">
                                            <i className="bi bi-telephone me-1"></i>
                                            {this.state.nomor_handphone
                                              ? this.state.nomor_handphone
                                              : "-"}
                                          </span>
                                        </span>
                                        <span className="d-flex align-items-center fs-7 text-muted fw-semibold me-5 mb-3">
                                          <span className="svg-icon svg-icon-4 me-1">
                                            <i className="bi bi-envelope-at me-1"></i>
                                            {this.state.email
                                              ? this.state.email
                                              : "-"}
                                          </span>
                                        </span>
                                      </div>
                                      <div className="d-flex flex-column flex-grow-1 pe-8">
                                        <div className="d-flex flex-wrap">
                                          <div className="card border rounded icon-category icon-category-sm p-5 mb-3 me-3">
                                            <div className="row align-items-center mx-n3">
                                              <div className="col-auto px-3">
                                                <div className="icon-h-p">
                                                  <i className="bi bi-book text-primary fa-2x"></i>
                                                </div>
                                              </div>
                                              <div className="col px-3">
                                                <div className="card-body p-0">
                                                  <h6 className="fs-8 mb-0 text-muted line-clamp-1">
                                                    Pelatihan
                                                  </h6>
                                                  <h6 className="mb-0 line-clamp-1 hover-clamp-off">
                                                    {this.state.totalRows}{" "}
                                                    Pelatihan
                                                  </h6>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div className="card border rounded icon-category icon-category-sm p-5 mb-3 me-3">
                                            <div className="row align-items-center mx-n3">
                                              <div className="col-auto px-3">
                                                <div className="icon-h-p">
                                                  <i className="bi bi-patch-check text-success fa-2x"></i>
                                                </div>
                                              </div>
                                              <div className="col px-3">
                                                <div className="card-body p-0">
                                                  <h6 className="fs-8 mb-0 text-muted line-clamp-1">
                                                    Lulus
                                                  </h6>
                                                  <h6 className="mb-0 line-clamp-1 hover-clamp-off">
                                                    {this.state.totalLulus}{" "}
                                                    Pelatihan
                                                  </h6>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div className="card border rounded icon-category icon-category-sm p-5 mb-3 me-3">
                                            <div className="row align-items-center mx-n3">
                                              <div className="col-auto px-3">
                                                <div className="icon-h-p">
                                                  <i className="bi bi-clipboard-x text-danger fa-2x"></i>
                                                </div>
                                              </div>
                                              <div className="col px-3">
                                                <div className="card-body p-0">
                                                  <h6 className="fs-8 mb-0 text-muted line-clamp-1">
                                                    Tidak Lulus
                                                  </h6>
                                                  <h6 className="mb-0 line-clamp-1 hover-clamp-off">
                                                    {this.state.totalTidakLulus}{" "}
                                                    Pelatihan
                                                  </h6>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div className="d-flex my-4">
                                      <ButtonSetAdmin
                                        isAdmin={this.state.isadmin}
                                        userId={this.state.datax_user.id}
                                        userNama={this.state.datax_user.nama}
                                        onRequestStart={() => {
                                          console.log("start req");
                                          this.setState({ loading: true });
                                        }}
                                        axiosConfig={this.configs}
                                        onSuccess={() => {
                                          this.setState({
                                            isadmin: !this.state.isadmin,
                                          });
                                        }}
                                      ></ButtonSetAdmin>
                                      {/* <a
                                                                                href="#"
                                                                                title="Set Admin"
                                                                                className="btn btn-sm bg-info text-white"
                                                                                onClick={() => {
                                                                                    this.handleMakeAdmin()
                                                                                }}
                                                                            >
                                                                                <i className="bi bi-person-fill-add fs-5 text-white me-1"></i>
                                                                                Set Admin
                                                                            </a> */}
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div>
                          <div className="d-flex border-bottom p-0">
                            <ul
                              className="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap"
                              style={{
                                overflowX: "auto",
                                overflowY: "hidden",
                                display: "flex",
                                whiteSpace: "nowrap",
                                marginBottom: 0,
                              }}
                            >
                              <li className="nav-item">
                                <a
                                  className="nav-link text-dark text-active-primary active"
                                  data-bs-toggle="tab"
                                  href="#"
                                  onClick={() => {
                                    this.setState({ selected: 1, main: 1 });
                                  }}
                                >
                                  <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                    Data Pokok
                                  </span>
                                </a>
                              </li>
                              <li className="nav-item">
                                <a
                                  href="#"
                                  className="nav-link text-dark text-active-primary false"
                                  data-bs-toggle="tab"
                                  onClick={() => {
                                    this.setState({ selected: 2, main: 1 });
                                  }}
                                >
                                  <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                    Riwayat Pelatihan
                                  </span>
                                </a>
                              </li>
                            </ul>
                          </div>
                          <div className="tab-content" id="detail-account-tab">
                            {this.state.main == 1 ? (
                              this.state.selected == 1 ? (
                                <div
                                  className="tab-pane fade show active"
                                  id="nav-profile"
                                  role="tabpanel"
                                  aria-labelledby="nav-profile"
                                >
                                  <form
                                    className="form"
                                    action="#"
                                    onSubmit={this.handleSubmit}
                                  >
                                    <div className="row mt-7 pb-7">
                                      <h2 className="fs-5 pt-7 text-muted mb-3">
                                        Data Diri
                                      </h2>
                                      <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                        <label className="form-label required">
                                          Nama Lengkap
                                        </label>
                                        <input
                                          disabled={true}
                                          className="form-control form-control-sm"
                                          placeholder="Isi Nama Sesuai KTP"
                                          name="nama"
                                          defaultValue={this.state.nama}
                                        />
                                        <span style={{ color: "red" }}>
                                          {this.state.errors["nama"]}
                                        </span>
                                      </div>
                                      <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                        <label className="form-label required">
                                          Email
                                        </label>
                                        <input
                                          disabled={true}
                                          autoComplete="off"
                                          className="form-control form-control-sm"
                                          placeholder="Isi Email"
                                          name="email"
                                          defaultValue={this.state.email}
                                        />
                                        <span style={{ color: "red" }}>
                                          {this.state.errors["email"]}
                                        </span>
                                      </div>
                                      <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                        <label className="form-label required">
                                          NIK
                                        </label>
                                        <input
                                          disabled={true}
                                          className="form-control form-control-sm"
                                          placeholder="Isi NIK Sesuai KTP"
                                          type="number"
                                          name="nik"
                                          defaultValue={this.state.nik}
                                        />
                                        <span style={{ color: "red" }}>
                                          {this.state.errors["nik"]}
                                        </span>
                                      </div>
                                      <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                        <label className="form-label required">
                                          Jenis Kelamin
                                        </label>
                                        <Select
                                          name="kelamin"
                                          placeholder="Pilih Jenis Kelamin"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.state.datax
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm selectpicker p-0"
                                          options={this.option_kelamin}
                                          value={this.state.valKelamin}
                                          onChange={this.handleChangeKelamin}
                                        />
                                        <span style={{ color: "red" }}>
                                          {this.state.errors["kelamin"]}
                                        </span>
                                      </div>
                                      <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                        <label className="form-label required">
                                          Nomor Handphone
                                        </label>
                                        <input
                                          autoComplete="off"
                                          className="form-control form-control-sm"
                                          disabled={true}
                                          placeholder="Nomor Handphone"
                                          name="handphone"
                                          defaultValue={
                                            this.state.nomor_handphone
                                          }
                                        />
                                        <span style={{ color: "red" }}>
                                          {this.state.errors["handphone"]}
                                        </span>
                                      </div>
                                      {/* <div className="col-md-6 col-lg-6 mt-3 mb-3">
																				<label className="form-label required">
																					Pendidikan
																				</label>
																				<Select
																					name="pendidikan"
																					placeholder="Pilih Pendidikan"
																					noOptionsMessage={({ inputValue }) =>
																						!inputValue
																							? this.state.datax
																							: "Data tidak tersedia"
																					}
																					className="form-select-sm selectpicker p-0"
																					options={this.state.option_pendidikan}
																					value={this.state.valPendidikan}
																					onChange={this.handleChangePendidikan}
																				/>
																				<span style={{ color: "red" }}>
																					{this.state.errors["pendidikan"]}
																				</span>
																			</div> */}
                                      {/* <div className="col-md-6 col-lg-6 mt-3 mb-3">
																				<label className="form-label required">
																					Tempat Lahir
																				</label>
																				<input
																					className="form-control form-control-sm"
																					placeholder="Isi Tempat Lahir"
																					name="tempat_lahir"
																					value={this.state.tempat_lahir}
																					onChange={
																						this.handleChangeTempatLahir
																					}
																				/>
																				<span style={{ color: "red" }}>
																					{this.state.errors["tempat_lahir"]}
																				</span>
																			</div> */}
                                      <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                        <label className="form-label required">
                                          Tanggal Lahir
                                        </label>
                                        <div className="pl-7">
                                          <input
                                            id="tanggal_lahir"
                                            name="tanggal_lahir"
                                            className="form-control form-control-sm"
                                            //placeholder='dd/mm/yy'
                                            defaultValue={
                                              this.state.tanggal_lahir
                                            }
                                            onBlur={
                                              this.handleChangeTanggalLahir
                                            }
                                            onChange={
                                              this.handleChangeTanggalLahir
                                            }
                                          />
                                        </div>
                                        <span style={{ color: "red" }}>
                                          {this.state.errors["tanggal_lahir"]}
                                        </span>
                                      </div>
                                      <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                        <label className="form-label">
                                          Pas Photo
                                        </label>
                                        <input
                                          type="file"
                                          className="form-control form-control-sm font-size-h4"
                                          name="logo_header"
                                          id="thumbnail"
                                          accept=".png,.jpg,.jpeg,.svg"
                                          onChange={this.handleChangeFoto}
                                        />
                                      </div>

                                      <div>
                                        <div className="my-8 border-top mx-0"></div>
                                      </div>
                                      <h2 className="fs-5 text-muted mb-3">
                                        Domisili
                                      </h2>
                                      {/* <div className="col-md-12 col-lg-12 mt-3 mb-3">
																				<label className="form-label required">
																					Alamat(Sesuai KTP)
																				</label>
																				<input
																					autoComplete="off"
																					className="form-control form-control-sm"
																					placeholder="Masukkan Alamat KTP"
																					name="alamat"
																					value={this.state.address || ""}
																					onChange={this.handleChangeAlamat}
																				/>
																				<span style={{ color: "red" }}>
																					{this.state.errors["alamat"]}
																				</span>
																			</div> */}
                                      <div className="row">
                                        <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                          <label className="form-label required">
                                            Provinsi
                                          </label>
                                          <Select
                                            name="provinsi"
                                            placeholder="Pilih Provinsi"
                                            noOptionsMessage={({
                                              inputValue,
                                            }) =>
                                              !inputValue
                                                ? this.state.datax
                                                : "Data tidak tersedia"
                                            }
                                            className="form-select-sm selectpicker p-0"
                                            options={
                                              this.state.datax_provinsi_1
                                            }
                                            value={this.state.valProvinsi_1}
                                            onChange={
                                              this.handleChangeProvinsi_1
                                            }
                                          />
                                          <span style={{ color: "red" }}>
                                            {this.state.errors["provinsi"]}
                                          </span>
                                        </div>
                                        <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                          <label className="form-label required">
                                            Kabupaten/Kota
                                          </label>
                                          <Select
                                            name="kabupaten"
                                            placeholder="Pilih Kabupaten"
                                            noOptionsMessage={({
                                              inputValue,
                                            }) =>
                                              !inputValue
                                                ? this.state.datax
                                                : "Data tidak tersedia"
                                            }
                                            className="form-select-sm selectpicker p-0"
                                            options={
                                              this.state.datax_kabupaten_1
                                            }
                                            value={this.state.valKabupaten_1}
                                            onChange={
                                              this.handleChangeKabupaten_1
                                            }
                                          />
                                          <span style={{ color: "red" }}>
                                            {this.state.errors["kota"]}
                                          </span>
                                        </div>
                                        <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                          <label className="form-label required">
                                            Kecamatan
                                          </label>
                                          <Select
                                            name="kecamatan"
                                            placeholder="Pilih Kecamatan"
                                            noOptionsMessage={({
                                              inputValue,
                                            }) =>
                                              !inputValue
                                                ? this.state.datax
                                                : "Data tidak tersedia"
                                            }
                                            className="form-select-sm selectpicker p-0"
                                            options={
                                              this.state.datax_kecamatan_1
                                            }
                                            value={this.state.valKecamatan_1}
                                            onChange={
                                              this.handleChangeKecamatan_1
                                            }
                                          />
                                          <span style={{ color: "red" }}>
                                            {this.state.errors["kecamatan"]}
                                          </span>
                                        </div>
                                        <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                          <label className="form-label required">
                                            Kelurahan/Desa
                                          </label>
                                          <Select
                                            name="desa"
                                            placeholder="Pilih Desa"
                                            noOptionsMessage={({
                                              inputValue,
                                            }) =>
                                              !inputValue
                                                ? this.state.datax
                                                : "Data tidak tersedia"
                                            }
                                            className="form-select-sm selectpicker p-0"
                                            options={this.state.datax_desa_1}
                                            value={this.state.valDesa_1}
                                            onChange={this.handleChangeDesa_1}
                                          />
                                          <span style={{ color: "red" }}>
                                            {this.state.errors["kelurahan"]}
                                          </span>
                                        </div>
                                        {/* <div className="col-md-12 col-lg-12 mt-2">
                                                                            <label className="form-label">Kode Pos</label>
                                                                            <input autoComplete='off' className="form-control form-control-sm" placeholder="Kode Pos" name="kode_pos" value={this.state.kode_pos} onChange={this.handleChangeKodePos} />
                                                                            <span style={{ color: "red" }}>{this.state.errors["kode_pos"]}</span>
                                                                        </div> */}
                                      </div>
                                      {/* <div className="my-5">
                                                                                        <h3 className="mb-1">Alamat Domisili</h3>
                                                                                        <div className="col-md-12 col-lg-12 mt-2">
                                                                                            <label className="form-label">Alamat</label>
                                                                                            <input autoComplete='off' className="form-control form-control-sm" placeholder="Masukkan Alamat KTP" name="alamat" value={this.state.alamat} onChange={this.handleChangeAlamat} />
                                                                                            <span style={{ color: "red" }}>{this.state.errors["alamat"]}</span>
                                                                                        </div>
                                                                                        <div className="row">
                                                                                            <div className="col-md-6 col-lg-6 mt-2">
                                                                                                <label className="form-label">Provinsi</label>
                                                                                                <Select
                                                                                                    name="provinsi"
                                                                                                    placeholder="Pilih Provinsi"
                                                                                                    noOptionsMessage={({ inputValue }) => !inputValue ? this.state.datax : "Data tidak tersedia"}
                                                                                                    className="form-select-sm selectpicker p-0"
                                                                                                    options={this.state.datax_provinsi_2}
                                                                                                    value={this.state.valProvinsi_2}
                                                                                                    onChange={this.handleChangeProvinsi_2}
                                                                                                />
                                                                                                <span style={{ color: "red" }}>{this.state.errors["provinsi"]}</span>
                                                                                            </div>
                                                                                            <div className="col-md-6 col-lg-6 mt-2">
                                                                                                <label className="form-label">Kabupaten/Kota</label>
                                                                                                <Select
                                                                                                    name="pendidikan"
                                                                                                    placeholder="Pilih Pendidikan"
                                                                                                    noOptionsMessage={({ inputValue }) => !inputValue ? this.state.datax : "Data tidak tersedia"}
                                                                                                    className="form-select-sm selectpicker p-0"
                                                                                                    options={this.option_pendidikan}
                                                                                                    value={this.state.valPendidikan}
                                                                                                    onChange={this.handleChangePendidikan}
                                                                                                />
                                                                                                <span style={{ color: "red" }}>{this.state.errors["kota"]}</span>
                                                                                            </div>
                                                                                            <div className="col-md-6 col-lg-6 mt-2">
                                                                                                <label className="form-label">Kecamatan</label>
                                                                                                <Select
                                                                                                    name="pendidikan"
                                                                                                    placeholder="Pilih Pendidikan"
                                                                                                    noOptionsMessage={({ inputValue }) => !inputValue ? this.state.datax : "Data tidak tersedia"}
                                                                                                    className="form-select-sm selectpicker p-0"
                                                                                                    options={this.option_pendidikan}
                                                                                                    value={this.state.valPendidikan}
                                                                                                    onChange={this.handleChangePendidikan}
                                                                                                />
                                                                                                <span style={{ color: "red" }}>{this.state.errors["kecamatan"]}</span>
                                                                                            </div>
                                                                                            <div className="col-md-6 col-lg-6 mt-2">
                                                                                                <label className="form-label">Kelurahan/Desa</label>
                                                                                                <Select
                                                                                                    name="pendidikan"
                                                                                                    placeholder="Pilih Pendidikan"
                                                                                                    noOptionsMessage={({ inputValue }) => !inputValue ? this.state.datax : "Data tidak tersedia"}
                                                                                                    className="form-select-sm selectpicker p-0"
                                                                                                    options={this.option_pendidikan}
                                                                                                    value={this.state.valPendidikan}
                                                                                                    onChange={this.handleChangePendidikan}
                                                                                                />
                                                                                                <span style={{ color: "red" }}>{this.state.errors["kelurahan"]}</span>
                                                                                            </div>
                                                                                            <div className="col-md-12 col-lg-12 mt-2">
                                                                                                <label className="form-label">Kode Pos</label>
                                                                                                <input autoComplete='off' className="form-control form-control-sm" placeholder="Kode Pos" name="kode_pos" value={this.state.kode_pos} onChange={this.handleChangeKodePos} />
                                                                                                <span style={{ color: "red" }}>{this.state.errors["kode_pos"]}</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="my-5">
                                                                                        <h3 className="mb-1">Upload Berkas Pribadi</h3>
                                                                                        <div className="col-md-12 col-lg-12 mt-2">
                                                                                            <label className="form-label">KTP</label>
                                                                                            <input
                                                                                                className="form-control form-control-sm"
                                                                                                placeholder="Kode Pos" name="kode_pos"
                                                                                                value={this.state.kode_pos}
                                                                                                onChange={this.handleChangeKodePos}
                                                                                                type="file"
                                                                                            />
                                                                                            <span style={{ color: "red" }}>{this.state.errors["kode_pos"]}</span>
                                                                                        </div>
                                                                                        <div className="col-md-12 col-lg-12 mt-2">
                                                                                            <label className="form-label">Ijazah</label>
                                                                                            <input
                                                                                                className="form-control form-control-sm"
                                                                                                placeholder="Kode Pos" name="kode_pos"
                                                                                                value={this.state.kode_pos}
                                                                                                onChange={this.handleChangeKodePos}
                                                                                                type="file"
                                                                                            />
                                                                                            <span style={{ color: "red" }}>{this.state.errors["kode_pos"]}</span>
                                                                                        </div>
                                                                                    </div> */}

                                      <div>
                                        <div className="my-8 border-top mx-0"></div>
                                      </div>
                                      <h2 className="fs-5 text-muted mb-3">
                                        Pekerjaan
                                      </h2>
                                      <div className="row">
                                        <div className="col-md-12 col-lg-12 mt-3 mb-3">
                                          <label className="form-label required">
                                            Status
                                          </label>
                                          <Select
                                            name="status"
                                            placeholder="Pilih Status Pekerjaan"
                                            noOptionsMessage={({
                                              inputValue,
                                            }) =>
                                              !inputValue
                                                ? this.state.datax
                                                : "Data tidak tersedia"
                                            }
                                            className="form-select-sm selectpicker p-0"
                                            options={
                                              this.state.option_status_pekerjaan
                                            }
                                            value={
                                              this.state.valStatusPekerjaan
                                            }
                                            onChange={
                                              this.handleChangeStatusPekerjaan
                                            }
                                          />
                                          <span style={{ color: "red" }}>
                                            {
                                              this.state.errors[
                                                "status_pekerjaan"
                                              ]
                                            }
                                          </span>
                                        </div>
                                        {this.state.statusBekerja ? (
                                          <div className="row col-12">
                                            <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                              <label className="form-label required">
                                                Pekerjaan
                                              </label>
                                              <Select
                                                name="status"
                                                placeholder="Pilih Status Pekerjaan"
                                                noOptionsMessage={({
                                                  inputValue,
                                                }) =>
                                                  !inputValue
                                                    ? this.state.datax
                                                    : "Data tidak tersedia"
                                                }
                                                className="form-select-sm selectpicker p-0"
                                                options={
                                                  this.state
                                                    .option_list_pekerjaan
                                                }
                                                value={this.state.valPekerjaan}
                                                onChange={
                                                  this.handleChangePekerjaan
                                                }
                                              />
                                              <span style={{ color: "red" }}>
                                                {this.state.errors["pekerjaan"]}
                                              </span>
                                            </div>
                                            <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                              <label className="form-label required">
                                                Institusi Tempat Bekerja
                                              </label>
                                              <input
                                                className="form-control form-control-sm"
                                                placeholder="institusi"
                                                name="institusi"
                                                value={
                                                  this.state.institusi || ""
                                                }
                                                onChange={
                                                  this.handleChangeInstitusi
                                                }
                                              />
                                              <span style={{ color: "red" }}>
                                                {this.state.errors["institusi"]}
                                              </span>
                                            </div>
                                          </div>
                                        ) : (
                                          ""
                                        )}
                                      </div>

                                      {/* <div>
																				<div className="my-8 border-top mx-0"></div>
																			</div>
																			<h2 className="fs-5 text-muted mb-3">
																				Kontak Darurat
																			</h2>
																			<div className="row">
																				<div className="col-md-4 col-lg-4 mt-3 mb-3">
																					<label className="form-label required">
																						Nama Kontak Darurat
																					</label>
																					<input
																						className="form-control form-control-sm"
																						placeholder="Isi Kontak Darurat"
																						name="kontak_darurat"
																						value={
																							this.state.nama_kontak_darurat ||
																							""
																						}
																						onChange={
																							this.handleChangeKontakDarurat
																						}
																					/>
																					<span style={{ color: "red" }}>
																						{
																							this.state.errors[
																							"kontak_darurat"
																							]
																						}
																					</span>
																				</div>
																				<div className="col-md-4 col-lg-4 mt-3 mb-3">
																					<label className="form-label required">
																						Nomor Kontak Darurat
																					</label>
																					<input
																						autoComplete="off"
																						className="form-control form-control-sm"
																						placeholder="Nomor Kontak Darurat"
																						type="number"
																						name="handphone_darurat"
																						value={
																							this.state
																								.nomor_handphone_darurat || ""
																						}
																						onChange={
																							this.handleChangeHandphoneDarurat
																						}
																					/>
																					<span style={{ color: "red" }}>
																						{
																							this.state.errors[
																							"handphone_darurat"
																							]
																						}
																					</span>
																				</div>
																				<div className="col-md-4 col-lg-4 mt-3 mb-3">
																					<label className="form-label required">
																						Hubungan
																					</label>
																					<Select
																						name="status"
																						placeholder="Pilih Hubungan Kontak Darurat"
																						noOptionsMessage={({
																							inputValue,
																						}) =>
																							!inputValue
																								? this.state.datax
																								: "Data tidak tersedia"
																						}
																						className="form-select-sm selectpicker p-0"
																						options={this.option_kontak_darurat}
																						value={this.state.valHubungan}
																						onChange={this.handleChangeHubungan}
																					/>
																					<span style={{ color: "red" }}>
																						{this.state.errors["hubungan"]}
																					</span>
																				</div>
																			</div> */}

                                      <div>
                                        <div className="my-8 border-top mx-0"></div>
                                      </div>
                                      <h2 className="fs-5 text-muted mb-3">
                                        Password
                                      </h2>
                                      <div className="col-md-12 col-lg-12 mt-3 mb-3">
                                        <label className="form-label">
                                          Password Baru
                                        </label>
                                        <div className="input-group mb-3">
                                          <input
                                            autoComplete="new-password"
                                            className="form-control form-control-sm"
                                            placeholder="Kata Sandi Baru"
                                            name="password"
                                            type={this.state.typePassword}
                                            value={this.state.password}
                                            onChange={this.handleChangePassword}
                                          />
                                          <span
                                            title="show/hide password"
                                            style={{ cursor: "pointer" }}
                                            className="input-group-text"
                                            id="basic-addon2"
                                            onClick={this.showPassword}
                                          >
                                            <i
                                              className={this.state.classEye}
                                            ></i>
                                          </span>
                                        </div>
                                        <span style={{ color: "red" }}>
                                          {this.state.errors["password"]}
                                        </span>
                                      </div>
                                      <div className="col-md-12 col-lg-12 mt-3 mb-3">
                                        <label className="form-label">
                                          Konfirmasi Password
                                        </label>
                                        <div className="input-group mb-3">
                                          <input
                                            className="form-control form-control-sm"
                                            placeholder="Ulangi Password"
                                            name="confirmPassword"
                                            type={
                                              this.state.typeConfirmPassword
                                            }
                                            value={this.state.confirmPassword}
                                            onChange={
                                              this.handleChangeConfirmPassword
                                            }
                                          />
                                          <span
                                            title="show/hide password"
                                            style={{ cursor: "pointer" }}
                                            className="input-group-text"
                                            id="basic-addon2"
                                            onClick={this.showConfirmPassword}
                                          >
                                            <i
                                              className={
                                                this.state.classConfirmEye
                                              }
                                            ></i>
                                          </span>
                                        </div>
                                        <span style={{ color: "red" }}>
                                          {this.state.errors["password"]}
                                        </span>
                                      </div>
                                      <div className="text-center my-7">
                                        <a
                                          href="#"
                                          className="btn btn-md btn-light me-2"
                                          title="kembali"
                                          onClick={this.back}
                                        >
                                          {" "}
                                          Batal{" "}
                                        </a>
                                        <button
                                          type="submit"
                                          className="btn btn-primary btn-md"
                                          disabled={this.state.isLoading}
                                        >
                                          {this.state.isLoading ? (
                                            <>
                                              <span
                                                className="spinner-border spinner-border-sm me-2"
                                                role="status"
                                                aria-hidden="true"
                                              ></span>
                                              <span className="sr-only">
                                                Loading...
                                              </span>
                                              Loading...
                                            </>
                                          ) : (
                                            <>
                                              <i className="fa fa-paper-plane me-1"></i>
                                              Simpan
                                            </>
                                          )}
                                        </button>
                                      </div>
                                    </div>
                                  </form>
                                </div>
                              ) : (
                                <div
                                  className="tab-pane fade show active"
                                  id="nav-profile"
                                  role="tabpanel"
                                  aria-labelledby="nav-profile"
                                >
                                  <form
                                    className="form"
                                    action="#"
                                    onSubmit={this.handleSubmit}
                                  >
                                    <div className="row pb-7">
                                      <div className="card-toolbar">
                                        <div className="d-flex align-items-center position-relative float-end mt-7 mb-5 me-2">
                                          <span className="svg-icon svg-icon-1 position-absolute ms-6">
                                            <svg
                                              width="24"
                                              height="24"
                                              viewBox="0 0 24 24"
                                              fill="none"
                                              xmlns="http://www.w3.org/2000/svg"
                                              className="mh-50px"
                                            >
                                              <rect
                                                opacity="0.5"
                                                x="17.0365"
                                                y="15.1223"
                                                width="8.15546"
                                                height="2"
                                                rx="1"
                                                transform="rotate(45 17.0365 15.1223)"
                                                fill="currentColor"
                                              ></rect>
                                              <path
                                                d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                                fill="currentColor"
                                              ></path>
                                            </svg>
                                          </span>
                                          <input
                                            type="text"
                                            data-kt-user-table-filter="search"
                                            className="form-control form-control-sm form-control-solid w-250px ps-14"
                                            placeholder="Cari Pelatihan"
                                            value={this.state.searchText}
                                            onKeyPress={this.handleKeyPress}
                                            onChange={this.handleChangeSearch}
                                          />
                                        </div>
                                      </div>
                                      <DataTable
                                        columns={this.columns}
                                        data={this.state.datax}
                                        progressPending={this.state.loading}
                                        highlightOnHover
                                        pointerOnHover
                                        pagination
                                        paginationServer
                                        paginationTotalRows={
                                          this.state.totalRows
                                        }
                                        paginationDefaultPage={
                                          this.state.currentPage
                                        }
                                        onChangeRowsPerPage={(
                                          currentRowsPerPage,
                                          currentPage,
                                        ) => {
                                          this.setState(
                                            {
                                              from_pagination_change: 1,
                                            },
                                            () => {
                                              this.handlePerRowsChange(
                                                currentRowsPerPage,
                                                currentPage,
                                              );
                                            },
                                          );
                                        }}
                                        onChangePage={(page, totalRows) => {
                                          this.setState(
                                            {
                                              from_pagination_change: 0,
                                            },
                                            () => {
                                              this.handlePageChange(
                                                page,
                                                totalRows,
                                              );
                                            },
                                          );
                                        }}
                                        customStyles={this.customStyles}
                                        persistTableHead={true}
                                        onSort={this.handleSort}
                                        noDataComponent={
                                          <div className="mt-5">
                                            Tidak Ada Data Pelatihan
                                          </div>
                                        }
                                        sortServer
                                      />
                                    </div>
                                  </form>
                                </div>
                              )
                            ) : this.state.selected_data == 1 ? (
                              <div
                                className="tab-pane fade show active"
                                id="nav-profile"
                                role="tabpanel"
                                aria-labelledby="nav-profile"
                              >
                                <div className="row mt-7 pb-7">
                                  <h2 className="fs-5 text-muted mb-3">
                                    Detail Pelatihan
                                  </h2>
                                  <div className="row">
                                    <div className="col-12 mt-5 mb-6">
                                      <p className="text-dark fs-7 mb-0">
                                        {this.state.detail_akademi}
                                      </p>
                                      <h1 className="align-items-center text-dark fw-bolder my-1 fs-4">
                                        {this.state.slug_pelatian_id}-{" "}
                                        {this.state.detail_nama_pelatihan}
                                      </h1>
                                      <span className="text-muted fw-semibold fs-7 mb-0">
                                        {this.state.detail_tema}
                                      </span>
                                    </div>

                                    <div className="col-md-4 col-lg-4 mt-3 mb-4">
                                      <label className="form-label">
                                        Penyelenggara
                                      </label>
                                      <div>
                                        <strong>
                                          {this.state.detail_penyelenggara}
                                        </strong>
                                      </div>
                                    </div>

                                    <div className="col-md-4 col-lg-4 mt-3 mb-4">
                                      <label className="form-label">
                                        Mitra
                                      </label>
                                      <div>
                                        <strong>
                                          {this.state.datax.nama_mitra
                                            ? this.state.datax.nama_mitra
                                            : "Swakelola"}
                                        </strong>
                                      </div>
                                    </div>

                                    <div className="col-md-4 col-lg-4 mt-3 mb-4">
                                      <label className="form-label">
                                        Metode
                                      </label>
                                      <div>
                                        <strong>
                                          {this.state.metode_pelatihan}
                                        </strong>
                                      </div>
                                    </div>

                                    {this.state.metode_pelatihan != "Online" ? (
                                      <div className="col-md-12 col-lg-12 mt-3 mb-4">
                                        <label className="form-label">
                                          Lokasi Pelatihan
                                        </label>
                                        <div>
                                          <strong>
                                            {this.state.alamat_detail},{" "}
                                            {this.state.kabupaten_detail},{" "}
                                            {this.state.provinsi_detail}
                                          </strong>
                                        </div>
                                      </div>
                                    ) : (
                                      ""
                                    )}

                                    <div className="col-md-6 col-lg-6 mt-3 mb-4">
                                      <label className="form-label">
                                        Tgl. Pelatihan
                                      </label>
                                      <div>
                                        <strong>
                                          {this.state.tanggal_pelatihan_detail}
                                        </strong>
                                      </div>
                                    </div>

                                    <div className="col-md-6 col-lg-6 mt-3 mb-4">
                                      <label className="form-label">
                                        Zonasi
                                      </label>
                                      <div>
                                        <strong>{this.state.zonasi}</strong>
                                      </div>
                                    </div>

                                    <div className="col-md-12 col-lg-12 mt-3 mb-4">
                                      <label className="form-label">
                                        Status Pelaksanaan
                                      </label>
                                      <div>
                                        <span
                                          className={
                                            "badge badge-light-" +
                                            this.state.color_status_pelaksanaan
                                          }
                                        >
                                          <strong>
                                            {this.state.status_pelaksanaan}
                                          </strong>
                                        </span>
                                      </div>
                                    </div>

                                    <div className="col-md-12 col-lg-12 mt-3 mb-4">
                                      <label className="form-label">
                                        Status Peserta
                                      </label>
                                      <div>
                                        <span
                                          className={
                                            "badge badge-light-" +
                                            getColorClassStatusPendaftaran(
                                              this.state.status_peserta.toLowerCase(),
                                            )
                                          }
                                        >
                                          <strong>
                                            {this.state.status_peserta}
                                          </strong>
                                        </span>
                                      </div>
                                    </div>

                                    <div className="text-center my-7 pt-7">
                                      <a
                                        href="#"
                                        className="btn btn-md btn-light me-2"
                                        title="Kembali"
                                        onClick={() => {
                                          this.setState({
                                            main: 1,
                                            selected: 2,
                                          });
                                        }}
                                      >
                                        Kembali ke Data Pelatihan
                                      </a>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            ) : (
                              <div
                                className="tab-pane fade show active"
                                id="nav-profile"
                                role="tabpanel"
                                aria-labelledby="nav-profile"
                              >
                                <form
                                  className="form"
                                  action="#"
                                  onSubmit={this.handleSubmitUbahPelatihan}
                                >
                                  <div className="row mt-7 pb-7">
                                    <h2 className="fs-5 text-muted mb-3">
                                      Edit Riwayat Pelatihan
                                    </h2>
                                    <div className="row">
                                      <div className="col-12 mt-5 mb-6">
                                        <p className="text-dark fs-7 mb-0">
                                          {this.state.detail_akademi}
                                        </p>
                                        <h1 className="align-items-center text-dark fw-bolder my-1 fs-4">
                                          IDPELATIHAN -{" "}
                                          {this.state.detail_nama_pelatihan}
                                        </h1>
                                        <span className="text-muted fw-semibold fs-7 mb-0">
                                          {this.state.detail_tema}
                                        </span>
                                      </div>

                                      <div className="col-md-4 col-lg-4 mt-3 mb-4">
                                        <label className="form-label">
                                          Penyelenggara
                                        </label>
                                        <div>
                                          <strong>
                                            {this.state.detail_penyelenggara}
                                          </strong>
                                        </div>
                                      </div>

                                      <div className="col-md-4 col-lg-4 mt-3 mb-4">
                                        <label className="form-label">
                                          Mitra
                                        </label>
                                        <div>
                                          <strong>
                                            Tampilkan mitra Disini (Jika Tanpa
                                            Mitra cantumkan : Swakelola)
                                          </strong>
                                        </div>
                                      </div>

                                      <div className="col-md-4 col-lg-4 mt-3 mb-4">
                                        <label className="form-label">
                                          Metode
                                        </label>
                                        <div>
                                          <strong>
                                            Tampilkan Metode pelatihan
                                          </strong>
                                        </div>
                                      </div>

                                      <div className="col-md-12 col-lg-12 mt-3 mb-4">
                                        <label className="form-label">
                                          Lokasi Pelatihan (Ini Hanya
                                          ditampilkan jika metode pelatihan
                                          Offline atau online/offline)
                                        </label>
                                        <div>
                                          <strong>
                                            Tampilkan Alamat, Kota/Kab, Provinsi
                                          </strong>
                                        </div>
                                      </div>

                                      <div className="col-md-6 col-lg-6 mt-3 mb-4">
                                        <label className="form-label">
                                          Tgl. Pelatihan
                                        </label>
                                        <div>
                                          <strong>
                                            dd mm yyyy - dd mm yyyy
                                          </strong>
                                        </div>
                                      </div>

                                      <div className="col-md-6 col-lg-6 mt-3 mb-4">
                                        <label className="form-label">
                                          Zonasi
                                        </label>
                                        <div>
                                          <strong>Tampilkan Zonasi</strong>
                                        </div>
                                      </div>

                                      <div className="col-md-12 col-lg-12 mt-3 mb-4">
                                        <label className="form-label">
                                          Status Pelaksanaan
                                        </label>
                                        <div>
                                          <strong>
                                            {this.state.detail_status_pelatihan}
                                          </strong>
                                        </div>
                                      </div>

                                      <div className="col-md-12 col-lg-12 mt-3 mb-4">
                                        <label className="form-label">
                                          Status Peserta
                                        </label>
                                        <div>
                                          <strong>
                                            Tampilkan Status Peserta Disini
                                          </strong>
                                        </div>
                                      </div>

                                      {/*  <div className="mt-4">
                                                                                            <input
                                                                                                type="checkbox"
                                                                                                id="custom_checkbox"
                                                                                                name="ck"
                                                                                                checked={this.state.ubah_data_status == 1 ? true : false}
                                                                                                onChange={
                                                                                                    (e) => {
                                                                                                        this.setState({
                                                                                                            ubah_data_status: !this.state.ubah_data_status ? 1 : 0
                                                                                                        });
                                                                                                    }
                                                                                                }
                                                                                            />
                                                                                            <label className="ms-3" htmlFor='ck'>Ubah Data</label>
                                                                                        </div> */}
                                      <div className="mt-4 highlight bg-light-primary p-5">
                                        <div className="col-lg-12 mb-7 fv-row text-primary">
                                          <h5 className="text-primary fs-5">
                                            Panduan
                                          </h5>
                                          <p className="text-primary">
                                            Sebelum melakukan pemindahan
                                            peserta, mohon untuk membaca panduan
                                            berikut :
                                          </p>
                                          <ul>
                                            <li>
                                              Pemindahan peserta hanya dapat
                                              dilakukan pada masa pendaftaran
                                            </li>
                                            <li>
                                              Pelatihan tujuan harus memiliki
                                              isian form yang sama seperti
                                              pelatihan asal (hal ini agar tidak
                                              terjadi anomali data)
                                            </li>
                                            <li>
                                              Pada dropdown pilihan pelatihan,
                                              hanya menampilkan pelatihan yang
                                              memiliki isian form yang sama
                                              dengan pelatihan asal
                                            </li>
                                          </ul>
                                        </div>

                                        <label className="form-label text-primary fw-semibold">
                                          Pindah Pelatihan
                                        </label>
                                        <Select
                                          name="detail_pelatihan"
                                          placeholder="Pilih Pelatihan"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.state.datax
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm selectpicker p-0 mb-5"
                                          options={
                                            this.state.detail_all_pelatihan
                                          }
                                          value={this.state.detailPelatihanVal}
                                          onChange={
                                            this.handleChangeDetailPelatihan
                                          }
                                        />
                                      </div>
                                    </div>
                                    <div className="text-center my-7 pt-7">
                                      <a
                                        href="#"
                                        className="btn btn-md btn-light me-2"
                                        title="Kembali"
                                        onClick={() => {
                                          this.setState({
                                            main: 1,
                                            selected: 2,
                                          });
                                        }}
                                      >
                                        Batal
                                      </a>
                                      <button
                                        type="submit"
                                        className="btn btn-primary btn-md"
                                      >
                                        <i className="fa fa-paper-plane me1"></i>
                                        Simpan
                                      </button>
                                    </div>
                                  </div>
                                </form>
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
