import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import {
  capitalWord,
  dateRange,
  tagColorMapStatusPeserta,
} from "../../pelatihan/Pelatihan/helper";
import {
  getColorClassStatusPendaftaran,
  indonesianDateFormat2,
} from "../../publikasi/helper";
import ButtonSetAdmin from "./components/ButtonSetAdmin";
import moment from "moment";

export default class UserDTSEditContent extends React.Component {
  constructor(props) {
    super(props);
    this.back = this.back.bind(this);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleChangeSearch = this.handleChangeSearchAction.bind(this);
    this.handleSort = this.handleSortAction.bind(this);
    this.handleDelete = this.handleDeleteAction.bind(this);
    this.pratinjauTriggerRef = React.createRef(null);
  }

  state = {
    tempLastNumber: 0,
    datax_user: [],
    datax: [],
    loading: false,
    errors: {},
    hover: 1,
    selected: 1,
    foto: "",
    ktp: "",
    nama: "",
    nik: "",
    email: "",
    nomor_handphone: "",
    tempat_lahir: "",
    tanggal_lahir: "",
    jenis_kelamin: "",
    jenjang_id: "",
    jenjang_nama: "",
    address: "",
    provinsi_id: "",
    provinsi_nama: "",
    kota_id: "",
    kota_nama: "",
    kecamatan_id: "",
    kecamatan_nama: "",
    kelurahan_id: "",
    kelurahan_nama: "",
    status_pekerjaan_id: "",
    status_pekerjaan_nama: "",
    pekerjaan_id: "",
    pekerjaan_nama: "",
    perusahaan: "",
    nama_kontak_darurat: "",
    hubungan: "",
    nomor_handphone_darurat: "",
    ijazah: "",
    datax_provinsi_1: [],
    valProvinsi_1: [],
    provinsi_id_1: "",
    datax_provinsi_2: [],
    valProvinsi_2: [],
    provinsi_id_2: "",
    datax_kabupaten_1: [],
    valKabupaten_1: [],
    kabupaten_id_1: "",
    datax_kabupaten_2: [],
    valKabupaten_2: [],
    kabupaten_id_2: "",
    datax_kecamatan_1: [],
    valKecamatan_1: [],
    kecamatan_id_1: "",
    datax_kecamatan_2: [],
    valKecamatan_2: [],
    kecamatan_id_2: "",
    datax_desa_1: [],
    valDesa_1: [],
    desa_id_1: "",
    datax_desa_2: [],
    valDesa_2: [],
    desa_id_2: "",
    from_pagination_change: 0,
    searchText: "",
    sort_by: "id",
    sort_val: "DESC",
    totalLulus: 0,
    totalTidakLulus: 0,
    isadmin: false,
  };

  option_status = [
    { value: 1, label: "Aktif" },
    { value: 2, label: "Tidak Aktif" },
  ];

  option_kelamin = [
    { value: 1, label: "Pria" },
    { value: 2, label: "Wanita" },
  ];

  option_pendidikan = [
    { value: "Tidak Sekolah", label: "Tidak Sekolah" },
    { value: "TK", label: "TK" },
    { value: "SD/Sederajat", label: "SD/Sederajat" },
    { value: "SMP/Sederajat", label: "SMP/Sederajat" },
    { value: "SMA/Sederajat", label: "SMA/Sederajat" },
    { value: "D1", label: "D1" },
    { value: "D2", label: "D2" },
    { value: "D3", label: "D3" },
    { value: "D4", label: "D4" },
    { value: "S1", label: "S1" },
    { value: "S2", label: "S2" },
    { value: "S3", label: "S3" },
  ];

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  style = {
    selectedSide: {
      padding: "8px",
      backgroundColor: "#ecf5fc",
      marginBottom: "4px",
      borderRadius: "8px",
      //fontSize: '16px',
      fontWeight: "700",
      width: "95%",
    },
    normalSide: {
      padding: "8px",
      //backgroundColor: '#ecf5fc',
      marginBottom: "4px",
      borderRadius: "8px",
      //fontSize: '16px',
      fontWeight: "700",
      width: "95%",
    },
  };

  columns = [
    {
      name: "No",
      center: true,
      cell: (row, index) => this.state.tempLastNumber + index + 1,
      width: "70px",
    },

    {
      name: "Nama Pelatihan",
      className: "min-w-300px mw-300px",
      width: "300px",
      sortable: true,
      grow: 6,
      wrap: true,
      allowOverflow: false,
      selector: (row) => (
        <div>
          <label className="d-flex flex-stack my-2">
            <span className="d-flex align-items-center me-2">
              <span className="d-flex flex-column">
                <span className="text-muted fs-7 fw-semibold">
                  {row.nama_akademi}
                </span>
                <h6 className="fw-bolder fs-7 mb-1">
                  {row.pelatian_id_slug} - {capitalWord(row.nama_pelatihan)}
                </h6>
                <h6 className="text-muted fs-7 fw-semibold mb-1">
                  {row.penyelenggara}
                </h6>
                <span className="text-muted fs-7 fw-semibold">
                  {row.metode_pelatihan}
                </span>
              </span>
            </span>
          </label>
        </div>
      ),
    },
    {
      name: "Tgl. Pelatihan",
      sortable: true,
      center: false,
      //width: '180px',
      selector: (row) => (
        <span className="fs-7">
          {dateRange(
            row.pelatihan_mulai,
            row.pelatihan_selesai,
            "YYYY-MM-DD HH:mm:ss",
            "DD MMM YYYY",
          )}
        </span>
      ),
    },
    {
      name: "Lokasi",
      sortable: true,
      cell: (row) => (
        <div>
          <span className="d-flex flex-column my-2">
            {row.metode_pelatihan?.toLowerCase()?.includes("offline") ? (
              <span
                className="fs-7"
                style={{
                  overflow: "hidden",
                  whiteSpace: "wrap",
                  textOverflow: "ellipses",
                }}
              >
                {`${capitalWord(row.kabupaten?.toLowerCase())}`}
              </span>
            ) : (
              <span
                className="fs-7"
                style={{
                  overflow: "hidden",
                  whiteSpace: "wrap",
                  textOverflow: "ellipses",
                }}
              >
                {capitalWord(row.metode_pelatihan)}
              </span>
            )}
          </span>
        </div>
      ),
    },
    {
      name: "Status",
      sortable: true,
      center: true,
      //width: '180px',
      selector: (row) => (
        <span className="fs-7">
          <span
            className={
              "badge badge-light-" +
                tagColorMapStatusPeserta[row.status_peserta] ??
              "danger" + " fs-9 m-1"
            }
          >
            {capitalWord(row.status_peserta) ?? "-"}
          </span>
        </span>
      ),
    },
    {
      name: "Nilai",
      sortable: false,
      width: "150px",
      cell: (row) => (
        <div>
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              this.getNilaiPeserta(row.user_id, row.pelatian_id);
            }}
            id={row.id}
            title="Lihat Nilai"
            className="btn btn-icon btn-success btn-sm me-1"
          >
            <i class="bi bi-journal-check text-white"></i>
          </a>
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    } else {
      let segment_url = window.location.pathname.split("/");
      let user_id = segment_url[4];
      this.setState({ user_id: user_id });

      let dataBodyUser = {
        userid: user_id,
      };

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/get_user_byid",
          dataBodyUser,
          this.configs,
        )
        .then((res) => {
          const datax_user = res.data.result.data[0];
          const statusx = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statusx) {
            const foto = datax_user.foto;
            const ktp = datax_user.ktp;
            const nama = datax_user.nama;
            const nik = datax_user.nik;
            const email = datax_user.email;
            const nomor_handphone = datax_user.nomor_handphone;
            const tempat_lahir = datax_user.tempat_lahir;
            let split_tanggal = "";
            if (datax_user.tanggal_lahir) {
              split_tanggal = datax_user.tanggal_lahir.split("-");
              split_tanggal =
                split_tanggal[2] +
                "/" +
                split_tanggal[1] +
                "/" +
                split_tanggal[0];
            }
            const tanggal_lahir = split_tanggal;
            let jenis_kelamin = datax_user.jenis_kelamin;
            if (jenis_kelamin == "1") {
              jenis_kelamin = "Pria";
            } else if (jenis_kelamin == "2") {
              jenis_kelamin = "Wanita";
            }
            const jenjang_id = datax_user.jenjang_id;
            const jenjang_nama = datax_user.jenjang_nama;
            const address = datax_user.address;
            const provinsi_id = datax_user.provinsi_id;
            const provinsi_nama = datax_user.provinsi_nama;
            const kota_id = datax_user.kota_id;
            const kota_nama = datax_user.kota_nama;
            const kecamatan_id = datax_user.kecamatan_id;
            const kecamatan_nama = datax_user.kecamatan_nama;
            const kelurahan_id = datax_user.kelurahan_id;
            const kelurahan_nama = datax_user.kelurahan_nama;
            const status_pekerjaan_id = datax_user.status_pekerjaan_id;
            const status_pekerjaan_nama = datax_user.status_pekerjaan_nama;
            const pekerjaan_id = datax_user.pekerjaan_id;
            const pekerjaan_nama = datax_user.pekerjaan_nama;
            const perusahaan = datax_user.perusahaan;
            const nama_kontak_darurat = datax_user.nama_kontak_darurat;
            const hubungan = datax_user.hubungan;
            const nomor_handphone_darurat = datax_user.nomor_handphone_darurat;
            const ijazah = datax_user.ijazah;

            this.setState();
            this.setState(
              {
                datax_user,
                foto,
                image: foto,
                ktp,
                nama,
                nik,
                email,
                nomor_handphone,
                tempat_lahir,
                tanggal_lahir,
                jenis_kelamin,
                jenjang_id,
                jenjang_nama,
                address,
                provinsi_id,
                provinsi_nama,
                kota_id,
                kota_nama,
                kecamatan_id,
                kecamatan_nama,
                kelurahan_id,
                kelurahan_nama,
                status_pekerjaan_id,
                status_pekerjaan_nama,
                pekerjaan_id,
                pekerjaan_nama,
                perusahaan,
                nama_kontak_darurat,
                hubungan,
                nomor_handphone_darurat,
                ijazah,
                isadmin: datax_user.isadmin,
              },
              () => {
                this.handleReload();
              },
            );
          }
        });
    }
  }

  handleReload(page, newPerPage) {
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });
    let dataBodyPelatihan = {
      userid: this.state.user_id,
      start: start_tmp,
      rows: length_tmp,
      cari: this.state.searchText,
      sort: this.state.sort_by,
      sort_val: this.state.sort_val,
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/user/list_admin_user_pelatihan",
        dataBodyPelatihan,
        this.configs,
      )
      .then((res) => {
        const datax = res.data.result.Data;
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        let totalLulus = 0;
        let totalTidakLulus = 0;
        for (let i = 0; i < datax.length; i++) {
          if (
            datax[i].status_peserta == "Lulus Pelatihan - Nilai" ||
            datax[i].status_peserta == "Lulus Pelatihan - Kehadiran"
          ) {
            totalLulus += 1;
          }

          if (datax[i].status_peserta.includes("Tidak Lulus")) {
            totalTidakLulus += 1;
          }
        }

        if (statusx) {
          this.setState({
            loading: false,
            datax: datax,
            totalRows: res.data.result.TotalLength[0].jml_data,
            totalLulus,
            totalTidakLulus,
          });
        } else {
          this.setState({
            loading: false,
            datax: [],
            totalRows: 0,
          });
          let messagex = "Data Tidak Ditemukan";
          if (this.state.selected != 1) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        }
      })
      .catch((error) => {
        this.setState({
          datax: [],
          loading: false,
          totalRows: 0,
        });
        //let messagex = error.response.data.result.Message;
        let messagex = "Data Tidak Ditemukan";
        if (this.state.selected != 1) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  back() {
    window.history.back();
  }

  handlePageChange = (page) => {
    if (this.state.from_pagination_change == 0) {
      this.setState({ loading: true });
      this.handleReload(page, this.state.newPerPage);
    }
  };
  handlePerRowsChange = async (newPerPage, page) => {
    if (this.state.from_pagination_change == 1) {
      this.setState({ loading: true });
      this.setState({ newPerPage: newPerPage }, () => {
        this.handleReload(page, this.state.newPerPage);
      });
    }
  };

  handleChangeSearchAction(e) {
    const searchText = e.currentTarget.value;
    this.setState({
      searchText: searchText,
    });
    if (searchText == "") {
      this.setState(
        {
          searchText: searchText,
        },
        () => {
          this.handleReload();
        },
      );
    }
  }

  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    this.setState({ searchText });
    if (e.key == "Enter") {
      if (searchText == "") {
        this.setState(
          {
            isSearch: false,
            searchText: "",
          },
          () => {
            this.handleReload();
          },
        );
      } else {
        this.setState({ loading: true });
        this.setState({ isSearch: true });
        this.setState({ searchText: searchText }, () => {
          this.handleReload();
        });
      }
    }
  }

  handleSortAction(column, sortDirection) {
    let server_name = "";
    if (column.name == "Nama Pelatihan") {
      server_name = "nama";
    } else if (column.name == "ID Pelatihan") {
      server_name = "id";
    } else if (column.name == "Status") {
      server_name = "status";
    }

    this.setState(
      {
        sort_by: server_name,
        sort_val: sortDirection.toUpperCase(),
      },
      () => {
        this.handleReload(1, this.state.newPerPage);
      },
    );
  }

  handleDeleteAction(e) {
    e.preventDefault();
    const idx = this.state.user_id;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          //const data = { id_role: idx }
          const dataFormPost = new FormData();
          dataFormPost.append("userid", idx);

          axios
            .post(
              process.env.REACT_APP_BASE_API_URI +
                "/user/delete_pelatihan_user",
              dataFormPost,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                let icon = "success";
                if (res.data.result.StatusCode == "404") {
                  icon = "warning";
                }
                swal
                  .fire({
                    title: messagex,
                    icon: icon,
                    confirmButtonText: "Ok",
                    allowOutsideClick: false,
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      window.location = "/site-management/user-dts";
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmationButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            });
        }
      });
  }

  getNilaiPeserta(idUser, idPelatihan) {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/rekappendaftaran/list-user-silabus",
        {
          pelatihan_id: idPelatihan,
          user_id: idUser,
        },
        this.configs,
      )
      .then((result) => {
        this.setState({ pratinjau: result.data.result }, () => {
          this.pratinjauTriggerRef.current.click();
        });
      })
      .catch((err) => {
        let statux = err.response.data.result.Status;
        let messagex = err.response.data.result.Message;
        swal
          .fire({
            title:
              messagex ?? "Terjadi kesalahan saat berkomunikasi dengan server.",
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
            }
          });
      });
  }

  render() {
    return (
      <div>
        <a
          href="#"
          hidden
          data-bs-toggle="modal"
          data-bs-target="#pratinjau-nilai"
          ref={this.pratinjauTriggerRef}
        ></a>
        <div className="modal fade" tabindex="-1" id="pratinjau-nilai">
          <div className="modal-dialog modal-md">
            <div className="modal-content">
              <div className="modal-header pe-0 pb-0">
                <div className="row w-100">
                  <div className="col-12 d-flex w-100 justify-content-between">
                    <h5 className="modal-title">Nilai Peserta</h5>
                    <div
                      className="btn btn-icon btn-sm btn-active-light-primary"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                    >
                      <span className="svg-icon svg-icon-2x">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                          fill="none"
                        >
                          <rect
                            opacity="0.5"
                            x="6"
                            y="17.3137"
                            width="16"
                            height="2"
                            rx="1"
                            transform="rotate(-45 6 17.3137)"
                            fill="currentColor"
                          />
                          <rect
                            x="7.41422"
                            y="6"
                            width="16"
                            height="2"
                            rx="1"
                            transform="rotate(45 7.41422 6)"
                            fill="currentColor"
                          />
                        </svg>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              {this.state.pratinjau && (
                <div className="modal-body">
                  <div className="card">
                    <div className="row justify-content-start">
                      <div className="col-12">
                        <h4 className="fw-bolder">
                          {capitalWord(this.state.pratinjau[0]?.nama_silabus)}
                        </h4>
                        <span className="text-muted">Nilai Peserta</span>
                      </div>
                      <div className="col-12">
                        <ul className="list-group">
                          {this.state.pratinjau.map((elem) => (
                            <div
                              className="d-flex align-items-center bg-light-primary rounded p-5 mb-2"
                              key={`detail_${elem.silabus_header_id}`}
                            >
                              <span className="svg-icon svg-icon-primary me-5">
                                <span className="svg-icon svg-icon-1">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <path
                                      opacity="0.3"
                                      d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                                      fill="currentColor"
                                    ></path>
                                    <path
                                      d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                                      fill="currentColor"
                                    ></path>
                                  </svg>
                                </span>
                              </span>
                              <div className="flex-grow-1 me-2">
                                <a
                                  href="#"
                                  className="fw-bold text-gray-800 text-hover-primary fs-6"
                                >
                                  {elem.silabus_detail ?? "Tidak ada data."}
                                </a>
                              </div>
                              <span className="fw-bold text-primary py-1">
                                {elem.nilai ?? ""}
                              </span>
                            </div>
                          ))}
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              )}

              <div className="modal-footer">
                <button
                  className="btn btn-light btn-sm"
                  data-bs-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>

        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  User DTS
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.back}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>

                <a
                  href={"/site-management/user-dts/edit/" + this.state.user_id}
                  className="btn btn-warning fw-bolder btn-sm me-2"
                >
                  <i className="bi bi-gear-fill"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Edit
                  </span>
                </a>

                <a
                  href="#"
                  onClick={this.handleDelete}
                  title="Hapus"
                  className="btn btn-sm btn-danger btn-active-light-info"
                >
                  <i className="bi bi-trash-fill text-white"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Detail User DTS
                          </h1>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="mb-7">
                          <div className="d-flex flex-wrap py-3">
                            <div className="flex-grow-1 mb-3">
                              <div className="d-flex flex-wrap flex-sm-nowrap mb-3">
                                <div className="me-7 mb-4">
                                  {this.state.image == null ? (
                                    <span className="symbol symbol-100px symbol-lg-150px me-6">
                                      <span className="symbol-label bg-light-primary">
                                        <span className="svg-icon svg-icon-1 svg-icon-primary text-center">
                                          <small style={{ fontSize: "2em" }}>
                                            Belum
                                            <br />
                                            Ada
                                            <br />
                                            Gambar
                                          </small>
                                        </span>
                                      </span>
                                    </span>
                                  ) : (
                                    <div className="symbol symbol-100px symbol-lg-150px symbol-fixed position-relative">
                                      <img
                                        src={
                                          process.env.REACT_APP_BASE_API_URI +
                                          "/download/get-file?path=" +
                                          this.state.image
                                        }
                                      />
                                    </div>
                                  )}
                                </div>
                                <div className="flex-grow-1">
                                  <div className="d-flex justify-content-between align-items-start flex-wrap mb-2">
                                    <div className="d-flex flex-column">
                                      <div className="d-flex align-items-center mb-2">
                                        <h2 className="fw-bolder me-1 mb-0">
                                          {this.state.nama
                                            ? this.state.nama
                                            : "-"}
                                        </h2>
                                      </div>
                                      <div className="d-flex flex-wrap fw-bold fs-6 mb-4 pe-2">
                                        <span className="d-flex align-items-center fs-7 text-muted fw-semibold me-5 mb-3">
                                          <span className="svg-icon svg-icon-4 me-1">
                                            <i className="bi bi-person-vcard me-1"></i>
                                            {this.state.nik
                                              ? this.state.nik
                                              : "-"}
                                          </span>
                                        </span>
                                        <span className="d-flex align-items-center fs-7 fw-semibold text-muted me-5 mb-3">
                                          <span className="svg-icon svg-icon-4 me-1">
                                            <i className="bi bi-telephone me-1"></i>
                                            {this.state.nomor_handphone
                                              ? this.state.nomor_handphone
                                              : "-"}
                                          </span>
                                        </span>
                                        <span className="d-flex align-items-center fs-7 text-muted fw-semibold me-5 mb-3">
                                          <span className="svg-icon svg-icon-4 me-1">
                                            <i className="bi bi-envelope-at me-1"></i>
                                            {this.state.email
                                              ? this.state.email
                                              : "-"}
                                          </span>
                                        </span>
                                      </div>
                                      <div className="d-flex flex-column flex-grow-1 pe-8">
                                        <div className="d-flex flex-wrap">
                                          <div className="card border rounded icon-category icon-category-sm p-5 mb-3 me-3">
                                            <div className="row align-items-center mx-n3">
                                              <div className="col-auto px-3">
                                                <div className="icon-h-p">
                                                  <i className="bi bi-book text-primary fa-2x"></i>
                                                </div>
                                              </div>
                                              <div className="col px-3">
                                                <div className="card-body p-0">
                                                  <h6 className="fs-8 mb-0 text-muted line-clamp-1">
                                                    Pelatihan
                                                  </h6>
                                                  <h6 className="mb-0 line-clamp-1 hover-clamp-off">
                                                    {this.state.totalRows}{" "}
                                                    Pelatihan
                                                  </h6>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div className="card border rounded icon-category icon-category-sm p-5 mb-3 me-3">
                                            <div className="row align-items-center mx-n3">
                                              <div className="col-auto px-3">
                                                <div className="icon-h-p">
                                                  <i className="bi bi-patch-check text-success fa-2x"></i>
                                                </div>
                                              </div>
                                              <div className="col px-3">
                                                <div className="card-body p-0">
                                                  <h6 className="fs-8 mb-0 text-muted line-clamp-1">
                                                    Lulus
                                                  </h6>
                                                  <h6 className="mb-0 line-clamp-1 hover-clamp-off">
                                                    {this.state.totalLulus}{" "}
                                                    Pelatihan
                                                  </h6>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div className="card border rounded icon-category icon-category-sm p-5 mb-3 me-3">
                                            <div className="row align-items-center mx-n3">
                                              <div className="col-auto px-3">
                                                <div className="icon-h-p">
                                                  <i className="bi bi-clipboard-x text-danger fa-2x"></i>
                                                </div>
                                              </div>
                                              <div className="col px-3">
                                                <div className="card-body p-0">
                                                  <h6 className="fs-8 mb-0 text-muted line-clamp-1">
                                                    Tidak Lulus
                                                  </h6>
                                                  <h6 className="mb-0 line-clamp-1 hover-clamp-off">
                                                    {this.state.totalTidakLulus}{" "}
                                                    Pelatihan
                                                  </h6>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div className="d-flex my-4">
                                      <ButtonSetAdmin
                                        isAdmin={this.state.isadmin}
                                        userId={this.state.datax_user.id}
                                        userNama={this.state.datax_user.nama}
                                        onRequestStart={() => {
                                          console.log("start req");
                                          this.setState({ loading: true });
                                        }}
                                        axiosConfig={this.configs}
                                        onSuccess={() => {
                                          this.setState({
                                            isadmin: !this.state.isadmin,
                                          });
                                        }}
                                      ></ButtonSetAdmin>
                                      {/* <a
                                                                                href="#"
                                                                                title="Set Admin"
                                                                                className="btn btn-sm bg-info text-white"
                                                                                onClick={this.handleMakeAdmin}
                                                                                
                                                                            >
                                                                                <i className="bi bi-person-fill-add fs-5 text-white me-1"></i>
                                                                                Set Admin
                                                                            </a> */}
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="d-flex border-bottom p-0">
                          <ul
                            className="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap"
                            style={{
                              overflowX: "auto",
                              overflowY: "hidden",
                              display: "flex",
                              whiteSpace: "nowrap",
                              marginBottom: 0,
                            }}
                          >
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary active"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_1"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                  Data Pokok
                                </span>
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary false"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_2"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                  Riwayat Pelatihan
                                </span>
                              </a>
                            </li>
                          </ul>
                        </div>
                        <div className="tab-content" id="detail-account-tab">
                          {/* tab 1 */}
                          <div
                            className="tab-pane fade show active"
                            id="kt_tab_pane_1"
                            role="tabpanel"
                          >
                            <div className="row mt-7 pb-7">
                              <h2 className="fs-5 pt-7 text-muted mb-3">
                                Data Diri
                              </h2>
                              <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                <label className="form-label">Tgl. Lahir</label>
                                <div>
                                  {moment(
                                    this.state.datax_user.tanggal_lahir,
                                  ).isValid() ? (
                                    <strong>
                                      {Math.floor(
                                        moment().diff(
                                          this.state.datax_user.tanggal_lahir,
                                          "years",
                                        ),
                                        true,
                                      )}{" "}
                                      Tahun
                                    </strong>
                                  ) : (
                                    "-"
                                  )}
                                </div>
                              </div>
                              <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                <label className="form-label">
                                  Jenis Kelamin
                                </label>
                                <div>
                                  <strong>
                                    {this.state.jenis_kelamin
                                      ? this.state.jenis_kelamin
                                      : "-"}
                                  </strong>
                                </div>
                              </div>
                              {/* <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                                                <label className="form-label">Pendidikan Terakhir</label>
                                                                <div><strong>{this.state.jenjang_nama ? this.state.jenjang_nama : '-'}</strong></div>
                                                            </div>*/}
                              <div>
                                <div className="my-8 border-top mx-0"></div>
                              </div>
                              <h2 className="fs-5 text-muted mb-3">Domisili</h2>
                              {/* <div className="col-md-12 col-lg-12 mt-3 mb-3">
                                                                <label className="form-label">Alamat Lengkap</label>
                                                                <div><strong>{this.state.address ? this.state.address : '-'}</strong></div>
                                                            </div>*/}

                              <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                <label className="form-label">Provinsi</label>
                                <div>
                                  <strong>
                                    {this.state.provinsi_nama
                                      ? this.state.provinsi_nama
                                      : "-"}
                                  </strong>
                                </div>
                              </div>

                              <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                <label className="form-label">Kota/Kab</label>
                                <div>
                                  <strong>
                                    {this.state.kota_nama
                                      ? this.state.kota_nama
                                      : "-"}
                                  </strong>
                                </div>
                              </div>

                              <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                <label className="form-label">Kecamatan</label>
                                <div>
                                  <strong>
                                    {this.state.kecamatan_nama
                                      ? this.state.kecamatan_nama
                                      : "-"}
                                  </strong>
                                </div>
                              </div>

                              <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                <label className="form-label">
                                  Desa/Kelurahan
                                </label>
                                <div>
                                  <strong>
                                    {this.state.kelurahan_nama
                                      ? this.state.kelurahan_nama
                                      : "-"}
                                  </strong>
                                </div>
                              </div>

                              {/* <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                                                <label className="form-label">Kode Pos</label>
                                                                <div><strong>Tampilkan disini</strong></div>
                                                            </div> */}
                              <div>
                                <div className="my-8 border-top mx-0"></div>
                              </div>
                              <h2 className="fs-5 text-muted mb-3">
                                Pekerjaan
                              </h2>
                              <div className="col-md-12 col-lg-12 mt-3 mb-3">
                                <label className="form-label">Status</label>
                                <div>
                                  <strong>
                                    {this.state.status_pekerjaan_nama
                                      ? this.state.status_pekerjaan_nama
                                      : "-"}
                                  </strong>
                                </div>
                              </div>
                              <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                <label className="form-label">Pekerjaan</label>
                                <div>
                                  <strong>
                                    {this.state.pekerjaan_nama
                                      ? this.state.pekerjaan_nama
                                      : "-"}
                                  </strong>
                                </div>
                              </div>
                              <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                <label className="form-label">
                                  Institusi Tempat Bekerja
                                </label>
                                <div>
                                  <strong>
                                    {this.state.perusahaan
                                      ? this.state.perusahaan
                                      : "-"}
                                  </strong>
                                </div>
                              </div>
                              {/*<div>
                                                                <div className="my-8 border-top mx-0"></div>
                                                            </div>
                                                            <h2 className="fs-5 text-muted mb-3">Kontak Darurat</h2>
                                                            <div className="col-md-4 col-lg-4 mt-3 mb-3">
                                                                <label className="form-label">Nama Kontak Darurat</label>
                                                                <div><strong>{this.state.nama_kontak_darurat ? this.state.nama_kontak_darurat : '-'}</strong></div>
                                                            </div>
                                                            <div className="col-md-4 col-lg-4 mt-3 mb-3">
                                                                <label className="form-label">Nomor Kontak Darurat</label>
                                                                <div><strong>{this.state.nomor_handphone_darurat ? this.state.nomor_handphone_darurat : '-'}</strong></div>
                                                            </div>
                                                            <div className="col-md-4 col-lg-4 mt-3 mb-3">
                                                                <label className="form-label">Hubungan</label>
                                                                <div><strong>{this.state.hubungan ? this.state.hubungan : '-'}</strong></div>
                                                            </div>*/}
                            </div>
                          </div>
                          <div
                            className="tab-pane fade"
                            id="kt_tab_pane_2"
                            role="tabpanel"
                          >
                            <div className="row pb-7">
                              <div className="card-toolbar">
                                <div className="d-flex align-items-center position-relative float-end mt-7 mb-5 me-2">
                                  <span className="svg-icon svg-icon-1 position-absolute ms-6">
                                    <svg
                                      width="24"
                                      height="24"
                                      viewBox="0 0 24 24"
                                      fill="none"
                                      xmlns="http://www.w3.org/2000/svg"
                                      className="mh-50px"
                                    >
                                      <rect
                                        opacity="0.5"
                                        x="17.0365"
                                        y="15.1223"
                                        width="8.15546"
                                        height="2"
                                        rx="1"
                                        transform="rotate(45 17.0365 15.1223)"
                                        fill="currentColor"
                                      ></rect>
                                      <path
                                        d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                        fill="currentColor"
                                      ></path>
                                    </svg>
                                  </span>
                                  <input
                                    type="text"
                                    data-kt-user-table-filter="search"
                                    className="form-control form-control-sm form-control-solid w-250px ps-14"
                                    placeholder="Cari Pelatihan"
                                    onKeyPress={this.handleKeyPress}
                                    onChange={this.handleChangeSearch}
                                  />
                                </div>
                              </div>
                              <DataTable
                                columns={this.columns}
                                data={this.state.datax}
                                progressPending={this.state.loading}
                                highlightOnHover
                                pointerOnHover
                                pagination
                                paginationServer
                                paginationTotalRows={this.state.totalRows}
                                paginationDefaultPage={this.state.currentPage}
                                onChangeRowsPerPage={(
                                  currentRowsPerPage,
                                  currentPage,
                                ) => {
                                  this.setState(
                                    {
                                      from_pagination_change: 1,
                                    },
                                    () => {
                                      this.handlePerRowsChange(
                                        currentRowsPerPage,
                                        currentPage,
                                      );
                                    },
                                  );
                                }}
                                onChangePage={(page, totalRows) => {
                                  this.setState(
                                    {
                                      from_pagination_change: 0,
                                    },
                                    () => {
                                      this.handlePageChange(page, totalRows);
                                    },
                                  );
                                }}
                                customStyles={this.customStyles}
                                persistTableHead={true}
                                onSort={this.handleSort}
                                noDataComponent={
                                  <div className="mt-5">
                                    Tidak Ada Data Pelatihan
                                  </div>
                                }
                                sortServer
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
