import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import imageCompression from "browser-image-compression";

const resizeFile = async (file) => {
  const options = {
    maxSizeMB: 5,
    maxWidthOrHeight: 300,
    useWebWorker: true,
  };

  return imageCompression(file, options);
};

export default class SettingAddonTambahContent extends React.Component {
  constructor(props) {
    super(props);
    this.kembali = this.kembaliAction.bind(this);
    this.handleSubmit = this.handleSubmitAction.bind(this);
    this.handleChangeScript = this.handleChangeScriptAction.bind(this);
    this.handleChangeStatus = this.handleChangeStatusAction.bind(this);
    this.handleChangeNama = this.handleChangeNamaAction.bind(this);
  }

  state = {
    datax: [],
    isLoading: false,
    loading: false,
    totalRows: "",
    tempLastNumber: 0,
    column: "",
    sortDirection: "ASC",
    errors: {},
    script: "",
    valStatus: [],
    status: false,
  };

  optionstatus = [
    { value: "1", label: "Publish" },
    { value: "0", label: "Unpublish" },
  ];

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
  }

  kembaliAction() {
    window.history.back();
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const check = this.checkEmpty(dataForm, ["nama", "script", "status"], [""]);
    return check;
  }

  resetError() {
    let errors = {};
    errors[("nama", "script", "status")] = "";
    this.setState({ errors: errors });
  }

  checkEmpty(dataForm, fieldName, fileFieldName) {
    let errors = {};
    let formIsValid = true;
    for (const field in fieldName) {
      const nameAttribute = fieldName[field];
      if (dataForm.get(nameAttribute) == "") {
        errors[nameAttribute] = "Tidak Boleh Kosong";
        formIsValid = false;
      }
    }
    if (this.state.script == "") {
      errors["script"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.status == "") {
      errors["status"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  handleSubmitAction(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });
      const dataFormSubmit = new FormData();
      let script = this.state.script;
      let script_clean = script.replace(/'/g, '"');
      let nama = dataForm.get("nama");
      let nama_clean = nama.replace(/'/g, "`");
      dataFormSubmit.append("nama", nama_clean);
      dataFormSubmit.append("scripts", script_clean);
      dataFormSubmit.append("status", this.state.status);
      dataFormSubmit.append("user_by", Cookies.get("user_id"));

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/setting/add_aos",
          dataFormSubmit,
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
                allowOutsideClick: false,
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/site-management/setting/addon";
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleChangeNamaAction(e) {
    const errors = this.state.errors;
    errors["nama"] = "";

    this.setState({
      errors,
    });
  }

  handleChangeScriptAction(e) {
    const errors = this.state.errors;
    errors["script"] = "";

    const script = e.currentTarget.value;
    this.setState({
      script,
      errors,
    });
  }

  handleChangeStatusAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["status"] = "";

    this.setState({
      status: selectedOption.value,
      valStatus: { value: selectedOption.value, label: selectedOption.label },
      errors,
    });
  };

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Add-On Script
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.kembali}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
              {/* <a href="https://back.dev.sdmdigital.id/api/report-pelatihan" className="btn btn-primary btn-sm me-2">
                <i className="las la-file-export"></i>
                Export
              </a>
              <a href="/pelatihan/tambah-pelatihan" className="btn btn-primary btn-sm">
                <i className="las la-plus"></i>
                Tambah Pelatihan
              </a> */}
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Tambah Add-On Script
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <form
                        className="form"
                        action="#"
                        onSubmit={this.handleSubmit}
                      >
                        <input
                          type="hidden"
                          name="csrf-token"
                          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                        />
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">Nama</label>
                          <input
                            className="form-control form-control-sm"
                            placeholder="Masukkan Nama Disini"
                            name="nama"
                            onChange={this.handleChangeNama}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["nama"]}
                          </span>
                        </div>
                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">Script</label>
                          <textarea
                            className="form-control form-control-sm"
                            placeholder="Masukkan Script Disini"
                            name="script"
                            rows={5}
                            value={this.state.script}
                            onChange={this.handleChangeScript}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["script"]}
                          </span>
                        </div>
                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">Status</label>
                          <Select
                            name="status"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            options={this.optionstatus}
                            value={this.state.valStatus}
                            onChange={this.handleChangeStatus}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["status"]}
                          </span>
                        </div>
                        <div className="text-center pt-10 my-7">
                          <button
                            onClick={this.kembali}
                            type="reset"
                            className="btn btn-light btn-md me-3"
                            data-kt-menu-dismiss="true"
                          >
                            Batal
                          </button>
                          <button
                            type="submit"
                            className="btn btn-primary btn-md"
                            id="submitQuestion1"
                            disabled={this.state.isLoading}
                          >
                            {this.state.isLoading ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm me-2"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Loading...</span>
                                Loading...
                              </>
                            ) : (
                              <>
                                <i className="fa fa-paper-plane me-1"></i>Simpan
                              </>
                            )}
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
