import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../SettingMenu-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const SettingMenu = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default SettingMenu;
