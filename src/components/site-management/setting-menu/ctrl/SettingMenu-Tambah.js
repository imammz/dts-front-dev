import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../SettingMenu-Tambah-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const SettingMenuTambah = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default SettingMenuTambah;
