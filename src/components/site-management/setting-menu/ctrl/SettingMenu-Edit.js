import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../SettingMenu-Edit-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const SettingMenuEdit = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default SettingMenuEdit;
