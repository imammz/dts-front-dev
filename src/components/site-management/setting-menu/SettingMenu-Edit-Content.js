import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";

export default class SettingMenuEditContent extends React.Component {
  constructor(props) {
    super(props);
    this.kembali = this.kembaliAction.bind(this);
    this.handleChangeTipe = this.handleChangeTipeAction.bind(this);
    this.handleChangeLevelHeader =
      this.handleChangeLevelHeaderAction.bind(this);
    this.handleChangeLevelFooter =
      this.handleChangeLevelFooterAction.bind(this);
    this.handleSubmit = this.handleSubmitAction.bind(this);
    this.handleChangeParent = this.handleChangeParentAction.bind(this);
    this.handleChangeCluster = this.handleChangeClusterAction.bind(this);
    this.handleChangeTarget = this.handleChangeTargetAction.bind(this);
    this.handleChangeStatus = this.handleChangeStatusAction.bind(this);
    this.handleChangeNama = this.handleChangeNamaAction.bind(this);
    this.handleChangeExternal = this.handleChangeExternalAction.bind(this);
    this.handleChangeUrl = this.handleChangeUrlAction.bind(this);
    this.handleDelete = this.handleDeleteAction.bind(this);
  }

  state = {
    id_menu: false,
    isLoading: false,
    datax: [],
    loading: false,
    totalRows: "",
    tempLastNumber: 0,
    column: "",
    sortDirection: "ASC",
    errors: {},
    tipe: false,
    id_tipe: false,
    valLevelHeader: [],
    level_header: 287,
    valLevelFooter: [],
    level_footer: 285,
    parent: false,
    valParent: [],
    cluster: false,
    valCluster: [],
    option_level: [],
    option_parent: [],
    option_cluster: [],
    target: false,
    status: false,
    valStatus: [],
    valTarget: [],
    nama: false,
    lvl_menu: false,
    id_head: false,
    target: false,
    status: false,
    jenis_menu: false,
    is_external_url: false,
    url: "",
    option_external: [
      { value: 0, label: "Tidak" },
      { value: 1, label: "Ya" },
    ],
  };

  option_level_header = [
    { value: 287, label: "Parent Menu" },
    { value: 288, label: "Menu" },
  ];
  option_level_footer = [
    { value: 285, label: "Cluster Link" },
    { value: 286, label: "Link" },
  ];
  option_target = [
    { value: 301, label: "Blank" },
    { value: 300, label: "Current" },
  ];
  option_status = [
    { value: 0, label: "Unpublish" },
    { value: 1, label: "Publish" },
  ];

  //parent dummy
  option_parent = [
    { value: 0, label: "Jadwal" },
    { value: 1, label: "Rilis Media" },
    { value: 2, label: "Event" },
  ];

  option_cluster = [
    { value: 0, label: "Pranala" },
    { value: 1, label: "Informasi" },
  ];

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    let segment_url = window.location.pathname.split("/");
    let id_menu = segment_url[5];
    this.setState({
      id_menu,
    });

    const data_post = { id: id_menu };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/setting/view_m_jnsmenu",
        null,
        this.configs,
      )
      .then((res) => {
        swal.close();
        const result = res.data.result;
        const status = result.Status;

        if (status) {
          const data = result.Data;

          this.setState(
            {
              option_level: data,
            },
            () => {
              axios
                .post(
                  process.env.REACT_APP_BASE_API_URI + "/setting/detail_menu",
                  data_post,
                  this.configs,
                )
                .then((res) => {
                  const statusx = res.data.result.Status;
                  const messagex = res.data.result.Message;
                  if (statusx) {
                    swal.close();
                    const datax = res.data.result.Data[0];
                    const jenis_menu = datax.jenis_menu;
                    const nama = datax.nama;
                    const lvl_menu = datax.lvl_menu;
                    const id_head = datax.id_head;
                    const target = datax.target;
                    const status = datax.status;
                    const id_tipe = jenis_menu;
                    const is_external_url = datax.is_url;
                    let url = datax.urlink;
                    if (is_external_url == 0) {
                      url = "";
                    }

                    let valTarget = [];
                    this.option_target.forEach(function (element) {
                      if (element.value == target) {
                        valTarget = {
                          value: element.value,
                          label: element.label,
                        };
                      }
                    });

                    let valStatus = [];
                    this.option_status.forEach(function (element) {
                      if (element.value == status) {
                        valStatus = {
                          value: element.value,
                          label: element.label,
                        };
                      }
                    });

                    let tipe = "Footer";
                    if (id_tipe == 281) {
                      //jika header
                      const level_header = lvl_menu;
                      tipe = "Header";
                      let label = "";
                      this.option_level_header.forEach(function (element) {
                        if (element.value == lvl_menu) {
                          label = element.label;
                        }
                      });
                      const valLevelHeader = { value: lvl_menu, label: label };

                      this.setState({
                        level_header,
                        valLevelHeader,
                      });
                    } else {
                      //jika footer
                      const level_footer = lvl_menu;
                      let label = "";
                      this.option_level_footer.forEach(function (element) {
                        if (element.value == lvl_menu) {
                          label = element.label;
                        }
                      });
                      const valLevelFooter = { value: lvl_menu, label: label };
                      this.setState({
                        level_footer,
                        valLevelFooter,
                      });
                    }

                    this.setState(
                      {
                        jenis_menu,
                        nama,
                        lvl_menu,
                        id_head,
                        target,
                        status,
                        id_tipe,
                        tipe,
                        valTarget,
                        valStatus,
                        is_external_url,
                        url,
                      },
                      () => {
                        let dataBody = {
                          mulai: 0,
                          limit: 100,
                          cari: "",
                          sort: "id desc",
                        };

                        axios
                          .post(
                            process.env.REACT_APP_BASE_API_URI +
                              "/setting/view_menu",
                            dataBody,
                            this.configs,
                          )
                          .then((res) => {
                            const datax = res.data.result.Data;
                            const statusx = res.data.result.Status;
                            const jumlahdata =
                              res.data.result.TotalLength[0].jml_data;
                            if (statusx && jumlahdata > 0) {
                              const option_parent = [];
                              const option_cluster = [];
                              datax.forEach(function (element, i) {
                                if (
                                  element.jenis_menu == "Header" &&
                                  element.level_menu == "Parent Menu"
                                ) {
                                  const valOption = {
                                    value: element.id,
                                    label: element.nama_menu,
                                  };
                                  option_parent.push(valOption);
                                } else if (
                                  element.jenis_menu == "Footer" &&
                                  element.level_menu == "Cluster Link"
                                ) {
                                  const valOption = {
                                    value: element.id,
                                    label: element.nama_menu,
                                  };
                                  option_cluster.push(valOption);
                                }
                              });
                              this.setState(
                                {
                                  option_parent,
                                  option_cluster,
                                },
                                () => {
                                  const id_head = this.state.id_head;
                                  console.log(this.state.lvl_menu);
                                  if (this.state.lvl_menu == 288) {
                                    let label = "";
                                    this.state.option_parent.forEach(
                                      function (element) {
                                        if (element.value == id_head) {
                                          label = element.label;
                                        }
                                      },
                                    );
                                    const valParent = {
                                      value: this.state.id_head,
                                      label: label,
                                    };
                                    const parent = id_head;
                                    this.setState({
                                      parent,
                                      valParent,
                                    });
                                  } else if (this.state.lvl_menu == 286) {
                                    let label = "";
                                    this.state.option_cluster.forEach(
                                      function (element) {
                                        if (element.value == id_head) {
                                          label = element.label;
                                        }
                                      },
                                    );
                                    const valCluster = {
                                      value: this.state.id_head,
                                      label: label,
                                    };
                                    const cluster = id_head;
                                    this.setState({
                                      cluster,
                                      valCluster,
                                    });
                                  }
                                },
                              );
                            } else {
                              const option_parent = [];
                              const option_cluster = [];
                              this.setState({
                                option_parent,
                                option_cluster,
                              });
                            }
                          })
                          .catch((error) => {
                            const option_parent = [];
                            const option_cluster = [];
                            this.setState({
                              option_parent,
                              option_cluster,
                            });
                          });
                      },
                    );
                  }
                })
                .catch((error) => {
                  let statux = error.response.data.result.Status;
                  let messagex = error.response.data.result.Message;
                  if (!statux) {
                    swal
                      .fire({
                        title: messagex,
                        icon: "warning",
                        confirmButtonText: "Ok",
                      })
                      .then((result) => {
                        if (result.isConfirmed) {
                        }
                      });
                  }
                });

              this.state.option_level.map(({ id, nama }, index) => {});
            },
          );
        }
      });
  }

  kembaliAction() {
    window.history.back();
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const check = this.checkEmpty(
      dataForm,
      ["nama", "level", "status", "target"],
      [""],
    );
    return check;
  }

  resetError() {
    let errors = {};
    errors[
      ("tipe",
      "nama",
      "level",
      "status",
      "target",
      "parent",
      "external_url",
      "url")
    ] = "";
    this.setState({ errors: errors });
  }

  validURL(str) {
    var pattern = new RegExp(
      "^(https?:\\/\\/)?" + // protocol
        "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
        "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
        "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
        "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
        "(\\#[-a-z\\d_]*)?$",
      "i",
    ); // fragment locator
    return !!pattern.test(str);
  }

  checkEmpty(dataForm, fieldName, fileFieldName) {
    let errors = {};
    let formIsValid = true;
    for (const field in fieldName) {
      const nameAttribute = fieldName[field];
      if (dataForm.get(nameAttribute) == "") {
        errors[nameAttribute] = "Tidak Boleh Kosong";
        formIsValid = false;
      }
    }

    if (this.state.tipe == false) {
      errors["tipe"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.is_external_url.length == 0) {
      errors["external_url"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.is_external_url == 1 && this.state.url == "") {
      errors["url"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (
      this.state.is_external_url == 1 &&
      !this.validURL(dataForm.get("url"))
    ) {
      errors["url"] = "Bukan Valid URL";
      formIsValid = false;
    }

    if (this.state.level_footer == 286 && this.state.cluster == "") {
      errors["parent"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }
    if (this.state.level_header == 288 && this.state.parent == "") {
      errors["parent"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.jenis_menu == 281 && this.state.level_header == "") {
      errors["level"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.jenis_menu == 282 && this.state.level_footer == "") {
      errors["level"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  handleChangeTipeAction(e) {
    this.resetError();
    const tipe = e.currentTarget.value;
    const id_tipe = e.currentTarget.getAttribute("id_level");

    this.setState({
      tipe,
      id_tipe,
      jenis_menu: id_tipe,
    });
  }

  handleChangeExternalAction(e) {
    const errors = this.state.errors;
    errors["external_url"] = "";
    this.setState({
      errors,
    });

    const is_external_url = e.currentTarget.value;
    this.setState({
      is_external_url,
    });
  }

  handleChangeLevelHeaderAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["level"] = "";
    this.setState({
      errors,
    });

    this.setState(
      {
        level_header: selectedOption.value,
        valLevelHeader: {
          value: selectedOption.value,
          label: selectedOption.label,
        },
      },
      () => {},
    );
  };

  handleChangeLevelFooterAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["level"] = "";
    this.setState({
      errors,
    });

    this.setState(
      {
        level_footer: selectedOption.value,
        valLevelFooter: {
          value: selectedOption.value,
          label: selectedOption.label,
        },
      },
      () => {},
    );
  };

  handleChangeParentAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["parent"] = "";
    this.setState({
      errors,
    });

    this.setState({
      parent: selectedOption.value,
      valParent: { value: selectedOption.value, label: selectedOption.label },
    });
  };

  handleChangeClusterAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["parent"] = "";
    this.setState({
      errors,
    });

    this.setState({
      cluster: selectedOption.value,
      valCluster: { value: selectedOption.value, label: selectedOption.label },
    });
  };

  handleChangeStatusAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["status"] = "";
    this.setState({
      errors,
    });

    this.setState({
      status: selectedOption.value,
      valStatus: { value: selectedOption.value, label: selectedOption.label },
    });
  };

  handleChangeTargetAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["target"] = "";
    this.setState({
      errors,
    });

    this.setState({
      target: selectedOption.value,
      valTarget: { value: selectedOption.value, label: selectedOption.label },
    });
  };

  handleChangeUrlAction(e) {
    const url = e.currentTarget.value;
    this.setState({
      url,
    });
  }

  handleSubmitAction(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });
      let level_menu = false;
      if (this.state.jenis_menu == 281) {
        level_menu = this.state.level_header;
      } else if (this.state.jenis_menu == 282) {
        level_menu = this.state.level_footer;
      }

      let id_head = 0;

      if (level_menu == 288) {
        id_head = this.state.parent;
      } else if (level_menu == 286) {
        id_head = this.state.cluster;
      }

      const dataFormSubmit = new FormData();
      dataFormSubmit.append("id_menu", this.state.id_menu);
      dataFormSubmit.append("jns_menu", this.state.jenis_menu);
      dataFormSubmit.append("nama", dataForm.get("nama"));
      dataFormSubmit.append("lvl_menu", level_menu);
      dataFormSubmit.append("id_head", id_head);
      dataFormSubmit.append("target", this.state.target);
      dataFormSubmit.append("status", this.state.status);
      dataFormSubmit.append("is_url", this.state.is_external_url);
      dataFormSubmit.append("urlink", this.state.url);
      dataFormSubmit.append("user_by", Cookies.get("user_id"));

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/setting/edit_menu",
          dataFormSubmit,
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
                allowOutsideClick: false,
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/site-management/setting/menu";
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      this.setState({ isLoading: false });
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleChangeNamaAction(e) {
    const errors = this.state.errors;
    errors["nama"] = "";
    this.setState({
      errors,
    });

    const nama = e.currentTarget.value;
    this.setState({
      nama,
    });
  }

  handleDeleteAction(e) {
    e.preventDefault();
    const idx = this.state.id_menu;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          const data = { id: idx };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/setting/del_menu",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              //const messagex = res.data.result.Message;
              const data = res.data.result.Data;
              const messagex = data[0].message;
              const statusDelete = data[0].statuscode;
              if (statux && statusDelete != "500") {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                    allowOutsideClick: false,
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      window.history.back();
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmationButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            });
        }
      });
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Menu
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.kembali}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>

                <a
                  href="#"
                  title="Hapus"
                  className="btn btn-sm btn-danger btn-active-light-info"
                  onClick={this.handleDelete}
                >
                  <i className="bi bi-trash-fill text-white"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </a>
              </div>
              {/* <a href="https://back.dev.sdmdigital.id/api/report-pelatihan" className="btn btn-primary btn-sm me-2">
                <i className="las la-file-export"></i>
                Export
              </a>
              <a href="/pelatihan/tambah-pelatihan" className="btn btn-primary btn-sm">
                <i className="las la-plus"></i>
                Tambah Pelatihan
              </a> */}
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Edit Menu
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <form
                        className="form"
                        action="#"
                        onSubmit={this.handleSubmit}
                      >
                        <input
                          type="hidden"
                          name="csrf-token"
                          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                        />
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">
                            Jenis Menu
                          </label>
                          <div className="d-flex">
                            {this.state.option_level.map(
                              ({ id, nama }, index) => {
                                return (
                                  <div
                                    className="form-check form-check-sm form-check-custom form-check-solid me-5"
                                    key={id}
                                  >
                                    <input
                                      checked={
                                        this.state.jenis_menu == id
                                          ? true
                                          : false
                                      }
                                      className="form-check-input"
                                      onChange={this.handleChangeTipe}
                                      type="radio"
                                      name="jenis_pertanyaan"
                                      value={nama}
                                      id_level={id}
                                      id={`jenis_pertanyaan{index}`}
                                    />

                                    <label
                                      className="form-check-label"
                                      htmlFor="jenis_pertanyaan1"
                                    >
                                      {nama}
                                    </label>
                                  </div>
                                );
                              },
                            )}
                          </div>
                          <span style={{ color: "red" }}>
                            {this.state.errors["tipe"]}
                          </span>
                        </div>
                        {this.state.tipe == "Header" ? (
                          <div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Nama Menu
                              </label>
                              <input
                                className="form-control form-control-sm"
                                placeholder="Masukkan Nama Menu Disini"
                                name="nama"
                                value={this.state.nama}
                                onChange={this.handleChangeNama}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["nama"]}
                              </span>
                            </div>
                            <div className="form-group fv-row mb-7">
                              <label className="form-label required">
                                Level Menu Header
                              </label>
                              <Select
                                name="level"
                                placeholder="Silahkan pilih"
                                noOptionsMessage={({ inputValue }) =>
                                  !inputValue
                                    ? this.state.datax
                                    : "Data tidak tersedia"
                                }
                                className="form-select-sm selectpicker p-0"
                                options={this.option_level_header}
                                value={this.state.valLevelHeader}
                                onChange={this.handleChangeLevelHeader}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["level"]}
                              </span>
                            </div>
                            <div className="form-group fv-row mb-7">
                              <label className="form-label required">
                                Target
                              </label>
                              <Select
                                name="target"
                                placeholder="Silahkan pilih"
                                noOptionsMessage={({ inputValue }) =>
                                  !inputValue
                                    ? this.state.datax
                                    : "Data tidak tersedia"
                                }
                                className="form-select-sm selectpicker p-0"
                                options={this.option_target}
                                value={this.state.valTarget}
                                onChange={this.handleChangeTarget}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["target"]}
                              </span>
                            </div>

                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                External URL
                              </label>
                              <div className="d-flex">
                                {this.state.option_external.map(
                                  ({ value, label }, index) => {
                                    return (
                                      <div
                                        className="form-check form-check-sm form-check-custom form-check-solid me-5"
                                        key={value}
                                      >
                                        <input
                                          className="form-check-input"
                                          onChange={this.handleChangeExternal}
                                          type="radio"
                                          name="external_url"
                                          value={value}
                                          checked={
                                            this.state.is_external_url == value
                                              ? true
                                              : false
                                          }
                                          id={`external_url{value}`}
                                        />
                                        <label
                                          className="form-check-label"
                                          htmlFor="jenis_pertanyaan1"
                                        >
                                          {label}
                                        </label>
                                      </div>
                                    );
                                  },
                                )}
                              </div>
                              <span style={{ color: "red" }}>
                                {this.state.errors["external_url"]}
                              </span>
                            </div>

                            {this.state.is_external_url == 1 ? (
                              <div className="col-lg-6 mb-7 fv-row">
                                <label className="form-label required">
                                  URL
                                </label>
                                <input
                                  className="form-control form-control-sm"
                                  placeholder="Masukkan URL External Disini"
                                  name="url"
                                  value={this.state.url}
                                  onChange={this.handleChangeUrl}
                                />
                                <span style={{ color: "red" }}>
                                  {this.state.errors["url"]}
                                </span>
                              </div>
                            ) : (
                              ""
                            )}

                            {this.state.level_header != 287 ? (
                              <div>
                                <div className="form-group fv-row mb-7">
                                  <label className="form-label required">
                                    Parent
                                  </label>
                                  <Select
                                    name="parent"
                                    placeholder="Silahkan pilih"
                                    noOptionsMessage={({ inputValue }) =>
                                      !inputValue
                                        ? this.state.datax
                                        : "Data tidak tersedia"
                                    }
                                    className="form-select-sm selectpicker p-0"
                                    options={this.state.option_parent}
                                    value={this.state.valParent}
                                    onChange={this.handleChangeParent}
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["parent"]}
                                  </span>
                                </div>
                              </div>
                            ) : (
                              ""
                            )}

                            <div className="form-group fv-row mb-7">
                              <label className="form-label required">
                                Status
                              </label>
                              <Select
                                name="status"
                                placeholder="Silahkan pilih"
                                noOptionsMessage={({ inputValue }) =>
                                  !inputValue
                                    ? this.state.datax
                                    : "Data tidak tersedia"
                                }
                                className="form-select-sm selectpicker p-0"
                                options={this.option_status}
                                value={this.state.valStatus}
                                onChange={this.handleChangeStatus}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["status"]}
                              </span>
                            </div>
                          </div>
                        ) : (
                          ""
                        )}
                        {this.state.tipe == "Footer" ? (
                          <div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Nama Menu
                              </label>
                              <input
                                className="form-control form-control-sm"
                                placeholder="Masukkan Nama Menu Disini"
                                name="nama"
                                onChange={this.handleChangeNama}
                                value={this.state.nama}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["nama"]}
                              </span>
                            </div>
                            <div className="form-group fv-row mb-7">
                              <label className="form-label required">
                                Level Menu Footer
                              </label>
                              <Select
                                name="level"
                                placeholder="Silahkan pilih"
                                noOptionsMessage={({ inputValue }) =>
                                  !inputValue
                                    ? this.state.datax
                                    : "Data tidak tersedia"
                                }
                                className="form-select-sm selectpicker p-0"
                                options={this.option_level_footer}
                                value={this.state.valLevelFooter}
                                onChange={this.handleChangeLevelFooter}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["level"]}
                              </span>
                            </div>
                            <div className="form-group fv-row mb-7">
                              <label className="form-label required">
                                Target
                              </label>
                              <Select
                                name="target"
                                placeholder="Silahkan pilih"
                                noOptionsMessage={({ inputValue }) =>
                                  !inputValue
                                    ? this.state.datax
                                    : "Data tidak tersedia"
                                }
                                className="form-select-sm selectpicker p-0"
                                options={this.option_target}
                                value={this.state.valTarget}
                                onChange={this.handleChangeTarget}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["target"]}
                              </span>
                            </div>

                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                External URL
                              </label>
                              <div className="d-flex">
                                {this.state.option_external.map(
                                  ({ value, label }, index) => {
                                    return (
                                      <div
                                        className="form-check form-check-sm form-check-custom form-check-solid me-5"
                                        key={value}
                                      >
                                        <input
                                          className="form-check-input"
                                          onChange={this.handleChangeExternal}
                                          type="radio"
                                          name="external_url"
                                          checked={
                                            this.state.is_external_url == value
                                              ? true
                                              : false
                                          }
                                          value={value}
                                          id={`external_url{value}`}
                                        />
                                        <label
                                          className="form-check-label"
                                          htmlFor="jenis_pertanyaan1"
                                        >
                                          {label}
                                        </label>
                                      </div>
                                    );
                                  },
                                )}
                              </div>
                              <span style={{ color: "red" }}>
                                {this.state.errors["external_url"]}
                              </span>
                            </div>

                            {this.state.is_external_url == 1 ? (
                              <div className="col-lg-6 mb-7 fv-row">
                                <label className="form-label required">
                                  URL
                                </label>
                                <input
                                  className="form-control form-control-sm"
                                  placeholder="Masukkan URL External Disini"
                                  name="url"
                                  value={this.state.url}
                                  onChange={this.handleChangeUrl}
                                />
                                <span style={{ color: "red" }}>
                                  {this.state.errors["url"]}
                                </span>
                              </div>
                            ) : (
                              ""
                            )}

                            {this.state.level_footer != 285 ? (
                              <div>
                                <div className="form-group fv-row mb-7">
                                  <label className="form-label required">
                                    Target Cluster
                                  </label>
                                  <Select
                                    name="parent"
                                    placeholder="Silahkan pilih"
                                    noOptionsMessage={({ inputValue }) =>
                                      !inputValue
                                        ? this.state.datax
                                        : "Data tidak tersedia"
                                    }
                                    className="form-select-sm selectpicker p-0"
                                    options={this.state.option_cluster}
                                    value={this.state.valCluster}
                                    onChange={this.handleChangeCluster}
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["parent"]}
                                  </span>
                                </div>
                              </div>
                            ) : (
                              ""
                            )}
                            <div className="form-group fv-row mb-7">
                              <label className="form-label required">
                                Status
                              </label>
                              <Select
                                name="status"
                                placeholder="Silahkan pilih"
                                noOptionsMessage={({ inputValue }) =>
                                  !inputValue
                                    ? this.state.datax
                                    : "Data tidak tersedia"
                                }
                                className="form-select-sm selectpicker p-0"
                                options={this.option_status}
                                value={this.state.valStatus}
                                onChange={this.handleChangeStatus}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["status"]}
                              </span>
                            </div>
                          </div>
                        ) : (
                          ""
                        )}
                        <div className="text-center">
                          <button
                            onClick={this.kembali}
                            type="reset"
                            className="btn btn-light btn-md me-3"
                            data-kt-menu-dismiss="true"
                          >
                            Batal
                          </button>
                          <button
                            type="submit"
                            className="btn btn-primary btn-md"
                            id="submitQuestion1"
                            disabled={this.state.isLoading}
                          >
                            {this.state.isLoading ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm me-2"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Loading...</span>
                                Loading...
                              </>
                            ) : (
                              <>
                                <i className="fa fa-paper-plane me-1"></i>Simpan
                              </>
                            )}
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
