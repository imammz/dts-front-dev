import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../SettingPage-Preview-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const SettingPagePreview = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default SettingPagePreview;
