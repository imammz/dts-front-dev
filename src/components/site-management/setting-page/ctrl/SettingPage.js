import React from "react";
import Header from "../../../../components/Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../SettingPage-Content";
import SideNav from "../../../../components/SideNav";
import Footer from "../../../../components/Footer";

const SettingPage = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default SettingPage;
