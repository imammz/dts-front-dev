import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../SettingPage-Edit-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const SettingPageEdit = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default SettingPageEdit;
