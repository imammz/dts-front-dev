import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import ImageCard from "./components/ImageCard";
import imageCompression from "browser-image-compression";
import "jodit";
import "jodit/build/jodit.min.css";
import JoditEditor from "jodit-react";

const resizeFile = async (file) => {
  const options = {
    maxSizeMB: 5,
    maxWidthOrHeight: 800,
    useWebWorker: true,
  };

  return imageCompression(file, options);
};

export default class SettingPageTambahContent extends React.Component {
  constructor(props) {
    super(props);
    this.isiForm = this.isiFormAction.bind(this);
    this.kembaliPilihTemplate = this.kembaliPilihTemplateAction.bind(this);
    this.kembali = this.kembaliAction.bind(this);
    this.handleChangeIsi = this.handleChangeIsiAction.bind(this);
    this.handleChangeIsiChange = this.handleChangeIsiChangeAction.bind(this);
    this.handleClick = this.handleClickAction.bind(this);
    this.handleChangeStatus = this.handleChangeStatusAction.bind(this);
    this.handleChangeImage = this.handleChangeImageAction.bind(this);
    this.handleChangeMenu = this.handleChangeMenuAction.bind(this);
    this.handleChangeName = this.handleChangeNameAction.bind(this);
    this.handleChangeTitle = this.handleChangeTitleAction.bind(this);
  }

  configJodit = {
    placeholderText: "Edit Your Content Here!",
    charCounterCount: false,
    minHeight: 450,
    useIframeResizer: false,
    uploader: {
      url: process.env.REACT_APP_BASE_API_URI + "/publikasi/jodit-upload-image",
    },
    buttonsSM: [
      "source",
      "|",
      "bold",
      "strikethrough",
      "underline",
      "italic",
      "|",
      "ul",
      "ol",
      "|",
      "outdent",
      "indent",
      "|",
      "font",
      "fontsize",
      "brush",
      "paragraph",
      "|",
      "image",
      "video",
      "table",
      "link",
      "|",
      "align",
      "undo",
      "redo",
      "|",
      "hr",
      "eraser",
      "copyformat",
      "|",
      "symbol",
      "fullsize",
      "print",
      "about",
    ],
  };

  state = {
    datax: [],
    loading: false,
    totalRows: "",
    isLoading: false,
    tempLastNumber: 0,
    column: "",
    sortDirection: "ASC",
    is_pilih_template: true,
    tipe_template: 0,
    errors: {},
    content: "",
    image: false,
    status: 1,
    valStatus: [],
    valMenu: [],
    id_menu: false,
    menu: [],
  };

  jodit;
  setRef = (jodit) => (this.jodit = jodit);

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  status = [
    { value: 1, label: "Listed" },
    { value: 0, label: "Unlisted" },
  ];

  styles = {
    template: {
      backgroundColor: "rgb(242, 247, 252)",
      border: "dashed rgb(173, 181, 189)",
    },
    template2_content: {
      backgroundColor: "rgb(242, 247, 252)",
      border: "dashed rgb(173, 181, 189)",
      height: "200px",
      lineHeight: "200px",
    },
    template3_image: {
      backgroundColor: "rgb(242, 247, 252)",
      border: "dashed rgb(173, 181, 189)",
      height: "100px",
    },
    template3_content: {
      backgroundColor: "rgb(242, 247, 252)",
      border: "dashed rgb(173, 181, 189)",
      paddingTop: "66px",
      height: "215px",
      lineHeight: "215px",
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    let dataBody = {
      mulai: 0,
      limit: 100,
      cari: "",
      sort: "id desc",
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/setting/view_menu_non",
        dataBody,
        this.configs,
      )
      .then((res) => {
        const datax = res.data.result.Data;
        const statusx = res.data.result.Status;
        if (statusx) {
          const menu = [];
          datax.forEach(function (element, i) {
            if (element.level_menu == "Link" || element.level_menu == "Menu") {
              if (element.status == "Publish") {
                const valMenu = { value: element.id, label: element.nama };
                menu.push(valMenu);
              }
            }
          });
          this.setState({
            menu,
          });
        } else {
          const menu = [];
          this.setState({
            menu,
          });
        }
      })
      .catch((error) => {
        const option_parent = [];
        const option_cluster = [];
        this.setState({
          option_parent,
          option_cluster,
        });
      });
  }

  isiFormAction(e) {
    e.preventDefault();
    const tipe_template = e.currentTarget.getAttribute("tipe_template");
    this.resetError();
    this.setState({
      is_pilih_template: false,
      tipe_template,
    });
  }

  kembaliPilihTemplateAction() {
    this.setState({
      is_pilih_template: true,
    });
  }

  kembaliAction() {
    window.history.back();
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const check = this.checkEmpty(
      dataForm,
      ["name", "title", "status", "menu"],
      [""],
    );
    return check;
  }

  resetError() {
    let errors = {};
    errors[("name", "title", "status", "image", "isi", "menu")] = "";
    this.setState({ errors: errors });
  }

  checkEmpty(dataForm, fieldName, fileFieldName) {
    let errors = {};
    let formIsValid = true;
    for (const field in fieldName) {
      const nameAttribute = fieldName[field];
      if (dataForm.get(nameAttribute) == "") {
        errors[nameAttribute] = "Tidak Boleh Kosong";
        formIsValid = false;
      }
    }

    if (this.state.content == "") {
      errors["isi"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.tipe_template != 2 && this.state.image == false) {
      errors["image"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  handleChangeIsiAction(value) {
    const errors = this.state.errors;
    errors["isi"] = "";

    this.setState(
      {
        content: value,
        errors,
      },
      () => {},
    );
  }

  handleChangeIsiChangeAction(value) {
    const errors = this.state.errors;
    errors["isi"] = "";

    this.setState({
      errors,
    });
  }

  handleClickAction(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });
      let timestamp = 1293683278;
      let date = new Date(timestamp * 1000);
      let ymdhis = date
        .toISOString()
        .match(/(\d{4}\-\d{2}\-\d{2})T(\d{2}:\d{2}:\d{2})/);
      let created_at = ymdhis[1] + " " + ymdhis[2];

      let title = dataForm.get("title").toLowerCase();
      let arr_title = title.split(" ");
      let slug = arr_title.join("-");
      //slug = slug + "-" + ymdhis[1] + '-' + ymdhis[2];

      let img = "";
      if (this.state.image) {
        img = this.state.image;
      }

      const property_template = [
        {
          title: dataForm.get("title"),
          content: this.state.content,
          image: img,
        },
      ];

      const bodyJson = [
        {
          created_at: created_at,
          created_by: Cookies.get("user_id"),
          name: dataForm.get("name"),
          id_menu: this.state.id_menu,
          url: slug,
          template_type: this.state.tipe_template,
          status: this.state.status,
          property_template: property_template,
        },
      ];

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/setting/add_page",
          bodyJson,
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message.message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
                allowOutsideClick: false,
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/site-management/setting/page";
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message.message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      this.setState({ isLoading: false });
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleChangeStatusAction(val) {
    const errors = this.state.errors;
    errors["status"] = "";
    this.setState({
      errors,
    });

    this.setState({
      valStatus: {
        label: val.label,
        value: val.value,
      },
      status: val.value,
    });
  }

  handleChangeImageAction(e) {
    const errors = this.state.errors;
    errors["image"] = "";
    this.setState({
      errors,
    });

    const imageFile = e.target.files[0];
    const reader = new FileReader();
    resizeFile(imageFile).then((image) => {
      reader.readAsDataURL(image);
      const context = this;
      reader.onload = function () {
        let result = reader.result;
        context.setState({ image: result });
      };
    });
  }

  handleChangeMenuAction(val) {
    const errors = this.state.errors;
    errors["menu"] = "";

    this.setState({
      valMenu: {
        label: val.label,
        value: val.value,
      },
      id_menu: val.value,
      errors,
    });
  }

  handleChangeNameAction(e) {
    const errors = this.state.errors;
    errors["name"] = "";
    this.setState({
      errors,
    });
  }

  handleChangeTitleAction(e) {
    const errors = this.state.errors;
    errors["title"] = "";
    this.setState({
      errors,
    });
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Page
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.kembali}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
              {/* <a href="https://back.dev.sdmdigital.id/api/report-pelatihan" className="btn btn-primary btn-sm me-2">
                <i className="las la-file-export"></i>
                Export
              </a>
              <a href="/pelatihan/tambah-pelatihan" className="btn btn-primary btn-sm">
                <i className="las la-plus"></i>
                Tambah Pelatihan
              </a> */}
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Tambah Page
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      {this.state.is_pilih_template ? (
                        <div className="pb-8">
                          <h5 className="me-3 mr-2 text-muted mb-10">
                            Pilih Template
                          </h5>
                          <div className="row d-flex justify-content-center mt-4">
                            <div className="card col-lg-4 rounded shadow text-center">
                              <div className="">
                                <div className="card-body">
                                  <div
                                    className="container text-center mt-5 mb-4"
                                    style={this.styles.template}
                                  >
                                    <h3 className="py-3 text-gray font-weight-bolder">
                                      Title Page
                                    </h3>
                                  </div>
                                  <div
                                    className="container text-center mb-4"
                                    style={this.styles.template}
                                  >
                                    <h3 className="py-10 text-gray font-weight-bolder">
                                      Image
                                    </h3>
                                  </div>
                                  <div
                                    className="container text-center"
                                    style={this.styles.template}
                                  >
                                    <h3 className="py-10 text-gray font-weight-bolder">
                                      Content
                                    </h3>
                                  </div>
                                  <div className="text-center my-7">
                                    <a
                                      className="btn btn-sm btn-primary ps-10 pe-10"
                                      href="#"
                                      tipe_template={1}
                                      onClick={this.isiForm}
                                    >
                                      Pilih Template 1
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="card col-lg-4 rounded shadow text-center">
                              <div className="">
                                <div className="card-body">
                                  <div
                                    className="container text-center mt-5 mb-4"
                                    style={this.styles.template}
                                  >
                                    <h3 className="py-3 text-gray font-weight-bolder">
                                      Title Page
                                    </h3>
                                  </div>
                                  <div
                                    className="container text-center my-7"
                                    style={this.styles.template2_content}
                                  >
                                    <h3 className="py-10 text-gray font-weight-bolder mt-15">
                                      Content
                                    </h3>
                                  </div>
                                  <div className="text-center my-7">
                                    <a
                                      className="btn btn-sm btn-primary ps-10 pe-10"
                                      href="#"
                                      tipe_template={2}
                                      onClick={this.isiForm}
                                    >
                                      Pilih Template 2
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="card col-lg-4 rounded shadow text-center">
                              <div className="">
                                <div className="card-body">
                                  <div className="row">
                                    <div
                                      className="container text-center mt-5 mb-4 col-md-12"
                                      style={this.styles.template}
                                    >
                                      <h3 className="py-3 text-gray font-weight-bolder">
                                        Title Page
                                      </h3>
                                    </div>
                                  </div>
                                  <div className="row d-flex">
                                    <div
                                      className="container text-center mb-4 col-12 col-md-6 col-xl-5 m-0"
                                      style={this.styles.template3_image}
                                    >
                                      <h3 className="py-10 text-gray font-weight-bolder">
                                        Image
                                      </h3>
                                    </div>
                                    <div
                                      className="container text-center col-12 col-md-6 col-xl-6 mr-0"
                                      style={this.styles.template3_content}
                                    >
                                      <h3 className="py-10 text-gray font-weight-bolder">
                                        Content
                                      </h3>
                                    </div>
                                  </div>
                                  <div className="text-center my-7">
                                    <a
                                      className="btn btn-sm btn-primary ps-10 pe-10"
                                      href="#"
                                      tipe_template={3}
                                      onClick={this.isiForm}
                                    >
                                      Pilih Template 3
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      ) : (
                        <div className="pb-8">
                          {/* form disini */}
                          <h5 className="me-3 mr-2 text-muted mb-10">
                            Template {this.state.tipe_template}
                          </h5>
                          <form
                            className="form"
                            action="#"
                            onSubmit={this.handleClick}
                          >
                            <input
                              type="hidden"
                              name="csrf-token"
                              value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                            />
                            <div className="row">
                              <div className="col-lg-12">
                                <h5 className="mb-3">Page Properties</h5>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Page Name
                                  </label>
                                  <input
                                    className="form-control form-control-sm"
                                    placeholder="Masukkan Page Name"
                                    name="name"
                                    type="text"
                                    onChange={this.handleChangeName}
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["name"]}
                                  </span>
                                </div>
                                <div>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Menu
                                    </label>
                                    <Select
                                      id="menu"
                                      name="menu"
                                      placeholder="Silahkan pilih"
                                      noOptionsMessage={({ inputValue }) =>
                                        !inputValue
                                          ? this.state.datax
                                          : "Data tidak tersedia"
                                      }
                                      className="form-select-sm selectpicker p-0"
                                      options={this.state.menu}
                                      value={this.state.valMenu}
                                      onChange={this.handleChangeMenu}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["menu"]}
                                    </span>
                                  </div>
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Page Status
                                  </label>
                                  <Select
                                    id="status"
                                    name="status"
                                    placeholder="Silahkan pilih"
                                    noOptionsMessage={({ inputValue }) =>
                                      !inputValue
                                        ? this.state.errors
                                        : "Data tidak tersedia"
                                    }
                                    className="form-select-sm selectpicker p-0"
                                    value={this.state.valStatus}
                                    onChange={this.handleChangeStatus}
                                    options={this.status}
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["status"]}
                                  </span>
                                </div>
                              </div>
                              <div className="col-lg-12">
                                <div className="border-top pt-10 mt-5"></div>
                                <h5 className="mb-3">Page Content</h5>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Title Page
                                  </label>
                                  <input
                                    className="form-control form-control-sm"
                                    placeholder="Tulis Judul Halaman"
                                    name="title"
                                    type="text"
                                    onChange={this.handleChangeTitle}
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["title"]}
                                  </span>
                                </div>
                                {this.state.tipe_template != 2 ? (
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Image
                                    </label>
                                    <ImageCard image={this.state.image} />
                                    <input
                                      type="file"
                                      className="form-control form-control-sm font-size-h4"
                                      name="logo_header"
                                      id="thumbnail"
                                      accept=".png,.jpg,.jpeg,.svg"
                                      onChange={this.handleChangeImage}
                                    />
                                    <div>
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["image"]}
                                      </span>
                                    </div>
                                  </div>
                                ) : (
                                  ""
                                )}

                                <div className="form-group fv-row mb-7">
                                  <label className="form-label required">
                                    Content
                                  </label>
                                  <JoditEditor
                                    editorRef={this.setRef}
                                    value={this.state.content}
                                    config={this.configJodit}
                                    onBlur={this.handleChangeIsi}
                                    onChange={this.handleChangeIsiChange}
                                  />

                                  <span style={{ color: "red" }}>
                                    {this.state.errors["isi"]}
                                  </span>
                                </div>

                                <div className="text-center pt-10 my-7">
                                  <button
                                    onClick={this.kembaliPilihTemplate}
                                    type="reset"
                                    className="btn btn-light btn-md me-3"
                                    data-kt-menu-dismiss="true"
                                  >
                                    Batal
                                  </button>
                                  <button
                                    type="submit"
                                    className="btn btn-primary btn-md"
                                    id="submitQuestion1"
                                    disabled={this.state.isLoading}
                                  >
                                    {this.state.isLoading ? (
                                      <>
                                        <span
                                          className="spinner-border spinner-border-sm me-2"
                                          role="status"
                                          aria-hidden="true"
                                        ></span>
                                        <span className="sr-only">
                                          Loading...
                                        </span>
                                        Loading...
                                      </>
                                    ) : (
                                      <>
                                        <i className="fa fa-paper-plane me-1"></i>
                                        Simpan
                                      </>
                                    )}
                                  </button>
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
