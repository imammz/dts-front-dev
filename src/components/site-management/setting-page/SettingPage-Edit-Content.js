import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import ImageCard from "./components/ImageCard";
import "jodit";
import "jodit/build/jodit.min.css";
import JoditEditor from "jodit-react";

import imageCompression from "browser-image-compression";

const resizeFile = async (file) => {
  const options = {
    maxSizeMB: 5,
    maxWidthOrHeight: 800,
    useWebWorker: true,
  };

  return imageCompression(file, options);
};

export default class SettingPageEditContent extends React.Component {
  constructor(props) {
    super(props);
    this.kembali = this.kembaliAction.bind(this);
    this.handleChangeName = this.handleChangeNameAction.bind(this);
    this.handleChangeStatus = this.handleChangeStatusAction.bind(this);
    this.handleChangeTitle = this.handleChangeTitleAction.bind(this);
    this.handleChangeIsi = this.handleChangeIsiAction.bind(this);
    this.handleChangeImage = this.handleChangeImageAction.bind(this);
    this.handleChangeMenu = this.handleChangeMenuAction.bind(this);
    this.handleClick = this.handleClickAction.bind(this);
    this.handleChangeIsiChange = this.handleChangeIsiChangeAction.bind(this);
    this.handleDelete = this.handleDeleteAction.bind(this);
  }

  configJodit = {
    placeholderText: "Edit Your Content Here!",
    charCounterCount: false,
    minHeight: 450,
    useIframeResizer: false,
    uploader: {
      url: process.env.REACT_APP_BASE_API_URI + "/publikasi/jodit-upload-image",
    },
    buttonsSM: [
      "source",
      "|",
      "bold",
      "strikethrough",
      "underline",
      "italic",
      "|",
      "ul",
      "ol",
      "|",
      "outdent",
      "indent",
      "|",
      "font",
      "fontsize",
      "brush",
      "paragraph",
      "|",
      "image",
      "video",
      "table",
      "link",
      "|",
      "align",
      "undo",
      "redo",
      "|",
      "hr",
      "eraser",
      "copyformat",
      "|",
      "symbol",
      "fullsize",
      "print",
      "about",
    ],
  };

  state = {
    datax: [],
    isLoading: false,
    loading: false,
    totalRows: "",
    tempLastNumber: 0,
    column: "",
    sortDirection: "ASC",
    tipe_template: 0,
    errors: {},
    id: null,
    property_template: [],
    name: "",
    valStatus: [],
    status: 0,
    title: "",
    image: false,
    content: "",
    valMenu: [],
    id_menu: false,
    menu: [],
    id_page: false,
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  jodit;
  setRef = (jodit) => (this.jodit = jodit);

  status = [
    { value: 1, label: "Listed" },
    { value: 0, label: "Unlisted" },
  ];

  styles = {
    template: {
      backgroundColor: "rgb(242, 247, 252)",
      border: "dashed rgb(173, 181, 189)",
    },
    template2_content: {
      backgroundColor: "rgb(242, 247, 252)",
      border: "dashed rgb(173, 181, 189)",
      height: "200px",
      lineHeight: "200px",
    },
    template3_image: {
      backgroundColor: "rgb(242, 247, 252)",
      border: "dashed rgb(173, 181, 189)",
      height: "100px",
    },
    template3_content: {
      backgroundColor: "rgb(242, 247, 252)",
      border: "dashed rgb(173, 181, 189)",
      paddingTop: "66px",
      height: "215px",
      lineHeight: "215px",
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    let segment_url = window.location.pathname.split("/");
    let id_page = segment_url[5];
    this.setState({ id_page });

    let dataBody = {
      mulai: 0,
      limit: 100,
      cari: "",
      sort: "id desc",
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/setting/view_menu_non",
        dataBody,
        this.configs,
      )
      .then((res) => {
        const datax = res.data.result.Data;
        const statusx = res.data.result.Status;
        if (statusx) {
          const menu = [];
          datax.forEach(function (element, i) {
            if (element.level_menu == "Link" || element.level_menu == "Menu") {
              if (element.status == "Publish") {
                const valMenu = { value: element.id, label: element.nama };
                menu.push(valMenu);
              }
            }
          });
          this.setState(
            {
              menu,
            },
            () => {
              this.handleReload();
            },
          );
        } else {
          const menu = [];
          swal.close();
          this.setState(
            {
              menu,
            },
            () => {
              this.handleReload();
            },
          );
        }
      })
      .catch((error) => {
        swal.close();
        const option_parent = [];
        const option_cluster = [];
        this.setState({
          option_parent,
          option_cluster,
        });
        const menu = [];

        this.setState(
          {
            menu,
          },
          () => {
            this.handleReload();
          },
        );
      });
  }

  handleReload() {
    const data = { id: this.state.id_page };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/setting/detail_page",
        data,
        this.configs,
      )
      .then((res) => {
        swal.close();
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statusx) {
          const datax = res.data.result.Data[0];
          let property_template = JSON.parse(datax.property_template);
          property_template = property_template[0];
          const name = datax.name;
          const status = datax.status;
          const title = property_template.title;
          let image = false;
          if (property_template.image) {
            image = property_template.image;
          }
          const content = property_template.content;
          const tipe_template = datax.template_type;
          const valStatus = {
            label: datax.status == 1 ? "Listed" : "Unlisted",
            value: datax.status,
          };
          let valMenu = [];

          if (datax.id_menu) {
            valMenu = {
              label: datax.menu,
              value: datax.id_menu,
            };
          }

          const menu = this.state.menu;
          menu.push(valMenu);

          const id_menu = datax.id_menu;

          this.setState({
            datax,
            property_template,
            name,
            status,
            valStatus,
            title,
            content,
            image,
            valMenu,
            id_menu,
            tipe_template,
            menu,
          });
        }
      })
      .catch((error) => {
        swal.close();
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  kembaliAction() {
    window.history.back();
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const check = this.checkEmpty(
      dataForm,
      ["name", "title", "status", "menu"],
      [""],
    );
    return check;
  }

  resetError() {
    let errors = {};
    errors[("name", "title", "status", "image", "isi", "menu")] = "";
    this.setState({ errors: errors });
  }

  checkEmpty(dataForm, fieldName, fileFieldName) {
    let errors = {};
    let formIsValid = true;
    for (const field in fieldName) {
      const nameAttribute = fieldName[field];
      if (dataForm.get(nameAttribute) == "") {
        errors[nameAttribute] = "Tidak Boleh Kosong";
        formIsValid = false;
      }
    }

    if (this.state.content == "") {
      errors["isi"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.tipe_template != 2 && this.state.image == false) {
      errors["image"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  handleChangeNameAction(e) {
    const errors = this.state.errors;
    errors["name"] = "";

    const name = e.currentTarget.value;
    this.setState({
      name,
      errors,
    });
  }

  handleChangeStatusAction(val) {
    const errors = this.state.errors;
    errors["status"] = "";

    this.setState({
      valStatus: {
        label: val.label,
        value: val.value,
      },
      status: val.value,
      errors,
    });
  }

  handleChangeTitleAction(e) {
    const errors = this.state.errors;
    errors["title"] = "";

    const title = e.currentTarget.value;
    this.setState({
      title,
      errors,
    });
  }

  handleChangeIsiAction(value) {
    const errors = this.state.errors;
    errors["isi"] = "";

    this.setState(
      {
        content: value,
        errors,
      },
      () => {},
    );
  }

  handleChangeIsiChangeAction(value) {
    const errors = this.state.errors;
    errors["isi"] = "";

    this.setState({
      errors,
    });
  }

  handleClickAction(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });
      let timestamp = 1293683278;
      let date = new Date(timestamp * 1000);
      let ymdhis = date
        .toISOString()
        .match(/(\d{4}\-\d{2}\-\d{2})T(\d{2}:\d{2}:\d{2})/);
      let created_at = ymdhis[1] + " " + ymdhis[2];

      let title = dataForm.get("title").toLowerCase();
      let arr_title = title.split(" ");
      let slug = arr_title.join("-");
      //slug = slug + "-" + ymdhis[1] + '-' + ymdhis[2];

      let img = "";
      if (this.state.image) {
        img = this.state.image;
      }

      const property_template = [
        {
          title: dataForm.get("title"),
          content: this.state.content,
          image: img,
        },
      ];

      const bodyJson = [
        {
          id_page: parseInt(this.state.id_page),
          update_by: Cookies.get("user_id"),
          nama_page: dataForm.get("name"),
          id_menu: this.state.id_menu,
          url: slug,
          template_type: this.state.tipe_template,
          status: this.state.status,
          property_template: property_template,
        },
      ];

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/setting/edit_page",
          bodyJson,
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
                allowOutsideClick: false,
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/site-management/setting/page";
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      this.setState({ isLoading: false });
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleChangeImageAction(e) {
    const errors = this.state.errors;
    errors["image"] = "";

    this.setState({
      errors,
    });

    const imageFile = e.target.files[0];
    const reader = new FileReader();
    resizeFile(imageFile).then((image) => {
      reader.readAsDataURL(image);
      const context = this;
      reader.onload = function () {
        let result = reader.result;
        context.setState({ image: result });
      };
    });
  }

  handleChangeMenuAction(val) {
    const errors = this.state.errors;
    errors["menu"] = "";

    this.setState({
      valMenu: {
        label: val.label,
        value: val.value,
      },
      id_menu: val.value,
      errors,
    });
  }

  handleDeleteAction(e) {
    e.preventDefault();
    const idx = this.state.id_page;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          const data = { id: idx };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/setting/del_page",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                    allowOutsideClick: false,
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      window.history.back();
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmationButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            });
        }
      });
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <path
                        opacity="0.3"
                        d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                        fill="#7239ea"
                      ></path>
                      <path
                        d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                        fill="#7239ea"
                      ></path>
                    </svg>
                  </span>{" "}
                  Site Management
                  <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  Peserta DTS
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.kembali}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>

                <a
                  href="#"
                  title="Hapus"
                  onClick={this.handleDelete}
                  className="btn btn-sm btn-danger btn-active-light-info"
                >
                  <i className="bi bi-trash-fill text-white"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </a>
              </div>
              {/* <a href="https://back.dev.sdmdigital.id/api/report-pelatihan" className="btn btn-primary btn-sm me-2">
                <i className="las la-file-export"></i>
                Export
              </a>
              <a href="/pelatihan/tambah-pelatihan" className="btn btn-primary btn-sm">
                <i className="las la-plus"></i>
                Tambah Pelatihan
              </a> */}
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Edit Page
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="pb-8">
                        {/* form disini */}
                        <form
                          className="form"
                          action="#"
                          onSubmit={this.handleClick}
                        >
                          <input
                            type="hidden"
                            name="csrf-token"
                            value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                          />
                          <div className="row">
                            <div className="col-lg-12">
                              <h5 className="mb-3">Page Properties</h5>
                              <div className="col-lg-12 mb-7 fv-row">
                                <label className="form-label required">
                                  Page Name
                                </label>
                                <input
                                  className="form-control form-control-sm"
                                  placeholder="Masukkan Page Name"
                                  name="name"
                                  type="text"
                                  value={this.state.name}
                                  onChange={this.handleChangeName}
                                />
                                <span style={{ color: "red" }}>
                                  {this.state.errors["name"]}
                                </span>
                              </div>
                              <div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Menu
                                  </label>
                                  <Select
                                    id="menu"
                                    name="menu"
                                    placeholder="Silahkan pilih"
                                    noOptionsMessage={({ inputValue }) =>
                                      !inputValue
                                        ? this.state.datax
                                        : "Data tidak tersedia"
                                    }
                                    className="form-select-sm selectpicker p-0"
                                    options={this.state.menu}
                                    value={this.state.valMenu}
                                    onChange={this.handleChangeMenu}
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["menu"]}
                                  </span>
                                </div>
                              </div>
                              <div className="col-lg-12 mb-7 fv-row">
                                <label className="form-label required">
                                  Page Status
                                </label>
                                <Select
                                  id="status"
                                  name="status"
                                  placeholder="Silahkan pilih"
                                  noOptionsMessage={({ inputValue }) =>
                                    !inputValue
                                      ? this.state.errors
                                      : "Data tidak tersedia"
                                  }
                                  className="form-select-sm selectpicker p-0"
                                  value={this.state.valStatus}
                                  onChange={this.handleChangeStatus}
                                  options={this.status}
                                />
                                <span style={{ color: "red" }}>
                                  {this.state.errors["primary"]}
                                </span>
                              </div>
                            </div>
                            <div className="col-lg-12">
                              <div className="border-top pt-10 mt-5"></div>
                              <h5 className="mb-3">Page Content</h5>
                              <div className="col-lg-12 mb-7 fv-row">
                                <label className="form-label required">
                                  Title Page
                                </label>
                                <input
                                  className="form-control form-control-sm"
                                  placeholder="Tulis Judul Halaman"
                                  name="title"
                                  type="text"
                                  value={this.state.title}
                                  onChange={this.handleChangeTitle}
                                />
                                <span style={{ color: "red" }}>
                                  {this.state.errors["title"]}
                                </span>
                              </div>
                              {this.state.tipe_template != 2 ? (
                                <div className="col-lg-5 col-md-5 col-xs-5">
                                  <div className="form-group fv-row mb-7">
                                    <label className="form-label">Image</label>
                                    <ImageCard image={this.state.image} />
                                    <input
                                      type="file"
                                      className="form-control form-control-sm font-size-h4"
                                      name="logo_header"
                                      id="thumbnail"
                                      accept=".png,.jpg,.jpeg,.svg"
                                      onChange={this.handleChangeImage}
                                    />
                                    <div>
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["image"]}
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              ) : (
                                ""
                              )}

                              <div className="form-group fv-row mb-7">
                                <label className="form-label required">
                                  Content Page
                                </label>
                                <JoditEditor
                                  editorRef={this.setRef}
                                  value={this.state.content}
                                  config={this.configJodit}
                                  onBlur={this.handleChangeIsi}
                                  onChange={this.handleChangeIsiChange}
                                />
                                <span style={{ color: "red" }}>
                                  {this.state.errors["isi"]}
                                </span>
                              </div>
                              <div className="text-center pt-7 my-7">
                                <button
                                  onClick={this.kembali}
                                  type="reset"
                                  className="btn btn-light btn-md me-3"
                                  data-kt-menu-dismiss="true"
                                >
                                  Batal
                                </button>
                                <button
                                  type="submit"
                                  className="btn btn-primary btn-md"
                                  disabled={this.state.isLoading}
                                >
                                  {this.state.isLoading ? (
                                    <>
                                      <span
                                        className="spinner-border spinner-border-sm me-2"
                                        role="status"
                                        aria-hidden="true"
                                      ></span>
                                      <span className="sr-only">
                                        Loading...
                                      </span>
                                      Loading...
                                    </>
                                  ) : (
                                    <>
                                      <i className="fa fa-paper-plane me-1"></i>
                                      Simpan
                                    </>
                                  )}
                                </button>
                              </div>
                            </div>
                          </div>
                          {/* <div className="text-center">
                                                            <a className="btn btn-warning btn-sm" href="#" onClick={this.kembaliPilihTemplate}>Kembali Pilih Template</a>
                                                        </div> */}
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
