import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../Panduan-Edit-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const PanduanEdit = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default PanduanEdit;
