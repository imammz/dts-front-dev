import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../Panduan-Statis-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const PanduanStatis = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default PanduanStatis;
