import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../../panduan/Panduan-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const Panduan = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default Panduan;
