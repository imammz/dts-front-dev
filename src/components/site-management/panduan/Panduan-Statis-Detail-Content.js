import React, { useState } from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import Select from "react-select";
import { Fade } from "@material-ui/core";
import {
  indonesianDateFormat,
  capitalizeFirstLetter,
} from "../../publikasi/helper";
import "../../publikasi/image_pratinjau.css";

export default class PanduanStatisDetailContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleCallBack = this.handleCallBackAction.bind(this);
  }
  state = {
    datax: [],
    loading: true,
    checked: false,
    panduans: [],
    panduans_ne: [],
    id_kategori: "",
  };
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    } else {
      let segment_url = window.location.pathname.split("/");
      let id_kategori = segment_url[3];
      this.setState(
        {
          id_kategori,
        },
        () => {
          this.handleReload();
        },
      );
    }
  }

  handleReload() {
    const dataBody = {
      start: 0,
      length: 200,
      kategori: this.state.id_kategori,
      status: "",
      search: "",
      sort_by: "pinned",
      sort_val: "DESC",
      pinned: "",
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/filter-panduan",
        dataBody,
        this.configs,
      )
      .then((res) => {
        const datax = res.data.result.Data;
        const statusx = res.data.result.Status;
        const panduans = [];
        const panduans_ne = [];
        if (statusx) {
          for (let i = 0; i < datax.length; i++) {
            if (datax[i].publish == 1) {
              panduans.push(
                <PanduanCard
                  judul={datax[i].judul}
                  isi={datax[i].isi}
                  id={datax[i].id}
                  key={datax[i].id}
                  check={false}
                  parentCallBack={this.handleCallBack}
                />,
              );
              const pan = {
                judul: datax[i].judul,
                isi: datax[i].isi,
                id: datax[i].id,
              };
              panduans_ne.push(pan);
            }
          }
        }
        this.setState({
          panduans,
          panduans_ne,
        });
      });
  }

  handleCallBackAction(e) {
    const id = e.id;
    const panduans = [];
    const panduans_ne = this.state.panduans_ne;
    for (let i = 0; i < panduans_ne.length; i++) {
      let status = false;
      if (panduans_ne[i].id == id) {
        status = true;
      }
      panduans.push(
        <PanduanCard
          judul={panduans_ne[i].judul}
          isi={panduans_ne[i].isi}
          id={panduans_ne[i].id}
          key={panduans_ne[i].id}
          check={status}
          parentCallBack={this.handleCallBack}
        />,
      );
    }
    this.setState(
      {
        panduans: [],
      },
      () => {
        this.setState({
          panduans: panduans,
        });
      },
    );
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Panduan
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <a href="/panduan" className="btn btn-light btn-sm">
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>

        <div
          style={{ minHeight: "800px" }}
          className="wrapper d-flex flex-column flex-row-fluid pt-7"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                {this.state.panduans}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function PanduanCard(props) {
  const [valChecked, setValChecked] = useState(props.check);
  const [judul, setJudul] = useState(props.judul);
  const [isi, setIsi] = useState(props.isi);
  const [id, setId] = useState(props.id);

  return (
    <div className="row">
      <div className="col-12 border rounded shadow mb-6 overflow-hidden">
        <div className="d-flex align-items-center">
          <h5 className="mb-0 w-100">
            <button
              type="button"
              onClick={() => {
                setValChecked(!valChecked);
                const callBackVal = {
                  id: id,
                  judul: judul,
                  isi: isi,
                };
                props.parentCallBack(callBackVal);
              }}
              className="w-100 d-flex align-items-center text-left p-5 min-height-80 btn btn-lg"
            >
              <strong>{judul}</strong>
            </button>
          </h5>
        </div>
        {valChecked ? (
          <Fade in={valChecked}>
            <div className="p-6 border-top">
              <div dangerouslySetInnerHTML={{ __html: isi }} />
            </div>
          </Fade>
        ) : (
          ""
        )}
      </div>
    </div>
  );
}
