import React, { useState } from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import Select from "react-select";
import {
  indonesianDateFormat,
  capitalizeFirstLetter,
} from "../../publikasi/helper";

export default class PanduanStatisContent extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    datax: [],
    loading: true,
    kategoris: [],
  };
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    } else {
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });

      const dataBody = {
        jenis: "Panduan",
      };
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/portal/get-kategori",
          dataBody,
          this.configs,
        )
        .then((res) => {
          const datax = res.data.result.Data;
          const statusx = res.data.result.Status;
          const messagex = res.data.result.Message;
          const kategoris = [];
          if (statusx) {
            this.setState({ datax });
            for (let i = 0; i < datax.length; i++) {
              kategoris.push(
                <KategoriCard
                  nama={datax[i].nama}
                  jml_data={datax[i].jml_data}
                  id={datax[i].id}
                />,
              );
            }
            this.setState({ kategoris });
          }
          swal.close();
        })
        .catch((error) => {
          console.log(error);
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  this.setState({ datax: [] });
                  this.setState({ loading: false });
                }
              });
          }
          swal.close();
        });
    }
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Panduan
                </h1>
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-7"
          style={{ minHeight: "600px" }}
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">{this.state.kategoris}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function KategoriCard(props) {
  const [valKategori, setKategori] = useState(props.nama);
  const [valJumlah, setJumlah] = useState(props.jml_data);
  const [valId, setId] = useState(props.id);
  return (
    <div className="col-md mb-md-6 mb-4 px-2">
      <a
        className="card card-border-hover border icon-category icon-category-sm p-5 lift shadow-dark-hover"
        href={"/panduan/detail/" + valId}
      >
        <div className="row align-items-center mx-n3">
          <div class="col-auto px-3">
            <div class="icon-h-p secondary">
              <i class="fas fa-bezier-curve"></i>
            </div>
          </div>
          <div className="col px-3">
            <h6 class="mb-0 line-clamp-1">{valKategori}</h6>
            <p class="mb-0 line-clamp-1">{valJumlah} Panduan</p>
          </div>
        </div>
      </a>
    </div>
  );
}
