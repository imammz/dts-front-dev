import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../LogSubm-Detail-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const LogSubmDetail = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default LogSubmDetail;
