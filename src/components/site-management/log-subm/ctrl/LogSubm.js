import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../LogSubm-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const LogSubm = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default LogSubm;
