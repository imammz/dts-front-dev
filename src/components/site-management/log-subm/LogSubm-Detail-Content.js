import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import { capitalizeTheFirstLetterOfEachWord } from "../../publikasi/helper";
import DataTable from "react-data-table-component";

export default class LogSubmDetailContent extends React.Component {
  constructor(props) {
    super(props);
    this.kembali = this.kembaliAction.bind(this);
  }

  state = {
    loading: false,
    id: null,
    datax: [],
    totalRows: 0,
    newPerPage: 10,
    tempLastNumber: 0,
    currentPage: 0,
    from_pagination_change: 0,
    paginationResetDefaultPage: false,
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  columns = [
    {
      name: "No",
      center: true,
      cell: (row, index) => this.state.tempLastNumber + index + 1,
      width: "70px",
    },
    {
      name: "Nomor Registrasi",
      sortable: true,
      //className: "min-w-300px mw-300px",
      grow: 6,
      //selector: row => capitalizeFirstLetter(row.judul),
      cell: (row) => {
        return (
          <span className="d-flex flex-column">
            <span
              className="fs-7"
              style={{
                overflow: "hidden",
                whiteSpace: "wrap",
                textOverflow: "ellipses",
              }}
            >
              {row.nomor_registrasi}
            </span>
          </span>
        );
      },
    },
    {
      name: "Status",
      sortable: true,
      cell: (row) => {
        let text_status = "Initialize";
        let classBadge = "badge badge-light-primary";
        if (row.status == 2) {
          text_status = "Sukses";
          classBadge = "badge badge-light-success";
        } else if (row.status == 3) {
          text_status = "Email Tidak Terkirim";
          classBadge = "badge badge-light-danger";
        } else if (row.status == 4) {
          text_status = "No-reg Tidak Valid";
          classBadge = "badge badge-light-danger";
        } else if (row.status == 5) {
          text_status = "Status Peserta tidak diizinkan";
          classBadge = "badge badge-light-danger";
        }

        return (
          <span className="d-flex flex-column">
            <span
              className="fs-7"
              style={{
                overflow: "hidden",
                whiteSpace: "wrap",
                textOverflow: "ellipses",
              }}
            >
              <span className={classBadge}>{text_status}</span>
            </span>
          </span>
        );
      },
    },
  ];

  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    let segment_url = window.location.pathname.split("/");
    let id_subm = segment_url[5];
    this.setState(
      {
        id: id_subm,
      },
      () => {
        this.handleReload();
      },
    );
  }

  handleReload(page, newPerPage) {
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });

    axios
      .get(
        process.env.REACT_APP_BASE_API_URI + "/subm-list/" + this.state.id,
        this.configs,
      )
      .then((res) => {
        this.setState({ loading: false });
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statusx) {
          swal.close();
          const datax = res.data.result.Data;
          this.setState({ datax });
        }
      })
      .catch((error) => {
        this.setState({ loading: false });
        console.log(error);
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  kembaliAction() {
    window.history.back();
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Log SUBM
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.kembali}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500">
                    <i className="fa fa-chevron-left pe-1"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Detail Log SUBM
                          </h1>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="highlight bg-light-primary mb-6">
                          <div className="col-lg-12 mb-7 fv-row text-primary">
                            <h5 className="text-primary fs-5">Informasi</h5>
                            <ul className="mb-0">
                              <li>
                                Pengiriman email dilakukan melalui sistem Queue
                                dengan menggunakan background proses
                              </li>
                              <li>
                                Silahkan cek Log secara berkala untuk update
                                informasi status
                              </li>
                            </ul>
                          </div>
                        </div>

                        <div className="table-responsive">
                          <DataTable
                            columns={this.columns}
                            data={this.state.datax}
                            progressPending={this.state.loading}
                            highlightOnHover
                            pointerOnHover
                            customStyles={this.customStyles}
                            persistTableHead={true}
                            noDataComponent={
                              <div className="mt-5">Tidak Ada Data</div>
                            }
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
