import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import Select from "react-select";
import {
  indonesianDateFormat,
  capitalizeFirstLetter,
} from "../../publikasi/helper";

export default class LogSubmContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleChangeSearch = this.handleChangeSearchAction.bind(this);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
  }
  state = {
    datax: [],
    loading: true,
    totalRows: 0,
    newPerPage: 10,
    tempLastNumber: 0,
    currentPage: 0,
    isSearch: false,
    sort_by: "id",
    sort_val: "DESC",
    from_pagination_change: 0,
    searchText: "",
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  columns = [
    {
      name: "No",
      center: true,
      cell: (row, index) => this.state.tempLastNumber + index + 1,
      width: "70px",
    },
    {
      name: "Judul - Subject",
      sortable: true,
      className: "min-w-300px mw-300px",
      width: "320px",
      grow: 6,
      selector: (row) => capitalizeFirstLetter(row.title),
      cell: (row) => {
        return (
          <span className="d-flex flex-column my-2">
            <a
              href={"/site-management/subm/log-subm/detail/" + row.id}
              id={row.id}
              className="text-dark"
            >
              <span
                className="fw-bolder fs-7"
                style={{
                  overflow: "hidden",
                  whiteSpace: "wrap",
                  textOverflow: "ellipses",
                }}
              >
                {capitalizeFirstLetter(row.title)}
              </span>
              <span
                className="text-muted fw-semibold d-block fs-7"
                style={{
                  overflow: "hidden",
                  whiteSpace: "wrap",
                  textOverflow: "ellipses",
                }}
              >
                {row.mail_subject
                  ? capitalizeFirstLetter(row.mail_subject)
                  : "-"}
              </span>
            </a>
          </span>
        );
      },
    },
    {
      name: "Author",
      sortable: true,
      className: "min-w-300px mw-300px",
      grow: 6,
      selector: (row) => capitalizeFirstLetter(row.name),
      cell: (row) => {
        return (
          <span className="d-flex flex-column my-2">
            <a
              href={"/site-management/panduan/edit/" + row.id}
              id={row.id}
              className="text-dark"
            >
              <span
                className="fs-7"
                style={{
                  overflow: "hidden",
                  whiteSpace: "wrap",
                  textOverflow: "ellipses",
                }}
              >
                {capitalizeFirstLetter(row.name)}
              </span>
            </a>
          </span>
        );
      },
    },
    {
      name: "Tgl.Dibuat",
      sortable: true,
      className: "min-w-100px",
      grow: 2,
      selector: (row) => indonesianDateFormat(row.created_at.split(" ")[0]),
      cell: (row) => {
        return (
          <span className="d-flex flex-column">
            <span
              className="fs-7"
              style={{
                overflow: "hidden",
                whiteSpace: "wrap",
                textOverflow: "ellipses",
              }}
            >
              {indonesianDateFormat(row.created_at.split(" ")[0])}
            </span>
          </span>
        );
      },
    },
    {
      name: "Aksi",
      center: true,
      grow: 3,
      cell: (row) => (
        <div>
          <a
            href={"/site-management/subm/log-subm/detail/" + row.id}
            id={row.id}
            className="btn btn-icon btn-bg-primary btn-sm me-1"
            title="Detail"
            alt="Detail"
          >
            <i className="bi bi-eye-fill text-white"></i>
          </a>
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    this.handleReload();
  }

  handleReload(page, newPerPage) {
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/subm-list",
        null,
        this.configs,
      )
      .then((res) => {
        let datax = res.data.result.Data;
        if (this.state.searchText != "") {
          const context = this.state;
          const newDatax = datax.filter(function (el) {
            const datax_title = el.title.toLowerCase();
            const search = context.searchText.toLowerCase();
            return datax_title.includes(search);
          });
          datax = newDatax;
        }
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statusx) {
          this.setState({ datax });
          this.setState({ loading: false });
          this.setState({ totalRows: res.data.result.Total });
        }
      });
  }

  handlePageChange = (page) => {
    if (this.state.from_pagination_change == 0) {
      this.setState({ loading: true });
      this.handleReload(page, this.state.newPerPage);
    }
  };
  handlePerRowsChange = async (newPerPage, page) => {
    if (this.state.from_pagination_change == 1) {
      this.setState({ loading: true });
      this.setState({ newPerPage: newPerPage }, () => {
        this.handleReload(page, this.state.newPerPage);
      });
    }
  };

  handleChangeSearchAction(e) {
    const searchText = e.currentTarget.value;
    this.setState({ searchText }, () => {
      this.handleReload();
    });
  }

  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    this.setState({ searchText }, () => {
      this.handleReload();
    });
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Log SUBM
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              {/* <div>
                                <button className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                                    data-bs-toggle="modal" data-bs-target="#filter">
                                    <i className="bi bi-sliders"></i>
                                    <span className="d-md-inline d-lg-inline d-xl-inline d-none">Filter</span>
                                </button>

                                <a href="/site-management/panduan/tambah" className="btn btn-success fw-bolder btn-sm">
                                    <i className="bi bi-plus-circle"></i>
                                    <span className="d-md-inline d-lg-inline d-xl-inline d-none">Tambah Panduan</span>
                                </a>
                            </div> */}
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-7"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 col-md-12 col-sm-12">
                    {/* <div className="row">
                                            <div className="col-xl-4 mb-3">
                                                <a href="#" onClick={this.handleClickFilterPublish} className="card hoverable">
                                                    <div className="card-body p-1">
                                                        <div className="d-flex flex-stack flex-grow-1 p-6">
                                                            <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-success mb-3 mt-1">
                                                                <span className="svg-icon svg-icon-success svg-icon-3x">
                                                                    <span
                                                                        className="svg-icon svg-icon-success svg-icon-3x">
                                                                        <i className="bi bi-question-circle-fill" style={{
                                                                            fontSize: "30px",
                                                                            color: "#47BE7D"
                                                                        }}></i>
                                                                    </span>
                                                                </span>
                                                            </div>
                                                            <div className="d-flex flex-column text-end mt-n4">
                                                                <h1 className="mb-n1"
                                                                    style={{ fontSize: "28px" }}>{this.state.totalPublish}</h1>
                                                                <div className="fw-bolder text-muted fs-7">Panduan Publish</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div className="col-xl-4 mb-3">
                                                <a href="#" onClick={this.handleClickFilterUnpublish} className="card hoverable">
                                                    <div className="card-body p-1">
                                                        <div className="d-flex flex-stack flex-grow-1 p-6">
                                                            <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-danger mb-3 mt-1">
                                                                <span className="svg-icon svg-icon-danger svg-icon-3x">
                                                                    <span
                                                                        className="svg-icon svg-icon-success svg-icon-3x">
                                                                        <i className="bi bi-question-circle-fill" style={{
                                                                            fontSize: "30px",
                                                                            color: "#F1416C"
                                                                        }}></i>
                                                                    </span>
                                                                </span>
                                                            </div>
                                                            <div className="d-flex flex-column text-end mt-n4">
                                                                <h1 className="mb-n1"
                                                                    style={{ fontSize: "28px" }}>{this.state.totalUnpublish}</h1>
                                                                <div className="fw-bolder text-muted fs-7">Panduan Unpublish</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div className="col-xl-4 mb-3">
                                                <a href="#" className="card hoverable">
                                                    <div className="card-body p-1">
                                                        <div className="d-flex flex-stack flex-grow-1 p-6">
                                                            <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-info mb-3 mt-1">
                                                                <span className="svg-icon svg-icon-info svg-icon-3x">
                                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className="mh-50px"><path
                                                                        d="M20 14H18V10H20C20.6 10 21 10.4 21 11V13C21 13.6 20.6 14 20 14ZM21 19V17C21 16.4 20.6 16 20 16H18V20H20C20.6 20 21 19.6 21 19ZM21 7V5C21 4.4 20.6 4 20 4H18V8H20C20.6 8 21 7.6 21 7Z"
                                                                        fill="#7239EA"></path><path opacity="0.3"
                                                                            d="M17 22H3C2.4 22 2 21.6 2 21V3C2 2.4 2.4 2 3 2H17C17.6 2 18 2.4 18 3V21C18 21.6 17.6 22 17 22ZM10 7C8.9 7 8 7.9 8 9C8 10.1 8.9 11 10 11C11.1 11 12 10.1 12 9C12 7.9 11.1 7 10 7ZM13.3 16C14 16 14.5 15.3 14.3 14.7C13.7 13.2 12 12 10.1 12C8.10001 12 6.49999 13.1 5.89999 14.7C5.59999 15.3 6.19999 16 7.39999 16H13.3Z"
                                                                            fill="#7239EA"></path></svg>
                                                                </span>
                                                            </div>
                                                            <div className="d-flex flex-column text-end mt-n4">
                                                                <h1 className="mb-n1"
                                                                    style={{ fontSize: "28px" }}>{this.state.totalAuthor}</h1>
                                                                <div className="fw-bolder text-muted fs-7">Total Author</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div> */}
                  </div>
                </div>
                <div className="col-lg-12 mt-5">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Log SUBM
                        </h1>
                      </div>
                      <div className="card-toolbar">
                        <div className="ml-5 d-flex align-items-center position-relative my-1 me-2">
                          <span className="svg-icon svg-icon-1 position-absolute ms-6">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                              className="mh-50px"
                            >
                              <rect
                                opacity="0.5"
                                x="17.0365"
                                y="15.1223"
                                width="8.15546"
                                height="2"
                                rx="1"
                                transform="rotate(45 17.0365 15.1223)"
                                fill="#a1a5b7"
                              ></rect>
                              <path
                                d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                fill="#a1a5b7"
                              ></path>
                            </svg>
                          </span>
                          {
                            <input
                              type="text"
                              data-kt-user-table-filter="search"
                              className="form-control form-control-sm form-control-solid w-250px ps-14"
                              placeholder="Cari Log"
                              onKeyPress={this.handleKeyPress}
                              onChange={this.handleChangeSearch}
                            />
                          }
                        </div>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="table-responsive">
                        <DataTable
                          columns={this.columns}
                          data={this.state.datax}
                          progressPending={this.state.loading}
                          highlightOnHover
                          pointerOnHover
                          pagination
                          paginationComponentOptions={{
                            selectAllRowsItem: true,
                            selectAllRowsItemText: "Semua",
                          }}
                          onChangePage={(page, totalRows) => {
                            this.setState(
                              {
                                from_pagination_change: 0,
                              },
                              () => {
                                this.handlePageChange(page, totalRows);
                              },
                            );
                          }}
                          onChangeRowsPerPage={(
                            currentRowsPerPage,
                            currentPage,
                          ) => {
                            this.setState(
                              {
                                from_pagination_change: 1,
                              },
                              () => {
                                this.handlePerRowsChange(
                                  currentRowsPerPage,
                                  currentPage,
                                );
                              },
                            );
                          }}
                          customStyles={this.customStyles}
                          persistTableHead={true}
                          noDataComponent={
                            <div className="mt-5">Tidak Ada Data</div>
                          }
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
