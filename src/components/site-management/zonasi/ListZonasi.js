import React, { useState, useEffect, useMemo, useRef } from "react";
import AkademiService from "../../../service/AkademiService";
import Pagination from "@material-ui/lab/Pagination";
import { useTable, useSortBy } from "react-table";
import { GlobalFilter, DefaultFilterForColumn } from "../../Filter";
import Header from "../../../components/Header";
import SideNav from "../../../components/SideNav";
import Footer from "../../../components/Footer";
import Select from "react-select";
import axios from "axios";
import { useHistory, useNavigate, useParams } from "react-router-dom";
import {
  useFilters,
  useGlobalFilter,
} from "react-table/dist/react-table.development";

const ListZonasi = (props) => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  const [akademi, setAkademi] = useState([]);
  const [searchTitle, setSearchTitle] = useState("");
  const [page, setPage] = useState(0);
  const [count, setCount] = useState(0);
  const [pageSize, setPageSize] = useState(10);
  const pageSizes = [10];
  const [fRep, setfRep] = useState();
  const uppercase = {
    "text-transforms": "capitalize",
  };
  const initialVal = [
    {
      label: "",
      value: "",
    },
  ];
  const [optionList, setOptioList] = useState(initialVal);
  const AkademiRef = useRef();
  AkademiRef.current = akademi;

  const history = useNavigate();
  useEffect(() => {
    retrieveAkademi();
    retriveOption();
  }, [page, pageSize]);

  const onChangeSearchTitle = (e) => {
    console.log(e);
    const searchTitle = e.target.value;
    setSearchTitle(searchTitle);
  };
  const retrieveAkademi = async () => {
    const params = getRequestParams(searchTitle, page, pageSize);
    const respo = await axios
      .post(process.env.REACT_APP_BASE_API_URI + "/masterdata/list-zonasi", {
        start: page,
        length: pageSize,
      })
      .then(function (response) {
        const { tutorials, totalPages } = response.data.result.Data;
        setAkademi(response.data.result.Data);
        setCount(response.data.result.Data.length);
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  const refreshList = () => {
    retrieveAkademi();
  };
  const handlePageChange = (event, value) => {
    if (value === 1) {
      setPage(0);
    } else {
      setPage(pageSize * value - 10);
    }
    console.log(pageSize);
    retrieveAkademi();
  };
  const handlePageSizeChange = (event) => {
    setPageSize(event.target.value);
    setPage(0);
  };
  const removeAllAkademi = () => {
    AkademiService.removeAll()
      .then((response) => {
        console.log(response);
        refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const deleteAkademi = (rowIndex) => {
    const id = AkademiRef.current[rowIndex].id;
    AkademiService.remove(id)
      .then((response) => {
        window.location.href = "";
        let newAkademi = [...AkademiRef.current];
        newAkademi.splice(rowIndex, 1);
        setAkademi(newAkademi);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const getRequestParams = (searchTitle, page, pageSize) => {
    let params = {};

    if (searchTitle) {
      params["title"] = searchTitle;
    }

    if (page) {
      params["page"] = page - 1;
    }

    if (pageSize) {
      params["size"] = pageSize;
    }

    return params;
  };
  const retriveOption = async () => {
    const respon = axios
      .post(process.env.REACT_APP_BASE_API_URI + "/masterdata/list-zonasi", {
        start: "0",
        length: "50",
      })
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.result.Status === true) {
            let repo = response.data.result.Data;
            let ui = [{}];
            const listItem = repo.map((number) =>
              ui.push({
                label: number.name,
                value: number.id,
              }),
            );
            setOptioList(ui);
            console.log(optionList);
          } else {
            setOptioList();
          }
        }
      });
  };
  const retrieveFind = async () => {
    const responsi = await axios
      .post(process.env.REACT_APP_BASE_API_URI + "/akademi/cari-akademi", {
        id: fRep,
      })
      .then(function (response) {
        if (response.status === 200) {
          console.log(response);
          if (response.data.result.Status === true) {
            setAkademi(response.data.result.Data);
            setCount(response.data.result.Data.length);
            retriveOption();
          } else {
            refreshList();
            retriveOption();
          }
        } else {
          refreshList();
          retriveOption();
        }
      })
      .catch((error) => {
        refreshList();
        retriveOption();
      });
  };
  const handleFilter = (value) => {
    setOptioList({ value: value });
    console.log(value.value);
    setfRep(value.value);
  };
  const handleSearch = () => {
    console.log(fRep);
    //   console.log(event.target);
    retrieveFind();
  };
  const handleShow = (cell) => {
    console.log(cell?.row?.original);
    console.log(cell?.row?.original.name);
    history("/pelatihan/akademi/content/" + cell?.row?.original.id);
  };
  const handleShowPrev = (cell) => {
    console.log(cell?.row?.original);
    console.log(cell?.row?.original.name);
    history("/pelatihan/akademi/preview/" + cell?.row?.original.id);
  };
  const columns = useMemo(
    () => [
      {
        Header: "No",
        accessor: "",
        className: "min-w-250px min-h-250px mw-250px mh-250px",
        sortType: "basic",
        style: { width: "150px !important" },
        Cell: (props) => {
          return (
            <div
              className="min-w-150px min-h-150px mw-150px mh-150px"
              style={{ textAlign: "center" }}
            >
              <center>
                <span>{props.row.original.created_at}</span>
              </center>
            </div>
          );
        },
      },
      {
        Header: "Nama Zonasi",
        accessor: "name",
        className: "min-w-250px min-h-250px mw-250px mh-250px",
        sortType: "basic",
      },
      {
        Header: "Status",
        Title: "status",
        accessor: "status",
        className: "min-w-100px",
        Cell: (props) => {
          return (
            <span className="badge badge-light-danger fs-7 m-1">
              {props.row.original.status}
            </span>
          );
        },
      },
      {
        Header: "Aksi",
        accessor: "actions",
        Cell: (props) => {
          const rowIdx = props.row.nama;
          return (
            <div>
              <button className="btn btn-icon btn-active-light-primary w-30px h-30px me-3">
                <span
                  onClick={() => handleShow(props)}
                  className="svg-icon svg-icon-3"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={24}
                    height={24}
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <path
                      d="M17.5 11H6.5C4 11 2 9 2 6.5C2 4 4 2 6.5 2H17.5C20 2 22 4 22 6.5C22 9 20 11 17.5 11ZM15 6.5C15 7.9 16.1 9 17.5 9C18.9 9 20 7.9 20 6.5C20 5.1 18.9 4 17.5 4C16.1 4 15 5.1 15 6.5Z"
                      fill="black"
                    />
                    <path
                      opacity="0.3"
                      d="M17.5 22H6.5C4 22 2 20 2 17.5C2 15 4 13 6.5 13H17.5C20 13 22 15 22 17.5C22 20 20 22 17.5 22ZM4 17.5C4 18.9 5.1 20 6.5 20C7.9 20 9 18.9 9 17.5C9 16.1 7.9 15 6.5 15C5.1 15 4 16.1 4 17.5Z"
                      fill="black"
                    />
                  </svg>
                </span>
              </button>
              <button
                className="btn btn-icon btn-active-light-primary w-30px h-30px me-3"
                onClick={() => handleShowPrev(props)}
              >
                <span className="svg-icon svg-icon-3">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={24}
                    height={24}
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <path
                      d="M17.5 11H6.5C4 11 2 9 2 6.5C2 4 4 2 6.5 2H17.5C20 2 22 4 22 6.5C22 9 20 11 17.5 11ZM15 6.5C15 7.9 16.1 9 17.5 9C18.9 9 20 7.9 20 6.5C20 5.1 18.9 4 17.5 4C16.1 4 15 5.1 15 6.5Z"
                      fill="black"
                    />
                    <path
                      opacity="0.3"
                      d="M17.5 22H6.5C4 22 2 20 2 17.5C2 15 4 13 6.5 13H17.5C20 13 22 15 22 17.5C22 20 20 22 17.5 22ZM4 17.5C4 18.9 5.1 20 6.5 20C7.9 20 9 18.9 9 17.5C9 16.1 7.9 15 6.5 15C5.1 15 4 16.1 4 17.5Z"
                      fill="black"
                    />
                  </svg>
                </span>
              </button>
            </div>
          );
        },
      },
    ],
    [],
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    state,
    prepareRow,
    setGlobalFilter,
    preGlobalFilteredRows,
  } = useTable(
    {
      columns,
      data: akademi,
      defaultColumn: { Filter: DefaultFilterForColumn },
    },
    useFilters,
    useGlobalFilter,
    useSortBy,
  );
  let rowCounter = 1;
  return (
    <div>
      <Header />
      <SideNav />
      <div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1 style={{ textTransform: "capitalize" }}>
                            List Master Zonasi
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          {/* <div> */}
                          <GlobalFilter
                            preGlobalFilteredRows={preGlobalFilteredRows}
                            globalFilter={state.globalFilter}
                            setGlobalFilter={setGlobalFilter}
                          />{" "}
                          &nbsp;&nbsp;&nbsp;
                          {/* </div>&nbsp;&nbsp;&nbsp; */}
                          <button
                            className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                            data-bs-toggle="modal"
                            data-bs-target="#filter"
                          >
                            <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                              >
                                <path
                                  d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                                  fill="currentColor"
                                />
                              </svg>
                            </span>
                            Filter
                          </button>
                          <div className="modal fade" tabindex="-1" id="filter">
                            <div className="modal-dialog modal-lg">
                              <div className="modal-content">
                                <div className="modal-header">
                                  <h5 className="modal-title">
                                    <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                      >
                                        <path
                                          d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                                          fill="currentColor"
                                        />
                                      </svg>
                                    </span>
                                    Filter
                                  </h5>
                                  <div
                                    className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                    data-bs-dismiss="modal"
                                    aria-label="Close"
                                  >
                                    <span className="svg-icon svg-icon-2x">
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                      >
                                        <rect
                                          opacity="0.5"
                                          x="6"
                                          y="17.3137"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(-45 6 17.3137)"
                                          fill="currentColor"
                                        />
                                        <rect
                                          x="7.41422"
                                          y="6"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(45 7.41422 6)"
                                          fill="currentColor"
                                        />
                                      </svg>
                                    </span>
                                  </div>
                                </div>
                                <div className="modal-body">
                                  <div className="row">
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label required">
                                        Nama Akademi
                                      </label>
                                      <Select
                                        name="level_pelatihan"
                                        placeholder="Silahkan pilih"
                                        noOptionsMessage={({ inputValue }) =>
                                          !inputValue
                                            ? optionList.value
                                            : "Data tidak tersedia"
                                        }
                                        value={optionList.value}
                                        className="form-select-sm selectpicker p-0"
                                        options={optionList}
                                        onChange={(value) =>
                                          handleFilter(value)
                                        }
                                      />
                                    </div>
                                  </div>
                                </div>
                                <div className="modal-footer">
                                  <div className="d-flex justify-content-between">
                                    <button
                                      type="button"
                                      className="btn btn-sm btn-danger me-3"
                                    >
                                      Reset
                                    </button>
                                    <button
                                      type="button"
                                      className="btn btn-sm btn-primary"
                                      onClick={handleSearch}
                                    >
                                      Apply Filter
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <a
                            href="/partnership/ttd/add"
                            className="btn btn-primary btn-sm"
                          >
                            <i className="las la-plus"></i>
                            Tambah Master Zonasi
                          </a>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="form-control-sm">
                          {"Items per Page: "}
                          <select
                            onChange={handlePageSizeChange}
                            value={pageSize}
                            className="form-control-sm"
                          >
                            {pageSizes.map((size) => (
                              <option key={size} value={size}>
                                {size}
                              </option>
                            ))}
                          </select>
                        </div>
                        <div className="table-responsive">
                          <table
                            className="table align-middle table-row-dashed fs-6"
                            {...getTableProps()}
                          >
                            <thead
                              style={{
                                background:
                                  "rgb(243, 246, 249) none repeat scroll 0% 0%",
                              }}
                            >
                              {headerGroups.map((headerGroup) => (
                                <tr
                                  className="fw-bold fs-6 text-gray-800"
                                  {...headerGroup.getHeaderGroupProps()}
                                >
                                  {headerGroup.headers.map((column) => (
                                    <th
                                      {...column.getHeaderProps(
                                        column.getSortByToggleProps(),
                                      )}
                                      style={{ padding: "15px" }}
                                    >
                                      {column.render("Header")}
                                      {/* Add a sort direction indicator */}
                                      {/* <span style={{ textAlign: "center" }}> */}
                                      {column.isSorted
                                        ? column.isSortedDesc
                                          ? " 🔽"
                                          : " 🔼"
                                        : ""}
                                      {/* </span> */}
                                    </th>
                                  ))}
                                </tr>
                              ))}
                            </thead>
                            <tbody {...getTableBodyProps()}>
                              {rows.map((row, i) => {
                                prepareRow(row);
                                return (
                                  <tr
                                    {...row.getRowProps()}
                                    style={{
                                      borderBottom: "1px solid #f3f6f9",
                                    }}
                                  >
                                    {row.cells.map((cell) => {
                                      return (
                                        <td
                                          {...cell.getCellProps()}
                                          style={{
                                            textAlign: "center!important",
                                            padding: "15px",
                                          }}
                                        >
                                          {cell.render("Cell")}
                                        </td>
                                      );
                                    })}
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                        </div>
                        <div className="d-flex justify-content-between align-items-center mt-7">
                          <div className="pagination">
                            <Pagination
                              className="page-item"
                              color="primary"
                              count={rows.length}
                              page={page}
                              siblingCount={0}
                              rowsPerPageOptions={pageSize}
                              rowsPerPage={pageSize}
                              boundaryCount={1}
                              variant="outlined"
                              shape="rounded"
                              onChange={handlePageChange}
                              showFirstButton
                              showLastButton
                            />
                          </div>

                          <p>{pageSize} data dari ....</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </div>

    // <Footer/>
  );
};

export default ListZonasi;
