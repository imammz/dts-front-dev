import React from "react";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import { Observer } from "mobx-react-lite";
import Select from "react-select";
import { isSuperAdmin } from "../../AksesHelper";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import axios from "axios";
import Editor from "@arbor-dev/ckeditor5-arbor-custom";

export default class RequestWABlastTambahContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleChangeAkademi = this.handleChangeAkademiAction.bind(this);
    this.handleChangeTema = this.handleChangeTemaAction.bind(this);
    this.handleChangePelatihan = this.handleChangePelatihanAction.bind(this);
    this.handleSubmit = this.handleSubmitAction.bind(this);
    this.kembali = this.kembaliAction.bind(this);
  }

  state = {
    datax: [],
    errors: {},
    isLoading: false,
    selected: 1,
    hover: 0,
    akademiData: [],
    temaData: [],
    pelatihanData: [],
    VAkademiID: "",
    VTemaID: "",
    VPelatihanID: "",
    VNarasi: "",
    VLengthNarasi: 0,
    VFile: null,
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  style = {
    selectedSide: {
      padding: "16px",
      backgroundColor: "#ecf5fc",
      marginBottom: "8px",
      borderRadius: "8px",
      //fontSize: '16px',
      fontWeight: "700",
      width: "95%",
    },
    normalSide: {
      padding: "16px",
      //backgroundColor: '#ecf5fc',
      marginBottom: "8px",
      borderRadius: "8px",
      //fontSize: '16px',
      fontWeight: "700",
      width: "95%",
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    } else {
      if (localStorage.getItem("from_subm") == 1) {
        this.setState(
          {
            selected: 3,
            hover: 3,
          },
          () => {
            localStorage.setItem("from_subm", 0);
          },
        );
      }
    }

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/akademi/list-akademi",
        {
          length: 100,
          start: 0,
        },
        this.configs,
      )
      .then((res) => {
        /** Replace akademi option with res.data */
        let akademiList = res.data.result.Data;
        let akademiOptions = [];
        for (const akademi in akademiList) {
          akademiOptions.push({
            value: akademiList[akademi].id,
            label: akademiList[akademi].name,
          });
        }
        this.setState({ akademiData: akademiOptions });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  handleChangeAkademiAction = (selectedOption) => {
    let VAkademiID = selectedOption;
    this.setState({
      VAkademiID,
      VTemaID: "",
      VPelatihanID: "",
      temaData: [],
      pelatihanData: [],
    });

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/cari_tema_byakademi",
        {
          id: VAkademiID.value,
          row: 2000,
          start: 0,
        },
        this.configs,
      )
      .then((res) => {
        let temaList = res.data.result.Data;
        let temaOptions = [];
        for (const tema in temaList) {
          temaOptions.push({
            value: temaList[tema].id,
            label: temaList[tema].name,
          });
        }
        this.setState({ temaData: temaOptions });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleChangeTemaAction = (selectedOption) => {
    let VTemaID = selectedOption;
    this.setState({ VTemaID, VPelatihanID: "", pelatihanData: [] });

    let pelatihanSudahAda = [];
    let pelatihanSudahAdaRequest = axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/whatsapp-broadcast-request-list",
        {},
        this.configs,
      )
      .then((res) => {
        let list = res.data.result.Data;
        for (const item in list) {
          pelatihanSudahAda.push(list[item].pelatihan.id);
        }
      });

    /** Make await promise */
    Promise.all([pelatihanSudahAdaRequest]).then(() => {
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/pelatihan",
          {
            id_akademi: this.state.VAkademiID.value,
            id_tema: VTemaID.value,
            mulai: 0,
            limit: 9999,
            param: null,
            sort: "id_pelatihan",
            sortval: "DESC",
          },
          this.configs,
        )
        .then((res) => {
          let pelatihanList = res.data.result.Data;
          let pelatihanOptions = [];
          for (const pelatihan in pelatihanList) {
            let pelatihanSudahMengajukan = pelatihanSudahAda.find(
              (v) => v == pelatihanList[pelatihan].id,
            );
            pelatihanOptions.push({
              value: pelatihanList[pelatihan].id,
              label:
                "[" +
                pelatihanList[pelatihan].penyelenggara +
                "] - " +
                pelatihanList[pelatihan].slug_pelatian_id +
                " - " +
                pelatihanList[pelatihan].pelatihan +
                " Batch " +
                pelatihanList[pelatihan].batch +
                " " +
                pelatihanList[pelatihan].provinsi +
                " (" +
                (pelatihanSudahMengajukan
                  ? "sudah pernah mengajukan"
                  : "belum pernah mengajukan") +
                ")",
              isDisabled: pelatihanSudahMengajukan,
            });
          }
          this.setState({ pelatihanData: pelatihanOptions });
        })
        .catch((err) => {
          console.log(err);
        });
    });
  };

  handleChangePelatihanAction = (selectedOption) => {
    let VPelatihanID = selectedOption;
    this.setState({ VPelatihanID });
  };

  kembaliAction() {
    window.history.back();
  }

  handleSubmitAction(e) {
    if (this.state.VLengthNarasi >= 900) {
      console.log("Narasi terlalu panjang, maksimal 900 karakter");
      return;
    }
    /** Post Using DataForm to /whatsapp-broadcast-request-create */
    let formData = new FormData();
    formData.append("pelatihan_id", this.state.VPelatihanID.value);
    formData.append("narasi", this.state.VNarasi);
    formData.append("file", this.state.VFile);
    this.setState({ isLoading: true });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/whatsapp-broadcast-request-create",
        formData,
        this.configs,
      )
      .then((res) => {
        if (res.data.result.Status == 1) {
          this.setState({ isLoading: false });
          swal
            .fire({
              title: "Sukses",
              text: res.data.message,
              icon: "success",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                window.location = "/site-management/whatsapp-blast";
              }
            });
        } else {
          this.setState({ isLoading: false });
          swal.fire({
            title: "Gagal",
            text: "Mohon periksa isian anda",
            icon: "error",
            confirmButtonText: "Ok",
          });
        }
      })
      .catch((err) => {
        this.setState({ isLoading: true });
        console.log(err);
      });

    e.preventDefault();
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  WhatsApp Blast
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.kembali}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Buat Pengajuan
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="col-lg-12">
                        <div className="row">
                          <form
                            className="form mt-6"
                            onSubmit={this.handleSubmit}
                            action="#"
                          >
                            <div className="highlight bg-light-primary">
                              <div className="col-lg-12 mb-7 fv-row text-primary">
                                <h5 className="text-primary fs-5">Panduan</h5>
                                <p className="text-primary">
                                  Mohon untuk membaca panduan berikut :
                                </p>
                                <ul>
                                  <li>
                                    Silahkan unduh template pengajuan pada link
                                    berikut{" "}
                                    <a
                                      href={
                                        process.env.REACT_APP_BASE_API_URI +
                                        "/download/get-file?path=data-peserta.csv&disk=dts-storage-sitemanagement"
                                      }
                                      className="btn btn-primary fw-semibold btn-sm py-1 px-2"
                                    >
                                      <i className="las la-cloud-download-alt fw-semibold me-1" />
                                      Download Template
                                    </a>
                                  </li>
                                  <li>
                                    Lengkapi isian dan upload kembali pada form
                                    yang telah disediakan
                                  </li>
                                  <li>
                                    Data yang telah disubmit tidak dapat diubah
                                    kembali, mohon pastikan data penerima dan
                                    narasi telah diinput dengan benar
                                  </li>
                                  <li>
                                    Layanan WA blast akan ditindak lanjuti oleh
                                    Tim Media Publikasi dan Pasca Pelatihan
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <div className="col-lg-12 mt-5 mb-8 fv-row">
                              <label className="form-label required">
                                Akademi
                              </label>
                              <Observer>
                                {() => (
                                  <>
                                    <Select
                                      id="akademi"
                                      name="akademi"
                                      placeholder="Silahkan pilih"
                                      className="form-select-sm selectpicker p-0"
                                      options={this.state.akademiData}
                                      onChange={this.handleChangeAkademi}
                                      value={this.state.VAkademiID}
                                    />
                                    <span style={{ color: "red" }}>{}</span>
                                  </>
                                )}
                              </Observer>
                            </div>
                            <div className="col-lg-12 mt-5 mb-8 fv-row">
                              <label className="form-label required">
                                Tema
                              </label>
                              <Observer>
                                {() => (
                                  <>
                                    <Select
                                      id="tema"
                                      name="tema"
                                      placeholder="Silahkan pilih"
                                      className="form-select-sm selectpicker p-0"
                                      options={this.state.temaData}
                                      onChange={this.handleChangeTema}
                                      value={this.state.VTemaID}
                                    />
                                    <span style={{ color: "red" }}>{}</span>
                                  </>
                                )}
                              </Observer>
                            </div>
                            <div className="col-lg-12 mt-5 mb-8 fv-row">
                              <label className="form-label required">
                                Pelatihan
                              </label>
                              <Observer>
                                {() => (
                                  <>
                                    <Select
                                      id="pelatihan"
                                      name="pelatihan"
                                      placeholder="Silahkan pilih"
                                      options={this.state.pelatihanData}
                                      onChange={this.handleChangePelatihan}
                                      value={this.state.VPelatihanID}
                                    />
                                    <span style={{ color: "red" }}>{}</span>
                                  </>
                                )}
                              </Observer>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Narasi Pesan (Maksimal{" "}
                                {900 - this.state.VLengthNarasi} karakter)
                              </label>
                              <CKEditor
                                editor={Editor}
                                data={this.state.valuedeskripsikomitmen}
                                placeholder="Masukkan deskripsi komitmen"
                                name="deskripsi_komitmen"
                                onReady={(editor) => {}}
                                config={{
                                  ckfinder: {
                                    uploadUrl:
                                      process.env.REACT_APP_BASE_API_URI +
                                      "/publikasi/ckeditor-upload-image",
                                  },
                                }}
                                onChange={(event, editor) => {
                                  const data = editor.getData();
                                  /** Word Count and clean HTML Tag /<[^>]*>/g and &nbsp */
                                  const text = data.replace(/<[^>]*>/g, "");
                                  const wordCount = text.replace(
                                    /&nbsp;/g,
                                    "",
                                  ).length;
                                  this.setState({ VLengthNarasi: wordCount });
                                  /** If VLengthNarasi sudah 900 maka tidak bisa ketik lagi */
                                  if (wordCount >= 900) {
                                    /** Show Alert */
                                    alert("Maksimal 900 karakter");
                                  }
                                  this.setState({ VNarasi: data });
                                }}
                              />
                            </div>
                            <div className="row mt-5 mb-8">
                              <div className="col-lg-12">
                                <div className="required">
                                  Upload Data Penerima (Sesuai Template)
                                </div>
                                <input
                                  type="file"
                                  className="form-control form-control-sm mb-2 mt-2"
                                  name="file"
                                  accept=".csv"
                                  onChange={(e) =>
                                    this.setState({ VFile: e.target.files[0] })
                                  }
                                />
                              </div>
                            </div>

                            <div className="text-center border-top pt-10 my-7">
                              <button
                                type="submit"
                                className="btn btn-md btn-primary"
                                disabled={this.state.isLoading}
                              >
                                {this.state.isLoading ? (
                                  <>
                                    <span
                                      className="spinner-border spinner-border-sm me-2"
                                      role="status"
                                      aria-hidden="true"
                                    ></span>
                                    <span className="sr-only">Loading...</span>
                                    Loading...
                                  </>
                                ) : (
                                  <>
                                    <i className="fa fa-paper-plane me-1"></i>
                                    Submit
                                  </>
                                )}
                              </button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
