import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import moment from "moment";
import {
  resolveBgStatusSubstansi,
  dateRange,
  FRONT_URL,
  resolveStatusPelatihanBg,
} from "../../pelatihan/Pelatihan/helper";
import { isExceptionRole } from "../../AksesHelper";

export default class RequestWABlastViewContentDetail extends React.Component {
  constructor(props) {
    super(props);
    Cookies.remove("pelatian_id");
    this.state = {
      whatsapp_detail: [],
    };
  }
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  optionstatus = [
    { value: "1", label: "Publish" },
    { value: "0", label: "Unpublish" },
    { value: "2", label: "Unlisted" },
  ];
  componentDidMount() {
    moment.locale("id");
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    let segment_url = window.location.pathname.split("/");
    let id_whatsapp = segment_url[4];
    axios
      .get(
        process.env.REACT_APP_BASE_API_URI +
          "/whatsapp-broadcast-request-list" +
          "/" +
          id_whatsapp,
        this.configs,
      )
      .then((res) => {
        this.setState({
          whatsapp_detail: res.data.result.Data[0],
        });
      });
  }
  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  WhatsApp Blast
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <a
                  href="/site-management/whatsapp-blast"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Detail Pengajuan
                          </h1>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="mb-7">
                          <div className="d-flex flex-wrap py-3">
                            <div className="symbol symbol-100px symbol-lg-120px me-4">
                              {this.state.whatsapp_detail?.pelatihan
                                ?.mitra_logo == null ? (
                                <img
                                  src={`/assets/media/logos/logo-kominfo.png`}
                                  alt=""
                                  height="100px"
                                  className="symbol-label"
                                />
                              ) : (
                                <img
                                  src={
                                    process.env.REACT_APP_BASE_API_URI +
                                    "/download/get-file?path=" +
                                    this.state.whatsapp_detail?.pelatihan
                                      ?.logo +
                                    "&disk=dts-storage-partnership"
                                  }
                                  height="100px"
                                  alt=""
                                  className="symbol-label"
                                />
                              )}
                            </div>
                            <div className="flex-grow-1 mb-3">
                              <div className="justify-content-between align-items-start flex-wrap pt-6">
                                {this.state.whatsapp_detail?.status == 1 ? (
                                  <span className={`badge badge-light-primary`}>
                                    Requested
                                  </span>
                                ) : this.state.whatsapp_detail?.status == 2 ? (
                                  <span className={`badge badge-light-success`}>
                                    Selesai Diproses
                                  </span>
                                ) : this.state.whatsapp_detail?.status == 3 ? (
                                  <span className={`badge badge-light-danger`}>
                                    Ditolak
                                  </span>
                                ) : (
                                  ""
                                )}
                                <h1 className="align-items-center text-dark fw-bolder my-1 fs-4">
                                  {this.state.whatsapp_detail?.pelatihan
                                    ?.slug_akademi +
                                    this.state.whatsapp_detail?.pelatihan?.id +
                                    " - " +
                                    this.state.whatsapp_detail?.pelatihan
                                      ?.nama_pelatihan +
                                    " (Batch " +
                                    this.state.whatsapp_detail?.pelatihan
                                      ?.batch +
                                    ")"}
                                </h1>
                                <p className="text-dark fs-7 mb-0">
                                  {
                                    this.state.whatsapp_detail?.pelatihan
                                      ?.nama_akademi
                                  }{" "}
                                  -
                                  <span className="text-muted fw-semibold fs-7 mx-1 mb-0">
                                    {
                                      this.state.whatsapp_detail?.pelatihan
                                        ?.nama_tema
                                    }
                                  </span>
                                </p>
                              </div>
                            </div>
                          </div>
                          <div className="d-flex flex-wrap fw-bold mt-5 fs-6 mb-4 pe-2">
                            <span className="d-flex align-items-center fs-7 text-dark me-5 mb-3">
                              <span className="svg-icon svg-icon-4 me-1">
                                <i className="bi bi-recycle text-dark me-1"></i>
                                {
                                  this.state.whatsapp_detail?.pelatihan
                                    ?.alur_pendaftaran
                                }
                              </span>
                            </span>
                            <span className="d-flex align-items-center fs-7 text-dark me-5 mb-3">
                              <span className="svg-icon svg-icon-4 me-1">
                                <i className="bi bi-mic text-dark me-1"></i>
                                {
                                  this.state.whatsapp_detail?.pelatihan
                                    ?.nama_penyelenggara
                                }
                              </span>
                            </span>
                            {this.state.whatsapp_detail?.pelatihan
                              ?.nama_mitra != null ? (
                              <span className="d-flex align-items-center fs-7 text-dark me-5 mb-3">
                                <span className="svg-icon svg-icon-4 me-1">
                                  <i className="bi bi-person-video3 text-dark me-1"></i>
                                  {
                                    this.state.whatsapp_detail?.pelatihan
                                      ?.nama_mitra
                                  }
                                </span>
                              </span>
                            ) : (
                              ""
                            )}
                            <span class="d-flex align-items-center fs-7 text-dark me-5 mb-3">
                              <span class="svg-icon svg-icon-4 me-1">
                                <i class="bi bi-person-video3 text-dark me-1"></i>
                                {
                                  this.state.whatsapp_detail?.pelatihan
                                    ?.metode_pelaksanaan
                                }
                              </span>
                            </span>
                            <span className="d-flex align-items-center fs-7 text-dark me-5 mb-3">
                              <span className="svg-icon svg-icon-4 me-1">
                                <i className="bi bi-geo-alt text-dark me-1"></i>
                                {
                                  this.state.whatsapp_detail?.pelatihan
                                    ?.nama_kabupaten
                                }
                              </span>
                            </span>
                          </div>
                        </div>
                        <div className="d-flex border-bottom p-0">
                          <ul
                            className="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap"
                            style={{
                              overflowX: "auto",
                              overflowY: "hidden",
                              display: "flex",
                              whiteSpace: "nowrap",
                              marginBottom: 0,
                            }}
                          >
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary active"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_1"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                  Detail
                                </span>
                              </a>
                            </li>
                          </ul>
                        </div>
                        <div className="tab-content" id="detail-account-tab">
                          {/* tab 1 */}
                          <div
                            className="tab-pane fade show active"
                            id="kt_tab_pane_1"
                            role="tabpanel"
                          >
                            <div className="row mt-7 pb-7">
                              <div className="row">
                                <div className="col-lg-6 my-3 fv-row">
                                  <label className="form-label">
                                    Total Penerima
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {this.state.whatsapp_detail.peserta
                                        ? JSON.parse(
                                            this.state.whatsapp_detail?.peserta,
                                          ).length
                                        : 0}
                                    </b>
                                  </div>
                                </div>
                                <div className="col-lg-6 my-3 fv-row">
                                  <label className="form-label">Log</label>
                                  <div className="d-flex">
                                    <span className="fw-semibold fs-7 d-block me-1">
                                      {this.state.whatsapp_detail.name}
                                    </span>{" "}
                                    - {this.state.whatsapp_detail.created_at}
                                  </div>
                                </div>
                                <div className="col-lg-12 mx-3 bg-light p-6 rounded my-5 fv-row">
                                  <label className="form-label">
                                    Narasi Pesan
                                  </label>
                                  <div
                                    className="d-flex"
                                    dangerouslySetInnerHTML={{
                                      __html:
                                        this.state.whatsapp_detail?.narasi,
                                    }}
                                  ></div>
                                </div>
                                {this.state.whatsapp_detail?.status == "3" ? (
                                  <div className="col-lg-12 mx-3 bg-light-danger p-6 rounded my-5 fv-row">
                                    <label className="form-label">
                                      Alasan Ditolak
                                    </label>
                                    <div
                                      className="d-flex text-danger fs-7 fw-semibold"
                                      dangerouslySetInnerHTML={{
                                        __html:
                                          this.state.whatsapp_detail
                                            ?.keterangan,
                                      }}
                                    ></div>
                                  </div>
                                ) : (
                                  ""
                                )}
                                <div className="col-lg-6">
                                  <div className="mt-5 mb-7 fv-row">
                                    <label className="form-label">
                                      Daftar Penerima
                                    </label>
                                    <div className="d-flex">
                                      <b>
                                        <a
                                          target="_blank"
                                          href={
                                            process.env.REACT_APP_BASE_API_URI +
                                            "/get-file?path=" +
                                            this.state.whatsapp_detail
                                              ?.file_admin +
                                            "&disk=dts-storage-publikasi"
                                          }
                                          className="btn btn-sm btn-success"
                                        >
                                          <i className="bi bi-cloud-download me-2"></i>
                                          Download
                                        </a>
                                      </b>
                                    </div>
                                  </div>
                                </div>
                                {this.state.whatsapp_detail?.status == "2" ? (
                                  <div className="col-lg-6">
                                    <div className="mt-5 mb-7 fv-row">
                                      <label className="form-label">
                                        Log WhatsApp Blast
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          <a
                                            target="_blank"
                                            href={
                                              process.env
                                                .REACT_APP_BASE_API_URI +
                                              "/get-file?path=" +
                                              this.state.whatsapp_detail
                                                ?.file_log +
                                              "&disk=dts-storage-publikasi"
                                            }
                                            className="btn btn-sm btn-info"
                                          >
                                            <i className="bi bi-cloud-download me-2"></i>
                                            Download
                                          </a>
                                        </b>
                                      </div>
                                    </div>
                                  </div>
                                ) : (
                                  ""
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
