import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";

export default class KetentuanPelatihan extends React.Component {
  constructor(props) {
    super(props);
    this.handleChangeJumlah = this.handleChangeJumlahAction.bind(this);
    this.handleSubmit = this.handleSubmitAction.bind(this);
  }

  state = {
    datax: [],
    loading: false,
    errors: {},
    option_role_dropdown: [],
    id_komponen: false,
    numberOfTraining: false,
    trainingPassStatus: false,
    statusNotPassedTraining: false,
    noTrainingAccepted: false,
    valJumlah: [],
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  option_jumlah_pelatihan = [
    { value: 1, label: "1 Pelatihan" },
    { value: 2, label: "2 Pelatihan" },
    { value: 3, label: "3 Pelatihan" },
    { value: 4, label: "4 Pelatihan" },
    { value: 5, label: "5 Pelatihan" },
    { value: 6, label: "6 Pelatihan" },
    { value: 7, label: "7 Pelatihan" },
    { value: 8, label: "8 Pelatihan" },
    { value: 9, label: "9 Pelatihan" },
    { value: 10, label: "10 Pelatihan" },
    { value: 11, label: "Lebih Dari 10 Pelatihan" },
  ];

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/API_Get_Ketentuan_Pelatihan",
        null,
        this.configs,
      )
      .then((res) => {
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statusx) {
          swal.close();
          const datax = res.data.result.Data[0];
          const id_komponen = datax.id_komponen;
          const training_rules = JSON.parse(datax.training_rules);
          const numberOfTraining = training_rules["numberOfTraining"];
          const trainingPassStatus = training_rules["trainingPassStatus"];
          const statusNotPassedTraining =
            training_rules["statusNotPassedTraining"];
          const noTrainingAccepted = training_rules["noTrainingAccepted"];
          let label = "";
          this.option_jumlah_pelatihan.forEach(function (element) {
            if (element.value == numberOfTraining) {
              label = element.label;
            }
          });
          const valJumlah = { value: numberOfTraining, label: label };
          this.setState({
            id_komponen,
            numberOfTraining,
            trainingPassStatus,
            statusNotPassedTraining,
            noTrainingAccepted,
            valJumlah,
          });
        }
      })
      .catch((error) => {
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  handleSubmitAction(e) {
    e.preventDefault();
    this.setState({ isLoading: true });
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    const body_json = [
      {
        id_komponen: this.state.id_komponen,
        update_by: Cookies.get("user_id"),
        training_rules: {
          numberOfTraining: this.state.numberOfTraining,
          trainingPassStatus: this.state.trainingPassStatus,
          statusNotPassedTraining: this.state.statusNotPassedTraining,
          noTrainingAccepted: this.state.noTrainingAccepted,
        },
      },
    ];

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/API_Simpan_Ketentuan_Pelatihan",
        JSON.stringify(body_json),
        this.configs,
      )
      .then((res) => {
        this.setState({ isLoading: false });
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          swal
            .fire({
              title: messagex,
              icon: "success",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      })
      .catch((error) => {
        this.setState({ isLoading: false });
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  handleChangeJumlahAction = (selectedOption) => {
    this.setState({
      numberOfTraining: selectedOption.value,
      valJumlah: { value: selectedOption.value, label: selectedOption.label },
    });
  };

  render() {
    return (
      <div className="row">
        <h3>Ketentuan Pelatihan</h3>
        <form className="form" action="#" onSubmit={this.handleSubmit}>
          <input
            type="hidden"
            name="csrf-token"
            value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
          />

          <div className="col-lg-12 mb-7 fv-row mt-8">
            <label className="form-label required">
              Batas maksimal pelatihan yang dapat diikuti dalam 1 Tahun
            </label>
            <Select
              name="status"
              placeholder="Silahkan pilih"
              noOptionsMessage={({ inputValue }) =>
                !inputValue ? this.state.datax : "Data tidak tersedia"
              }
              className="form-select-sm selectpicker p-0"
              options={this.option_jumlah_pelatihan}
              value={this.state.valJumlah}
              onChange={this.handleChangeJumlah}
            />
            <span style={{ color: "red" }}>{this.state.errors["status"]}</span>
          </div>
          <div className="col-lg-12 mb-3 fv-row" key={`custom-checkbox1`}>
            <input
              type="checkbox"
              id={`custom-checkbox`}
              name="ck"
              checked={this.state.trainingPassStatus == 1 ? true : false}
              onChange={(e) => {
                this.setState({
                  trainingPassStatus: !this.state.trainingPassStatus ? 1 : 0,
                });
              }}
            />
            <label className="ms-3" htmlFor={"custom-checkbox1"}>
              Status Lulus Pelatihan
            </label>
          </div>
          <div className="col-lg-12 mb-3 fv-row" key={`custom-checkbox2`}>
            <input
              type="checkbox"
              id={`custom-checkbox`}
              name="ck"
              checked={this.state.statusNotPassedTraining == 1 ? true : false}
              onChange={(e) => {
                this.setState({
                  statusNotPassedTraining: !this.state.statusNotPassedTraining
                    ? 1
                    : 0,
                });
              }}
            />
            <label className="ms-3" htmlFor={"custom-checkbox2"}>
              Status Tidak Lulus Pelatihan
            </label>
          </div>
          <div className="col-lg-12 mb-3 fv-row" key={`custom-checkbox3`}>
            <input
              type="checkbox"
              id={`custom-checkbox`}
              name="ck"
              checked={this.state.noTrainingAccepted == 1 ? true : false}
              onChange={(e) => {
                this.setState({
                  noTrainingAccepted: !this.state.noTrainingAccepted ? 1 : 0,
                });
              }}
            />
            <label className="ms-3" htmlFor={"custom-checkbox3"}>
              Tidak Diterima Pelatihan
            </label>
          </div>

          <div className="form-group fv-row my-7">
            <button
              type="submit"
              className="btn btn-md btn-block d-block btn-primary"
              disabled={this.state.isLoading}
            >
              {this.state.isLoading ? (
                <>
                  <span
                    className="spinner-border spinner-border-sm me-2"
                    role="status"
                    aria-hidden="true"
                  ></span>
                  <span className="sr-only">Loading...</span>Loading...
                </>
              ) : (
                <>
                  <i className="fa fa-paper-plane me-1"></i>Simpan
                </>
              )}
            </button>
          </div>
        </form>
      </div>
    );
  }
}
