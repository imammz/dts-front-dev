import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import { read, utils } from "xlsx";
import Cookies from "js-cookie";
import Select from "react-select";
import { capitalizeTheFirstLetterOfEachWord } from "../../../publikasi/helper";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "@arbor-dev/ckeditor5-arbor-custom";

export default class BCEmail extends React.Component {
  constructor(props) {
    super(props);
    this.isBroadcast = this.isBroadcastAction.bind(this);
    this.uploadCsv = this.uploadCsvAction.bind(this);
    this.handleSubmit = this.handleSubmitAction.bind(this);
    this.handleChangeStatus = this.handleChangeStatusAction.bind(this);
    this.handleChangeSubject = this.handleChangeSubjectAction.bind(this);
    this.handleChangeIsi = this.handleChangeIsiAction.bind(this);
    this.handleChangeJudul = this.handleChangeJudulAction.bind(this);
    this.formDatax = new FormData();
  }

  state = {
    datax: [],
    loading: false,
    isLoading: false,
    errors: {},
    option_role_dropdown: [],
    is_broadcast: false,
    arr_id_registrasi: [],
    valStatus: [],
    status_id: null,
    subject: "",
    isi: "",
    judul: "",
    broadcast_empty: true,
    csv_upload: false,
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/umum/list-status-peserta",
        null,
        this.configs,
      )
      .then((res) => {
        const option_role_peserta = res.data.result.Data;
        const option_role_dropdown = [];

        option_role_peserta.forEach(function (element, i) {
          if (element.name.toLowerCase() != "submit") {
            const option = {
              value: element.id,
              label: capitalizeTheFirstLetterOfEachWord(element.name),
            };
            option_role_dropdown.push(option);
          }
        });
        this.setState({
          option_role_dropdown,
        });
      });
  }

  /*
	1. Kirim Email Tanpa Update Status
	2. Kirim Email saja
	3. Update Status saja
	4. Kirim Email dan Update Status
	*/

  /* handleSubmitAction(e) {
		const dataForm = new FormData(e.currentTarget);
		this.resetError();
		e.preventDefault();
		if (this.checkEmpty()) {
			swal.fire({
				title: "Mohon Tunggu!",
				icon: "info", // add html attribute if you want or remove
				allowOutsideClick: false,
				didOpen: () => {
					swal.showLoading();
				},
			});

			const nomor_registrasi = [];
			this.state.arr_id_registrasi.forEach(function (element) {
				nomor_registrasi.push({ nomor_registrasi: element[0] });
			});

			const bodyJson = {
				categoryOptItems: nomor_registrasi,
			};

			if (this.state.is_broadcast == 1 && this.state.status_id) {
				const formData = new FormData();
				formData.append("status", this.state.status_id);
				formData.append("user_by", Cookies.get("user_id"));
				formData.append("json", JSON.stringify(bodyJson));

				axios
					.post(
						process.env.REACT_APP_BASE_API_URI + "/setting/upload_subm_mails",
						formData,
						this.configs
					)
					.then((res) => {
						const statux = res.data.result.Status;
						const datax = res.data.result.Data;
						const messagex = res.data.result.Message;
						if (statux) {
							let html_datax_success = "";
							let html_datax_failed = "";
							const jsonData = [
								{
									id_komponen: 3,
									id_status: this.state.status_id,
									update_by: Cookies.get("user_id"),
									training_rules: {
										broadcast: 1,
										subject: this.state.subject,
										body: this.state.isi,
									},
								},
							];

							html_datax_success += "<ol>";
							html_datax_failed += "<ol>";
							let count_success = 0;
							let count_failed = 0;
							datax.forEach(function (element) {
								if (element.code == 0) {
									const noreg_status =
										'<li class="text-start"><strong>' +
										element.noreg +
										"</strong> : " +
										capitalizeTheFirstLetterOfEachWord(element.status) +
										"</li>";
									html_datax_failed += noreg_status;
									count_failed++;
								} else {
									const noreg_status =
										'<li class="text-start"><strong>' +
										element.noreg +
										"</strong></li>";
									html_datax_success += noreg_status;
									count_success++;
								}
							});

							html_datax_success += "</ol>";
							html_datax_failed += "</ol>";
							const html_datax = `
								<div class="row mb-5">
									<div class="col-md-6">
										<h3><span class="badge badge-success">Berhasil ${count_success}</span></h3>
									</div>
									<div class="col-md-6">
										<h3><span class="badge badge-danger">Gagal ${count_failed}</span></h3>
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-md-12">
										<h4>Berhasil</h4>
										${html_datax_success}
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-md-12">
										<h4>Gagal</h4>
										${html_datax_failed}
									</div>
								</div>
							`;

							axios
								.post(
									process.env.REACT_APP_BASE_API_URI + "/setting/add_subm_mail",
									JSON.stringify(jsonData),
									this.configs
								)
								.then((res2) => {
									const status2 = res2.data.result.Status;
									const message2 = res2.data.result.Message;
									if (status2) {
										swal
											.fire({
												title: "Status SUBM",
												icon: "success",
												html: html_datax,
												confirmButtonText: "Ok",
												allowOutsideClick: false,
											})
											.then((result) => {
												if (result.isConfirmed) {
													localStorage.setItem("from_subm", 1);
													window.location =
														"/site-management/setting/pelatihan";
												}
											});
									} else {
										swal
											.fire({
												title: message2,
												icon: "warning",
												confirmButtonText: "Ok",
											})
											.then((result) => {
												if (result.isConfirmed) {
												}
											});
									}
								});
						} else {
							swal
								.fire({
									title: messagex,
									icon: "warning",
									confirmButtonText: "Ok",
								})
								.then((result) => {
									if (result.isConfirmed) {
									}
								});
						}
					})
					.catch((error) => {
						let statux = error.response.data.result.Status;
						let messagex = error.response.data.result.Data;
						if (!statux) {
							swal
								.fire({
									title: messagex,
									icon: "warning",
									confirmButtonText: "Ok",
								})
								.then((result) => {
									if (result.isConfirmed) {
									}
								});
						}
					});
			} else if (this.state.is_broadcast == 0 && this.state.status_id) {
				//tidak ada email
				const formData = new FormData();
				formData.append("status", this.state.status_id);
				formData.append("user_by", Cookies.get("user_id"));
				formData.append("json", JSON.stringify(bodyJson));

				axios
					.post(
						process.env.REACT_APP_BASE_API_URI + "/setting/upload_subm",
						formData,
						this.configs
					)
					.then((res) => {
						const statux = res.data.result.Status;
						const datax = res.data.result.Data;
						let html_datax_success = "";
						let html_datax_failed = "";
						if (statux) {
							const jsonData = [
								{
									id_komponen: 3,
									id_status: this.state.status_id,
									update_by: Cookies.get("user_id"),
									training_rules: {
										broadcast: 0,
										subject: "",
										body: "",
									},
								},
							];
							html_datax_success += "<ol>";
							html_datax_failed += "<ol>";
							let count_success = 0;
							let count_failed = 0;
							datax.forEach(function (element) {
								if (element.code == 0) {
									const noreg_status =
										'<li class="text-start"><strong>' +
										element.noreg +
										"</strong> : " +
										capitalizeTheFirstLetterOfEachWord(element.status) +
										"</li>";
									html_datax_failed += noreg_status;
									count_failed++;
								} else {
									const noreg_status =
										'<li class="text-start"><strong>' +
										element.noreg +
										"</strong></li>";
									html_datax_success += noreg_status;
									count_success++;
								}
							});
							html_datax_success += "</ol>";
							html_datax_failed += "</ol>";

							const html_datax = `
								<div class="row mb-5">
									<div class="col-md-6">
										<h3><span class="badge badge-success">Berhasil ${count_success}</span></h3>
									</div>
									<div class="col-md-6">
										<h3><span class="badge badge-danger">Gagal ${count_failed}</span></h3>
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-md-12">
										<h4>Berhasil</h4>
										${html_datax_success}
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-md-12">
										<h4>Gagal</h4>
										${html_datax_failed}
									</div>
								</div>
							`;

							axios
								.post(
									process.env.REACT_APP_BASE_API_URI + "/setting/add_subm",
									JSON.stringify(jsonData),
									this.configs
								)
								.then((res2) => {
									const status2 = res2.data.result.Status;
									const message2 = res2.data.result.Message;
									if (status2) {
										swal
											.fire({
												title: "Status SUBM",
												//icon: "success",
												html: html_datax,
												heightAuto: false,
												confirmButtonText: "Ok",
												allowOutsideClick: false,
											})
											.then((result) => {
												document.getElementById(
													"swal2-html-container"
												).style.maxHeight = null;
												if (result.isConfirmed) {
													localStorage.setItem("from_subm", 1);
													window.location =
														"/site-management/setting/pelatihan";
												}
											});
									} else {
										swal
											.fire({
												title: message2,
												icon: "warning",
												confirmButtonText: "Ok",
											})
											.then((result) => {
												if (result.isConfirmed) {
												}
											});
									}
								});
						} else {
							swal
								.fire({
									title: "Gagal",
									icon: "warning",
									confirmButtonText: "Ok",
								})
								.then((result) => {
									if (result.isConfirmed) {
									}
								});
						}
					})
					.catch((error) => {
						let statux = error.response.data.result.Status;
						let messagex = error.response.data.result.Data;
						if (!statux) {
							swal
								.fire({
									title: messagex,
									icon: "warning",
									confirmButtonText: "Ok",
								})
								.then((result) => {
									if (result.isConfirmed) {
									}
								});
						}
					});
			} else if (this.state.is_broadcast == 1 && this.state.status_id == null) {
				const formData = new FormData();
				formData.append("status", 12);
				formData.append("user_by", Cookies.get("user_id"));
				formData.append("json", JSON.stringify(bodyJson));
				formData.append("subject", this.state.subject);
				formData.append("bodymail", this.state.isi);

				axios
					.post(
						process.env.REACT_APP_BASE_API_URI + "/setting/upload_subm_mail_0",
						formData,
						this.configs
					)
					.then((res) => {
						const statux = res.data.result.Status;
						const messagex = res.data.result.Data;
						if (statux) {
							swal
								.fire({
									title: messagex,
									icon: "success",
									confirmButtonText: "Ok",
								})
								.then((result) => {
									if (result.isConfirmed) {
										localStorage.setItem("from_subm", 1);
										window.location = "/site-management/setting/pelatihan";
									}
								});
						} else {
							swal
								.fire({
									title: messagex,
									icon: "warning",
									confirmButtonText: "Ok",
								})
								.then((result) => {
									if (result.isConfirmed) {
									}
								});
						}
					})
					.catch((error) => {
						let statux = error.response.data.result.Status;
						let messagex = error.response.data.result.Data;
						if (!statux) {
							swal
								.fire({
									title: messagex,
									icon: "warning",
									confirmButtonText: "Ok",
								})
								.then((result) => {
									if (result.isConfirmed) {
									}
								});
						}
					});
			}
		} else {
			swal
				.fire({
					title: "Mohon Periksa Isian",
					icon: "warning",
					confirmButtonText: "Ok",
				})
				.then((result) => {
					if (result.isConfirmed) {
					}
				});
		}
	} */

  //04-03-2023 Endpoint Change from Rendra to Rafi
  handleSubmitAction(e) {
    this.resetError();
    e.preventDefault();
    if (this.checkEmpty()) {
      this.setState({ isLoading: true });
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });
      let status_id = this.state.status_id;
      if (status_id == null) {
        status_id = "";
      }
      this.formDatax.append("title", this.state.judul);
      this.formDatax.append("status", status_id);
      this.formDatax.append("mail_subject", this.state.subject);
      this.formDatax.append("mail_content", this.state.isi);

      let type = 1;
      if (this.state.is_broadcast == 1 && this.state.status_id) {
        type = 1;
      } else if (this.state.is_broadcast == 0 && this.state.status_id) {
        type = 3;
      } else if (this.state.is_broadcast == 1 && this.state.status_id == null) {
        type = 2;
      }

      this.formDatax.append("type", type);

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/subm-create",
          this.formDatax,
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
                allowOutsideClick: false,
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location =
                    "/site-management/subm/log-subm/detail/" +
                    res.data.result.Data;
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Data;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      this.setState({ isLoading: false });
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleChangeTipeAction(e) {
    this.resetError();
    const tipe = e.currentTarget.value;
    this.setState({ tipe });
  }

  resetError() {
    let errors = {};
    errors[("file_csv", "status", "broadcast", "subject", "isi", "judul")] = "";
    this.setState({ errors: errors });
  }

  checkEmpty() {
    let errors = {};
    let formIsValid = true;

    if (this.state.csv_upload == false) {
      errors["file_csv"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.judul == null || this.state.judul == "") {
      errors["judul"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.broadcast_empty) {
      errors["broadcast"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.is_broadcast == 1) {
      if (this.state.subject == "") {
        errors["subject"] = "Tidak Boleh Kosong";
        formIsValid = false;
      }

      if (this.state.isi == "") {
        errors["isi"] = "Tidak Boleh Kosong";
        formIsValid = false;
      }
    }
    this.setState({ errors });
    return formIsValid;
  }

  isBroadcastAction(e) {
    const errors = this.state.errors;
    errors["broadcast"] = "";
    this.setState({ errors });

    const is_broadcast = e.currentTarget.value;
    this.setState(
      {
        is_broadcast: is_broadcast,
        broadcast_empty: false,
      },
      () => {},
    );
  }

  uploadCsvAction(e) {
    /* const errors = this.state.errors;
		errors["file_csv"] = "";
		this.setState({ errors });

		const [file] = e.target.files;
		const reader = new FileReader();

		reader.onload = (evt) => {
			const bstr = evt.target.result;
			const wb = read(bstr, { type: "binary" });
			const wsname = wb.SheetNames[0];
			const ws = wb.Sheets[wsname];
			const data = utils.sheet_to_json(ws, { header: 1 });
			data.shift();
			this.setState({
				arr_id_registrasi: data,
			});
		};

		reader.readAsBinaryString(file); */
    //04-03-2023 BE change from rendra to rafi
    const errors = this.state.errors;
    errors["file_csv"] = "";
    this.setState({ errors });
    this.setState({ csv_upload: true });
    const csvFile = e.target.files[0];
    this.formDatax.append("file", csvFile);
  }

  handleChangeStatusAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["status"] = "";
    this.setState({ errors });

    this.setState({
      status_id: selectedOption.value,
      valStatus: { value: selectedOption.value, label: selectedOption.label },
    });
  };

  handleChangeSubjectAction(e) {
    const errors = this.state.errors;
    errors["subject"] = "";
    this.setState({ errors });

    const subject = e.currentTarget.value;
    this.setState({
      subject,
    });
  }

  handleChangeJudulAction(e) {
    const errors = this.state.errors;
    errors["judul"] = "";
    this.setState({ errors });

    const judul = e.currentTarget.value;
    this.setState({
      judul,
    });
  }

  handleChangeIsiAction(e) {
    const errors = this.state.errors;
    errors["isi"] = "";
    this.setState({ errors });

    const isi = e;
    this.setState({
      isi,
    });
  }

  render() {
    return (
      <div className="row">
        <form className="form mt-6" action="#" onSubmit={this.handleSubmit}>
          <div className="highlight bg-light-primary">
            <div className="col-lg-12 mb-7 fv-row text-primary">
              <h5 className="text-primary fs-5">Panduan</h5>
              <p className="text-primary">
                Sebelum melakukan import, mohon untuk membaca panduan berikut :
              </p>
              <ul>
                <li>
                  Silahkan unduh template untuk melakukan import pada link
                  berikut{" "}
                  <a
                    href={
                      process.env.REACT_APP_BASE_API_URI +
                      "/download/get-file?path=data-peserta.csv&disk=dts-storage-sitemanagement"
                    }
                    className="btn btn-primary fw-semibold btn-sm py-1 px-2"
                  >
                    <i className="las la-cloud-download-alt fw-semibold me-1" />
                    Download Template
                  </a>
                </li>
                <li>
                  Lengkapi isian pada kolom dengan Nomor Registrasi peserta
                </li>
                <li>Upload kembali template yang telah dilengkapi</li>
              </ul>
            </div>
          </div>

          <div className="col-lg-12 mb-7 fv-row mt-8">
            <div className="col-lg-12">
              <div className="form-label required">Judul</div>
              <input
                className="form-control form-control-sm"
                placeholder="Masukkan Judul SUBM"
                name="judul"
                value={this.state.judul}
                onChange={this.handleChangeJudul}
              />
              <span style={{ color: "red" }}>{this.state.errors["judul"]}</span>
            </div>
          </div>

          <div className="row mt-5 mb-8">
            <div className="col-lg-12">
              <div className="required">Upload Data Peserta</div>
              <input
                type="file"
                className="form-control form-control-sm mb-2 mt-2"
                name="upload_template"
                accept=".csv"
                onChange={this.uploadCsv}
              />
              <span style={{ color: "red" }}>
                {this.state.errors["file_csv"]}
              </span>
            </div>
          </div>

          <div className="col-lg-12 mb-7 fv-row mt-8">
            <label className="form-label">Status Peserta</label>
            <Select
              name="status"
              placeholder="Silahkan pilih"
              noOptionsMessage={({ inputValue }) =>
                !inputValue ? this.state.datax : "Data tidak tersedia"
              }
              className="form-select-sm selectpicker p-0"
              options={this.state.option_role_dropdown}
              value={this.state.valStatus}
              onChange={this.handleChangeStatus}
            />
            <span style={{ color: "red" }}>{this.state.errors["status"]}</span>
          </div>

          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">
              Broadcast Email & Send Notification
            </label>
            <div className="d-flex">
              <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                <input
                  className="form-check-input"
                  onChange={this.isBroadcast}
                  type="radio"
                  name="jenis_pertanyaan"
                  value={1}
                  id="jenis_pertanyaan2"
                />
                <label className="form-check-label" htmlFor="jenis_pertanyaan2">
                  Ya
                </label>
              </div>
              <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                <input
                  className="form-check-input"
                  onChange={this.isBroadcast}
                  type="radio"
                  name="jenis_pertanyaan"
                  value={0}
                  id="jenis_pertanyaan1"
                />
                <label className="form-check-label" htmlFor="jenis_pertanyaan1">
                  Tidak
                </label>
              </div>
            </div>
            <span style={{ color: "red" }}>
              {this.state.errors["broadcast"]}
            </span>
          </div>

          {this.state.is_broadcast == 1 ? (
            <div>
              <div className="col-lg-12 mb-7 fv-row">
                <label className="form-label required">Subject</label>
                <input
                  className="form-control form-control-sm"
                  placeholder="Masukkan Subject Menu Disini"
                  name="subject"
                  value={this.state.subject}
                  onChange={this.handleChangeSubject}
                />
                <span style={{ color: "red" }}>
                  {this.state.errors["subject"]}
                </span>
              </div>
              <div className="form-group fv-row mb-7">
                <CKEditor
                  editor={Editor}
                  name="isi"
                  onReady={(editor) => {
                    // You can store the "editor" and use when it is needed.
                  }}
                  config={{
                    ckfinder: {
                      // Upload the images to the server using the CKFinder QuickUpload command.
                      uploadUrl:
                        process.env.REACT_APP_BASE_API_URI +
                        "/publikasi/ckeditor-upload-image",
                    },
                  }}
                  onChange={(event, editor) => {
                    const data = editor.getData();
                    this.handleChangeIsi(data);
                  }}
                  onBlur={(event, editor) => {}}
                  onFocus={(event, editor) => {}}
                />
                <span style={{ color: "red" }}>{this.state.errors["isi"]}</span>
              </div>
            </div>
          ) : (
            ""
          )}
          <div className="form-group fv-row my-7">
            <button
              type="submit"
              className="btn btn-md btn-block d-block btn-primary"
              disabled={this.state.isLoading}
            >
              {this.state.isLoading ? (
                <>
                  <span
                    className="spinner-border spinner-border-sm me-2"
                    role="status"
                    aria-hidden="true"
                  ></span>
                  <span className="sr-only">Loading...</span>Loading...
                </>
              ) : (
                <>
                  <i className="fa fa-paper-plane me-1"></i>Kirim
                </>
              )}
            </button>
          </div>
        </form>
      </div>
    );
  }
}
