import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";

export default class PromptUpdate extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmitAction.bind(this);
  }

  state = {
    datax: [],
    isLoading: false,
    loading: false,
    errors: {},
    checkedNotification: false,
    checkedEmail: false,
    notification_status: "",
    email_status: "",
    id_komponen: false,
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/setting/view_prompt",
        null,
        this.configs,
      )
      .then((res) => {
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statusx) {
          swal.close();
          const datax = res.data.result.Data[0];
          const id_komponen = datax.id_komponen;
          const training_rules = JSON.parse(datax.training_rules);
          const checkedNotification =
            training_rules["notification"][0]["status"] == 1 ? true : false;
          let notification_status = "Tidak Aktif";
          if (checkedNotification) {
            notification_status = "Aktif";
          }
          const checkedEmail =
            training_rules["email"][0]["status"] == 1 ? true : false;
          let email_status = "Tidak Aktif";
          if (checkedEmail) {
            email_status = "Aktif";
          }
          this.setState({
            id_komponen,
            checkedNotification,
            checkedEmail,
            notification_status,
            email_status,
          });
        }
      })
      .catch((error) => {
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  handleSubmitAction(e) {
    e.preventDefault();
    this.setState({ isLoading: true });
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    const body_json = [
      {
        id_komponen: this.state.id_komponen,
        update_by: Cookies.get("user_id"),
        training_rules: {
          notification: [{ status: this.state.checkedNotification ? 1 : 0 }],
          email: [{ status: this.state.checkedEmail ? 1 : 0 }],
        },
      },
    ];

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/setting/update_prompt",
        JSON.stringify(body_json),
        this.configs,
      )
      .then((res) => {
        this.setState({ isLoading: false });
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          swal
            .fire({
              title: messagex,
              icon: "success",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        } else {
          this.setState({ isLoading: false });
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      })
      .catch((error) => {
        this.setState({ isLoading: false });
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  render() {
    return (
      <div className="row">
        <h3>Prompt Update Notification</h3>
        <form className="form" action="#" onSubmit={this.handleSubmit}>
          <input
            type="hidden"
            name="csrf-token"
            value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
          />

          <div className="col-lg-12 col-md-12 col-xs-12 mt-5">
            <label className="form-label">
              <strong>Notification</strong>
            </label>
            <br />
            <FormControlLabel
              control={
                <Switch
                  checked={this.state.checkedNotification}
                  onChange={() => {
                    this.setState({
                      checkedNotification: !this.state.checkedNotification,
                      notification_status: this.state.checkedNotification
                        ? "Tidak Aktif"
                        : "Aktif",
                    });
                  }}
                  value="notification"
                />
              }
              label={this.state.notification_status}
            />
          </div>
          <div className="col-lg-12 col-md-12 col-xs-12 mt-8">
            <label className="form-label">
              <strong>Email</strong>
            </label>
            <br />
            <FormControlLabel
              control={
                <Switch
                  checked={this.state.checkedEmail}
                  onChange={() => {
                    this.setState({
                      checkedEmail: !this.state.checkedEmail,
                      email_status: this.state.checkedEmail
                        ? "Tidak Aktif"
                        : "Aktif",
                    });
                  }}
                  value="email"
                />
              }
              label={this.state.email_status}
            />
          </div>
          <div className="form-group fv-row my-7">
            <button
              type="submit"
              className="btn btn-md btn-block d-block btn-primary"
              disabled={this.state.isLoading}
            >
              {this.state.isLoading ? (
                <>
                  <span
                    className="spinner-border spinner-border-sm me-2"
                    role="status"
                    aria-hidden="true"
                  ></span>
                  <span className="sr-only">Loading...</span>Loading...
                </>
              ) : (
                <>
                  <i className="fa fa-paper-plane me-1"></i>Simpan
                </>
              )}
            </button>
          </div>
        </form>
      </div>
    );
  }
}
