import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import {
  kuotaNumericOnlyDirect,
  numericOnlyDirect,
} from "../../../publikasi/helper";

export default class FileSize extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmitAction.bind(this);
    this.handleChangeDocumentSize =
      this.handleChangeDocumentSizeAction.bind(this);
    this.handleChangeImageSize = this.handleChangeImageSizeAction.bind(this);
  }

  state = {
    datax: [],
    isLoading: false,
    loading: false,
    errors: {},
    id_komponen: false,
    image_size: false,
    document_size: false,
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/API_Get_File_Size",
        null,
        this.configs,
      )
      .then((res) => {
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statusx) {
          swal.close();
          const datax = res.data.result.Data[0];
          const id_komponen = datax.id_komponen;
          const training_rules = JSON.parse(datax.training_rules);
          const image_size = training_rules["image"][0]["size"];
          const document_size = training_rules["document"][0]["size"];
          this.setState({
            id_komponen,
            image_size,
            document_size,
          });
        }
      })
      .catch((error) => {
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const check = this.checkEmpty(dataForm, ["image", "documents"], [""]);
    return check;
  }

  resetError() {
    let errors = {};
    errors[("image", "documents")] = "";
    this.setState({ errors: errors });
  }

  checkEmpty(dataForm, fieldName, fileFieldName) {
    let errors = {};
    let formIsValid = true;
    for (const field in fieldName) {
      const nameAttribute = fieldName[field];

      if (dataForm.get(nameAttribute) == "") {
        errors[nameAttribute] = "Tidak Boleh Kosong";
        formIsValid = false;
      }
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  handleSubmitAction(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });
      const body_json = [
        {
          id_komponen: this.state.id_komponen,
          update_by: Cookies.get("user_id"),
          training_rules: {
            image: [{ size: this.state.image_size }],
            document: [{ size: this.state.document_size }],
          },
        },
      ];

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/API_Simpan_File_Size",
          JSON.stringify(body_json),
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      this.setState({ isLoading: false });
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleChangeDocumentSizeAction(e) {
    let size = e.currentTarget.value;
    size = numericOnlyDirect(size);
    this.setState({
      document_size: size,
    });
  }

  handleChangeImageSizeAction(e) {
    let size = e.currentTarget.value;
    size = numericOnlyDirect(size);
    this.setState({
      image_size: size,
    });
  }

  render() {
    return (
      <div className="row">
        <h3>File Size</h3>
        <form className="form" action="#" onSubmit={this.handleSubmit}>
          <input
            type="hidden"
            name="csrf-token"
            value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
          />
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">Image(MB)</label>
            <input
              className="form-control form-control-sm"
              placeholder="MB"
              name="image"
              type="number"
              value={this.state.image_size}
              onChange={this.handleChangeImageSize}
            />
            <span style={{ color: "red" }}>{this.state.errors["image"]}</span>
          </div>
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">Document(MB)</label>
            <input
              className="form-control form-control-sm"
              placeholder="MB"
              name="documents"
              type="number"
              value={this.state.document_size}
              onChange={this.handleChangeDocumentSize}
            />
            <span style={{ color: "red" }}>
              {this.state.errors["documents"]}
            </span>
          </div>
          <div className="form-group fv-row my-7">
            <button
              type="submit"
              className="btn btn-md btn-block d-block btn-primary"
              disabled={this.state.isLoading}
            >
              {this.state.isLoading ? (
                <>
                  <span
                    className="spinner-border spinner-border-sm me-2"
                    role="status"
                    aria-hidden="true"
                  ></span>
                  <span className="sr-only">Loading...</span>Loading...
                </>
              ) : (
                <>
                  <i className="fa fa-paper-plane me-1"></i>Simpan
                </>
              )}
            </button>
          </div>
        </form>
      </div>
    );
  }
}
