import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "@arbor-dev/ckeditor5-arbor-custom";

export default class TemplateEmail extends React.Component {
  constructor(props) {
    super(props);
    this.handleChangeStatus = this.handleChangeStatusAction.bind(this);
    this.handleChangeContent = this.handleChangeContentAction.bind(this);
    this.handleChangeSubject = this.handleChangeSubjectAction.bind(this);
    this.handleSubmit = this.handleSubmitAction.bind(this);
  }

  state = {
    datax: [],
    loading: false,
    isLoading: false,
    errors: {},
    option_role_dropdown: [],
    valStatus: [],
    training_rules: [],
    subject: "",
    content: "",
    status: false,
    id_komponen: false,
    id_status: false,
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/umum/list-status-peserta",
        null,
        this.configs,
      )
      .then((res) => {
        const option_role_peserta = res.data.result.Data;
        const option_role_dropdown = [];

        option_role_peserta.forEach(function (element, i) {
          const option = { value: element.id, label: element.name };
          option_role_dropdown.push(option);
        });
        this.setState({
          option_role_dropdown,
        });
      });

    const selectpicker = document.getElementsByClassName("selectpicker")[0];
    selectpicker.style.zIndex = "999";
  }

  handleSubmitAction(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });
      let subject = this.state.subject;
      let content = this.state.content;
      let subject_clean = subject.replace(/'/g, "`");
      let content_clean = content.replace(/'/g, "`");
      const body_json = [
        {
          id_komponen: this.state.id_komponen,
          id_status: this.state.id_status,
          update_by: Cookies.get("user_id"),
          training_rules: {
            subject: subject_clean,
            body: content_clean,
          },
        },
      ];
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/setting/update_template",
          JSON.stringify(body_json),
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      this.setState({ isLoading: false });
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const check = this.checkEmpty(
      dataForm,
      ["status", "subject", "content"],
      [],
    );
    return check;
  }

  resetError() {
    let errors = {};
    errors[("status", "subject", "content")] = "";
    this.setState({ errors: errors });
  }

  checkEmpty(dataForm, fieldName, fileFieldName) {
    let errors = {};
    let formIsValid = true;
    for (const field in fieldName) {
      const nameAttribute = fieldName[field];
      if (dataForm.get(nameAttribute) == "") {
        errors[nameAttribute] = "Tidak Boleh Kosong";
        formIsValid = false;
      }
    }
    if (this.state.status == false) {
      errors["status"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }
    if (this.state.content == "") {
      errors["content"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  handleChangeStatusAction(val) {
    const errors = this.state.errors;
    errors["status"] = "";
    this.setState(
      {
        valStatus: {
          label: val.label,
          value: val.value,
        },
        status: val.value,
        id_status: val.value,
        errors,
      },
      () => {
        let is_test_substansi = 0;
        if (val.value == 3) {
          is_test_substansi = 1;
        }
        this.setState({ is_test_substansi });
        swal.fire({
          title: "Mohon Tunggu!",
          icon: "info", // add html attribute if you want or remove
          allowOutsideClick: false,
          didOpen: () => {
            swal.showLoading();
          },
        });
        let dataBody = {
          id_status: val.value,
        };
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/setting/view_template",
            dataBody,
            this.configs,
          )
          .then((res) => {
            const datax = res.data.result.Data[0];
            const statusx = res.data.result.Status;
            let messagex = res.data.result.Message;
            if (statusx) {
              swal.close();
              const training_rules = JSON.parse(datax.training_rules);
              const id_komponen = datax.id_komponen;
              const subject = training_rules.subject;
              const content = training_rules.body;
              this.setState({
                training_rules,
                id_komponen,
                subject,
                content,
              });
            } else {
              swal
                .fire({
                  title: messagex,
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                  }
                });
            }
          })
          .catch((error) => {
            const subject = "";
            const content = "";
            this.setState({
              subject,
              content,
              id_komponen: 2,
            });
            swal.close();
          });
      },
    );
  }

  handleChangeContentAction(value) {
    const errors = this.state.errors;
    errors["content"] = "";
    this.state.content = value;
    this.setState({
      errors,
    });
  }

  handleChangeSubjectAction(e) {
    const errors = this.state.errors;
    errors["subject"] = "";
    const subject = e.currentTarget.value;
    this.setState({
      subject,
      errors,
    });
  }

  render() {
    return (
      <div className="row">
        <h3>Template Email</h3>
        <form className="form" action="#" onSubmit={this.handleSubmit}>
          <input
            type="hidden"
            name="csrf-token"
            value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
          />

          <div className="col-lg-12 mb-7 fv-row mt-8">
            <label className="form-label required">Status</label>
            <Select
              name="status"
              placeholder="Silahkan pilih"
              noOptionsMessage={({ inputValue }) =>
                !inputValue ? this.state.datax : "Data tidak tersedia"
              }
              className="form-select-sm selectpicker p-0"
              options={this.state.option_role_dropdown}
              value={this.state.valStatus}
              onChange={this.handleChangeStatus}
            />
            <span style={{ color: "red" }}>{this.state.errors["status"]}</span>
          </div>
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">Subject</label>
            <input
              className="form-control form-control-sm"
              placeholder="Masukkan Subject Menu Disini"
              name="subject"
              value={this.state.subject}
              onChange={this.handleChangeSubject}
              //disabled={this.state.status == false ? true : false}
            />
            <span style={{ color: "red" }}>{this.state.errors["subject"]}</span>
          </div>
          {this.state.is_test_substansi == 1 ? (
            <div
              className="col-lg-12 mb-7 fv-row text-muted"
              style={{ fontSize: "10px" }}
            >
              <strong>Keterangan Status</strong>
              <ul>
                <li>
                  Gunakan <span className="text-danger">type</span> untuk
                  menampilkan status tipe subvit peserta (Tes Substansi, Trivia,
                  atau Survey).
                </li>
                <li>
                  Gunakan <span className="text-danger">pelatihan</span> untuk
                  menampilkan pelatihan yang diikuti oleh peserta.
                </li>
              </ul>
            </div>
          ) : (
            ""
          )}
          <div className="form-group fv-row mb-7">
            <CKEditor
              editor={Editor}
              name="isi"
              data={this.state.content}
              onReady={(editor) => {
                // You can store the "editor" and use when it is needed.
              }}
              config={{
                ckfinder: {
                  // Upload the images to the server using the CKFinder QuickUpload command.
                  uploadUrl:
                    process.env.REACT_APP_BASE_API_URI +
                    "/publikasi/ckeditor-upload-image",
                },
              }}
              onChange={(event, editor) => {
                const data = editor.getData();
                this.handleChangeContent(data);
              }}
              onBlur={(event, editor) => {}}
              onFocus={(event, editor) => {}}
            />
            <span style={{ color: "red" }}>{this.state.errors["content"]}</span>
          </div>
          <div className="form-group fv-row my-7">
            <button
              type="submit"
              className="btn btn-md btn-block d-block btn-primary"
              disabled={this.state.isLoading}
            >
              {this.state.isLoading ? (
                <>
                  <span
                    className="spinner-border spinner-border-sm me-2"
                    role="status"
                    aria-hidden="true"
                  ></span>
                  <span className="sr-only">Loading...</span>Loading...
                </>
              ) : (
                <>
                  <i className="fa fa-paper-plane me-1"></i>Simpan
                </>
              )}
            </button>
          </div>
        </form>
      </div>
    );
  }
}
