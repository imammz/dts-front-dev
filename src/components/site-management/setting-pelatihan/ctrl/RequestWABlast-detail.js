import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../RequestWABlast-View-Content-Detail";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const RequestWABlastViewContentDetail = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default RequestWABlastViewContentDetail;
