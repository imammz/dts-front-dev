import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../RequestWABlast-View-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const RequestWABlastView = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default RequestWABlastView;
