import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../RequestWABlast-Tambah-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const RequestWABlastTambah = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default RequestWABlastTambah;
