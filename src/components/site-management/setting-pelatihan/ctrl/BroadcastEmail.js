import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../BroadcastEmail-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const BroadcastEmail = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default BroadcastEmail;
