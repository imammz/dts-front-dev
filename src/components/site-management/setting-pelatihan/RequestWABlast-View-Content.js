import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import {
  capitalizeFirstLetter,
  capitalizeTheFirstLetterOfEachWord,
} from "../../publikasi/helper";
import moment from "moment/moment";

export default class RequestWABlastViewContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleChangeSearch = this.handleChangeSearchAction.bind(this);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
  }

  state = {
    datax: [],
    loading: false,
    totalRows: "",
    tempLastNumber: 0,
    column: "",
    sortDirection: "role_id DESC",
    sort: "role_id DESC",
    param: "",
    currentPage: 1,
    newPerPage: 10,
  };

  arrayDefaultRole = [1, 11, 86, 107, 109, 110, 117, 120];

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  columns = [
    {
      name: "No",
      center: "true",
      cell: (row, index) => row.number,
      width: "70px",
    },
    {
      name: "ID",
      width: "100px",
      sortable: true,
      selector: (row) =>
        capitalizeFirstLetter(row.pelatihan.slug_akademi + row.pelatihan.id),
      cell: (row) => {
        return (
          <div>
            <span className="fs-7">
              {row.pelatihan.slug_akademi + row.pelatihan.id}
            </span>
          </div>
        );
      },
    },
    {
      name: "Nama Pelatihan",
      sortable: true,
      selector: (row) => row.pelatihan.nama_pelatihan,
      width: "350px",
      cell: (row) => (
        <div>
          <a
            href={"/site-management/whatsapp-blast/detail/" + row.id}
            id={row.id}
            title="Detail"
            className="text-dark"
          >
            <label className="d-flex flex-stack my-2 cursor-pointer">
              <span className="d-flex align-items-center me-2">
                <span className="symbol symbol-50px me-6">
                  <span className="symbol-label bg-light-primary">
                    <span className="svg-icon svg-icon-1 svg-icon-primary">
                      {row.mitra_logo == null ? (
                        <img
                          src={`/assets/media/logos/logo-kominfo.png`}
                          alt=""
                          height="100px"
                          className="symbol-label"
                        />
                      ) : (
                        <img
                          src={
                            process.env.REACT_APP_BASE_API_URI +
                            "/download/get-file?path=" +
                            row.mitra_logo +
                            "&disk=dts-storage-partnership"
                          }
                          alt=""
                          className="symbol-label"
                        />
                      )}
                    </span>
                  </span>
                </span>
                <span className="d-flex flex-column my-2">
                  <span className="text-muted fs-7 fw-semibold">
                    {row.pelatihan.metode_pelatihan}
                  </span>
                  <span
                    className="fw-bolder fs-7"
                    style={{
                      overflow: "hidden",
                      whiteSpace: "wrap",
                      textOverflow: "ellipses",
                    }}
                  >
                    {row.pelatihan.nama_pelatihan} (Batch{" "}
                    {row.pelatihan.batch ? row.pelatihan.batch : "1"})
                  </span>
                  <h6 className="text-muted fs-7 fw-semibold mb-1">
                    {row.nama_penyelenggara}
                  </h6>
                </span>
              </span>
            </label>
          </a>
        </div>
      ),
    },
    {
      name: "Total Penerima",
      width: "150px",
      sortable: true,
      selector: (row) => (row.peserta ? JSON.parse(row.peserta).length : 0),
      cell: (row) => (
        <div>
          <span className="fs-7">
            {row.peserta ? JSON.parse(row.peserta).length : 0}
          </span>
        </div>
      ),
    },
    {
      name: "Log",
      sortable: true,
      center: false,
      //width: '180px',
      selector: (row) => row.name,
      cell: (row) => (
        <div>
          <span className="fw-semibold fs-7">{row.name}</span>
          <br />
          <span className="fs-8">{row.created_at}</span>
        </div>
      ),
    },
    {
      name: "Status",
      sortable: true,
      center: true,
      selector: (row) => row.status,
      cell: (row) => (
        <div>
          {row.status == 1 ? (
            <span className={"badge badge-light-primary fs-7 d-block m-1"}>
              Requested
            </span>
          ) : row.status == 2 ? (
            <span className={"badge badge-light-success fs-7 d-block m-1"}>
              Selesai Diproses
            </span>
          ) : row.status == 3 ? (
            <span className={"badge badge-light-danger fs-7 d-block m-1"}>
              Ditolak
            </span>
          ) : (
            <span className={"badge badge-light-danger fs-7 d-block m-1"}>
              Unknown
            </span>
          )}
        </div>
      ),
    },
    {
      name: "Aksi",
      center: true,
      right: true,
      cell: (row) => (
        <div>
          <a
            href={"/site-management/whatsapp-blast/detail/" + row.id}
            title="Detail"
            className="btn btn-icon btn-bg-primary btn-sm me-1"
          >
            <i className="bi bi-eye-fill text-white"></i>
          </a>
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    this.handleReload();
  }

  handleReload(page, newPerPage) {
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });

    let dataBody = {
      id_akademi: 0,
      id_penyelenggara: 0,
      id_tema: 0,
      provinsi: 0,
      status_pelatihan: 0,
      status_substansi: 0,
      tahun: 2023,
      mulai: start_tmp,
      limit: length_tmp,
      param: this.state.param,
      sort: this.state.sort,
      status_publish: 99,
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/whatsapp-broadcast-request-list",
        dataBody,
        this.configs,
      )
      .then((res) => {
        this.setState({ loading: false });
        let datax = [];
        if (this.state.param) {
          datax = res.data.result.Data.filter((item) => {
            // Filter only column pelatihan.nama_pelatihan and name
            return (
              item.pelatihan.nama_pelatihan
                .toLowerCase()
                .includes(this.state.param.toLowerCase()) ||
              item.name.toLowerCase().includes(this.state.param.toLowerCase())
            );
          });
        } else {
          datax = res.data.result.Data;
        }
        /** Add Number Column */
        datax = datax.map((item, index) => {
          return {
            ...item,
            number: index + 1,
          };
        });
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        const total = res.data.result.Total;
        if (statusx) {
          this.setState({ datax });
          this.setState({ totalRows: total });
          this.setState({ currentPage: page });
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      })
      .catch((error) => {
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      });
  }
  handlePageChange = (page) => {
    this.setState({ loading: true });
    this.handleReload(page, this.state.newPerPage);
  };
  handlePerRowsChange = async (newPerPage, page) => {
    this.setState({ loading: true });
    this.setState({ newPerPage: newPerPage });
    this.handleReload(page, newPerPage);
  };

  handleChangeSearchAction(e) {
    const searchText = e.currentTarget.value;
    if (searchText == "") {
      this.setState(
        {
          loading: true,
          param: "",
        },
        () => {
          this.handleReload();
        },
      );
    }
  }

  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    if (e.key == "Enter") {
      if (searchText == "") {
        this.setState(
          {
            isSearch: false,
            param: "",
          },
          () => {
            this.handleReload();
          },
        );
      } else {
        this.setState({ loading: true });
        this.setState({ isSearch: true });
        this.setState({ param: searchText }, () => {
          this.handleReload();
        });
      }
    }
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  WhatsApp Blast
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <a
                  href={"/site-management/whatsapp-blast/tambah"}
                  className="btn btn-success fw-bolder btn-sm"
                >
                  <i className="bi bi-plus-circle"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Tambah Pengajuan
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Riwayat Pengajuan
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <div className="d-flex align-items-center position-relative my-1 me-2">
                            <span className="svg-icon svg-icon-1 position-absolute ms-6">
                              <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className="mh-50px"
                              >
                                <rect
                                  opacity="0.5"
                                  x="17.0365"
                                  y="15.1223"
                                  width="8.15546"
                                  height="2"
                                  rx="1"
                                  transform="rotate(45 17.0365 15.1223)"
                                  fill="currentColor"
                                ></rect>
                                <path
                                  d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                  fill="currentColor"
                                ></path>
                              </svg>
                            </span>
                            <input
                              type="text"
                              data-kt-user-table-filter="search"
                              className="form-control form-control-sm form-control-solid w-250px ps-14"
                              placeholder="Cari"
                              onKeyPress={this.handleKeyPress}
                              onChange={this.handleChangeSearch}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="table-responsive">
                          <DataTable
                            columns={this.columns}
                            data={this.state.datax}
                            progressPending={this.state.loading}
                            highlightOnHover
                            pointerOnHover
                            pagination
                            customStyles={this.customStyles}
                            persistTableHead={true}
                            noDataComponent={
                              <div className="mt-5">Tidak Ada Data</div>
                            }
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
