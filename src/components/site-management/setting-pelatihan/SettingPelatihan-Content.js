import React from "react";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import PromptUpdate from "./form/PromptUpdate";
import TemplateEmail from "./form/TemplateEmail";
import SUBM from "./form/SUBM";
import FileSize from "./form/FileSize";
import KetentuanPelatihan from "./form/KetentuanPelatihan";

export default class SettingPelatihanContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmitAction.bind(this);
  }

  state = {
    datax: [],
    isLoading: false,
    errors: {},
    selected: 1,
    hover: 0,
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  style = {
    selectedSide: {
      padding: "16px",
      backgroundColor: "#ecf5fc",
      marginBottom: "8px",
      borderRadius: "8px",
      //fontSize: '16px',
      fontWeight: "700",
      width: "95%",
    },
    normalSide: {
      padding: "16px",
      //backgroundColor: '#ecf5fc',
      marginBottom: "8px",
      borderRadius: "8px",
      //fontSize: '16px',
      fontWeight: "700",
      width: "95%",
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    } else {
      if (localStorage.getItem("from_subm") == 1) {
        this.setState(
          {
            selected: 3,
            hover: 3,
          },
          () => {
            localStorage.setItem("from_subm", 0);
          },
        );
      }
    }
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const check = this.checkEmpty(
      dataForm,
      [
        "deskripsi",
        "alamat",
        "primary",
        "secondary",
        "extras",
        "email",
        "koordinat",
      ],
      [""],
    );
    return check;
  }

  resetError() {
    let errors = {};
    errors[
      ("deskripsi",
      "alamat",
      "primary",
      "secondary",
      "extras",
      "email",
      "koordinat")
    ] = "";
    this.setState({ errors: errors });
  }

  validURL(str) {
    var pattern = new RegExp(
      "^(https?:\\/\\/)?" + // protocol
        "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
        "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
        "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
        "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
        "(\\#[-a-z\\d_]*)?$",
      "i",
    ); // fragment locator
    return !!pattern.test(str);
  }

  checkEmpty(dataForm, fieldName, fileFieldName) {
    let errors = {};
    let formIsValid = true;
    for (const field in fieldName) {
      const nameAttribute = fieldName[field];
      if (dataForm.get(nameAttribute) == "") {
        errors[nameAttribute] = "Tidak Boleh Kosong";
        formIsValid = false;
      }
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  handleSubmitAction(e) {
    let timestamp = 1293683278;
    let date = new Date(timestamp * 1000);
    let ymdhis = date
      .toISOString()
      .match(/(\d{4}\-\d{2}\-\d{2})T(\d{2}:\d{2}:\d{2})/);
    ymdhis = ymdhis[1] + " " + ymdhis[2];

    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Pelatihan
                </h1>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Setting Pelatihan
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="row">
                        <div
                          style={{ height: "max-content" }}
                          className="col-lg-3 border-end"
                        >
                          <div className="card-body">
                            <div
                              className="fv-row"
                              style={
                                this.state.selected == 1 ||
                                this.state.hover == 1
                                  ? this.style.selectedSide
                                  : this.style.normalSide
                              }
                              onMouseEnter={() => this.setState({ hover: 1 })}
                              onMouseLeave={() => this.setState({ hover: 0 })}
                            >
                              <a
                                href="#"
                                onClick={() => {
                                  this.setState({ selected: 1 });
                                }}
                              >
                                Prompt Update
                              </a>
                            </div>
                            <div
                              className="fv-row mt-4"
                              style={
                                this.state.selected == 2 ||
                                this.state.hover == 2
                                  ? this.style.selectedSide
                                  : this.style.normalSide
                              }
                              onMouseEnter={() => this.setState({ hover: 2 })}
                              onMouseLeave={() => this.setState({ hover: 0 })}
                            >
                              <a
                                href="#"
                                onClick={() => {
                                  this.setState({ selected: 2 });
                                }}
                              >
                                Template Email
                              </a>
                            </div>
                            {/*<div className="fv-row mt-4"
                                                                style={this.state.selected == 3 || this.state.hover == 3 ? this.style.selectedSide : this.style.normalSide}
                                                                onMouseEnter={() => this.setState({ hover: 3 })}
                                                                onMouseLeave={() => this.setState({ hover: 0 })}
                                                            >
                                                                <a href="#" onClick={() => { this.setState({ selected: 3 }) }}>Broadcast Email</a>
                                                            </div>*/}
                            <div
                              className="fv-row mt-4"
                              style={
                                this.state.selected == 4 ||
                                this.state.hover == 4
                                  ? this.style.selectedSide
                                  : this.style.normalSide
                              }
                              onMouseEnter={() => this.setState({ hover: 4 })}
                              onMouseLeave={() => this.setState({ hover: 0 })}
                            >
                              <a
                                href="#"
                                onClick={() => {
                                  this.setState({ selected: 4 });
                                }}
                              >
                                File Size
                              </a>
                            </div>
                            <div
                              className="fv-row mt-4"
                              style={
                                this.state.selected == 5 ||
                                this.state.hover == 5
                                  ? this.style.selectedSide
                                  : this.style.normalSide
                              }
                              onMouseEnter={() => this.setState({ hover: 5 })}
                              onMouseLeave={() => this.setState({ hover: 0 })}
                            >
                              <a
                                href="#"
                                onClick={() => {
                                  this.setState({ selected: 5 });
                                }}
                              >
                                Ketentuan Pelatihan
                              </a>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-9">
                          <div className="card-body">
                            {this.state.selected == 1 ? <PromptUpdate /> : ""}
                            {this.state.selected == 2 ? <TemplateEmail /> : ""}
                            {/*{this.state.selected == 3 ?
                                                                <SUBM /> : ''
                                                            }*/}
                            {this.state.selected == 4 ? <FileSize /> : ""}
                            {this.state.selected == 5 ? (
                              <KetentuanPelatihan />
                            ) : (
                              ""
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
