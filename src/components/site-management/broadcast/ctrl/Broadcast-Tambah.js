import { useEffect } from "react";
import Header from "../../../Header";
import Content from "../Broadcast-Tambah-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";
import { cekPermition } from "../../../AksesHelper";

const BroadcastTambah = () => {
  useEffect(() => {
    if (cekPermition().view !== 1) {
      // Swal.fire({
      //     title: "Akses Tidak Diizinkan",
      //     html: "<i> Anda akan kembali kehalaman login </i>",
      //     icon: "warning",
      //   }).then(()=>{
      //     logout();
      //   });
    }
  }, []);

  return (
    <div>
      <SideNav />
      <Header />
      <Content />
      <Footer />
    </div>
  );
};

export default BroadcastTambah;
