import Header from "../../../Header";
import Content from "../Broadcast-Edit-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const BroadcastEdit = () => {
  return (
    <div>
      <SideNav />
      <Header />
      <Content />
      <Footer />
    </div>
  );
};

export default BroadcastEdit;
