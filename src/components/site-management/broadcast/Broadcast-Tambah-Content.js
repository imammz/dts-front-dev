import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "@arbor-dev/ckeditor5-arbor-custom";
import Select from "react-select";
import "./style_tags.css";
import Flatpickr from "react-flatpickr";
import { Indonesian } from "flatpickr/dist/l10n/id.js";
import moment from "moment";

export default class BroadcastTambahContent extends React.Component {
  constructor(props) {
    super(props);
    this.formDatax = new FormData();
    this.handleClick = this.handleClickAction.bind(this);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleChangeThumbnail = this.handleChangeThumbnailAction.bind(this);
    this.handleChangeIsi = this.handleChangeIsiAction.bind(this);
    this.handleChangeJudul = this.handleChangeJudulAction.bind(this);
    this.handleChangeKategori = this.handleChangeKategoriAction.bind(this);
    this.handleChangeStatus = this.handleChangeStatusAction.bind(this);
    this.handleChangeJenis = this.handleChangeJenisAction.bind(this);
    this.handleChangeLevel = this.handleChangeLevelAction.bind(this);
    this.handleChangeAkademi = this.handleChangeAkademiAction.bind(this);
    this.handleChangeTema = this.handleChangeTemaAction.bind(this);
    this.handleChangePelatihan = this.handleChangePelatihanAction.bind(this);
    this.handleChangeContent = this.handleChangeContentAction.bind(this);
    this.handleChangeStatusPeserta =
      this.handleChangeStatusPesertaAction.bind(this);
    this.state = {
      datax: [],
      isLoading: false,
      loading: true,
      optionsakademi: [],
      optionstema: [],
      optionspelatihan: [],
      optionsstatuspeserta: [],
      tags: [],
      fields: {},
      errors: {},
      isi_broadcast: "",
      thumbnail: "",
      judul: "",
      level_broadcast: "",
      valStatus: "",
      jenis_broadcast: "",
      tanggal_publish_mulai: "",
      tanggal_publish_selesai: "",
      id_akademi: null,
      id_tema: null,
      id_pelatihan: null,
      status_peserta: null,
      valContent: "",
    };
    this.formDatax = new FormData();
  }

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  optionstatus = [
    { value: "1", label: "Publish" },
    { value: "0", label: "Unpublish" },
  ];

  optioncontents = [
    { value: "Image", label: "Image" },
    { value: "Video", label: "Video" },
    { value: "Rich Content", label: "Rich Content" },
  ];

  optionjenis = [
    { value: 1, label: "Public" },
    { value: 2, label: "Peserta" },
  ];

  optionlevel = [
    { value: 1, label: "Semua" },
    { value: 2, label: "Akademi" },
    { value: 3, label: "Tema" },
    { value: 4, label: "Pelatihan" },
  ];

  handleChangeStatusPesertaAction(selected) {
    this.setState({ status_peserta: selected });
  }
  handleChangeContentAction(selected) {
    this.setState({ valContent: selected });
  }
  handleChangeAkademiAction(selected) {
    this.setState({ id_akademi: selected.value }, () => {
      if (this.state.level_broadcast > 2) {
        // get tema by akademi
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/cari_tema_byakademi",
            { start: 1, rows: 100, id: this.state.id_akademi },
            this.configs,
          )
          .then((res) => {
            const optionx = res.data.result.Data;
            const optionstema = [];
            optionx.map((data) =>
              optionstema.push({ value: data.id, label: data.name }),
            );
            this.setState({ optionstema });
            this.setState({ id_tema: null });
          })
          .catch((err) => {
            let messagex = err.response.data.result.Message;
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          });
      }
    });
  }
  handleChangeTemaAction(selected) {
    this.setState({ id_tema: selected.value }, () => {
      if (this.state.level_broadcast > 3) {
        // get pelatihan by tema
        const dataBody = {
          mulai: 0,
          limit: 1000,
          id_penyelenggara: 0,
          id_akademi: this.state.id_akademi,
          id_tema: this.state.id_tema,
          status_substansi: 0,
          status_pelatihan: 0,
          status_publish: 99,
          provinsi: 0,
          param: null,
          sort: "id_pelatihan",
          sortval: "DESC",
          tahun: new Date().getFullYear(),
          id_silabus: 0,
        };

        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/pelatihan",
            dataBody,
            this.configs,
          )
          .then((res) => {
            const optionx = res.data.result.Data;
            const optionspelatihan = [];
            optionx.map((data) => {
              const metode_pelatihan =
                data.metode_pelatihan == "Online"
                  ? data.metode_pelatihan.toUpperCase()
                  : data.nm_kab + " " + data.nm_prov;
              optionspelatihan.push({
                value: data.id,
                label:
                  "[" +
                  data.penyelenggara +
                  "] - " +
                  data.slug_pelatian_id +
                  " - " +
                  data.pelatihan +
                  " Batch " +
                  data.batch +
                  " - " +
                  metode_pelatihan,
              });
            });
            this.setState({
              optionspelatihan,
              id_pelatihan: [],
            });
          })
          .catch((error) => {
            const optionspelatihan = [];
            this.setState({
              optionspelatihan,
              id_pelatihan: [],
            });
            let messagex = error.response.data.result.Message;
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          });
      }
    });
  }
  handleChangePelatihanAction(e) {
    this.setState({ id_pelatihan: e.value });
  }
  handleChangeJenisAction(e) {
    let selected = e.currentTarget.value;
    this.setState({ jenis_broadcast: e.currentTarget.value }, () => {
      if (selected == 1) {
        this.setState({ level_broadcast: "" }, () => {
          this.setState({
            id_akademi: null,
            id_tema: null,
            id_pelatihan: null,
          });
        });
      }
    });
  }

  handleChangeLevelAction(e) {
    this.setState({ level_broadcast: e.currentTarget.value }, (val) => {
      if (
        parseInt(this.state.level_broadcast) > 1 &&
        this.state.optionsakademi.length === 0
      ) {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/publikasi/list-akademi",
            { start: 0, length: 1000, status: "publish" },
            this.configs,
          )
          .then((res) => {
            const optionx = res.data.result.Data;
            const optionsakademi = [];
            optionx.map((data) =>
              optionsakademi.push({
                value: data.id,
                label: data.name + " (" + data?.slug + ")",
              }),
            );
            this.setState({ optionsakademi });
            this.setState({ selakademi: [] });
          })
          .catch((err) => {
            let messagex = err.response.data.result.Message;
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          });
      }
      if (parseInt(this.state.level_broadcast) == 2) {
        this.setState({
          id_tema: null,
          id_pelatihan: null,
        });
      } else if (parseInt(this.state.level_broadcast) == 3) {
        this.setState({
          id_pelatihan: null,
        });
      }
    });
  }
  handleClickBatalAction(e) {
    swal
      .fire({
        title: "Apakah Anda Yakin?",
        icon: "warning",
        confirmButtonText: "Ya",
        showCancelButton: true,
        cancelButtonText: "Tidak",
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
      })
      .then((result) => {
        if (result.isConfirmed) {
          //window.location = '/publikasi/broadcast';
          window.history.back();
        }
      });
  }

  handleChangeThumbnailAction(e) {
    const logofile = e.target.files[0];
    this.formDatax.append("thumbnail", logofile);
  }

  handleChangeIsiAction(value) {
    this.setState({ isi_broadcast: value });
  }

  handleChangeJudulAction(e) {
    const judul = e.currentTarget.value;
    this.setState({
      judul,
    });
  }

  handleChangeKategoriAction = (selectedOption) => {
    this.setState(
      {
        //level_footer: selectedOption.value,
        valLevel: {
          value: selectedOption.value,
          label: selectedOption.label,
        },
      },
      () => {},
    );
  };

  handleChangeStatusAction = (selectedOption) => {
    this.setState(
      {
        //level_footer: selectedOption.value,
        valStatus: { value: selectedOption.value, label: selectedOption.label },
      },
      () => {},
    );
  };

  validate(formElement) {
    const errors = this.state.errors;
    // konten validation
    if (
      this.state.valContent?.value == "Rich Content" &&
      (this.state.isi_broadcast == "" || this.state.isi_broadcast == null)
    ) {
      errors["isi"] = "Tidak Boleh Kosong";
    }
    if (this.state.valContent?.value == "Image") {
      let file = document.getElementById("thumbnail").files[0];
      console.log("file", file);
      if (!file) {
        errors["thumbnail"] = "Tidak Boleh Kosong";
      }
    }
    // konten validation end
    if (
      this.state.tanggal_publish_mulai == "" ||
      this.state.tanggal_publish_mulai == null
    ) {
      errors["tanggal_publish_mulai"] = "Tidak Boleh Kosong";
    }
    if (
      this.state.tanggal_publish_selesai == "" ||
      this.state.tanggal_publish_selesai == null
    ) {
      errors["tanggal_publish_selesai"] = "Tidak Boleh Kosong";
    }
    if (this.state.valContent == "" || this.state.valContent == null) {
      errors["jenis_konten"] = "Tidak Boleh Kosong";
    }
    if (this.state.valStatus == "" || this.state.valStatus == null) {
      errors["status"] = "Tidak Boleh Kosong";
    }
    if (this.state.jenis_broadcast != 1 && !this.state.status_peserta) {
      errors["status_peserta"] = "Tidak Boleh Kosong";
    }
    // check trigger field
    if (this.state.level_broadcast > 1) {
      if (!this.state.id_akademi) {
        errors["id_akademi"] = "Tidak Boleh Kosong";
      }
    }
    if (this.state.level_broadcast > 2) {
      if (!this.state.id_tema) {
        errors["id_tema"] = "Tidak Boleh Kosong";
      }
    }
    if (this.state.level_broadcast > 3) {
      if (!this.state.id_pelatihan) {
        errors["id_pelatihan"] = "Tidak Boleh Kosong";
      }
    }

    this.setState({ errors: errors }, () => {
      const inputs = formElement.querySelectorAll("input");
      inputs.forEach((input) => {
        input.focus();
        input.blur();
      });
    });
    console.log(errors);
    return Object.keys(this.state.errors).length == 0;
  }

  handleClickAction(e) {
    const dataForm = new FormData(e.currentTarget);
    e.preventDefault();
    if (this.validate(e.target)) {
      this.setState({ isLoading: true });
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });

      const dataFormSubmit = new FormData();
      dataFormSubmit.append("jenis", this.state.jenis_broadcast);
      dataFormSubmit.append("judul", dataForm.get("judul"));
      dataFormSubmit.append(
        "level",
        this.state.level_broadcast == "" ? 0 : this.state.level_broadcast,
      );

      let sendStatus =
        this.state.jenis_broadcast == 2
          ? this.state.status_peserta.map((val) => ({
              id: val.value,
            }))
          : [];
      dataFormSubmit.append("status_peserta", JSON.stringify(sendStatus));
      dataFormSubmit.append(
        "akademi_id",
        this.state.level_broadcast >= 2 ? this.state.id_akademi : "",
      );
      dataFormSubmit.append(
        "tema_id",
        this.state.level_broadcast >= 3 ? this.state.id_tema : "",
      );
      dataFormSubmit.append(
        "pelatihan_id",
        this.state.level_broadcast >= 4 ? this.state.id_pelatihan : "",
      );
      dataFormSubmit.append(
        "tanggal_publish_mulai",
        this.handleDate(this.state.tanggal_publish_mulai),
      );
      dataFormSubmit.append(
        "tanggal_publish_selesai",
        this.handleDate(this.state.tanggal_publish_selesai),
      );
      dataFormSubmit.append("publish", dataForm.get("status"));
      dataFormSubmit.append("jenis_content", this.state.valContent.value);
      dataFormSubmit.append(
        "url",
        this.state.valContent.value == "Video"
          ? dataForm.get("link_video")
          : null,
      );
      dataFormSubmit.append(
        "gambar",
        this.state.valContent.value == "Image"
          ? this.formDatax.get("thumbnail")
          : null,
      );
      dataFormSubmit.append(
        "isi",
        this.state.valContent.value == "Rich Content"
          ? this.state.isi_broadcast
          : null,
      );

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/broadcast/tambah",
          dataFormSubmit,
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
                allowOutsideClick: false,
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/site-management/broadcast";
                }
              });
          } else {
            this.setState({ isLoading: false });
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      this.setState({ isLoading: false });
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleDate(dateString) {
    if (moment(dateString, "DD MMMM YYYY").isValid()) {
      return moment(dateString, "DD MMMM YYYY").format("YYYY-MM-DD");
    }
    return dateString;
  }

  validURL(str, key) {
    const errors = this.state.errors;
    var pattern = new RegExp(
      "^(https?:\\/\\/)?" + // protocol
        "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
        "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
        "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
        "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
        "(\\#[-a-z\\d_]*)?$",
      "i",
    ); // fragment locator
    if (!!pattern.test(str)) {
      delete errors[key];
      this.setState({ errors });
    } else {
      errors[key] = "Bukan Valid URL";
      this.setState({ errors });
    }
  }
  emptyValidation(value, key) {
    const errors = this.state.errors;
    if (value == "" || value == null) {
      errors[key] = "Tidak boleh kosong";
      this.setState({ errors });
      return Promise.reject("Tidak boleh kosong");
    } else {
      delete errors[key];
      this.setState({ errors });
      return Promise.resolve(value);
    }
  }

  lengthValidation(value, key) {
    const errors = this.state.errors;
    if (!typeof value !== "string" && value.length === 0) {
      errors[key] = "Input harus berupa string";
      this.setState({ errors });
      return Promise.reject("Tidak boleh kosong");
    } else if (value.length > 160) {
      errors[key] = "Panjang " + key + " tidak boleh melebihi 160 karakter";
      this.setState({ errors });
      return Promise.reject(
        "Panjang " + key + " tidak boleh melebihi 160 karakter",
      );
    } else {
      delete errors[key];
      this.setState({ errors });
      return Promise.resolve(value);
    }
  }

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/umum/list-status-peserta",
        {},
        this.configs,
      )
      .then((res) => {
        if (res.data?.result?.Data && res.data?.result?.Data[0]) {
          const optionsstatuspeserta = [];
          res.data.result.Data.forEach((elem) => {
            optionsstatuspeserta.push({
              label: elem.name,
              value: elem.id,
            });
          });
          this.setState({ optionsstatuspeserta });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="32"
                      height="32"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        opacity="0.3"
                        d="M12.9 10.7L3 5V19L12.9 13.3C13.9 12.7 13.9 11.3 12.9 10.7Z"
                        fill="currentColor"
                      ></path>
                      <path
                        d="M21 10.7L11.1 5V19L21 13.3C22 12.7 22 11.3 21 10.7Z"
                        fill="#f1416c"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Publikasi
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Broadcast
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Tambah Broadcast
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <form
                        className="form"
                        action="#"
                        onSubmit={this.handleClick}
                      >
                        <input
                          type="hidden"
                          name="csrf-token"
                          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                        />

                        <div className="form-group fv-row mb-7">
                          <label className="form-label">Jenis Broadcast</label>
                          <div className="d-flex">
                            {this.optionjenis.map((elem, index) => (
                              <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                <input
                                  className="form-check-input"
                                  type="radio"
                                  name="jenis_broadcast"
                                  value={elem.value}
                                  id={`"jenis_broadcast${index}"`}
                                  onChange={this.handleChangeJenis}
                                />
                                <label
                                  className="form-check-label"
                                  htmlFor={`"jenis_broadcast${index}"`}
                                >
                                  {elem.label}
                                </label>
                              </div>
                            ))}
                          </div>
                          <span style={{ color: "red" }}>
                            {this.state.errors["jenis_broadcast"]}
                          </span>
                        </div>
                        {this.state.jenis_broadcast == 2 && (
                          <>
                            <div className="form-group fv-row mb-7">
                              <label className="form-label">
                                Level Broadcast
                              </label>
                              <div className="d-flex">
                                {this.optionlevel.map((elem, index) => (
                                  <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                    <input
                                      className="form-check-input"
                                      type="radio"
                                      name="level_broadcast"
                                      value={elem.value}
                                      id={`"level_broadcast${index}"`}
                                      onChange={this.handleChangeLevel}
                                    />
                                    <label
                                      className="form-check-label"
                                      htmlFor={`"level_broadcast${index}"`}
                                    >
                                      {elem.label}
                                    </label>
                                  </div>
                                ))}
                              </div>
                              <span style={{ color: "red" }}>
                                {this.state.errors["jenis_broadcast"]}
                              </span>
                            </div>
                          </>
                        )}

                        {this.state.level_broadcast > 1 && (
                          <div className="form-group fv-row mb-7">
                            <label className="form-label required">
                              Akademi
                            </label>
                            <Select
                              id="id_akademi"
                              name="id_akademi"
                              placeholder="Silahkan pilih akademi"
                              noOptionsMessage={() => "Data tidak tersedia"}
                              className="form-select-sm selectpicker p-0"
                              onChange={(e) => {
                                this.handleChangeAkademi(e);
                                this.emptyValidation(e.value, "id_akademi")
                                  .then((value) => {})
                                  .catch((err) => {});
                              }}
                              options={this.state.optionsakademi}
                              isDisabled={this.state.optionsakademi.length == 0}
                            />
                            <span style={{ color: "red" }}>
                              {this.state.errors["id_akademi"]}
                            </span>
                          </div>
                        )}

                        {this.state.level_broadcast > 2 && (
                          <div className="form-group fv-row mb-7">
                            <label className="form-label required">Tema</label>
                            <Select
                              id="id_tema"
                              name="id_tema"
                              placeholder="Silahkan pilih tema"
                              noOptionsMessage={() => "Data tidak tersedia"}
                              className="form-select-sm selectpicker p-0"
                              onChange={(e) => {
                                this.handleChangeTema(e);
                                this.emptyValidation(e.value, "id_tema")
                                  .then((value) => {})
                                  .catch((err) => {});
                              }}
                              options={this.state.optionstema}
                              isDisabled={this.state.optionstema.length == 0}
                            />
                            <span style={{ color: "red" }}>
                              {this.state.errors["id_tema"]}
                            </span>
                          </div>
                        )}

                        {this.state.level_broadcast > 3 && (
                          <div className="form-group fv-row mb-7">
                            <label className="form-label required">
                              Pelatihan
                            </label>
                            <Select
                              id="id_pelatihan"
                              name="id_pelatihan"
                              placeholder="Silahkan pilih pelatihan"
                              noOptionsMessage={() => "Data tidak tersedia"}
                              className="form-select-sm selectpicker p-0"
                              onChange={(e) => {
                                this.handleChangePelatihan(e);
                                this.emptyValidation(e.value, "id_pelatihan")
                                  .then((value) => {})
                                  .catch((err) => {});
                              }}
                              options={this.state.optionspelatihan}
                              isDisabled={
                                this.state.optionspelatihan.length == 0
                              }
                            />
                            <span style={{ color: "red" }}>
                              {this.state.errors["id_pelatihan"]}
                            </span>
                          </div>
                        )}

                        {(this.state.jenis_broadcast == 1 ||
                          (this.state.jenis_broadcast == 2 &&
                            this.state.level_broadcast)) && (
                          <>
                            {this.state.jenis_broadcast == 2 && (
                              <div className="form-group fv-row mb-7">
                                <label className="form-label required">
                                  Status Peserta
                                </label>
                                <Select
                                  id="status_peserta"
                                  name="status_peserta"
                                  placeholder="Silahkan pilih status pseserta"
                                  noOptionsMessage={() => "Data tidak tersedia"}
                                  className="form-select-sm selectpicker p-0"
                                  isMulti={true}
                                  onChange={(e) => {
                                    this.handleChangeStatusPeserta(e);
                                    this.emptyValidation(
                                      e[0].value,
                                      "status_peserta",
                                    )
                                      .then((value) => {})
                                      .catch((err) => {});
                                  }}
                                  options={this.state.optionsstatuspeserta}
                                  isDisabled={
                                    this.state.optionsstatuspeserta.length == 0
                                  }
                                />
                                <span style={{ color: "red" }}>
                                  {this.state.errors["status_peserta"]}
                                </span>
                              </div>
                            )}

                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Judul
                              </label>
                              <input
                                className="form-control form-control-sm"
                                placeholder="Masukkan Judul Disini"
                                name="judul"
                                value={this.state.judul}
                                onChange={this.handleChangeJudul}
                                onBlur={(e) => {
                                  this.emptyValidation(e.target.value, "judul")
                                    .then((val) => {
                                      this.lengthValidation(val, "judul");
                                    })
                                    .catch((err) => {});
                                }}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["judul"]}
                              </span>
                            </div>

                            <div className="form-group fv-row mb-7">
                              <label className="form-label required">
                                Jenis Konten
                              </label>
                              <Select
                                name="jenis_konten"
                                placeholder="Silahkan pilih"
                                noOptionsMessage={() => "Data tidak tersedia"}
                                className="form-select-sm selectpicker p-0"
                                options={this.optioncontents}
                                value={this.state.valContent}
                                onChange={(e) => {
                                  this.handleChangeContent(e);
                                  this.emptyValidation(e.value, "jenis_konten");
                                }}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["jenis_konten"]}
                              </span>
                            </div>

                            {this.state.valContent?.value == "Rich Content" && (
                              <div className="form-group fv-row mb-7">
                                <label className="form-label required">
                                  Isi Broadcast
                                </label>
                                <CKEditor
                                  editor={Editor}
                                  name="isi"
                                  onReady={(editor) => {
                                    // You can store the "editor" and use when it is needed.
                                  }}
                                  config={{
                                    ckfinder: {
                                      // Upload the images to the server using the CKFinder QuickUpload command.
                                      uploadUrl:
                                        process.env.REACT_APP_BASE_API_URI +
                                        "/publikasi/ckeditor-upload-image",
                                    },
                                  }}
                                  onChange={(event, editor) => {
                                    const data = editor.getData();
                                    this.handleChangeIsi(data);
                                  }}
                                  onBlur={(event, editor) => {
                                    const data = editor.getData();
                                    this.emptyValidation(data, "isi")
                                      .then((val) => {})
                                      .catch((err) => {});
                                  }}
                                  onFocus={(event, editor) => {}}
                                />
                                <span style={{ color: "red" }}>
                                  {this.state.errors["isi"]}
                                </span>
                              </div>
                            )}

                            {this.state.valContent?.value == "Image" && (
                              <div className="form-group fv-row mb-7">
                                <label className="form-label required">
                                  Upload Thumbnail
                                </label>
                                <input
                                  type="file"
                                  className="form-control form-control-sm font-size-h4"
                                  name="thumbnail"
                                  id="thumbnail"
                                  accept=".png,.jpg,.jpeg,.svg"
                                  onChange={(e) => {
                                    this.handleChangeThumbnail(e);
                                    if (
                                      this.state.valContent.value == "Image"
                                    ) {
                                      this.emptyValidation(
                                        e.target.files[0],
                                        "thumbnail",
                                      )
                                        .then((val) => {})
                                        .catch((err) => {});
                                    }
                                  }}
                                />
                                <small className="text-muted">
                                  Resolusi yang direkomendasikan adalah 837 *
                                  640. Fokus visual pada bagian tengah gambar.
                                </small>
                                <div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["thumbnail"]}
                                  </span>
                                </div>
                              </div>
                            )}

                            {this.state.valContent?.value == "Video" && (
                              <div className="col-lg-12 mb-7 fv-row">
                                <label className="form-label required">
                                  Link Video Broadcast
                                </label>
                                <input
                                  className="form-control form-control-sm form-input"
                                  placeholder="http://www.example.com"
                                  name="link_video"
                                  onBlur={(e) => {
                                    if (
                                      this.state.valContent.value == "Video"
                                    ) {
                                      this.emptyValidation(
                                        e.target.value,
                                        "link_video",
                                      )
                                        .then((value) => {
                                          this.validURL(value, "link_video");
                                        })
                                        .catch();
                                    }
                                  }}
                                />
                                <span style={{ color: "red" }}>
                                  {this.state.errors["link_video"]}
                                </span>
                              </div>
                            )}

                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Tanggal Mulai Tayang
                              </label>
                              <Flatpickr
                                options={{
                                  locale: Indonesian,
                                  dateFormat: "d F Y",
                                  enableTime: "false",
                                  minDate: "today",
                                }}
                                className="form-control form-control-sm"
                                placeholder="Masukkan tanggal mulai broadcast akan ditayangkan"
                                name="tanggal_publish_mulai"
                                onChange={(value) => {
                                  this.setState({
                                    tanggal_publish_mulai: value[0],
                                  });
                                  this.emptyValidation(
                                    value[0],
                                    "tanggal_publish_mulai",
                                  )
                                    .then((value) => {})
                                    .catch((err) => {});
                                }}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["tanggal_publish_mulai"]}
                              </span>
                            </div>

                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Tanggal Berhenti Tayang
                              </label>
                              <Flatpickr
                                options={{
                                  locale: Indonesian,
                                  dateFormat: "d F Y",
                                  enableTime: "false",
                                  minDate:
                                    this.state.tanggal_publish_mulai ?? "",
                                }}
                                className="form-control form-control-sm"
                                placeholder="Masukkan tanggal broadcast berhenti ditayangkan"
                                name="tanggal_publish_selesai"
                                onChange={(value) => {
                                  this.setState({
                                    tanggal_publish_selesai: value[0],
                                  });
                                  this.emptyValidation(
                                    value[0],
                                    "tanggal_publish_selesai",
                                  )
                                    .then((value) => {})
                                    .catch((err) => {});
                                }}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["tanggal_publish_selesai"]}
                              </span>
                            </div>

                            <div className="form-group fv-row mb-7">
                              <label className="form-label required">
                                Status
                              </label>
                              <Select
                                name="status"
                                placeholder="Silahkan pilih"
                                noOptionsMessage={({ inputValue }) =>
                                  !inputValue
                                    ? this.state.datax
                                    : "Data tidak tersedia"
                                }
                                className="form-select-sm selectpicker p-0"
                                options={this.optionstatus}
                                value={this.state.valStatus}
                                onChange={(e) => {
                                  this.handleChangeStatus(e);
                                  this.emptyValidation(e.value, "status")
                                    .then((value) => {})
                                    .catch((err) => {});
                                }}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["status"]}
                              </span>
                            </div>

                            <div className="form-group fv-row pt-7 mb-7">
                              <div className="d-flex justify-content-center mb-7">
                                <button
                                  onClick={this.handleClickBatal}
                                  type="reset"
                                  className="btn btn-md btn-light me-3"
                                  data-kt-menu-dismiss="true"
                                >
                                  Batal
                                </button>
                                <button
                                  disabled={this.state.isLoading}
                                  type="submit"
                                  className="btn btn-primary btn-md"
                                  id="submitQuestion1"
                                >
                                  {this.state.isLoading ? (
                                    <>
                                      <span
                                        className="spinner-border spinner-border-sm me-2"
                                        role="status"
                                        aria-hidden="true"
                                      ></span>
                                      <span className="sr-only">
                                        Loading...
                                      </span>
                                      Loading...
                                    </>
                                  ) : (
                                    <>
                                      <i className="fa fa-paper-plane me-1"></i>
                                      Simpan
                                    </>
                                  )}
                                </button>
                              </div>
                            </div>
                          </>
                        )}
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
