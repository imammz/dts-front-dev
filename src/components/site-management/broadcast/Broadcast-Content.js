import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import Select from "react-select";
import DataTable from "react-data-table-component";
import { capitalizeFirstLetter } from "../../publikasi/helper";
import "../../publikasi/image_pratinjau.css";
import { dateRange } from "../../pelatihan/Pelatihan/helper";

export default class BroadcastContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickDelete = this.handleClickDeleteAction.bind(this);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleClickFilterPublish =
      this.handleClickFilterPublishAction.bind(this);
    this.handleClickFilterUnpublish =
      this.handleClickFilterUnpublishAction.bind(this);
    this.handleClickFilter = this.handleClickFilterAction.bind(this);
    this.handleClickPratinjau = this.handleClickPratinjauAction.bind(this);
    this.handleClickReset = this.handleClickResetAction.bind(this);
    this.handleChangeJenisFilter =
      this.handleChangeJenisFilterAction.bind(this);
    this.handleChangeLevelFilter =
      this.handleChangeLevelFilterAction.bind(this);
    this.handleChangeStatusFilter =
      this.handleChangeStatusFilterAction.bind(this);
    this.handleChangeSearch = this.handleChangeSearchAction.bind(this);
    this.handleSort = this.handleSortAction.bind(this);
  }
  state = {
    datax: [],
    loading: true,
    totalRows: 0,
    newPerPage: 10,
    totalBroadcast: 0,
    totalAuthor: 0,
    totalPublish: 0,
    totalUnpublish: 0,
    totalViews: 0,
    tempLastNumber: 0,
    currentPage: 0,
    isSearch: false,
    status: "",
    author: "",
    kategori: "",
    searchText: "",
    pratinjau: [],
    column: "id",
    dataxkategori: [],
    valJenisFilter: [],
    valLevelFilter: [],
    valStatusFilter: [],
    isFilter: false,
    filterValue: {},
    sort_by: "id",
    sort_val: "DESC",
    from_pagination_change: 0,
  };
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  optionstatus = [
    { value: "1", label: "Publish" },
    { value: "0", label: "Unpublish" },
  ];

  optionjenis = [
    { value: 1, label: "Public" },
    { value: 2, label: "Peserta" },
  ];

  optionlevel = [
    { value: 1, label: "Semua" },
    { value: 2, label: "Akademi" },
    { value: 3, label: "Tema" },
    { value: 4, label: "Pelatihan" },
  ];
  columns = [
    {
      name: "No",
      center: true,
      cell: (row, index) => this.state.tempLastNumber + index + 1,
      width: "70px",
    },
    {
      name: "Judul Broadcast",
      sortable: true,
      className: "min-w-300px mw-300px",
      width: "320px",
      grow: 6,
      selector: (row) => capitalizeFirstLetter(row.judul),
      cell: (row) => {
        return (
          <div>
            <label className="d-flex flex-stack cursor-pointer">
              <a
                href="#"
                data-bs-toggle="modal"
                data-bs-target="#pratinjau"
                onClick={this.handleClickPratinjau}
                judul={row.judul}
                level={row.level_nama}
                jenis={row.jenis_nama}
                url={row.url}
                gambar={
                  process.env.REACT_APP_BASE_API_URI +
                  "/publikasi/get-file?path=" +
                  row.gambar
                }
                isi={row.isi}
                created_by={row.created_by}
                created_at={row.created_at}
                jenis_content={row.jenis_content}
                tanggal_publish_mulai={row.tanggal_publish_mulai}
                tanggal_publish_selesai={row.tanggal_publish_selesai}
                status_peserta={JSON.stringify(row.status_peserta)}
                id={row.id}
                title="Pratinjau"
                className="text-dark"
              >
                <span className="d-flex align-items-center">
                  <span className="symbol symbol-75px">
                    <span
                      className="symbol-label"
                      style={{ backgroundColor: "transparent" }}
                    >
                      <span className="svg-icon">
                        <img
                          src={
                            process.env.REACT_APP_BASE_API_URI +
                            "/publikasi/get-file?path=" +
                            row.gambar
                          }
                          alt=""
                          className="w-75 rounded my-0"
                        />
                      </span>
                    </span>
                  </span>
                  <div className="row">
                    <div className="col-12">
                      <span
                        className="fw-bolder fs-7"
                        style={{
                          overflow: "hidden",
                          whiteSpace: "wrap",
                          textOverflow: "ellipses",
                        }}
                      >
                        {capitalizeFirstLetter(row.judul)}
                      </span>
                    </div>
                    <div className="col-12">
                      <span className="badge badge-light-primary">
                        {capitalizeFirstLetter(row.jenis_nama)}
                      </span>
                    </div>
                  </div>
                </span>
              </a>
            </label>
          </div>
        );
      },
    },
    {
      name: "Level",
      sortable: true,
      className: "min-w-100px",
      grow: 2,
      selector: (row) => capitalizeFirstLetter(row.kategori),
      cell: (row) => {
        return (
          <span className="d-flex flex-column">
            <span
              className="fs-7"
              style={{
                overflow: "hidden",
                whiteSpace: "wrap",
                textOverflow: "ellipses",
              }}
            >
              {capitalizeFirstLetter(
                row.level_nama != "-" ? row.level_nama : "Semua",
              )}
            </span>
          </span>
        );
      },
    },
    {
      name: "Target Peserta",
      center: true,
      sortable: false,
      width: "200px",
      className: "min-w-150px",
      grow: 2,
      cell: (row) => {
        if (row.jenis > 1) {
          return (
            <div>
              {row.status_peserta.map((elem) => (
                <span className="badge badge-light-info fs-7 m-1">
                  {capitalizeFirstLetter(elem.status)}
                </span>
              ))}
            </div>
          );
        } else
          return (
            <span className="badge badge-light-success fs-7 m-1">
              Seluruh populasi
            </span>
          );
      },
    },
    {
      name: "Tgl. Tayang",
      sortable: true,
      className: "min-w-150px",
      grow: 2,
      selector: (row) =>
        dateRange(
          row.tanggal_publish_mulai,
          row.tanggal_publish_selesai,
          "YYYY-MM-DD",
        ),
      cell: (row) => {
        return (
          <span className="d-flex flex-column">
            <span
              className="fs-7"
              style={{
                overflow: "hidden",
                whiteSpace: "wrap",
                textOverflow: "ellipses",
              }}
            >
              {dateRange(
                row.tanggal_publish_mulai,
                row.tanggal_publish_selesai,
                "YYYY-MM-DD",
              )}
            </span>
          </span>
        );
      },
    },
    {
      name: "Created By",
      sortable: true,
      className: "min-w-100px",
      grow: 2,
      selector: (row) => capitalizeFirstLetter(row.created_by_nama),
      cell: (row) => {
        return (
          <span className="d-flex flex-column">
            <span
              className="fs-7"
              style={{
                overflow: "hidden",
                whiteSpace: "wrap",
                textOverflow: "ellipses",
              }}
            >
              {capitalizeFirstLetter(row.created_by_nama)}
            </span>
          </span>
        );
      },
    },
    {
      name: "Status",
      center: true,
      width: "100px",
      sortable: true,
      className: "min-w-100px",
      cell: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.publish == 1 ? "success" : "danger") +
              " fs-7 m-1"
            }
          >
            {row.publish == 1 ? "Publish" : "Unpublish"}
          </span>
        </div>
      ),
    },
    {
      name: "Aksi",
      center: true,
      width: "150px",
      cell: (row) => (
        <div className="row">
          <a
            href="#"
            data-bs-toggle="modal"
            data-bs-target="#pratinjau"
            onClick={this.handleClickPratinjau}
            judul={row.judul}
            level={row.level_nama}
            jenis={row.jenis_nama}
            gambar={
              process.env.REACT_APP_BASE_API_URI +
              "/publikasi/get-file?path=" +
              row.gambar
            }
            isi={row.isi}
            created_by={row.created_by}
            created_at={row.created_at}
            jenis_content={row.jenis_content}
            tanggal_publish_mulai={row.tanggal_publish_mulai}
            tanggal_publish_selesai={row.tanggal_publish_selesai}
            status_peserta={JSON.stringify(row.status_peserta)}
            url={row.url}
            id={row.id}
            title="Lihat"
            className="btn btn-icon btn-bg-primary btn-sm me-1"
            alt="Lihat"
          >
            <i className="bi bi-eye-fill text-white"></i>
          </a>
          <a
            href={"/site-management/broadcast/edit/" + row.id}
            id={row.id}
            title="Edit"
            className="btn btn-icon btn-bg-warning btn-sm me-1"
            alt="Edit"
          >
            <i className="bi bi-gear-fill text-white"></i>
          </a>

          <a
            href="#"
            id={row.id}
            onClick={this.handleClickDelete}
            title="Hapus"
            className="btn btn-icon btn-bg-danger btn-sm me-1"
          >
            <i className="bi bi-trash-fill text-white"></i>
          </a>
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };
  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    this.handleReload();
  }

  handleClickFilterPublishAction(e) {
    this.setState({ status: 1 }, function () {
      this.handleReload(1, this.state.newPerPage);
    });
  }

  handleClickFilterUnpublishAction(e) {
    this.setState({ status: 0 }, function () {
      this.handleReload(1, this.state.newPerPage);
    });
  }

  handleClickFilterAction(e) {
    const dataForm = new FormData(e.currentTarget);
    e.preventDefault();
    this.setState({ loading: true });
    const author = dataForm.get("author");
    const kategori = dataForm.get("kategori");
    const status = dataForm.get("status");

    this.setState(
      {
        author,
        kategori,
        status,
      },
      () => {
        this.handleReload(1, this.state.newPerPage);
      },
    );
  }

  handleClickPratinjauAction(e) {
    const level = e.currentTarget.getAttribute("level");
    const judul = e.currentTarget.getAttribute("judul");
    const gambar = e.currentTarget.getAttribute("gambar");
    const isi = e.currentTarget.getAttribute("isi");
    const jenis = e.currentTarget.getAttribute("jenis");
    const created_by = e.currentTarget.getAttribute("created_by");
    const created_at = e.currentTarget.getAttribute("created_at");
    const url = e.currentTarget.getAttribute("url");
    const tanggal_publish_mulai = e.currentTarget.getAttribute(
      "tanggal_publish_mulai",
    );
    const tanggal_publish_selesai = e.currentTarget.getAttribute(
      "tanggal_publish_selesai",
    );
    const jenis_content = e.currentTarget.getAttribute("jenis_content");
    const status_peserta = e.currentTarget.getAttribute("status_peserta");
    const id = e.currentTarget.getAttribute("id");

    this.setState(
      {
        pratinjau: {
          jenis_content: jenis_content,
          id: id,
          kategori: level,
          judul: judul,
          gambar: gambar,
          isi: isi,
          url: url,
          jenis: jenis,
          level: level,
          created_by: created_by,
          created_at: created_at,
          tanggal_publish_mulai: tanggal_publish_mulai,
          tanggal_publish_selesai: tanggal_publish_selesai,
          status_peserta: status_peserta,
        },
      },
      () => {},
    );
  }

  handleClickDeleteAction(e) {
    const idx = e.currentTarget.id;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          const data = { id: idx };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/broadcast/delete",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload(
                        this.state.currentPage,
                        this.state.newPerPage,
                      );
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmationBUttonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload();
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload();
                    }
                  });
              }
            });
        }
      });
  }

  handleReload(page, newPerPage) {
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });

    let dataBody = {
      start: start_tmp,
      length: length_tmp,
      jenis: this.state.valJenisFilter?.value
        ? this.state.valJenisFilter.value
        : "",
      level: this.state.valLevelFilter?.value
        ? this.state.valLevelFilter.value
        : "",
      publish: this.state.valStatusFilter?.value
        ? this.state.valStatusFilter.value
        : "",
      search: this.state.searchText,
      sort_by: this.state.sort_by,
      sort_val: this.state.sort_val.toUpperCase(),
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/broadcast/filter",
        dataBody,
        this.configs,
      )
      .then((res) => {
        const datax = res.data.result.Data;
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statusx) {
          this.setState({ datax });
          this.setState({ loading: false });
          this.setState({ totalRows: res.data.result.TotalLength });
          this.setState({ totalBroadcast: res.data.result.Total });
          this.setState({ totalPublish: res.data.result.Rekap[0].publish });
          this.setState({ totalUnpublish: res.data.result.Rekap[0].unpublish });
          this.setState({ totalViews: res.data.result.TotalViews });
          this.setState({ currentPage: page });
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      })
      .catch((error) => {
        let statux = error.response.data.result?.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      });
  }

  handlePageChange = (page) => {
    if (this.state.from_pagination_change == 0) {
      this.setState({ loading: true });
      this.handleReload(page, this.state.newPerPage);
    }
  };
  handlePerRowsChange = async (newPerPage, page) => {
    if (this.state.from_pagination_change == 1) {
      this.setState({ loading: true });
      this.setState({ newPerPage: newPerPage }, () => {
        this.handleReload(page, this.state.newPerPage);
      });
    }
  };
  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    if (e.key == "Enter") {
      if (searchText == "") {
        this.setState(
          {
            isSearch: false,
            searchText: "",
          },
          () => {
            this.handleReload();
          },
        );
      } else {
        this.setState({ loading: true });
        this.setState({ isSearch: true });
        this.setState({ searchText: searchText }, () => {
          this.handleReload();
        });
      }
    }
  }

  handleClickResetAction() {
    this.setState(
      {
        valJenisFilter: [],
        valLevelFilter: [],
        valStatusFilter: [],
        isFilter: false,
        status: "",
        author: "",
        kategori: "",
      },
      () => {
        this.handleReload(1, this.state.newPerPage);
      },
    );
  }

  handleChangeJenisFilterAction = (selectedOption) => {
    this.setState({
      valJenisFilter: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  };

  handleChangeLevelFilterAction = (selectedOption) => {
    this.setState({
      valLevelFilter: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  };

  handleChangeStatusFilterAction = (selectedOption) => {
    this.setState({
      valStatusFilter: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  };

  handleChangeSearchAction(e) {
    const searchText = e.currentTarget.value;
    if (searchText == "") {
      this.setState(
        {
          loading: true,
          searchText: "",
        },
        () => {
          this.handleReload();
        },
      );
    }
  }

  handleSortAction(column, sortDirection) {
    let server_name = "";
    if (column.name == "Judul Broadcast") {
      server_name = "judul";
    } else if (column.name == "Tgl. Tayang") {
      server_name = "tanggal_publish_mulai";
    } else if (column.name == "Created By") {
      server_name = "created_by_nama";
    } else if (column.name == "Status") {
      server_name = "publish";
    } else if (column.name == "Level") {
      server_name = "level";
    }

    this.setState(
      {
        sort_by: server_name,
        sort_val: sortDirection,
      },
      () => {
        this.handleReload(1, this.state.newPerPage);
      },
    );
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="32"
                      height="32"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        opacity="0.3"
                        d="M12.9 10.7L3 5V19L12.9 13.3C13.9 12.7 13.9 11.3 12.9 10.7Z"
                        fill="currentColor"
                      ></path>
                      <path
                        d="M21 10.7L11.1 5V19L21 13.3C22 12.7 22 11.3 21 10.7Z"
                        fill="#f1416c"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Broadcast
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                  data-bs-toggle="modal"
                  data-bs-target="#filter"
                >
                  <i className="bi bi-sliders"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Filter
                  </span>
                </button>

                <a
                  href="/site-management/broadcast/tambah"
                  className="btn btn-success fw-bolder btn-sm"
                >
                  <i className="bi bi-plus-circle"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Tambah Broadcast
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-7"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 col-md-12 col-sm-12">
                    <div className="row">
                      <div className="col-6 col-lg-4 mb-3">
                        <a href="#" className="card hoverable">
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-info mb-3 mt-1">
                                <span className="svg-icon svg-icon-info svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <path
                                      d="M20 14H18V10H20C20.6 10 21 10.4 21 11V13C21 13.6 20.6 14 20 14ZM21 19V17C21 16.4 20.6 16 20 16H18V20H20C20.6 20 21 19.6 21 19ZM21 7V5C21 4.4 20.6 4 20 4H18V8H20C20.6 8 21 7.6 21 7Z"
                                      fill="#7239EA"
                                    ></path>
                                    <path
                                      opacity="0.3"
                                      d="M17 22H3C2.4 22 2 21.6 2 21V3C2 2.4 2.4 2 3 2H17C17.6 2 18 2.4 18 3V21C18 21.6 17.6 22 17 22ZM10 7C8.9 7 8 7.9 8 9C8 10.1 8.9 11 10 11C11.1 11 12 10.1 12 9C12 7.9 11.1 7 10 7ZM13.3 16C14 16 14.5 15.3 14.3 14.7C13.7 13.2 12 12 10.1 12C8.10001 12 6.49999 13.1 5.89999 14.7C5.59999 15.3 6.19999 16 7.39999 16H13.3Z"
                                      fill="#7239EA"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.totalBroadcast}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Total Broadcast
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-6 col-lg-4 mb-3">
                        <a
                          href="#"
                          onClick={this.handleClickFilterPublish}
                          className="card hoverable"
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-success mb-3 mt-1">
                                <span className="svg-icon svg-icon-success svg-icon-3x">
                                  <span className="svg-icon svg-icon-success svg-icon-3x">
                                    <i
                                      className="bi bi-newspaper"
                                      style={{
                                        fontSize: "30px",
                                        color: "#47BE7D",
                                      }}
                                    ></i>
                                  </span>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.totalPublish}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Broadcast Publish
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-6 col-lg-4 col-sm-12 mb-3">
                        <a
                          href="#"
                          onClick={this.handleClickFilterUnpublish}
                          className="card hoverable"
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-danger mb-3 mt-1">
                                <span className="svg-icon svg-icon-danger svg-icon-3x">
                                  <span className="svg-icon svg-icon-success svg-icon-3x">
                                    <i
                                      className="bi bi-newspaper"
                                      style={{
                                        fontSize: "30px",
                                        color: "#F1416C",
                                      }}
                                    ></i>
                                  </span>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.totalUnpublish}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Broadcast Unpublish
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-12 mt-5">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Daftar Broadcast
                        </h1>
                      </div>
                      <div className="card-toolbar">
                        <div className="ml-5 d-flex align-items-center position-relative my-1 me-2">
                          <span className="svg-icon svg-icon-1 position-absolute ms-6">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                              className="mh-50px"
                            >
                              <rect
                                opacity="0.5"
                                x="17.0365"
                                y="15.1223"
                                width="8.15546"
                                height="2"
                                rx="1"
                                transform="rotate(45 17.0365 15.1223)"
                                fill="#a1a5b7"
                              ></rect>
                              <path
                                d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                fill="#a1a5b7"
                              ></path>
                            </svg>
                          </span>
                          <input
                            type="text"
                            data-kt-user-table-filter="search"
                            className="form-control form-control-sm form-control-solid w-250px ps-14"
                            placeholder="Cari Broadcast"
                            onKeyPress={this.handleKeyPress}
                            onChange={this.handleChangeSearch}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="table-responsive">
                        <DataTable
                          columns={this.columns}
                          data={this.state.datax}
                          progressPending={this.state.loading}
                          highlightOnHover
                          pointerOnHover
                          pagination
                          paginationServer
                          paginationTotalRows={this.state.totalBroadcast}
                          paginationComponentOptions={{
                            selectAllRowsItem: true,
                            selectAllRowsItemText: "Semua",
                          }}
                          onChangeRowsPerPage={(
                            currentRowsPerPage,
                            currentPage,
                          ) => {
                            this.setState(
                              {
                                from_pagination_change: 1,
                              },
                              () => {
                                this.handlePerRowsChange(
                                  currentRowsPerPage,
                                  currentPage,
                                );
                              },
                            );
                          }}
                          onChangePage={(page, totalRows) => {
                            this.setState(
                              {
                                from_pagination_change: 0,
                              },
                              () => {
                                this.handlePageChange(page, totalRows);
                              },
                            );
                          }}
                          customStyles={this.customStyles}
                          persistTableHead={true}
                          noDataComponent={
                            <div className="mt-5">Tidak Ada Data</div>
                          }
                          onSort={this.handleSort}
                          sortServer
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="modal fade" tabIndex="-1" id="pratinjau">
          <div className="modal-dialog modal-lg">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Pratinjau</h5>
                <div
                  className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <span className="svg-icon svg-icon-2x">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <rect
                        opacity="0.5"
                        x="6"
                        y="17.3137"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(-45 6 17.3137)"
                        fill="currentColor"
                      />
                      <rect
                        x="7.41422"
                        y="6"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(45 7.41422 6)"
                        fill="currentColor"
                      />
                    </svg>
                  </span>
                </div>
              </div>
              <div className="modal-body" key={this.state.pratinjau.id}>
                <br></br>
                <h2 className="mt-3 mb-3">{this.state.pratinjau.judul}</h2>
                <div className="">
                  <span className="fw-bolder">Jenis Broadcast: </span>{" "}
                  <span className="badge badge-light-primary">
                    {this.state.pratinjau.jenis}
                  </span>
                </div>
                <div className="">
                  <span className="fw-bolder">Level Broadcast:</span>{" "}
                  <span className="badge badge-light-success">
                    {this.state.pratinjau.level == "-"
                      ? "Semua Peserta DTS"
                      : this.state.pratinjau.level}
                  </span>
                </div>
                {this.state.pratinjau.jenis == "Peserta" && (
                  <>
                    <span className="fw-bolder">Target Peserta:</span>{" "}
                    {JSON.parse(this.state.pratinjau.status_peserta).map(
                      (elem) => (
                        <span className="badge badge-light-info">
                          {elem.status}
                        </span>
                      ),
                    )}
                  </>
                )}
                {this.state.pratinjau.jenis_content == "Image" && (
                  <div className="text-center mt-2">
                    <img
                      src={this.state.pratinjau.gambar}
                      width="50%"
                      className="rounded"
                    />
                  </div>
                )}
                {this.state.pratinjau.jenis_content == "Rich Content" && (
                  <div className="rounded p-2 mt-3 isi">
                    <div
                      dangerouslySetInnerHTML={{
                        __html: this.state.pratinjau.isi,
                      }}
                    />
                  </div>
                )}
                {this.state.pratinjau.jenis_content == "Video" && (
                  <div style={{ width: "100%", height: "50vh" }}>
                    <iframe
                      src={this.state.pratinjau.url}
                      frameBorder="0"
                      allow="autoplay; encrypted-media"
                      allowFullScreen
                      width="100%"
                      height="100%"
                      title="video"
                    />
                  </div>
                )}
              </div>
              <div className="modal-footer">
                <a
                  href={
                    "/site-management/broadcast/edit/" + this.state.pratinjau.id
                  }
                  className="btn btn-warning btn-sm me-3"
                >
                  Edit
                </a>
                <button
                  className="btn btn-light btn-sm"
                  data-bs-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="modal fade" tabIndex="-1" id="filter">
          <div className="modal-dialog modal-md">
            <div className="modal-content">
              <div className="modal-header py-3">
                <h5 className="modal-title">
                  <span className="svg-icon svg-icon-5 me-1">
                    <i className="bi bi-sliders text-black"></i>
                  </span>
                  Filter Data Broadcast
                </h5>
                <div
                  className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <span className="svg-icon svg-icon-2x">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <rect
                        opacity="0.5"
                        x="6"
                        y="17.3137"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(-45 6 17.3137)"
                        fill="currentColor"
                      />
                      <rect
                        x="7.41422"
                        y="6"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(45 7.41422 6)"
                        fill="currentColor"
                      />
                    </svg>
                  </span>
                </div>
              </div>
              <form
                className="form"
                action="#"
                onSubmit={this.handleClickFilter}
              >
                <input
                  type="hidden"
                  name="csrf-token"
                  value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                />
                <div className="modal-body">
                  <div className="fv-row form-group mb-7">
                    <label className="form-label">Jenis</label>
                    <Select
                      id="jenis"
                      name="jenis"
                      value={this.state.valJenisFilter}
                      placeholder="Silahkan pilih Jenis Broadcast"
                      noOptionsMessage={() => "Data tidak tersedia"}
                      className="form-select-sm selectpicker p-0"
                      options={this.optionjenis}
                      onChange={this.handleChangeJenisFilter}
                    />
                  </div>
                  <div className="fv-row form-group mb-7">
                    <label className="form-label">Level</label>
                    <Select
                      id="level"
                      name="level"
                      value={this.state.valLevelFilter}
                      placeholder="Silahkan pilih Level Broadcast"
                      noOptionsMessage={() => "Data tidak tersedia"}
                      className="form-select-sm selectpicker p-0"
                      options={this.optionlevel}
                      onChange={this.handleChangeLevelFilter}
                    />
                  </div>
                  <div className="fv-row form-group mb-7">
                    <label className="form-label">Status</label>
                    <Select
                      id="status"
                      name="status"
                      value={this.state.valStatusFilter}
                      placeholder="Silahkan pilih Status Broadcast"
                      noOptionsMessage={() => "Data tidak tersedia"}
                      className="form-select-sm selectpicker p-0"
                      options={this.optionstatus}
                      onChange={this.handleChangeStatusFilter}
                    />
                  </div>
                </div>
                <div className="modal-footer">
                  <div className="d-flex justify-content-between">
                    <button
                      type="reset"
                      className="btn btn-sm btn-light me-3"
                      onClick={this.handleClickReset}
                    >
                      Reset
                    </button>
                    <button
                      type="submit"
                      className="btn btn-sm btn-primary"
                      data-bs-dismiss="modal"
                    >
                      Apply Filter
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
