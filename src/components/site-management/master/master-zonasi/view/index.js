import React from "react";
import Header from "../../../../Header";
import SideNav from "../../../../SideNav";
import Footer from "../../../../Footer";
import Select from "react-select";
import DataTable from "react-data-table-component";
import { deleteZonasi, fetchDetailZonasi, fetchZonasi } from "../actions";
import Swal from "sweetalert2";
import {
  capitalizeFirstLetter,
  capitalizeTheFirstLetterOfEachWord,
} from "../../../../publikasi/helper";
import { withRouter } from "../../../../RouterUtil";

class Content extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentRowsPerPage: 10,
      currentPage: 1,
      sortBy: 0,
      sortDir: "asc",

      loading: false,
      totalData: 0,
      data: [],
      dataAkademi: [],
      dataTema: [],
      dataPelatihan: [],
    };
  }

  onChangeRowsPerPage = (currentRowsPerPage, currentPage) => {
    this.setState({
      currentRowsPerPage,
      currentPage,
    });
  };

  onChangePage = (currentPage, totalRows) => {
    this.setState({
      currentPage: currentPage,
    });
  };

  onSort = ({ id }, direction, rows) => {
    this.setState({
      sortBy: id,
      sortDir: direction,
    });
  };

  fetch = async () => {
    const { id } = this.props.params || {};

    try {
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info",
        allowOutsideClick: false,
        didOpen: () => Swal.showLoading(),
      });

      const [{ provinsis = [], pelatihans = [] }] = await Promise.all([
        fetchDetailZonasi({ id }),
      ]);

      let dataAkademi = {};
      let dataTema = {};
      let dataPelatihan = [];
      pelatihans.forEach((pel) => {
        const { nama_akademi, nama_pelatihan, nama_tema } = pel || {};

        if (!dataAkademi[nama_akademi]) dataAkademi[nama_akademi] = [];
        dataAkademi[nama_akademi] = [...dataAkademi[nama_akademi], pel];

        if (!dataTema[nama_tema]) dataTema[nama_tema] = [];
        dataTema[nama_tema] = [...dataTema[nama_tema], pel];

        dataPelatihan = [
          ...dataPelatihan,
          { label: nama_pelatihan, status_pelatihan: "-", status: 1 },
        ];
      });

      dataAkademi = Object.values(dataAkademi).reduce((arr, darr) => {
        const [d] = darr || [];
        return [
          ...arr,
          { label: d?.nama_akademi, jumlah: (darr || []).length, status: 1 },
        ];
      }, []);

      dataTema = Object.values(dataTema).reduce((arr, darr) => {
        const [d] = darr || [];
        return [
          ...arr,
          { label: d?.nama_tema, jumlah: (darr || []).length, status: 1 },
        ];
      }, []);

      const [d] = provinsis || [];

      this.setState({
        totalData: provinsis.length,
        data: {
          Header: { judul_zonasi: d?.judul_zonasi, status: d?.status },
          Data: provinsis,
        },
        dataAkademi: Object.values(dataAkademi),
        dataTema: Object.values(dataTema),
        dataPelatihan: Object.values(dataPelatihan),
      });

      Swal.close();
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  remove = async (id) => {
    try {
      const { isConfirmed = false } = await Swal.fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      });

      if (isConfirmed) {
        Swal.fire({
          title: "Mohon Tunggu!",
          icon: "info",
          allowOutsideClick: false,
          didOpen: () => Swal.showLoading(),
        });

        const message = await deleteZonasi({ id });

        Swal.close();

        await Swal.fire({
          title: message || "Zonasi berhasil dihapus",
          icon: "success",
          confirmButtonText: "Ok",
        });

        this.back();
      }

      // throw new Error("API delete belum tersedia");
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  back = () => {
    window.location.href = "/site-management/master/zonasi";
  };

  componentDidMount() {
    this.fetch();
  }

  render() {
    const { id } = this.props.params || {};
    const status = (
      this.state.data?.Header?.status || "Tidak Aktif"
    ).toLowerCase();

    const columns = [
      {
        name: "No",
        center: true,
        width: "70px",
        cell: (row, index) => (
          <div className="row">
            <div className="col-md-12">
              <span>
                {(this.state.currentPage - 1) * this.state.currentRowsPerPage +
                  index +
                  1}
              </span>
            </div>
          </div>
        ),
      },
      {
        name: "Nama Provinsi",
        // width: '225px',
        cell: ({ provinsi }) => (
          <div className="row">
            <div className="col-md-12">
              <span>{provinsi}</span>
            </div>
          </div>
        ),
      },
      {
        name: "Kota / Kabupaten",
        // width: '225px',
        cell: ({ kota_kabupaten = [] }) => (
          <div className="row">
            {kota_kabupaten.map(({ label }) => (
              <div className="col-md-12 m-1">
                <span>- {label}</span>
              </div>
            ))}
          </div>
        ),
      },
    ];

    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Master Zonasi
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={() => this.back()}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500">
                    <i className="fa fa-chevron-left pe-1"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>

                <a
                  href={`/site-management/master/zonasi/edit/${id}`}
                  className="btn btn-warning fw-bolder btn-sm me-2"
                >
                  <i className="bi bi-gear-fill"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Edit
                  </span>
                </a>

                <button
                  className={`btn btn-sm btn-danger btn-active-light-info ${
                    (this.state.dataPelatihan || []).length > 0
                      ? "disabled"
                      : ""
                  }`}
                  onClick={() => this.remove(id)}
                >
                  <i className="bi bi-trash-fill text-white pe-1"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Detail Zonasi
                          </h1>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="mb-7">
                          <div className="d-flex flex-wrap py-3">
                            <div className="flex-grow-1 mb-3">
                              <div className="d-flex justify-content-between align-items-start flex-wrap">
                                <div className="d-flex flex-column">
                                  <div className="d-flex align-items-center mb-0">
                                    <h5 className="fw-bolder mb-0 fs-5">
                                      Zonasi{" "}
                                      {capitalizeTheFirstLetterOfEachWord(
                                        this.state.data?.Header?.judul_zonasi,
                                      )}
                                    </h5>
                                  </div>
                                  <div className="d-flex flex-wrap fw-bold fs-6 pe-2">
                                    <span className="text-muted fs-7">
                                      <span
                                        className={
                                          "badge badge-light-" +
                                          (status == "aktif"
                                            ? "success"
                                            : "danger") +
                                          " fs-7 m-1"
                                        }
                                      >
                                        {capitalizeFirstLetter(status)}
                                      </span>
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div className="d-flex border-bottom p-0">
                          <ul className="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap">
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary active"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_1"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline d-none">
                                  Wilayah
                                </span>
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary false"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_2"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline d-none">
                                  Targeting
                                </span>
                              </a>
                            </li>
                          </ul>
                        </div>

                        <div className="tab-content" id="detail-account-tab">
                          {/* tab 1 */}
                          <div
                            className="tab-pane fade show active"
                            id="kt_tab_pane_1"
                            role="tabpanel"
                          >
                            <div className="mt-7">
                              <div className="row pt-7">
                                <div className="table-responsive">
                                  <DataTable
                                    columns={columns}
                                    data={this.state.data?.Data}
                                    // progressPending={this.state.loading}
                                    persistTableHead={true}
                                    noDataComponent={
                                      <div className="mt-5">Tidak Ada Data</div>
                                    }
                                    highlightOnHover
                                    pointerOnHover
                                    pagination
                                    // paginationServer
                                    // sortServer
                                    paginationTotalRows={this.state.totalData}
                                    onChangeRowsPerPage={
                                      this.onChangeRowsPerPage
                                    }
                                    onChangePage={this.onChangePage}
                                    onSort={this.onSort}
                                    customStyles={{
                                      headCells: {
                                        style: {
                                          background: "rgb(243, 246, 249)",
                                        },
                                      },
                                    }}
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div
                            className="tab-pane fade"
                            id="kt_tab_pane_2"
                            role="tabpanel"
                          >
                            <div className="mt-7">
                              <div className="row pt-7">
                                <h2 className="fs-5 text-muted mb-3">
                                  Target Akademi
                                </h2>
                                <div className="col-lg-12">
                                  <div className="table-responsive">
                                    <DataTable
                                      columns={[
                                        {
                                          name: "No",
                                          width: "70px",
                                          center: true,
                                          cell: ({ idx }) => idx + 1,
                                        },
                                        {
                                          name: "Akademi",
                                          selector: ({ label }) => (
                                            <div className="mt-2">{label}</div>
                                          ),
                                        },
                                        {
                                          name: "Pelatihan",
                                          width: "300px",
                                          center: true,
                                          selector: ({ jumlah }) => {
                                            return <span>{jumlah}</span>;
                                          },
                                        },
                                        {
                                          name: "Status",
                                          width: "200px",
                                          center: true,
                                          selector: ({ status }) => (
                                            <span
                                              className={
                                                "badge badge-light-" +
                                                (status == 1
                                                  ? "success"
                                                  : "danger") +
                                                " fs-7 m-1"
                                              }
                                            >
                                              {status == 1
                                                ? "Publish"
                                                : "Unpublish"}
                                            </span>
                                          ),
                                        },
                                      ]}
                                      data={(
                                        this?.state?.dataAkademi || []
                                      ).map((d, idx) => ({ ...d, idx }))}
                                      highlightOnHover
                                      pointerOnHover
                                      pagination
                                      // paginationTotalRows={state.soals.length}
                                      // selectableRows
                                      // selectableRowSelected={({ id = null } = {}) => state.selectedRows.map(({ id } = {}) => id).includes(id)}
                                      // onSelectedRowsChange={({ selectedRows }) => state.setSelectedRows(selectedRows)}
                                      customStyles={{
                                        headCells: {
                                          style: {
                                            background: "rgb(243, 246, 249)",
                                          },
                                        },
                                      }}
                                      noDataComponent={
                                        <div className="mt-5">
                                          Tidak Ada Data
                                        </div>
                                      }
                                      persistTableHead={true}
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="row pt-7">
                                <h2 className="fs-5 text-muted mb-3">
                                  Target Tema
                                </h2>
                                <div className="col-lg-12">
                                  <div className="table-responsive">
                                    <DataTable
                                      columns={[
                                        {
                                          name: "No",
                                          width: "70px",
                                          center: true,
                                          cell: ({ idx }) => idx + 1,
                                        },
                                        {
                                          name: "Tema",
                                          selector: ({ label }) => (
                                            <div className="mt-2">{label}</div>
                                          ),
                                        },
                                        {
                                          name: "Pelatihan",
                                          width: "300px",
                                          center: true,
                                          selector: ({ jumlah }) => {
                                            return <span>{jumlah}</span>;
                                          },
                                        },
                                        {
                                          name: "Status",
                                          width: "200px",
                                          center: true,
                                          selector: ({ status }) => (
                                            <span
                                              className={
                                                "badge badge-light-" +
                                                (status == 1
                                                  ? "success"
                                                  : "danger") +
                                                " fs-7 m-1"
                                              }
                                            >
                                              {status == 1
                                                ? "Publish"
                                                : "Unpublish"}
                                            </span>
                                          ),
                                        },
                                      ]}
                                      data={(this?.state?.dataTema || []).map(
                                        (d, idx) => ({ ...d, idx }),
                                      )}
                                      highlightOnHover
                                      pointerOnHover
                                      pagination
                                      // paginationTotalRows={state.soals.length}
                                      // selectableRows
                                      // selectableRowSelected={({ id = null } = {}) => state.selectedRows.map(({ id } = {}) => id).includes(id)}
                                      // onSelectedRowsChange={({ selectedRows }) => state.setSelectedRows(selectedRows)}
                                      customStyles={{
                                        headCells: {
                                          style: {
                                            background: "rgb(243, 246, 249)",
                                          },
                                        },
                                      }}
                                      noDataComponent={
                                        <div className="mt-5">
                                          Tidak Ada Data
                                        </div>
                                      }
                                      persistTableHead={true}
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="row pt-7">
                                <h2 className="fs-5 text-muted mb-3">
                                  Target Pelatihan
                                </h2>
                                <div className="col-lg-12">
                                  <div className="table-responsive">
                                    <DataTable
                                      columns={[
                                        {
                                          name: "No",
                                          width: "70px",
                                          center: true,
                                          cell: ({ idx }) => idx + 1,
                                        },
                                        {
                                          name: "Pelatihan",
                                          selector: ({ label }) => (
                                            <div className="mt-2">{label}</div>
                                          ),
                                        },
                                        // {
                                        //   name: 'Jadwal Pelatihan',
                                        //   width: "200px",
                                        //   center: true,
                                        //   selector: ({ pelatihan_mulai, pelatihan_selesai }) => {
                                        //     return <span>{pelatihan_mulai} - {pelatihan_selesai}</span>;
                                        //   }
                                        // },
                                        // {
                                        //   name: 'Status Pelatihan',
                                        //   width: "200px",
                                        //   center: true,
                                        //   selector: ({ status_pelatihan }) => {
                                        //     return <span>{status_pelatihan}</span>;
                                        //   }
                                        // },
                                        {
                                          name: "Status",
                                          width: "200px",
                                          center: true,
                                          selector: ({ status }) => (
                                            <span
                                              className={
                                                "badge badge-light-" +
                                                (status == 1
                                                  ? "success"
                                                  : "danger") +
                                                " fs-7 m-1"
                                              }
                                            >
                                              {status == 1
                                                ? "Publish"
                                                : "Unpublish"}
                                            </span>
                                          ),
                                        },
                                      ]}
                                      data={(
                                        this?.state?.dataPelatihan || []
                                      ).map((d, idx) => ({ ...d, idx }))}
                                      highlightOnHover
                                      pointerOnHover
                                      pagination
                                      // paginationTotalRows={state.soals.length}
                                      // selectableRows
                                      // selectableRowSelected={({ id = null } = {}) => state.selectedRows.map(({ id } = {}) => id).includes(id)}
                                      // onSelectedRowsChange={({ selectedRows }) => state.setSelectedRows(selectedRows)}
                                      customStyles={{
                                        headCells: {
                                          style: {
                                            background: "rgb(243, 246, 249)",
                                          },
                                        },
                                      }}
                                      noDataComponent={
                                        <div className="mt-5">
                                          Tidak Ada Data
                                        </div>
                                      }
                                      persistTableHead={true}
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const ViewZonasi = (props) => {
  return (
    <div>
      <SideNav />
      <Header />
      <Content {...props} />
      <Footer />
    </div>
  );
};

export default withRouter(ViewZonasi);
