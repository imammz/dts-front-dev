import React from "react";
import Header from "../../../../Header";
import SideNav from "../../../../SideNav";
import Footer from "../../../../Footer";
import Select from "react-select";
import DataTable from "react-data-table-component";
import {
  deleteZonasi,
  fetchDetailZonasi,
  fetchListKotaKab,
  fetchListProvinsi,
  fetchStatusAktif,
  fetchZonasi,
  resetError,
  updateZonasi,
  validate,
} from "../actions";
import Swal from "sweetalert2";
import { capitalizeFirstLetter } from "../../../../publikasi/helper";
import { withRouter } from "../../../../RouterUtil";
import AddEditForm from "../add/AddEditForm";
import Cookies from "js-cookie";

class Content extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentRowsPerPage: 10,
      currentPage: 1,
      sortBy: 0,
      sortDir: "asc",
      isLoading: false,

      dataStatusAktif: [],
      dataProvinsi: [],
      dataPelatihan: [],
      loadingKotaKabs: {},
      dataKotaKabs: {},

      loading: false,
      name: "",
      status: null,
      totalData: 0,
      detailZonasis: [],
      selectedKotaKabs: {},
    };
  }

  fetch = async () => {
    const { id } = this.props.params || {};

    try {
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info",
        allowOutsideClick: false,
        didOpen: () => Swal.showLoading(),
      });

      const [
        { provinsis = [], pelatihans = [] },
        dataStatusAktif,
        dataProvinsi,
      ] = await Promise.all([
        fetchDetailZonasi({ id }),
        fetchStatusAktif(),
        fetchListProvinsi(),
      ]);

      const [d] = provinsis || [];
      const Header = { judul_zonasi: d?.judul_zonasi, status: d?.status },
        Data = provinsis;
      const [{ value: statusValue }] =
        dataStatusAktif.filter(({ label }) => label == Header?.status) || [];

      this.setState(
        {
          name: Header?.judul_zonasi,
          status: statusValue,
          detailZonasis: Data,
          dataStatusAktif: dataStatusAktif || [],
          dataProvinsi: dataProvinsi || [],
          dataPelatihan: pelatihans,
        },
        async () => {
          let loadingKotaKabs = this.state.detailZonasis.reduce(
            (obj, { provinsi_id }) => ({ ...obj, [provinsi_id]: true }),
            {},
          );
          this.setState({ loadingKotaKabs });

          const batchkotaKabs = await Promise.all(
            this.state.detailZonasis.map(({ provinsi_id }) =>
              fetchListKotaKab({ prov_id: provinsi_id }),
            ),
          );
          const dataKotaKabs = this.state.detailZonasis.reduce(
            (obj, { provinsi_id }, idx) => ({
              ...obj,
              [provinsi_id]: batchkotaKabs[idx],
            }),
            {},
          );
          const selectedKotaKabs = this.state.detailZonasis.reduce(
            (obj, { provinsi_id, kota_kabupaten }) => ({
              ...obj,
              [provinsi_id]: kota_kabupaten,
            }),
            {},
          );
          loadingKotaKabs = this.state.detailZonasis.reduce(
            (obj, { provinsi_id }) => ({ ...obj, [provinsi_id]: false }),
            {},
          );

          this.setState({ loadingKotaKabs, dataKotaKabs, selectedKotaKabs });
        },
      );

      Swal.close();
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  save = async () => {
    const { id } = this.props.params || {};

    try {
      this.setState({ isLoading: true });
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info",
        allowOutsideClick: false,
        didOpen: () => Swal.showLoading(),
      });

      const payload = [
        {
          id_zonasi: id,
          judul_zonasi: this.state.name,
          status: this.state.status,
          id_user: Cookies.get("user_id"),
          detail_json: this.state.detailZonasis.map(
            ({ provinsi, provinsi_id }) => {
              const kotaKabs = this.state.selectedKotaKabs[provinsi_id] || [];
              return {
                nmprop: provinsi,
                kdprop: provinsi_id,
                nmkabs: kotaKabs.map(({ value, label }) => ({ value, label })),
              };
            },
          ),
        },
      ];

      const message = await updateZonasi(payload);

      Swal.close();
      this.setState({ isLoading: false });
      await Swal.fire({
        title: message || "Zonasi berhasil diupdate",
        icon: "success",
        confirmButtonText: "Ok",
      });

      window.location.href = "/site-management/master/zonasi";
    } catch (err) {
      this.setState({ isLoading: false });
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
        didOpen: () => Swal.hideLoading(),
      });
    }
  };

  checkAndSave = () => {
    const errors = validate(this.state);
    if (Object.keys(errors).length == 0) {
      this.save();
    } else {
      this.setState({ ...resetError(), ...errors });

      Swal.fire({
        title: "Masih terdapat isian yang belum lengkap",
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  remove = async (id) => {
    try {
      const { isConfirmed = false } = await Swal.fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      });

      if (isConfirmed) {
        Swal.fire({
          title: "Mohon Tunggu!",
          icon: "info",
          allowOutsideClick: false,
          didOpen: () => Swal.showLoading(),
        });

        const message = await deleteZonasi({ id });

        Swal.close();

        await Swal.fire({
          title: message || "Zonasi berhasil dihapus",
          icon: "success",
          confirmButtonText: "Ok",
        });

        this.back();
      }

      // throw new Error("API delete belum tersedia");
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  back = () => {
    window.location.href = "/site-management/master/zonasi";
  };

  componentDidMount() {
    this.fetch();
  }

  render() {
    const { id } = this.props.params || {};

    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Master Zonasi
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={() => this.back()}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>

                <a
                  href="#"
                  title="Hapus"
                  className={`btn btn-sm btn-danger btn-active-light-info ${
                    (this.state.dataPelatihan || []).length ? "disabled" : ""
                  }`}
                  onClick={(e) => {
                    e.preventDefault();
                    this.remove(id);
                  }}
                >
                  <i className="bi bi-trash-fill text-white"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </a>
              </div>
              {/* <a href="https://back.dev.sdmdigital.id/api/report-pelatihan" className="btn btn-primary btn-sm me-2">
                <i className="las la-file-export"></i>
                Export
              </a>
              <a href="/pelatihan/tambah-pelatihan" className="btn btn-primary btn-sm">
                <i className="las la-plus"></i>
                Tambah Pelatihan
              </a> */}
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Edit Zonasi
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="row">
                        <AddEditForm
                          {...this.props}
                          {...this.state}
                          onChange={(k, v, cb) => this.setState({ [k]: v }, cb)}
                          onSave={() => this.checkAndSave()}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const ViewZonasi = (props) => {
  return (
    <div>
      <SideNav />
      <Header />
      <Content {...props} />
      <Footer />
    </div>
  );
};

export default withRouter(ViewZonasi);
