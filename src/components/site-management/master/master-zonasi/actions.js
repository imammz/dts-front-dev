import axios from "axios";
import Cookies from "js-cookie";
import Swal from "sweetalert2";

const configs = {
  headers: {
    Authorization: "Bearer " + Cookies.get("token"),
  },
};

export const resetError = () => {
  return {
    nameError: null,
    statusError: null,
    detailZonasisError: null,
  };
};

export const validate = ({
  name,
  status,
  detailZonasis = [],
  selectedKotaKabs = {},
}) => {
  let error = {};

  if (!name) error["nameError"] = "Nama zonasi tidak boleh kosong";
  if (status == null) error["statusError"] = "Status zonasi tidak boleh kosong";
  // if (selectedProvinsis.length == 0) error['selectedProvinsisError'] = 'Provinsi satuan kerja tidak boleh kosong';

  detailZonasis.forEach(({ provinsi_id }, idx) => {
    if (!provinsi_id) {
      if (!error["detailZonasisError"]) error["detailZonasisError"] = {};
      if (!error["detailZonasisError"][idx])
        error["detailZonasisError"][idx] = {};
      error["detailZonasisError"][idx]["provinsiError"] =
        "Provinsi tidak boleh kosong";
    }

    if ((selectedKotaKabs[provinsi_id] || []).length == 0) {
      if (!error["detailZonasisError"]) error["detailZonasisError"] = {};
      if (!error["detailZonasisError"][idx])
        error["detailZonasisError"][idx] = {};
      error["detailZonasisError"][idx]["kotaKabsError"] =
        "Kota/Kabupaten tidak boleh kosong";
    }
  });

  return error;
};

const checkToken = () => {
  return new Promise(async (resolve, reject) => {
    if (Cookies.get("token") == null) {
      const { isConfirmed } = await Swal.fire({
        title: "Unauthenticated.",
        text: "Silahkan login ulang",
        icon: "warning",
        confirmButtonText: "Ok",
      });

      if (isConfirmed) return reject();
    }

    resolve();
  });
};

export const fetchZonasi = async ({
  start,
  length,
  search = "",
  orderBy = "name",
  orderDir = "desc",
}) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/masterdata/API_List_Master_Zonasi",
            {
              mulai: start,
              limit: length,
              cari: search,
              sort: `${orderBy} ${orderDir}`,
            },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data || {};
            let {
              TotalLength = [],
              Data = [],
              Status = false,
              Message = "",
            } = result || {};
            [TotalLength] = TotalLength;
            const { jml_data = 0 } = TotalLength || {};

            if (Status) resolve({ Total: jml_data, Data });
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err?.response || {};
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchDetailZonasi = async ({ id }) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/API_Detail_Master_Zonasi",
            { id },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let {
              DataZonasi = [],
              DataPelatihan = [],
              Data = [],
              Status = false,
              Message = "",
            } = result || {};

            let Header = {};
            DataZonasi = DataZonasi.map((zonasi) => {
              if (zonasi) {
                Header = {
                  judul_zonasi: zonasi?.judul_zonasi,
                  status: zonasi?.status,
                };
                let kota_kabupaten = JSON.parse(zonasi["kota_kabupaten"]);
                let [fkota_kabupaten] = kota_kabupaten || [];
                if (Array.isArray(fkota_kabupaten))
                  [kota_kabupaten] = kota_kabupaten;
                zonasi["kota_kabupaten"] = kota_kabupaten;
              }
              return zonasi;
            });

            if (Status)
              resolve({
                Header,
                provinsis: DataZonasi,
                pelatihans: DataPelatihan,
              });
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchStatusAktif = async () => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        resolve([
          { value: 1, label: "Aktif" },
          { value: 0, label: "Tidak Aktif" },
        ]);
      })
      .catch(reject);
  });
};

export const fetchListProvinsi = async () => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(process.env.REACT_APP_BASE_API_URI + "/provinsi", {}, configs)
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            Data = Data.map(({ id, name, meta }) => ({
              value: id,
              label: name,
            }));

            if (Status) resolve(Data);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchListKotaKab = async ({ prov_id }) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/kabupaten",
            { kdprop: prov_id },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            Data = Data.map(({ id, name, meta }) => ({
              value: id,
              label: name,
            }));

            if (Status) resolve(Data);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const insertZonasi = async (payload) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/API_Tambah_Master_Zonasi",
            payload,
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Message);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const updateZonasi = async (payload) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/update_zonasi",
            payload,
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Message);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const deleteZonasi = async (payload) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/delete_zonasi",
            payload,
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Message);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};
