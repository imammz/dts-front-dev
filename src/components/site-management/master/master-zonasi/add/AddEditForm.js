import React from "react";
import Select from "react-select";
import Swal from "sweetalert2";
import { fetchListKotaKab } from "../actions";

const getOrDefault = (objOrArr, keyOrIdx, def = null) => {
  if (!objOrArr) return def;
  if (Array.isArray(objOrArr) || typeof objOrArr == "object")
    return objOrArr[keyOrIdx] || def;
  return def;
};

class AddEditForm extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    console.log(this.props.isLoading);
    let {
      name = "",
      nameError = null,
      status,
      statusError = null,
      dataStatusAktif = [],
      dataProvinsi = [],
      loadingKotaKabs = {},
      dataKotaKabs = {},
      selectedKotaKabs = {},
      detailZonasis = [],
      detailZonasisError = {},
      onChange = (k, v) => {},
      onSave = () => {},
    } = this.props || {};

    const [oStatus] = dataStatusAktif.filter((s) => s.value == status);

    return (
      <>
        <div className="col-lg-12 mb-7 fv-row">
          <label className="form-label required">Nama Zonasi</label>
          <input
            className="form-control form-control-sm"
            placeholder="Masukkan Nama Zonasi"
            value={name}
            onChange={(e) => {
              onChange("name", e.target.value);
              onChange("nameError", "");
            }}
          />
          <span style={{ color: "red" }}>{nameError}</span>
        </div>
        <div className="col-lg-12 mb-7 fv-row">
          <label className="form-label required">Status</label>
          <Select
            placeholder="Silahkan Pilih Status"
            // noOptionsMessage={({ inputValue }) => !inputValue ? this.state.dataxakademi : "Data tidak tersedia"}
            value={oStatus}
            className="form-select-sm selectpicker p-0"
            onChange={({ value }) => {
              onChange("status", value);
              onChange("statusError", "");
            }}
            options={dataStatusAktif}
            // isOptionDisabled={(option) => option.disabled}
          />
          <span style={{ color: "red" }}>{statusError}</span>
        </div>
        <div className="col-lg-12">
          {detailZonasis.map(({ provinsi_id, provinsi }, idx) => {
            let _dataKotaKabs = [
              { value: "*", label: "Pilih Semua" },
              ...(dataKotaKabs[provinsi_id] || []),
            ];

            return (
              <div className="row">
                <div className="col-lg-5 mb-7 fv-row">
                  <label className="form-label required">Provinsi</label>
                  <Select
                    placeholder="Silahkan Pilih Provinsi"
                    // noOptionsMessage={({ inputValue }) => !inputValue ? this.state.dataxakademi : "Data tidak tersedia"}
                    value={
                      provinsi_id
                        ? { value: provinsi_id, label: provinsi }
                        : null
                    }
                    className="form-select-sm selectpicker p-0"
                    onChange={({ value, label }) => {
                      detailZonasis[idx]["provinsi_id"] = value;
                      detailZonasis[idx]["provinsi"] = label;

                      detailZonasisError[idx] = detailZonasisError[idx] || [];
                      detailZonasisError[idx]["provinsiError"] = null;
                      onChange("detailZonasisError", detailZonasisError);
                      onChange(
                        "detailZonasis",
                        [...detailZonasis],
                        async () => {
                          onChange("loadingKotaKabs", {
                            ...loadingKotaKabs,
                            [value]: true,
                          });
                          onChange("dataKotaKabs", {
                            ...dataKotaKabs,
                            [value]: [],
                          });
                          onChange("selectedKotaKabs", {
                            ...selectedKotaKabs,
                            [value]: [],
                          });

                          let kotaKabs = await fetchListKotaKab({
                            prov_id: value,
                          });

                          onChange("loadingKotaKabs", {
                            ...loadingKotaKabs,
                            [value]: false,
                          });
                          onChange("dataKotaKabs", {
                            ...dataKotaKabs,
                            [value]: kotaKabs,
                          });
                        },
                      );
                    }}
                    options={dataProvinsi}
                    // isOptionDisabled={(option) => option.disabled}
                  />
                  <span style={{ color: "red" }}>
                    {getOrDefault(
                      getOrDefault(detailZonasisError, idx, {}),
                      "provinsiError",
                      null,
                    )}
                  </span>
                </div>
                <div className="col-lg-6 mb-7 fv-row">
                  <label className="form-label required">
                    Kota / Kabupaten
                  </label>
                  <Select
                    isMulti
                    placeholder="Silahkan Pilih Kota / Kabupaten"
                    // noOptionsMessage={({ inputValue }) => !inputValue ? this.state.dataxakademi : "Data tidak tersedia"}
                    value={selectedKotaKabs[provinsi_id]}
                    className="form-select-sm selectpicker p-0"
                    onChange={(values) => {
                      detailZonasisError[idx] = detailZonasisError[idx] || [];
                      detailZonasisError[idx]["kotaKabsError"] = null;
                      onChange("detailZonasisError", detailZonasisError);

                      const isAllSelected = values
                        .map(({ value }) => value)
                        .includes("*");
                      if (isAllSelected) {
                        onChange("selectedKotaKabs", {
                          ...selectedKotaKabs,
                          [provinsi_id]: dataKotaKabs[provinsi_id] || [],
                        });
                      } else {
                        onChange("selectedKotaKabs", {
                          ...selectedKotaKabs,
                          [provinsi_id]: values,
                        });
                      }
                    }}
                    options={_dataKotaKabs}
                    isDisabled={loadingKotaKabs[provinsi_id] || false}
                  />
                  <span style={{ color: "red" }}>
                    {getOrDefault(
                      getOrDefault(detailZonasisError, idx, {}),
                      "kotaKabsError",
                      null,
                    )}
                  </span>
                </div>
                <div className="col-lg-1 mb-7 pt-8 fv-row">
                  <a
                    href="#"
                    title="Hapus"
                    className={`btn btn-icon btn-light-danger w-30px h-30px me-3 ${
                      detailZonasis.length <= 1 ? "disabled" : ""
                    }`}
                    onClick={() => {
                      detailZonasis.splice(idx, 1);
                      onChange("detailZonasis", [...detailZonasis]);
                    }}
                  >
                    <span className="svg-icon svg-icon-3">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width={24}
                        height={24}
                        viewBox="0 0 24 24"
                        fill="none"
                      >
                        <path
                          d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z"
                          fill="black"
                        />
                        <path
                          opacity="0.5"
                          d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z"
                          fill="black"
                        />
                        <path
                          opacity="0.5"
                          d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z"
                          fill="black"
                        />
                      </svg>
                    </span>
                  </a>
                </div>
              </div>
            );
          })}
        </div>
        <div>
          <div className="row my-7">
            <div className="col-9">
              <a
                className="btn btn-sm btn-light text-success d-block btn-block"
                onClick={() =>
                  onChange("detailZonasis", [...detailZonasis, {}])
                }
              >
                <i className="bi bi-plus-circle text-success me-1"></i>Tambah
                Provinsi
              </a>
            </div>
            <div className="col-3">
              <a
                className="btn btn-sm btn-info d-block btn-block"
                onClick={() => {
                  dataProvinsi.forEach(({ value, label }, idx) => {
                    detailZonasis[idx] = detailZonasis[idx] || {};
                    detailZonasis[idx]["provinsi_id"] = value;
                    detailZonasis[idx]["provinsi"] = label;

                    detailZonasisError[idx] = detailZonasisError[idx] || [];
                    detailZonasisError[idx]["provinsiError"] = null;
                  });

                  onChange("detailZonasisError", detailZonasisError);
                  onChange("detailZonasis", [...detailZonasis], async () => {
                    dataProvinsi.forEach(({ value, label }, idx) => {
                      loadingKotaKabs[value] = true;
                      dataKotaKabs[value] = [];
                      selectedKotaKabs[value] = [];
                    });

                    onChange("loadingKotaKabs", { ...loadingKotaKabs });
                    onChange("dataKotaKabs", { ...dataKotaKabs });
                    onChange("selectedKotaKabs", { ...selectedKotaKabs });

                    let kotaKabs = await Promise.all(
                      dataProvinsi.map(({ value }) =>
                        fetchListKotaKab({ prov_id: value }),
                      ),
                    );

                    dataProvinsi.forEach(({ value, label }, idx) => {
                      loadingKotaKabs[value] = false;
                      dataKotaKabs[value] = kotaKabs[idx];
                      selectedKotaKabs[value] = kotaKabs[idx];
                    });

                    onChange("loadingKotaKabs", { ...loadingKotaKabs });
                    onChange("dataKotaKabs", { ...dataKotaKabs });
                    onChange("selectedKotaKabs", { ...selectedKotaKabs });
                  });
                }}
              >
                <i className="bi bi-check-circle me-1"></i>Set Semua Provinsi
              </a>
            </div>
          </div>
          <div className="row border-top pt-10 my-8">
            <div className="col-lg-12 text-center">
              <button
                className="btn btn-md btn-light me-3"
                onClick={() =>
                  (window.location.href = "/site-management/master/zonasi")
                }
              >
                Batal
              </button>
              <button
                disabled={this.props.isLoading}
                className="btn btn-md btn-primary block"
                onClick={() => onSave()}
              >
                {this.props.isLoading ? (
                  <>
                    <span
                      className="spinner-border spinner-border-sm me-2"
                      role="status"
                      aria-hidden="true"
                    ></span>
                    <span className="sr-only">Loading...</span>Loading...
                  </>
                ) : (
                  <>
                    <i className="fa fa-paper-plane me-1"></i>Simpan
                  </>
                )}
              </button>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default AddEditForm;
