import axios from "axios";
import Cookies from "js-cookie";
import Swal from "sweetalert2";

const configs = {
  headers: {
    Authorization: "Bearer " + Cookies.get("token"),
  },
};

export const resetError = () => {
  return {
    nameError: null,
    statusError: null,
    selectedProvinsisError: null,
  };
};

export const validate = ({ name, status, selectedProvinsis = [] }) => {
  let error = {};

  if (!name) error["nameError"] = "Nama satuan kerja tidak boleh kosong";
  if (status == null)
    error["statusError"] = "Status satuan kerja tidak boleh kosong";
  if (selectedProvinsis.length == 0)
    error["selectedProvinsisError"] =
      "Provinsi satuan kerja tidak boleh kosong";

  return error;
};

const checkToken = () => {
  return new Promise(async (resolve, reject) => {
    if (Cookies.get("token") == null) {
      const { isConfirmed } = await Swal.fire({
        title: "Unauthenticated.",
        text: "Silahkan login ulang",
        icon: "warning",
        confirmButtonText: "Ok",
      });

      if (isConfirmed) return reject();
    }

    resolve();
  });
};

export const fetchSatuanKerja = async ({
  start,
  length,
  search = "",
  orderBy = "nama_satker",
  orderDir = "desc",
}) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/list_satker_admin",
            {
              mulai: start,
              limit: length,
              cari: search,
              sort: `${orderBy} ${orderDir}`,
            },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let {
              TotalLength,
              Data = [],
              Status = false,
              Message = "",
            } = result || {};
            [TotalLength] = TotalLength;
            const { jml_data = 0 } = TotalLength || {};

            if (Status) resolve({ Total: jml_data, Data });
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchDetailSatuanKerja = async ({ id }) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/satker_select_id",
            { id },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let {
              DataWilayah = [],
              DataPelatihan = [],
              Status = false,
              Message = "",
            } = result || {};

            if (Status)
              resolve({ provinsis: DataWilayah, pelatihans: DataPelatihan });
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchStatusAktif = async () => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        resolve([
          { value: 1, label: "Aktif" },
          { value: 0, label: "Tidak Aktif" },
        ]);
      })
      .catch(reject);
  });
};

export const fetchListProvinsi = async () => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(process.env.REACT_APP_BASE_API_URI + "/provinsi", {}, configs)
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            Data = Data.map(({ id, name, meta }) => ({
              value: id,
              label: name,
            }));

            if (Status) resolve(Data);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchListKotaKab = async ({ prov_id }) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/kabupaten",
            { kdprop: prov_id },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            Data = Data.map(({ id, name, meta }) => ({
              value: id,
              label: name,
            }));

            if (Status) resolve(Data);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const insertSatuanKerja = async (payload) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/add_satker",
            payload,
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Message);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const updateSatuanKerja = async (payload) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/update_satker",
            payload,
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Message);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const deleteSatuanKerja = async (payload) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/delete_satker",
            payload,
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Message);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};
