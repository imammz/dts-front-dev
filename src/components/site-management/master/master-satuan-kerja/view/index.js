import React from "react";
import Header from "../../../../Header";
import SideNav from "../../../../SideNav";
import Footer from "../../../../Footer";
import Select from "react-select";
import DataTable from "react-data-table-component";
import {
  deleteSatuanKerja,
  fetchDetailSatuanKerja,
  fetchListProvinsi,
  fetchStatusAktif,
  fetchZonasi,
} from "../actions";
import Swal from "sweetalert2";
import {
  capitalizeFirstLetter,
  capitalizeTheFirstLetterOfEachWord,
} from "../../../../publikasi/helper";
import { withRouter } from "../../../../RouterUtil";
import Cookies from "js-cookie";
import { capitalWord, dateRange } from "../../../../pelatihan/Pelatihan/helper";

class Content extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentRowsPerPage: 10,
      currentPage: 1,
      sortBy: 0,
      sortDir: "asc",

      loading: false,
      totalData: 0,
      data: [],
      dataAkademi: [],
      dataTema: [],
      dataPelatihan: [],
    };
  }

  onChangeRowsPerPage = (currentRowsPerPage, currentPage) => {
    this.setState({
      currentRowsPerPage,
      currentPage,
    });
  };

  onChangePage = (currentPage, totalRows) => {
    this.setState({
      currentPage: currentPage,
    });
  };

  onSort = ({ id }, direction, rows) => {
    this.setState({
      sortBy: id,
      sortDir: direction,
    });
  };

  fetch = async () => {
    const { id } = this.props.params || {};

    try {
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info",
        allowOutsideClick: false,
        didOpen: () => Swal.showLoading(),
      });

      const [
        { provinsis = [], pelatihans = [] },
        dataStatusAktif,
        dataProvinsi,
      ] = await Promise.all([
        fetchDetailSatuanKerja({ id }),
        fetchStatusAktif(),
        fetchListProvinsi(),
      ]);

      let name = "",
        status = null,
        selectedProvinsis = [];
      provinsis.forEach(({ name: iname, status: istatus, kdprop }) => {
        let [prop] = dataProvinsi.filter(({ value }) => value == kdprop);
        name = iname;
        status = istatus;
        selectedProvinsis = [...selectedProvinsis, prop];
      });

      let dataAkademi = {};
      let dataTema = {};
      let dataPelatihan = [];
      pelatihans.forEach((pel) => {
        const { akademi, pelatihan, tema } = pel || {};

        if (!dataAkademi[akademi]) dataAkademi[akademi] = [];
        dataAkademi[akademi] = [...dataAkademi[akademi], pel];

        if (!dataTema[tema]) dataTema[tema] = [];
        dataTema[tema] = [...dataTema[tema], pel];

        dataPelatihan = [
          ...dataPelatihan,
          { label: pelatihan, status_pelatihan: "-", status: 1, ...pel },
        ];
      });

      dataAkademi = Object.values(dataAkademi).reduce((arr, darr) => {
        const [d] = darr || [];
        return [
          ...arr,
          { label: d?.akademi, jumlah: (darr || []).length, status: 1 },
        ];
      }, []);

      dataTema = Object.values(dataTema).reduce((arr, darr) => {
        const [d] = darr || [];
        return [
          ...arr,
          { label: d?.tema, jumlah: (darr || []).length, status: 1 },
        ];
      }, []);

      this.setState({
        totalData: name ? 1 : 0,
        data: [{ name, status, selectedProvinsis }],
        dataAkademi: Object.values(dataAkademi),
        dataTema: Object.values(dataTema),
        dataPelatihan: Object.values(dataPelatihan),
      });

      Swal.close();
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  back = () => {
    window.location.href = "/site-management/master/satuan-kerja";
  };

  remove = async (id) => {
    try {
      const { isConfirmed = false } = await Swal.fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      });

      if (isConfirmed) {
        Swal.fire({
          title: "Mohon Tunggu!",
          icon: "info",
          allowOutsideClick: false,
          didOpen: () => Swal.showLoading(),
        });

        const message = await deleteSatuanKerja({
          id,
          user_by: parseInt(Cookies.get("user_id")),
        });

        Swal.close();

        await Swal.fire({
          title: message || "Satuan kerja berhasil dihapus",
          icon: "success",
          confirmButtonText: "Ok",
        });

        this.back();
      }

      // throw new Error("API delete belum tersedia");
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  componentDidMount() {
    this.fetch();
  }

  render() {
    const { id } = this.props.params || {};
    const name = (this.state.data || [])[0]?.name || "";
    const status = (this.state.data || [])[0]?.status || "";

    const columns = [
      {
        name: "No",
        center: true,
        width: "70px",
        cell: (row, index) => (
          <div className="row">
            <div className="col-md-12">
              <span>
                {(this.state.currentPage - 1) * this.state.currentRowsPerPage +
                  index +
                  1}
              </span>
            </div>
          </div>
        ),
      },
      {
        name: "Provinsi",
        // width: '225px',
        cell: ({ selectedProvinsis = [] }) => (
          <div className="row">
            {selectedProvinsis.map(({ label }) => (
              <div className="col-md-12 m-1">
                <span>- {label}</span>
              </div>
            ))}
          </div>
        ),
      },
    ];

    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Satuan Kerja
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={() => this.back()}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500">
                    <i className="fa fa-chevron-left pe-1"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>

                <a
                  href={`/site-management/master/satuan-kerja/edit/${id}`}
                  className="btn btn-warning fw-bolder btn-sm me-2"
                >
                  <i className="bi bi-gear-fill"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Edit
                  </span>
                </a>

                <button
                  className={`btn btn-sm btn-danger btn-active-light-info ${
                    this.state.dataPelatihan.length > 0 ? "disabled" : ""
                  }`}
                  onClick={(e) => {
                    this.remove(id);
                    e.preventDefault();
                  }}
                >
                  <i className="bi bi-trash-fill text-white pe-1"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Detail Satuan Kerja
                          </h1>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="mb-7">
                          <div className="d-flex flex-wrap py-3">
                            <div className="flex-grow-1 mb-3">
                              <div className="d-flex justify-content-between align-items-start flex-wrap">
                                <div className="d-flex flex-column">
                                  <div className="d-flex align-items-center mb-0">
                                    <h5 className="fw-bolder mb-0 fs-5">
                                      {capitalizeFirstLetter(name)}
                                    </h5>
                                  </div>
                                  <div className="d-flex flex-wrap fw-bold fs-6 pe-2">
                                    <span className="text-muted fs-7">
                                      <span
                                        className={
                                          "badge badge-light-" +
                                          (status == 1 ? "success" : "danger") +
                                          " fs-7 m-1"
                                        }
                                      >
                                        {capitalizeFirstLetter(
                                          status == 1 ? "Aktif" : "Tidak Aktif",
                                        )}
                                      </span>
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div className="d-flex border-bottom p-0">
                          <ul className="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap">
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary active"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_1"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline d-none">
                                  Wilayah Kerja
                                </span>
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary false"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_2"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline d-none">
                                  Pelatihan
                                </span>
                              </a>
                            </li>
                          </ul>
                        </div>

                        <div className="tab-content" id="detail-account-tab">
                          {/* tab 1 */}
                          <div
                            className="tab-pane fade show active"
                            id="kt_tab_pane_1"
                            role="tabpanel"
                          >
                            <div className="mt-7">
                              <div className="row pt-7">
                                <div className="table-responsive">
                                  <DataTable
                                    columns={columns}
                                    data={this.state.data}
                                    // progressPending={this.state.loading}
                                    persistTableHead={true}
                                    noDataComponent={
                                      <div className="mt-5">Tidak Ada Data</div>
                                    }
                                    highlightOnHover
                                    pointerOnHover
                                    pagination
                                    // paginationServer
                                    // sortServer
                                    paginationTotalRows={this.state.totalData}
                                    onChangeRowsPerPage={
                                      this.onChangeRowsPerPage
                                    }
                                    onChangePage={this.onChangePage}
                                    onSort={this.onSort}
                                    customStyles={{
                                      headCells: {
                                        style: {
                                          background: "rgb(243, 246, 249)",
                                        },
                                      },
                                    }}
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div
                            className="tab-pane fade"
                            id="kt_tab_pane_2"
                            role="tabpanel"
                          >
                            <div className="mt-7">
                              <div className="row pt-7">
                                <h2 className="fs-5 text-muted mb-3">
                                  Target Akademi
                                </h2>
                                <div className="col-lg-12">
                                  <div className="table-responsive">
                                    <DataTable
                                      columns={[
                                        {
                                          name: "No",
                                          width: "70px",
                                          center: true,
                                          cell: ({ idx }) => idx + 1,
                                        },
                                        {
                                          name: "Akademi",
                                          selector: ({ label }) => (
                                            <div className="mt-2">{label}</div>
                                          ),
                                        },
                                        {
                                          name: "Jml. Pelatihan",
                                          center: true,
                                          selector: ({ jumlah }) => {
                                            return (
                                              <span className="badge badge-light-primary">
                                                {jumlah}
                                              </span>
                                            );
                                          },
                                        },
                                        {
                                          name: "Status",
                                          width: "200px",
                                          center: true,
                                          selector: ({ status }) => (
                                            <span
                                              className={
                                                "badge badge-light-" +
                                                (status == 1
                                                  ? "success"
                                                  : "danger") +
                                                " fs-7 m-1"
                                              }
                                            >
                                              {status == 1
                                                ? "Publish"
                                                : "Unpublish"}
                                            </span>
                                          ),
                                        },
                                      ]}
                                      data={(
                                        this?.state?.dataAkademi || []
                                      ).map((d, idx) => ({ ...d, idx }))}
                                      highlightOnHover
                                      pointerOnHover
                                      pagination
                                      // paginationTotalRows={state.soals.length}
                                      // selectableRows
                                      // selectableRowSelected={({ id = null } = {}) => state.selectedRows.map(({ id } = {}) => id).includes(id)}
                                      // onSelectedRowsChange={({ selectedRows }) => state.setSelectedRows(selectedRows)}
                                      customStyles={{
                                        headCells: {
                                          style: {
                                            background: "rgb(243, 246, 249)",
                                          },
                                        },
                                      }}
                                      noDataComponent={
                                        <div className="mt-5">
                                          Tidak Ada Data
                                        </div>
                                      }
                                      persistTableHead={true}
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="row pt-7">
                                <h2 className="fs-5 text-muted mb-3">
                                  Target Tema
                                </h2>
                                <div className="col-lg-12">
                                  <div className="table-responsive">
                                    <DataTable
                                      columns={[
                                        {
                                          name: "No",
                                          width: "70px",
                                          center: true,
                                          cell: ({ idx }) => idx + 1,
                                        },
                                        {
                                          name: "Tema",
                                          selector: ({ label }) => (
                                            <div className="mt-2">{label}</div>
                                          ),
                                        },
                                        {
                                          name: "Jml. Pelatihan",
                                          center: true,
                                          selector: ({ jumlah }) => {
                                            return (
                                              <span className="badge badge-light-primary">
                                                {jumlah}
                                              </span>
                                            );
                                          },
                                        },
                                        {
                                          name: "Status",
                                          width: "200px",
                                          center: true,
                                          selector: ({ status }) => (
                                            <span
                                              className={
                                                "badge badge-light-" +
                                                (status == 1
                                                  ? "success"
                                                  : "danger") +
                                                " fs-7 m-1"
                                              }
                                            >
                                              {status == 1
                                                ? "Publish"
                                                : "Unpublish"}
                                            </span>
                                          ),
                                        },
                                      ]}
                                      data={(this?.state?.dataTema || []).map(
                                        (d, idx) => ({ ...d, idx }),
                                      )}
                                      highlightOnHover
                                      pointerOnHover
                                      pagination
                                      // paginationTotalRows={state.soals.length}
                                      // selectableRows
                                      // selectableRowSelected={({ id = null } = {}) => state.selectedRows.map(({ id } = {}) => id).includes(id)}
                                      // onSelectedRowsChange={({ selectedRows }) => state.setSelectedRows(selectedRows)}
                                      customStyles={{
                                        headCells: {
                                          style: {
                                            background: "rgb(243, 246, 249)",
                                          },
                                        },
                                      }}
                                      noDataComponent={
                                        <div className="mt-5">
                                          Tidak Ada Data
                                        </div>
                                      }
                                      persistTableHead={true}
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="row pt-7">
                                <h2 className="fs-5 text-muted mb-3">
                                  Target Pelatihan
                                </h2>
                                <div className="col-lg-12">
                                  <div className="table-responsive">
                                    <DataTable
                                      columns={[
                                        {
                                          name: "No",
                                          width: "70px",
                                          grow: 1,
                                          center: true,
                                          sortable: false,
                                          cell: (row) => {
                                            return (
                                              <div>
                                                <span>
                                                  {(row?.idx || 0) + 1}
                                                </span>
                                              </div>
                                            );
                                          },
                                        },
                                        {
                                          Header: "ID Pelatihan",
                                          accessor: "",
                                          className:
                                            "min-w-250px min-h-250px mw-250px mh-250px",
                                          sortType: "basic",
                                          name: "ID",
                                          width: "150px",
                                          grow: 1,
                                          initSort: "nama",
                                          sortable: true,
                                          cell: (currentPelatihan) => {
                                            return (
                                              <div>
                                                <span>
                                                  {
                                                    currentPelatihan?.slug_pelatian_id
                                                  }
                                                </span>
                                              </div>
                                            );
                                          },
                                          selector: (currentPelatihan) =>
                                            currentPelatihan.id_pelatihan,
                                        },
                                        {
                                          Header: "Pelatihan",
                                          accessor: "",
                                          className:
                                            "min-w-250px min-h-250px mw-250px mh-250px",
                                          sortType: "basic",
                                          name: "Nama Pelatihan",
                                          grow: 1,
                                          width: "300px",
                                          initSort: "nama",
                                          sortable: true,
                                          cell: (currentPelatihan) => {
                                            return (
                                              <div className="my-2">
                                                <span className="text-muted fw-semibold">
                                                  {
                                                    currentPelatihan.metode_pelatihan
                                                  }
                                                </span>
                                                <br />
                                                <span>
                                                  <b>
                                                    {capitalWord(
                                                      currentPelatihan.pelatihan,
                                                    )}
                                                  </b>
                                                  <br />
                                                  <span className="text-muted fw-semibold">
                                                    {
                                                      currentPelatihan.penyelenggara
                                                    }
                                                  </span>
                                                </span>
                                              </div>
                                            );
                                          },
                                          selector: (currentPelatihan) =>
                                            currentPelatihan.pelatihan,
                                        },
                                        {
                                          Header: "Jadwal Pendaftaran",
                                          accessor: "",
                                          sortType: "basic",
                                          name: "Jadwal Pendaftaran",
                                          grow: 1,
                                          width: "200px",
                                          initSort: "nama",
                                          sortable: true,
                                          cell: (row) => (
                                            <div>
                                              <span className="fs-7">
                                                {dateRange(
                                                  row.pendaftaran_start,
                                                  row.pendaftaran_end,
                                                  "YYYY-MM-DD HH:mm:ss",
                                                  "DD MMM YYYY",
                                                )}
                                              </span>
                                              <p className="fs-8 my-0 fw-semibold text-muted">
                                                {dateRange(
                                                  row.pendaftaran_start,
                                                  row.pendaftaran_end,
                                                  "YYYY-MM-DD HH:mm:ss",
                                                  "HH:mm:ss",
                                                )}
                                              </p>
                                            </div>
                                          ),
                                          selector: (currentPelatihan) =>
                                            currentPelatihan.pelatihan,
                                        },
                                        {
                                          Header: "Jadwal Pelatihan",
                                          accessor: "",
                                          sortType: "basic",
                                          name: "Jadwal Pelatihan",
                                          grow: 1,
                                          width: "200px",
                                          initSort: "nama",
                                          sortable: true,
                                          cell: (row) => (
                                            <div>
                                              <span className="fs-7">
                                                {dateRange(
                                                  row.pelatihan_start,
                                                  row.pelatihan_end,
                                                  "YYYY-MM-DD HH:mm:ss",
                                                  "DD MMM YYYY",
                                                )}
                                              </span>
                                              <p className="fs-8 my-0 fw-semibold text-muted">
                                                {dateRange(
                                                  row.pelatihan_start,
                                                  row.pelatihan_end,
                                                  "YYYY-MM-DD HH:mm:ss",
                                                  "HH:mm:ss",
                                                )}
                                              </p>
                                            </div>
                                          ),
                                          selector: (currentPelatihan) =>
                                            currentPelatihan.pelatihan,
                                        },
                                        {
                                          Header: "Status Publsh",
                                          accessor: "",
                                          className:
                                            "min-w-250px min-h-250px mw-250px mh-250px",
                                          sortType: "basic",
                                          name: "Status",
                                          grow: 1,
                                          initSort: "nama",
                                          sortable: true,
                                          cell: (currentPelatihan) => {
                                            return (
                                              <div>
                                                <span
                                                  className={
                                                    "badge badge-light-" +
                                                    (currentPelatihan.status_publish ==
                                                    "1"
                                                      ? "success"
                                                      : currentPelatihan.status_publish ==
                                                          "0"
                                                        ? "danger"
                                                        : "warning") +
                                                    " fs-7 m-1"
                                                  }
                                                >
                                                  {currentPelatihan.status_publish ==
                                                  "1"
                                                    ? "Publish"
                                                    : currentPelatihan.status_publish ==
                                                        "0"
                                                      ? "Unpublish"
                                                      : "Unlisted"}
                                                </span>
                                              </div>
                                            );
                                          },
                                          selector: (currentPelatihan) =>
                                            currentPelatihan.pelatihan,
                                        },
                                        {
                                          Header: "Status Pelatihan",
                                          accessor: "",
                                          className:
                                            "min-w-250px min-h-250px mw-250px mh-250px",
                                          sortType: "basic",
                                          name: "Status Pelatihan",
                                          grow: 1,
                                          initSort: "nama",
                                          sortable: true,
                                          cell: (currentPelatihan) => {
                                            return (
                                              <div>
                                                <span
                                                  className={
                                                    "badge badge-light-" +
                                                    (currentPelatihan.status_pelatihan ==
                                                    "Selesai"
                                                      ? "success"
                                                      : currentPelatihan.status_pelatihan ==
                                                          "Dibatalkan"
                                                        ? "danger"
                                                        : currentPelatihan.status_pelatihan ==
                                                            "Pelatihan"
                                                          ? "warning"
                                                          : "primary") +
                                                    " fs-7 m-1"
                                                  }
                                                >
                                                  {
                                                    currentPelatihan.status_pelatihan
                                                  }
                                                </span>
                                              </div>
                                            );
                                          },
                                          selector: (currentPelatihan) =>
                                            currentPelatihan.pelatihan,
                                        },
                                        {
                                          Header: "Aksi",
                                          accessor: "",
                                          className:
                                            "min-w-250px min-h-250px mw-250px mh-250px",
                                          sortType: "basic",
                                          name: "Aksi",
                                          grow: 1,
                                          width: "150px",
                                          initSort: "nama",
                                          sortable: true,
                                          cell: (currentPelatihan) => {
                                            return (
                                              <div className="row">
                                                <a
                                                  href={
                                                    "/pelatihan/view-pelatihan/" +
                                                    currentPelatihan.pelatian_id
                                                  }
                                                  id={
                                                    currentPelatihan.pelatian_id
                                                  }
                                                  title="View"
                                                  className="btn btn-icon btn-sm btn-primary"
                                                >
                                                  <span className="svg-icon svg-icon-3">
                                                    <i className="bi bi-eye-fill"></i>
                                                  </span>
                                                </a>
                                              </div>
                                            );
                                          },
                                          selector: (currentPelatihan) =>
                                            currentPelatihan.pelatihan,
                                        },
                                      ]}
                                      data={(
                                        this?.state?.dataPelatihan || []
                                      ).map((d, idx) => ({ ...d, idx }))}
                                      highlightOnHover
                                      pointerOnHover
                                      pagination
                                      customStyles={{
                                        headCells: {
                                          style: {
                                            background: "rgb(243, 246, 249)",
                                          },
                                        },
                                      }}
                                      noDataComponent={
                                        <div className="mt-5">
                                          Tidak Ada Data
                                        </div>
                                      }
                                      persistTableHead={true}
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const ViewZonasi = (props) => {
  return (
    <div>
      <SideNav />
      <Header />
      <Content {...props} />
      <Footer />
    </div>
  );
};

export default withRouter(ViewZonasi);
