import React from "react";
import Header from "../../../../Header";
import SideNav from "../../../../SideNav";
import Footer from "../../../../Footer";
import Select from "react-select";
import DataTable from "react-data-table-component";
import { deleteSatuanKerja, fetchSatuanKerja } from "../actions";
import Swal from "sweetalert2";
import {
  capitalizeFirstLetter,
  capitalizeTheFirstLetterOfEachWord,
} from "../../../../publikasi/helper";
import { withRouter } from "../../../../RouterUtil";
import Cookies from "js-cookie";

class FilterDialog extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      ref,
      id = "filter",
      onReset = () => null,
      onFilter = () => null,
    } = this.props || {};

    return (
      <div className="modal fade" tabindex="-1" id={id} ref={ref}>
        <div className="modal-dialog modal-lg">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">
                <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <path
                      d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                      fill="currentColor"
                    />
                  </svg>
                </span>
                Filter
              </h5>
              <div
                className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal"
                aria-label="Close"
              >
                <span className="svg-icon svg-icon-2x">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <rect
                      opacity="0.5"
                      x="6"
                      y="17.3137"
                      width="16"
                      height="2"
                      rx="1"
                      transform="rotate(-45 6 17.3137)"
                      fill="currentColor"
                    />
                    <rect
                      x="7.41422"
                      y="6"
                      width="16"
                      height="2"
                      rx="1"
                      transform="rotate(45 7.41422 6)"
                      fill="currentColor"
                    />
                  </svg>
                </span>
              </div>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-lg-12 mb-7 fv-row">
                  <label className="form-label">Penyelenggara</label>
                  <Select
                    name="penyelenggara"
                    placeholder="Silahkan pilih"
                    // noOptionsMessage={({ inputValue }) => !inputValue ? this.state.dataxpenyelenggara : "Data tidak tersedia"}
                    className="form-select-sm form-select-solid selectpicker p-0"
                    // options={this.state.dataxpenyelenggara}
                    // onChange={this.handleChangePenyelenggara}
                    // value={this.state.dataxpenyelenggaravalue}
                  />
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <div className="d-flex justify-content-between">
                <button
                  type="reset"
                  className="btn btn-sm btn-danger me-3"
                  onClick={onReset}
                >
                  Reset
                </button>
                <button
                  type="submit"
                  className="btn btn-sm btn-primary"
                  data-bs-dismiss="modal"
                  onClick={onFilter}
                >
                  Apply Filter
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

class Content extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentRowsPerPage: 10,
      currentPage: 1,
      sortBy: 1,
      sortDir: "desc",
      keyword: "",

      loading: false,
      totalData: 0,
      data: [],
    };
  }

  onChangeRowsPerPage = (currentRowsPerPage, currentPage) => {
    this.setState(
      {
        currentRowsPerPage,
        currentPage,
      },
      () => this.fetch(),
    );
  };

  onChangePage = (currentPage, totalRows) => {
    this.setState(
      {
        currentPage: currentPage,
      },
      () => this.fetch(),
    );
  };

  onSort = ({ id }, direction, rows) => {
    this.setState(
      {
        sortBy: id,
        sortDir: direction,
      },
      () => this.fetch(),
    );
  };

  search = (keyword) => {
    this.setState(
      {
        keyword,
        currentPage: 1,
      },
      () => this.fetch(),
    );
  };

  fetch = async () => {
    try {
      // Swal.fire({
      //   title: 'Mohon Tunggu!',
      //   icon: 'info',
      //   allowOutsideClick: false,
      //   didOpen: () => Swal.showLoading()
      // });

      let sortBy = this.state.sortBy,
        sortDir = this.state.sortDir;
      if (
        new URL(window.location.href).searchParams.has("spotlight-inserted")
      ) {
        sortBy = 1;
        sortDir = "desc";
      }

      this.setState({
        loading: true,
        totalData: 0,
        data: [],
      });

      const sortFields = ["id", "id", "name", "jml_pelatihan", "status_satker"];
      const [dataZonasi] = await Promise.all([
        fetchSatuanKerja({
          start: (this.state.currentPage - 1) * this.state.currentRowsPerPage,
          length: this.state.currentRowsPerPage,
          search: this.state.keyword,
          orderBy: sortFields[sortBy],
          orderDir: sortDir,
        }),
      ]);

      const { Total = 0, Data = [] } = dataZonasi || {};

      this.setState({
        loading: false,
        totalData: Total,
        data: Data,
      });

      // Swal.close();
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }

    this.setState({ loading: false });
  };

  remove = async (id) => {
    try {
      const { isConfirmed = false } = await Swal.fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      });

      if (isConfirmed) {
        Swal.fire({
          title: "Mohon Tunggu!",
          icon: "info",
          allowOutsideClick: false,
          didOpen: () => Swal.showLoading(),
        });

        const message = await deleteSatuanKerja({
          id,
          user_by: parseInt(Cookies.get("user_id")),
        });

        Swal.close();

        await Swal.fire({
          title: message || "Satuan kerja berhasil dihapus",
          icon: "success",
          confirmButtonText: "Ok",
        });

        this.fetch();
      }

      // throw new Error("API delete belum tersedia");
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  componentDidMount() {
    this.fetch();
  }

  render() {
    const columns = [
      {
        name: "No",
        center: true,
        width: "70px",
        cell: (row, index) => (
          <div>
            <span>
              {(this.state.currentPage - 1) * this.state.currentRowsPerPage +
                index +
                1}
            </span>
          </div>
        ),
      },
      {
        name: "Nama Satuan Kerja",
        width: "250px",
        sortable: true,
        cell: ({ id, name }) => (
          <div>
            <span className="d-flex flex-column my-2">
              <a
                href={"/site-management/master/satuan-kerja/view/" + id}
                className="text-dark"
              >
                <span
                  className="fw-bolder fs-7"
                  style={{
                    overflow: "hidden",
                    whiteSpace: "wrap",
                    textOverflow: "ellipses",
                  }}
                >
                  {name}
                </span>
              </a>
            </span>
          </div>
        ),
      },
      {
        name: "Akademi",
        center: true,
        sortable: true,
        selector: ({ jml_akademi = 0 }) => (
          <div>
            <span className="badge badge-light-primary fs-7 m-1">
              {jml_akademi}
            </span>
          </div>
        ),
      },
      {
        name: "Tema",
        center: true,
        sortable: true,
        selector: ({ jml_tema = 0 }) => (
          <div>
            <span className="badge badge-light-primary fs-7 m-1">
              {jml_tema}
            </span>
          </div>
        ),
      },
      {
        name: "Pelatihan",
        center: true,
        sortable: true,
        selector: ({ jml_pelatihan = 0 }) => (
          <div>
            <span className="badge badge-light-primary fs-7 m-1">
              {jml_pelatihan}
            </span>
          </div>
        ),
      },
      {
        name: "Status",
        center: true,
        width: "110px",
        sortable: true,
        selector: ({ status_satker = "" }) => (
          <div>
            <span
              className={
                "badge badge-light-" +
                (status_satker.toLowerCase() == "aktif"
                  ? "success"
                  : "danger") +
                " fs-7 m-1"
              }
            >
              {capitalizeFirstLetter(status_satker)}
            </span>
          </div>
        ),
      },
      {
        name: "Aksi",
        center: true,
        width: "150px",
        cell: ({ id, jml_pelatihan = 0 }) => {
          return (
            <div>
              <a
                href={"/site-management/master/satuan-kerja/view/" + id}
                title="Detail"
                className="btn btn-icon btn-bg-primary btn-sm me-1"
              >
                <i className="bi bi-eye-fill text-white"></i>
              </a>
              <a
                href={"/site-management/master/satuan-kerja/edit/" + id}
                title="Edit"
                className="btn btn-icon btn-bg-warning btn-sm me-1"
              >
                <i className="bi bi-gear-fill text-white"></i>
              </a>
              <a
                href="#"
                onClick={(e) => {
                  this.remove(id);
                  e.preventDefault();
                }}
                title="Hapus"
                className={`btn btn-icon btn-bg-danger btn-sm me-1 ${
                  jml_pelatihan != 0 ? "disabled" : ""
                }`}
              >
                <i className="bi bi-trash-fill text-white"></i>
              </a>
            </div>
          );
        },
      },
    ];

    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Satuan Kerja
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <a
                  href={"/site-management/master/satuan-kerja/add"}
                  className="btn btn-success fw-bolder btn-sm"
                >
                  <i className="bi bi-plus-circle"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Tambah Satuan Kerja
                  </span>
                </a>
              </div>
              {/* <a href="https://back.dev.sdmdigital.id/api/report-pelatihan" className="btn btn-primary btn-sm me-2">
                <i className="las la-file-export"></i>
                Export
              </a>
              <a href="/pelatihan/tambah-pelatihan" className="btn btn-primary btn-sm">
                <i className="las la-plus"></i>
                Tambah Pelatihan
              </a> */}
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Daftar Satuan Kerja
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <div className="d-flex align-items-center position-relative my-1 me-2">
                            <span className="svg-icon svg-icon-1 position-absolute ms-6">
                              <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className="mh-50px"
                              >
                                <rect
                                  opacity="0.5"
                                  x="17.0365"
                                  y="15.1223"
                                  width="8.15546"
                                  height="2"
                                  rx="1"
                                  transform="rotate(45 17.0365 15.1223)"
                                  fill="currentColor"
                                ></rect>
                                <path
                                  d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                  fill="currentColor"
                                ></path>
                              </svg>
                            </span>
                            <input
                              type="text"
                              data-kt-user-table-filter="search"
                              className="form-control form-control-sm form-control-solid w-250px ps-14"
                              placeholder="Cari Satuan Kerja"
                              onKeyPress={(e) => {
                                const keyword = e.target.value;
                                if (e.key == "Enter") {
                                  this.search(keyword);
                                  e.preventDefault();
                                }
                              }}
                              onChange={(e) => {
                                const keyword = e.target.value;
                                if (!keyword) {
                                  this.search(keyword);
                                  e.preventDefault();
                                }
                              }}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="table-responsive">
                          <DataTable
                            columns={columns}
                            data={this.state.data.map((row, idx) => ({
                              ...row,
                              idx,
                            }))}
                            progressPending={this.state.loading}
                            persistTableHead={true}
                            noDataComponent={
                              <div className="mt-5">Tidak Ada Data</div>
                            }
                            highlightOnHover
                            pointerOnHover
                            pagination
                            paginationServer
                            sortServer
                            paginationTotalRows={this.state.totalData}
                            paginationComponentOptions={{
                              selectAllRowsItem: true,
                              selectAllRowsItemText: "Semua",
                            }}
                            onChangeRowsPerPage={this.onChangeRowsPerPage}
                            onChangePage={this.onChangePage}
                            onSort={this.onSort}
                            customStyles={{
                              headCells: {
                                style: {
                                  background: "rgb(243, 246, 249)",
                                },
                              },
                            }}
                            conditionalRowStyles={[
                              {
                                when: ({ idx = 0 }) =>
                                  new URL(
                                    window.location.href,
                                  ).searchParams.has("spotlight-inserted") &&
                                  idx == 0,
                                classNames: ["bg-warning"],
                              },
                            ]}
                          />
                        </div>
                      </div>

                      <FilterDialog />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const MasterZonasi = () => {
  return (
    <div>
      <SideNav />
      <Header />
      <Content />
      <Footer />
    </div>
  );
};

export default withRouter(MasterZonasi);
