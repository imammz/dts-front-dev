import React from "react";
import Select from "react-select";
import Swal from "sweetalert2";
import { fetchListKotaKab } from "../actions";

class AddEditForm extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    console.log(this.props.isLoading);
    let {
      name = "",
      nameError = null,
      status,
      statusError = null,
      dataStatusAktif = [],
      dataProvinsi = [],
      selectedProvinsis = [],
      selectedProvinsisError = null,
      onChange = (k, v) => {},
      onSave = () => {},
    } = this.props || {};

    const [oStatus] = dataStatusAktif.filter((s) => s.value == status);

    let _dataProvinsi = [
      { value: "*", label: "Pilih Semua" },
      ...(dataProvinsi || []),
    ];

    return (
      <>
        <div className="col-lg-12 mb-7 fv-row">
          <label className="form-label required">Nama Satuan Kerja</label>
          <input
            className="form-control form-control-sm"
            placeholder="Masukkan Nama Satuan Kerja"
            value={name}
            onChange={(e) => {
              onChange("nameError", "");
              onChange("name", e.target.value);
            }}
          />
          <span style={{ color: "red" }}>{nameError}</span>
        </div>
        <div className="col-lg-12 mb-7 fv-row">
          <label className="form-label required">Wilayah Kerja</label>
          <Select
            isMulti
            placeholder="Silahkan Pilih Provinsi"
            // noOptionsMessage={({ inputValue }) => !inputValue ? this.state.dataxakademi : "Data tidak tersedia"}
            value={selectedProvinsis}
            className="form-select-sm selectpicker p-0"
            onChange={(selectedProvinsis) => {
              onChange("selectedProvinsisError", "");
              const isAllSelected = selectedProvinsis
                .map(({ value }) => value)
                .includes("*");
              if (isAllSelected) {
                onChange("selectedProvinsis", dataProvinsi);
              } else {
                onChange("selectedProvinsis", selectedProvinsis);
              }
            }}
            options={_dataProvinsi}
            // isOptionDisabled={(option) => option.disabled}
          />
          <span style={{ color: "red" }}>{selectedProvinsisError}</span>
        </div>
        <div className="col-lg-12 mb-7 fv-row">
          <label className="form-label required">Status</label>
          <Select
            placeholder="Silahkan Pilih Status"
            // noOptionsMessage={({ inputValue }) => !inputValue ? this.state.dataxakademi : "Data tidak tersedia"}
            value={oStatus}
            className="form-select-sm selectpicker p-0"
            onChange={({ value }) => {
              onChange("statusError", "");
              onChange("status", value);
            }}
            options={dataStatusAktif}
            // isOptionDisabled={(option) => option.disabled}
          />
          <span style={{ color: "red" }}>{statusError}</span>
        </div>
        <div className="row my-7">
          <div className="col-lg-12 text-center">
            <button
              className="btn btn-md btn-light me-5"
              onClick={() =>
                (window.location.href = "/site-management/master/satuan-kerja")
              }
            >
              Batal
            </button>
            <button
              className="btn btn-md btn-primary"
              onClick={() => onSave()}
              disabled={this.props.isLoading}
            >
              {this.props.isLoading ? (
                <>
                  <span
                    className="spinner-border spinner-border-sm me-2"
                    role="status"
                    aria-hidden="true"
                  ></span>
                  <span className="sr-only">Loading...</span>Loading...
                </>
              ) : (
                <>
                  <i className="fa fa-paper-plane me-1"></i>Simpan
                </>
              )}
            </button>
          </div>
        </div>
      </>
    );
  }
}

export default AddEditForm;
