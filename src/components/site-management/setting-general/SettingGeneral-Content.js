import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import ImageCard from "./components/ImageCard";
import SocialMedia from "./components/SocialMedia";
import Link from "./components/Link";
import Select from "react-select";
import imageCompression from "browser-image-compression";
import Telp from "./components/Telp";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "@arbor-dev/ckeditor5-arbor-custom";

const resizeFile = async (file) => {
  const options = {
    maxSizeMB: 5,
    maxWidthOrHeight: 300,
    useWebWorker: true,
  };

  return imageCompression(file, options);
};

export default class SettingGeneralContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmitAction.bind(this);
    this.handleTambahSocialMedia =
      this.handleTambahSocialMediaAction.bind(this);
    this.handleTambahLink = this.handleTambahLinkAction.bind(this);
    this.handleDeleteCallBackSocialMedia =
      this.handleDeleteCallBackSocialMediaAction.bind(this);
    this.handleDeleteCallBackLink =
      this.handleDeleteCallBackLinkAction.bind(this);
    this.handleChangeHeader = this.handleChangeHeaderAction.bind(this);
    this.handleChangeFooter = this.handleChangeFooterAction.bind(this);
    this.handleChangeDescription =
      this.handleChangeDescriptionAction.bind(this);
    this.handleChangeAlamat = this.handleChangeAlamatAction.bind(this);
    this.handleCallBackSocialMedia =
      this.handleCallBackSocialMediaAction.bind(this);
    this.handleCallBackLink = this.handleCallBackLinkAction.bind(this);
    this.handleChangePrimary = this.handleChangePrimaryAction.bind(this);
    this.handleChangeSecondary = this.handleChangeSecondaryAction.bind(this);
    this.handleChangeExtras = this.handleChangeExtrasAction.bind(this);
    this.handleTambahTelp = this.handleTambahTelpAction.bind(this);
    this.handleCallBackTelp = this.handleCallBackTelpAction.bind(this);
    this.handleDeleteCallBackTelp =
      this.handleDeleteCallBackTelpAction.bind(this);
    this.handleChangeEmail = this.handleChangeEmailAction.bind(this);
    this.handleChangeKoordinat = this.handleChangeKoordinatAction.bind(this);
    this.handleChangeSyaratKetentuan =
      this.handleChangeSyaratKetentuanAction.bind(this);
    this.handleChangeKebijakanPrivasi =
      this.handleChangeKebijakanPrivasiAction.bind(this);
  }

  styles = {
    font: {
      position: "absolute",
      top: "10",
      left: "50%",
      transform: "translateX(-50%)",
    },
  };

  color_schemes = [
    { value: 1, label: "primary (#0100BE)" },
    { value: 2, label: "secondary (#2A62ED)" },
    { value: 3, label: "extras (#5EC8F2)" },
  ];

  state = {
    datax: [],
    isLoading: false,
    errors: {},
    socials: [],
    socials_last_index: 0,
    max_social: 99,
    num_social: 0,
    links: [],
    links_last_index: 0,
    telp: [],
    max_link: 99,
    num_link: 0,
    image_header: false,
    image_footer: false,
    description: "",
    alamat: "",
    valPrimary: [],
    valSecondary: [],
    valExtras: [],
    social_media: [],
    external_links: [],
    created_at: "",
    created_by: "",
    primary: 0,
    secondary: 0,
    extras: 0,
    telps_arr: [],
    telp_last_index: 0,
    max_telp: 99,
    num_telp: 0,
    email: "",
    text_koordinat: "",
    koordinat: [],
    kebijakanprivasi: "",
    syaratketentuan: "",
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/setting/view_general",
        null,
        this.configs,
      )
      .then((res) => {
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statusx) {
          swal.close();
          const datax = res.data.result.Data[0];
          const image_header = datax.header_logo;
          const image_footer = datax.footer_logo;
          const description = datax.logo_description;
          const alamat = datax.alamat;
          const social_media = JSON.parse(datax.social_media);
          const color = JSON.parse(datax.color);
          const external_links = JSON.parse(datax.link);
          const text_koordinat = datax.koordinat;
          const telps_arr = JSON.parse(datax.notelp); //JSON.parse(datax.telp);
          const email = datax.email;
          const syaratketentuan = datax.syarat_ketentuan
            ? datax.syarat_ketentuan
            : "";
          const kebijakanprivasi = datax.kebijakan_privacy
            ? datax.kebijakan_privacy
            : "";
          this.setState({
            image_header,
            image_footer,
            description,
            alamat,
            text_koordinat,
            email,
            syaratketentuan,
            kebijakanprivasi,
          });

          const socials = [];
          let num_social = this.state.num_social;
          let socials_last_index = this.state.socials_last_index;
          const context = this;
          social_media.forEach(function (element, i) {
            const name = element.name;
            const link = element.link_social_media;
            const image_logo = element.image_logo;
            const icon = element.icon;
            socials.push(
              <SocialMedia
                index={i + 1}
                key={i + 1}
                deleteCallBack={context.handleDeleteCallBackSocialMedia}
                inputCallBack={context.handleCallBackSocialMedia}
                name={name}
                link={link}
                image_logo={image_logo}
                icon={icon}
              />,
            );
            element.key = i + 1;
            num_social++;
            socials_last_index++;
          });

          this.setState({
            social_media,
            num_social,
            socials,
            socials_last_index,
          });

          const links = [];
          let num_link = this.state.num_link;
          let links_last_index = this.state.links_last_index;
          external_links.forEach(function (element, i) {
            const name = element.nama_platform;
            const link_public = element.link_public;
            const link_member = element.link_member;
            const image = element.icon;
            const deskripsi = element.deskripsi;
            links.push(
              <Link
                index={i + 1}
                key={i + 1}
                deleteCallBack={context.handleDeleteCallBackLink}
                inputCallBack={context.handleCallBackLink}
                name={name}
                link_member={link_member}
                link_public={link_public}
                image={image}
                deskripsi={deskripsi}
              />,
            );
            element.key = i + 1;
            num_link++;
            links_last_index++;
          });
          this.setState({
            external_links,
            num_link,
            links,
            links_last_index,
          });

          const telp = [];
          let num_telp = this.state.num_telp;
          let telp_last_index = this.state.telp_last_index;
          telps_arr.forEach(function (element, i) {
            const nomor = element.nomor;
            telp.push(
              <Telp
                index={i + 1}
                key={i + 1}
                deleteCallBack={context.handleDeleteCallBackTelp}
                inputCallBack={context.handleCallBackTelp}
                nomor={nomor}
              />,
            );
            element.key = i + 1;
            num_telp++;
            telp_last_index++;
          });

          this.setState({
            telps_arr,
            num_telp,
            telp,
            telp_last_index,
          });

          color.forEach(function (element, i) {
            if (element.name == "Primary") {
              const primary = element.color;
              context.setState({
                valPrimary: {
                  label: context.color_schemes[element.color - 1].label,
                  value: element.color,
                },
                primary,
              });
            }
            if (element.name == "Secondary") {
              const secondary = element.color;
              context.setState({
                valSecondary: {
                  label: context.color_schemes[element.color - 1].label,
                  value: element.color,
                },
                secondary,
              });
            }
            if (element.name == "Extras") {
              const extras = element.color;
              context.setState({
                valExtras: {
                  label: context.color_schemes[element.color - 1].label,
                  value: element.color,
                },
                extras,
              });
            }
          });
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      })
      .catch((error) => {
        console.log(error);
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const check = this.checkEmpty(
      dataForm,
      [
        "deskripsi",
        "alamat",
        "primary",
        "secondary",
        "extras",
        "email",
        "koordinat",
        "syaratketentuan",
        "kebijakanprivasi",
      ],
      [""],
    );
    return check;
  }

  resetError() {
    let errors = {};
    errors[
      ("deskripsi",
      "alamat",
      "primary",
      "secondary",
      "extras",
      "email",
      "koordinat",
      "syaratketentuan",
      "kebijakanprivasi")
    ] = "";
    this.setState({ errors: errors });
  }

  validURL(str) {
    var pattern = new RegExp(
      "^(https?:\\/\\/)?" + // protocol
        "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
        "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
        "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
        "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
        "(\\#[-a-z\\d_]*)?$",
      "i",
    ); // fragment locator
    return !!pattern.test(str);
  }

  checkEmpty(dataForm, fieldName, fileFieldName) {
    let errors = {};
    let formIsValid = true;
    for (const field in fieldName) {
      const nameAttribute = fieldName[field];
      if (dataForm.get(nameAttribute) == "") {
        errors[nameAttribute] = "Tidak Boleh Kosong";
        formIsValid = false;
      }
    }

    const checkComponent = [
      this.checkErrorComponent("name_social_media", "error_name_socmed"),
      this.checkErrorComponent("link_social_media", "error_social_media"),
      this.checkErrorComponent("nama_platform", "error_nama_platform"),
      this.checkErrorComponent(
        "deskripsi_platform",
        "error_platform_deskripsi",
      ),
      this.checkErrorComponent("link_public", "error_link_public"),
      this.checkErrorComponent("link_member", "error_link_member"),
      this.checkErrorComponent("nomor_telp", "error_nomor_telp"),
    ];

    checkComponent.forEach(function (err) {
      if (err) {
        formIsValid = false;
      }
    });

    let validRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (this.state.email.match(validRegex) == false) {
      errors["email"] = "Email Tidak Valid";
      formIsValid = false;
    }

    if (this.state.syaratketentuan == "") {
      errors["syaratketentuan"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }
    if (this.state.kebijakanprivasi == "") {
      errors["kebijakanprivasi"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  checkErrorComponent(field_input, field_error) {
    //check class component
    const all_nama_socmed = Array.from(
      document.getElementsByClassName(field_input),
    );

    const arrError = [];

    for (let i = 0; i < all_nama_socmed.length; i++) {
      let valueNama = all_nama_socmed[i].getAttribute("value");
      let index = all_nama_socmed[i].getAttribute("index");
      if (valueNama == "" || valueNama == null) {
        const err = {
          index: index,
          message: "Tidak Boleh Kosong",
        };
        arrError.push(err);
      }
      if (
        field_input == "link_social_media" ||
        field_input == "link_public" ||
        field_input == "link_member"
      ) {
        if (!this.validURL(valueNama)) {
          const err = {
            index: index,
            message: "Bukan Valid URL",
          };
          arrError.push(err);
        }
      }

      if (field_input == "nomor_telp") {
        if (valueNama.length > 13) {
          const err = {
            index: index,
            message: "Maksimal 13 digit",
          };
          arrError.push(err);
        }

        if (valueNama.length < 7) {
          const err = {
            index: index,
            message: "Minimal 7 digit",
          };
          arrError.push(err);
        }
      }
    }

    let is_error = false;
    const allError = Array.from(document.getElementsByClassName(field_error));
    for (let i = 0; i < allError.length; i++) {
      allError[i].textContent = "";
      let index = allError[i].getAttribute("index");
      for (let j = 0; j < arrError.length; j++) {
        if (arrError[j].index == index) {
          allError[i].textContent = arrError[j].message;
          this.setState({ is_next_soal: false });
          is_error = true;
        }
      }
    }

    return is_error;
  }

  handleSubmitAction(e) {
    let timestamp = 1293683278;
    let date = new Date(timestamp * 1000);
    let ymdhis = date
      .toISOString()
      .match(/(\d{4}\-\d{2}\-\d{2})T(\d{2}:\d{2}:\d{2})/);
    ymdhis = ymdhis[1] + " " + ymdhis[2];

    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });
      const colorJson = [
        {
          name: "Primary",
          color: this.state.primary,
        },
        {
          name: "Secondary",
          color: this.state.secondary,
        },
        {
          name: "Extras",
          color: this.state.extras,
        },
      ];
      const social_media = this.state.social_media;
      social_media.forEach(function (element, i) {
        delete element.key;
      });

      const external_links = this.state.external_links;
      external_links.forEach(function (element, i) {
        delete element.key;
      });

      const telps_arr = this.state.telps_arr;
      telps_arr.forEach(function (element, i) {
        delete element.key;
      });

      const bodyJson = [
        {
          created_at: ymdhis,
          created_by: Cookies.get("user_id"),
          updated_at: ymdhis,
          updated_by: Cookies.get("user_id"),
          header_logo: this.state.image_header,
          footer_logo: this.state.image_footer,
          logo_description: this.state.description,
          syarat_ketentuan: this.state.syaratketentuan,
          kebijakan_privacy: this.state.kebijakanprivasi,
          alamat: this.state.alamat,
          color: colorJson,
          social_media: social_media,
          link: external_links,
          notelp: telps_arr,
          email: this.state.email,
          koordinat: this.state.text_koordinat,
        },
      ];

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/setting/update_general",
          bodyJson,
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
                allowOutsideClick: false,
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location.reload();
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      this.setState({ isLoading: false });
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleTambahSocialMediaAction() {
    if (this.state.num_social < this.state.max_social) {
      const current_index = this.state.socials_last_index + 1;
      this.setState({
        socials: [
          ...this.state.socials,
          <SocialMedia
            index={current_index}
            key={current_index}
            deleteCallBack={this.handleDeleteCallBackSocialMedia}
            inputCallBack={this.handleCallBackSocialMedia}
            name={false}
            link={false}
            image_logo={false}
            icon={false}
          />,
        ],
        num_social: this.state.num_social + 1,
        socials_last_index: current_index,
      });
    } else {
      swal
        .fire({
          title: "Maksimal Pilihan hanya " + this.state.max_social,
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleDeleteCallBackSocialMediaAction(e) {
    const index_delete = e.index_delete;
    const newList = this.state.socials.filter(
      (item) => item.key !== index_delete,
    );

    const newSocialMedia = this.state.social_media.filter((item) => {
      return parseInt(item.key) !== parseInt(index_delete);
    });

    this.setState(
      {
        socials: newList,
        social_media: newSocialMedia,
        num_social: this.state.num_social - 1,
      },
      () => {},
    );
  }

  handleTambahLinkAction() {
    if (this.state.num_link < this.state.max_link) {
      const current_index = this.state.links_last_index + 1;
      this.setState({
        links: [
          ...this.state.links,
          <Link
            index={current_index}
            key={current_index}
            deleteCallBack={this.handleDeleteCallBackLink}
            inputCallBack={this.handleCallBackLink}
            name={false}
            link_member={false}
            link_public={false}
            image={false}
            deksripsi={false}
          />,
        ],
        num_link: this.state.num_link + 1,
        links_last_index: current_index,
      });
    } else {
      swal
        .fire({
          title: "Maksimal Pilihan hanya " + this.state.max_link,
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleTambahTelpAction() {
    if (this.state.num_telp < this.state.max_telp) {
      const current_index = this.state.telp_last_index + 1;
      this.setState({
        telp: [
          ...this.state.telp,
          <Telp
            index={current_index}
            key={current_index}
            deleteCallBack={this.handleDeleteCallBackTelp}
            inputCallBack={this.handleCallBackTelp}
            nomor={false}
          />,
        ],
        num_telp: this.state.num_telp + 1,
        telp_last_index: current_index,
      });
    } else {
      swal
        .fire({
          title: "Maksimal Pilihan hanya " + this.state.max_telp,
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleDeleteCallBackLinkAction(e) {
    const index_delete = e.index_delete;
    const newList = this.state.links.filter(
      (item) => item.key !== index_delete,
    );

    const newExternal = this.state.external_links.filter((item) => {
      return parseInt(item.key) !== parseInt(index_delete);
    });

    this.setState(
      {
        links: newList,
        external_links: newExternal,
        num_link: this.state.num_link - 1,
      },
      () => {},
    );
  }

  handleChangeHeaderAction(e) {
    const imageFile = e.target.files[0];
    const reader = new FileReader();
    resizeFile(imageFile).then((image) => {
      reader.readAsDataURL(image);
      const context = this;
      reader.onload = function () {
        let result = reader.result;
        context.setState({ image_header: result });
      };
    });
  }

  handleChangeFooterAction(e) {
    const imageFile = e.target.files[0];
    const reader = new FileReader();
    resizeFile(imageFile).then((image) => {
      reader.readAsDataURL(image);
      const context = this;
      reader.onload = function () {
        let result = reader.result;
        context.setState({ image_footer: result });
      };
    });
  }

  handleChangeDescriptionAction(value) {
    const errors = this.state.errors;
    errors["deskripsi"] = "";

    const description = value;
    this.setState({
      description,
      errors,
    });
  }

  handleChangeSyaratKetentuanAction(value) {
    const errors = this.state.errors;
    errors["syaratketentuan"] = "";

    const syaratketentuan = value;
    this.setState({
      syaratketentuan,
      errors,
    });
  }

  handleChangeKebijakanPrivasiAction(value) {
    const errors = this.state.errors;
    errors["kebijakanprivasi"] = "";

    const kebijakanprivasi = value;
    this.setState({
      kebijakanprivasi,
      errors,
    });
  }

  handleChangeAlamatAction(e) {
    const errors = this.state.errors;
    errors["alamat"] = "";

    const alamat = e.currentTarget.value;
    this.setState({
      alamat,
      errors,
    });
  }

  handleChangeEmailAction(e) {
    const errors = this.state.errors;
    errors["email"] = "";

    const email = e.currentTarget.value;
    this.setState({
      email,
      errors,
    });
  }

  handleChangeKoordinatAction(e) {
    const text_koordinat = e.currentTarget.value;

    this.setState({
      text_koordinat,
    });
  }

  handleCallBackSocialMediaAction(e) {
    const name = e.name;
    const key = e.key;
    const link = e.link;
    const image = e.image;
    const icon = e.icon;
    const social_media = [];
    //if edit
    let is_add = true;
    this.state.social_media.forEach(function (element, i) {
      if (element.key == key) {
        element.name = name;
        element.link_social_media = link;
        element.image_logo = image;
        element.icon = icon;
        is_add = false;
      }
      social_media.push(element);
    });
    if (is_add) {
      social_media.push({
        image_logo: image,
        name: name,
        link_social_media: link,
        key: key,
        icon: icon,
      });
    }
    this.setState({ social_media }, () => {});
  }

  handleCallBackLinkAction(e) {
    const name = e.name;
    const key = e.key;
    const link_public = e.link_public;
    const link_member = e.link_member;
    const image = e.image;
    const deskripsi = e.deskripsi;
    const external_links = [];
    //if edit
    let is_add = true;

    this.state.external_links.forEach(function (element, i) {
      if (element.key == key) {
        element.nama_platform = name;
        element.link_public = link_public;
        element.link_member = link_member;
        element.icon = image;
        element.deskripsi = deskripsi;
        is_add = false;
      }
      external_links.push(element);
    });
    if (is_add) {
      external_links.push({
        nama_platform: name,
        link_public: link_public,
        link_member: link_member,
        icon: image,
        deskripsi: deskripsi,
        key: key,
      });
    }
    this.setState({ external_links }, () => {});
  }

  handleCallBackTelpAction(e) {
    const nomor = e.nomor;
    const key = e.key;
    //if edit
    let is_add = true;
    const telps_arr = [];
    this.state.telps_arr.forEach(function (element, i) {
      if (element.key == key) {
        element.nomor = nomor;
        is_add = false;
      }
      telps_arr.push(element);
    });
    if (is_add) {
      telps_arr.push({
        nomor: nomor,
        key: key,
      });
    }
    this.setState({ telps_arr }, () => {});
  }

  handleChangePrimaryAction(val) {
    this.setState({
      valPrimary: {
        label: val.label,
        value: val.value,
      },
      primary: val.value,
    });
  }

  handleDeleteCallBackTelpAction(e) {
    const index_delete = e.index_delete;
    const newList = this.state.telp.filter((item) => item.key !== index_delete);

    const newTelp = this.state.telps_arr.filter((item) => {
      return parseInt(item.key) !== parseInt(index_delete);
    });

    this.setState(
      {
        telp: newList,
        telps_arr: newTelp,
        num_telp: this.state.num_telp - 1,
      },
      () => {},
    );
  }

  handleChangeSecondaryAction(val) {
    this.setState({
      valSecondary: {
        label: val.label,
        value: val.value,
      },
      secondary: val.value,
    });
  }

  handleChangeExtrasAction(val) {
    this.setState({
      valExtras: {
        label: val.label,
        value: val.value,
      },
      extras: val.value,
    });
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  General
                </h1>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Setting
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <form
                        className="form"
                        action="#"
                        onSubmit={this.handleSubmit}
                      >
                        <input
                          type="hidden"
                          name="csrf-token"
                          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                        />

                        <div className="row">
                          <div className="col-lg-3 col-md-3 col-xs-3">
                            <div className="form-group fv-row mb-7">
                              <label className="form-label">Logo Header</label>
                              <ImageCard image={this.state.image_header} />
                              <input
                                type="file"
                                className="form-control form-control-sm font-size-h4"
                                name="logo_header"
                                id="thumbnail"
                                accept=".png,.jpg,.jpeg,.svg"
                                onChange={this.handleChangeHeader}
                              />
                              {/* <small className="text-muted">Maksimal Ukuran File 5MB</small> */}
                              <div>
                                <span style={{ color: "red" }}>
                                  {this.state.errors["logo_header"]}
                                </span>
                              </div>
                            </div>
                          </div>
                          <div className="col-lg-3 col-md-3 col-xs-3">
                            <div className="form-group fv-row mb-7">
                              <label className="form-label">Logo Footer</label>
                              <ImageCard image={this.state.image_footer} />
                              <input
                                type="file"
                                className="form-control form-control-sm font-size-h4"
                                name="logo_footer"
                                id="thumbnail"
                                accept=".png,.jpg,.jpeg,.svg"
                                onChange={this.handleChangeFooter}
                              />
                              {/* <small className="text-muted">Maksimal Ukuran File 5MB</small> */}
                              <div>
                                <span style={{ color: "red" }}>
                                  {this.state.errors["logo_footer"]}
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label">Deskripsi</label>
                          <CKEditor
                            editor={Editor}
                            name="isi"
                            onReady={(editor) => {
                              // You can store the "editor" and use when it is needed.
                            }}
                            config={{
                              ckfinder: {
                                // Upload the images to the server using the CKFinder QuickUpload command.
                                uploadUrl:
                                  process.env.REACT_APP_BASE_API_URI +
                                  "/publikasi/ckeditor-upload-image",
                              },
                            }}
                            onChange={(event, editor) => {
                              const data = editor.getData();
                              this.handleChangeDescription(data);
                            }}
                            onBlur={(event, editor) => {}}
                            onFocus={(event, editor) => {}}
                            data={this.state.description}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["deskripsi"]}
                          </span>
                        </div>

                        <h2 className="mt-5">Social Media</h2>

                        {this.state.socials}

                        <div className="col-lg-12 text-end mt-5">
                          <a
                            onClick={this.handleTambahSocialMedia}
                            className="btn btn-info btn-sm "
                          >
                            <i className="las la-plus"></i>
                            Tambah Social Media
                          </a>
                        </div>

                        <h2 className="mt-5">Platform Shortcut</h2>

                        {this.state.links}

                        <div className="col-lg-12 text-end mt-5">
                          <a
                            onClick={this.handleTambahLink}
                            className="btn btn-info btn-sm "
                          >
                            <i className="las la-plus"></i>
                            Platform Shortcut
                          </a>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">
                            Alamat Lengkap
                          </label>
                          <textarea
                            className="form-control form-control-sm"
                            placeholder="Masukkan Alamat Lengkap Disini"
                            name="alamat"
                            rows="5"
                            value={this.state.alamat}
                            onChange={this.handleChangeAlamat}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["alamat"]}
                          </span>
                        </div>

                        <h2 className="mt-5">Telepon</h2>

                        {this.state.telp}

                        <div className="col-lg-12 text-end mt-5">
                          <a
                            onClick={this.handleTambahTelp}
                            className="btn btn-info btn-sm "
                          >
                            <i className="las la-plus"></i>
                            Tambah Telepon
                          </a>
                        </div>

                        {
                          <div className="col-lg-12 mb-7 fv-row">
                            <label className="form-label required">Email</label>
                            <input
                              className="form-control form-control-sm"
                              placeholder="Masukkan Email Disini"
                              name="email"
                              value={this.state.email}
                              type="email"
                              onChange={this.handleChangeEmail}
                            />
                            <span style={{ color: "red" }}>
                              {this.state.errors["email"]}
                            </span>
                          </div>
                        }

                        {
                          <div className="col-lg-12 mb-7 fv-row">
                            <label className="form-label required">
                              Koordinat (Latitude, Longitude)
                            </label>
                            <input
                              className="form-control form-control-sm"
                              placeholder="Masukkan Koordinat Disini"
                              name="koordinat"
                              value={this.state.text_koordinat}
                              onChange={this.handleChangeKoordinat}
                            />
                            <span style={{ color: "red" }}>
                              {this.state.errors["email"]}
                            </span>
                          </div>
                        }

                        <h2 className="mt-5 mb-3">Color Schemes</h2>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">Primary</label>
                          <Select
                            id="primary"
                            name="primary"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.errors
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            value={this.state.valPrimary}
                            onChange={this.handleChangePrimary}
                            options={this.color_schemes}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["primary"]}
                          </span>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">
                            Secondary
                          </label>
                          <Select
                            id="secondary"
                            name="secondary"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.errors
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            value={this.state.valSecondary}
                            onChange={this.handleChangeSecondary}
                            options={this.color_schemes}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["secondary"]}
                          </span>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">Extras</label>
                          <Select
                            id="extras"
                            name="extras"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.errors
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            value={this.state.valExtras}
                            onChange={this.handleChangeExtras}
                            options={this.color_schemes}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["extras"]}
                          </span>
                        </div>

                        <h2 className="mt-5 mb-3">
                          Syarat Ketentuan dan Kebijakan Privasi
                        </h2>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">
                            Syarat Ketentuan
                          </label>
                          <CKEditor
                            editor={Editor}
                            name="syaratketentuan"
                            onReady={(editor) => {
                              // You can store the "editor" and use when it is needed.
                            }}
                            config={{
                              ckfinder: {
                                // Upload the images to the server using the CKFinder QuickUpload command.
                                uploadUrl:
                                  process.env.REACT_APP_BASE_API_URI +
                                  "/publikasi/ckeditor-upload-image",
                              },
                            }}
                            onChange={(event, editor) => {
                              const data = editor.getData();
                              this.handleChangeSyaratKetentuan(data);
                            }}
                            onBlur={(event, editor) => {}}
                            onFocus={(event, editor) => {}}
                            data={this.state.syaratketentuan}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["syaratketentuan"]}
                          </span>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">
                            Kebijakan Privasi
                          </label>
                          <CKEditor
                            editor={Editor}
                            name="kebijakanprivasi"
                            onReady={(editor) => {
                              // You can store the "editor" and use when it is needed.
                            }}
                            config={{
                              ckfinder: {
                                // Upload the images to the server using the CKFinder QuickUpload command.
                                uploadUrl:
                                  process.env.REACT_APP_BASE_API_URI +
                                  "/publikasi/ckeditor-upload-image",
                              },
                            }}
                            onChange={(event, editor) => {
                              const data = editor.getData();
                              this.handleChangeKebijakanPrivasi(data);
                            }}
                            onBlur={(event, editor) => {}}
                            onFocus={(event, editor) => {}}
                            data={this.state.kebijakanprivasi}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["kebijakanprivasi"]}
                          </span>
                        </div>

                        <div className="row my-7">
                          <button
                            type="submit"
                            className="btn btn-primary btn-md "
                            disabled={this.state.isLoading}
                          >
                            {this.state.isLoading ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm me-2"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Loading...</span>
                                Loading...
                              </>
                            ) : (
                              <>
                                <i className="fa fa-paper-plane me-1"></i>Simpan
                              </>
                            )}
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
