import React from "react";
import ImageCard from "./ImageCard";
import imageCompression from "browser-image-compression";
import FontAwesomeIcon from "../../assets/fontawesome-icon";
import Select from "react-select";

const resizeFile = async (file) => {
  const options = {
    maxSizeMB: 5,
    maxWidthOrHeight: 300,
    useWebWorker: true,
  };

  return imageCompression(file, options);
};

export default class SocialMedia extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickDelete = this.handleClickDeleteAction.bind(this);
    this.handleChangeNama = this.handleChangeNamaAction.bind(this);
    this.handleChangeLink = this.handleChangeLinkAction.bind(this);
    this.handleChangeImage = this.handleChangeImageAction.bind(this);
    this.handleChangeIcon = this.handleChangeIconAction.bind(this);
    this.state = {
      index: this.props.index,
      errors: {},
      name: this.props.name ? this.props.name : "",
      link: this.props.link ? this.props.link : "",
      //image: this.props.image_logo,
      image: false,
      iconOption: [],
      icon: this.props.icon ? this.props.icon : [],
    };
  }

  componentDidMount() {
    const iconOption = FontAwesomeIcon.map((icon) => ({
      label: icon,
      value: icon,
    }));

    this.setState({ iconOption });
  }
  handleClickDeleteAction(e) {
    const index = e.currentTarget.getAttribute("index");
    const callBackVal = {
      index_delete: index,
    };
    this.props.deleteCallBack(callBackVal);
  }

  handleChangeNamaAction(e) {
    const name = e.currentTarget.value;
    this.setState({
      name: name,
    });
    const callBackVal = {
      name: name,
      link: this.state.link,
      key: this.state.index,
      image: this.state.image,
      icon: this.state.icon,
    };
    this.props.inputCallBack(callBackVal);
  }

  handleChangeLinkAction(e) {
    const link = e.currentTarget.value;
    this.setState({
      link: link,
    });
    const callBackVal = {
      name: this.state.name,
      link: link,
      key: this.state.index,
      image: this.state.image,
      icon: this.state.icon,
    };
    this.props.inputCallBack(callBackVal);
  }

  handleChangeImageAction(e) {
    const imageFile = e.target.files[0];
    const reader = new FileReader();
    resizeFile(imageFile).then((image) => {
      reader.readAsDataURL(image);
      const context = this;
      reader.onload = function () {
        let result = reader.result;
        context.setState({ image: result });
        const callBackVal = {
          name: context.state.name,
          link: context.state.link,
          key: context.state.index,
          image: result,
          icon: this.state.icon,
        };
        context.props.inputCallBack(callBackVal);
      };
    });
  }

  handleChangeIconAction(e) {
    const icon = e;
    this.setState({
      icon,
    });
    const callBackVal = {
      name: this.state.name,
      link: this.state.link,
      key: this.state.index,
      image: this.state.image,
      icon: icon,
    };
    this.props.inputCallBack(callBackVal);
  }

  render() {
    return (
      <div className="row bg-gray-200 rounded-sm p-5 mt-3">
        <div className="row">
          <div className="col-lg-3 form-group fv-row mb-7">
            <label className="form-label required">Icon Widget</label>
            <Select
              name="icon"
              placeholder="Silahkan pilih"
              noOptionsMessage={({ inputValue }) =>
                !inputValue ? this.state.datax : "Data tidak tersedia"
              }
              className="form-select-sm selectpicker p-0"
              options={this.state.iconOption}
              onChange={this.handleChangeIcon}
              value={this.state.icon}
              getOptionLabel={(e) => (
                <div style={{ display: "flex", alignItems: "center" }}>
                  <span>
                    <i className={`${e.value} fa-2x text-primary`}></i>
                  </span>
                  <span style={{ marginLeft: 5 }}>{e.label}</span>
                </div>
              )}
            />
            <span style={{ color: "red" }}>{this.state.errors["icon"]}</span>
          </div>
          <div className="col-lg-3   mb-7 fv-row">
            <label className="form-label required">Nama Social Media</label>
            <input
              className="form-control form-control-sm name_social_media"
              placeholder="Masukkan Nama Disini"
              name="name"
              index={this.state.index}
              value={this.state.name}
              onChange={this.handleChangeNama}
            />
            <span
              className="error_name_socmed"
              style={{ color: "red" }}
              index={this.state.index}
            >
              {this.state.errors["name"]}
            </span>
          </div>
          <div className="col-lg-4 mb-7 fv-row">
            <label className="form-label required">Link Social Media</label>
            <input
              className="form-control form-control-sm link_social_media"
              placeholder="Masukkan Link Disini"
              name="link"
              index={this.state.index}
              value={this.state.link}
              onChange={this.handleChangeLink}
            />
            <span
              className="error_social_media"
              style={{ color: "red" }}
              index={this.state.index}
            >
              {this.state.errors["link"]}
            </span>
          </div>

          <div className="col-lg-1 pt-7 mb-7 fv-row">
            <a
              title="Hapus"
              onClick={this.handleClickDelete}
              index={this.props.index}
              className="btn btn-icon btn-danger w-40px h-40px"
            >
              <span className="svg-icon svg-icon-3">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={34}
                  height={34}
                  viewBox="0 0 24 24"
                  fill="none"
                >
                  <path
                    d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z"
                    fill="black"
                  />
                  <path
                    opacity="0.5"
                    d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z"
                    fill="black"
                  />
                  <path
                    opacity="0.5"
                    d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z"
                    fill="black"
                  />
                </svg>
              </span>
            </a>
          </div>
        </div>
      </div>
    );
  }
}
