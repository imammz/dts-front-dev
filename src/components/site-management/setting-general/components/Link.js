import React from "react";
import ImageCard from "../../setting-page/components/ImageCard";
import imageCompression from "browser-image-compression";

const resizeFile = async (file) => {
  const options = {
    maxSizeMB: 5,
    maxWidthOrHeight: 300,
    useWebWorker: true,
  };

  return imageCompression(file, options);
};

export default class Link extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickDelete = this.handleClickDeleteAction.bind(this);
    this.handleChangeNama = this.handleChangeNamaAction.bind(this);
    this.handleChangeImage = this.handleChangeImageAction.bind(this);
    this.handleChangeDeskripsi = this.handleChangeDeskripsiAction.bind(this);
    this.handleChangeLinkPublic = this.handleChangeLinkPublicAction.bind(this);
    this.handleChangeLinkMember = this.handleChangeLinkMemberAction.bind(this);

    this.state = {
      errors: {},
      index: this.props.index,
      nama: this.props.name ? this.props.name : "",
      link_public: this.props.link_public ? this.props.link_public : "",
      link_member: this.props.link_member ? this.props.link_member : "",
      image: this.props.image,
      deskripsi: this.props.deskripsi ? this.props.deskripsi : "",
    };
  }
  handleClickDeleteAction(e) {
    const index = e.currentTarget.getAttribute("index");
    const callBackVal = {
      index_delete: index,
    };
    this.props.deleteCallBack(callBackVal);
  }

  handleChangeNamaAction(e) {
    const nama = e.currentTarget.value;
    this.setState({
      nama: nama,
    });

    const callBackVal = {
      name: nama,
      link_public: this.state.link_public,
      link_member: this.state.link_member,
      key: this.state.index,
      image: this.state.image,
      deskripsi: this.state.deskripsi,
    };
    this.props.inputCallBack(callBackVal);
  }

  handleChangeLinkPublicAction(e) {
    const link_public = e.currentTarget.value;
    this.setState({
      link_public: link_public,
    });
    const callBackVal = {
      name: this.state.nama,
      link_public: link_public,
      link_member: this.state.link_member,
      key: this.state.index,
      image: this.state.image,
      deskripsi: this.state.deskripsi,
    };
    this.props.inputCallBack(callBackVal);
  }

  handleChangeLinkMemberAction(e) {
    const link_member = e.currentTarget.value;
    this.setState({
      link_member: link_member,
    });
    const callBackVal = {
      name: this.state.nama,
      link_public: this.state.link_public,
      link_member: link_member,
      key: this.state.index,
      image: this.state.image,
      deskripsi: this.state.deskripsi,
    };
    this.props.inputCallBack(callBackVal);
  }

  handleChangeImageAction(e) {
    const imageFile = e.target.files[0];
    const reader = new FileReader();
    resizeFile(imageFile).then((image) => {
      reader.readAsDataURL(image);
      const context = this;
      reader.onload = function () {
        let result = reader.result;
        context.setState({ image: result });
        const callBackVal = {
          name: context.state.nama,
          link_public: context.state.link_public,
          link_member: context.state.link_member,
          key: context.state.index,
          image: result,
          deskripsi: context.state.deskripsi,
        };
        context.props.inputCallBack(callBackVal);
      };
    });
  }

  handleChangeDeskripsiAction(e) {
    const deskripsi = e.currentTarget.value;
    this.setState({
      deskripsi,
    });

    const callBackVal = {
      name: this.state.nama,
      link_public: this.state.link_public,
      link_member: this.state.link_member,
      key: this.state.index,
      image: this.state.image,
      deskripsi: deskripsi,
    };
    this.props.inputCallBack(callBackVal);
  }

  render() {
    return (
      <div className="row bg-gray-200 rounded-sm p-5 mt-3">
        <div className="row">
          <div className="col-md-3 col-lg-3 mt-3">
            <div className="col-lg-12 mb-7 fv-row">
              <label className="form-label">Icon</label>
              <ImageCard image={this.state.image} />
              <input
                type="file"
                className="form-control form-control-sm font-size-h4"
                name="icon"
                id="thumbnail"
                accept=".png"
                onChange={this.handleChangeImage}
              />
              <div>
                <span style={{ color: "red" }}>
                  {this.state.errors["logo_header"]}
                </span>
              </div>
            </div>
          </div>
          <div className="col-md-8 col-lg-8 mt-3">
            <div className="col-lg-12 mb-7 fv-row ms-4 pe-7">
              <label className="form-label required">Nama Platform</label>
              <input
                className="form-control form-control-sm nama_platform"
                placeholder="Masukkan Nama Disini"
                name="nama"
                index={this.state.index}
                value={this.state.nama}
                onChange={this.handleChangeNama}
              />
              <span
                className="error_nama_platform"
                style={{ color: "red" }}
                index={this.state.index}
              >
                {this.state.errors["nama"]}
              </span>
            </div>

            <div className="col-lg-12 mb-7 fv-row ms-4 pe-7">
              <label className="form-label required">Deskripsi</label>
              <input
                className="form-control form-control-sm deskripsi_platform"
                placeholder="Masukkan Deskripsi Disini"
                name="deskripsi"
                index={this.state.index}
                value={this.state.deskripsi}
                onChange={this.handleChangeDeskripsi}
              />
              <span
                className="error_platform_deskripsi"
                style={{ color: "red" }}
                index={this.state.index}
              >
                {this.state.errors["deskripsi"]}
              </span>
            </div>

            <div className="col-lg-12 col-md-12 mb-7 fv-row row ms-1">
              <div className="col-lg-6 mb-7 fv-row">
                <label className="form-label required">Link Public</label>
                <input
                  className="form-control form-control-sm link_public"
                  placeholder="Masukkan Link Public Disini"
                  name="link_public"
                  index={this.state.index}
                  value={this.state.link_public}
                  onChange={this.handleChangeLinkPublic}
                />
                <span
                  style={{ color: "red" }}
                  className="error_link_public"
                  index={this.state.index}
                >
                  {this.state.errors["link_public"]}
                </span>
              </div>
              <div className="col-lg-6 mb-7 fv-row">
                <label className="form-label required">Link Member</label>
                <input
                  className="form-control form-control-sm link_member"
                  placeholder="Masukkan Link Member Disini"
                  name="link_member"
                  index={this.state.index}
                  value={this.state.link_member}
                  onChange={this.handleChangeLinkMember}
                />
                <span
                  style={{ color: "red" }}
                  className="error_link_member"
                  index={this.state.index}
                >
                  {this.state.errors["link_member"]}
                </span>
              </div>
            </div>
          </div>
          <div className="col-lg-1 col-md-1 col-xs-1 mt-20 pt-10">
            <div className="mt-7 fv-row">
              <a
                title="Hapus"
                onClick={this.handleClickDelete}
                index={this.props.index}
                className="btn btn-icon btn-danger w-40px h-40px"
              >
                <span className="svg-icon svg-icon-3">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={34}
                    height={34}
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <path
                      d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z"
                      fill="black"
                    />
                    <path
                      opacity="0.5"
                      d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z"
                      fill="black"
                    />
                    <path
                      opacity="0.5"
                      d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z"
                      fill="black"
                    />
                  </svg>
                </span>
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
