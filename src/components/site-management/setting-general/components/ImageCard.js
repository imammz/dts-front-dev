import React from "react";
import { Card, CardMedia } from "@material-ui/core";

export default class ImageCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      image: this.props.image,
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.image !== this.props.image) {
      this.setState({
        image: this.props.image,
      });
    }
  }

  render() {
    return (
      <div>
        <Card className="mt-3 mb-3">
          <div style={{ position: "relative" }}>
            {this.state.image ? (
              <CardMedia
                component="img"
                style={{
                  height: 140,
                  boxShadow: "rgba(149, 157, 165, 0.6) 0px 8px 24px",
                }}
                src={this.state.image}
                title="Image"
              />
            ) : (
              <div
                className="text-center pt-20"
                style={{
                  height: 140,
                  boxShadow: "rgba(149, 157, 165, 0.6) 0px 8px 24px",
                }}
              >
                <h3
                  style={{
                    color: "#bdc3c7",
                  }}
                >
                  Belum Ada Gambar
                </h3>
              </div>
            )}
          </div>
        </Card>
      </div>
    );
  }
}
