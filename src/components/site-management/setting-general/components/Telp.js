import React from "react";

export default class Telp extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickDelete = this.handleClickDeleteAction.bind(this);
    this.handleChangeNomor = this.handleChangeNomorAction.bind(this);
    this.state = {
      index: this.props.index,
      errors: {},
      nomor: this.props.nomor ? this.props.nomor : "",
    };
  }

  handleClickDeleteAction(e) {
    const index = e.currentTarget.getAttribute("index");
    const callBackVal = {
      index_delete: index,
    };
    this.props.deleteCallBack(callBackVal);
  }

  handleChangeNomorAction(e) {
    const nomor = e.currentTarget.value;
    this.setState({
      nomor: nomor,
    });
    const callBackVal = {
      nomor: nomor,
      key: this.state.index,
    };
    this.props.inputCallBack(callBackVal);
  }

  render() {
    return (
      <div className="row bg-gray-200 rounded-sm p-5 mt-3">
        <div className="col-lg-10 col-md-10 col-xs-2">
          <label className="form-label required">Nomor Telepon</label>
          <input
            className="form-control form-control-sm nomor_telp"
            placeholder="Masukkan Nomor Telepon Disini"
            name="name"
            index={this.state.index}
            value={this.state.nomor}
            onChange={this.handleChangeNomor}
            type="number"
            autoComplete="off"
          />
          <span
            className="error_nomor_telp"
            style={{ color: "red" }}
            index={this.state.index}
          >
            {this.state.errors["name"]}
          </span>
        </div>
        <div className="col-lg-2 col-md-2 col-xs-2 mt-6">
          <a
            title="Hapus"
            onClick={this.handleClickDelete}
            index={this.props.index}
            className="btn btn-icon btn-danger w-40px h-40px"
          >
            <span className="svg-icon svg-icon-3">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width={34}
                height={34}
                viewBox="0 0 24 24"
                fill="none"
              >
                <path
                  d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z"
                  fill="black"
                />
                <path
                  opacity="0.5"
                  d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z"
                  fill="black"
                />
                <path
                  opacity="0.5"
                  d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z"
                  fill="black"
                />
              </svg>
            </span>
          </a>
        </div>
      </div>
    );
  }
}
