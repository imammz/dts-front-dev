import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../SettingGeneral-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const SettingGeneral = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default SettingGeneral;
