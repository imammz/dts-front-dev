import React, { useState, useEffect, useMemo, useRef } from "react";
import AdministratorService from "../../../service/AdministratorService";
import { useTable } from "react-table";
import { GlobalFilter, DefaultFilterForColumn } from "../../Filter";
import Header from "../../Header";
import SideNav from "../../SideNav";
import Footer from "../../Footer";
import { useHistory, useNavigate, useParams } from "react-router-dom";
import {
  useFilters,
  useGlobalFilter,
} from "react-table/dist/react-table.development";

const AdministratorList = (props) => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  const [Administrator, setAdministrator] = useState([]);
  const [searchTitle, setSearchTitle] = useState("");
  const AdministratorRef = useState([]);
  AdministratorRef.current = Administrator;
  const history = useNavigate();
  useEffect(() => {
    retrieveAdministrator();
  }, []);

  const onChangeSearchTitle = (e) => {
    console.log(e);
    const searchTitle = e.target.value;
    setSearchTitle(searchTitle);
  };
  const retrieveAdministrator = () => {
    AdministratorService.getAll()
      .then((response) => {
        console.log(response.data);
        setAdministrator(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const refreshList = () => {
    retrieveAdministrator();
  };
  const removeAllAdministrator = () => {
    AdministratorService.removeAll()
      .then((response) => {
        console.log(response);
        refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const deleteAdministrator = (rowIndex) => {
    const id = AdministratorRef.current[rowIndex].id;
    AdministratorService.remove(id)
      .then((response) => {
        window.location.href = "";
        let newAdministrator = [...AdministratorRef.current];
        newAdministrator.splice(rowIndex, 1);
        setAdministrator(newAdministrator);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const handleShow = (cell) => {
    console.log(cell?.row?.original);
    console.log(cell?.row?.original.name);
    history("/pelatihan/Administrator/content/" + cell?.row?.original.id);
  };
  const columns = useMemo(
    () => [
      {
        Header: "No",
        accessor: "",
        className:
          "text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0",
      },
      {
        Header: "Nama Lengkap",
        accessor: "name",
        className:
          "text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0",
        sortable: true,
      },
      {
        Header: "Email",
        accessor: "email",
        className:
          "text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0",
      },
      {
        Header: "Role",
        accessor: "nomor_hp",
        className: "min-w-100px",
      },
      {
        Title: "status",
        accessor: "status",
        className: "min-w-100px",
        Cell: (props) => {
          return props.value ? "Published" : "Pending";
        },
      },
      {
        Header: "Actions",
        accessor: "actions",
        Cell: (props) => {
          const rowIdx = props.row.nama;
          return (
            <div>
              <span onClick={() => handleShow(props)}>
                <i className="far fa-edit action mr-2"></i>
              </span>
              <span onClick={() => deleteAdministrator(rowIdx)}>
                <i className="fas fa-trash action"></i>
              </span>
            </div>
          );
        },
      },
    ],
    [],
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    state,
    prepareRow,
    setGlobalFilter,
    preGlobalFilteredRows,
  } = useTable(
    {
      columns,
      data: Administrator,
      defaultColumn: { Filter: DefaultFilterForColumn },
    },
    useFilters,
    useGlobalFilter,
  );
  return (
    <div>
      <Header />
      <SideNav />
      <div className="toolbar" id="kt_toolbar">
        <div
          id="kt_toolbar_container"
          className="container-fluid d-flex flex-stack"
        >
          <div
            data-kt-swapper="true"
            data-kt-swapper-mode="prepend"
            data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
            className="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0"
          >
            <h1 className="d-flex align-items-center text-dark fw-bolder fs-3 my-1">
              {urlSegmentZero}
            </h1>
            <span className="h-20px border-gray-200 border-start mx-4" />
            <ul className="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
              <li className="breadcrumb-item text-muted">
                <a
                  href="../../demo1/dist/index.html"
                  className="text-muted text-hover-primary"
                >
                  {urlSegmentZero}
                </a>
              </li>
              <li className="breadcrumb-item">
                <span className="bullet bg-gray-200 w-5px h-2px" />
              </li>
              <li className="breadcrumb-item text-muted">{urlSegmenttOne}</li>
              <li className="breadcrumb-item">
                <span className="bullet bg-gray-200 w-5px h-2px" />
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div
        className="wrapper d-flex flex-column flex-row-fluid"
        id="kt_wrapper"
      >
        <div
          className="content d-flex flex-column flex-column-fluid"
          id="kt_content"
        >
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container-xxl">
              <div className="card card-flush">
                <div className="card-header mt-6">
                  <div className="card-title">
                    <div className="card-body pt-0">
                      <div className="col-md-5">
                        <a
                          href="/site-management/administrator/add"
                          className="btn btn-light-primary"
                        >
                          {/*begin::Svg Icon | path: icons/duotune/general/gen035.svg*/}
                          <span className="svg-icon svg-icon-3">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width={24}
                              height={24}
                              viewBox="0 0 24 24"
                              fill="none"
                            >
                              <rect
                                opacity="0.3"
                                x={2}
                                y={2}
                                width={20}
                                height={20}
                                rx={5}
                                fill="black"
                              />
                              <rect
                                x="10.8891"
                                y="17.8033"
                                width={12}
                                height={2}
                                rx={1}
                                transform="rotate(-90 10.8891 17.8033)"
                                fill="black"
                              />
                              <rect
                                x="6.01041"
                                y="10.9247"
                                width={12}
                                height={2}
                                rx={1}
                                fill="black"
                              />
                            </svg>
                          </span>
                          {/*end::Svg Icon*/}Tambah Administrator
                        </a>
                      </div>
                      <br />
                      <h1>List Administrator</h1>
                      <hr />
                      <div className="col-md-4">
                        <div className="input-group mb-3">
                          <GlobalFilter
                            preGlobalFilteredRows={preGlobalFilteredRows}
                            globalFilter={state.globalFilter}
                            setGlobalFilter={setGlobalFilter}
                          />
                        </div>
                      </div>
                      <div className="card-body">
                        <table
                          className="table align-middle table-row-dashed fs-6 gy-5 mb-0"
                          {...getTableProps()}
                        >
                          <thead>
                            {headerGroups.map((headerGroup) => (
                              <tr {...headerGroup.getHeaderGroupProps()}>
                                {headerGroup.headers.map((column) => (
                                  <th {...column.getHeaderProps()}>
                                    {column.render("Header")}
                                  </th>
                                ))}
                              </tr>
                            ))}
                          </thead>
                          <tbody
                            className="fw-bold text-gray-600"
                            {...getTableBodyProps()}
                          >
                            {rows.map((row, i) => {
                              prepareRow(row);
                              return (
                                <tr {...row.getRowProps()}>
                                  {row.cells.map((cell) => {
                                    return (
                                      <td {...cell.getCellProps()}>
                                        {cell.render("Cell")}
                                      </td>
                                    );
                                  })}
                                </tr>
                              );
                            })}
                          </tbody>
                        </table>
                      </div>
                      <div className="col-md-8">
                        <button
                          className="btn btn-sm btn-danger"
                          onClick={removeAllAdministrator}
                        >
                          Remove All
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </div>

    // <Footer/>
  );
};

export default AdministratorList;
