import React, { useEffect, useState } from "react";
import AdministratorService from "../../../service/AdministratorService";
import Header from "../../Header";
import SideNav from "../../SideNav";
import Footer from "../../Footer";
import axios from "axios";
const AddAdministrator = () => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  const initialAdministratorState = {
    id: null,
    title: "",
    description: "",
    published: false,
  };
  const [Administrator, setAdministrator] = useState(initialAdministratorState);
  const [submitted, setSubmitted] = useState(false);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setAdministrator({ ...Administrator, [name]: value });
  };

  const retrieveRoleAdministrator = () => {
    AdministratorService.lvRole()
      .then((response) => {
        console.log(response.data);
        setAdministrator(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const saveAdministrator = () => {
    var data = {
      title: Administrator.title,
      description: Administrator.description,
    };
    AdministratorService.create(data)
      .then((response) => {
        setAdministrator({
          id: response.data.id,
          title: response.data.title,
          description: response.data.description,
          published: response.data.published,
        });
        setSubmitted(true);
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const newAdministrator = () => {
    setAdministrator(initialAdministratorState);
    setSubmitted(false);
  };

  return (
    <div>
      <Header />
      <SideNav />
      <div
        className="wrapper d-flex flex-column flex-row-fluid"
        id="kt_wrapper"
        style={{ paddingTop: 8 }}
      >
        <div
          className="content d-flex flex-column flex-column-fluid"
          id="kt_content"
        >
          <div className="toolbar" id="kt_toolbar">
            <div
              id="kt_toolbar_container"
              className="container-fluid d-flex flex-stack"
            >
              <div
                data-kt-swapper="true"
                data-kt-swapper-mode="prepend"
                data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                className="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0"
              >
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-3 my-1">
                  {urlSegmentZero}
                </h1>
                <span className="h-20px border-gray-200 border-start mx-4" />
                <ul className="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                  <li className="breadcrumb-item text-muted">
                    <a
                      href="../../demo1/dist/index.html"
                      className="text-muted text-hover-primary"
                    >
                      {urlSegmentZero}
                    </a>
                  </li>
                  <li className="breadcrumb-item">
                    <span className="bullet bg-gray-200 w-5px h-2px" />
                  </li>
                  <li className="breadcrumb-item text-muted">
                    {urlSegmenttOne}
                  </li>
                  <li className="breadcrumb-item">
                    <span className="bullet bg-gray-200 w-5px h-2px" />
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container-xxl">
              <div className="card card-flush">
                <div className="mt-6">
                  <div className="card-title">
                    <div className="card-body pt-0">
                      <div>
                        <h1>Tambah Data Administrator</h1>
                        <hr />
                        <form>
                          <div className="form-group fv-row mb-12">
                            <label className="fs-6 fw-bold form-label mb-2">
                              <span className="required">Nama Lengkap</span>
                            </label>
                            <input
                              className="form-control form-control-solid"
                              placeholder="Masukkan Nama Lengkap"
                              name="kode"
                            />
                          </div>
                          <div className="form-group fv-row mb-12">
                            <label className="fs-6 fw-bold form-label mb-2">
                              <span className="required">Email</span>
                            </label>
                            <input
                              className="form-control form-control-solid"
                              placeholder="Masukkan Email"
                              name="kode"
                            />
                          </div>
                          <div className="form-group fv-row mb-12">
                            <label className="fs-6 fw-bold form-label mb-2">
                              <span className="required">Status</span>
                            </label>
                            <select
                              className="form-select form-select-solid"
                              data-kt-select2="true"
                              data-placeholder="Select option"
                              data-dropdown-parent="#kt_menu_61484c5a8da38"
                              data-allow-clear="true"
                            >
                              <option />
                              <option value={1}>Aktif</option>
                              <option value={2}>Tidak Aktfi</option>
                            </select>
                          </div>
                          <div className="form-group fv-row mb-12">
                            <label className="fs-6 fw-bold form-label mb-2">
                              <span className="required">Password</span>
                            </label>
                            <input
                              className="form-control form-control-solid"
                              placeholder="Masukkan Password"
                              name="kode"
                            />
                          </div>
                          <div className="form-group fv-row mb-12">
                            <label className="fs-6 fw-bold form-label mb-2">
                              <span className="required">
                                Konfirmasi Password
                              </span>
                            </label>
                            <input
                              className="form-control form-control-solid"
                              placeholder="Masukkan Konfirmasi Password"
                              name="kode"
                            />
                          </div>
                          <div className="form-group fv-row mb-12">
                            <label className="fs-6 fw-bold form-label mb-2">
                              <span className="required">Role</span>
                            </label>
                            <select
                              className="form-select form-select-solid"
                              data-kt-select2="true"
                              data-placeholder="Select option"
                              data-dropdown-parent="#kt_menu_61484c5a8da38"
                              data-allow-clear="true"
                            >
                              <option />
                              <option value={1}>Aktif</option>
                              <option value={2}>Tidak Aktfi</option>
                            </select>
                          </div>
                          <div className="form-group fv-row mb-12">
                            <label className="fs-6 fw-bold form-label mb-2">
                              <span className="required">Satuan Kerja</span>
                            </label>
                            <select
                              className="form-select form-select-solid"
                              data-kt-select2="true"
                              data-placeholder="Select option"
                              data-dropdown-parent="#kt_menu_61484c5a8da38"
                              data-allow-clear="true"
                            >
                              <option />
                              <option value={1}>Aktif</option>
                              <option value={2}>Tidak Aktfi</option>
                            </select>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddAdministrator;
