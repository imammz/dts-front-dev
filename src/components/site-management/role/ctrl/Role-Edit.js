import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../Role-Edit-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const RoleEdit = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default RoleEdit;
