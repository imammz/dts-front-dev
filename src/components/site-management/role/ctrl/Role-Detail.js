import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../Role-Detail-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const RoleDetail = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default RoleDetail;
