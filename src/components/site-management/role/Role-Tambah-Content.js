import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";

export default class RoleTambah extends React.Component {
  constructor(props) {
    super(props);
    this.kembali = this.kembaliAction.bind(this);
    this.handleSubmit = this.handleSubmitAction.bind(this);
    this.handleChangeStatus = this.handleChangeStatusAction.bind(this);
    this.handleChangeEditable = this.handleChangeEditableAction.bind(this);
    this.handleChangeNama = this.handleChangeNamaAction.bind(this);
    this.handleCheckedPublishChange =
      this.handleCheckedPublishChangeAction.bind(this);
    this.handleCheckedManageChange =
      this.handleCheckedManageChangeAction.bind(this);
    this.handleCheckedViewChange =
      this.handleCheckedViewChangeAction.bind(this);
    this.handleChangeVerifikator =
      this.handleChangeVerifikatorAction.bind(this);
    this.handleChangeAkademi = this.handleChangeAkademiAction.bind(this);
    this.handleChangeTema = this.handleChangeTemaAction.bind(this);
    this.handleChangePelatihan = this.handleChangePelatihanAction.bind(this);
  }

  style = {
    table: {
      borderCollapse: "collapse",
      width: "100%",
    },
    th: {
      paddingTop: "12px",
      paddingBottom: "12px",
      textAlign: "center",
      backgroundColor: "#F3F6F9",
      color: "#6C6C6C",
      border: "1px solid #ddd",
      padding: "8px",
    },
    td: {
      border: "1px solid #ddd",
      padding: "8px",
    },
  };

  state = {
    datax: [],
    loading: false,
    errors: {},
    isLoading: false,
    valStatus: [],
    status: "",
    valEditable: [],
    editable: "",
    nama: "",
    level_0: [],
    level_sorted: [],
    level_sorted2: [],
    checkedPublishState: [],
    checkedViewState: [],
    checkedManageState: [],
    arr_id_publish_checked: [],
    arr_id_view_checked: [],
    arr_id_manage_checked: [],
    from_child: 0,
    from_child2: 0,
    verifikator: "",
    dataxakademi: [],
    dataxtema: [],
    dataxpelatihan: [],
    valAkademi: [],
    valTema: [],
    valPelatihan: [],
    isDisabledTema: true,
    isDisabledPelatihan: true,
    akademi_id: "",
    tema_id: "",
  };

  optionstatus = [
    { value: 1, label: "Aktif" },
    { value: 0, label: "Tidak Aktif" },
  ];

  optioneditable = [
    { value: 1, label: "Yes" },
    { value: 0, label: "No" },
  ];

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    this.loadAkademi();

    localStorage.setItem("from_child", 0);
    localStorage.setItem("from_child_manage", 0);
    localStorage.setItem("from_child_view", 0);

    localStorage.setItem("from_child_2", 0);
    localStorage.setItem("from_child_manage_2", 0);
    localStorage.setItem("from_child_view_2", 0);

    const dataBody = {
      mulai: 0,
      limit: 100,
      sort: "role_id asc",
      id_role: 0,
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/detail-role",
        dataBody,
        this.configs,
      )
      .then((res) => {
        swal.close();
        const detail_role = res.data.result.Detail;
        const level_0 = [];
        const level_sorted = [];
        const level_sorted2 = [];
        detail_role.forEach(function (element) {
          if (element.parent_id == 0) {
            element.parent = true;
            level_0.push(element);
          }
        });

        //iterasi pertama, assign child level 1
        level_0.forEach(function (element, i) {
          element.child1 = false;
          level_sorted.push(element);
          detail_role.forEach(function (element2, j) {
            if (element2.parent_id == element.permissions_id) {
              element2.child1 = true;
              level_sorted.push(element2);
            }
          });
        });

        //iterasi kedua, assign child level 2
        level_sorted.forEach(function (element, i) {
          element.child2 = false;
          //sementara
          element.checked_publish = true;
          element.checked_view = true;
          element.checked_manage = true;
          level_sorted2.push(element);
          detail_role.forEach(function (element2, j) {
            if (
              element.parent != false &&
              element.child1 != false &&
              element2.parent_id == element.permissions_id
            ) {
              element2.child2 = true;
              //sementara
              element2.checked_publish = true;
              element2.checked_view = true;
              element2.checked_manage = true;
              level_sorted2.push(element2);
            }
          });
        });

        this.setState(
          {
            level_0,
            level_sorted,
            level_sorted2,
          },
          () => {
            document.getElementById("checkbox-manage-100").click();
            document.getElementById("checkbox-publish-100").disabled = true;
            document.getElementById("checkbox-view-100").disabled = true;
            document.getElementById("checkbox-manage-100").disabled = true;
            document.getElementById("checkbox-publish-110").disabled = true;
            document.getElementById("checkbox-view-110").disabled = true;
            document.getElementById("checkbox-manage-110").disabled = true;
            document.getElementById("checkbox-publish-120").disabled = true;
            document.getElementById("checkbox-view-120").disabled = true;
            document.getElementById("checkbox-manage-120").disabled = true;
            document.getElementById("checkbox-publish-130").disabled = true;
            document.getElementById("checkbox-view-130").disabled = true;
            document.getElementById("checkbox-manage-130").disabled = true;
            document.getElementById("checkbox-publish-111").disabled = true;
            document.getElementById("checkbox-view-111").disabled = true;
            document.getElementById("checkbox-manage-111").disabled = true;
          },
        );
      });
  }

  loadAkademi() {
    const date = new Date();
    const y = date.getFullYear();
    //akademi
    const dataAkademik = {
      start: 0,
      length: 200,
      status: 99,
      param: "",
      sort: "created_at",
      sort_val: "desc",
      tahun: y,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list-akademi-s3",
        dataAkademik,
        this.configs,
      )
      .then((res) => {
        swal.close();
        const optionx = res.data.result.Data;
        const dataxakademi = [];
        optionx.map((data) =>
          dataxakademi.push({ value: data.id, label: data.name }),
        );
        this.setState({
          dataxakademi,
        });
      });
  }

  handleChangeAkademiAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["akademi"] = "";
    this.setState({ errors });

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    this.setState({
      valTema: [],
      valPelatihan: [],
      tema_id: null,
      pelatihan_id: null,
      pelatihan_disabled: true,
    });

    let akademi_id = selectedOption.value;

    this.setState({
      akademi_id,
      valAkademi: { value: selectedOption.value, label: selectedOption.label },
    });

    const dataBody = {
      start: 0,
      rows: 100,
      id_akademi: selectedOption.value,
      status: "null",
      cari: 0,
      sort: "tema",
      sort_val: "ASC",
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list_tema_filter2",
        dataBody,
        this.configs,
      )
      .then((res) => {
        swal.close();
        const isDisabledTema = false;
        const optionx = res.data.result.Data;
        const dataxtema = [];
        optionx.map((data) =>
          dataxtema.push({ value: data.id, label: data.name }),
        );
        this.setState({
          isDisabledTema,
          dataxtema,
        });
      })
      .catch((error) => {
        const dataxtema = [];
        this.setState({
          dataxtema,
          isDisabledTema: true,
        });
        let messagex = error.response.data.result.Message;
        swal
          .fire({
            title: messagex,
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
            }
          });
      });
  };

  handleChangeTemaAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["tema"] = "";
    this.setState({ errors });

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    this.setState({
      valPelatihan: [],
      pelatihan_id: null,
    });

    let tema_id = selectedOption.value;

    this.setState({
      tema_id: tema_id,
      valTema: { value: selectedOption.value, label: selectedOption.label },
    });

    const dataBody = {
      jns_param: 3,
      akademi_id: this.state.akademi_id,
      theme_id: selectedOption.value,
      pelatihan_id: 0,
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/survey/combo_akademi_pelatihan",
        dataBody,
        this.configs,
      )
      .then((res) => {
        swal.close();
        const isDisabledPelatihan = false;
        this.setState({
          valPelatihan: [],
          isDisabledPelatihan,
        });

        const optionx = res.data.result.Data;
        const dataxpelatihan = [];

        optionx.map((data) =>
          dataxpelatihan.push({ value: data.pid, label: data.nama }),
        );
        this.setState({
          dataxpelatihan,
        });
      })
      .catch((error) => {
        console.log(error);
        const dataxpelatihan = [];
        this.setState({
          dataxpelatihan,
          valPelatihan: [],
        });
        let messagex = error.response.data.result.Message;

        swal
          .fire({
            title: messagex,
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
            }
          });
        this.setState({ isDisabledPelatihan: true });
      });
  };

  handleChangePelatihanAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["byPelatihan"] = "";
    this.setState({ errors });

    this.setState(
      {
        valPelatihan: selectedOption,
      },
      () => {
        console.log(this.state.valPelatihan);
      },
    );
  };

  kembaliAction() {
    window.history.back();
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const check = this.checkEmpty(dataForm, ["nama"], [""]);
    return check;
  }

  resetError() {
    let errors = {};
    errors[("nama", "status", "editable", "role")] = "";
    this.setState({ errors: errors });
  }

  checkEmpty(dataForm, fieldName, fileFieldName) {
    let errors = {};
    let formIsValid = true;
    for (const field in fieldName) {
      const nameAttribute = fieldName[field];
      if (dataForm.get(nameAttribute) == "") {
        errors[nameAttribute] = "Tidak Boleh Kosong";
        formIsValid = false;
      }
    }
    if (this.state.status.length == 0) {
      errors["status"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.editable.length == 0) {
      errors["editable"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.verifikator.length == 0) {
      errors["verifikator"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    if (this.state.verifikator == 1) {
      if (this.state.akademi_id == "") {
        errors["akademi"] = "Tidak Boleh Kosong";
        formIsValid = false;
      }

      if (this.state.tema_id == "") {
        errors["tema"] = "Tidak Boleh Kosong";
        formIsValid = false;
      }

      if (this.state.valPelatihan.length == 0) {
        errors["pelatihan"] = "Tidak Boleh Kosong";
        formIsValid = false;
      }
    }

    let is_empty = true;

    const context = this.state;
    this.state.level_sorted2.forEach(function (element, i) {
      context.arr_id_publish_checked.forEach(function (element2, j) {
        if (element2 == element.permissions_id) {
          is_empty = false;
        }
      });
      context.arr_id_manage_checked.forEach(function (element2, j) {
        if (element2 == element.permissions_id) {
          is_empty = false;
        }
      });
      context.arr_id_view_checked.forEach(function (element2, j) {
        if (element2 == element.permissions_id) {
          is_empty = false;
        }
      });
    });

    if (is_empty) {
      errors["role"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  handleSubmitAction(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });

      const permissions_json = [];
      const context = this.state;
      this.state.level_sorted2.forEach(function (element, i) {
        let detail = {};
        detail.id_permissions = element.permissions_id;
        detail.sub_menu = element.sub_menu;
        detail.parent_id = element.parent_id;
        detail.menu_name = element.menu_name;
        detail.publish_role = 0;
        detail.manage_role = 0;
        detail.view_role = 0;

        context.arr_id_publish_checked.forEach(function (element2, j) {
          if (element2 == element.permissions_id) {
            detail.publish_role = 1;
          }
        });
        context.arr_id_manage_checked.forEach(function (element2, j) {
          if (element2 == element.permissions_id) {
            detail.manage_role = 1;
          }
        });
        context.arr_id_view_checked.forEach(function (element2, j) {
          if (element2 == element.permissions_id) {
            detail.view_role = 1;
          }
        });
        permissions_json.push(detail);
      });

      const arrPelatihan = [];

      if (this.state.verifikator == 1) {
        this.state.valPelatihan.forEach(function (element, i) {
          arrPelatihan.push(element.value);
        });
      }

      const bodyJson = [
        {
          role_name: this.state.nama,
          role_status: this.state.status,
          role_type: this.state.editable,
          asVerifikator: this.state.verifikator,
          created_by: Cookies.get("user_id"),
          permissions_json: permissions_json,
          id_pelatihan: arrPelatihan,
        },
      ];

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/tambah-role",
          bodyJson,
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/site-management/role";
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      this.setState({ isLoading: false });
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleChangeStatusAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["status"] = "";
    this.setState({
      status: selectedOption.value,
      valStatus: { value: selectedOption.value, label: selectedOption.label },
      errors,
    });
  };

  handleChangeEditableAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["editable"] = "";
    this.setState({
      editable: selectedOption.value,
      valEditable: { value: selectedOption.value, label: selectedOption.label },
      errors,
    });
  };

  handleChangeNamaAction(e) {
    const errors = this.state.errors;
    errors["nama"] = "";
    const nama = e.currentTarget.value;
    this.setState({
      nama,
      errors,
    });
  }

  handleCheckedPublishChangeAction(position) {
    const errors = this.state.errors;
    errors["role"] = "";
    this.setState({
      errors,
    });

    const id_checked = position;
    const arr_id_checked = this.state.arr_id_publish_checked;
    let checked = false;
    if (arr_id_checked.includes(id_checked)) {
      const index_checked = arr_id_checked.indexOf(id_checked);
      if (index_checked !== -1) {
        arr_id_checked.splice(index_checked, 1);
        checked = false;
      }
    } else {
      arr_id_checked.push(id_checked);
      checked = true;
    }
    this.setState({ arr_id_publish_checked: arr_id_checked }, () => {});

    if (checked == true) {
      const clicked_checkbox = document.getElementById(
        "checkbox-publish-" + position,
      );
      clicked_checkbox.setAttribute("is_checked", 1);
      //apakah yg di click parent?
      if (clicked_checkbox.getAttribute("parent") == 1) {
        let parent_id = clicked_checkbox.getAttribute("value");

        const allCheckedPublish = Array.from(
          document.getElementsByClassName("check_publish"),
        );

        for (let i = 0; i < allCheckedPublish.length; i++) {
          if (
            allCheckedPublish[i].getAttribute("parent_id") == parent_id &&
            allCheckedPublish[i].getAttribute("is_checked") == 0 &&
            this.state.from_child == 0 &&
            localStorage.getItem("from_child") == 0
          ) {
            allCheckedPublish[i].click();
          }
        }
        //localStorage.setItem('from_child',0);
      } else if (clicked_checkbox.getAttribute("child1") == 1) {
        //dia sebagai parent
        let current_id = clicked_checkbox.getAttribute("value");

        const allCheckedPublish = Array.from(
          document.getElementsByClassName("check_publish"),
        );

        for (let i = 0; i < allCheckedPublish.length; i++) {
          if (
            allCheckedPublish[i].getAttribute("parent_id") == current_id &&
            allCheckedPublish[i].getAttribute("is_checked") == 0 &&
            localStorage.getItem("from_child_2") == 0
          ) {
            allCheckedPublish[i].click();
          }
        }

        //dia sebagai child
        let parent_id = clicked_checkbox.getAttribute("parent_id");

        for (let i = 0; i < allCheckedPublish.length; i++) {
          if (
            allCheckedPublish[i].getAttribute("value") == parent_id &&
            allCheckedPublish[i].getAttribute("is_checked") == 0
          ) {
            localStorage.setItem("from_child", 1);
            allCheckedPublish[i].click();
          }
        }
        localStorage.setItem("from_child", 0);
      } else if (clicked_checkbox.getAttribute("child2") == 1) {
        const allCheckedPublish = Array.from(
          document.getElementsByClassName("check_publish"),
        );

        //sebagai child
        let parent_id = clicked_checkbox.getAttribute("parent_id");
        for (let i = 0; i < allCheckedPublish.length; i++) {
          if (
            allCheckedPublish[i].getAttribute("value") == parent_id &&
            allCheckedPublish[i].getAttribute("is_checked") == 0
          ) {
            localStorage.setItem("from_child_2", 1);
            allCheckedPublish[i].click();
          }
        }
        localStorage.setItem("from_child_2", 0);
      }
    } else {
      localStorage.setItem("from_child", 0);
      const clicked_checkbox = document.getElementById(
        "checkbox-publish-" + position,
      );
      clicked_checkbox.setAttribute("is_checked", 0);
      let current_id = clicked_checkbox.getAttribute("value");

      const allCheckedPublish = Array.from(
        document.getElementsByClassName("check_publish"),
      );

      for (let i = 0; i < allCheckedPublish.length; i++) {
        if (
          allCheckedPublish[i].getAttribute("parent_id") == current_id &&
          allCheckedPublish[i].getAttribute("is_checked") == 1
        ) {
          allCheckedPublish[i].setAttribute("is_checked", 0);
          allCheckedPublish[i].click();
        }
      }

      //check kalo udah di uncheck semua
      if (
        clicked_checkbox.getAttribute("child1") == 1 ||
        clicked_checkbox.getAttribute("child2") == 1
      ) {
        let parent_id = clicked_checkbox.getAttribute("parent_id");
        const allCheckedPublish = Array.from(
          document.getElementsByClassName("check_publish"),
        );
        let click = true;
        for (let i = 0; i < allCheckedPublish.length; i++) {
          if (
            allCheckedPublish[i].getAttribute("parent_id") == parent_id &&
            allCheckedPublish[i].getAttribute("is_checked") == 1
          ) {
            click = false;
          }
        }
        const parent_click = document.getElementById(
          "checkbox-publish-" + parent_id,
        );
        if (click && parent_click.getAttribute("is_checked") == 1) {
          parent_click.click();
        }
      }
    }
  }

  handleCheckedManageChangeAction(position) {
    const errors = this.state.errors;
    errors["role"] = "";
    this.setState({
      errors,
    });

    const id_checked = position;
    const arr_id_checked = this.state.arr_id_manage_checked;
    let checked = false;
    if (arr_id_checked.includes(id_checked)) {
      const index_checked = arr_id_checked.indexOf(id_checked);
      if (index_checked !== -1) {
        arr_id_checked.splice(index_checked, 1);
        checked = false;
      }
    } else {
      arr_id_checked.push(id_checked);
      checked = true;
    }
    this.setState({ arr_id_manage_checked: arr_id_checked }, () => {});

    if (checked == true) {
      const checkbox_publish_id = "checkbox-publish-" + position;
      const checkbox_publish = document.getElementById(checkbox_publish_id);
      checkbox_publish.checked = true;
      checkbox_publish.setAttribute("is_checked", 1);

      const arr_id_publish = this.state.arr_id_publish_checked;
      if (arr_id_publish.includes(position)) {
        const index_checked = arr_id_publish.indexOf(position);
        if (index_checked !== -1) {
          arr_id_publish.splice(index_checked, 1);
        }
      }

      arr_id_publish.push(position);

      const checkbox_view_id = "checkbox-view-" + position;
      const checkbox_view = document.getElementById(checkbox_view_id);
      checkbox_view.checked = true;
      checkbox_view.setAttribute("is_checked", 1);

      const arr_id_view = this.state.arr_id_view_checked;
      if (arr_id_view.includes(position)) {
        const index_checked = arr_id_view.indexOf(position);
        if (index_checked !== -1) {
          arr_id_view.splice(index_checked, 1);
        }
      }

      arr_id_view.push(position);

      this.setState({
        arr_id_view_checked: arr_id_view,
        arr_id_publish_checked: arr_id_publish,
      });

      const clicked_checkbox = document.getElementById(
        "checkbox-manage-" + position,
      );
      clicked_checkbox.setAttribute("is_checked", 1);
      //apakah yg di click parent?
      if (clicked_checkbox.getAttribute("parent") == 1) {
        let parent_id = clicked_checkbox.getAttribute("value");

        const allCheckedPublish = Array.from(
          document.getElementsByClassName("check_manage"),
        );

        for (let i = 0; i < allCheckedPublish.length; i++) {
          if (
            allCheckedPublish[i].getAttribute("parent_id") == parent_id &&
            allCheckedPublish[i].getAttribute("is_checked") == 0 &&
            this.state.from_child == 0 &&
            localStorage.getItem("from_child_manage") == 0
          ) {
            allCheckedPublish[i].click();
          }
        }

        //localStorage.setItem('from_child',0);
      } else if (clicked_checkbox.getAttribute("child1") == 1) {
        //dia sebagai parent
        let current_id = clicked_checkbox.getAttribute("value");

        const allCheckedPublish = Array.from(
          document.getElementsByClassName("check_manage"),
        );

        for (let i = 0; i < allCheckedPublish.length; i++) {
          if (
            allCheckedPublish[i].getAttribute("parent_id") == current_id &&
            allCheckedPublish[i].getAttribute("is_checked") == 0 &&
            localStorage.getItem("from_child_manage_2") == 0
          ) {
            allCheckedPublish[i].click();
          }
        }

        //dia sebagai child
        let parent_id = clicked_checkbox.getAttribute("parent_id");

        for (let i = 0; i < allCheckedPublish.length; i++) {
          if (
            allCheckedPublish[i].getAttribute("value") == parent_id &&
            allCheckedPublish[i].getAttribute("is_checked") == 0
          ) {
            localStorage.setItem("from_child_manage", 1);
            allCheckedPublish[i].click();
          }
        }
        localStorage.setItem("from_child_manage", 0);
      } else if (clicked_checkbox.getAttribute("child2") == 1) {
        const allCheckedPublish = Array.from(
          document.getElementsByClassName("check_manage"),
        );

        //sebagai child
        let parent_id = clicked_checkbox.getAttribute("parent_id");
        for (let i = 0; i < allCheckedPublish.length; i++) {
          if (
            allCheckedPublish[i].getAttribute("value") == parent_id &&
            allCheckedPublish[i].getAttribute("is_checked") == 0
          ) {
            localStorage.setItem("from_child_manage_2", 1);
            allCheckedPublish[i].click();
          }
        }
        localStorage.setItem("from_child_manage_2", 0);
      }
    } else {
      const arr_id_publish = this.state.arr_id_publish_checked;
      if (arr_id_publish.includes(position)) {
        const index_checked = arr_id_publish.indexOf(position);
        if (index_checked !== -1) {
          arr_id_publish.splice(index_checked, 1);
        }
      }

      const arr_id_view = this.state.arr_id_view_checked;
      if (arr_id_view.includes(position)) {
        const index_checked = arr_id_view.indexOf(position);
        if (index_checked !== -1) {
          arr_id_view.splice(index_checked, 1);
        }
      }

      this.setState({
        arr_id_view_checked: arr_id_view,
        arr_id_publish_checked: arr_id_publish,
      });

      const checkbox_publish_id = "checkbox-publish-" + position;
      const checkbox_publish = document.getElementById(checkbox_publish_id);
      checkbox_publish.checked = false;
      checkbox_publish.setAttribute("is_checked", 0);

      const checkbox_view_id = "checkbox-view-" + position;
      const checkbox_view = document.getElementById(checkbox_view_id);
      checkbox_view.checked = false;
      checkbox_view.setAttribute("is_checked", 0);

      localStorage.setItem("from_child", 0);
      const clicked_checkbox = document.getElementById(
        "checkbox-manage-" + position,
      );
      clicked_checkbox.setAttribute("is_checked", 0);
      let current_id = clicked_checkbox.getAttribute("value");

      const allCheckedPublish = Array.from(
        document.getElementsByClassName("check_manage"),
      );

      for (let i = 0; i < allCheckedPublish.length; i++) {
        if (
          allCheckedPublish[i].getAttribute("parent_id") == current_id &&
          allCheckedPublish[i].getAttribute("is_checked") == 1
        ) {
          allCheckedPublish[i].setAttribute("is_checked_manage", 0);
          allCheckedPublish[i].click();
        }
      }

      //check kalo udah di uncheck semua
      if (
        clicked_checkbox.getAttribute("child1") == 1 ||
        clicked_checkbox.getAttribute("child2") == 1
      ) {
        let parent_id = clicked_checkbox.getAttribute("parent_id");
        const allCheckedPublish = Array.from(
          document.getElementsByClassName("check_manage"),
        );
        let click = true;
        for (let i = 0; i < allCheckedPublish.length; i++) {
          if (
            allCheckedPublish[i].getAttribute("parent_id") == parent_id &&
            allCheckedPublish[i].getAttribute("is_checked") == 1
          ) {
            click = false;
          }
        }
        const parent_click = document.getElementById(
          "checkbox-manage-" + parent_id,
        );
        if (click && parent_click.getAttribute("is_checked") == 1) {
          parent_click.click();
        }
      }
    }
  }

  handleCheckedViewChangeAction(position) {
    const errors = this.state.errors;
    errors["role"] = "";
    this.setState({
      errors,
    });

    const id_checked = position;
    const arr_id_checked = this.state.arr_id_view_checked;
    let checked = false;
    if (arr_id_checked.includes(id_checked)) {
      const index_checked = arr_id_checked.indexOf(id_checked);
      if (index_checked !== -1) {
        arr_id_checked.splice(index_checked, 1);
        checked = false;
      }
    } else {
      arr_id_checked.push(id_checked);
      checked = true;
    }
    this.setState({ arr_id_view_checked: arr_id_checked }, () => {
      console.log(this.state.arr_id_view_checked);
    });

    if (checked == true) {
      const checkbox_publish_id = "checkbox-publish-" + position;
      const checkbox_publish = document.getElementById(checkbox_publish_id);
      checkbox_publish.checked = true;
      checkbox_publish.setAttribute("is_checked", 1);

      const arr_id_publish = this.state.arr_id_publish_checked;
      if (arr_id_publish.includes(position)) {
        const index_checked = arr_id_publish.indexOf(position);
        if (index_checked !== -1) {
          arr_id_publish.splice(index_checked, 1);
        }
      }
      arr_id_publish.push(position);

      this.setState({
        arr_id_publish_checked: arr_id_publish,
      });

      const clicked_checkbox = document.getElementById(
        "checkbox-view-" + position,
      );
      clicked_checkbox.setAttribute("is_checked", 1);
      //apakah yg di click parent?
      if (clicked_checkbox.getAttribute("parent") == 1) {
        let parent_id = clicked_checkbox.getAttribute("value");

        const allCheckedPublish = Array.from(
          document.getElementsByClassName("check_view"),
        );

        for (let i = 0; i < allCheckedPublish.length; i++) {
          if (
            allCheckedPublish[i].getAttribute("parent_id") == parent_id &&
            allCheckedPublish[i].getAttribute("is_checked") == 0 &&
            this.state.from_child == 0 &&
            localStorage.getItem("from_child_view") == 0
          ) {
            allCheckedPublish[i].click();
          }
        }
        //localStorage.setItem('from_child',0);
      } else if (clicked_checkbox.getAttribute("child1") == 1) {
        //dia sebagai parent
        let current_id = clicked_checkbox.getAttribute("value");

        const allCheckedPublish = Array.from(
          document.getElementsByClassName("check_view"),
        );

        for (let i = 0; i < allCheckedPublish.length; i++) {
          if (
            allCheckedPublish[i].getAttribute("parent_id") == current_id &&
            allCheckedPublish[i].getAttribute("is_checked") == 0 &&
            localStorage.getItem("from_child_view_2") == 0
          ) {
            allCheckedPublish[i].click();
          }
        }

        //dia sebagai child
        let parent_id = clicked_checkbox.getAttribute("parent_id");

        for (let i = 0; i < allCheckedPublish.length; i++) {
          if (
            allCheckedPublish[i].getAttribute("value") == parent_id &&
            allCheckedPublish[i].getAttribute("is_checked") == 0
          ) {
            localStorage.setItem("from_child_view", 1);
            allCheckedPublish[i].click();
          }
        }
        localStorage.setItem("from_child_view", 0);
      } else if (clicked_checkbox.getAttribute("child2") == 1) {
        const allCheckedPublish = Array.from(
          document.getElementsByClassName("check_view"),
        );

        //sebagai child
        let parent_id = clicked_checkbox.getAttribute("parent_id");
        for (let i = 0; i < allCheckedPublish.length; i++) {
          if (
            allCheckedPublish[i].getAttribute("value") == parent_id &&
            allCheckedPublish[i].getAttribute("is_checked") == 0
          ) {
            localStorage.setItem("from_child_view_2", 1);
            allCheckedPublish[i].click();
          }
        }
        localStorage.setItem("from_child_view_2", 0);
      }
    } else {
      localStorage.setItem("from_child", 0);
      const clicked_checkbox = document.getElementById(
        "checkbox-view-" + position,
      );
      clicked_checkbox.setAttribute("is_checked", 0);
      let current_id = clicked_checkbox.getAttribute("value");

      const allCheckedPublish = Array.from(
        document.getElementsByClassName("check_view"),
      );

      for (let i = 0; i < allCheckedPublish.length; i++) {
        if (
          allCheckedPublish[i].getAttribute("parent_id") == current_id &&
          allCheckedPublish[i].getAttribute("is_checked") == 1
        ) {
          allCheckedPublish[i].setAttribute("is_checked_view", 0);
          allCheckedPublish[i].click();
        }
      }

      //check kalo udah di uncheck semua
      if (
        clicked_checkbox.getAttribute("child1") == 1 ||
        clicked_checkbox.getAttribute("child2") == 1
      ) {
        let parent_id = clicked_checkbox.getAttribute("parent_id");
        const allCheckedPublish = Array.from(
          document.getElementsByClassName("check_view"),
        );
        let click = true;
        for (let i = 0; i < allCheckedPublish.length; i++) {
          if (
            allCheckedPublish[i].getAttribute("parent_id") == parent_id &&
            allCheckedPublish[i].getAttribute("is_checked") == 1
          ) {
            click = false;
          }
        }
        const parent_click = document.getElementById(
          "checkbox-view-" + parent_id,
        );
        if (click && parent_click.getAttribute("is_checked") == 1) {
          parent_click.click();
        }
      }
    }
  }

  handleChangeVerifikatorAction(e) {
    const verifikator = e.target.value;
    const errors = this.state.errors;
    errors["verifikator"] = "";
    this.setState({
      verifikator,
      errors,
    });
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Role Management
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.kembali}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Tambah Role
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <form
                        className="form"
                        action="#"
                        onSubmit={this.handleSubmit}
                      >
                        <input
                          type="hidden"
                          name="csrf-token"
                          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                        />
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">
                            Nama Role
                          </label>
                          <input
                            className="form-control form-control-sm"
                            placeholder="Masukkan Nama Disini"
                            name="nama"
                            value={this.state.nama}
                            onChange={this.handleChangeNama}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["nama"]}
                          </span>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">
                            Verifikator
                          </label>
                          <div className="d-flex">
                            <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                              <input
                                className="form-check-input"
                                onChange={this.handleChangeVerifikator}
                                type="radio"
                                name="verifikator"
                                value="1"
                                id="verifikator"
                              />
                              <label
                                className="form-check-label"
                                htmlFor="jenis_pertanyaan1"
                              >
                                Ya
                              </label>
                            </div>
                            <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                              <input
                                className="form-check-input"
                                onChange={this.handleChangeVerifikator}
                                type="radio"
                                name="verifikator"
                                value="0"
                                id="not_verifikator"
                              />
                              <label
                                className="form-check-label"
                                htmlFor="jenis_pertanyaan1"
                              >
                                Tidak
                              </label>
                            </div>
                          </div>
                          <span style={{ color: "red" }}>
                            {this.state.errors["verifikator"]}
                          </span>
                        </div>

                        {this.state.verifikator == 1 ? (
                          <div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Akademi
                              </label>
                              <Select
                                id="id_akademi"
                                name="idakademi"
                                placeholder="Silahkan pilih"
                                noOptionsMessage={({ inputValue }) =>
                                  !inputValue
                                    ? this.state.dataxakademi
                                    : "Data tidak tersedia"
                                }
                                className="form-select-sm selectpicker p-0"
                                onChange={this.handleChangeAkademi}
                                options={this.state.dataxakademi}
                                value={this.state.valAkademi}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["akademi"]}
                              </span>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row hide">
                              <label className="form-label required">
                                Tema
                              </label>
                              <Select
                                name="idtema"
                                placeholder="Silahkan pilih"
                                noOptionsMessage={({ inputValue }) =>
                                  !inputValue
                                    ? this.state.dataxtema
                                    : "Data tidak tersedia"
                                }
                                className="form-select-sm selectpicker p-0"
                                options={this.state.dataxtema}
                                isDisabled={this.state.isDisabledTema}
                                value={this.state.valTema}
                                onChange={this.handleChangeTema}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["tema"]}
                              </span>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Pelatihan
                              </label>
                              <Select
                                name="idpelatihan"
                                placeholder="Silahkan pilih"
                                /* noOptionsMessage={({ inputValue }) => !inputValue ? this.state.dataxtema : "Data tidak tersedia"} */
                                className="form-select-sm selectpicker p-0"
                                options={this.state.dataxpelatihan}
                                isDisabled={this.state.isDisabledPelatihan}
                                value={this.state.valPelatihan}
                                isMulti={true}
                                onChange={this.handleChangePelatihan}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["pelatihan"]}
                              </span>
                            </div>
                          </div>
                        ) : (
                          ""
                        )}

                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">
                            Editable
                          </label>
                          <Select
                            name="editable"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            options={this.optioneditable}
                            value={this.state.valEditable}
                            onChange={this.handleChangeEditable}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["editable"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">Status</label>
                          <Select
                            name="status"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            options={this.optionstatus}
                            value={this.state.valStatus}
                            onChange={this.handleChangeStatus}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["status"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <h5 className="required">Role Akses</h5>
                          <table style={this.style.table}>
                            <thead>
                              <tr>
                                <th rowSpan="2" style={this.style.th}>
                                  Menu
                                </th>
                                <th colSpan="3" style={this.style.th}>
                                  Hak Akses
                                </th>
                              </tr>
                              <tr>
                                <td style={this.style.th}>Publish</td>
                                <td style={this.style.th}>View</td>
                                <td style={this.style.th}>Manage</td>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.level_sorted2.map((item) => {
                                return item.parent_id == 0 &&
                                  item.parent == true ? (
                                  <tr key={item.permissions_id}>
                                    <td style={this.style.td}>
                                      <i className="las la-plus-circle"></i>{" "}
                                      {item.menu_name}
                                    </td>
                                    <td
                                      style={this.style.td}
                                      className="text-center"
                                    >
                                      {item.checked_publish == true ? (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-publish-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_publish"
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          checked={
                                            this.state.checkedPublishState[
                                              item.permissions_id
                                            ]
                                          }
                                          is_checked={0}
                                          onChange={() =>
                                            this.handleCheckedPublishChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      ) : (
                                        ""
                                      )}
                                    </td>
                                    <td
                                      style={this.style.td}
                                      className="text-center"
                                    >
                                      {item.checked_view == true ? (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-view-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_view"
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          checked={
                                            this.state.checkedViewState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedViewChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      ) : (
                                        ""
                                      )}
                                    </td>
                                    <td
                                      style={this.style.td}
                                      className="text-center"
                                    >
                                      {item.checked_manage == true ? (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-manage-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_manage"
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          checked={
                                            this.state.checkedManageState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedManageChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      ) : (
                                        ""
                                      )}
                                    </td>
                                  </tr>
                                ) : item.child1 == true ? (
                                  <tr key={item.permissions_id}>
                                    <td style={this.style.td}>
                                      <i className="las la-minus-circle ms-5"></i>{" "}
                                      {item.menu_name}
                                    </td>
                                    <td
                                      style={this.style.td}
                                      className="text-center"
                                    >
                                      {item.checked_publish == true ? (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-publish-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          className="check_publish"
                                          checked={
                                            this.state.checkedPublishState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedPublishChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      ) : (
                                        ""
                                      )}
                                    </td>
                                    <td
                                      style={this.style.td}
                                      className="text-center"
                                    >
                                      {item.checked_view == true ? (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-view-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_view"
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          checked={
                                            this.state.checkedViewState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedViewChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      ) : (
                                        ""
                                      )}
                                    </td>
                                    <td
                                      style={this.style.td}
                                      className="text-center"
                                    >
                                      {item.checked_manage == true ? (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-manage-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_manage"
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          checked={
                                            this.state.checkedManageState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedManageChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      ) : (
                                        ""
                                      )}
                                    </td>
                                  </tr>
                                ) : (
                                  <tr key={item.permissions_id}>
                                    <td style={this.style.td}>
                                      <i className="las la-minus-circle ms-10"></i>{" "}
                                      {item.menu_name}
                                    </td>
                                    <td
                                      style={this.style.td}
                                      className="text-center"
                                    >
                                      {item.checked_publish == true ? (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-publish-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_publish"
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          checked={
                                            this.state.checkedPublishState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedPublishChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      ) : (
                                        ""
                                      )}
                                    </td>
                                    <td
                                      style={this.style.td}
                                      className="text-center"
                                    >
                                      {item.checked_view == true ? (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-view-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_view"
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          checked={
                                            this.state.checkedViewState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedViewChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      ) : (
                                        ""
                                      )}
                                    </td>
                                    <td
                                      style={this.style.td}
                                      className="text-center"
                                    >
                                      {item.checked_manage == true ? (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-manage-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_manage"
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          value={item.permissions_id}
                                          checked={
                                            this.state.checkedManageState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedManageChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      ) : (
                                        ""
                                      )}
                                    </td>
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                          <span style={{ color: "red" }}>
                            {this.state.errors["role"]}
                          </span>
                        </div>

                        <div className="text-center pt-10 my-7">
                          <button
                            onClick={this.kembali}
                            type="reset"
                            className="btn btn-light btn-md me-3"
                            data-kt-menu-dismiss="true"
                          >
                            Batal
                          </button>
                          <button
                            type="submit"
                            className="btn btn-primary btn-md"
                            id="submitQuestion1"
                            disabled={this.state.isLoading}
                          >
                            {this.state.isLoading ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm me-2"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Loading...</span>
                                Loading...
                              </>
                            ) : (
                              <>
                                <i className="fa fa-paper-plane me-1"></i>Simpan
                              </>
                            )}
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
