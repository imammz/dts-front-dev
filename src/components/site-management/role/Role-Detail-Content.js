import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import { capitalizeTheFirstLetterOfEachWord } from "../../publikasi/helper";

export default class RoleDetailContent extends React.Component {
  constructor(props) {
    super(props);
    this.kembali = this.kembaliAction.bind(this);
    this.handleDelete = this.handleDeleteAction.bind(this);
  }

  style = {
    table: {
      borderCollapse: "collapse",
      width: "100%",
    },
    th: {
      paddingTop: "12px",
      paddingBottom: "12px",
      textAlign: "center",
      backgroundColor: "#F3F6F9",
      color: "#6C6C6C",
      border: "1px solid #ddd",
      padding: "8px",
    },
    td: {
      border: "1px solid #ddd",
      padding: "8px",
    },
  };

  state = {
    datax: [],
    loading: false,
    errors: {},
    valStatus: [],
    status: "",
    valEditable: [],
    editable: "",
    nama: "",
    level_0: [],
    level_sorted: [],
    level_sorted2: [],
    checkedPublishState: [],
    checkedViewState: [],
    checkedManageState: [],
    arr_id_publish_checked: [],
    arr_id_view_checked: [],
    arr_id_manage_checked: [],
    from_child: 0,
    id_role: false,
  };

  arrayDefaultRole = ["1", "11", "86", "107", "109", "110", "117", "120"];

  optionstatus = [
    { value: 1, label: "Aktif" },
    { value: 0, label: "Tidak Aktif" },
  ];

  optioneditable = [
    { value: 1, label: "Yes" },
    { value: 0, label: "No" },
  ];

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    let segment_url = window.location.pathname.split("/");
    let id_role = segment_url[4];
    this.setState({ id_role: id_role });

    const dataBody = {
      mulai: 0,
      limit: 100,
      sort: "role_id asc",
      id_role: id_role,
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/detail-role",
        dataBody,
        this.configs,
      )
      .then((res) => {
        swal.close();
        const datax = res.data.result.Data[0];
        const nama = datax.role_name;
        const status = datax.role_status;
        const editable = datax.role_editable;

        this.setState({
          nama,
          status,
          editable,
        });
        const detail_role = res.data.result.Detail;
        const level_0 = [];
        const level_sorted = [];
        const level_sorted2 = [];
        detail_role.forEach(function (element) {
          if (element.parent_id == 0) {
            element.parent = true;
            level_0.push(element);
          }
        });

        level_0.sort((a, b) => (a.permissions_id > b.permissions_id ? 1 : -1));

        //iterasi pertama, assign child level 1
        level_0.forEach(function (element, i) {
          element.child1 = false;
          level_sorted.push(element);
          detail_role.forEach(function (element2, j) {
            if (element2.parent_id == element.permissions_id) {
              element2.child1 = true;
              level_sorted.push(element2);
            }
          });
        });

        //iterasi kedua, assign child level 2
        level_sorted.forEach(function (element, i) {
          element.child2 = false;
          //sementara
          element.checked_publish = true;
          element.checked_view = true;
          element.checked_manage = true;
          level_sorted2.push(element);
          detail_role.forEach(function (element2, j) {
            if (
              element.parent != false &&
              element.child1 != false &&
              element2.parent_id == element.permissions_id
            ) {
              element2.child2 = true;
              //sementara
              element2.checked_publish = true;
              element2.checked_view = true;
              element2.checked_manage = true;
              level_sorted2.push(element2);
            }
          });
        });

        this.setState(
          {
            level_0,
            level_sorted,
            level_sorted2,
          },
          () => {},
        );
      });
  }

  kembaliAction() {
    window.history.back();
  }

  handleDeleteAction(e) {
    e.preventDefault();
    const idx = this.state.id_role;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          const data = { id_role: idx };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/hapus-role",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                    allowOutsideClick: false,
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      window.history.back();
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmationButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      window.history.back();
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      window.history.back();
                    }
                  });
              }
            });
        }
      });
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Site Management
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Role Management
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.kembali}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500">
                    <i className="fa fa-chevron-left pe-1"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>

                <a
                  href={"/site-management/role/edit/" + this.state.id_role}
                  className="btn btn-warning fw-bolder btn-sm me-2"
                >
                  <i className="bi bi-gear-fill"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Edit
                  </span>
                </a>
                {this.arrayDefaultRole.includes(this.state.id_role) == false ? (
                  <a
                    href="#"
                    onClick={this.handleDelete}
                    className="btn btn-sm btn-danger btn-active-light-info"
                  >
                    <i className="bi bi-trash-fill text-white pe-1"></i>
                    <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                      Hapus
                    </span>
                  </a>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Detail Role
                          </h1>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="row mb-7">
                          <div className="col-md-6 col-lg-6 col-xs-6">
                            <strong>Nama Role</strong>
                            <div>{this.state.nama}</div>
                          </div>
                          <div className="col-md-6 col-lg-6 col-xs-6"></div>
                        </div>

                        <div className="row mb-7">
                          <div className="col-md-6 col-lg-6 col-xs-6">
                            <strong>Status Role</strong>
                            <div>
                              {capitalizeTheFirstLetterOfEachWord(
                                this.state.status,
                              )}
                            </div>
                          </div>
                          <div className="col-md-6 col-lg-6 col-xs-6">
                            <strong>Status Editable</strong>
                            <div>
                              {capitalizeTheFirstLetterOfEachWord(
                                this.state.editable,
                              )}
                            </div>
                          </div>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <strong className="mb-1">Role Akses</strong>
                          <table style={this.style.table}>
                            <thead>
                              <tr>
                                <th rowSpan="2" style={this.style.th}>
                                  Menu
                                </th>
                                <th colSpan="3" style={this.style.th}>
                                  Hak Akses
                                </th>
                              </tr>
                              <tr>
                                <td style={this.style.th}>Publish</td>
                                <td style={this.style.th}>View</td>
                                <td style={this.style.th}>Manage</td>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.level_sorted2.map((item) => {
                                return item.parent_id == 0 &&
                                  item.parent == true ? (
                                  <tr key={item.permissions_id}>
                                    <td style={this.style.td}>
                                      <i className="las la-plus-circle"></i>{" "}
                                      {item.menu_name}
                                    </td>
                                    <td
                                      style={this.style.td}
                                      className="text-center"
                                    >
                                      {item.publish_role == 1 ? (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-publish-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_publish"
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          checked={
                                            this.state.checkedPublishState[
                                              item.permissions_id
                                            ]
                                          }
                                          defaultChecked={true}
                                          disabled={true}
                                          is_checked={0}
                                          onChange={() =>
                                            this.handleCheckedPublishChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      ) : (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-publish-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_publish"
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          checked={
                                            this.state.checkedPublishState[
                                              item.permissions_id
                                            ]
                                          }
                                          defaultChecked={false}
                                          disabled={true}
                                          is_checked={0}
                                          onChange={() =>
                                            this.handleCheckedPublishChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      )}
                                    </td>
                                    <td
                                      style={this.style.td}
                                      className="text-center"
                                    >
                                      {item.view_role == 1 ? (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-view-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_view"
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          defaultChecked={true}
                                          disabled={true}
                                          checked={
                                            this.state.checkedViewState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedViewChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      ) : (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-view-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_view"
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          defaultChecked={false}
                                          disabled={true}
                                          checked={
                                            this.state.checkedViewState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedViewChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      )}
                                    </td>
                                    <td
                                      style={this.style.td}
                                      className="text-center"
                                    >
                                      {item.manage_role == true ? (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-manage-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_manage"
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          defaultChecked={true}
                                          disabled={true}
                                          checked={
                                            this.state.checkedManageState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedManageChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      ) : (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-manage-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_manage"
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          defaultChecked={false}
                                          disabled={true}
                                          checked={
                                            this.state.checkedManageState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedManageChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      )}
                                    </td>
                                  </tr>
                                ) : item.child1 == true ? (
                                  <tr key={item.permissions_id}>
                                    <td style={this.style.td}>
                                      <i className="las la-minus-circle ms-5"></i>{" "}
                                      {item.menu_name}
                                    </td>
                                    <td
                                      style={this.style.td}
                                      className="text-center"
                                    >
                                      {item.publish_role == 1 ? (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-publish-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          defaultChecked={true}
                                          disabled={true}
                                          className="check_publish"
                                          checked={
                                            this.state.checkedPublishState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedPublishChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      ) : (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-publish-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          defaultChecked={false}
                                          disabled={true}
                                          className="check_publish"
                                          checked={
                                            this.state.checkedPublishState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedPublishChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      )}
                                    </td>
                                    <td
                                      style={this.style.td}
                                      className="text-center"
                                    >
                                      {item.view_role == 1 ? (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-view-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_view"
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          defaultChecked={true}
                                          disabled={true}
                                          checked={
                                            this.state.checkedViewState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedViewChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      ) : (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-view-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_view"
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          defaultChecked={false}
                                          disabled={true}
                                          checked={
                                            this.state.checkedViewState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedViewChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      )}
                                    </td>
                                    <td
                                      style={this.style.td}
                                      className="text-center"
                                    >
                                      {item.manage_role == 1 ? (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-manage-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_manage"
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          defaultChecked={true}
                                          disabled={true}
                                          checked={
                                            this.state.checkedManageState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedManageChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      ) : (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-manage-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_manage"
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          defaultChecked={false}
                                          disabled={true}
                                          checked={
                                            this.state.checkedManageState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedManageChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      )}
                                    </td>
                                  </tr>
                                ) : (
                                  <tr key={item.permissions_id}>
                                    <td style={this.style.td}>
                                      <i className="las la-minus-circle ms-10"></i>{" "}
                                      {item.menu_name}
                                    </td>
                                    <td
                                      style={this.style.td}
                                      className="text-center"
                                    >
                                      {item.publish_role == 1 ? (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-publish-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_publish"
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          defaultChecked={true}
                                          disabled={true}
                                          checked={
                                            this.state.checkedPublishState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedPublishChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      ) : (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-publish-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_publish"
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          defaultChecked={false}
                                          disabled={true}
                                          checked={
                                            this.state.checkedPublishState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedPublishChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      )}
                                    </td>
                                    <td
                                      style={this.style.td}
                                      className="text-center"
                                    >
                                      {item.view_role == 1 ? (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-view-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_view"
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          defaultChecked={true}
                                          disabled={true}
                                          checked={
                                            this.state.checkedViewState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedViewChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      ) : (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-view-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_view"
                                          value={item.permissions_id}
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          defaultChecked={false}
                                          disabled={true}
                                          checked={
                                            this.state.checkedViewState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedViewChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      )}
                                    </td>
                                    <td
                                      style={this.style.td}
                                      className="text-center"
                                    >
                                      {item.manage_role == 1 ? (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-manage-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_manage"
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          defaultChecked={true}
                                          disabled={true}
                                          value={item.permissions_id}
                                          checked={
                                            this.state.checkedManageState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedManageChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      ) : (
                                        <input
                                          type="checkbox"
                                          id={`checkbox-manage-${item.permissions_id}`}
                                          name={item.permissions_id}
                                          className="check_manage"
                                          parent={item.parent ? 1 : 0}
                                          child1={item.child1 ? 1 : 0}
                                          child2={item.child2 ? 1 : 0}
                                          parent_id={item.parent_id}
                                          is_checked={0}
                                          defaultChecked={false}
                                          disabled={true}
                                          value={item.permissions_id}
                                          checked={
                                            this.state.checkedManageState[
                                              item.permissions_id
                                            ]
                                          }
                                          onChange={() =>
                                            this.handleCheckedManageChange(
                                              item.permissions_id,
                                            )
                                          }
                                        />
                                      )}
                                    </td>
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
