import axios from "axios";
import Cookies from "js-cookie";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

const configs = {
  headers: {
    Authorization: "Bearer " + Cookies.get("token"),
  },
};

const MySwal = withReactContent(Swal);

const checkToken = () => {
  return new Promise(async (resolve, reject) => {
    if (Cookies.get("token") == null) {
      const { isConfirmed } = await Swal.fire({
        title: "Unauthenticated.",
        text: "Silahkan login ulang",
        icon: "warning",
        confirmButtonText: "Ok",
      });

      if (isConfirmed) return reject();
    }

    resolve();
  });
};

export const fetchAkademi = async ({
  tahun = "0",
  start = 0,
  length = 10,
  status = "all",
  param = "",
  sort = "id",
  sort_val = "desc",
}) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/list-akademi-s3",
            {
              tahun,
              start,
              length,
              status,
              param,
              sort,
              sort_val: sort_val,
            },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            const statux = res.data.result.Status;
            const messagex = res.data.result.Message;

            let { Total, Data, Status = false, Message = "" } = result || {};

            if (statux) {
              resolve({
                total: Total,
                data: Data,
              });
            } else {
              MySwal.fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              }).then((result) => {
                console.log(result);
              });
              resolve({
                total: 0,
                data: [],
              });
              reject(Message);
            }
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchAkademiById = async ({ id }) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/akademi/cari-akademi-s3",
            { id },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data, Status = false, Message = "" } = result || {};
            [Data] = Data || [];
            Data = {
              ...Data,
              brosur: { name: Data?.brosur },
              brosurPreview: Data?.brosur,
              logo: { name: Data?.logo },
              icon: { name: Data?.icon },
            };

            if (Status) resolve(Data);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchTemaByAkademi = async ({ id, start = 0, length = 10 }) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/akademi/detail-akademi-tema-paging",
            { id, start, length },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Total, Tema, Status = false, Message = "" } = result || {};

            if (Status)
              resolve({
                total: Total,
                data: Tema,
              });
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchMitraByAkademi = async ({ id, start = 0, length = 10 }) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/akademi/detail-akademi-mitra-paging",
            { id, start, length },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Total, mitra, Status = false, Message = "" } = result || {};

            if (Status)
              resolve({
                total: Total,
                data: mitra,
              });
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchPenyelenggaraByAkademi = async ({
  id,
  start = 0,
  length = 10,
}) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/akademi/detail-akademi-penyelenggara-paging",
            { id, start, length },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let {
              Total,
              penyelenggara,
              Status = false,
              Message = "",
            } = result || {};

            if (Status)
              resolve({
                total: Total,
                data: penyelenggara,
              });
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchPelatihanByAkademi = async ({
  id,
  start = 0,
  length = 10,
}) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/akademi/detail-akademi-pelatihan-paging",
            { id, start, length },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let {
              Total,
              pelatihan,
              Status = false,
              Message = "",
            } = result || {};

            if (Status)
              resolve({
                total: Total,
                data: pelatihan,
              });
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const deleteAkademi = async ({ id }) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/akademi/softdelete-akademi",
            { id },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            let { Data = [], Status = false, Message = "" } = data || {};

            if (Status) resolve(Message);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const validate = (data) => {
  let { name, slug, tittle_name, deskripsi, brosur, logo, icon, status } =
    data || {};
  let error = {};

  if (!name) error["nameError"] = "Nama akademi tidak boleh kosong";
  if (!slug) error["slugError"] = "Singkatan akademi tidak boleh kosong";
  if (!tittle_name) error["tittle_nameError"] = "Nama alias tidak boleh kosong";
  if (!deskripsi) error["deskripsiError"] = "Deksripsi tidak boleh kosong";
  if (!brosur) error["brosurError"] = "Brosur tidak boleh kosong";
  if (!logo) error["logoError"] = "Logo tidak boleh kosong";
  if (!icon) error["iconError"] = "Icon tidak boleh kosong";
  if (status == null) error["statusError"] = "Status tidak boleh kosong";

  return error;
};

export const resetError = () => {
  return {};
};

export const hasError = (error = {}) => {
  let _hasError = false;
  Object.keys(error).forEach((k) => {
    if (!error[k]) {
    } else if (Array.isArray(error[k]) || typeof error[k] == "object") {
      _hasError = _hasError || hasError(error[k]);
    } else {
      _hasError = _hasError || true;
    }
  });
  return _hasError;
};

export const update = async (payload) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/akademi/update-akademi-s3",
            payload,
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Message);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const insert = async (payload) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/akademi/tambah-akademi-s3",
            payload,
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Message);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};
