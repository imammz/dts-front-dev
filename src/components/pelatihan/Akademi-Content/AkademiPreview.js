import React from "react";
import Select from "react-select";
import Swal from "sweetalert2";
import Footer from "../../../components/Footer";
import Header from "../../../components/Header";
import SideNav from "../../../components/SideNav";
import { withRouter } from "../../RouterUtil";
import {
  deleteAkademi,
  fetchAkademiById,
  fetchMitraByAkademi,
  fetchPelatihanByAkademi,
  fetchPenyelenggaraByAkademi,
  fetchTemaByAkademi,
  hasError,
  resetError,
  update,
  validate,
} from "./actions";
import DataTable from "react-data-table-component";
import { capitalWord, dateRange, handleFormatDate } from "../Pelatihan/helper";
import Cookies from "js-cookie";
import RenderFormElement from "../Pelatihan/RenderFormElement";

class Content extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      statusList: [
        {
          label: "Publish",
          value: 1,
        },
        {
          label: "Unpublish",
          value: 0,
        },
      ],

      data: null,
      loading: false,

      currentTemaRowsPerPage: 10,
      currentTemaPage: 1,
      totalTema: 0,
      dataTema: [],
      dataTemaLoading: false,

      currentPelatihanRowsPerPage: 10,
      currentPelatihanPage: 1,
      totalPelatihan: 0,
      dataPelatihan: [],
      dataPelatihanLoading: false,

      currentMitraRowsPerPage: 10,
      currentMitraPage: 1,
      totalMitra: 0,
      dataMitra: [],
      dataMitraLoading: false,

      currentPenyelenggaraRowsPerPage: 10,
      currentPenyelenggaraPage: 1,
      totalPenyelenggara: 0,
      dataPenyelenggara: [],
      dataPenyelenggaraLoading: false,
    };
  }

  fetch = async () => {
    const { id } = this.props.params || {};

    try {
      this.setState({ loading: true });
      const data = await fetchAkademiById({ id });
      this.setState({ data, loading: false });
    } catch (err) {}
  };

  fetchTemaByAkademi = async () => {
    const { id } = this.props.params || {};

    try {
      this.setState({ dataTemaLoading: true });
      const { total, data } = await fetchTemaByAkademi({
        id,
        start:
          (this.state.currentTemaPage - 1) * this.state.currentTemaRowsPerPage,
        length: this.state.currentTemaRowsPerPage,
      });
      this.setState({
        totalTema: total,
        dataTema: data,
        dataTemaLoading: false,
      });
    } catch (err) {}
  };

  onTemaChangeRowsPerPage = (currentRowsPerPage, currentPage) => {
    this.setState(
      {
        currentTemaRowsPerPage: currentRowsPerPage,
        currentTemaPage: currentPage,
      },
      () => this.fetchTemaByAkademi(),
    );
  };

  onTemaChangePage = (page, totalRows) => {
    this.setState({ currentTemaPage: page }, () => this.fetchTemaByAkademi());
  };

  fetchMitraByAkademi = async () => {
    const { id } = this.props.params || {};

    try {
      this.setState({ dataMitraLoading: true });
      const { total, data } = await fetchMitraByAkademi({
        id,
        start:
          (this.state.currentMitraPage - 1) *
          this.state.currentMitraRowsPerPage,
        length: this.state.currentMitraRowsPerPage,
      });
      this.setState({
        totalMitra: total,
        dataMitra: data,
        dataMitraLoading: false,
      });
    } catch (err) {}
  };

  fetchPenyelenggaraByAkademi = async () => {
    const { id } = this.props.params || {};

    try {
      this.setState({ dataPenyelenggaraLoading: true });
      const { total, data } = await fetchPenyelenggaraByAkademi({
        id,
        start:
          (this.state.currentPenyelenggaraPage - 1) *
          this.state.currentPenyelenggaraRowsPerPage,
        length: this.state.currentPenyelenggaraRowsPerPage,
      });
      this.setState({
        totalPenyelenggara: total,
        dataPenyelenggara: data,
        dataPenyelenggaraLoading: false,
      });
    } catch (err) {}
  };

  onMitraChangeRowsPerPage = (currentRowsPerPage, currentPage) => {
    this.setState(
      {
        currentMitraRowsPerPage: currentRowsPerPage,
        currentMitraPage: currentPage,
      },
      () => this.fetchMitraByAkademi(),
    );
  };

  onMitraChangePage = (page, totalRows) => {
    this.setState({ currentMitraPage: page }, () => this.fetchMitraByAkademi());
  };

  onPenyelenggaraChangeRowsPerPage = (currentRowsPerPage, currentPage) => {
    this.setState(
      {
        currentPenyelenggaraRowsPerPage: currentRowsPerPage,
        currentPenyelenggaraPage: currentPage,
      },
      () => this.fetchPenyelenggaraByAkademi(),
    );
  };

  onPenyelenggaraChangePage = (page, totalRows) => {
    this.setState({ currentPenyelenggaraPage: page }, () =>
      this.fetchPenyelenggaraByAkademi(),
    );
  };

  fetchPelatihanByAkademi = async () => {
    const { id } = this.props.params || {};

    try {
      this.setState({ dataPelatihanLoading: true });
      const { total, data } = await fetchPelatihanByAkademi({
        id,
        start:
          (this.state.currentPelatihanPage - 1) *
          this.state.currentPelatihanRowsPerPage,
        length: this.state.currentPelatihanRowsPerPage,
      });
      this.setState({
        totalPelatihan: total,
        dataPelatihan: data,
        dataPelatihanLoading: false,
      });
    } catch (err) {}
  };

  onPelatihanChangeRowsPerPage = (currentRowsPerPage, currentPage) => {
    this.setState(
      {
        currentPelatihanRowsPerPage: currentRowsPerPage,
        currentPelatihanPage: currentPage,
      },
      () => this.fetchPelatihanByAkademi(),
    );
  };

  onPelatihanChangePage = (page, totalRows) => {
    this.setState({ currentPelatihanPage: page }, () =>
      this.fetchPelatihanByAkademi(),
    );
  };

  onRemove = async (id) => {
    try {
      const { isConfirmed = false } = await Swal.fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      });

      if (isConfirmed) {
        Swal.fire({
          title: "Mohon Tunggu!",
          icon: "info",
          allowOutsideClick: false,
          didOpen: () => Swal.showLoading(),
        });

        const message = await deleteAkademi({ id });

        Swal.close();

        await Swal.fire({
          title: message || "Akademi berhasil dihapus",
          icon: "success",
          confirmButtonText: "Ok",
        });

        this.back();
      }

      // throw new Error("API delete belum tersedia");
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  back() {
    window.location.href = "/pelatihan/akademi";
  }

  componentDidMount() {
    this.fetch();
    this.fetchTemaByAkademi();
    this.fetchMitraByAkademi();
    this.fetchPelatihanByAkademi();
    this.fetchPenyelenggaraByAkademi();
  }

  render() {
    const { id } = this.props.params || {};

    const mitraOption = [
      { value: "99", label: "Semua status", color: "primary" },
      { value: "1", label: "Aktif", color: "success" },
      { value: "0", label: "Tidak Aktif", color: "danger" },
    ];

    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Akademi
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <a
                  href="#"
                  onClick={() => this.back()}
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </a>
                <a
                  href={"/pelatihan/akademi/content/" + id}
                  className="btn btn-sm btn-warning btn-active-light-info me-2"
                >
                  <i className="bi bi-gear-fill text-white"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Edit
                  </span>
                </a>
                <a
                  href="#"
                  className="btn btn-sm btn-danger btn-active-light-info"
                  onClick={() => this.onRemove(id)}
                >
                  <i className="bi bi-trash-fill text-white"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Detail Akademi
                          </h1>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="col-lg-12 mb-7">
                          <div className="d-flex flex-wrap py-3">
                            <div className="symbol symbol-100px symbol-lg-120px">
                              <img
                                src={this.state.data?.logo?.name}
                                alt=""
                                className="img-fluid me-3"
                              />
                            </div>

                            <div className="flex-grow-1 mb-3">
                              <div className="d-flex justify-content-between align-items-start flex-wrap pt-6">
                                <div className="d-flex flex-column">
                                  <div className="d-flex align-items-center mb-2">
                                    <h5 className="fw-bolder mb-n1 fs-5">
                                      {this.state.data?.name} (
                                      {this.state.data?.slug})
                                    </h5>
                                  </div>
                                  <div className="d-flex flex-wrap fw-bold fs-6 pe-2">
                                    <span className="fs-7 text-gray-600">
                                      {this.state.data?.tittle_name}
                                    </span>
                                  </div>
                                </div>
                                <div className="d-flex">
                                  <a
                                    href={this.state.data?.brosur?.name}
                                    target="_blank"
                                    className="btn btn-sm btn-info me-2"
                                    id="kt_user_follow_button"
                                  >
                                    <i className="bi bi-file-earmark-text"></i>
                                    <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                      Brosur Akademi
                                    </span>
                                  </a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="d-flex border-bottom p-0">
                          <ul
                            className="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap"
                            style={{
                              overflowX: "auto",
                              overflowY: "hidden",
                              display: "flex",
                              whiteSpace: "nowrap",
                              marginBottom: 0,
                            }}
                          >
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary active"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_1"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                  Informasi Akademi
                                </span>
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary false"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_2"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                  Tema
                                </span>
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary false"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_3"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                  Mitra
                                </span>
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary false"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_4"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                  Pelatihan
                                </span>
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary false"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_5"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                  Penyelenggara
                                </span>
                              </a>
                            </li>
                          </ul>
                        </div>
                        <div className="tab-content" id="detail-account-tab">
                          {/* tab 1 */}
                          <div
                            className="tab-pane fade show active"
                            id="kt_tab_pane_1"
                            role="tabpanel"
                          >
                            <div className="row mt-7 pb-7">
                              <h5 className="fs-5 text-muted pt-5">
                                Deskripsi
                              </h5>
                              <div
                                dangerouslySetInnerHTML={{
                                  __html: this.state.data?.deskripsi,
                                }}
                              ></div>
                            </div>
                          </div>

                          <div
                            className="tab-pane fade"
                            id="kt_tab_pane_2"
                            role="tabpanel"
                          >
                            <div className="row mt-7 pb-7">
                              <DataTable
                                columns={[
                                  {
                                    className:
                                      "min-w-250px min-h-250px mw-250px mh-250px",
                                    name: "No",
                                    width: "70px",
                                    grow: 1,
                                    center: true,
                                    sortable: false,
                                    cell: (currentTema, idx) => {
                                      return (
                                        <div>
                                          <span>
                                            {(this.state.currentTemaPage - 1) *
                                              this.state
                                                .currentTemaRowsPerPage +
                                              idx +
                                              1}
                                          </span>
                                        </div>
                                      );
                                    },
                                  },
                                  {
                                    Header: "Tema",
                                    accessor: "",
                                    className:
                                      "min-w-250px min-h-250px mw-250px mh-250px",
                                    sortType: "basic",
                                    name: "Nama Tema",
                                    grow: 1,
                                    initSort: "nama",
                                    sortable: true,
                                    cell: (currentTema) => {
                                      return (
                                        <div>
                                          <span>{currentTema.name}</span>
                                        </div>
                                      );
                                    },
                                    selector: (currentTema) => currentTema.name,
                                  },
                                  {
                                    Header: "Pelatihan",
                                    accessor: "",
                                    className:
                                      "min-w-250px min-h-250px mw-250px mh-250px",
                                    sortType: "basic",
                                    name: "Jml. Pelatihan",
                                    grow: 1,
                                    center: true,
                                    initSort: "nama",
                                    sortable: true,
                                    cell: (currentTema) => {
                                      return (
                                        <div>
                                          <span className="badge badge-light-primary fs-7">
                                            {currentTema.jml_pelatihan || 0}
                                          </span>
                                        </div>
                                      );
                                    },
                                    selector: (currentTema) => "",
                                  },
                                  {
                                    Header: "Status",
                                    accessor: "",
                                    className:
                                      "min-w-250px min-h-250px mw-250px mh-250px",
                                    sortType: "basic",
                                    name: "Status",
                                    grow: 1,
                                    initSort: "nama",
                                    sortable: true,
                                    cell: (currentTema) => {
                                      return (
                                        <div>
                                          <span className="badge badge-light-success fs-7">
                                            {currentTema.status}
                                          </span>
                                        </div>
                                      );
                                    },
                                    selector: (currentTema) => currentTema.name,
                                  },
                                  {
                                    Header: "Aksi",
                                    accessor: "",
                                    className:
                                      "min-w-250px min-h-250px mw-250px mh-250px",
                                    sortType: "basic",
                                    name: "Aksi",
                                    center: true,
                                    grow: 1,
                                    width: "150px",
                                    initSort: "nama",
                                    sortable: true,
                                    cell: (currentTema) => {
                                      return (
                                        <div className="row">
                                          <a
                                            href={
                                              "/pelatihan/view-tema/" +
                                              currentTema.tema_id
                                            }
                                            id={currentTema.tema_id}
                                            title="View"
                                            className="btn btn-icon btn-primary btn-sm"
                                          >
                                            <span className="svg-icon svg-icon-3">
                                              <i className="bi bi-eye-fill"></i>
                                            </span>
                                          </a>
                                        </div>
                                      );
                                    },
                                    selector: (currentTema) => currentTema.name,
                                  },
                                ]}
                                data={this.state.dataTema}
                                progressPending={this.state.dataTemaLoading}
                                highlightOnHover
                                pointerOnHover
                                pagination
                                paginationServer
                                onChangeRowsPerPage={
                                  this.onTemaChangeRowsPerPage
                                }
                                paginationTotalRows={this.state.totalTema}
                                onChangePage={this.onTemaChangePage}
                                // defaultSortAsc={true}
                                persistTableHead={true}
                                sortServer
                                noDataComponent={
                                  <div className="mt-5">Tidak Ada Data</div>
                                }
                              />
                            </div>
                          </div>

                          <div
                            className="tab-pane fade"
                            id="kt_tab_pane_3"
                            role="tabpanel"
                          >
                            <div className="row mt-7 pb-7">
                              <DataTable
                                columns={[
                                  {
                                    className:
                                      "min-w-250px min-h-250px mw-250px mh-250px",
                                    name: "No",
                                    width: "70px",
                                    grow: 1,
                                    sortable: false,
                                    cell: (currentTema, idx) => {
                                      return (
                                        <div>
                                          <span>
                                            {(this.state.currentMitraPage - 1) *
                                              this.state
                                                .currentMitraRowsPerPage +
                                              idx +
                                              1}
                                          </span>
                                        </div>
                                      );
                                    },
                                  },
                                  {
                                    Header: "Mitra",
                                    accessor: "",
                                    className:
                                      "min-w-250px min-h-250px mw-250px mh-250px",
                                    sortType: "basic",
                                    name: "Nama Mitra",
                                    grow: 1,
                                    initSort: "nama",
                                    sortable: true,
                                    cell: (currentMitra) => {
                                      return (
                                        <div>
                                          <span>{currentMitra.nama_mitra}</span>
                                        </div>
                                      );
                                    },
                                    selector: (currentMitra) =>
                                      currentMitra.nama_mitra,
                                  },
                                  {
                                    Header: "Pelatihan",
                                    accessor: "",
                                    className:
                                      "min-w-250px min-h-250px mw-250px mh-250px",
                                    sortType: "basic",
                                    name: "Jml. Pelatihan",
                                    grow: 1,
                                    initSort: "nama",
                                    sortable: true,
                                    center: true,
                                    cell: (currentMitra) => {
                                      return (
                                        <div>
                                          <span className="badge badge-light-primary fs-7">
                                            {currentMitra.jml_pelatihan || 0}
                                          </span>
                                        </div>
                                      );
                                    },
                                    selector: (currentMitra) =>
                                      currentMitra.nama_mitra,
                                  },
                                  {
                                    Header: "Status",
                                    accessor: "",
                                    className:
                                      "min-w-250px min-h-250px mw-250px mh-250px",
                                    sortType: "basic",
                                    name: "Status",
                                    grow: 1,
                                    initSort: "nama",
                                    sortable: true,
                                    cell: (currentMitra) => {
                                      const status = mitraOption.find(
                                        (o) =>
                                          o.value == currentMitra.status_mitra,
                                      );
                                      return (
                                        <div>
                                          <span
                                            className={`badge badge-light-${status.color} fs-7`}
                                          >
                                            {status.label}
                                          </span>
                                        </div>
                                      );
                                    },
                                    selector: (currentMitra) =>
                                      currentMitra.status,
                                  },
                                  {
                                    Header: "Aksi",
                                    accessor: "",
                                    className:
                                      "min-w-250px min-h-250px mw-250px mh-250px",
                                    sortType: "basic",
                                    name: "Aksi",
                                    grow: 1,
                                    width: "150px",
                                    center: true,
                                    initSort: "nama",
                                    sortable: true,
                                    cell: (currentMitra) => {
                                      return (
                                        <div>
                                          <a
                                            href={
                                              "/partnership/view-mitra/" +
                                              currentMitra.id_mitra
                                            }
                                            id={currentMitra.id_mitra}
                                            title="View"
                                            className="btn btn-icon btn-primary btn-sm"
                                          >
                                            <span className="svg-icon svg-icon-3">
                                              <i className="bi bi-eye-fill"></i>
                                            </span>
                                          </a>
                                        </div>
                                      );
                                    },
                                    selector: (currentMitra) =>
                                      currentMitra.name,
                                  },
                                ]}
                                data={this.state.dataMitra}
                                progressPending={this.state.dataMitraLoading}
                                highlightOnHover
                                pointerOnHover
                                pagination
                                paginationServer
                                paginationTotalRows={this.state.totalMitra}
                                onChangeRowsPerPage={
                                  this.onMitraChangeRowsPerPage
                                }
                                onChangePage={this.onMitraChangePage}
                                // defaultSortAsc={true}
                                persistTableHead={true}
                                sortServer
                                noDataComponent={
                                  <div className="mt-5">Tidak Ada Data</div>
                                }
                              />
                            </div>
                          </div>

                          <div
                            className="tab-pane fade"
                            id="kt_tab_pane_4"
                            role="tabpanel"
                          >
                            <div className="row mt-7 pb-7">
                              <DataTable
                                columns={[
                                  {
                                    className:
                                      "min-w-250px min-h-250px mw-250px mh-250px",
                                    name: "No",
                                    width: "70px",
                                    grow: 1,
                                    center: true,
                                    sortable: false,
                                    cell: (currentTema, idx) => {
                                      return (
                                        <div>
                                          <span>
                                            {(this.state.currentPelatihanPage -
                                              1) *
                                              this.state
                                                .currentPelatihanRowsPerPage +
                                              idx +
                                              1}
                                          </span>
                                        </div>
                                      );
                                    },
                                  },
                                  {
                                    Header: "ID Pelatihan",
                                    accessor: "",
                                    className:
                                      "min-w-250px min-h-250px mw-250px mh-250px",
                                    sortType: "basic",
                                    name: "ID",
                                    width: "150px",
                                    grow: 1,
                                    initSort: "nama",
                                    sortable: true,
                                    cell: (currentPelatihan) => {
                                      return (
                                        <div>
                                          <span>
                                            {currentPelatihan?.slug_pelatian_id}
                                          </span>
                                        </div>
                                      );
                                    },
                                    selector: (currentPelatihan) =>
                                      currentPelatihan.id_pelatihan,
                                  },
                                  {
                                    Header: "Pelatihan",
                                    accessor: "",
                                    className:
                                      "min-w-250px min-h-250px mw-250px mh-250px",
                                    sortType: "basic",
                                    name: "Nama Pelatihan",
                                    grow: 1,
                                    width: "300px",
                                    initSort: "nama",
                                    sortable: true,
                                    cell: (currentPelatihan) => {
                                      return (
                                        <div className="my-2">
                                          <span>
                                            <b>
                                              {capitalWord(
                                                currentPelatihan.pelatihan,
                                              )}
                                            </b>
                                            <br />
                                            <span className="text-muted fw-semibold">
                                              {currentPelatihan.penyelenggara}
                                            </span>
                                            <br />
                                            <span className="text-muted fw-semibold">
                                              {
                                                currentPelatihan.metode_pelatihan
                                              }
                                            </span>
                                          </span>
                                        </div>
                                      );
                                    },
                                    selector: (currentPelatihan) =>
                                      currentPelatihan.pelatihan,
                                  },
                                  {
                                    Header: "Jadwal Pendaftaran",
                                    accessor: "",
                                    sortType: "basic",
                                    name: "Jadwal Pendaftaran",
                                    grow: 1,
                                    width: "200px",
                                    initSort: "nama",
                                    sortable: true,
                                    cell: (row) => (
                                      <div>
                                        <span className="fs-7">
                                          {dateRange(
                                            row.pendaftaran_start,
                                            row.pendaftaran_end,
                                            "YYYY-MM-DD HH:mm:ss",
                                            "DD MMM YYYY",
                                          )}
                                        </span>
                                        <p className="fs-8 my-0 fw-semibold text-muted">
                                          {dateRange(
                                            row.pendaftaran_start,
                                            row.pendaftaran_end,
                                            "YYYY-MM-DD HH:mm:ss",
                                            "HH:mm:ss",
                                          )}
                                        </p>
                                      </div>
                                    ),
                                    selector: (currentPelatihan) =>
                                      currentPelatihan.pelatihan,
                                  },
                                  {
                                    Header: "Jadwal Pelatihan",
                                    accessor: "",
                                    sortType: "basic",
                                    name: "Jadwal Pelatihan",
                                    grow: 1,
                                    width: "200px",
                                    initSort: "nama",
                                    sortable: true,
                                    cell: (row) => (
                                      <div>
                                        <span className="fs-7">
                                          {dateRange(
                                            row.pelatihan_start,
                                            row.pelatihan_end,
                                            "YYYY-MM-DD HH:mm:ss",
                                            "DD MMM YYYY",
                                          )}
                                        </span>
                                        <p className="fs-8 my-0 fw-semibold text-muted">
                                          {dateRange(
                                            row.pelatihan_start,
                                            row.pelatihan_end,
                                            "YYYY-MM-DD HH:mm:ss",
                                            "HH:mm:ss",
                                          )}
                                        </p>
                                      </div>
                                    ),
                                    selector: (currentPelatihan) =>
                                      currentPelatihan.pelatihan,
                                  },
                                  {
                                    Header: "Status Publsh",
                                    accessor: "",
                                    className:
                                      "min-w-250px min-h-250px mw-250px mh-250px",
                                    sortType: "basic",
                                    name: "Status",
                                    grow: 1,
                                    initSort: "nama",
                                    sortable: true,
                                    cell: (currentPelatihan) => {
                                      return (
                                        <div>
                                          <span
                                            className={
                                              "badge badge-light-" +
                                              (currentPelatihan.status_publish ==
                                              "1"
                                                ? "success"
                                                : currentPelatihan.status_publish ==
                                                    "0"
                                                  ? "danger"
                                                  : "warning") +
                                              " fs-7 m-1"
                                            }
                                          >
                                            {currentPelatihan.status_publish ==
                                            "1"
                                              ? "Publish"
                                              : currentPelatihan.status_publish ==
                                                  "0"
                                                ? "Unpublish"
                                                : "Unlisted"}
                                          </span>
                                        </div>
                                      );
                                    },
                                    selector: (currentPelatihan) =>
                                      currentPelatihan.pelatihan,
                                  },
                                  {
                                    Header: "Status Pelatihan",
                                    accessor: "",
                                    className:
                                      "min-w-250px min-h-250px mw-250px mh-250px",
                                    sortType: "basic",
                                    name: "Status Pelatihan",
                                    grow: 1,
                                    initSort: "nama",
                                    sortable: true,
                                    cell: (currentPelatihan) => {
                                      return (
                                        <div>
                                          <span
                                            className={
                                              "badge badge-light-" +
                                              (currentPelatihan.status_pelatihan ==
                                              "Selesai"
                                                ? "success"
                                                : currentPelatihan.status_pelatihan ==
                                                    "Dibatalkan"
                                                  ? "danger"
                                                  : currentPelatihan.status_pelatihan ==
                                                      "Pelatihan"
                                                    ? "warning"
                                                    : "primary") +
                                              " fs-7 m-1"
                                            }
                                          >
                                            {currentPelatihan.status_pelatihan}
                                          </span>
                                        </div>
                                      );
                                    },
                                    selector: (currentPelatihan) =>
                                      currentPelatihan.pelatihan,
                                  },
                                  {
                                    Header: "Aksi",
                                    accessor: "",
                                    className:
                                      "min-w-250px min-h-250px mw-250px mh-250px",
                                    sortType: "basic",
                                    name: "Aksi",
                                    grow: 1,
                                    width: "150px",
                                    initSort: "nama",
                                    sortable: true,
                                    cell: (currentPelatihan) => {
                                      return (
                                        <div className="row">
                                          <a
                                            href={
                                              "/pelatihan/view-pelatihan/" +
                                              currentPelatihan.id_pelatihan
                                            }
                                            id={currentPelatihan.id_pelatihan}
                                            title="View"
                                            className="btn btn-icon btn-sm btn-primary"
                                          >
                                            <span className="svg-icon svg-icon-3">
                                              <i className="bi bi-eye-fill"></i>
                                            </span>
                                          </a>
                                        </div>
                                      );
                                    },
                                    selector: (currentPelatihan) =>
                                      currentPelatihan.pelatihan,
                                  },
                                ]}
                                data={this.state.dataPelatihan}
                                progressPending={
                                  this.state.dataPelatihanLoading
                                }
                                highlightOnHover
                                pointerOnHover
                                pagination
                                paginationServer
                                paginationTotalRows={this.state.totalPelatihan}
                                onChangeRowsPerPage={
                                  this.onPelatihanChangeRowsPerPage
                                }
                                onChangePage={this.onPelatihanChangePage}
                                // defaultSortAsc={true}
                                persistTableHead={true}
                                sortServer
                                noDataComponent={
                                  <div className="mt-5">Tidak Ada Data</div>
                                }
                              />
                            </div>
                          </div>

                          <div
                            className="tab-pane fade"
                            id="kt_tab_pane_5"
                            role="tabpanel"
                          >
                            <div className="row mt-7 pb-7">
                              <DataTable
                                columns={[
                                  {
                                    className:
                                      "min-w-250px min-h-250px mw-250px mh-250px",
                                    name: "No",
                                    grow: 1,
                                    width: "70px",
                                    center: true,
                                    sortable: false,
                                    cell: (currentTema, idx) => {
                                      return (
                                        <div>
                                          <span>
                                            {(this.state
                                              .currentPenyelenggaraPage -
                                              1) *
                                              this.state
                                                .currentPenyelenggaraRowsPerPage +
                                              idx +
                                              1}
                                          </span>
                                        </div>
                                      );
                                    },
                                  },
                                  {
                                    Header: "Penyelenggara",
                                    accessor: "",
                                    className:
                                      "min-w-250px min-h-250px mw-250px mh-250px",
                                    sortType: "basic",
                                    name: "Penyelenggara",
                                    grow: 1,
                                    initSort: "nama",
                                    sortable: true,
                                    cell: (row) => {
                                      return (
                                        <div>
                                          <span>{row.penyelenggara}</span>
                                        </div>
                                      );
                                    },
                                  },
                                  {
                                    Header: "Pelatihan",
                                    accessor: "",
                                    className:
                                      "min-w-250px min-h-250px mw-250px mh-250px",
                                    sortType: "basic",
                                    name: "Jml. Pelatihan",
                                    grow: 1,
                                    initSort: "nama",
                                    center: true,
                                    sortable: true,
                                    cell: (row) => {
                                      return (
                                        <div>
                                          <span className="badge badge-light-primary fs-7">
                                            {row.jml_pelatihan || 0}
                                          </span>
                                        </div>
                                      );
                                    },
                                  },
                                  // {
                                  //   Header: "Aksi",
                                  //   accessor: "",
                                  //   className: "min-w-250px min-h-250px mw-250px mh-250px",
                                  //   sortType: 'basic',
                                  //   name: "Aksi",
                                  //   grow: 1,
                                  //   initSort: 'nama',
                                  //   sortable: true,
                                  //   cell: row => {
                                  //     return (
                                  //       <div>
                                  //         <a href={"/partnership/view-penyenggara/" + row.id_penyelenggara} id={row.id_penyelenggara} title="View" className="btn btn-icon btn-active-light-primary w-30px h-30px me-3">
                                  //           <span className="svg-icon svg-icon-3">
                                  //             <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" fill="none">
                                  //               <path d="M21.7 18.9L18.6 15.8C17.9 16.9 16.9 17.9 15.8 18.6L18.9 21.7C19.3 22.1 19.9 22.1 20.3 21.7L21.7 20.3C22.1 19.9 22.1 19.3 21.7 18.9Z" fill="currentColor" />
                                  //               <path opacity="0.3" d="M11 20C6 20 2 16 2 11C2 6 6 2 11 2C16 2 20 6 20 11C20 16 16 20 11 20ZM11 4C7.1 4 4 7.1 4 11C4 14.9 7.1 18 11 18C14.9 18 18 14.9 18 11C18 7.1 14.9 4 11 4ZM8 11C8 9.3 9.3 8 11 8C11.6 8 12 7.6 12 7C12 6.4 11.6 6 11 6C8.2 6 6 8.2 6 11C6 11.6 6.4 12 7 12C7.6 12 8 11.6 8 11Z" fill="currentColor" />
                                  //             </svg>
                                  //           </span>
                                  //         </a>
                                  //       </div>

                                  //     );
                                  //   },
                                  // }
                                ]}
                                data={this.state.dataPenyelenggara}
                                progressPending={
                                  this.state.dataPenyelenggaraLoading
                                }
                                highlightOnHover
                                pointerOnHover
                                pagination
                                paginationServer
                                paginationTotalRows={
                                  this.state.totalPenyelenggara
                                }
                                onChangeRowsPerPage={
                                  this.onPenyelenggaraChangeRowsPerPage
                                }
                                onChangePage={this.onPenyelenggaraChangePage}
                                // defaultSortAsc={true}
                                persistTableHead={true}
                                sortServer
                                noDataComponent={
                                  <div className="mt-5">Tidak Ada Data</div>
                                }
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

class AkademiContent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Header />
        <SideNav />
        <Content {...this.props} />
        <Footer />
      </div>
    );
  }
}

export default withRouter(AkademiContent);
