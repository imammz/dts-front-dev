import React from "react";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Select from "react-select";
import Swal from "sweetalert2";
import Footer from "../../../components/Footer";
import Header from "../../../components/Header";
import SideNav from "../../../components/SideNav";
import { withRouter } from "../../RouterUtil";
import {
  deleteAkademi,
  fetchAkademiById,
  hasError,
  resetError,
  update,
  validate,
} from "./actions";
import Cookies from "js-cookie";
import { isValidHttpUrl, isValidUrl } from "../../../utils/commonutil";
import Editor from "@arbor-dev/ckeditor5-arbor-custom";
class Content extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      statusList: [
        {
          label: "Publish",
          value: 1,
        },
        {
          label: "Unpublish",
          value: 0,
        },
      ],

      data: null,
      loading: false,
      isLoading: false,
      preview: { logo: "", icon: "" },
    };
  }

  fetch = async () => {
    const { id } = this.props.params || {};

    try {
      this.setState({ loading: true });
      const data = await fetchAkademiById({ id });
      this.setState({ data, loading: false });
    } catch (err) {}
  };

  validateAndSave = () => {
    const errors = validate(this.state?.data);
    if (!hasError(errors)) {
      this.save();
    } else {
      this.setState({ ...resetError(), ...errors });

      Swal.fire({
        title: "Masih terdapat isian yang belum lengkap",
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  save = async () => {
    this.setState({ isLoading: true });
    const { id } = this.props.params || {};

    try {
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info",
        allowOutsideClick: false,
        didOpen: () => Swal.showLoading(),
      });

      var payload = new FormData();
      if (!this.state.data?.brosur?.name?.startsWith("http"))
        payload.append("brosur", this.state.data?.brosur);
      payload.append(
        "deskripsi",
        (this.state.data?.deskripsi || "").replace(/'/g, '"'),
      );
      payload.append("id", id);
      if (!this.state.data?.logo?.name?.startsWith("http"))
        payload.append("logo", this.state.data?.logo);
      payload.append("name", this.state.data?.name);
      payload.append("slug", this.state.data?.slug);
      payload.append("status", this.state.data?.status);
      if (!this.state.data?.icon?.name?.startsWith("http"))
        payload.append("icon", this.state.data?.icon);
      payload.append("tittle_name", this.state.data?.tittle_name);

      const message = await update(payload);

      Swal.close();
      this.setState({ isLoading: false });

      await Swal.fire({
        title: message || "Akademi berhasil disimpan",
        icon: "success",
        confirmButtonText: "Ok",
      });

      window.location.href = "/pelatihan/akademi";
    } catch (err) {
      this.setState({ isLoading: false });
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  onRemove = async (id) => {
    try {
      const { isConfirmed = false } = await Swal.fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      });

      if (isConfirmed) {
        Swal.fire({
          title: "Mohon Tunggu!",
          icon: "info",
          allowOutsideClick: false,
          didOpen: () => Swal.showLoading(),
        });

        const message = await deleteAkademi({ id });

        Swal.close();

        await Swal.fire({
          title: message || "Akademi berhasil dihapus",
          icon: "success",
          confirmButtonText: "Ok",
        });

        this.back();
      }

      // throw new Error("API delete belum tersedia");
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  back() {
    window.location.href = "/pelatihan/akademi";
  }

  componentDidMount() {
    this.fetch();
  }

  render() {
    const { id } = this.props.params || {};

    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Akademi
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <a
                  href="#"
                  onClick={() => this.back()}
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </a>
                <a
                  href="#"
                  className="btn btn-sm btn-danger btn-active-light-info"
                  onClick={() => this.onRemove(id)}
                >
                  <i className="bi bi-trash-fill text-white"></i>
                  <span
                    className={`d-md-inline d-lg-inline d-xl-inline d-none ${
                      this.state.data?.jml_tema ||
                      this.state.data?.jml_pelatihan
                        ? "disabled"
                        : ""
                    }`}
                  >
                    Hapus
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Edit Akademi
                          </h1>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="form-group fv-row mb-7">
                          <label className="fs-6 fw-bold form-label mb-2">
                            <span className="required">Nama Akademi</span>
                          </label>
                          <input
                            className="form-control form-control-sm font-size-h4"
                            onChange={(e) => {
                              let data = this.state.data || {};
                              data.name = e.target.value;
                              this.setState({ data, nameError: null });
                            }}
                            value={this.state.data?.name}
                            placeholder="Masukkan Nama Akademi"
                            id="name"
                            name="name"
                          />
                          <span style={{ color: "red" }}>
                            {this.state?.nameError}
                          </span>
                        </div>
                        <div className="form-group fv-row mb-7">
                          <label className="fs-6 fw-bold form-label mb-2">
                            <span className="required">Singkatan Akademi</span>
                          </label>
                          <input
                            className="form-control form-control-sm font-size-h4 text-uppercase"
                            maxLength={5}
                            onChange={(e) => {
                              let data = this.state.data || {};
                              data.slug = e.target.value;
                              this.setState({ data, slugError: null });
                            }}
                            value={this.state.data?.slug}
                            id="slug"
                            placeholder="Masukkan Kode Akademi"
                            name="slug"
                          />
                          <span style={{ color: "red" }}>
                            {this.state?.slugError}
                          </span>
                        </div>
                        <div className="form-group fv-row mb-7">
                          <label className="fs-6 fw-bold form-label mb-2">
                            <span className="required">
                              Alias (Istilah Indonesia)
                            </span>
                          </label>
                          <input
                            className="form-control form-control-sm font-size-h4"
                            onChange={(e) => {
                              let data = this.state.data || {};
                              data.tittle_name = e.target.value;
                              this.setState({ data, tittle_nameError: null });
                            }}
                            value={this.state.data?.tittle_name}
                            placeholder="Masukkan Nama Alias"
                            id="tittle_name"
                            name="tittle_name"
                          />
                          <span style={{ color: "red" }}>
                            {this.state?.tittle_nameError}
                          </span>
                        </div>
                        <div className="form-group  fv-row mb-7">
                          <label className="fs-6 fw-bold form-label mb-2">
                            <span className="required">Deksripsi Akademi</span>
                          </label>
                          <CKEditor
                            editor={Editor}
                            data={this.state.data?.deskripsi}
                            name="deskripsi"
                            id="deskripsi"
                            dispatchEvent="ispatch"
                            subscribeTo="[ 'focus', 'change' ]"
                            initData={this.state.data?.deskripsi}
                            config={{
                              ckfinder: {
                                // Upload the images to the server using the CKFinder QuickUpload command.
                                uploadUrl:
                                  process.env.REACT_APP_BASE_API_URI +
                                  "/pelatihan/ckeditor-upload",
                              },
                            }}
                            onChange={(e, editor) => {
                              let data = this.state.data || {};
                              data.deskripsi = editor.getData();
                              this.setState({ data, deskripsiError: null });
                            }}
                          ></CKEditor>
                          <span style={{ color: "red" }}>
                            {this.state?.deskripsiError}
                          </span>
                        </div>
                        <div className="form-group  fv-row mb-7">
                          <label className="fs-6 fw-bold form-label mb-2">
                            <span className="required">
                              Upload Brosur/Flyer
                            </span>
                          </label>
                          <input
                            type="file"
                            className="form-control form-control-sm mb-2"
                            name="upload_silabus"
                            accept=".pdf"
                            onInput={(e) => {
                              const [f] = [...e.target.files];

                              const reader = new FileReader();
                              reader.addEventListener(
                                "load",
                                () => {
                                  let data = this.state.data || {};
                                  data.brosur = f;
                                  data.brosurPreview = reader.result;
                                  this.setState({ data, brosurError: null });
                                },
                                false,
                              );
                              reader.readAsDataURL(f);
                              e.target.value = null;
                            }}
                          />
                          <small className="text-muted">
                            Format File (.pdf), Max 10240 Kb
                          </small>
                          <br />
                          <span style={{ color: "red" }}>
                            {this.state?.brosurError}
                          </span>
                          {this.state.data?.brosur && (
                            <div className="row">
                              <div className="col-lg-12 mb-7 fv-row">
                                <a
                                  target="_blank"
                                  href={this.state.data?.brosur?.name}
                                >
                                  {this.state.data?.brosur?.name}
                                </a>
                              </div>
                            </div>
                          )}
                          {this.state.data?.brosurPreview && (
                            <div className="row">
                              <div className="col-lg-12 mb-7 fv-row">
                                <div class="embed-responsive embed-responsive-16by9 d-inline-block">
                                  {/* <iframe class="embed-responsive-item" src={this.state.data?.brosurPreview} allowfullscreen></iframe> */}
                                </div>
                                <div
                                  className="btn btn-sm btn-icon btn-light-danger float-end"
                                  title="Hapus file"
                                  onClick={(e) => {
                                    let data = this.state.data || {};
                                    data.brosur = null;
                                    data.brosurPreview = null;
                                    this.setState({ data });
                                  }}
                                >
                                  <span className="las la-trash-alt" />
                                </div>
                              </div>
                            </div>
                          )}
                        </div>
                        <div className="form-group  fv-row mb-7">
                          <label className="fs-6 fw-bold form-label mb-2">
                            <span className="required">
                              Logo Akademi (akan dicantumkan pada sertifikat
                              kelulusan)
                            </span>
                          </label>
                          <input
                            type="file"
                            className="form-control form-control-sm mb-2"
                            name="upload_silabus"
                            accept="image/png, image/gif, image/jpeg"
                            onInput={(e) => {
                              const [f] = [...e.target.files];
                              let data = this.state.data || {};
                              data.icon = f;
                              this.setState({ data, iconError: null });
                              e.target.value = null;

                              var reader = new FileReader();
                              reader.onloadend = function () {
                                // console.log(reader.result)
                                this.setState((prevState) => {
                                  let preview = prevState.preview;
                                  return {
                                    ...prevState,
                                    preview: {
                                      ...preview,
                                      icon: reader.result,
                                    },
                                  };
                                });
                              }.bind(this);
                              reader.readAsDataURL(f);
                            }}
                          />
                          <small className="text-muted">
                            Format File (.png, .jpg, .gif), Max 10240 Kb
                          </small>
                          <br />
                          <span style={{ color: "red" }}>
                            {this.state?.iconError}
                          </span>
                          {this.state.data?.icon && (
                            <div className="row">
                              <div className="col-lg-12 mb-7 fv-row">
                                <a
                                  target="_blank"
                                  href={this.state.data?.icon?.name}
                                >
                                  {this.state.data?.icon?.name}
                                </a>
                              </div>
                            </div>
                          )}
                          {this.state.data?.icon?.name && (
                            <div className="row">
                              <div className="col-lg-12 mb-7 fv-row">
                                <div className="symbol symbol-100px symbol-lg-120px">
                                  <img
                                    src={
                                      isValidHttpUrl(
                                        this.state.data?.icon?.name,
                                      )
                                        ? this.state.data?.icon?.name
                                        : this.state.preview.icon
                                    }
                                    alt=""
                                    className="img-fluid me-3"
                                  />
                                </div>
                                <div
                                  className="btn btn-sm btn-icon btn-light-danger float-end"
                                  title="Hapus file"
                                  onClick={(e) => {
                                    let data = this.state.data || {};
                                    data.icon = null;
                                    this.setState({ data });
                                  }}
                                >
                                  <span className="las la-trash-alt" />
                                </div>
                              </div>
                            </div>
                          )}
                        </div>
                        <div className="form-group  fv-row mb-7">
                          <label className="fs-6 fw-bold form-label mb-2">
                            <span className="required">Icon Akademi</span>
                          </label>
                          <input
                            type="file"
                            className="form-control form-control-sm mb-2"
                            name="upload_silabus"
                            accept="image/png, image/gif, image/jpeg"
                            onInput={(e) => {
                              const [f] = [...e.target.files];
                              let data = this.state.data || {};
                              data.logo = f;
                              this.setState({ data, logoError: null });
                              e.target.value = null;

                              var reader = new FileReader();
                              reader.onloadend = function () {
                                // console.log(reader.result)
                                this.setState((prevState) => {
                                  let preview = prevState.preview;
                                  return {
                                    ...prevState,
                                    preview: {
                                      ...preview,
                                      logo: reader.result,
                                    },
                                  };
                                });
                              }.bind(this);
                              reader.readAsDataURL(f);
                            }}
                          />
                          <small className="text-muted">
                            Format File (.png, .jpg, .gif), Max 10240 Kb
                          </small>
                          <br />
                          <span style={{ color: "red" }}>
                            {this.state?.logoError}
                          </span>
                          {this.state.data?.logo && (
                            <div className="row">
                              <div className="col-lg-12 mb-7 fv-row">
                                <a
                                  target="_blank"
                                  href={this.state.data?.logo?.name}
                                >
                                  {this.state.data?.logo?.name}
                                </a>
                              </div>
                            </div>
                          )}
                          {this.state.data?.logo?.name && (
                            <div className="row">
                              <div className="col-lg-12 mb-7 fv-row">
                                {/* <span>{this.state.data?.logo?.name}</span> */}
                                <div className="symbol symbol-100px symbol-lg-120px">
                                  <img
                                    src={
                                      isValidHttpUrl(
                                        this.state.data?.logo?.name,
                                      )
                                        ? this.state.data?.logo?.name
                                        : this.state.preview.logo
                                    }
                                    alt=""
                                    className="img-fluid me-3"
                                  />
                                </div>
                                <div
                                  className="btn btn-sm btn-icon btn-light-danger float-end"
                                  title="Hapus file"
                                  onClick={(e) => {
                                    let data = this.state.data || {};
                                    data.logo = null;
                                    this.setState({ data });
                                  }}
                                >
                                  <span className="las la-trash-alt" />
                                </div>
                              </div>
                            </div>
                          )}
                        </div>
                        <div className="form-group  fv-row mb-7">
                          <label className="fs-6 fw-bold form-label mb-2">
                            <span className="required">Status Akademi</span>
                          </label>
                          <>
                            <Select
                              name="status"
                              placeholder="Silahkan pilih"
                              className="form-select-sm selectpicker p-0"
                              value={this.state.statusList.find(
                                ({ value }) => value == this.state.data?.status,
                              )}
                              // defaultValue={selectStatus}
                              // isOptionSelected={selectStatus}
                              options={this.state.statusList}
                              onChange={({ value }) => {
                                let data = this.state.data || {};
                                data.status = value;
                                this.setState({ data, statusError: null });
                              }}
                            />
                          </>
                          <span style={{ color: "red" }}>
                            {this.state?.statusError}
                          </span>
                        </div>
                        <div className="form-group fv-row pt-7 mb-7">
                          <div className="d-flex justify-content-center mb-7">
                            <a
                              href="/pelatihan/akademi"
                              type="reset"
                              className="btn btn-md btn-light btn-active-light-primary me-4"
                              data-kt-menu-dismiss="true"
                            >
                              Batal
                            </a>
                            <button
                              disabled={this.state.isLoading}
                              type="submit"
                              className="btn btn-primary btn-md"
                              onClick={(e) => this.validateAndSave(e)}
                            >
                              {this.state.isLoading ? (
                                <>
                                  <span
                                    className="spinner-border spinner-border-sm me-2"
                                    role="status"
                                    aria-hidden="true"
                                  ></span>
                                  <span className="sr-only">Loading...</span>
                                  Loading...
                                </>
                              ) : (
                                <>
                                  <i className="fa fa-paper-plane me-1"></i>
                                  Simpan
                                </>
                              )}
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

class AkademiContent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Header />
        <SideNav />
        <Content {...this.props} />
        <Footer />
      </div>
    );
  }
}

export default withRouter(AkademiContent);
