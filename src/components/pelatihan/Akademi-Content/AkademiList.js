import React from "react";
// import { useTable, useSortBy, usePagination } from 'react-table';
import DataTable from "react-data-table-component";
import Select from "react-select";
import Swal from "sweetalert2";
import Footer from "../../../components/Footer";
import Header from "../../../components/Header";
import SideNav from "../../../components/SideNav";
import {
  cekPermition,
  isAdministrator,
  isSuperAdmin,
  logout,
} from "../../AksesHelper";
import { capitalizeFirstLetter } from "../../publikasi/helper";
import { deleteAkademi, fetchAkademi } from "./actions";
import Cookies from "js-cookie";
import withReactContent from "sweetalert2-react-content";

const MySwal = withReactContent(Swal);
class Content extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentRowsPerPage: 10,
      currentPage: 1,
      sortField: "created_at",
      sortDir: "desc",

      statusList: [
        {
          label: "Publish",
          value: 1,
        },
        {
          label: "Unpublish",
          value: 0,
        },
      ],
      selectedStatus: 99,
      searchKeyword: "",

      total: 0,
      data: [],
      loading: false,
    };
  }

  fetch = async () => {
    try {
      this.setState({ loading: true });

      const { total, data } = await fetchAkademi({
        start: (this.state.currentPage - 1) * this.state.currentRowsPerPage,
        length: this.state.currentRowsPerPage,
        param: this.state.searchKeyword,
        status: this.state.selectedStatus,
        sort: this.state.sortField,
        sort_val: this.state.sortDir,
      });

      this.setState({ total, data, loading: false });
    } catch (err) {
      this.setState({ total: 0, data: [], loading: false });
      MySwal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      }).then((result) => {
        console.log(result);
      });
    }
  };

  componentDidMount() {
    this.fetch();

    if (cekPermition().view !== 1) {
      Swal.fire({
        title: "Akses Tidak Diizinkan",
        html: "<i> Anda akan kembali kehalaman login </i>",
        icon: "warning",
      }).then(() => {
        logout();
      });
    }
  }

  componentDidUpdate() {
    // this.loadData();
  }

  onChangeRowsPerPage = (currentRowsPerPage, currentPage) => {
    this.setState({ currentRowsPerPage, currentPage }, () => this.fetch());
  };

  onChangePage = (page, totalRows) => {
    this.setState({ currentPage: page }, () => this.fetch());
  };

  onSort = ({ sortField }, sortDir) => {
    this.setState({ sortField, sortDir }, () => this.fetch());
  };

  onRemove = async (id) => {
    try {
      const { isConfirmed = false } = await Swal.fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      });

      if (isConfirmed) {
        Swal.fire({
          title: "Mohon Tunggu!",
          icon: "info",
          allowOutsideClick: false,
          didOpen: () => Swal.showLoading(),
        });

        const message = await deleteAkademi({ id });

        Swal.close();

        await Swal.fire({
          title: message || "Akademi berhasil dihapus",
          icon: "success",
          confirmButtonText: "Ok",
        }).then(() => {
          this.fetch();
        });

        this.setState({ searchKeyword: 0 }, () => this.fetch());
      }

      // throw new Error("API delete belum tersedia");
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  onFilterReset = () => {
    this.setState({ selectedStatus: 99 }, () => this.fetch());
  };

  onFilterFilter = () => {
    this.setState({ currentPage: 1 }, () => this.fetch());
  };

  onFilterSearch = (searchKeyword) => {
    this.setState({ searchKeyword }, () => this.fetch());
  };

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Akademi
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                  data-bs-toggle="modal"
                  data-bs-target="#filter"
                >
                  <i className="bi bi-sliders"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Filter
                  </span>
                </button>

                {cekPermition().manage === 1 && isSuperAdmin() && (
                  <a
                    href="/pelatihan/akademi/add"
                    className="btn btn-success fw-bolder btn-sm"
                  >
                    <i className="bi bi-plus-circle"></i>
                    <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                      Tambah Akademi
                    </span>
                  </a>
                )}
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Daftar Akademi
                          </h1>
                        </div>

                        <div className="card-toolbar">
                          <div className="d-flex align-items-center position-relative my-1 me-2">
                            <span className="svg-icon svg-icon-1 position-absolute ms-6">
                              <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className="mh-50px"
                              >
                                <rect
                                  opacity="0.5"
                                  x="17.0365"
                                  y="15.1223"
                                  width="8.15546"
                                  height="2"
                                  rx="1"
                                  transform="rotate(45 17.0365 15.1223)"
                                  fill="currentColor"
                                ></rect>
                                <path
                                  d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                  fill="currentColor"
                                ></path>
                              </svg>
                            </span>
                            <input
                              type="text"
                              data-kt-user-table-filter="search"
                              className="form-control form-control-sm form-control-solid w-250px ps-14"
                              placeholder="Cari Akademi"
                              onKeyPress={(e) => {
                                if (e.key == "Enter")
                                  this.onFilterSearch(e.target.value);
                              }}
                              onChange={(e) => {
                                if (!e.target.value) this.onFilterSearch("");
                              }}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <DataTable
                          columns={[
                            {
                              name: "No",
                              width: "70px",
                              center: true,
                              sortable: false,
                              cell: (row, index) => {
                                return (
                                  <span>
                                    {(this.state.currentPage - 1) *
                                      this.state.currentRowsPerPage +
                                      index +
                                      1}
                                  </span>
                                );
                              },
                            },
                            {
                              Header: "Nama Akademi",
                              accessor: "",
                              className:
                                "min-w-300px min-h-50px mw-300px mh-50px",
                              // sortType: 'basic',
                              allowOverflow: true,
                              name: "Nama Akademi",
                              width: "350px",
                              sortField: "name",
                              sortable: true,
                              margin: "20px",
                              grow: 6,
                              selector: (akademi) =>
                                capitalizeFirstLetter(
                                  akademi.name.toLowerCase(),
                                ),
                              cell: (akademi) => {
                                return (
                                  <label className="d-flex flex-stack my-2 cursor-pointer">
                                    <span className="d-flex align-items-center me-2">
                                      <span className="symbol symbol-50px me-6">
                                        <span className="symbol-label bg-light-primary">
                                          <span className="svg-icon svg-icon-1 svg-icon-primary">
                                            <img
                                              src={akademi.logo}
                                              alt=""
                                              className="symbol-label"
                                            />
                                          </span>
                                        </span>
                                      </span>
                                      <span className="d-flex flex-column">
                                        <a
                                          href={
                                            "/pelatihan/akademi/preview/" +
                                            akademi.id
                                          }
                                          className="text-dark"
                                        >
                                          <span
                                            className="fw-bolder fs-7"
                                            style={{
                                              overflow: "hidden",
                                              whiteSpace: "wrap",
                                              textOverflow: "ellipses",
                                            }}
                                          >
                                            {capitalizeFirstLetter(
                                              akademi.name.toLowerCase(),
                                            )}
                                          </span>
                                        </a>
                                        <span className="fs-7 fw-semibold text-muted">
                                          {akademi.tittle_name}
                                        </span>
                                      </span>
                                    </span>
                                  </label>
                                );
                              },
                              style: {
                                textTransform: "capitalize",
                              },
                            },
                            {
                              Header: "Alias",
                              accessor: "",
                              className:
                                "min-w-200px min-h-50px mw-200px mh-50px",
                              // sortType: 'basic',
                              allowOverflow: true,
                              name: "Alias",
                              width: "100px",
                              sortField: "slug",
                              sortable: true,
                              margin: "20px",
                              grow: 6,
                              selector: (akademi) =>
                                capitalizeFirstLetter(
                                  akademi.name.toLowerCase(),
                                ),
                              cell: (akademi) => {
                                return (
                                  <label className="d-flex flex-stack my-2 cursor-pointer">
                                    <span className="d-flex align-items-center me-2">
                                      <span className="d-flex flex-column">
                                        <span className="fs-7 fw-semibold text-muted">
                                          {akademi.slug}
                                        </span>
                                      </span>
                                    </span>
                                  </label>
                                );
                              },
                              style: {
                                textTransform: "capitalize",
                              },
                            },
                            {
                              Header: "Tema",
                              accessor: "",
                              className: " ",
                              // sortType: 'basic',
                              name: "Tema",
                              sortField: "jml_tema",
                              center: true,
                              allowOverflow: true,
                              grow: 2,
                              sortable: true,
                              selector: (akademi) => akademi.jml_tema,
                              cell: (akademi) => {
                                return (
                                  <span className="d-flex flex-column">
                                    <span className="fs-7">
                                      {akademi.jml_tema} Tema
                                    </span>
                                  </span>
                                );
                              },
                            },
                            {
                              Header: "Pelatihan",
                              accessor: "jml_pelatihan",
                              className: "min-w-100px",
                              // sortType: 'basic',
                              sortField: "jml_pelatihan",
                              allowOverflow: true,
                              name: "Pelatihan",
                              center: true,
                              sortable: true,
                              selector: (akademi) => akademi.jml_pelatihan,
                              grow: 2,
                              width: "150px",
                              cell: (akademi) => {
                                return (
                                  <span className="d-flex flex-column">
                                    <span className="fs-7">
                                      {akademi.jml_pelatihan} Pelatihan
                                    </span>
                                  </span>
                                );
                              },
                            },
                            {
                              Header: "Penyelenggara",
                              accessor: "penyelenggara",
                              className: "min-w-100px",
                              sortType: "basic",
                              center: true,
                              fontWeight: "bold",
                              allowOverflow: true,
                              name: "Penyelenggara",
                              sortField: "penyelenggara",
                              sortable: true,
                              grow: 3,
                              width: "150px",
                              selector: (akademi) => akademi.penyelenggara,
                              cell: (akademi) => {
                                return (
                                  <span className="d-flex flex-column">
                                    <span className="fs-7">
                                      {akademi.penyelenggara || 0} Penyelenggara
                                    </span>
                                  </span>
                                );
                              },
                            },
                            {
                              Header: "Mitra",
                              accessor: "mitra",
                              className: "min-w-100px",
                              sortType: "basic",
                              center: true,
                              fontWeight: "bold",
                              allowOverflow: true,
                              name: "Mitra",
                              sortField: "mitra",
                              sortable: true,
                              grow: 3,
                              width: "100px",
                              selector: (akademi) => akademi.mitra,
                              cell: (akademi) => {
                                return (
                                  <span className="d-flex flex-column">
                                    <span className="fs-7">
                                      {akademi.mitra} Mitra
                                    </span>
                                  </span>
                                );
                              },
                            },
                            {
                              Header: "Status",
                              Title: "status",
                              accessor: "status",
                              allowOverflow: true,
                              center: true,
                              className: "min-w-100px",
                              name: "Status",
                              sortField: "status",
                              sortable: true,
                              sortType: "basic",
                              grow: 2,
                              selector: (akademi) => akademi.status,
                              cell: (akademi) => {
                                return (
                                  <span
                                    className={
                                      "badge badge-light-" +
                                      (akademi.status == "publish" ||
                                      akademi.status == "1"
                                        ? "success"
                                        : akademi.status == "unpublish" ||
                                            akademi.status == "0"
                                          ? "danger"
                                          : "warning") +
                                      " fs-7 m-1"
                                    }
                                  >
                                    {akademi.status == "publish" ||
                                    akademi.status == 1
                                      ? "Publish"
                                      : akademi.status == "unpublish" ||
                                          akademi.status == "0"
                                        ? "Unpublish"
                                        : "Unpublish"}
                                  </span>
                                );
                              },
                            },
                            {
                              Header: "Aksi",
                              accessor: "actions",
                              name: "Aksi",
                              grow: 2,
                              center: true,
                              allowOverflow: true,
                              sortable: false,
                              selector: (akademi) => {
                                return (
                                  <div>
                                    <a
                                      href={
                                        "/pelatihan/akademi/preview/" +
                                        akademi.id
                                      }
                                      className="btn btn-icon btn-bg-primary btn-sm me-1"
                                      title="Lihat"
                                      alt="Lihat"
                                    >
                                      <i className="bi bi-eye-fill text-white"></i>
                                    </a>
                                    {cekPermition().manage === 1 ? (
                                      <a
                                        href={
                                          "/pelatihan/akademi/content/" +
                                          akademi.id
                                        }
                                        className="btn btn-icon btn-bg-warning btn-sm me-1"
                                        title="Edit"
                                        alt="Edit"
                                      >
                                        <i className="bi bi-gear-fill text-white"></i>
                                      </a>
                                    ) : (
                                      <></>
                                    )}

                                    {cekPermition().manage === 1 ? (
                                      <button
                                        className={`btn btn-icon btn-bg-danger btn-sm me-1 ${
                                          akademi.jml_tema ||
                                          akademi.jml_pelatihan
                                            ? "disabled"
                                            : ""
                                        }`}
                                        title="Hapus"
                                        onClick={() =>
                                          this.onRemove(akademi?.id)
                                        }
                                        alt="Hapus"
                                      >
                                        <i className="bi bi-trash-fill text-white"></i>
                                      </button>
                                    ) : (
                                      <></>
                                    )}
                                  </div>
                                );
                              },
                            },
                          ]}
                          data={this.state.data}
                          progressPending={this.state.loading}
                          progressComponent={
                            <div style={{ padding: "24px" }}>
                              <img src="/assets/media/loader/loader-biru.gif" />
                            </div>
                          }
                          highlightOnHover
                          pointerOnHover
                          pagination
                          paginationServer={true}
                          paginationTotalRows={this.state.total}
                          paginationComponentOptions={{
                            selectAllRowsItem: true,
                            selectAllRowsItemText: "Semua",
                          }}
                          onChangeRowsPerPage={this.onChangeRowsPerPage}
                          onChangePage={this.onChangePage}
                          customStyles={{
                            headCells: {
                              style: {
                                background: "rgb(243, 246, 249)",
                                fontWeight: "bold",
                              },
                            },
                          }}
                          persistTableHead={true}
                          noDataComponent={
                            <div className="mt-5">Tidak Ada Data</div>
                          }
                          onSort={this.onSort}
                          sortServer
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {this.renderModalFilter()}
      </div>
    );
  }

  renderModalFilter() {
    return (
      <div className="modal fade" tabindex="-1" id="filter">
        <div className="modal-dialog modal-md">
          <div className="modal-content">
            <div className="modal-header py-3">
              <h5 className="modal-title">
                <span className="svg-icon svg-icon-5 me-1">
                  <i className="bi bi-sliders text-black"></i>
                </span>
                Filter Data Akademi
              </h5>
              <div
                className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal"
                aria-label="Close"
              >
                <span className="svg-icon svg-icon-2x">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <rect
                      opacity="0.5"
                      x="6"
                      y="17.3137"
                      width="16"
                      height="2"
                      rx="1"
                      transform="rotate(-45 6 17.3137)"
                      fill="currentColor"
                    />
                    <rect
                      x="7.41422"
                      y="6"
                      width="16"
                      height="2"
                      rx="1"
                      transform="rotate(45 7.41422 6)"
                      fill="currentColor"
                    />
                  </svg>
                </span>
              </div>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-lg-12 mb-7 fv-row">
                  <label className="form-label">Status</label>
                  <Select
                    name="level_pelatihan"
                    placeholder="Silahkan pilih"
                    value={this.state.statusList.find(
                      ({ value }) => value == this.state.selectedStatus,
                    )}
                    onChange={({ value }) =>
                      this.setState({ selectedStatus: value })
                    }
                    className="form-select-sm selectpicker p-0"
                    options={this.state.statusList}
                  />
                </div>
              </div>
            </div>
            <div className="modal-footer py-3">
              <div className="d-flex justify-content-between">
                <button
                  type="button"
                  className="btn btn-sm btn-light me-3"
                  onClick={() => this.onFilterReset()}
                >
                  Reset
                </button>
                <button
                  type="button"
                  className="btn btn-sm btn-primary"
                  onClick={(e) => this.onFilterFilter(e)}
                >
                  Apply Filter
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

class AkademiList extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Header />
        <SideNav />
        <Content />
        <Footer />
      </div>
    );
  }
}

export default AkademiList;
