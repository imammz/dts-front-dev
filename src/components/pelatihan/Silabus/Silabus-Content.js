import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import DataTable from "react-data-table-component";
import { capitalWord, zeroDecimalValue } from "../Pelatihan/helper";
import { cekPermition, isExceptionRole } from "../../AksesHelper";

export default class SilabusContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleChangeSearch = this.handleChangeSearchAction.bind(this);
    this.handleClickReset = this.handleClickResetAction.bind(this);
    this.handleChangeAkademi = this.handleChangeAkademiAction.bind(this);
    this.handleChangeTema = this.handleChangeTemaAction.bind(this);
    this.handleSort = this.handleSortAction.bind(this);
    this.pratinjauTriggerRef = React.createRef(null);
  }
  state = {
    datax: [],
    dataxsilabus: [],
    dataxakademivalue: [],
    dataxtemavalue: [],
    dataxakademi: [],
    dataxtema: [],
    currentPage: 1,
    dataxsilabusvalue: "",
    dataxstatusvalue: "",
    dataxsortvalue: "desc",
    dataxsortby: "id",
    loading: true,
    rowSkeletons: 20,
    dataxsearch: "",
    newPerPage: 10,
    numberrow: 0,
    from: "home",
    param: "",
    isDisabled: true,
    pratinjau: null,
    selected: null,
  };
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  optionstatus = [
    { value: "1", label: "Publish" },
    { value: "0", label: "Unpublish" },
  ];
  customLoader = (
    <div style={{ padding: "24px" }}>
      <img src="/assets/media/loader/loader-biru.gif" />
    </div>
  );
  columns = [
    {
      name: "No",
      center: true,
      width: "70px",
      cell: (row, index) => (
        <div>
          <span>{this.state.numberrow + index + 1}</span>
        </div>
      ),
    },
    {
      name: "Nama Silabus",
      className: "min-w-300px mw-300px",
      sortable: true,
      grow: 6,
      selector: (row) => capitalWord(row.judul_silabus),
      cell: (row) => (
        <div>
          <a
            href="#"
            onClick={() => {
              this.handlePratinjauClick(row);
            }}
            title="Preview"
            className="text-dark"
          >
            <span className="fs-7 fw-semibold text-muted d-block">
              ID : {row.id}
            </span>
            <span className="fw-bolder y-2 fs-7">
              {capitalWord(row.judul_silabus)}
            </span>
            <br />
            <span className="fs-7 fw-semibold text-muted">
              {row.nama_akademi}
            </span>
          </a>
        </div>
      ),
    },
    {
      name: "Akademi",
      sortable: true,
      center: true,
      className: "min-w-100px mw-100px",
      grow: 2,
      selector: (row) => capitalWord(row.jml_akademi),
      cell: (row) => (
        <span className="badge badge-light-primary fs-7 m-1">
          {row.jml_akademi}
        </span>
      ),
    },
    {
      name: "Tema",
      sortable: true,
      center: true,
      className: "min-w-100px mw-100px",
      grow: 2,
      selector: (row) => capitalWord(row.jml_tema),
      cell: (row) => (
        <span className="badge badge-light-primary fs-7 m-1">
          {row.jml_tema}
        </span>
      ),
    },
    {
      name: "Pelatihan",
      sortable: true,
      center: true,
      grow: 2,
      className: "min-w-100px mw-100px",
      selector: (row) => row.jml_pelatihan,
      cell: (row) => (
        <span className="badge badge-light-primary fs-7 m-1">
          {row.jml_pelatihan}
        </span>
      ),
    },
    {
      name: "Total JP",
      sortable: true,
      center: true,
      grow: 2,
      className: "min-w-100px mw-100px",
      selector: (row) => (
        <span className="badge badge-light-primary fs-7 m-1">
          {row.jml_jp - Math.round(row.jml_jp) == 0
            ? Math.round(row.jml_jp)
            : row.jml_jp}
        </span>
      ),
      // cell: row => (
      //   <div>
      //     <span className={"badge badge-light-" + (row.status == 1 ? "success" : (row.status == 0 ? "danger" : "warning")) + " fs-7 m-1"}>{row.status==1 ? 'Publish' : row.status==0 ? 'Unpublish' : 'Unlisted'}</span>
      //   </div>
      // ),
    },
    {
      name: "Aksi",
      center: true,
      grow: 3,
      cell: (row) => (
        <div className="row">
          <a
            href="#"
            onClick={() => {
              this.handlePratinjauClick(row);
            }}
            title="Preview"
            className="btn btn-icon btn-bg-primary btn-sm me-1"
            alt="Lihat"
          >
            <i className="bi bi-eye-fill text-white"></i>
          </a>

          {cekPermition().manage === 1 && row.jml_pelatihan < 1 ? (
            <>
              {isExceptionRole() && (
                <a
                  href={"/pelatihan/edit-silabus/" + row.id}
                  title="Edit Silabus"
                  className="btn btn-icon btn-bg-warning btn-sm me-1"
                  alt="Lihat"
                >
                  <i className="bi bi-gear-fill text-white"></i>
                </a>
              )}
              {isExceptionRole() && (
                <a
                  href="#"
                  title="Hapus Silabus"
                  onClick={() => {
                    this.handleClickDeleteAction(row.id);
                  }}
                  className="btn btn-icon btn-bg-danger btn-sm me-1"
                  alt="Delete Silabus"
                >
                  <i className="bi bi-trash-fill text-white"></i>
                </a>
              )}
            </>
          ) : (
            <>
              <a
                href="#"
                title="Edit Silabus"
                className="btn disabled btn-icon btn-bg-warning btn-sm me-1"
                alt="Lihat"
              >
                <i className="bi bi-gear-fill text-white"></i>
              </a>
              <a
                href="#"
                title="Hapus Silabus"
                className="btn disabled btn-icon btn-bg-danger btn-sm me-1"
                alt="Delete Silabus"
              >
                <i className="bi bi-trash-fill text-white"></i>
              </a>
            </>
          )}
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };
  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    const dataAkademik = { start: 0, length: 100, status: "publish" };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/survey/list-akademi",
        dataAkademik,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxakademi = [];
        dataxakademi.push({ value: "", label: "Semua" });
        optionx.map((data) =>
          dataxakademi.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxakademi });
      });
    this.handleReload("", this.state.param, 1, 10);
  }
  handleChangeTemaAction = (tema_id) => {
    let dataxtemavalue = tema_id;
    console.log(dataxtemavalue);
    this.setState({ dataxtemavalue });
  };
  handleChangeAkademiAction = (akademi_id) => {
    let dataxakademivalue = akademi_id;
    this.setState({ dataxakademivalue });
    const dataBody = { start: 1, rows: 100, id: akademi_id.value };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/cari_tema_byakademi",
        dataBody,
        this.configs,
      )
      .then((res) => {
        this.setState({ isDisabled: false });
        const optionx = res.data.result.Data;
        if (!optionx[0]) {
          throw Error(res.data.result.Message);
        }
        const dataxtema = [];
        dataxtema.push({ value: "", label: "Semua" });
        optionx.map((data) =>
          dataxtema.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxtema });
        this.setState({ dataxtemavalue: [] });
      })
      .catch((error) => {
        const dataxtema = [];
        this.setState({ dataxtema });
        console.log(error);
      });
  };

  handleSortAction(column, sortDirection) {
    let server_name = "";
    if (column.name == "Nama Silabus") {
      server_name = "title";
    } else if (column.name == "Akademi") {
      server_name = "jml_akademi";
    } else if (column.name == "Tema") {
      server_name = "jml_tema";
    } else if (column.name == "Pelatihan") {
      server_name = "jml_pelatihan";
    } else if (column.name == "Total JP") {
      server_name = "jml_jp";
    }

    this.setState(
      {
        dataxsortvalue: sortDirection,
        dataxsortby: server_name,
      },
      () => {
        this.handleReload("", "", 1, this.state.newPerPage);
      },
    );
  }

  handleClickDeleteAction(silabusId) {
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "info",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/del_silabus",
              { id: silabusId, user_by: Cookies.get("user_id") },
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (true == statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload(
                        "",
                        this.state.param,
                        1,
                        this.state.newPerPage,
                      );
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload(
                        "",
                        this.state.param,
                        1,
                        this.state.newPerPage,
                      );
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.setState({ datax: [] });
                      this.setState({ loading: false });
                      this.handleReload(
                        "",
                        this.state.param,
                        1,
                        this.state.newPerPage,
                      );
                    }
                  });
              }
            });
        }
      });
  }

  handleReload(from, param = "", page, newPerPage) {
    console.log(
      "from :" +
        from +
        ", param :" +
        param +
        ", page :" +
        page +
        ", newPerPage :" +
        newPerPage,
    );
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * (page - 1);
      // start_tmp = start_tmp - length_tmp + 1;
      this.setState({ numberrow: start_tmp });
    } else {
      this.setState({ numberrow: 0 });
    }

    let dataBody = {
      mulai: start_tmp,
      limit: length_tmp,
      id_akademi: this.state.dataxakademivalue.value
        ? this.state.dataxakademivalue.value
        : "",
      id_tema: this.state.dataxtemavalue.value
        ? this.state.dataxtemavalue.value
        : "",
      cari: param,
      sort_by: this.state.dataxsortby,
      sort_val: this.state.dataxsortvalue,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/silabus_filter",
        dataBody,
        this.configs,
      )
      .then((res) => {
        const datax = res.data.result.Data;
        datax.map((data, index) => {
          return {
            ...data,
            id_silabus: data.id,
            id: data + "_" + index + "_" + new Date().getMilliseconds(),
          };
        });
        this.setState({ datax });
        this.setState({ loading: false });
        this.setState({ totalRows: res.data.result.TotalData });
        this.setState({ currentPage: page });
      })
      .catch((error) => {
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      });
  }

  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    this.setState({ param: searchText });
    if (e.key === "Enter") {
      this.setState({ numberrow: 0 });
      this.setState({ currentPage: 1 });
      this.setState({ issearch: true });
      this.setState(
        { loading: true, dataxsortvalue: "desc", dataxsortby: "id" },
        () => {
          this.handleReload("", searchText, 1, this.state.newPerPage);
        },
      );
    }
  }
  handleClickResetAction(e) {
    e.preventDefault();
    this.setState(
      {
        numberrow: 0,
        dataxakademivalue: [],
        dataxtemavalue: [],
      },
      () => {
        // this.setState({ isfilter: false });
        this.handleReload("", this.state.param, 1, 10);
      },
    );
  }
  handlePageChange = (page) => {
    this.handleReload("", "", page, this.state.newPerPage);
  };
  handlePerRowsChange = async (newPerPage, page) => {
    this.setState({ newPerPage: newPerPage }, () => {
      this.handleReload("", "", page, newPerPage);
    });
    // this.setState({ numberrow: 1 });
  };
  handleChangeSearchAction(e) {
    const searchText = e.currentTarget.value;
    this.setState({ numberrow: 0 });
    if (searchText == "") {
      this.setState({ loading: true }, () => {
        this.handleReload("", "", 1, this.state.newPerPage);
      });
    }
  }
  handlePratinjauClick(elem) {
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    let dataBody = {
      mulai: 0,
      limit: 9999,
      id_penyelenggara: 0,
      id_akademi: null,
      id_tema: null,
      status_substansi: 0,
      status_pelatihan: 0,
      status_publish: 99,
      provinsi: 0,
      param: "",
      sort: "id",
      sortval: "DESC",
      id_silabus: elem.id,
      tahun: new Date().getFullYear(),
    };
    this.setState({ selected: elem });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/detail_silabus",
        { id: elem.id },
        this.configs,
      )
      .then((res) => {
        const messagex = res.data.result.Message;
        if (res.data?.result?.Status) {
          this.setState(
            {
              pratinjau: {
                ...this.state.pratinjau,
                pelatihan: res.data.result.DataPelatihan.filter(
                  (element) =>
                    element.nama_pelatihan != null &&
                    element.nama_akademi != null &&
                    element.nama_tema != null,
                ),
                detail: res.data.result.DataSilabus,
              },
            },
            () => {
              swal.close();
              this.pratinjauTriggerRef.current.click();
            },
          );
        }
        // return axios.post(
        //   process.env.REACT_APP_BASE_API_URI + "/pelatihan",
        //   dataBody,
        //   this.configs
        // );
      })
      // .then((res) => {
      //   const messagex = res.data.result.Message;
      //   if (res.data?.result?.Status) {
      //     this.setState((prevState) => ({
      //       pratinjau: {
      //         ...prevState.pratinjau,
      //         pelatihan: res.data.result.Data,
      //       },
      //     }));
      //     swal.close();
      //     this.pratinjauTriggerRef.current.click();
      //   } else {
      //     throw Error(messagex);
      //   }
      // })
      .catch((err) => {
        console.log(err);
        const message =
          typeof err == "string"
            ? err
            : err.response.data?.result?.Message
              ? err.response.data?.result?.Message
              : "Terjadi kesalahan.";
        if (message.includes("Data Tidak Di Temukan")) {
          this.pratinjauTriggerRef.current.click();
          swal.close();
        } else {
          swal.fire({
            title: message,
            icon: "warning",
            confirmButtonText: "Ok",
          });
        }
      });
  }
  render() {
    return (
      <div>
        <div className="modal fade" tabIndex="-1" id="filter-modal">
          <div className="modal-dialog modal-lg">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">
                  <span className="svg-icon svg-icon-5 me-1">
                    <i className="bi bi-sliders text-black"></i>
                  </span>
                  Filter Silabus
                </h5>
                <div
                  className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <span className="svg-icon svg-icon-2x">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <rect
                        opacity="0.5"
                        x="6"
                        y="17.3137"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(-45 6 17.3137)"
                        fill="currentColor"
                      />
                      <rect
                        x="7.41422"
                        y="6"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(45 7.41422 6)"
                        fill="currentColor"
                      />
                    </svg>
                  </span>
                </div>
              </div>
              <form
                action="#"
                onSubmit={(e) => {
                  e.preventDefault();
                  this.handleReload("home", "", 1, 10);
                }}
              >
                <div className="modal-body">
                  <div className="row">
                    <div className="col-lg-12 mb-7 fv-row">
                      <label className="form-label">Akademi</label>
                      <Select
                        name="akademi_id"
                        placeholder="Silahkan pilih"
                        noOptionsMessage={({ inputValue }) =>
                          !inputValue
                            ? this.state.dataxakademi
                            : "Data tidak tersedia"
                        }
                        className="form-select-sm form-select-solid selectpicker p-0"
                        options={this.state.dataxakademi}
                        onChange={this.handleChangeAkademi}
                        value={this.state.dataxakademivalue}
                      />
                    </div>
                    <div className="col-lg-12 mb-7 fv-row">
                      <label className="form-label">Tema</label>
                      <Select
                        name="tema_id"
                        placeholder="Silahkan pilih"
                        noOptionsMessage={({ inputValue }) =>
                          !inputValue
                            ? this.state.dataxtema
                            : "Data tidak tersedia"
                        }
                        className="form-select-sm form-select-solid selectpicker p-0"
                        options={this.state.dataxtema}
                        isDisabled={this.state.isDisabled}
                        onChange={this.handleChangeTema}
                        value={this.state.dataxtemavalue}
                      />
                    </div>
                  </div>
                </div>
                <div className="modal-footer">
                  <div className="d-flex justify-content-between">
                    <button
                      type="reset"
                      className="btn btn-sm btn-light me-3"
                      onClick={this.handleClickReset}
                    >
                      Reset
                    </button>
                    <button
                      type="submit"
                      className="btn btn-sm btn-primary"
                      data-bs-dismiss="modal"
                    >
                      Apply Filter
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <a
          href="#"
          hidden
          data-bs-toggle="modal"
          data-bs-target="#pratinjau"
          ref={this.pratinjauTriggerRef}
        ></a>
        <div className="modal fade" tabindex="-1" id="pratinjau">
          <div className="modal-dialog modal-lg">
            <div className="modal-content">
              <div className="modal-header pe-0 pb-0">
                <div className="row w-100">
                  <div className="col-12 d-flex w-100 justify-content-between">
                    <h5 className="modal-title mt-5 mb-6">
                      {capitalWord(this.state.selected?.judul_silabus)}
                      <span className="text-muted d-block small">
                        ID : {this.state.selected?.id} | Total :{" "}
                        {zeroDecimalValue(this.state.selected?.jml_jp)} Jam
                        pelajaran
                      </span>
                    </h5>
                    <div
                      className="btn btn-icon btn-sm btn-active-light-primary"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                    >
                      <span className="svg-icon svg-icon-2x">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                          fill="none"
                        >
                          <rect
                            opacity="0.5"
                            x="6"
                            y="17.3137"
                            width="16"
                            height="2"
                            rx="1"
                            transform="rotate(-45 6 17.3137)"
                            fill="currentColor"
                          />
                          <rect
                            x="7.41422"
                            y="6"
                            width="16"
                            height="2"
                            rx="1"
                            transform="rotate(45 7.41422 6)"
                            fill="currentColor"
                          />
                        </svg>
                      </span>
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="d-grid">
                      <ul className="nav nav-tabs flex-nowrap text-nowrap">
                        <li className="nav-item">
                          <a
                            className="nav-link btn-flex btn btn-active-light active btn-color-gray-600 btn-active-color-primary rounded-bottom-0"
                            data-bs-toggle="tab"
                            href="#kt_tab_pane_1"
                          >
                            <span className="svg-icon svg-icon-2">
                              <span className="svg-icon svg-icon-primary">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  width="24px"
                                  height="24px"
                                  viewBox="0 0 24 24"
                                  version="1.1"
                                >
                                  <g
                                    stroke="none"
                                    stroke-width="1"
                                    fill="none"
                                    fill-rule="evenodd"
                                  >
                                    <rect x="0" y="0" width="24" height="24" />
                                    <path
                                      d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z"
                                      fill="#000000"
                                      opacity="0.3"
                                    />
                                    <path
                                      d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z"
                                      fill="#000000"
                                    />
                                    <rect
                                      fill="#000000"
                                      opacity="0.3"
                                      x="10"
                                      y="9"
                                      width="7"
                                      height="2"
                                      rx="1"
                                    />
                                    <rect
                                      fill="#000000"
                                      opacity="0.3"
                                      x="7"
                                      y="9"
                                      width="2"
                                      height="2"
                                      rx="1"
                                    />
                                    <rect
                                      fill="#000000"
                                      opacity="0.3"
                                      x="7"
                                      y="13"
                                      width="2"
                                      height="2"
                                      rx="1"
                                    />
                                    <rect
                                      fill="#000000"
                                      opacity="0.3"
                                      x="10"
                                      y="13"
                                      width="7"
                                      height="2"
                                      rx="1"
                                    />
                                    <rect
                                      fill="#000000"
                                      opacity="0.3"
                                      x="7"
                                      y="17"
                                      width="2"
                                      height="2"
                                      rx="1"
                                    />
                                    <rect
                                      fill="#000000"
                                      opacity="0.3"
                                      x="10"
                                      y="17"
                                      width="7"
                                      height="2"
                                      rx="1"
                                    />
                                  </g>
                                </svg>
                              </span>
                            </span>
                            <span className="d-flex flex-column align-items-start">
                              <span className="fs-4 fw-bold">
                                Detail Silabus
                              </span>
                            </span>
                          </a>
                        </li>
                        <li className="nav-item">
                          <a
                            className="nav-link btn btn-flex btn-active-light btn-color-gray-600 btn-active-color-success rounded-bottom-0"
                            data-bs-toggle="tab"
                            href="#kt_tab_pane_2"
                          >
                            <span className="svg-icon svg-icon-2">
                              <span className="svg-icon svg-icon-primary svg-icon-2x">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  width="24px"
                                  height="24px"
                                  viewBox="0 0 24 24"
                                  version="1.1"
                                >
                                  <g
                                    stroke="none"
                                    stroke-width="1"
                                    fill="none"
                                    fill-rule="evenodd"
                                  >
                                    <rect x="0" y="0" width="24" height="24" />
                                    <path
                                      d="M13.6855025,18.7082217 C15.9113859,17.8189707 18.682885,17.2495635 22,17 C22,16.9325178 22,13.1012863 22,5.50630526 L21.9999762,5.50630526 C21.9999762,5.23017604 21.7761292,5.00632908 21.5,5.00632908 C21.4957817,5.00632908 21.4915635,5.00638247 21.4873465,5.00648922 C18.658231,5.07811173 15.8291155,5.74261533 13,7 C13,7.04449645 13,10.79246 13,18.2438906 L12.9999854,18.2438906 C12.9999854,18.520041 13.2238496,18.7439052 13.5,18.7439052 C13.5635398,18.7439052 13.6264972,18.7317946 13.6855025,18.7082217 Z"
                                      fill="#000000"
                                    />
                                    <path
                                      d="M10.3144829,18.7082217 C8.08859955,17.8189707 5.31710038,17.2495635 1.99998542,17 C1.99998542,16.9325178 1.99998542,13.1012863 1.99998542,5.50630526 L2.00000925,5.50630526 C2.00000925,5.23017604 2.22385621,5.00632908 2.49998542,5.00632908 C2.50420375,5.00632908 2.5084219,5.00638247 2.51263888,5.00648922 C5.34175439,5.07811173 8.17086991,5.74261533 10.9999854,7 C10.9999854,7.04449645 10.9999854,10.79246 10.9999854,18.2438906 L11,18.2438906 C11,18.520041 10.7761358,18.7439052 10.4999854,18.7439052 C10.4364457,18.7439052 10.3734882,18.7317946 10.3144829,18.7082217 Z"
                                      fill="#000000"
                                      opacity="0.3"
                                    />
                                  </g>
                                </svg>
                              </span>
                            </span>
                            <span className="d-flex flex-column align-items-start">
                              <span className="fs-4 fw-bold">Pelatihan</span>
                            </span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              {this.state.pratinjau && (
                <div className="modal-body">
                  <div className="card">
                    <div className="tab-content" id="detail-silabus-tab">
                      <div
                        className="tab-pane fade show active"
                        id="kt_tab_pane_1"
                        role="tabpanel"
                      >
                        <div className="row justify-content-start">
                          <div className="col-12">
                            <ul className="list-group">
                              {this.state.pratinjau?.detail &&
                                this.state.pratinjau.detail.map((elem) => (
                                  <div
                                    className="d-flex align-items-center bg-light-primary rounded p-5 mb-2"
                                    key={`detail_${elem.id}`}
                                  >
                                    <span className="svg-icon svg-icon-primary me-5">
                                      <span className="svg-icon svg-icon-1">
                                        <svg
                                          width="24"
                                          height="24"
                                          viewBox="0 0 24 24"
                                          fill="none"
                                          xmlns="http://www.w3.org/2000/svg"
                                          className="mh-50px"
                                        >
                                          <path
                                            opacity="0.3"
                                            d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                                            fill="currentColor"
                                          ></path>
                                          <path
                                            d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                                            fill="currentColor"
                                          ></path>
                                        </svg>
                                      </span>
                                    </span>
                                    <div className="flex-grow-1 pe-2">
                                      <a
                                        href="#"
                                        className="fw-bold text-gray-800 text-hover-primary fs-6"
                                      >
                                        {elem.materi ?? "Tidak ada data."}
                                      </a>
                                      {/* <span className="text-muted fw-semibold d-block">
                                        Due in 2 Days
                                      </span> */}
                                    </div>
                                    <div
                                      className="fw-bold text-primary py-1 text-end"
                                      style={{ width: "50px" }}
                                    >
                                      {elem.jam_pelajaran ?? 0} JP
                                    </div>
                                  </div>
                                ))}
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div
                        className="tab-pane fade"
                        id="kt_tab_pane_2"
                        role="tabpanel"
                      >
                        <div className="row justify-content-start">
                          <div className="col-12">
                            {this.state.pratinjau?.pelatihan &&
                              this.state.pratinjau?.pelatihan[0] != null && (
                                <span className="text-muted"></span>
                              )}
                          </div>
                          <div className="col-12">
                            {this.state.pratinjau?.pelatihan?.length > 0 ? (
                              this.state.pratinjau?.pelatihan.map((elem) => (
                                <div className="d-flex align-items-center bg-light rounded p-5 mb-2">
                                  <span className="svg-icon svg-icon-success me-5">
                                    <span className="svg-icon svg-icon-1">
                                      <svg
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                        xmlns="http://www.w3.org/2000/svg"
                                        className="mh-50px"
                                      >
                                        <path
                                          opacity="0.3"
                                          d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                                          fill="currentColor"
                                        ></path>
                                        <path
                                          d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                                          fill="currentColor"
                                        ></path>
                                      </svg>
                                    </span>
                                  </span>
                                  <div className="flex-grow-1 me-2">
                                    <a
                                      href="#"
                                      className="fw-bold text-muted text-hover-success fs-7"
                                    >
                                      {elem.nama_akademi} - {elem.nama_tema}
                                    </a>
                                    <span className="text-gray-800 fw-bold d-block">
                                      {`${elem.nama_pelatihan}`}
                                    </span>
                                  </div>
                                </div>
                              ))
                            ) : (
                              <div className="flex-grow-1 me-2">
                                <span className="text-muted fw-semibold d-block">
                                  Tidak ada pelatihan yang menggunakan silabus.
                                </span>
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )}

              <div className="modal-footer">
                <button
                  className="btn btn-light btn-sm"
                  data-bs-dismiss="modal"
                >
                  Tutup
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Silabus
                </h1>
              </div>
            </div>
            {isExceptionRole() && (
              <div className="d-flex align-items-end my-2">
                <div>
                  <button
                    className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                    data-bs-toggle="modal"
                    data-bs-target="#filter-modal"
                  >
                    <i className="bi bi-sliders"></i>
                    <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                      Filter
                    </span>
                  </button>
                  <a
                    className="btn btn-success fw-bolder btn-sm"
                    href="/pelatihan/create-silabus"
                  >
                    <i className="bi bi-plus-circle text-hover-primary"></i>
                    <span className="d-md-inline d-lg-inline d-xl-inline ms-1 d-none">
                      Tambah Silabus
                    </span>
                  </a>
                </div>
              </div>
            )}
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Silabus Pelatihan
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          {/* <div> */}
                          <span className="svg-icon svg-icon-1 position-absolute ms-6">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                              className="mh-50px"
                            >
                              <rect
                                opacity="0.5"
                                x="17.0365"
                                y="15.1223"
                                width="8.15546"
                                height="2"
                                rx="1"
                                transform="rotate(45 17.0365 15.1223)"
                                fill="#a1a5b7"
                              ></rect>
                              <path
                                d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                fill="#a1a5b7"
                              ></path>
                            </svg>
                          </span>
                          <input
                            type="text"
                            className="form-control form-control-sm form-control-solid w-250px ps-14"
                            placeholder="Cari Silabus"
                            onKeyPress={this.handleKeyPress}
                            onChange={this.handleChangeSearch}
                          />
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="table-responsive">
                          <DataTable
                            columns={this.columns}
                            data={this.state.datax}
                            progressPending={this.state.loading}
                            progressComponent={this.customLoader}
                            highlightOnHover
                            pointerOnHover
                            pagination
                            paginationServer
                            paginationTotalRows={this.state.totalRows}
                            paginationComponentOptions={{
                              selectAllRowsItem: true,
                              selectAllRowsItemText: "Semua",
                            }}
                            paginationDefaultPage={this.state.currentPage}
                            onChangeRowsPerPage={this.handlePerRowsChange}
                            onChangePage={this.handlePageChange}
                            customStyles={this.customStyles}
                            persistTableHead={true}
                            noDataComponent={
                              <div className="mt-5">Tidak Ada Data</div>
                            }
                            sortServer
                            onSort={this.handleSort}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
