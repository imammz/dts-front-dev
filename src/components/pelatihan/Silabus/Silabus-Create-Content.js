import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import { numericOnly } from "../../publikasi/helper";
import moment from "moment";
import { capitalWord, zeroDecimalValue } from "../Pelatihan/helper";
import Select from "react-select";

export default class SilabusContent extends React.Component {
  constructor(props) {
    super(props);
    this.viewPelatihanRef = React.createRef(null);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleClickAddSilabus = this.handleClickAddSilabusAction.bind(this);
    this.handleClickDeleteSilabus =
      this.handleClickDeleteSilabusAction.bind(this);
    this.handleInputSilabus = this.handleInputSilabusAction.bind(this);
    this.handleClickSaveSilabus = this.handleClickSaveSilabusAction.bind(this);
    this.handleChangeAkademi = this.handleChangeAkademiAction.bind(this);
    this.state = {
      fields: {},
      errors: {},
      mSilabus: {},
      silabusList: [
        {
          id: 1,
          jp_silabus: "",
          isi_silabus: "",
        },
      ],
      akademi: null,
      isLoading: false,
      pelatihan: [],
      dataxakademi: [],
    };
  }
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  componentDidMount() {
    moment.locale("id");
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/akademi/list-akademi",
        { start: 0, length: 1000, status: "publish" },
        this.configs,
      )
      .then((res) => {
        swal.close();
        const dataxakademi = [];
        if (res.data.result.Status && res.data.result.Data[0] != null) {
          res.data.result.Data.forEach((dat) => {
            dataxakademi.push({
              value: dat.id,
              label: dat.name,
            });
          });
          this.setState({ dataxakademi });
        } else {
          throw new Error(res.data.result.Message);
        }
      })
      .catch((err) => {
        swal.fire({
          title:
            err.response?.data?.result?.Message ??
            "Terjadi kesalahan saat mengambil akademi",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      });
  }
  handleChangeAkademiAction = (e) => {
    this.emptyValidation(e.value, "akademi")
      .then((value) => {
        this.setState({
          mSilabus: { ...this.state.mSilabus, akademi: e },
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  handleClickSaveSilabusAction = (e) => {
    e.preventDefault();
    // create silabus data
    const jsonBody = [
      {
        jml_jp: this.state.mSilabus.totalJp,
        judul: this.state.mSilabus.nama,
        id_user: Cookies.get("user_id"),
        id_akademi: this.state.mSilabus.akademi?.value,
        detail_json: this.state.silabusList.map((elem) => {
          return {
            name: elem.isi_silabus,
            jam_pelajaran: elem.jp_silabus,
          };
        }),
      },
    ];
    this.validate(e.target).then((result) => {
      swal.close();
      if (result) {
        swal
          .fire({
            title: "Apakah anda yakin?",
            text: "Pastikan isian data Silabus telah benar, Silabus tidak dapat diubah jika nantinya telah digunakan pada Pelatihan",
            icon: "info",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Ya, Simpan",
            cancelButtonText: "Batal",
          })
          .then((result) => {
            if (result.isConfirmed) {
              if (result) {
                this.setState({ isLoading: true });
                swal.fire({
                  title: "Mohon Tunggu!",
                  icon: "info", // add html attribute if you want or remove
                  allowOutsideClick: false,
                  didOpen: () => {
                    swal.showLoading();
                  },
                });
                axios
                  .post(
                    process.env.REACT_APP_BASE_API_URI + "/create_silabus",
                    jsonBody,
                    this.configs,
                  )
                  .then((res) => {
                    this.setState({ isLoading: false });
                    swal
                      .fire({
                        title:
                          res.data.result.Message ?? "Berhasil menyimpan data!",
                        allowOutsideClick: false,
                        icon: "success",
                      })
                      .then((result) => {
                        if (result.isConfirmed) {
                          window.location.href = "/pelatihan/silabus";
                        }
                      });
                  })
                  .catch((err) => {
                    this.setState({ isLoading: false });
                    swal.fire({
                      title:
                        err.response.data.result.Message ??
                        "Terjadi kesalahan!",
                      icon: "warning",
                    });
                  });
              } else {
                this.setState({ isLoading: false });
                swal.fire({
                  title: "Masih terdapat kesalahan pada isian",
                  icon: "warning",
                  confirmButtonText: "Ok",
                });
              }
            }
          });
      }
    });
  };

  handleDeleteSilabus(silabusId) {
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "info",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/del_silabus",
              { id: silabusId, user_by: Cookies.get("user_id") },
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (true == statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    allowOutsideClick: false,
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      window.location = "/pelatihan/silabus";
                    }
                  });
              } else {
                swal.fire({
                  title: messagex,
                  icon: "warning",
                  confirmButtonText: "Ok",
                });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal.fire({
                  title: messagex,
                  icon: "warning",
                  confirmButtonText: "Ok",
                });
              }
            });
        }
      });
  }

  async validate(formElement) {
    const inputs = formElement.querySelectorAll("input");
    inputs.forEach((input) => {
      input.focus();
      input.blur();
    });
    await this.emptyValidation(this.state.mSilabus?.akademi?.value, "akademi");
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(Object.keys(this.state.errors).length == 0);
      }, inputs.length * 50);
    });
  }
  emptyValidation(value, key) {
    const errors = this.state.errors;
    if (value == "" || value == null) {
      errors[key] = "Tidak boleh kosong";
      this.setState({ errors });
      return Promise.reject("Tidak boleh kosong");
    } else {
      delete errors[key];
      this.setState({ errors });
      return Promise.resolve(value);
    }
  }
  positivenumberValidation(value, key) {
    const errors = this.state.errors;
    if (!parseFloat(value) || value.split(".").length > 2) {
      errors[key] = "Input harus berupa Angka.";
      this.setState({ errors });
      // window?.document?.querySelectorAll('[name="jp_silabus"]');
      return Promise.reject("Input harus berupa Angka.");
      // } else if (parseFloat(value) > 100) {
      //   errors[key] = "Input tidak boleh melebihi 100";
      //   this.setState({ errors });
      //   window?.document?.querySelectorAll('[name="jp_silabus"]');
      //   return Promise.reject("Input tidak boleh melebihi 100");
    } else if (parseFloat(value, 0) <= 0) {
      errors[key] = "Angka harus lebih dari 0";
      this.setState({ errors });
      // window?.document?.querySelectorAll('[name="jp_silabus"]');
      return Promise.reject("Angka harus lebih dari 0");
    } else {
      delete errors[key];
      this.setState({ errors });
      return Promise.resolve(value);
    }
  }
  jpsilabusValidation(value, key, sub, ref) {
    const errors = this.state.errors;
    const jpArr = window?.document?.querySelectorAll('[name^="' + sub + '"]');
    const totalJP = window?.document?.querySelector(
      '[name="' + ref + '"]',
    ).value;
    let jumlahJP = 0;
    jpArr?.forEach((el) => {
      if (el.value != "") {
        jumlahJP += parseFloat(el.value, 0);
      }
    });
    if (jumlahJP != totalJP) {
      this.setState({ errors });
      jpArr.forEach((el, index) => {
        errors[el.name] =
          "Jumlah jam pelajaran harus sama dengan total jam pelajaran";
        this.setState({ errors });
      });
      return Promise.reject(
        "Jumlah jam pelajaran harus sama dengan total jam pelajaran",
      );
    } else {
      jpArr.forEach((el, index) => {
        delete errors[el.name];
        this.setState({ errors });
      });
      return Promise.resolve(value);
    }
  }
  resetError() {
    let errors = {};
    errors["name"] = "";
    errors["deskripsi"] = "";
    this.setState({ errors: errors });
  }
  handleClickBatalAction(e) {
    swal
      .fire({
        title: "Apakah Anda Yakin?",
        icon: "warning",
        text: "Perubahan tidak akan tersimpan!",
        confirmButtonText: "Ya",
        showCancelButton: true,
        cancelButtonText: "Tidak",
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
      })
      .then((result) => {
        if (result.isConfirmed) {
          window.location = "/pelatihan/silabus";
        }
      });
  }

  handleClickAddSilabusAction = () => {
    const lastId = this.state.silabusList[this.state.silabusList.length - 1].id;
    this.setState({
      silabusList: [
        ...this.state.silabusList,
        {
          id: lastId + 1,
          jp_silabus: "",
          isi_silabus: "",
        },
      ],
    });
  };
  handleInputSilabusAction = (field, payload, id) => {
    this.state.silabusList.forEach((elem) => {
      if (elem.id == id) {
        return (elem[field] = payload);
      }
    });
  };

  handleClickDeleteSilabusAction = (index) => {
    let fields = this.state.silabusList;
    fields.splice(index, 1);
    this.setState({ silabusList: fields });
  };

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Silabus
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
                <a
                  href="#"
                  onClick={() => {
                    // this.handleDeleteSilabus(this.state.silabusId);
                  }}
                  className="btn btn-sm btn-danger btn-active-light-info me-2"
                >
                  <i className="bi bi-trash-fill text-white"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-5">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Tambah Silabus
                          </h1>
                        </div>
                      </div>
                      <div className="card-body">
                        <form action="#" onSubmit={this.handleClickSaveSilabus}>
                          <div className="row mt-6 pb-5">
                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Nama Silabus
                              </label>
                              <input
                                type="text"
                                className="form-control form-control-sm"
                                placeholder="Masukkan nama silabus"
                                name="nama_silabus"
                                key="nama_silabus"
                                defaultValue={this.state.mSilabus.nama}
                                onBlur={(e) => {
                                  this.emptyValidation(
                                    e.target.value,
                                    "nama_silabus",
                                  )
                                    .then((value) => {
                                      this.setState({
                                        mSilabus: {
                                          ...this.state.mSilabus,
                                          nama: value,
                                        },
                                      });
                                    })
                                    .catch((err) => {
                                      console.log(err);
                                    });
                                }}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["nama_silabus"]}
                              </span>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Akademi
                              </label>
                              <Select
                                id="id_akademi"
                                name="akademi_id"
                                placeholder="Silahkan pilih"
                                noOptionsMessage={() => "Data tidak tersedia"}
                                className="form-select-sm selectpicker p-0"
                                options={this.state.dataxakademi}
                                value={this.state.mSilabus.akademi}
                                onChange={this.handleChangeAkademi}
                                disabled={this.state.dataxakademi.length == 0}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["akademi"]}
                              </span>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Total Jam Pelajaran (JP)
                              </label>
                              <input
                                className="form-control form-control-sm"
                                placeholder="Masukkan total jam pelajaran"
                                defaultValue={this.state.mSilabus.totalJp}
                                onBlur={(e) => {
                                  this.setState({
                                    mSilabus: {
                                      ...this.state.mSilabus,
                                      totalJp: e.target.value,
                                    },
                                  });
                                  this.emptyValidation(
                                    e.target.value,
                                    "total_jp",
                                  )
                                    .then((value) => {
                                      return this.positivenumberValidation(
                                        value,
                                        "total_jp",
                                      );
                                    })
                                    .then((value) => {
                                      return this.jpsilabusValidation(
                                        value,
                                        `jp_silabus`,
                                        "jp_silabus",
                                        "total_jp",
                                      );
                                    })
                                    .then((value) => {})
                                    .catch((err) => {
                                      console.log(err);
                                    });
                                }}
                                onChange={(e) => {
                                  numericOnly(e.target);
                                }}
                                id="total_jp"
                                name="total_jp"
                                key="total_jp"
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["total_jp"]}
                              </span>
                            </div>
                            <h6 className="my-5 d-flex align-items-center">
                              Daftar Silabus :
                            </h6>
                            {this.state.silabusList &&
                              this.state.silabusList.map((elem, index) => {
                                return (
                                  <div
                                    key={elem.id}
                                    className="row align-items-end mb-5 trigger-silabus"
                                  >
                                    <div
                                      className={`${
                                        index > 0 ? "col-lg-6" : "col-lg-6"
                                      } fw-row`}
                                    >
                                      <label className="form-label required">
                                        Judul Materi
                                      </label>
                                      <input
                                        className="form-control form-control-sm silabus"
                                        placeholder="Masukkan Judul Materi"
                                        defaultValue={elem.isi_silabus}
                                        onBlur={(evt) => {
                                          this.emptyValidation(
                                            evt.target.value,
                                            `jp_silabus_${elem.id}`,
                                          ).then((value) => {
                                            this.handleInputSilabus(
                                              "isi_silabus",
                                              value,
                                              elem.id,
                                            );
                                          });
                                        }}
                                        key={`isi_silabus_${elem.id}`}
                                        name={`isi_silabus`}
                                      />
                                      <span style={{ color: "red" }}>
                                        {
                                          this.state.errors[
                                            `isi_silabus_${elem.id}`
                                          ]
                                        }
                                      </span>
                                    </div>
                                    <div
                                      className={`${
                                        index > 0 ? "col-lg-5" : "col-lg-6"
                                      } fw-row`}
                                    >
                                      <label className="form-label required">
                                        JP Silabus
                                      </label>
                                      <input
                                        className="form-control form-control-sm silabus numeric-silabus"
                                        placeholder="Masukkan JP"
                                        key={`jp_silabus_${elem.id}`}
                                        id={`jp_silabus_${elem.id}`}
                                        name={`jp_silabus_${elem.id}`}
                                        defaultValue={elem.jp_silabus}
                                        onBlur={(e) => {
                                          this.handleInputSilabus(
                                            "jp_silabus",
                                            e.target.value,
                                            elem.id,
                                          );
                                          this.emptyValidation(
                                            e.target.value,
                                            `jp_silabus_${elem.id}`,
                                          )
                                            .then((value) => {
                                              return this.positivenumberValidation(
                                                value,
                                                `jp_silabus_${elem.id}`,
                                              );
                                            })
                                            .then((value) => {
                                              return this.jpsilabusValidation(
                                                value,
                                                `jp_silabus`,
                                                "jp_silabus",
                                                "total_jp",
                                              );
                                            })
                                            .then((value) => {})
                                            .catch((err) => {
                                              console.log(err);
                                            });
                                        }}
                                        onChange={(e) => {
                                          numericOnly(e.target);
                                        }}
                                      />
                                      <span style={{ color: "red" }}>
                                        {
                                          this.state.errors[
                                            `jp_silabus_${elem.id}`
                                          ]
                                        }
                                      </span>
                                    </div>
                                    {index != 0 && (
                                      <div className="col-lg-1">
                                        <div className="mt-7 fv-row">
                                          <a
                                            title="Hapus"
                                            onClick={() => {
                                              this.handleClickDeleteSilabusAction(
                                                index,
                                              );
                                            }}
                                            className="btn btn-icon btn-sm btn-danger w-lg-100"
                                          >
                                            <i className="bi bi-trash-fill"></i>
                                          </a>
                                        </div>
                                      </div>
                                    )}
                                  </div>
                                );
                              })}
                            <div className="row text-center">
                              <div className="col-lg-12 text-center mb-5">
                                <a
                                  onClick={this.handleClickAddSilabus}
                                  className="btn btn-light text-success btn-sm d-block fw-semibold me-3 mr-2"
                                >
                                  <i className="bi bi-plus-circle text-success me-1"></i>{" "}
                                  Tambah Materi
                                </a>
                              </div>
                              <div className="col-lg-12 text-center pt-7 mb-7">
                                <button
                                  onClick={this.handleClickBatal}
                                  type="reset"
                                  className="btn btn-md btn-light me-3"
                                  data-kt-menu-dismiss="true"
                                >
                                  Batal
                                </button>
                                <button
                                  disabled={this.state.isLoading}
                                  type="submit"
                                  className="btn btn-primary btn-md"
                                  id="simpan-silabus"
                                >
                                  {this.state.isLoading ? (
                                    <>
                                      <span
                                        className="spinner-border spinner-border-sm me-2"
                                        role="status"
                                        aria-hidden="true"
                                      ></span>
                                      <span className="sr-only">
                                        Loading...
                                      </span>
                                      Loading...
                                    </>
                                  ) : (
                                    <>
                                      <i className="fa fa-paper-plane me-1"></i>
                                      Simpan
                                    </>
                                  )}
                                </button>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
