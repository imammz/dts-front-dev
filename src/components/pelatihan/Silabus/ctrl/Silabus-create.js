import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../Silabus-Create-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const SilabusCreate = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default SilabusCreate;
