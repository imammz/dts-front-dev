import axios from "axios";
import Cookies from "js-cookie";
import Swal from "sweetalert2";

const configs = {
  headers: {
    Authorization: "Bearer " + Cookies.get("token"),
  },
};

const checkToken = () => {
  return new Promise(async (resolve, reject) => {
    if (Cookies.get("token") == null) {
      const { isConfirmed } = await Swal.fire({
        title: "Unauthenticated.",
        text: "Silahkan login ulang",
        icon: "warning",
        confirmButtonText: "Ok",
      });

      if (isConfirmed) return reject();
    }

    resolve();
  });
};

export const fetchDetailReportPelatihan = async ({ id }) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/r-detail-rekap-pendaftaran-byid",
            { id },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let {
              TotalLength,
              data_pelatihan = [],
              Status = false,
              Message = "",
            } = result || {};

            if (Status)
              resolve({
                totalData: TotalLength,
                header: data_pelatihan[0],
              });
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchPesertaReportPelatihan = async ({
  id,
  start = 0,
  length = 10,
}) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/pelatihan/detail-report-pelatihan-id",
            { id, start, length },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let {
              Total,
              Data = [],
              Status = false,
              Message = "",
            } = result || {};

            if (Status)
              resolve({
                totalData: Total,
                data: Data,
              });
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const resetErrorChangeSertifikasi = () => {
  return {
    statusSertifikasiError: null,
    sertifikatError: null,
  };
};

export const validateChangeSertifikasi = ({
  statusSertifikasi,
  sertifikat,
}) => {
  let error = {};

  if (!statusSertifikasi)
    error["statusSertifikasiError"] = "Status sertifikasi tidak boleh kosong";
  // if (statusSertifikasi == 1 && !sertifikat) error['sertifikatError'] = 'Sertifikat tidak boleh kosong';

  return error;
};

export const hasError = (error = {}) => {
  let _hasError = false;
  Object.keys(error).forEach((k) => {
    if (!error[k]) {
    } else if (Array.isArray(error[k]) || typeof error[k] == "object") {
      _hasError = _hasError || hasError(error[k]);
    } else {
      _hasError = _hasError || true;
    }
  });
  return _hasError;
};

export const uploadSertifikasi = async (payload) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/pelatihan/update-peserta-sertifikat-bulk",
            payload,
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let {
              Data = [],
              Status = false,
              StatusCode = "200",
              Message = "",
            } = result || {};

            if (StatusCode == "200") resolve(Message);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};
