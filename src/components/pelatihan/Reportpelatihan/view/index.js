import axios from "axios";
import * as FileSaver from "file-saver";
import Cookies from "js-cookie";
import moment from "moment";
import React from "react";
import DataTable from "react-data-table-component";
import { Link } from "react-router-dom";
import Select from "react-select";
import swal from "sweetalert2";
import * as XLSX from "xlsx";
import { formatIndonesianDateRange } from "../../FormHelper";
import {
  dateRangeWithSeparator,
  FRONT_URL,
  tagColorMapStatusPeserta,
} from "../../Pelatihan/helper";

import Footer from "../../../../components/Footer";
import Header from "../../../../components/Header";
import SideNav from "../../../../components/SideNav";
import ShowUUPdp from "./../../Rekappendaftaran/showUUPdp";
import Swal from "sweetalert2";
import {
  hasError,
  resetErrorChangeSertifikasi,
  uploadSertifikasi,
  validateChangeSertifikasi,
} from "../actions";
import ImportPeserta from "./import-peserta";
import "./style.scss";

function s2ab(s) {
  if (typeof ArrayBuffer !== "undefined") {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xff;
    return buf;
  } else {
    var buf = new Array(s.length);
    for (var i = 0; i != s.length; ++i) buf[i] = s.charCodeAt(i) & 0xff;
    return buf;
  }
}

class ModalUbahKelulusan extends React.Component {
  constructor(props) {
    super(props);
    this.ref = React.createRef();
  }

  save = async () => {
    try {
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info",
        allowOutsideClick: false,
        didOpen: () => Swal.showLoading(),
      });

      // const payload = new FormData();
      // payload.append("pelatihan_id", this?.props?.selectedUser?.pelatian_id);
      // payload.append("id_user", this?.props?.selectedUser?.user_id);
      // payload.append("status", this?.props?.statusSertifikasi);
      // payload.append("file", this?.props?.sertifikat);

      const b64cert = await new Promise((resolve, reject) => {
        if (this?.props?.sertifikat) {
          var reader = new FileReader();
          reader.onload = () => {
            resolve(reader.result);
          };
          reader.readAsDataURL(this?.props?.sertifikat);
        } else {
          resolve(null);
        }
      });

      const payload = {
        categoryOptItems: [
          {
            id_user: this?.props?.selectedUser?.user_id,
            sertifikat: this?.props?.statusSertifikasi,
            pelatihan_id: this?.props?.selectedUser?.pelatian_id,
            file_sertifikat: b64cert,
            updatedby: parseInt(Cookies.get("user_id")),
          },
        ],
      };

      const message = await uploadSertifikasi(payload);

      Swal.close();

      await Swal.fire({
        title: message || "Sertifikasi berhasil disimpan",
        icon: "success",
        confirmButtonText: "Ok",
      });

      window.location.reload();
    } catch (err) {
      Swal.close();
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  validateAndSave = (e) => {
    const errors = validateChangeSertifikasi(this.props);
    if (!hasError(errors)) {
      this?.props?.onChange({ ...resetErrorChangeSertifikasi() });
      this.save();
    } else {
      e.preventDefault();

      this?.props?.onChange({ ...resetErrorChangeSertifikasi(), ...errors });

      Swal.fire({
        title: "Masih terdapat isian yang belum lengkap",
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  render() {
    return (
      <div
        className="modal fade"
        tabindex="-1"
        id="upload_sertifikat"
        ref={this.ref}
      >
        <div className="modal-dialog modal-lg">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">
                <span className="svg-icon svg-icon-5 me-1">
                  <i className="bi bi-patch-plus text-black"></i>
                </span>
                Upload Sertifikat
              </h5>
              <div
                className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal"
                aria-label="Close"
              >
                <span className="svg-icon svg-icon-2x">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <rect
                      opacity="0.5"
                      x="6"
                      y="17.3137"
                      width="16"
                      height="2"
                      rx="1"
                      transform="rotate(-45 6 17.3137)"
                      fill="currentColor"
                    />
                    <rect
                      x="7.41422"
                      y="6"
                      width="16"
                      height="2"
                      rx="1"
                      transform="rotate(45 7.41422 6)"
                      fill="currentColor"
                    />
                  </svg>
                </span>
              </div>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-lg-12 mb-7 fv-row">
                  <label className="form-label">
                    <strong>{this?.props?.selectedUser?.name}</strong>
                  </label>
                  <div className="d-flex">
                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5 mb-3">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="status_sertifikat"
                        value="1"
                        id="status_sertifikasi1"
                        checked={this?.props?.statusSertifikasi == 1}
                        onChange={(e) =>
                          this?.props?.onChange({
                            statusSertifikasi: e.target.value,
                          })
                        }
                      />
                      <label
                        className="form-check-label"
                        for="status_sertifikasi1"
                      >
                        Lulus Sertifikasi / Kompeten
                      </label>
                    </div>
                  </div>
                  <div className="d-flex">
                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="status_sertifikat"
                        value="0"
                        id="status_sertifikasi2"
                        checked={this?.props?.statusSertifikasi == 0}
                        onChange={(e) =>
                          this?.props?.onChange({
                            statusSertifikasi: e.target.value,
                          })
                        }
                      />
                      <label
                        className="form-check-label"
                        for="status_sertifikasi2"
                      >
                        Tidak Lulus Sertifikasi / Belum Kompeten
                      </label>
                    </div>
                  </div>
                  <span style={{ color: "red" }}>
                    {this?.props?.statusSertifikasiError}
                  </span>
                </div>
                <div className="col-lg-12 mb-7 fv-row">
                  <label className="form-label">
                    Upload Sertifikasi (Optional)
                  </label>
                  <input
                    type="file"
                    className="form-control form-control-sm mb-2"
                    name="upload_sertifikat"
                    accept=".pdf"
                    onChange={(e) => {
                      const [file] = e.target.files;
                      this?.props?.onChange({ sertifikat: file });

                      // const reader = new FileReader();
                      // reader.addEventListener("load", () => {
                      //   this?.props?.onChange({ 'sertifikat': reader.result });
                      // }, false);
                      // reader.readAsDataURL(file);
                    }}
                  />
                  <small className="text-muted">
                    Format File (.pdf), Max 5 MB
                  </small>
                  <br />
                  <span style={{ color: "red" }}>
                    {this?.props?.sertifikatError}
                  </span>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <div className="d-flex justify-content-between">
                <button
                  type="submit"
                  className="btn btn-sm btn-primary"
                  onClick={(e) => this.validateAndSave(e)}
                >
                  Upload
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

class RekappendaftarandetailContent extends React.Component {
  constructor(props) {
    super(props);
    Cookies.remove("pelatian_id");
    this.updateModelCloseButtonRef = React.createRef(null);
    this.modalRef = React.createRef(null);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handlesubmitFilter = this.handlesubmitFilterAction.bind(this);
    this.handlesubmitFilterPending =
      this.handlesubmitFilterPendingAction.bind(this);
    this.exportPesertaTable = this.exportPesertaTableAction.bind(this);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleSearchEmpty = this.handleSearchEmptyAction.bind(this);
    this.handleChangeStatusTestSubstansi =
      this.handleChangeStatusTestSubstansiAction.bind(this);
    this.handleChangeStatusValidasi =
      this.handleChangeStatusValidasiAction.bind(this);
    this.handleChangeStatusBerkas =
      this.handleChangeStatusBerkasAction.bind(this);
    this.handleChangeStatusPeserta =
      this.handleChangeStatusPesertaAction.bind(this);
    this.handleChangeStatusPesertaUpdate =
      this.handleChangeStatusPesertaUpdateAction.bind(this);
    this.handleClickReset = this.handleClickResetAction.bind(this);
    this.handleClickResetPending =
      this.handleClickResetPendingAction.bind(this);
    this.handleSubmitUpdateStatusBulk =
      this.handleSubmitUpdateStatusBulkAction.bind(this);
    this.exportPesertaPendingTable = this.exportPesertaTableAction.bind(this);
  }
  state = {
    fields: {},
    errors: {},
    substansi_pelatihan: 0,
    datax: [],
    jadwalx: [],
    dataxpelatihan: [],
    titleheader: "",
    numberrow: 1,
    statistikx: null,
    page: 1,
    newPerPage: 10,
    newPerPagePending: 10,
    searchQuery: "",
    valStatusTestSubstansi: [],
    valStatusBerkas: [],
    valStatusPeserta: [],
    valStatusPesertaUpdate: [],
    loading: false,
    pendingTableLoading: false,
    selectedTableRows: [],
    filterOptions: [],
    errors: {},
    dataxpending: [],
    searchPendingQuery: "",
    totalPendingRow: "",
    sort: "name",
    sort_val: "asc",
    totalRows: 0,
    valStatusVlalidasi: [],
    importPeserta: false,
    showPdpModal: false,
    preferedExt: "csv",
  };
  capitalWord(str) {
    if (str && typeof str == "string") {
      return str.replace(/\w\S*/g, function (kata) {
        const kataBaru = kata.slice(0, 1).toUpperCase() + kata.substr(1);
        return kataBaru;
      });
    } else return str;
  }
  handleKeyPressAction(e) {
    if (e.key == "Enter") {
      if (e.target.name == "pendingSearch") {
        this.setState({ searchPendingQuery: e.target.value }, () => {
          this.handleReloadListUserPending(1, 10);
        });
      } else if (e.target.name == "search") {
        this.setState({ searchQuery: e.target.value }, () => {
          this.handleReloadListUser(1, 10);
        });
      }
    }
  }
  handleSearchEmptyAction(e) {
    if (e.target.value.length == 0 && e.target.name == "search") {
      this.setState({ searchQuery: "" }, () => {
        this.handleReloadListUser(1, 10);
      });
    } else if (e.target.value.length == 0 && e.target.name == "pendingSearch") {
      this.setState({ searchPendingQuery: "" }, () => {
        this.handleReloadListUserPending(1, 10);
      });
    }
  }
  handleClickResetAction() {
    this.setState(
      {
        valStatusBerkas: [],
        valStatusPeserta: [],
        valStatusTestSubstansi: [],
      },
      () => {
        this.handleReloadListUser();
      },
    );
  }
  handleClickResetPendingAction() {
    this.setState(
      {
        valStatusVlalidasi: [],
      },
      () => {
        this.handleReloadListUserPending();
      },
    );
  }

  handlePageChange = (page) => {
    this.setState({ tableLoading: true, page: page }, () => {
      this.handleReloadListUser(page, this.state.newPerPage);
    });
  };

  handlePendingPageChange = (page) => {
    this.setState({ pendingTableLoading: true });
    this.handleReloadListUserPending(page, this.state.newPerPagePending);
  };

  handlePerRowsChange = async (newPerPage, page) => {
    this.setState({ tableLoading: true, newPerPage: newPerPage }, () => {
      this.handleReloadListUser(this.state.page, this.state.newPerPage);
    });
  };

  handlePendingPerRowsChange = async (newPerPage, page) => {
    this.setState({ pendingTableLoading: true });
    this.setState({ newPerPagePending: newPerPage });
    this.handleReloadListUser(page, newPerPage);
  };

  handleSubmitUpdateStatusBulkAction(e) {
    e.preventDefault();
    const errors = {};
    if (this.state.selectedTableRows.length == 0) {
      errors["daftar_peserta"] = "Pilihan peseta tidak boleh kosong.";
    }
    if (!this.state.valStatusPesertaUpdate.value) {
      errors["status_peserta"] = "Status tidak boleh kosong.";
    }
    if (Object.keys(errors).length > 0) {
      this.setState({ errors: errors });
      return;
    }
    const payload = this.state.selectedTableRows.map((row) => {
      return {
        id_user: row.user_id,
        status: this.state.valStatusPesertaUpdate["value"] ?? "",
        pelatihan_id: this.state.dataxpelatihan.id,
        reminder_berkas: 0,
        reminder_profile: 0,
        reminder_riwayat: 1,
        reminder_dokumen: 1,
        updatedby: Cookies.get("user_id"),
      };
    });
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/rekappendaftaran/update-peserta-pendaftaran-bulk",
        { categoryOptItems: payload },
        this.configs,
      )
      .then((resp) => {
        swal
          .fire({
            title: "Update data berhasil",
            text: resp.data.result?.Message ?? "Berhasil melakukan update...",
            icon: "success",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
              this.handleReloadListUser();
            }
          });
        this.updateModelCloseButtonRef?.current.click();
      })
      .catch((err) => {
        console.error(err);
        swal
          .fire({
            title: "Terjadi kesalahan",
            text:
              err.data?.response?.result?.Message ??
              "Terjadi kesalahan sata melakukan update...",
            icon: "error",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
              this.handleReloadListUser();
            }
          });
      });
  }
  handleChangeStatusValidasiAction(selectedOption) {
    this.setState({
      valStatusVlalidasi: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  }

  handleChangeStatusTestSubstansiAction = (selectedOption) => {
    this.setState({
      valStatusTestSubstansi: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  };
  handleChangeStatusPesertaAction = (selectedOption) => {
    this.setState({
      valStatusPeserta: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  };
  handleChangeStatusPesertaUpdateAction = (selectedOption) => {
    this.setState({
      valStatusPesertaUpdate: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  };
  handleChangeStatusBerkasAction = (selectedOption) => {
    this.setState({
      valStatusBerkas: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  };

  handleChangeSort = (sortField, sortOrder) => {
    this.setState({ sort: sortField, sort_val: sortOrder }, () =>
      this.handleReloadListUser(),
    );
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  customLoader = (
    <div style={{ padding: "24px" }}>
      <img src="/assets/media/loader/loader-biru.gif" />
    </div>
  );

  pendingColumns = [
    {
      name: "No",
      center: true,
      width: "70px",
      cell: (row, index) => (
        <div>
          <span>{this.state.numberrow + index + 0}</span>
        </div>
      ),
    },
    {
      name: "Nama",
      sortable: false,
      cell: (row) => (
        <div>
          <span>
            <b>{this.capitalWord(row.nama)}</b>
          </span>
        </div>
      ),
    },
    {
      name: "NIK",
      sortable: false,
      center: true,
      selector: (row) => row.nik,
      width: "170px",
    },
    {
      name: "Nomor Handphone",
      sortable: false,
      center: true,
      width: "200px",
      cell: (row) => row.no_hp,
    },
    {
      name: "Email",
      sortable: false,
      width: "200px",
      cell: (row) => row.email,
    },
    {
      name: "Status",
      sortable: false,
      width: "200px",
      cell: (row) => (
        <div>
          <span
            className={`badge badge-light-${
              row.validasi == "VALID" ? "success" : "danger"
            }`}
          >
            {row.validasi}
          </span>
        </div>
      ),
    },
    {
      name: "Keterangan validasi",
      sortable: false,
      cell: (row) => this.capitalWord(row.keterangan),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
        //fontWeight: 'bold',
      },
    },
  };

  fileType =
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
  fileExtension = ".xlsx";

  exportToCSV(apiData, fileName, extension = "xlsx") {
    // const ws = XLSX.utils.json_to_sheet(apiData);
    // const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
    // const wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });
    // const data = new Blob([s2ab(wbout)], { type: "application/octet-stream" });
    // FileSaver.saveAs(data, fileName + this.fileExtension);

    var ws = XLSX.utils.json_to_sheet(apiData);
    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "data");
    var wbout = XLSX.write(wb, {
      bookType: extension,
      bookSST: true,
      type: "binary",
      Props: {
        Subject: "Rekap pendaftaran",
        Tags: "Daftar pendaftaran",
        Title: "Generated From Digitalent Scholarship",
        Comments: `By (${Cookies.get("user_name")} - ${Cookies.get(
          "user_id",
        )}) on ${moment().format("DD/MM/YYYY, HH:mm:ss")}`,
        Name: `From DTS By (${Cookies.get("user_id")}) on ${moment().format(
          "DD/MM/YYYY, HH:mm:ss",
        )}`,
      },
    });
    FileSaver.saveAs(
      new Blob([s2ab(wbout)], { type: "application/octet-stream" }),
      fileName + "." + extension,
    );
  }

  exportPesertaTableAction(format) {
    const dataExport = this.state.datax;
    const excel = [];

    dataExport.forEach((element, i) => {
      const keys = Object.keys(element);
      const rows = {};
      for (let key of keys) {
        if (key.toLowerCase().includes("code")) continue;
        if (key.toLowerCase().includes("status")) continue;
        if (key.toLowerCase().includes("test2")) continue;
        if (key.toLowerCase().includes("test_upload_dokumen_alfi")) continue;
        if (key.toLowerCase().includes("file_sertifikat")) continue;
        if (key.toLowerCase().includes("status_sertifikasi")) continue;
        if (key.toLowerCase().includes("kode_pos")) continue;
        if (key.toLowerCase().includes("ktp")) continue;
        if (key.toLowerCase().includes("jenjang")) continue;
        if (key.toLowerCase().includes("asal")) continue;
        if (key.toLowerCase().includes("pendidikan")) continue;
        if (key.toLowerCase().includes("program_studi")) continue;
        if (key.toLowerCase().includes("ipk")) continue;
        if (key.toLowerCase().includes("tahun_masuk")) continue;
        if (key.toLowerCase().includes("ijasah")) continue;
        if (key.toLowerCase().includes("pekerjaan")) continue;
        if (key.toLowerCase().includes("foto")) continue;
        if (key.toLowerCase().includes("tempat_lahir")) continue;
        if (key.toLowerCase().includes("hubungan")) continue;
        if (key.toLowerCase().includes("nama_kontak_darurat")) continue;
        if (key.toLowerCase().includes("nomor_handphone_darurat")) continue;
        if (key.toLowerCase().includes("alamat")) continue;
        if (key.toLowerCase().includes("nik")) continue;
        if (key.toLowerCase().includes("tanggal_lahir")) continue;
        if (key.toLowerCase().includes("address")) continue;
        if (key.toLowerCase().includes("provinsi")) continue;
        if (key.toLowerCase().includes("kota")) continue;
        if (key.toLowerCase().includes("kecamatan")) continue;
        if (key.toLowerCase().includes("kelurahan")) continue;

        rows["Pelatihan ID"] = element.pelatian_id;
        if (!key.includes("_id") && !key.includes("id_")) {
          const col = this.capitalWord(key).replaceAll("_", " ");
          if (key == "tgl_updated") {
            rows[col] = moment(element[key]).isValid()
              ? moment(element[key]).format("DD MMMM YYYY")
              : "-";
          } else if (key == "waktu_updated") {
            rows[col] = moment(element[key], "HH:mm:ss").isValid()
              ? moment(element[key], "HH:mm:ss").format("HH:mm:ss")
              : "-";
          } else {
            rows[col] = this.capitalWord(element[key]);
          }
        }

        rows["status_sertifikasi"] = element?.sertifikasi_international;
      }

      excel.push(rows);
    });
    let tanggal = Date().toString();
    let split_tanggal = tanggal.split(" ");
    let text_tanggal =
      split_tanggal[1] +
      "_" +
      split_tanggal[2] +
      "_" +
      split_tanggal[3] +
      "_" +
      split_tanggal[4];
    const isPendingName = "";

    // const excel = (dataExport || []).map(({
    //   pelatian_id, name, nik, jenis_kelamin, tempat_lahir, tanggal_lahir, nama_kontak_darurat, hubungan, nomor_handphone_darurat
    // }) => {
    //   return ({
    //     'Pelatihan ID': pelatian_id,
    //     'Nama': name,
    //     'NIK': nik,
    //     'Jenis Kelamin': jenis_kelamin,
    //     'Tempat Lahir': tempat_lahir,
    //     'Tanggal Lahir': moment(tanggal_lahir).format('DD MMM YYYY'),
    //     'Nama Kontak Darurat': nama_kontak_darurat,
    //     'Hubungan': hubungan,
    //     'Nomor HP Darurat': nomor_handphone_darurat,

    //   });
    // })

    this.exportToCSV(
      excel,
      "Peserta Pelatihan " +
        isPendingName +
        this.state.dataxpelatihan.name +
        " " +
        text_tanggal,
      format,
    );
  }

  componentDidMount() {
    moment.locale("id");
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    let segment_url = window.location.pathname.split("/");
    let urlSegmentOne = segment_url[3];
    Cookies.set("pelatian_id", urlSegmentOne);
    let data = {
      id: urlSegmentOne,
    };

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    Promise.all([
      axios.post(
        process.env.REACT_APP_BASE_API_URI + "/r-detail-rekap-pendaftaran-byid",
        data,
        this.configs,
      ),
      axios.post(
        process.env.REACT_APP_BASE_API_URI + "/pelatihanz/findpelatihan2",
        data,
        this.configs,
      ),
    ])
      .then(([res, res2]) => {
        const dataxpelatihan2 = res2.data.result.Data[0];

        swal.close();
        const dataxpelatihan = res.data.result.data_pelatihan[0];
        this.setState({
          dataxpelatihan: { ...dataxpelatihan2, ...dataxpelatihan },
          substansi_pelatihan: res.data.result.Substansi_pelatihan,
        });

        const jadwalx = res.data.result.jadwal_detail[0];
        this.setState({ jadwalx });

        const statistikx = res.data.result.rekap_pendaftaran[0];
        this.setState({ statistikx });

        //handle list user
        this.handleReloadListUser(1, 10);
        this.handleReloadListUserPending(1, 10);
      })
      .catch((err) => {
        console.error(err);
        swal.fire({
          title: err.response?.data?.result?.Message ?? "Terjadi kesalahan!",
          icon: "warning",
        });
      });

    axios
      .all([
        axios.post(
          process.env.REACT_APP_BASE_API_URI + "/umum/list-status-peserta",
          {},
          this.configs,
        ),
        axios.post(
          process.env.REACT_APP_BASE_API_URI + "/umum/list-status-substansi",
          {},
          this.configs,
        ),
        axios.post(
          process.env.REACT_APP_BASE_API_URI + "/umum/list-status-administrasi",
          {},
          this.configs,
        ),
        axios.post(
          process.env.REACT_APP_BASE_API_URI +
            "/rekappendaftaran/list-status-validasi",
          {},
          this.configs,
        ),
      ])
      .then((resps) => {
        const optionsTemp = [];
        resps.forEach((resp, index) => {
          if (resp.data?.result?.Data) {
            const rawdData = resp.data.result.Data.map((elem) => {
              return {
                label:
                  "name" in elem
                    ? this.capitalWord(elem.name)
                    : this.capitalWord(elem.keterangan),
                value: "name" in elem ? elem.name : elem.id,
              };
            });
            rawdData.unshift({
              label: "Pilih Semua",
              value: index == 3 ? 99 : 0,
            });
            optionsTemp.push(rawdData);
          }
        });
        this.setState({ filterOptions: optionsTemp });
      })
      .catch((err) => {
        console.error(err);
        swal.fire({
          title: err.response?.data?.result?.Message ?? "Terjadi kesalahan!",
          icon: "warning",
        });
      });
    this.modalRef?.current.addEventListener("hidden.bs.modal", (event) => {
      // do something...
      this.setState({ valStatusPesertaUpdate: [], errors: {} });
    });
  }
  handleReloadListUser(page, newPerPage) {
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - length_tmp;
    }
    let segment_url = window.location.pathname.split("/");
    let urlSegmentOne = segment_url[3];
    let dataListUser = {
      id: urlSegmentOne,
      mulai: start_tmp,
      limit: this.state.newPerPage,
      tes_substansi: this.state.valStatusTestSubstansi["value"] ?? 0,
      status_berkas: this.state.valStatusBerkas["value"] ?? 0,
      status_peserta: this.state.valStatusPeserta["value"] ?? 0,
      param: this.state.searchQuery,
      sort: this.state.sort + " " + this.state.sort_val,
      sertifikasi: 0,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/rekappendaftaran/detail-peserta-paging-v2",
        dataListUser,
        this.configs,
      )
      .then((res) => {
        const datax = res.data.result.pelatihan;
        this.setState(
          { datax: datax, totalRows: res.data.result.Total },
          () => {},
        );
        this.setState({ loading: false });
      })
      .catch((err) => {
        console.error(err);
        swal
          .fire({
            title: "Terjadi kesalahan",
            text:
              err.data?.response?.result?.Message ??
              "Terjadi kesalahan saat mengambil data...",
            icon: "error",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
              this.handleReloadListUser();
            }
          });
      });
  }

  handleReloadListUserPending(page, newPerPage) {
    this.setState({ pendingTableLoading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - length_tmp;
    }
    let segment_url = window.location.pathname.split("/");
    let urlSegmentOne = segment_url[3];
    let dataListUser = {
      id: urlSegmentOne,
      mulai: start_tmp,
      limit: this.state.newPerPagePending,
      param: this.state.searchPendingQuery,
      sort: "name",
      sort_val: "asc",
      status: this.state.valStatusVlalidasi.value ?? 99,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/rekappendaftaran/detail-peserta-import-paging",
        dataListUser,
        this.configs,
      )
      .then((res) => {
        const dataxpending = res.data.result.pelatihan;
        this.setState({
          dataxpending: dataxpending,
          totalPendingRow: res.data.result.Total,
        });
        this.setState({ pendingTableLoading: false });
      })
      .catch((err) => {
        console.error(err);
        swal
          .fire({
            title: "Terjadi kesalahan",
            text:
              err.data?.response?.result?.Message ??
              "Terjadi kesalahan saat mengambil data...",
            icon: "error",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
              this.handleReloadListUser();
            }
          });
      });
  }
  handlesubmitFilterAction(e) {
    e.preventDefault();
    this.setState({ loading: true });
    this.handleReloadListUser(1, 10);
  }
  handlesubmitFilterPendingAction(e) {
    e.preventDefault();
    this.setState({ pendingTableLoading: true });
    this.handleReloadListUserPending(1, 10);
  }
  handleClickBatalAction(e) {
    if (this.state?.importPeserta)
      return this.setState({ importPeserta: false });
    window.location = "/pelatihan/reportpelatihan";
  }
  render() {
    const Checkbox = React.forwardRef(({ onClick, ...rest }, ref) => {
      return (
        <>
          <div className="form-check" style={{ backgroundColor: "" }}>
            <input
              type="checkbox"
              className="form-check-input"
              ref={ref}
              onClick={onClick}
              {...rest}
            />
            <label className="form-check-label" id="booty-check" />
          </div>
        </>
      );
    });
    let additionalColumns = [];
    if (this.state.substansi_pelatihan == 1) {
      additionalColumns = [
        {
          name: "Test Substansi",
          sortable: true,
          sortField: "status_tessubstansi",
          center: true,
          width: "200px",
          cell: (row) => (
            <div className="text-center">
              <span>Score: {row?.nilai || `-`}</span>
              <br />
              <span>Passing Grade: {row?.passing_grade || `-`}</span>
              <br />
              <span
                className={`badge badge-light-${
                  row?.status_eligible?.toLowerCase() == "eligible"
                    ? "success"
                    : "danger"
                } fs-7`}
              >
                {row?.status_eligible}
              </span>
            </div>
          ),
        },
        {
          name: "Hasil Test",
          sortable: true,
          center: true,
          width: "200px",
          selector: (row) => row.jawaban_benar,
          cell: (row) => (
            <div>
              <div className="text-center">
                Jawaban Benar: <b>{row.jawaban_benar ?? "-"}</b>
                <br />
                Jawaban Salah: <b>{row.jawaban_salah ?? "-"}</b>
                <br />
                {row?.lama_ujian && (
                  <span className={`badge badge-light-success fs-7`}>
                    {row?.lama_ujian}
                  </span>
                )}
              </div>
            </div>
          ),
        },
      ];
    }

    let columns = [
      {
        name: "No",
        center: true,
        width: "70px",
        cell: (row, index) => (
          <div>
            <span>
              {(this.state.page - 1) * this.state.newPerPage + index + 1}
            </span>
          </div>
        ),
      },
      {
        name: "Nama Peserta",
        sortable: true,
        sortField: "name",
        width: "auto",
        cell: (row) => (
          <div className="d-flex flex-stack my-2">
            <span className="d-flex align-items-center me-2">
              <span className="d-flex flex-column">
                {/*<h6 className="text-muted fs-7 fw-semibold mb-1">{row.nik}</h6>*/}
                <h6 className="text-muted fs-7 fw-semibold mb-1">
                  {row.nomor_registrasi}
                </h6>
                <h6 className="fw-bolder fs-7 mb-1">
                  {this.capitalWord(row.name)}
                </h6>
                {/*<span className="text-muted fs-7 fw-semibold">
                      {row.email}
                    </span>*/}
              </span>
            </span>
          </div>
        ),
      },
      {
        name: "Status Peserta",
        sortable: true,
        sortField: "status_peserta",
        center: false,
        width: "auto",
        cell: (row) => (
          <div>
            <span
              className={`badge badge-light-${
                tagColorMapStatusPeserta[row.status_peserta] ?? "danger"
              }`}
            >
              {this.capitalWord(row.status_peserta)}
            </span>
          </div>
        ),
      },
      {
        name:
          "File Sertifikasi " +
          (this.state.dataxpelatihan.sertifikasi == `Tidak Ada`
            ? ``
            : this.state.dataxpelatihan.sertifikasi),
        sortable: true,
        sortField: "sertifikasi_international",
        center: false,
        width: "auto",
        cell: (row) => (
          // <div>
          //   <span className={`badge badge-light-${row.status_sertifikasi == null ? 'warning' : (row.status_sertifikasi ? 'danger' : 'primary')}`}>
          //     {this.capitalWord(row.status_sertifikasi == null ? 'Data Tidak Tersedia' : (row.status_sertifikasi ? 'Lulus Sertifikasi' : 'Tidak Lulus Sertifikasi'))}
          //   </span>
          // </div>
          <div>
            {(this.state.dataxpelatihan.sertifikasi == "Global") |
            (this.state.dataxpelatihan.sertifikasi == "SKKNI (Nasional)") ? (
              <span
                className={`badge badge-light-${
                  row.sertifikasi_international == null
                    ? "warning"
                    : row.sertifikasi_international.toLowerCase() == "tidak ada"
                      ? "danger"
                      : "success"
                }`}
              >
                {this.capitalWord(
                  row.sertifikasi_international == null
                    ? "Data Tidak Tersedia"
                    : row.sertifikasi_international.toLowerCase() == "tidak ada"
                      ? "Tidak Tersedia"
                      : "Tersedia",
                )}
              </span>
            ) : (
              <span className={`badge badge-light-info`}>
                Pelatihan Tanpa Sertifikasi
              </span>
            )}
          </div>
        ),
      },
      {
        name: "Log",
        sortable: false,
        cell: (row) => (
          <div>
            <span>{row.updated_oleh ?? "-"}</span>
            <br />
            <span>
              {row.tgl_updated
                ? moment(row.tgl_updated).format("DD MMMM YYYY")
                : "-"}
            </span>
            <br />
            <span>
              {row.waktu_updated
                ? moment(row.waktu_updated, "HH:mm:ss").format("HH:mm:ss")
                : "-"}
            </span>
          </div>
        ),
      },
      ...((this.state.dataxpelatihan?.sertifikasi || "").toLowerCase() ==
      "tidak ada"
        ? [{}]
        : [
            {
              name: "Aksi",
              center: true,
              cell: (row) => (
                <div className="row">
                  <a
                    href="#"
                    id={row.user_id}
                    title="View"
                    className="btn btn-icon btn-warning btn-sm"
                    data-bs-toggle="modal"
                    data-bs-target="#upload_sertifikat"
                    onClick={() => this.setState({ selectedUser: row })}
                  >
                    <i className="bi bi-gear-fill text-white"></i>
                  </a>
                </div>
              ),
            },
          ]),
    ];

    return (
      <div>
        <ShowUUPdp
          file_id={this.state.dataxpelatihan.id}
          jenis="report pelatihan peserta pelatihan export"
          show={this.state.showPdpModal}
          onResolve={() => {
            this.exportPesertaTableAction(this.state.preferedExt);
            this.setState({ showPdpModal: false });
          }}
          onReject={() => {
            this.setState({ showPdpModal: false });
          }}
        />
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Report Pelatihan
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Detail Report Pelatihan
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <div className="d-flex align-items-center position-relative my-1">
                            <a
                              href={
                                "/pelatihan/view-pelatihan/" +
                                this.state.dataxpelatihan.id
                              }
                              title="Info"
                              className={`btn btn-sm btn-light-primary btn-active-light-primary text-primary`}
                            >
                              <i className="bi bi-info-circle text-primary me-1"></i>
                              <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                Detail Pelatihan
                              </span>
                            </a>
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        {this.state?.importPeserta ? (
                          <ImportPeserta
                            data={this.state.datax}
                            onCancel={() =>
                              this.setState({ importPeserta: false })
                            }
                          />
                        ) : (
                          <>
                            <div className="mt-7">
                              <div className="d-flex flex-wrap py-3">
                                <div className="symbol symbol-100px symbol-lg-120px me-4">
                                  {this.state.dataxpelatihan.mitra_logo ==
                                  null ? (
                                    <img
                                      src={`/assets/media/logos/logo-kominfo.png`}
                                      height="100px"
                                      alt=""
                                      className="symbol-label"
                                    />
                                  ) : (
                                    <img
                                      src={
                                        process.env.REACT_APP_BASE_API_URI +
                                        "/download/get-file?path=" +
                                        this.state.dataxpelatihan.mitra_logo +
                                        "&disk=dts-storage-partnership"
                                      }
                                      height="100px"
                                      alt=""
                                      className="symbol-label"
                                    />
                                  )}
                                </div>
                                <div className="flex-grow-1 mb-3">
                                  <div className="justify-content-between align-items-start flex-wrap pt-6">
                                    <span
                                      className={`badge badge-${
                                        this.state.dataxpelatihan
                                          .status_pelatihan == "Selesai"
                                          ? "success"
                                          : this.state.dataxpelatihan
                                                .status_pelatihan ==
                                              "Dibatalkan"
                                            ? "danger"
                                            : this.state.dataxpelatihan
                                                  .status_pelatihan ==
                                                "Pelatihan"
                                              ? "warning"
                                              : "primary"
                                      }`}
                                    >
                                      {
                                        this.state.dataxpelatihan
                                          .status_pelatihan
                                      }
                                    </span>
                                    <h1 className="align-items-center text-dark fw-bolder my-1 fs-4">
                                      {(
                                        this.state.dataxpelatihan.id_slug || ""
                                      ).toUpperCase()}{" "}
                                      - {this.state.dataxpelatihan.pelatihan}{" "}
                                      (Batch {this.state.dataxpelatihan.batch})
                                    </h1>
                                    <p className="text-dark fs-7 mb-0">
                                      {this.state.dataxpelatihan.akademi} -{" "}
                                      <span className="text-muted fw-semibold fs-7 mb-0">
                                        {this.state.dataxpelatihan.tema}
                                      </span>
                                    </p>
                                  </div>
                                </div>
                              </div>
                              <div>
                                <div className="my-8 border-top mx-0"></div>
                              </div>
                              <div className="mb-5">
                                <div className="border-bottom mb-5">
                                  <h2 className="fs-5 text-muted mb-3">
                                    Informasi Pelatihan
                                  </h2>
                                  <div className="row">
                                    <div className="col-lg-4 mb-7 fv-row">
                                      <label className="form-label">
                                        Penyelenggara
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {this.state.dataxpelatihan
                                            .penyelenggara ?? "Belum ada data"}
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-4 mb-7 fv-row">
                                      <label className="form-label">
                                        Mitra
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {this.state.dataxpelatihan
                                            .metode_pelaksanaan == "Mitra"
                                            ? this.state.dataxpelatihan
                                                .mitra_name
                                            : this.state.dataxpelatihan
                                                .metode_pelaksanaan}
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-4 mb-7 fv-row">
                                      <label className="form-label">
                                        Metode
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {this.state.dataxpelatihan
                                            .metode_pelatihan ??
                                            "Belum ada data"}
                                        </b>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="border-bottom mb-5">
                                  <h2 className="fs-5 text-muted mb-3">
                                    Pelaksanaan
                                  </h2>
                                  <div className="row">
                                    <div className="col-lg-4 mb-7 fv-row">
                                      <label className="form-label">
                                        Alur Pelatihan
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {this.state.jadwalx.alur_pendaftaran}
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-4 mb-7 fv-row">
                                      <label className="form-label">
                                        Sertifikasi
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {
                                            this.state.dataxpelatihan
                                              .sertifikasi
                                          }
                                        </b>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col-lg-4 mb-7 fv-row">
                                      <label className="form-label">
                                        Jadwal Pendaftaran
                                      </label>
                                      <div className="d-flex">
                                        {this.state.jadwalx
                                          .waktu_pendaftaran && (
                                          <b>
                                            {dateRangeWithSeparator(
                                              this.state.jadwalx
                                                .waktu_pendaftaran,
                                              "-",
                                              "DD MMMM YYYY",
                                            )}
                                          </b>
                                        )}
                                      </div>
                                    </div>
                                    <div className="col-lg-4 mb-7 fv-row">
                                      <label className="form-label">
                                        Jadwal Pelatihan
                                      </label>
                                      <div className="d-flex">
                                        {this.state.jadwalx.waktu_pelatihan && (
                                          <b>
                                            {dateRangeWithSeparator(
                                              this.state.jadwalx
                                                .waktu_pelatihan,
                                              "-",
                                              "DD MMMM YYYY",
                                            )}
                                          </b>
                                        )}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="border-bottom mb-5">
                                  <h2 className="fs-5 text-muted mb-3">
                                    Kuota Pelatihan
                                  </h2>
                                  <div className="row">
                                    <div className="col-lg-4 mb-7 fv-row">
                                      <label className="form-label">
                                        Jumlah Kuota Peserta
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {this.state.jadwalx.kuota_peserta}{" "}
                                          Peserta
                                        </b>
                                      </div>
                                    </div>
                                    {/* <div className="col-lg-4 mb-7 fv-row">
                                      <label className="form-label">
                                        Jumlah Kuota Pendaftar
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {this.state.jadwalx.kuota_pendaftar}{" "}
                                          Pendaftar
                                        </b>
                                      </div>
                                    </div> */}
                                    {this.state.jadwalx.kuota_pendaftar !=
                                      "999999" && (
                                      <div className="col-lg-4 mb-7 fv-row">
                                        <label className="form-label">
                                          Jumlah Kuota Pendaftar
                                        </label>
                                        <div className="d-flex">
                                          <b>
                                            {this.state.jadwalx.kuota_pendaftar}{" "}
                                            Pendaftar
                                          </b>
                                        </div>
                                      </div>
                                    )}
                                    <div className="col-lg-4 mb-7 fv-row">
                                      <label className="form-label">
                                        Jumlah Pendaftar
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {this.state.statistikx
                                            ?.jml_pendaftar ??
                                            "Belum ada data"}{" "}
                                          Pendaftar
                                        </b>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div>
                                  <h2 className="fs-5 text-muted mb-3">
                                    Lokasi Pelatihan
                                  </h2>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label">Zonasi</label>
                                    <div className="d-flex">
                                      <b>
                                        {" "}
                                        {this.state.dataxpelatihan.zonasi ??
                                          "Belum ada data"}
                                      </b>
                                    </div>
                                  </div>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label">Lokasi</label>
                                    <div className="row">
                                      <div className="col-12">
                                        <b>
                                          <span
                                            className="address"
                                            dangerouslySetInnerHTML={{
                                              __html: (
                                                this.state.dataxpelatihan
                                                  ?.metode_pelatihan || ""
                                              )
                                                .toLowerCase()
                                                .includes("offline")
                                                ? `${[
                                                    this.state.dataxpelatihan
                                                      ?.alamat,
                                                    this.state.dataxpelatihan
                                                      ?.kab_name,
                                                    this.state.dataxpelatihan
                                                      ?.nm_prov,
                                                  ]
                                                    .filter((a) => !!a)
                                                    .join(", ")}`
                                                : (
                                                      this.state.dataxpelatihan
                                                        ?.metode_pelatihan || ""
                                                    )
                                                      .toLowerCase()
                                                      .includes("online")
                                                  ? "Online"
                                                  : "Belum ada data",
                                            }}
                                          />
                                        </b>
                                        {/* <b>Tampilkan alamat lengkap, Provinis, Kota, kAB</b> */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div>
                              <div className="my-8 border-top mx-0"></div>
                            </div>
                            <div className="col-lg-12 mt-7">
                              <div className="card-header p-0">
                                <div className="card-title">
                                  <div className="d-flex align-items-center position-relative my-1 me-2">
                                    <span className="svg-icon svg-icon-1 position-absolute ms-6">
                                      <svg
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                        xmlns="http://www.w3.org/2000/svg"
                                        className="mh-50px"
                                      >
                                        <rect
                                          opacity="0.5"
                                          x="17.0365"
                                          y="15.1223"
                                          width="8.15546"
                                          height="2"
                                          rx="1"
                                          transform="rotate(45 17.0365 15.1223)"
                                          fill="currentColor"
                                        ></rect>
                                        <path
                                          d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                          fill="currentColor"
                                        ></path>
                                      </svg>
                                    </span>
                                    <input
                                      type="text"
                                      name="search"
                                      data-kt-user-table-filter="search"
                                      className="form-control form-control-sm form-control-solid w-250px ps-14"
                                      placeholder="Cari Peserta"
                                      onKeyPress={this.handleKeyPress}
                                      onChange={this.handleSearchEmpty}
                                    />
                                  </div>
                                </div>
                                <div className="card-toolbar">
                                  <button
                                    className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                                    data-bs-toggle="modal"
                                    data-bs-target="#filter"
                                  >
                                    <i className="bi bi-sliders"></i>
                                    <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                      Filter
                                    </span>
                                  </button>
                                  {(
                                    this.state.dataxpelatihan?.sertifikasi || ""
                                  ).toLowerCase() != "tidak ada" && (
                                    <button
                                      className="btn btn-sm btn-flex btn-light btn-success fw-bolder me-2"
                                      onClick={() =>
                                        this.setState({ importPeserta: true })
                                      }
                                    >
                                      <i className="bi bi-patch-plus"></i>
                                      <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                        Import Sertifikat
                                      </span>
                                    </button>
                                  )}

                                  <div className="dropdown d-inline">
                                    <button
                                      className="btn btn-light fw-bolder btn-sm dropdown-toggle fs-7 me-2"
                                      type="button"
                                      data-bs-toggle="dropdown"
                                      aria-expanded="false"
                                    >
                                      <i className="bi bi-gear me-1"></i>
                                      <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                        Export Data
                                      </span>
                                    </button>
                                    <ul
                                      className="dropdown-menu"
                                      style={{
                                        position: "absolute",
                                        zIndex: "999999",
                                      }}
                                    >
                                      <li>
                                        <button
                                          name="peserta"
                                          className="dropdown-item px-5 my-1"
                                          onClick={(e) => {
                                            this.setState({
                                              showPdpModal: true,
                                              preferedExt: "csv",
                                            });
                                          }}
                                        >
                                          <i className="bi bi-cloud-upload text-dark text-hover-primary me-1"></i>
                                          Export (csv)
                                        </button>
                                      </li>
                                      <li>
                                        <button
                                          name="peserta"
                                          className="dropdown-item px-5 my-1"
                                          onClick={(e) => {
                                            this.setState({
                                              showPdpModal: true,
                                              preferedExt: "xlsx",
                                            });
                                          }}
                                        >
                                          <i className="bi bi-cloud-upload text-dark text-hover-primary me-1"></i>
                                          Export (xlsx)
                                        </button>
                                      </li>
                                    </ul>
                                  </div>

                                  {/* <button
                                    name="peserta"
                                    className="btn btn-sm btn-flex btn-light fw-bolder me-2 "
                                    onClick={this.exportPesertaTable}
                                  >
                                    <i className="bi bi-cloud-download"></i>
                                    <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                      Export
                                    </span>
                                  </button> */}
                                  {/* <button
                              className="btn btn-sm btn-flex btn-light btn-warning fw-bolder me-2"
                              data-bs-toggle="modal"
                              data-bs-target="#update-bulk-modal"
                            >
                              <span className="svg-icon svg-icon-primary">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  width="24px"
                                  height="24px"
                                  viewBox="0 0 24 24"
                                  version="1.1"
                                >
                                  <g
                                    stroke="none"
                                    stroke-width="1"
                                    fill="none"
                                    fill-rule="evenodd"
                                  >
                                    <rect x="0" y="0" width="24" height="24" />
                                    <path
                                      d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z"
                                      fill="#000000"
                                      opacity="0.3"
                                    />
                                    <path
                                      d="M10.875,15.75 C10.6354167,15.75 10.3958333,15.6541667 10.2041667,15.4625 L8.2875,13.5458333 C7.90416667,13.1625 7.90416667,12.5875 8.2875,12.2041667 C8.67083333,11.8208333 9.29375,11.8208333 9.62916667,12.2041667 L10.875,13.45 L14.0375,10.2875 C14.4208333,9.90416667 14.9958333,9.90416667 15.3791667,10.2875 C15.7625,10.6708333 15.7625,11.2458333 15.3791667,11.6291667 L11.5458333,15.4625 C11.3541667,15.6541667 11.1145833,15.75 10.875,15.75 Z"
                                      fill="#000000"
                                    />
                                    <path
                                      d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z"
                                      fill="#000000"
                                    />
                                  </g>
                                </svg>
                              </span>
                              Update Bulk
                            </button> */}
                                  <div
                                    className="modal fade"
                                    tabIndex="-1"
                                    id="filter"
                                  >
                                    <div className="modal-dialog modal-lg">
                                      <div className="modal-content">
                                        <div className="modal-header">
                                          <h5 className="modal-title">
                                            <span className="svg-icon svg-icon-5 me-1">
                                              <i className="bi bi-sliders text-black"></i>
                                            </span>
                                            Filter Data Peserta
                                          </h5>
                                          <div
                                            className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                            data-bs-dismiss="modal"
                                            aria-label="Close"
                                          >
                                            <span className="svg-icon svg-icon-2x">
                                              <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="24"
                                                height="24"
                                                viewBox="0 0 24 24"
                                                fill="none"
                                              >
                                                <rect
                                                  opacity="0.5"
                                                  x="6"
                                                  y="17.3137"
                                                  width="16"
                                                  height="2"
                                                  rx="1"
                                                  transform="rotate(-45 6 17.3137)"
                                                  fill="currentColor"
                                                />
                                                <rect
                                                  x="7.41422"
                                                  y="6"
                                                  width="16"
                                                  height="2"
                                                  rx="1"
                                                  transform="rotate(45 7.41422 6)"
                                                  fill="currentColor"
                                                />
                                              </svg>
                                            </span>
                                          </div>
                                        </div>
                                        <form
                                          action="#"
                                          onSubmit={this.handlesubmitFilter}
                                        >
                                          <div className="modal-body">
                                            <div className="row">
                                              <div className="col-lg-12 mb-7 fv-row">
                                                <label className="form-label">
                                                  Status Peserta
                                                </label>
                                                <Select
                                                  name="status-peserta"
                                                  placeholder="Silahkan pilih"
                                                  value={
                                                    this.state.valStatusPeserta
                                                  }
                                                  noOptionsMessage={({
                                                    inputValue,
                                                  }) =>
                                                    !inputValue
                                                      ? this.state
                                                          .filterOptions[0]
                                                      : "Data tidak tersedia"
                                                  }
                                                  className="form-select-sm form-select-solid selectpicker p-0"
                                                  options={
                                                    this.state.filterOptions[0]
                                                  }
                                                  onChange={
                                                    this
                                                      .handleChangeStatusPeserta
                                                  }
                                                />
                                              </div>
                                            </div>
                                          </div>
                                          <div className="modal-footer">
                                            <div className="d-flex justify-content-between">
                                              <button
                                                type="reset"
                                                className="btn btn-sm btn-light me-3"
                                                onClick={this.handleClickReset}
                                              >
                                                Reset
                                              </button>
                                              <button
                                                type="submit"
                                                className="btn btn-sm btn-primary"
                                                data-bs-dismiss="modal"
                                              >
                                                Apply Filter
                                              </button>
                                            </div>
                                          </div>
                                        </form>
                                      </div>
                                    </div>
                                  </div>

                                  <div
                                    className="modal fade"
                                    tabIndex="-1"
                                    id="filter-pending"
                                  >
                                    <div className="modal-dialog modal-lg">
                                      <div className="modal-content">
                                        <div className="modal-header">
                                          <h5 className="modal-title">
                                            <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                                              <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="24"
                                                height="24"
                                                viewBox="0 0 24 24"
                                                fill="none"
                                              >
                                                <path
                                                  d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                                                  fill="currentColor"
                                                />
                                              </svg>
                                            </span>
                                            Filter
                                          </h5>
                                          <div
                                            className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                            data-bs-dismiss="modal"
                                            aria-label="Close"
                                          >
                                            <span className="svg-icon svg-icon-2x">
                                              <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="24"
                                                height="24"
                                                viewBox="0 0 24 24"
                                                fill="none"
                                              >
                                                <rect
                                                  opacity="0.5"
                                                  x="6"
                                                  y="17.3137"
                                                  width="16"
                                                  height="2"
                                                  rx="1"
                                                  transform="rotate(-45 6 17.3137)"
                                                  fill="currentColor"
                                                />
                                                <rect
                                                  x="7.41422"
                                                  y="6"
                                                  width="16"
                                                  height="2"
                                                  rx="1"
                                                  transform="rotate(45 7.41422 6)"
                                                  fill="currentColor"
                                                />
                                              </svg>
                                            </span>
                                          </div>
                                        </div>
                                        <form
                                          action="#"
                                          onSubmit={
                                            this.handlesubmitFilterPending
                                          }
                                        >
                                          <div className="modal-body">
                                            <div className="row">
                                              <div className="col-lg-12 mb-7 fv-row">
                                                <label className="form-label">
                                                  Status Import
                                                </label>
                                                <Select
                                                  name="status-test-substansi"
                                                  placeholder="Silahkan pilih"
                                                  value={
                                                    this.state
                                                      .valStatusVlalidasi
                                                  }
                                                  noOptionsMessage={({
                                                    inputValue,
                                                  }) =>
                                                    !inputValue
                                                      ? this.state
                                                          .filterOptions[3]
                                                      : "Data tidak tersedia"
                                                  }
                                                  className="form-select-sm form-select-solid selectpicker p-0"
                                                  options={
                                                    this.state.filterOptions[3]
                                                  }
                                                  onChange={
                                                    this
                                                      .handleChangeStatusValidasi
                                                  }
                                                />
                                              </div>
                                            </div>
                                          </div>
                                          <div className="modal-footer">
                                            <div className="d-flex justify-content-between">
                                              <button
                                                type="reset"
                                                className="btn btn-sm btn-danger me-3"
                                                onClick={this.handleClickReset}
                                              >
                                                Reset
                                              </button>
                                              <button
                                                type="submit"
                                                className="btn btn-sm btn-primary"
                                                data-bs-dismiss="modal"
                                              >
                                                Apply Filter
                                              </button>
                                            </div>
                                          </div>
                                        </form>
                                      </div>
                                    </div>
                                  </div>

                                  <div
                                    ref={this.modalRef}
                                    className="modal fade"
                                    tabIndex="-1"
                                    id="update-bulk-modal"
                                  >
                                    <div className="modal-dialog modal-lg">
                                      <div className="modal-content">
                                        <div className="modal-header">
                                          <h5 className="modal-title">
                                            <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                                              <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="24"
                                                height="24"
                                                viewBox="0 0 24 24"
                                                fill="none"
                                              >
                                                <path
                                                  d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                                                  fill="currentColor"
                                                />
                                              </svg>
                                            </span>
                                            Update Status Peserta Bulk
                                          </h5>
                                          <div
                                            className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                            data-bs-dismiss="modal"
                                            aria-label="Close"
                                            ref={this.updateModelCloseButtonRef}
                                          >
                                            <span className="svg-icon svg-icon-2x">
                                              <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="24"
                                                height="24"
                                                viewBox="0 0 24 24"
                                                fill="none"
                                              >
                                                <rect
                                                  opacity="0.5"
                                                  x="6"
                                                  y="17.3137"
                                                  width="16"
                                                  height="2"
                                                  rx="1"
                                                  transform="rotate(-45 6 17.3137)"
                                                  fill="currentColor"
                                                />
                                                <rect
                                                  x="7.41422"
                                                  y="6"
                                                  width="16"
                                                  height="2"
                                                  rx="1"
                                                  transform="rotate(45 7.41422 6)"
                                                  fill="currentColor"
                                                />
                                              </svg>
                                            </span>
                                          </div>
                                        </div>
                                        <form
                                          action="#"
                                          onSubmit={
                                            this.handleSubmitUpdateStatusBulk
                                          }
                                        >
                                          <div className="modal-body">
                                            <div className="row">
                                              {this.state.selectedTableRows
                                                .length > 0 && (
                                                <div>
                                                  <h5>
                                                    Daftar peserta terpilih
                                                  </h5>
                                                  <div className="d-flex">
                                                    {this.state.selectedTableRows.map(
                                                      (elem, index) => (
                                                        <span
                                                          className="alert alert-primary alert-sm fade show m-1"
                                                          role="alert"
                                                        >
                                                          <strong>
                                                            {elem?.name}
                                                          </strong>
                                                        </span>
                                                      ),
                                                    )}
                                                  </div>
                                                </div>
                                              )}
                                              {this.state.selectedTableRows
                                                .length == 0 && (
                                                <>
                                                  <div
                                                    className="w-100 alert alert-danger fade show m-1"
                                                    style={{
                                                      textAlign: "center",
                                                    }}
                                                    role="alert"
                                                  >
                                                    <strong>
                                                      Belum ada peserta
                                                      terpilih!
                                                    </strong>
                                                  </div>
                                                  <span
                                                    style={{ color: "red" }}
                                                  >
                                                    {
                                                      this.state.errors[
                                                        "daftar_peserta"
                                                      ]
                                                    }
                                                  </span>
                                                </>
                                              )}
                                              <div className="col-lg-12 mb-7 fv-row">
                                                <label className="form-label">
                                                  Status Peserta
                                                </label>
                                                <Select
                                                  name="status-peserta"
                                                  placeholder="Silahkan pilih"
                                                  value={
                                                    this.state
                                                      .valStatusPesertaUpdate
                                                  }
                                                  noOptionsMessage={({
                                                    inputValue,
                                                  }) =>
                                                    !inputValue
                                                      ? this.state
                                                          .filterOptions[0]
                                                      : "Data tidak tersedia"
                                                  }
                                                  className="form-select-sm form-select-solid selectpicker p-0"
                                                  options={this.state.filterOptions[0]?.slice(
                                                    1,
                                                  )}
                                                  onChange={
                                                    this
                                                      .handleChangeStatusPesertaUpdate
                                                  }
                                                />
                                                <span style={{ color: "red" }}>
                                                  {
                                                    this.state.errors[
                                                      "status_peserta"
                                                    ]
                                                  }
                                                </span>
                                              </div>
                                            </div>
                                          </div>
                                          <div className="modal-footer">
                                            <div className="d-flex justify-content-between">
                                              <button
                                                type="reset"
                                                className="btn btn-sm btn-danger me-3"
                                                data-bs-dismiss="modal"
                                              >
                                                Batal
                                              </button>
                                              <button
                                                type="submit"
                                                className="btn btn-sm btn-primary"
                                              >
                                                Submit
                                              </button>
                                            </div>
                                          </div>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <DataTable
                                columns={columns}
                                data={this.state.datax}
                                progressPending={this.state.loading}
                                progressComponent={this.customLoader}
                                highlightOnHover
                                pointerOnHover
                                pagination
                                paginationServer
                                paginationTotalRows={this.state.totalRows}
                                onChangeRowsPerPage={this.handlePerRowsChange}
                                onChangePage={this.handlePageChange}
                                customStyles={this.customStyles}
                                persistTableHead={true}
                                noDataComponent={
                                  <div className="mt-5">Tidak Ada Data</div>
                                }
                                onSort={({ sortField }, order) =>
                                  this.handleChangeSort(sortField, order)
                                }
                                // selectableRows
                                // selectableRowsComponent={Checkbox}
                                // onSelectedRowsChange={(evt) => {
                                //   this.setState({
                                //     selectedTableRows: evt.selectedRows,
                                //   });
                                // }}
                              />
                            </div>
                          </>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <ModalUbahKelulusan
          {...this.props}
          {...this.state}
          onChange={(value) => this.setState(value)}
        />
      </div>
      // </div>
    );
  }
}

const RekappendaftaranView = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <RekappendaftarandetailContent />
      <Footer />
    </div>
  );
};

export default RekappendaftaranView;
