import React from "react";
import DataTable from "react-data-table-component";
import Select from "react-select";
import * as XLSX from "xlsx";
import { saveAs } from "file-saver";
import Cookies from "js-cookie";

import template from "./index.js";
import Swal from "sweetalert2";
import { uploadSertifikasi } from "../actions";

function s2ab(s) {
  if (typeof ArrayBuffer !== "undefined") {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xff;
    return buf;
  } else {
    var buf = new Array(s.length);
    for (var i = 0; i != s.length; ++i) buf[i] = s.charCodeAt(i) & 0xff;
    return buf;
  }
}

class ImportPeserta extends React.Component {
  constructor(props) {
    super(props);

    this.filePesertaRef = React.createRef();

    this.state = {
      fileTemplateXlsx: null,
      fileTemplateImages: [],
    };
  }

  downloadFileTemplateXlsx = () => {
    var ws = XLSX.utils.json_to_sheet(
      this.props?.data.map(({ pelatian_id, user_id, name }) => ({
        "Id Pelatihan": pelatian_id,
        "Id Peserta": user_id,
        "Nama Peserta": name,
        "Status (Lulus = 1, Tidak Lulus = 0)": "",
        "Nama File Sertifikat": "",
      })),
    );

    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Sertifikat Peserta");
    var wbout = XLSX.write(wb, {
      bookType: "xlsx",
      bookSST: true,
      type: "binary",
    });
    saveAs(
      new Blob([s2ab(wbout)], { type: "application/octet-stream" }),
      "Template Sertifikat Peserta.xlsx",
    );
  };

  onFileTemplateXlsxChange = (f) => {
    this.setState({ fileTemplateXlsx: f });
  };

  addFileTemplateImages = (fs) => {
    this.setState({
      fileTemplateImages: [...this.state.fileTemplateImages, ...fs],
    });
  };

  removeFileTemplateImage = (idx) => {
    let fileTemplateImages = this.state.fileTemplateImages;
    fileTemplateImages.splice(idx, 1);

    this.setState({
      fileTemplateImages: [...fileTemplateImages],
    });
  };

  onCancel = () => {
    this.setState({
      fileTemplateXlsx: null,
      fileTemplateImages: [],
    });

    this.props?.onCancel();
  };

  doImport = async () => {
    try {
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info",
        allowOutsideClick: false,
        didOpen: () => Swal.showLoading(),
      });

      const certificateNames = this.state.fileTemplateImages?.map(
        (f) => f?.name,
      );

      const templateBin = await new Promise((resolve, reject) => {
        var reader = new FileReader();
        reader.onload = () => {
          resolve(reader.result);
        };
        reader.readAsArrayBuffer(this.state.fileTemplateXlsx);
      });

      var wb = XLSX.read(new Uint8Array(templateBin), { type: "array" });
      var pesertas = XLSX.utils.sheet_to_json(wb.Sheets["Sertifikat Peserta"], {
        header: 2,
      });

      // Validate first
      for (var peserta of pesertas) {
        if (!certificateNames.includes(peserta["Nama File Sertifikat"]))
          throw new Error(
            `File sertifikat ${peserta["Nama File Sertifikat"]} tidak ditemukan!`,
          );
      }

      const sertifikats = (
        await Promise.all(
          (this.state.fileTemplateImages || []).map(async (f) => {
            return new Promise((resolve, reject) => {
              var reader = new FileReader();
              reader.onload = () => {
                resolve({ name: f.name, data: reader.result });
              };
              reader.readAsDataURL(f);
            });
          }),
        )
      ).reduce((obj, { name, data }) => ({ ...obj, [name]: data }), {});

      const payload = {
        categoryOptItems: pesertas.map((peserta) => ({
          id_user: peserta["Id Peserta"],
          sertifikat: peserta["Status (Lulus = 1, Tidak Lulus = 0)"],
          pelatihan_id: peserta["Id Pelatihan"],
          file_sertifikat: sertifikats[peserta["Nama File Sertifikat"]],
          updatedby: parseInt(Cookies.get("user_id")),
        })),
      };

      const message = await uploadSertifikasi(payload);

      Swal.close();
      await Swal.fire({
        title: "Sertifikasi berhasil disimpan",
        icon: "success",
        confirmButtonText: "Ok",
      });

      window.location.reload();
    } catch (err) {
      Swal.close();
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  render() {
    return (
      <div className="col-lg-12 mt-7">
        <div className="highlight bg-light-primary mt-7">
          <div className="col-lg-12 mb-7 fv-row text-primary">
            <h5 className="text-primary fs-5">Panduan</h5>
            <p className="text-primary">
              Sebelum melakukan import soal, mohon untuk membaca panduan berikut
              :
            </p>
            <ul>
              <li>
                Silahkan unduh template untuk melakukan import pada link berikut{" "}
                <a
                  href="#"
                  onClick={() => this.downloadFileTemplateXlsx()}
                  className="btn btn-primary fw-semibold btn-sm py-1 px-2"
                >
                  <i className="las la-cloud-download-alt fw-semibold me-1" />
                  Download Template
                </a>
              </li>
              <li>
                Lengkapi isian pada kolom kelulusan dan nama file sertifikat
              </li>
              <li>Upload kembali template yang telah dilengkapi</li>
              <li>
                Upload file sertifikat dengan nama file dan jumlah yang sama
                pada rekap (dengan melalukan "Select-All" pada file-file
                sertifikat yang akan diupload)
              </li>
            </ul>
          </div>
        </div>

        <div className="row mt-7">
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">
              Upload Template Peserta
            </label>
            <input
              ref={this.filePesertaRef}
              type="file"
              className="form-control form-control-sm mb-2"
              name="upload_silabus"
              accept=".xlsx"
              onInput={(e) => {
                const [f] = [...e.target.files];
                this.onFileTemplateXlsxChange(f);
                e.target.value = null;
              }}
            />
            <small className="text-muted">
              Format File (.xlsx), Max 10240 Kb
            </small>
            <br />
          </div>
          {this.state.fileTemplateXlsx && (
            <div className="col-lg-12 mb-7 fv-row">
              <div className="row">
                <div className="col-lg-12 mb-7 fv-row">
                  <span>{this.state.fileTemplateXlsx?.name}</span>
                  <div
                    className="btn btn-sm btn-icon btn-light-danger float-end"
                    title="Hapus file"
                    onClick={(e) => this.onFileTemplateXlsxChange(null)}
                  >
                    <span className="las la-trash-alt" />
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>

        <div className="row">
          <div className="col-lg-12 mb-7 fv-row">
            <label className="form-label required">Upload Sertifikat</label>
            <input
              type="file"
              className="form-control form-control-sm mb-2"
              name="upload_silabus"
              multiple
              accept="application/pdf"
              onInput={(e) => {
                this.addFileTemplateImages([...e.target.files]);
                e.target.value = null;
              }}
            />
            <small className="text-muted">
              Format File (.pdf), Max 10240 Kb
            </small>
            <br />
          </div>
          {this.state.fileTemplateImages && (
            <div className="col-lg-12 mb-7 fv-row">
              <div className="row">
                {this.state.fileTemplateImages.map((file, idx) => {
                  return (
                    <div className="col-lg-12 mb-7 fv-row">
                      <span>{file.name}</span>
                      <div
                        className="btn btn-sm btn-icon btn-light-danger float-end"
                        title="Hapus file"
                        onClick={(e) => this.removeFileTemplateImage(idx)}
                      >
                        <span className="las la-trash-alt" />
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          )}
        </div>

        <div className="my-7 pt-7 text-center">
          <button
            onClick={this.onCancel}
            type="reset"
            className="btn btn-md btn-light me-2"
            data-kt-menu-dismiss="true"
          >
            <span className="d-md-inline d-lg-inline d-xl-inline d-none">
              Batal
            </span>
          </button>
          <button
            className="btn btn-primary btn-md"
            disabled={!this.state.fileTemplateXlsx}
            onClick={(e) => {
              e.preventDefault();
              this.doImport();
            }}
          >
            <i className="fa fa-paper-plane me-1"></i>Simpan
          </button>
        </div>
      </div>
    );
  }
}

export default ImportPeserta;
