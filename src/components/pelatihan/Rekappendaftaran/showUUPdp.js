import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import SignatureCanvas from "react-signature-canvas";
import withReactContent from "sweetalert2-react-content";
import Cookies from "js-cookie";
import Swal from "sweetalert2";

const ShowUUPdp = ({ file_id, jenis, show, onResolve, onReject }) => {
  const toggleRef = useRef(null);
  const pdpModalRef = useRef(null);
  const toggleCloseRef = useRef(null);
  const bodyRef = useRef(null);
  let [error, setError] = useState({});

  const configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  useEffect(() => {
    if (show) {
      toggleRef?.current?.click();
      canvasRef?.clear();
      setAgreement(false);
    } else {
      toggleCloseRef?.current?.click();
    }
  }, [show]);

  const signatureValidation = () => {
    const err = error;
    if (canvasRef?.isEmpty()) {
      err["signature"] = "Tidak boleh kosong";
      setError({ ...err });
    } else {
      setError({});
    }
  };

  useEffect(() => {
    pdpModalRef?.current.addEventListener("hidden.bs.modal", onReject);
    return () => {
      pdpModalRef?.current.removeEventListener("hidden.bs.modal", onReject);
    };
  }, []);

  useEffect(() => {
    if (show && Object.keys(error).length == 0) {
      submitToA();
    }
  }, [error]);
  const submitToA = () => {
    MySwal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        MySwal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/rekappendaftaran/insert-log-unduh",
        {
          user_id: Cookies.get("user_id"),
          jenis: jenis,
          path: window?.location?.pathname,
          tanda_tangan: canvasRef.toDataURL(),
          isi: bodyRef.current.innerHTML,
          file_id: file_id,
        },
        configs,
      )
      .then((result) => {
        toggleRef?.current.click();
        MySwal.close();
        if (result.data.result[0]?.statuscode == "200") {
          onResolve();
        } else {
          throw Error(result.data.result[0]?.message);
        }
      })
      .catch((err) => {
        console.log(err);
        // toggleCloseRef?.current.click();
        const message = err.response?.data?.result?.Message ?? err.message;
        MySwal.fire({
          title: "Terjadi Kesalahan!",
          icon: "error", // add html attribute if you want or removes
          text: message ?? "Error ketika akan terhubung dengan server.",
        });
      });
  };
  const MySwal = withReactContent(Swal);
  let [agreement, setAgreement] = useState(false);
  let [cBase64, setBase64] = useState("");
  let canvasRef = null;
  return (
    <div>
      <a
        href="#"
        ref={toggleRef}
        data-bs-toggle="modal"
        data-bs-target="#pratinjau"
        data-bs-backdrop="static"
        data-bs-keyboard="false"
      >
        <i className="bi bi-eye-fill text-white"></i>
      </a>
      <div
        className="modal fade"
        tabindex="-1"
        id="pratinjau"
        ref={pdpModalRef}
      >
        <div className="modal-dialog modal-lg">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Terms and Agreements</h5>
              <div
                className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal"
                aria-label="Close"
                ref={toggleCloseRef}
                onClick={() => {
                  onReject();
                }}
              >
                <span className="svg-icon svg-icon-2x">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <rect
                      opacity="0.5"
                      x="6"
                      y="17.3137"
                      width="16"
                      height="2"
                      rx="1"
                      transform="rotate(-45 6 17.3137)"
                      fill="currentColor"
                    />
                    <rect
                      x="7.41422"
                      y="6"
                      width="16"
                      height="2"
                      rx="1"
                      transform="rotate(45 7.41422 6)"
                      fill="currentColor"
                    />
                  </svg>
                </span>
              </div>
            </div>
            <div className="modal-body px-15">
              <div id="isi" ref={bodyRef}>
                <h4 className="text-center mb-5">
                  PERJANJIAN EKSPORT / UNDUH DATA PARTISIPAN DTS & DLA
                </h4>
                <p style={{ textIndent: "30px", textAlign: "justify" }}>
                  <strong>
                    Perjanjian ini merupakan bentuk kesepakatan yang sah dan
                    mengikat antara Anda dengan Pengelola Sistem (Puslitbang
                    Aptika dan IKP)
                  </strong>
                  . Dengan (memberi tanda (V) pada pernyataan [ "Saya menerima"]
                  dan) mengklik tombol “SETUJU”, berarti Anda menyatakan bahwa
                  Anda sudah mempelajari, memahami, dan menyetujui untuk terikat
                  oleh semua ketentuan dalam Perjanjian ini. Perjanjian ini
                  memuat ketentuan bahwa:
                </p>
                <ol>
                  <li style={{ textAlign: "justify" }}>
                    Anda memahami dan menyetujui bahwa setiap informasi yang ada
                    di dalam data yang akan diekspor dan/atau diunduh ini adalah
                    <strong>
                      milik Kementerian Komunikasi dan Informatika, yang di
                      dalamnya dimungkinkan memuat Data Pribadi milik
                      pendaftar/peserta (partisipan) Digital Talent Scholarship
                      (DTS) dan/atau Digital Leadership Academy (DLA)
                    </strong>
                    , yang dapat berupa: nama lengkap partisipan, jenis kelamin
                    partisipan, agama partisipan, status perkawinan partisipan,
                    kewarganegaraan partisipan, nomor induk kependudukan
                    partisipan, e-mail partisipan, nomor handphone partisipan,
                    tempat dan tanggal lahir partisipan, nama kontak darurat
                    partisipan, nomor kontak darurat partisipan, hubungan kontak
                    darurat partisipan, berkas foto partisipan, berkas Kartu
                    Tanda Penduduk partisipan, berkas ijazah partisipan, alamat
                    KTP lengkap partisipan, alamat domisili lengkap partisipan,
                    data pendidikan partisipan, data pekerjaan partisipan, data
                    nilai tes partisipan, data isian survei partisipan, data
                    riwayat pelatihan partisipan, data sertifikat partisipan,
                    dan/atau data lainnya yang termasuk data pribadi menurut
                    ketentuan peraturan perundang-undangan;
                  </li>
                  <li style={{ textAlign: "justify" }}>
                    Anda tidak akan menggunakan dan memproses informasi di dalam
                    hasil ekspor dan/atau unduhan data partisipan tersebut
                    selain untuk{" "}
                    <strong>
                      tujuan pengelolaan pelaksanaan program DTS dan/atau DLA
                      sesuai dengan lingkup penugasan yang diberikan
                    </strong>
                    ;
                  </li>
                  <li style={{ textAlign: "justify" }}>
                    Anda akan melakukan pemrosesan terhadap informasi di dalam
                    hasil ekspor data dan/atau unduhan data partisipan tersebut
                    secara bertanggung jawab dan dapat dibuktikan secara jelas;
                  </li>
                  <li style={{ textAlign: "justify" }}>
                    Anda akan{" "}
                    <strong>
                      melindungi keamanan informasi di dalam hasil ekspor data
                      dan/atau unduhan data partisipan
                    </strong>{" "}
                    tersebut dari pengaksesan yang tidak sah, pengungkapan yang
                    tidak sah, pengubahan yang tidak sah, penyalahgunaan,
                    perusakan, dan/atau penghilangan data pribadi;
                  </li>
                  <li style={{ textAlign: "justify" }}>
                    Anda tidak akan memberikan, mentransfer, mendistribusikan,
                    dan/atau menyediakan data pribadi partisipan DTS dan/atau
                    DLA yang ada di dalam hasil ekspor dan/atau unduhan data
                    partisipan tersebut kepada pihak lain;{" "}
                  </li>
                  <li style={{ textAlign: "justify" }}>
                    Anda setuju untuk{" "}
                    <strong>
                      mengganti rugi dan membebaskan Kepala Puslitbang Aptika
                      dan IKP
                    </strong>{" "}
                    dari setiap biaya, klaim, permintaan, pengeluaran, dan
                    kerugian dalam bentuk apapun yang timbul sehubungan dengan
                    pelanggaran keamanan informasi yang merupakan hasil dari
                    pelanggaran yang disebabkan oleh Anda atau kelalaian Anda,
                    dan dari setiap tindakan, kelalaian, atau keteledoran yang
                    menyebabkan terjadinya pelanggaran terhadap peraturan
                    pelindungan data pribadi di Indonesia;
                  </li>
                  <li style={{ textAlign: "justify" }}>
                    Anda bersedia diberikan{" "}
                    <strong> sanksi hukum, baik perdata maupun pidana</strong>,
                    sesuai ketentuan yang berlaku apabila Anda terbukti
                    melakukan pelanggaran.
                  </li>
                </ol>
              </div>
              <div className="row mt-5">
                <div className="col-6">
                  <div className="form-check">
                    <input
                      className="form-check-input"
                      type="checkbox"
                      value="1"
                      id="flexCheckDefault"
                      checked={agreement}
                      onChange={(e) => {
                        setAgreement(e.target.checked);
                      }}
                    />
                    <label
                      className="form-check-label"
                      htmlFor="flexCheckDefault"
                    >
                      Saya menerima syarat dan ketentuan dan berniat untuk
                      melanjutkan.
                    </label>
                  </div>
                </div>
                <div className="col-6">
                  <div
                    className="float-end"
                    style={{
                      width: "300px",
                      height: "150px",
                      border: error?.signature ? "1px solid red" : "",
                      backgroundColor: "whitesmoke",
                      position: "relative",
                      borderRadius: "10px",
                    }}
                  >
                    <button
                      className="btn btn-sm btn-light btn-text-dark btn-hover-text-dark font-weight-bold"
                      style={{ position: "absolute", right: 15, top: 8 }}
                      onClick={() => {
                        canvasRef.clear();
                      }}
                    >
                      <i className="bi bi-eraser-fill"></i>
                    </button>
                    <SignatureCanvas
                      ref={(ref) => (canvasRef = ref)}
                      onEnd={(e) => {
                        setError({ ...error, signature: "" });
                        // console.log(canvasRef.isEmpty());
                        // setBase64(canvasRef?.toDataURL());
                        // console.log("canvas ttd:", canvasRef.toDataURL);
                      }}
                      canvasProps={{
                        width: 300,
                        height: 150,
                      }}
                    />
                    <span className="text-danger">{error["signature"]}</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="modal-footer justify-content-between d-flex mt-5">
              <button
                className="btn btn-light btn-sm"
                data-bs-dismiss="modal"
                onClick={() => {
                  toggleCloseRef?.current.click();
                }}
              >
                Batal
              </button>
              <button
                className="btn btn-primary btn-sm"
                disabled={!agreement}
                onClick={() => {
                  signatureValidation();
                }}
              >
                Setuju
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ShowUUPdp;
