import React, { useEffect, useState } from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import DataTable from "react-data-table-component";
import moment from "moment";
import {
  handleFormatDate,
  capitalWord,
  PESERTA_PELATIHAN_KEY,
  PELATIHAN_ID_KEY,
  dateRange,
  tagColorMapStatusPeserta,
  disabledStatus,
} from "../Pelatihan/helper";
import "./assets/custom.css";
import { cekPermition } from "../../AksesHelper";

export default class RekappendaftaranviewdaftarpesertaContent extends React.Component {
  constructor(props) {
    super(props);
    this.pratinjauTriggerRef = React.createRef(null);
    this.pratinjauBerkasTriggerRef = React.createRef(null);
    this.stepOneRef = React.createRef(null);
    this.stepTwoRef = React.createRef(null);
    this.stepThreeRef = React.createRef(null);
    this.stepFourRef = React.createRef(null);
    this.stepperNav = React.createRef(null);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleClickSimpan = this.handleClickSimpanAction.bind(this);
    this.handleChangeStatusPeserta =
      this.handleChangeStatusPesertaAction.bind(this);
    this.handleChangeSort = this.handleChangeSortAction.bind(this);
    this.handleChangeSearch = this.handleChangeSearchAction.bind(this);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.onDocumentLoadSuccess = this.onDocumentLoadSuccess.bind(this);
  }
  state = {
    showModalBody: false,
    pageLength: null,
    pageNumber: 1,
    isLoading: false,
    dataxprofil: [],
    dataxstatuspeserta: [],
    dataxriwayatpelatihan: [],
    dataxberkas: [],
    dataxdokumen: [],
    titleheader: "",
    numberrow: 1,
    role_id: Cookies.get("role_id_user"),
    url_profile: "https://via.placeholder.com/200",
    url_ktp: "https://via.placeholder.com/200",
    url_ijazah: "https://via.placeholder.com/200",
    valStatusPesertaNew: [],
    progresProfil: 0,
    statusPesertaOptions: [],
    payloadData: {},
    pratinjau: [],
    riwayatLoading: false,
    page: 1,
    newPerPage: 10,
    tempLastNumber: 0,
    column: "id",
    sortDirection: "desc",
    searchText: "",
    isRowChangeRef: false,
    listUserID: [],
    currentUser: null,
    pratinjauFile: {},
  };
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  handleChangeSearchAction(e) {
    const searchText = e.currentTarget.value;
    this.setState({ searchText }, () => {
      if (searchText == "") {
        this.handleReloadRiwayat();
      }
    });
  }

  resolveMime(ext) {
    console.log("ext:", ext);
    let mime = "pdf";
    if (["jpg", "jpeg", "png", "webp", "svg", "giff", "tiff"].includes(ext)) {
      mime = "img";
    }
    return mime;
  }

  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    if (e.key == "Enter") {
      if (searchText == "") {
        this.handleReloadRiwayat();
      } else {
        this.setState({ searchText }, () => {
          this.handleReloadRiwayat();
        });
      }
    }
  }

  handlePerRowsChange = async (arg1, arg2, srcEvent) => {
    if (srcEvent == "page-change") {
      this.setState({ riwayatLoading: true }, () => {
        if (!this.state.isRowChangeRef) {
          this.handleReloadRiwayat(arg1, this.state.newPerPage);
        }
      });
    } else if (srcEvent == "row-change") {
      this.setState({ isRowChangeRef: true }, () => {
        this.handleReloadRiwayat(arg2, arg1);
      });
      this.setState({ riwayatLoading: true, newPerPage: arg1 }, () => {
        this.setState({ isRowChangeRef: false });
      });
    }
  };

  handleReloadRiwayat(page, newPerPage) {
    this.setState({ riwayatLoading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });
    const dataBody = {
      mulai: start_tmp,
      limit: length_tmp,
      sort: this.state.column,
      param: this.state.searchText,
      sort_val: this.state.sortDirection,
      id_user: window.location.pathname.split("/")[3],
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/riwayat-pelatihan-list",
        dataBody,
        this.configs,
      )
      .then((res) => {
        const dataxriwayatpelatihan =
          res.data.result.Riwayat && res.data.result.Riwayat[0] == null
            ? []
            : res.data.result.Riwayat;
        this.setState({ dataxriwayatpelatihan });
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          this.setState({
            riwayatLoading: false,
            totalRows: res.data.result.TotalLength,
            page,
          });
        } else {
          throw Error(messagex);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  componentDidMount() {
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    // pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;
    // pdfjs.GlobalWorkerOptions.workerSrc = "/assets/js/custom/pdf.worker.js";
    let segment_url = window.location.pathname.split("/");
    let urlSegmentOne = segment_url[3];
    let urlSegmentTwo = segment_url[4];
    let data = {
      id_user: urlSegmentOne,
      id_form: urlSegmentTwo,
      pelatihan_id: Cookies.get("pelatian_id"),
    };
    this.setState(
      {
        listUserID: Cookies.get(PESERTA_PELATIHAN_KEY)
          ? JSON.parse(Cookies.get(PESERTA_PELATIHAN_KEY))
          : [],
      },
      () => {
        this.setState({
          currentUser:
            this.state.listUserID.filter(
              (elem) => elem.id == urlSegmentOne,
            )[0] ?? null,
        });
      },
    );
    this.setState(
      {
        payloadData: {
          ...data,
        },
      },
      () => {
        this.handleReloadRiwayat();
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/pelatihan/detail-peserta-rekappendaftaran",
            this.state.payloadData,
            this.configs,
          )
          .then((res) => {
            swal.close();
            const dataxprofil = res.data.result.Profil[0];
            const dataxstatuspeserta = res.data.result.Status_peserta[0];
            const dataxberkas = res.data.result.Berkas;
            const dataxdokumen = res.data.result.Dokumen[0];
            const datax = res.data.result.list_peserta;
            this.setState({
              dataxprofil: dataxprofil,
              titleheader: dataxprofil.name + " - " + dataxprofil.nik,
              url_profile:
                process.env.REACT_APP_BASE_BACKDEV_API_URI +
                "/uploads/foto/" +
                this.state.dataxprofil.foto,
              url_ktp:
                process.env.REACT_APP_BASE_BACKDEV_API_URI +
                "/uploads/ktp/" +
                this.state.dataxprofil.file_ktp,
              url_ijazah:
                process.env.REACT_APP_BASE_BACKDEV_API_URI +
                "/uploads/ijazah/" +
                this.state.dataxprofil.file_path,
              dataxstatuspeserta: dataxstatuspeserta ?? {
                status_peserta: "",
              },
              dataxberkas: dataxberkas,
              dataxdokumen: dataxdokumen ?? {},
              datax: datax,
              valStatusPesertaNew:
                dataxstatuspeserta?.status_peserta == null
                  ? {
                      label: capitalWord("Belum ada status peserta"),
                      value: "Belum ada status peserta",
                    }
                  : {
                      label: capitalWord(
                        dataxstatuspeserta.status_peserta ?? "",
                      ),
                      value: dataxstatuspeserta?.id,
                    },
            });
          })
          .catch((err) => {
            if (err.data?.response?.result?.Data) {
              console.log(err);
            }
            const message = err.data?.response
              ? err.data?.response?.result?.Message
              : err.message;
            swal.fire({
              title: "Terjadi kesalahan",
              text:
                message ??
                "Terjadi kesalahan sata melakukan memindahkan peserta...",
              icon: "error",
              confirmButtonText: "Ok",
            });
          });

        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/list-status-peserta-by-pelatihan",
            { pelatihan_id: Cookies.get(PELATIHAN_ID_KEY) },
            this.configs,
          )
          .then((res) => {
            if (res.data?.result?.Data) {
              const rawdData = res.data.result.Data.map((elem) => {
                return {
                  label: capitalWord(elem.name),
                  value: elem.id,
                };
              }).filter((elem) => elem.label.toLowerCase() != "submit2");
              this.setState({ statusPesertaOptions: rawdData });
            }
          })
          .catch((err) => {
            console.log(err);
          });
      },
    );
  }

  onDocumentLoadSuccess(pdf) {
    let pageLength = pdf?._pdfInfo?.numPages ?? 1;
    this.setState({ pageLength });
  }
  getNilaiPeserta(idUser, idPelatihan) {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/rekappendaftaran/list-user-silabus",
        {
          pelatihan_id: idPelatihan,
          user_id: idUser,
        },
        this.configs,
      )
      .then((result) => {
        this.setState({ pratinjau: result.data.result }, () => {
          this.pratinjauTriggerRef.current.click();
        });
      })
      .catch((err) => {
        let statux = err.response.data.result.Status;
        let messagex = err.response.data.result.Message;
        swal
          .fire({
            title:
              messagex ?? "Terjadi kesalahan saat berkomunikasi dengan server.",
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
            }
          });
      });
  }
  handleChangeSortAction(column, sortDirection) {
    let server_name = "";
    if (column.name == "Pelatihan") {
      server_name = "pelatihan";
    } else if (column.name == "Pelaksanaan") {
      server_name = "pelatihan_mulai";
    } else if (column.name == "Status") {
      server_name = "status_peserta";
    }

    this.setState(
      {
        column: server_name,
        sortDirection: sortDirection,
      },
      () => {
        this.handleReloadRiwayat(1, this.state.newPerPage);
      },
    );
  }
  customLoader = (
    <div style={{ padding: "24px" }}>
      <img src="/assets/media/loader/loader-biru.gif" />
    </div>
  );
  columns = [
    {
      name: "No",
      center: true,
      width: "70px",
      cell: (row, index) => (
        <div>
          <span>{this.state.numberrow + index + 0}</span>
        </div>
      ),
    },
    {
      name: "Nama Pelatihan",
      className: "min-w-300px mw-300px",
      width: "350px",
      sortable: true,
      grow: 6,
      wrap: true,
      allowOverflow: false,
      cell: (row) => (
        <div>
          <label className="d-flex flex-stack my-2">
            <span className="d-flex align-items-center me-2">
              <span className="d-flex flex-column">
                <span className="text-muted fs-7 fw-semibold">
                  {row.akademi}
                </span>
                <h6 className="fw-bolder fs-7 mb-1">
                  {row.id} - {capitalWord(row.pelatihan)}
                </h6>
                <h6 className="text-muted fs-7 fw-semibold mb-1">
                  {row.penyelenggara}
                </h6>
                <span className="text-muted fs-7 fw-semibold">
                  {row.metode_pelatihan}
                </span>
              </span>
            </span>
          </label>
        </div>
      ),
    },
    {
      name: "Tgl. Pelatihan",
      width: "200px",
      sortable: true,
      cell: (row) => (
        <div>
          <span className="fs-7">
            {dateRange(
              row.pelaksanaan.split(" sd ")[0],
              row.pelaksanaan.split(" sd ")[1],
              "DD MMM YYYY",
            )}
          </span>
        </div>
      ),
    },
    {
      name: "Lokasi",
      width: "150px",
      sortable: true,
      cell: (row) => (
        <div>
          <span className="d-flex flex-column my-2">
            {row.metode_pelatihan?.toLowerCase()?.includes("offline") ? (
              <span
                className="fs-7"
                style={{
                  overflow: "hidden",
                  whiteSpace: "wrap",
                  textOverflow: "ellipses",
                }}
              >
                {`${capitalWord(row.nm_kab?.toLowerCase())}`}
              </span>
            ) : (
              <span
                className="fs-7"
                style={{
                  overflow: "hidden",
                  whiteSpace: "wrap",
                  textOverflow: "ellipses",
                }}
              >
                {capitalWord(row.metode_pelatihan)}
              </span>
            )}
          </span>
        </div>
      ),
    },
    {
      name: "Status",
      width: "225px",
      sortable: true,
      cell: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
                tagColorMapStatusPeserta[row.status_peserta] ??
              "danger" + " fs-9 m-1"
            }
          >
            {capitalWord(row.status_peserta) ?? "-"}
          </span>
        </div>
      ),
    },
    {
      name: "Alasan Batal",
      width: "225px",
      sortable: true,
      cell: (row) => (
        <div>
          {row.status_peserta?.toLowerCase() == "pembatalan" ? (
            <p>{row.alasan_batal ?? "Belum ada alasan"}</p>
          ) : (
            "-"
          )}
        </div>
      ),
    },
    {
      name: "Nilai",
      sortable: false,
      // width: "150px",
      cell: (row) => (
        <div>
          <a
            href="#"
            // disabled={!row.status_peserta?.includes("Lulus Pelatihan")}
            onClick={(e) => {
              e.preventDefault();
              this.getNilaiPeserta(row.user_id, row.id);
            }}
            id={row.id}
            title="Lihat Nilai"
            className="btn btn-icon btn-success btn-sm me-1"
          >
            <i class="bi bi-journal-check text-white"></i>
          </a>
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
        //fontWeight: 'bold',
      },
    },
  };
  handleClickBatalAction(e) {
    window.location =
      "/pelatihan/detail-rekappendaftaran/" + Cookies.get("pelatian_id");
  }
  handleClickSimpanAction(e) {
    e.preventDefault();
    if (
      this.state.valStatusPesertaNew.value == this.state.dataxstatuspeserta.id
    ) {
      this.setState({ isLoading: false });
      swal.fire({
        title: "Terjadi kesalahan",
        body: "Tidak ada perubahan pada status peserta.",
        icon: "warning",
      });
    }
    swal
      .fire({
        title: "Perhatian!",
        text: "Ubah status Peserta?",
        icon: "warning",
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, update!",
        cancelButtonText: "Batal",
      })
      .then((result) => {
        if (result.isConfirmed) {
          this.setState({ isLoading: true });
          const payload = {
            status_pilihan: 0,
            status: this.state.valStatusPesertaNew.value,
            pelatihan_id: this.state.payloadData.pelatihan_id,
            reminder_berkas: 0,
            reminder_profile: 0,
            reminder_riwayat: 1,
            reminder_dokumen: 1,
            updatedby: Cookies.get("user_id"),
            categoryOptItems: [
              {
                id_user: this.state.payloadData.id_user,
              },
            ],
          };
          swal.fire({
            title: "Mohon Tunggu!",
            icon: "info", // add html attribute if you want or remove
            allowOutsideClick: false,
            didOpen: () => {
              swal.showLoading();
            },
          });

          axios
            .post(
              process.env.REACT_APP_BASE_API_URI +
                "/rekappendaftaran/update-peserta-pendaftaran-bulk",
              [payload],
              this.configs,
            )
            .then((res) => {
              this.setState({ isLoading: false });
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            })
            .catch((error) => {
              this.setState({ isLoading: false });
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            });
        }
      });
  }
  handleChangeStatusPesertaAction = (status_peserta) => {
    this.setState({ valStatusPesertaNew: status_peserta });
  };
  render() {
    const changeActiveStepper = (e, index) => {
      this.stepperNav?.current?.childNodes?.forEach((li) => {
        li.childNodes[0]?.classList.remove("active");
        li.childNodes[0]?.classList.remove("false");
        li.childNodes[0]?.classList.add("false");
      });
      this.stepperNav?.current?.childNodes[
        index
      ]?.childNodes[0]?.classList.remove("false");
      this.stepperNav?.current?.childNodes[index]?.childNodes[0]?.classList.add(
        "active",
      );
    };
    return (
      <div>
        <a
          href="#"
          hidden
          data-bs-toggle="modal"
          data-bs-target="#pratinjau-file"
          ref={this.pratinjauBerkasTriggerRef}
        ></a>
        <div className="modal fade" tabindex="-1" id="pratinjau-file">
          <div className="modal-dialog modal-lg">
            <div className="modal-content">
              <div className="modal-header">
                <h5 class="modal-title">Preview Lampiran</h5>
                <div
                  class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <span class="svg-icon svg-icon-2x">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <rect
                        opacity="0.5"
                        x="6"
                        y="17.3137"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(-45 6 17.3137)"
                        fill="currentColor"
                      />
                      <rect
                        x="7.41422"
                        y="6"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(45 7.41422 6)"
                        fill="currentColor"
                      />
                    </svg>
                  </span>
                </div>
              </div>
              {this.state.showModalBody && (
                <div className="modal-body">
                  {this.state.pratinjauFile.mime == "img" && (
                    <img
                      src={this.state.pratinjauFile.value}
                      width="100%"
                      className="rounded"
                    />
                  )}
                  {this.state.pratinjauFile.mime == "pdf" && (
                    <div className="h-100">
                      <object
                        data={this.state.pratinjauFile.value}
                        type="application/pdf"
                        className="w-100"
                        style={{ height: "500px" }}
                      >
                        <embed
                          src={this.state.pratinjauFile.value}
                          type="application/pdf"
                        />
                      </object>
                    </div>
                  )}

                  <div className="modal-footer">
                    <div className="col-lg-12 d-block mb-3 py-3 px-5 text-primary rounded bg-light-primary ">
                      Jika lampiran tidak dapat dipreview, silahkan unduh dengan
                      mengklik tombol download
                    </div>

                    <button
                      className="btn btn-light btn-sm"
                      data-bs-dismiss="modal"
                    >
                      Close
                    </button>
                    <a
                      className="btn btn-primary btn-sm"
                      target="__blank"
                      href={this.state.pratinjauFile.value}
                    >
                      Download
                    </a>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
        <a
          href="#"
          hidden
          data-bs-toggle="modal"
          data-bs-target="#pratinjau-nilai"
          ref={this.pratinjauTriggerRef}
        ></a>

        <div className="modal fade" tabindex="-1" id="pratinjau-nilai">
          <div className="modal-dialog modal-md">
            <div className="modal-content">
              <div className="modal-header pe-0 pb-0">
                <div className="row w-100">
                  <div className="col-12 d-flex w-100 justify-content-between">
                    <h5 className="modal-title">Nilai Peserta</h5>
                    <div
                      className="btn btn-icon btn-sm btn-active-light-primary"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                    >
                      <span className="svg-icon svg-icon-2x">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                          fill="none"
                        >
                          <rect
                            opacity="0.5"
                            x="6"
                            y="17.3137"
                            width="16"
                            height="2"
                            rx="1"
                            transform="rotate(-45 6 17.3137)"
                            fill="currentColor"
                          />
                          <rect
                            x="7.41422"
                            y="6"
                            width="16"
                            height="2"
                            rx="1"
                            transform="rotate(45 7.41422 6)"
                            fill="currentColor"
                          />
                        </svg>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              {this.state.pratinjau && (
                <div className="modal-body">
                  <div className="card">
                    <div className="row justify-content-start">
                      <div className="col-12">
                        <h4 className="fw-bolder">
                          {capitalWord(this.state.pratinjau[0]?.nama_silabus)}
                        </h4>
                        <span className="text-muted">Nilai Peserta</span>
                      </div>
                      <div className="col-12">
                        <ul className="list-group">
                          {this.state.pratinjau.map((elem) => (
                            <div
                              className="d-flex align-items-center bg-light-primary rounded p-5 mb-2"
                              key={`detail_${elem.silabus_header_id}`}
                            >
                              <span className="svg-icon svg-icon-primary me-5">
                                <span className="svg-icon svg-icon-1">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <path
                                      opacity="0.3"
                                      d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                                      fill="currentColor"
                                    ></path>
                                    <path
                                      d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                                      fill="currentColor"
                                    ></path>
                                  </svg>
                                </span>
                              </span>
                              <div className="flex-grow-1 me-2">
                                <a
                                  href="#"
                                  className="fw-bold text-gray-800 text-hover-primary fs-6"
                                >
                                  {elem.silabus_detail ?? "Tidak ada data."}
                                </a>
                              </div>
                              <span className="fw-bold text-primary py-1">
                                {elem.nilai ?? ""}
                              </span>
                            </div>
                          ))}
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              )}

              <div className="modal-footer">
                <button
                  className="btn btn-light btn-sm"
                  data-bs-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>

        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Rekap Pendaftaran
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Detail User DTS
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <div className="d-flex align-items-center position-relative my-1">
                            {this.state.listUserID.length > 0 &&
                              this.state.currentUser?.index > 1 && (
                                <a
                                  href={
                                    "/pelatihan/view-rekappendaftaran-daftarpeserta/" +
                                    this.state.listUserID[
                                      this.state.currentUser?.index - 2
                                    ]?.id +
                                    "/" +
                                    this.state.payloadData.id_form
                                  }
                                  className="btn btn-sm btn-primary me-2"
                                >
                                  <i className="fa fa-chevron-left me-1"></i>
                                  Peserta Sebelumnya
                                </a>
                              )}
                            {this.state.listUserID.length > 0 &&
                              this.state.currentUser?.index <
                                this.state.listUserID.length && (
                                <a
                                  href={
                                    "/pelatihan/view-rekappendaftaran-daftarpeserta/" +
                                    this.state.listUserID[
                                      this.state.currentUser?.index
                                    ]?.id +
                                    "/" +
                                    this.state.payloadData.id_form
                                  }
                                  className="btn btn-sm btn-primary"
                                >
                                  Peserta Selanjutnya
                                  <i className="fa fa-chevron-right me-1"></i>
                                </a>
                              )}
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="mb-7">
                          <div className="d-flex flex-wrap py-3">
                            <div className="flex-grow-1 mb-3">
                              <div className="d-flex flex-wrap flex-sm-nowrap mb-3">
                                <div className="me-7 mb-4">
                                  <div className="symbol symbol-100px symbol-lg-150px symbol-fixed position-relative">
                                    <img
                                      src={
                                        process.env.REACT_APP_BASE_API_URI +
                                        "/download/get-file?path=" +
                                        this.state.dataxprofil.foto
                                      }
                                      alt="Display Profile"
                                    />
                                  </div>
                                </div>
                                <div className="flex-grow-1">
                                  <div className="col-lg-12 justify-content-between align-items-start flex-wrap mb-2">
                                    <div className="d-flex flex-column">
                                      <h4 className="me-1 mb-0">
                                        {" "}
                                        {
                                          this.state.dataxstatuspeserta
                                            .nomor_registrasi
                                        }
                                      </h4>
                                      <div className="d-flex align-items-center mb-2">
                                        <h2 className="fw-bolder me-1 mb-0">
                                          {" "}
                                          {this.state.dataxprofil.name}
                                        </h2>
                                      </div>
                                      <div className="d-flex flex-wrap fw-bold fs-6 mb-4 pe-2">
                                        {/* <span className="d-flex align-items-center fs-7 text-muted fw-semibold me-5 mb-3">
                                          <span className="svg-icon svg-icon-4 me-1">
                                            <i className="bi bi-person-vcard me-1"></i>
                                            {this.state.dataxprofil.nik}
                                          </span>
                                        </span>
                                        <span className="d-flex align-items-center fs-7 fw-semibold text-muted me-5 mb-3">
                                          <span className="svg-icon svg-icon-4 me-1">
                                            <i className="bi bi-telephone me-1"></i>
                                            +
                                            {
                                              this.state.dataxprofil
                                                .nomor_handphone
                                            }
                                          </span>
                                        </span>*/}
                                        <span className="d-flex align-items-center fs-7 text-muted fw-semibold me-5 mb-3">
                                          <span className="svg-icon svg-icon-4 me-1">
                                            <i className="bi bi-envelope-at me-1"></i>
                                            {this.state.dataxprofil.email}
                                          </span>
                                        </span>
                                      </div>
                                      <div className="d-flex flex-column flex-grow-1 pe-8">
                                        <div className="py-3 px-5 rounded bg-light-primary ">
                                          <form
                                            action="#"
                                            onSubmit={this.handleClickSimpan}
                                            className="row w-100"
                                          >
                                            <div className="col-12">
                                              <label className="fw-semibold form-label required">
                                                Status Peserta{" "}
                                                <a
                                                  href="#"
                                                  data-bs-toggle="modal"
                                                  data-bs-target="#filter"
                                                  className="ms-3 float-end text-primary fw-semibold"
                                                >
                                                  <i className="bi bi-info-circle text-primary"></i>{" "}
                                                  Mohon Dibaca
                                                </a>
                                              </label>
                                              <div className="input-group mb-1">
                                                {this.state.role_id == 1 ? (
                                                  <>
                                                    <Select
                                                      name="status-peserta"
                                                      placeholder="Silahkan pilih"
                                                      value={
                                                        this.state
                                                          .valStatusPesertaNew
                                                      }
                                                      noOptionsMessage={() => {
                                                        "Tidak ada data";
                                                      }}
                                                      className="form-select  form-select-sm d-inline-block border-0 selectpicker p-0"
                                                      options={
                                                        this.state
                                                          .statusPesertaOptions
                                                      }
                                                      onChange={
                                                        this
                                                          .handleChangeStatusPeserta
                                                      }
                                                    />
                                                  </>
                                                ) : (
                                                  <>
                                                    <Select
                                                      name="status-peserta"
                                                      placeholder="Silahkan pilih"
                                                      value={
                                                        this.state
                                                          .valStatusPesertaNew
                                                      }
                                                      noOptionsMessage={() => {
                                                        "Tidak ada data";
                                                      }}
                                                      className="form-select  form-select-sm d-inline-block border-0 selectpicker p-0"
                                                      options={
                                                        this.state
                                                          .statusPesertaOptions
                                                      }
                                                      onChange={
                                                        this
                                                          .handleChangeStatusPeserta
                                                      }
                                                      isDisabled={disabledStatus().includes(
                                                        this.state.valStatusPesertaNew?.label?.toLowerCase(),
                                                      )}
                                                    />
                                                  </>
                                                )}

                                                <span className="input-group-btn">
                                                  <button className="btn btn-success btn-sm mt-1 ms-2">
                                                    {this.state.isLoading ? (
                                                      <>
                                                        <span
                                                          className="spinner-border spinner-border-sm me-2"
                                                          role="status"
                                                          aria-hidden="true"
                                                        ></span>
                                                        <span className="sr-only">
                                                          Loading...
                                                        </span>
                                                        Loading...
                                                      </>
                                                    ) : (
                                                      <>
                                                        <i className="bi bi-check-circle-fill"></i>
                                                        Simpan
                                                      </>
                                                    )}
                                                  </button>
                                                </span>
                                              </div>

                                              <div
                                                className="modal fade"
                                                tabIndex="-1"
                                                id="filter"
                                              >
                                                <div className="modal-dialog modal-md">
                                                  <div className="modal-content">
                                                    <div className="modal-header">
                                                      <h5 className="modal-title">
                                                        <span className="svg-icon svg-icon-5 me-1">
                                                          <i className="bi bi-info-circle text-black"></i>
                                                        </span>
                                                        Panduan Memilih Status
                                                        Pendaftar/Peserta
                                                      </h5>
                                                      <div
                                                        className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                                        data-bs-dismiss="modal"
                                                        aria-label="Close"
                                                      >
                                                        <span className="svg-icon svg-icon-2x">
                                                          <svg
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            width="24"
                                                            height="24"
                                                            viewBox="0 0 24 24"
                                                            fill="none"
                                                          >
                                                            <rect
                                                              opacity="0.5"
                                                              x="6"
                                                              y="17.3137"
                                                              width="16"
                                                              height="2"
                                                              rx="1"
                                                              transform="rotate(-45 6 17.3137)"
                                                              fill="currentColor"
                                                            />
                                                            <rect
                                                              x="7.41422"
                                                              y="6"
                                                              width="16"
                                                              height="2"
                                                              rx="1"
                                                              transform="rotate(45 7.41422 6)"
                                                              fill="currentColor"
                                                            />
                                                          </svg>
                                                        </span>
                                                      </div>
                                                    </div>
                                                    <div className="modal-body">
                                                      <div className="row p-5">
                                                        <p>
                                                          Harap membaca
                                                          keterangan status
                                                          berikut. Setiap PIC
                                                          Pelatihan{" "}
                                                          <strong>
                                                            Wajib Disiplin
                                                          </strong>{" "}
                                                          dalam melakukan update
                                                          status peserta,
                                                          <br />
                                                          karena setiap status
                                                          berdampak kepada
                                                          siklus peserta pada
                                                          aplikasi
                                                        </p>
                                                        <ol>
                                                          <li className="mb-3">
                                                            <h6 className="text-primary">
                                                              Submit
                                                            </h6>
                                                            <span>
                                                              Status default
                                                              ketika Pendaftar
                                                              melakukan
                                                              pendaftaran pada
                                                              pelatihan.
                                                              Pendaftar dengan
                                                              status ini{" "}
                                                              <strong>
                                                                tidak dapat
                                                              </strong>{" "}
                                                              melakukan
                                                              pendaftaran ke
                                                              pelatihan lain.
                                                            </span>
                                                          </li>
                                                          <li className="mb-3">
                                                            <h6 className="text-danger">
                                                              Tidak Lulus
                                                              Administrasi
                                                            </h6>
                                                            <span>
                                                              Pendaftar
                                                              dinyatakan Tidak
                                                              Lulus Seleksi
                                                              Administrasi.
                                                              Setelah diubah ke
                                                              status ini,
                                                              Pendaftar{" "}
                                                              <strong>
                                                                dapat
                                                              </strong>{" "}
                                                              melakukan
                                                              pendaftaran ke
                                                              pelatihan lain.
                                                            </span>
                                                          </li>
                                                          <li className="mb-3">
                                                            <h6 className="text-primary">
                                                              Tes Substansi
                                                            </h6>
                                                            <span>
                                                              Pendaftar terpilih
                                                              untuk mengikuti
                                                              Tes Substansi.
                                                              Pendaftar dengan
                                                              status ini{" "}
                                                              <strong>
                                                                tidak dapat
                                                              </strong>{" "}
                                                              melakukan
                                                              pendaftaran ke
                                                              pelatihan lain.
                                                            </span>
                                                          </li>
                                                          <li className="mb-3">
                                                            <h6 className="text-danger">
                                                              Tidak Lulus Tes
                                                              Substansi
                                                            </h6>
                                                            <span>
                                                              Pendaftar
                                                              dinyatakan Tidak
                                                              Lulus Tes
                                                              Substansi.
                                                              Pendaftar dengan
                                                              status ini{" "}
                                                              <strong>
                                                                dapat
                                                              </strong>{" "}
                                                              melakukan
                                                              pendaftaran ke
                                                              pelatihan lain.
                                                            </span>
                                                          </li>
                                                          <li className="mb-3">
                                                            <h6 className="text-success">
                                                              Lulus Tes
                                                              Substansi
                                                            </h6>
                                                            <span>
                                                              Pendaftar
                                                              dinyatakan Lulus
                                                              Tes Substansi.
                                                              Pendaftar dengan
                                                              status ini{" "}
                                                              <strong>
                                                                tidak dapat
                                                              </strong>{" "}
                                                              melakukan
                                                              pendaftaran ke
                                                              pelatihan lain.
                                                            </span>
                                                          </li>
                                                          <li className="mb-3">
                                                            <h6 className="text-success">
                                                              Diterima
                                                            </h6>
                                                            <span>
                                                              Pendaftar
                                                              dinyatakan
                                                              Diterima untuk
                                                              mengikuti
                                                              pelatihan.
                                                              Pendaftar dengan
                                                              status ini{" "}
                                                              <strong>
                                                                tidak dapat
                                                              </strong>{" "}
                                                              melakukan
                                                              pendaftaran ke
                                                              pelatihan lain.
                                                            </span>
                                                          </li>
                                                          <li className="mb-3">
                                                            <h6 className="text-success">
                                                              Pelatihan
                                                            </h6>
                                                            <span>
                                                              Peserta dinyatakan
                                                              mengikuti
                                                              pelatihan, selain
                                                              dapat diupdate
                                                              secara manual oleh
                                                              PIC Pelatihan,
                                                              <br />
                                                              Sistem juga akan
                                                              otomatis
                                                              mengudpate seluruh
                                                              pendaftar dengan
                                                              status "Diterima"
                                                              menjadi "Pelatihan
                                                              " jika telah
                                                              memasuki jadwal
                                                              pelatihan.
                                                              <br />
                                                              Peserta dengan
                                                              status ini{" "}
                                                              <strong>
                                                                tidak dapat
                                                              </strong>{" "}
                                                              melakukan
                                                              pendaftaran ke
                                                              pelatihan lain.
                                                            </span>
                                                          </li>
                                                          <li className="mb-3">
                                                            <h6 className="text-success">
                                                              Lulus Pelatihan -
                                                              Kehadiran
                                                            </h6>
                                                            <span>
                                                              Peserta dinyatakan
                                                              Lulus berdasrkan
                                                              kehadiran (ini
                                                              berlaku bagi
                                                              pelatihan yang
                                                              menentukan
                                                              kelulusan
                                                              berdasarkan
                                                              kehadiran).
                                                              Peserta dengan
                                                              status ini{" "}
                                                              <strong>
                                                                dapat
                                                              </strong>{" "}
                                                              melakukan
                                                              pendaftaran ke
                                                              pelatihan lain.
                                                            </span>
                                                          </li>
                                                          <li className="mb-3">
                                                            <h6 className="text-success">
                                                              Lulus Pelatihan -
                                                              Nilai
                                                            </h6>
                                                            <span>
                                                              Peserta dinyatakan
                                                              Lulus berdasrkan
                                                              nilai (ini berlaku
                                                              bagi pelatihan
                                                              yang menentukan
                                                              kelulusan
                                                              berdasarkan
                                                              nilai). Peserta
                                                              dengan status ini{" "}
                                                              <strong>
                                                                dapat
                                                              </strong>{" "}
                                                              melakukan
                                                              pendaftaran ke
                                                              pelatihan lain.
                                                            </span>
                                                          </li>
                                                          <li className="mb-3">
                                                            <h6 className="text-success">
                                                              Berhak Sertifikasi
                                                            </h6>
                                                            <span>
                                                              Peserta telah
                                                              lulus pelatihan
                                                              dan dinyatakan
                                                              berhak mengikuti
                                                              sertifikasi (ini
                                                              berlaku bagi
                                                              pelatihan yang
                                                              mengadakan
                                                              sertifikasi
                                                              kompetensi
                                                              berbasis
                                                              SKKNI/Global/Industri).
                                                              Peserta dengan
                                                              status ini{" "}
                                                              <strong>
                                                                dapat
                                                              </strong>{" "}
                                                              melakukan
                                                              pendaftaran ke
                                                              pelatihan lain.
                                                            </span>
                                                          </li>
                                                          <li className="mb-3">
                                                            <h6 className="text-success">
                                                              Ikut Sertifikasi
                                                            </h6>
                                                            <span>
                                                              Peserta telah
                                                              lulus pelatihan
                                                              dan dinyatakan
                                                              mengikuti
                                                              sertifikasi (ini
                                                              berlaku bagi
                                                              pelatihan yang
                                                              mengadakan
                                                              sertifikasi
                                                              kompetensi
                                                              berbasis
                                                              SKKNI/Global/Industri)).
                                                              Peserta dengan
                                                              status ini{" "}
                                                              <strong>
                                                                dapat
                                                              </strong>{" "}
                                                              melakukan
                                                              pendaftaran ke
                                                              pelatihan lain.
                                                            </span>
                                                          </li>
                                                          <li className="mb-3">
                                                            <h6 className="text-success">
                                                              Lulus Sertifikasi
                                                            </h6>
                                                            <span>
                                                              Peserta telah
                                                              lulus pelatihan
                                                              dan dinyatakan
                                                              lulus sertifikasi
                                                              (ini berlaku bagi
                                                              pelatihan yang
                                                              mengadakan
                                                              sertifikasi
                                                              kompetensi
                                                              berbasis
                                                              SKKNI/Global/Industri)).
                                                              Peserta dengan
                                                              status ini{" "}
                                                              <strong>
                                                                dapat
                                                              </strong>{" "}
                                                              melakukan
                                                              pendaftaran ke
                                                              pelatihan lain.
                                                            </span>
                                                          </li>
                                                          <li className="mb-3">
                                                            <h6 className="text-danger">
                                                              Tidak Lulus
                                                              Pelatihan - Nilai
                                                            </h6>
                                                            <span>
                                                              Peserta dinyatakan
                                                              Tidak Lulus
                                                              berdasarkan nilai
                                                              (ini berlaku bagi
                                                              pelatihan yang
                                                              menentukan
                                                              kelulusan
                                                              berdasarkan
                                                              nilai). Peserta
                                                              dengan status ini{" "}
                                                              <strong>
                                                                dapat
                                                              </strong>{" "}
                                                              melakukan
                                                              pendaftaran ke
                                                              pelatihan lain.
                                                            </span>
                                                          </li>
                                                          <li className="mb-3">
                                                            <h6 className="text-danger">
                                                              Tidak Lulus
                                                              Pelatihan -
                                                              Kehadiran
                                                            </h6>
                                                            <span>
                                                              Peserta dinyatakan
                                                              Tidak Lulus
                                                              berdasarkan
                                                              kehadiran (ini
                                                              berlaku bagi
                                                              pelatihan yang
                                                              menentukan
                                                              kelulusan
                                                              berdasarkan
                                                              kehadiran).
                                                              Peserta dengan
                                                              status ini{" "}
                                                              <strong>
                                                                dapat
                                                              </strong>{" "}
                                                              melakukan
                                                              pendaftaran ke
                                                              pelatihan lain.
                                                            </span>
                                                          </li>
                                                          <li className="mb-3">
                                                            <h6 className="text-danger">
                                                              Tidak Hadir
                                                            </h6>
                                                            <span>
                                                              Peserta tidak
                                                              mengikuti
                                                              pelatihan. Peserta
                                                              dengan status ini{" "}
                                                              <strong>
                                                                dapat
                                                              </strong>{" "}
                                                              melakukan
                                                              pendaftaran ke
                                                              pelatihan lain.
                                                            </span>
                                                          </li>
                                                          <li className="mb-3">
                                                            <h6 className="text-warning">
                                                              Cadangan
                                                            </h6>
                                                            <span>
                                                              Pendaftar
                                                              dinyatakan sebagai
                                                              Cadangan, ini
                                                              berguna jika
                                                              nantinya ybs
                                                              menggantikan
                                                              Peserta lain.
                                                              Pendaftar dengan
                                                              status Cadangan,{" "}
                                                              <strong>
                                                                tidak dapat
                                                              </strong>{" "}
                                                              mendaftar ke
                                                              Pelatihan lain
                                                              sampai statusnya
                                                              diudpate oleh PIC
                                                              Pelatihan menjadi
                                                              status lain.
                                                              Sistem juga akan
                                                              otomatis
                                                              mengudpate seluruh
                                                              pendaftar dengan
                                                              status "Cadangan"
                                                              menjadi "Tidak
                                                              Lulus
                                                              Administrasi" jika
                                                              jadwal pelatihan
                                                              telah berakhir.
                                                            </span>
                                                          </li>
                                                          <li className="mb-3">
                                                            <h6 className="text-danger">
                                                              Mengundurkan Diri
                                                            </h6>
                                                            <span>
                                                              Peserta
                                                              mengundurkan diri
                                                              saat pelatihan
                                                              berlangsung.
                                                              Peserta dengan
                                                              status ini{" "}
                                                              <strong>
                                                                dapat
                                                              </strong>{" "}
                                                              melakukan
                                                              pendaftaran ke
                                                              pelatihan lain.
                                                            </span>
                                                          </li>
                                                          <li className="mb-3">
                                                            <h6 className="text-danger">
                                                              Pembatalan
                                                            </h6>
                                                            <span>
                                                              Pendaftar
                                                              membatalkan status
                                                              Pendaftarannya.
                                                              Pendaftar dapat
                                                              melakukan
                                                              pembatalan secara
                                                              mandiri melalui
                                                              akun
                                                              masing-masing,
                                                              selama masih pada
                                                              jadwal Pendaftaran
                                                              Pelatihan.
                                                              Pendaftar dengan
                                                              status ini{" "}
                                                              <strong>
                                                                dapat
                                                              </strong>{" "}
                                                              melakukan
                                                              pendaftaran ke
                                                              pelatihan lain.
                                                            </span>
                                                          </li>
                                                        </ol>
                                                      </div>
                                                    </div>
                                                    <div className="modal-footer">
                                                      <div className="d-flex justify-content-between">
                                                        <button
                                                          type="reset"
                                                          className="btn btn-sm btn-light me-3"
                                                          data-bs-dismiss="modal"
                                                          aria-label="Close"
                                                        >
                                                          Tutup
                                                        </button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </form>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div className="d-flex border-bottom p-0">
                          <ul
                            className="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap"
                            style={{
                              overflowX: "auto",
                              overflowY: "hidden",
                              display: "flex",
                              whiteSpace: "nowrap",
                              marginBottom: 0,
                            }}
                          >
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary active"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_1"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                  Data Pokok
                                </span>
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary false"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_2"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                  Berkas Pendaftaran
                                </span>
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary false"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_3"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                  Riwayat Pelatihan
                                </span>
                              </a>
                            </li>
                          </ul>
                        </div>
                        <div className="tab-content" id="detail-account-tab">
                          <div
                            className="tab-pane fade show active"
                            id="kt_tab_pane_1"
                            role="tabpanel"
                          >
                            <div className="row mt-7 pb-7">
                              <h2 className="fs-5 pt-7 text-muted mb-3">
                                Data Diri
                              </h2>
                              <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                <label className="form-label">Usia</label>
                                <div>
                                  <strong>
                                    {Math.floor(
                                      moment().diff(
                                        this.state.dataxprofil.tanggal_lahir,
                                        "years",
                                      ),
                                      true,
                                    )}{" "}
                                    Tahun
                                  </strong>
                                </div>
                              </div>
                              <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                <label className="form-label">
                                  Jenis Kelamin
                                </label>
                                <div>
                                  <strong>
                                    {this.state.dataxprofil.jenis_kelamin == 1
                                      ? "Pria"
                                      : this.state.dataxprofil.jenis_kelamin ==
                                          2
                                        ? "Wanita"
                                        : "-"}
                                  </strong>
                                </div>
                              </div>
                              {/*<div className="col-md-6 col-lg-6 mt-3 mb-3">
                                <label className="form-label">
                                  Pendidikan Terakhir
                                </label>
                                <div>
                                  <strong>
                                    {this.state.dataxprofil.pendidikan}
                                  </strong>
                                </div>
                              </div>*/}
                              <div>
                                <div className="my-8 border-top mx-0"></div>
                              </div>
                              <h2 className="fs-5 text-muted mb-3">Domisili</h2>
                              {/*<div className="col-md-12 col-lg-12 mt-3 mb-3">
                                <label className="form-label">
                                  Alamat Lengkap
                                </label>
                                <div>
                                  <strong>
                                    {this.state.dataxprofil.address}
                                  </strong>
                                </div>
                              </div>*/}

                              <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                <label className="form-label">Provinsi</label>
                                <div>
                                  <strong>
                                    {this.state.dataxprofil.prop_name}
                                  </strong>
                                </div>
                              </div>

                              <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                <label className="form-label">Kota/Kab</label>
                                <div>
                                  <strong>
                                    {this.state.dataxprofil.kab_name}
                                  </strong>
                                </div>
                              </div>

                              <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                <label className="form-label">Kecamatan</label>
                                <div>
                                  <strong>
                                    {this.state.dataxprofil.kec_name}
                                  </strong>
                                </div>
                              </div>

                              <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                <label className="form-label">
                                  Desa/Kelurahan
                                </label>
                                <div>
                                  <strong>
                                    {this.state.dataxprofil.kel_name}
                                  </strong>
                                </div>
                              </div>

                              {/* <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                <label className="form-label">Kode Pos</label>
                                <div>
                                  <strong>Tampilkan disini</strong>
                                </div>
                              </div> */}
                              <div>
                                <div className="my-8 border-top mx-0"></div>
                              </div>
                              <h2 className="fs-5 text-muted mb-3">
                                Pekerjaan
                              </h2>
                              <div className="col-md-12 col-lg-12 mt-3 mb-3">
                                <label className="form-label">Status</label>
                                <div>
                                  <strong>
                                    {this.state.dataxprofil.status_pekerjaan}
                                  </strong>
                                </div>
                              </div>
                              <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                <label className="form-label">Pekerjaan</label>
                                <div>
                                  <strong>
                                    {this.state.dataxprofil.pekerjaan}
                                  </strong>
                                </div>
                              </div>
                              <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                <label className="form-label">
                                  Institusi Tempat Bekerja
                                </label>
                                <div>
                                  <strong>
                                    {this.state.dataxprofil.perusahaan}
                                  </strong>
                                </div>
                              </div>
                              {/* <div>
                                  <div className="my-8 border-top mx-0"></div>
                                </div>
                                <h2 className="fs-5 text-muted mb-3">
                                Kontak Darurat
                              </h2>
                              <div className="col-md-4 col-lg-4 mt-3 mb-3">
                                <label className="form-label">
                                  Nama Kontak Darurat
                                </label>
                                <div>
                                  <strong>
                                    {this.state.dataxprofil.nama_kontak_darurat}
                                  </strong>
                                </div>
                              </div>
                              <div className="col-md-4 col-lg-4 mt-3 mb-3">
                                <label className="form-label">
                                  Nomor Kontak Darurat
                                </label>
                                <div>
                                  <strong>
                                    +
                                    {
                                      this.state.dataxprofil
                                        .nomor_handphone_darurat
                                    }
                                  </strong>
                                </div>
                              </div>
                              <div className="col-md-4 col-lg-4 mt-3 mb-3">
                                <label className="form-label">Hubungan</label>
                                <div>
                                  <strong>
                                    {this.state.dataxprofil.hubungan}
                                  </strong>
                                </div>
                              </div>*/}
                            </div>
                          </div>
                          <div
                            className="tab-pane fade"
                            id="kt_tab_pane_2"
                            role="tabpanel"
                          >
                            <div className="row mt-7 pb-7">
                              {this.state.dataxberkas &&
                                this.state.dataxberkas.map((data) => (
                                  <>
                                    {data.element.includes("file") ? (
                                      <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                        <label className="form-label">
                                          {data.name}
                                        </label>
                                        <div>
                                          <a
                                            href="#"
                                            className="fw-bolder"
                                            onClick={(e) => {
                                              console.log("value: ", data);
                                              e.preventDefault();
                                              let val = data.value?.split(".");
                                              this.setState(
                                                {
                                                  pratinjauFile: {
                                                    mime: this.resolveMime(
                                                      val[val.length - 1],
                                                    ),
                                                    value: data.value,
                                                  },
                                                  showModalBody: false,
                                                },
                                                () => {
                                                  this.setState(
                                                    { showModalBody: true },
                                                    () => {
                                                      this.pratinjauBerkasTriggerRef.current.click();
                                                    },
                                                  );
                                                },
                                              );
                                            }}
                                          >
                                            Preview Lampiran
                                          </a>
                                        </div>
                                      </div>
                                    ) : (
                                      <div className="col-md-6 col-lg-6 mt-3 mb-3">
                                        <label className="form-label">
                                          {data.name}
                                        </label>
                                        <div>
                                          <strong>{data.value}</strong>
                                        </div>
                                      </div>
                                    )}
                                  </>
                                ))}
                              <div>
                                <div className="my-8 border-top mx-0"></div>
                              </div>
                              <h2 className="fs-5 text-muted mb-3">
                                Form Komitmen
                              </h2>
                              <div className="col-lg-12 my-7">
                                <div className="p-5 rounded bg-light-success">
                                  <p className="mb-0">
                                    Telah Menyatakan Menyetujui dengan
                                    sebenarnya secara sadar dan tanpa paksaan
                                    <br />
                                    Log :{" "}
                                    {moment(
                                      this.state.dataxdokumen.tgl_menyatakan ??
                                        "",
                                    ).isValid()
                                      ? moment(
                                          this.state.dataxdokumen
                                            .tgl_menyatakan,
                                        ).format("DD MMMM YYYY")
                                      : ""}{" "}
                                    -{" "}
                                    {moment(
                                      this.state.dataxdokumen.waktu ?? "",
                                      "HH:mm:ss",
                                    ).isValid()
                                      ? moment(
                                          this.state.dataxdokumen.waktu,
                                          "HH:mm:ss",
                                        ).format("HH:mm:ss")
                                      : ""}
                                  </p>
                                </div>
                              </div>
                              <div>
                                <p className="fw-bolder mb-1">
                                  Dengan ini Saya menyatakan bahwa :
                                </p>
                                <div
                                  dangerouslySetInnerHTML={{
                                    __html:
                                      this.state.dataxdokumen.deskripsi ?? "",
                                  }}
                                ></div>
                              </div>
                            </div>
                          </div>
                          <div
                            className="tab-pane fade"
                            id="kt_tab_pane_3"
                            role="tabpanel"
                          >
                            <div className="row pb-7">
                              <div className="card-toolbar">
                                <div className="d-flex align-items-center position-relative float-end mt-7 mb-5 me-2">
                                  <span className="svg-icon svg-icon-1 position-absolute ms-6">
                                    <svg
                                      width="24"
                                      height="24"
                                      viewBox="0 0 24 24"
                                      fill="none"
                                      xmlns="http://www.w3.org/2000/svg"
                                      className="mh-50px"
                                    >
                                      <rect
                                        opacity="0.5"
                                        x="17.0365"
                                        y="15.1223"
                                        width="8.15546"
                                        height="2"
                                        rx="1"
                                        transform="rotate(45 17.0365 15.1223)"
                                        fill="currentColor"
                                      ></rect>
                                      <path
                                        d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                        fill="currentColor"
                                      ></path>
                                    </svg>
                                  </span>
                                  <input
                                    type="text"
                                    data-kt-user-table-filter="search"
                                    className="form-control form-control-sm form-control-solid w-250px ps-14"
                                    placeholder="Cari Pelatihan"
                                    onKeyPress={this.handleKeyPress}
                                    onChange={this.handleChangeSearch}
                                  />
                                </div>
                              </div>
                              <DataTable
                                columns={this.columns}
                                data={this.state.dataxriwayatpelatihan}
                                progressPending={this.state.riwayatLoading}
                                progressComponent={this.customLoader}
                                // highlightOnHover
                                pointerOnHover
                                pagination
                                customStyles={this.customStyles}
                                persistTableHead={true}
                                noDataComponent={
                                  <div className="mt-5">Tidak Ada Data</div>
                                }
                                onChangeRowsPerPage={(
                                  currentRowsPerPage,
                                  currentPage,
                                ) => {
                                  this.handlePerRowsChange(
                                    currentRowsPerPage,
                                    currentPage,
                                    "row-change",
                                  );
                                }}
                                onChangePage={(page, totalRows) => {
                                  this.handlePerRowsChange(
                                    page,
                                    totalRows,
                                    "page-change",
                                  );
                                }}
                                onSort={this.handleChangeSort}
                                sortServer
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
