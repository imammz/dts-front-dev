import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import DataTable from "react-data-table-component";
import * as XLSX from "xlsx";
import * as FileSaver from "file-saver";
import { dateRange, capitalWord, handleFormatDate } from "../Pelatihan/helper";
import moment from "moment";
import { capitalizeFirstLetter } from "../../publikasi/helper";

export default class Rekappendaftaran extends React.Component {
  constructor(props) {
    super(props);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleChangeSearch = this.handleChangeSearchAction.bind(this);
    this.handleClickSearch = this.handleClickSearchAction.bind(this);
    this.handleClickReset = this.handleClickResetAction.bind(this);
    this.handleChangePenyelenggara =
      this.handleChangePenyelenggaraAction.bind(this);
    this.handleChangeAkademi = this.handleChangeAkademiAction.bind(this);
    this.handleChangeTema = this.handleChangeTemaAction.bind(this);
    this.handleChangeStatusSubstansi =
      this.handleChangeStatusSubstansiAction.bind(this);
    this.handleChangeStatusPelatihan =
      this.handleChangeStatusPelatihanAction.bind(this);
    this.handleChangeStatusPublish =
      this.handleChangeStatusPublishAction.bind(this);
    this.handleChangeProvinsi = this.handleChangeProvinsiAction.bind(this);
    this.handleChangeYear = this.handleChangeYearAction.bind(this);
  }
  state = {
    datax: [],
    dataxpenyelenggaravalue: "",
    dataxakademivalue: "",
    dataxakademi: "",
    dataxtemavalue: [],
    dataxtemavalue: [],
    dataxstatussubstansivalue: "",
    dataxstatuspelatihanvalue: "",
    dataxstatuspublishvalue: "",
    dataxprovinsivalue: "",
    dataxtahunvalue: "",
    review: 0,
    revisi: 0,
    disetujui: 0,
    isDisabled: true,
    loading: true,
    totalRows: 0,
    newPerPage: 10,
    numberrow: 1,
    from: "home",
    optionyear: "",
    dataxsortvalue: null,
    dataxsortvalvalue: null,
    currentyear: new Date().getFullYear(),
    url_download: "",
    issearch: false,
  };
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  optionstatus = [
    { value: "99", label: "Semua" },
    { value: "1", label: "Publish" },
    { value: "0", label: "Unpublish" },
    { value: "2", label: "Unlisted" },
  ];
  substansistatus = [
    { value: "0", label: "Semua" },
    { value: "Review", label: "Review" },
    { value: "Revisi", label: "Revisi" },
    { value: "Disetujui", label: "Disetujui" },
    { value: "Ditolak", label: "Ditolak" },
  ];
  pelatihanstatus = [
    { value: "0", label: "Semua" },
    { value: "Review Substansi", label: "Review Substansi" },
    { value: "Menunggu Pendaftaran", label: "Menunggu Pendaftaran" },
    { value: "Pendaftaran", label: "Pendaftaran" },
    { value: "Seleksi", label: "Seleksi" },
    { value: "Pelatihan", label: "Pelatihan" },
    { value: "Selesai", label: "Selesai" },
    { value: "Dibatalkan", label: "Dibatalkan" },
  ];
  generateYear() {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/pelatihanz/list-tahun",
        [],
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const optionyear = [];

        optionx.map((data) =>
          optionyear.push({ value: data.tahun, label: data.tahun }),
        );
        this.setState({ optionyear });
      });
  }
  customLoader = (
    <div style={{ padding: "24px" }}>
      <img src="/assets/media/loader/loader-biru.gif" />
    </div>
  );
  columns = [
    {
      name: "No",
      center: true,
      width: "70px",
      cell: (row, index) => (
        <div>
          <span>{this.state.numberrow + index + 0}</span>
        </div>
      ),
    },
    {
      name: "ID",
      sortable: true,
      width: "100px",
      selector: (row) => row.id_slug,
    },
    {
      name: "Nama Pelatihan",
      sortable: true,
      selector: (row) => capitalWord(row.pelatihan),
      width: "350px",
      cell: (row) => (
        <div>
          <a
            href={"/pelatihan/detail-rekappendaftaran/" + row.id_pelatihan}
            title="Detail"
            className="text-dark"
          >
            <label className="d-flex flex-stack my-2 cursor-pointer">
              <span className="d-flex align-items-center me-2">
                <span className="symbol symbol-50px me-6">
                  <span className="symbol-label bg-light-primary">
                    <span className="svg-icon svg-icon-1 svg-icon-primary">
                      {row.mitra_logo == null ? (
                        <img
                          src={`/assets/media/logos/logo-kominfo.png`}
                          alt=""
                          className="symbol-label"
                        />
                      ) : (
                        <img
                          src={
                            process.env.REACT_APP_BASE_API_URI +
                            "/download/get-file?path=" +
                            row.mitra_logo +
                            "&disk=dts-storage-partnership"
                          }
                          alt=""
                          className="symbol-label"
                        />
                      )}
                    </span>
                  </span>
                </span>
                <span className="d-flex flex-column my-2">
                  <span className="text-muted fs-7 fw-semibold">
                    {row.metode_pelatihan}
                  </span>
                  <span
                    className="fw-bolder fs-7"
                    style={{
                      overflow: "hidden",
                      whiteSpace: "wrap",
                      textOverflow: "ellipses",
                    }}
                  >
                    {`${capitalWord(row.pelatihan)} (Batch ${row.batch})`}
                  </span>
                  <h6 className="text-muted fs-7 fw-semibold mb-1">
                    {row.penyelenggara}
                  </h6>
                </span>
              </span>
            </label>
          </a>
        </div>
      ),
    },
    {
      name: "Jadwal Pendaftaran",
      sortable: true,
      width: "200px",
      selector: (row) => row.pendaftaran_start,
      cell: (row) => (
        <div>
          <span className="fs-7">
            {dateRange(
              row.pendaftaran_start,
              row.pendaftaran_end,
              "YYYY-MM-DD HH:mm:ss",
              "DD MMM YYYY",
            )}
          </span>
          <p className="fs-8 my-0 fw-semibold text-muted">
            {dateRange(
              row.pendaftaran_start,
              row.pendaftaran_end,
              "YYYY-MM-DD HH:mm:ss",
              "HH:mm:ss",
            )}
          </p>
        </div>
      ),
    },
    {
      name: "Jadwal Pelatihan",
      sortable: true,
      width: "200px",
      selector: (row) => row.pelatihan_start,
      cell: (row) => (
        <div>
          <span className="fs-7">
            {dateRange(
              row.pelatihan_start,
              row.pelatihan_end,
              "YYYY-MM-DD HH:mm:ss",
              "DD MMM YYYY",
            )}
          </span>
          <p className="fs-8 my-0 fw-semibold text-muted">
            {dateRange(
              row.pelatihan_start,
              row.pelatihan_end,
              "YYYY-MM-DD HH:mm:ss",
              "HH:mm:ss",
            )}
          </p>
        </div>
        // <div>
        //   <span className="fs-7">
        //     {dateRange(
        //       row.pelatihan_start,
        //       row.pelatihan_end,
        //       "YYYY-MM-DD HH:mm:ss",
        //       "DD MMM YYYY"
        //     )}
        //   </span>
        // </div>
      ),
    },
    {
      name: "Kuota Peserta",
      sortable: true,
      center: true,
      selector: (row) => row.kuota_peserta,
      cell: (row) => (
        <div>
          <span className="badge badge-light-primary fs-7 m-1">
            {row?.kuota_peserta}
          </span>
        </div>
      ),
    },
    {
      name: "Jml. Pendaftar",
      sortable: true,
      center: true,
      width: "200px",
      selector: (row) => row.total_daftar_peserta,
      cell: (row) => (
        <div>
          <span className="badge badge-light-success fs-7 m-1">
            {row?.total_daftar_peserta}
          </span>
        </div>
      ),
    },
    {
      name: "Status Pelatihan",
      sortable: true,
      center: true,
      width: "200px",
      selector: (row) => row.status_pelatihan,
      cell: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.status_pelatihan == "Selesai"
                ? "success"
                : row.status_pelatihan == "Dibatalkan"
                  ? "danger"
                  : row.status_pelatihan == "Pelatihan"
                    ? "warning"
                    : "primary") +
              " fs-7 m-1"
            }
          >
            {capitalWord(row.status_pelatihan)}
          </span>
        </div>
      ),
    },
    {
      name: "Aksi",
      center: true,
      width: "150px",
      cell: (row) => (
        <div className="row">
          {/* <a
            href={"/pelatihan/view-rekappendaftaran/" + row.id_pelatihan}
            id={row.id_pelatihan}
            title="View"
            className="btn btn-icon btn-active-light-primary w-30px h-30px me-3"
          >
            <span className="svg-icon svg-icon-3">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width={24}
                height={24}
                viewBox="0 0 24 24"
                fill="none"
              >
                <path
                  d="M21.7 18.9L18.6 15.8C17.9 16.9 16.9 17.9 15.8 18.6L18.9 21.7C19.3 22.1 19.9 22.1 20.3 21.7L21.7 20.3C22.1 19.9 22.1 19.3 21.7 18.9Z"
                  fill="currentColor"
                />
                <path
                  opacity="0.3"
                  d="M11 20C6 20 2 16 2 11C2 6 6 2 11 2C16 2 20 6 20 11C20 16 16 20 11 20ZM11 4C7.1 4 4 7.1 4 11C4 14.9 7.1 18 11 18C14.9 18 18 14.9 18 11C18 7.1 14.9 4 11 4ZM8 11C8 9.3 9.3 8 11 8C11.6 8 12 7.6 12 7C12 6.4 11.6 6 11 6C8.2 6 6 8.2 6 11C6 11.6 6.4 12 7 12C7.6 12 8 11.6 8 11Z"
                  fill="currentColor"
                />
              </svg>
            </span>
          </a> */}
          <a
            href={"/pelatihan/detail-rekappendaftaran/" + row.id_pelatihan}
            id={row.id_pelatihan}
            title="Detail"
            className="btn btn-icon btn-primary btn-sm"
          >
            <i className="fa fa-eye"></i>
          </a>
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
        fontWeight: "bold",
      },
    },
  };
  componentDidMount() {
    moment.locale("id");
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    const dataPenyelenggara = {
      mulai: 0,
      limit: 9999,
      cari: "",
      sort: "id desc",
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list_satker",
        dataPenyelenggara,
        this.configs,
      )
      .then((res) => {
        const options = res.data.result.Data;
        const dataxpenyelenggara = [];
        dataxpenyelenggara.push({ value: 0, label: "Semua" });
        options.map((data) =>
          dataxpenyelenggara.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxpenyelenggara });
      });
    const dataAkademik = { start: 0, length: 100, status: "publish" };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/akademi/list-akademi",
        dataAkademik,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxakademi = [];
        dataxakademi.push({ value: 0, label: "Semua" });
        optionx.map((data) =>
          dataxakademi.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxakademi });
      });
    const dataProv = { start: 1, rows: 1000 };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/provinsi",
        dataProv,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxprov = [];
        dataxprov.push({ value: 0, label: "Semua" });
        optionx.map((data) =>
          dataxprov.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxprov });
      });
    this.handleReload("home", "", 1, 10);
    this.generateYear();
    this.setState({
      url_download:
        process.env.REACT_APP_BASE_API_BE +
        "/report-pendaftaran?sta_pub=99&sta_sub=0&sta_pel=0&penyelenggara=0&akademi=0&tema=0&provinsi=0&tahun=" +
        this.state.currentyear,
    });
  }
  exportRekapTableAction() {
    const dataExport = this.state.datax;
    const excel = [];
    const userId = Cookies.get("user_id");
    const currtime = moment().format("YYYYMMDDHHmmss");
    dataExport.forEach((element, i) => {
      const keys = Object.keys(element);
      const rows = {};
      rows["ID Pelatihan" + userId + "_" + currtime] = element.id_slug;
      rows["Pelatihan" + userId + "_" + currtime] = element.pelatihan;
      rows["Penyelenggara" + userId + "_" + currtime] = element.penyelenggara;
      rows["Metode Pelatihan " + userId + "_" + currtime] =
        element.metode_pelatihan;
      rows["Status Pelatihan" + userId + "_" + currtime] =
        element.status_pelatihan;
      rows["Tanggal Mulai Pendaftaran" + userId + "_" + currtime] =
        handleFormatDate(
          element.pendaftaran_start,
          "DD MMMM YYYY HH:mm:ss",
          "YYYY-MM-DD HH:mm:ss",
        );
      rows["Tanggal Selesai Pendaftaran" + userId + "_" + currtime] =
        handleFormatDate(
          element.pendaftaran_end,
          "DD MMMM YYYY HH:mm:ss",
          "YYYY-MM-DD HH:mm:ss",
        );
      rows["Tanggal Mulai Pelatihan" + userId + "_" + currtime] =
        handleFormatDate(
          element.pelatihan_start,
          "DD MMMM YYYY HH:mm:ss",
          "YYYY-MM-DD HH:mm:ss",
        );
      rows["Tanggal Selesai Pelatihan" + userId + "_" + currtime] =
        handleFormatDate(
          element.pelatihan_end,
          "DD MMMM YYYY HH:mm:ss",
          "YYYY-MM-DD HH:mm:ss",
        );
      rows["Kuota pendaftar" + userId + "_" + currtime] = element.kuota_peserta;
      rows["Total Pendaftar" + userId + "_" + currtime] =
        element.total_daftar_peserta;
      excel.push(rows);
    });
    let tanggal = Date().toString();
    let split_tanggal = tanggal.split(" ");
    let text_tanggal =
      split_tanggal[1] +
      "_" +
      split_tanggal[2] +
      "_" +
      split_tanggal[3] +
      "_" +
      split_tanggal[4];
    this.exportToCSV(excel, "Rekap Pendaftaran " + text_tanggal);
  }

  exportToCSV(apiData, fileName) {
    const ws = XLSX.utils.json_to_sheet(apiData);
    const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
    const excelBuffer = XLSX.write(wb, {
      bookType: "xlsx",
      type: "array",
      Props: {
        Subject: "Rekap pendaftaran",
        Tags: "Daftar Pelatihan",
        Title: "Generated From Digitalent Scholarship",
        Comments: `By (${Cookies.get("user_name")} - ${Cookies.get(
          "user_id",
        )}) on ${moment().format("DD/MM/YYYY, HH:mm:ss")}`,
        Name: `From DTS By (${Cookies.get("user_id")}) on ${moment().format(
          "DD/MM/YYYY, HH:mm:ss",
        )}`,
      },
    });
    const data = new Blob([excelBuffer], { type: this.fileType });
    FileSaver.saveAs(data, fileName + ".xlsx");
  }

  handleReload(from, param, page, newPerPage) {
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - length_tmp;
    }
    let dataBody = {
      mulai: start_tmp,
      limit: length_tmp,
      id_penyelenggara:
        this.state.dataxpenyelenggaravalue.value == null
          ? 0
          : this.state.dataxpenyelenggaravalue.value,
      id_akademi:
        this.state.dataxakademivalue.value == null
          ? 0
          : this.state.dataxakademivalue.value,
      id_tema:
        this.state.dataxtemavalue.value == null
          ? 0
          : this.state.dataxtemavalue.value,
      status_substansi: 0,
      status_pelatihan:
        this.state.dataxstatuspelatihanvalue.value == null
          ? 0
          : this.state.dataxstatuspelatihanvalue.value,
      status_publish: 99,
      provinsi:
        this.state.dataxprovinsivalue.value == null
          ? 0
          : this.state.dataxprovinsivalue.value,
      tahun:
        this.state.dataxtahunvalue.value == null
          ? 0
          : this.state.dataxtahunvalue.value,
      param: param,
      sort:
        this.state.dataxsortvalue == null
          ? this.state.issearch
            ? "pelatihan"
            : "id_pelatihan"
          : this.state.dataxsortvalue,
      sort_val:
        this.state.dataxsortvalvalue == null
          ? this.state.issearch
            ? "ASC"
            : "DESC"
          : this.state.dataxsortvalvalue.toUpperCase(),
    };
    if (this.state.dataxtahunvalue.value == null) {
      dataBody.tahun = this.state.currentyear;
    } else {
      dataBody.tahun = this.state.dataxtahunvalue.value;
    }
    if ("home" == from) {
      this.setState({ from: "home" });
      this.setState({
        url_download:
          process.env.REACT_APP_BASE_API_BE +
          "/report-pendaftaran?sta_pub=" +
          dataBody.status_publish +
          "&sta_sub=" +
          dataBody.status_substansi +
          "&sta_pel=" +
          dataBody.status_pelatihan +
          "&penyelenggara=" +
          dataBody.id_penyelenggara +
          "&akademi=" +
          dataBody.id_akademi +
          "&tema=" +
          dataBody.id_tema +
          "&provinsi=" +
          dataBody.provinsi +
          "&tahun=" +
          dataBody.tahun,
      });
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/list-rekap-pendaftaran",
          dataBody,
          this.configs,
        )
        .then((res) => {
          this.setState({ numberrow: start_tmp + 1 });
          const datax = res.data.result.Data;
          this.setState({ datax: datax });
          this.setState({ loading: false });
          this.setState({ totalRows: res.data.result.TotalData });
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  this.setState({ datax: [] });
                  this.setState({ loading: false });
                }
              });
          }
        });
    } else if ("filter" == from) {
      this.setState({ from: "filter" });
      let data = {
        mulai: 0,
        limit: 100,
        id_penyelenggara:
          this.state.dataxpenyelenggaravalue.value == null
            ? 0
            : this.state.dataxpenyelenggaravalue.value,
        id_akademi:
          this.state.dataxakademivalue.value == null
            ? 0
            : this.state.dataxakademivalue.value,
        id_tema:
          this.state.dataxtemavalue.value == null
            ? 0
            : this.state.dataxtemavalue.value,
        status_pelatihan:
          this.state.dataxstatuspelatihanvalue.value == null
            ? 0
            : this.state.dataxstatuspelatihanvalue.value,
        provinsi:
          this.state.dataxprovinsivalue.value == null
            ? 0
            : this.state.dataxprovinsivalue.value,
        tahun:
          this.state.dataxtahunvalue.value == null
            ? 0
            : this.state.dataxtahunvalue.value,
      };
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI +
            "/pelatihan/rekappelatihan-filter",
          data,
          this.configs,
        )
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            this.setState({ numberrow: start_tmp + 1 });
            const datax = res.data.result.Data;
            this.setState({ datax });
            this.setState({ loading: false });
            this.setState({ totalRows: res.data.result.TotalData });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  this.setState({ loading: false });
                }
              });
          }
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  this.setState({ datax: [] });
                  this.setState({ loading: false });
                }
              });
          }
        });
    }
  }
  doSearch(searchText) {
    if (searchText == "") {
      this.handleReload("home", "", 1, 10);
    } else {
      this.handleReload("home", searchText, 1, 10);
    }
  }
  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    this.setState({ param: searchText });
    this.setState({ numberrow: 1 });
    this.setState({ issearch: true });
    if (e.key === "Enter") {
      this.setState({ loading: true }, () => {
        this.doSearch(searchText);
      });
    }
  }
  handleChangeSearchAction(e) {
    const searchText = e.currentTarget.value;
    this.setState({ issearch: false });
    this.setState({ param: searchText });
    if (searchText == "") {
      this.setState({ loading: true }, () => {
        this.handleReload("home", "", 1, 10);
      });
    }
  }
  handleClickSearchAction(e) {
    e.preventDefault();
    this.setState({ loading: true });
    this.handleReload("home", "", 1, 10);
  }
  handleClickResetAction(e) {
    e.preventDefault();
    this.setState(
      {
        dataxpenyelenggaravalue: "",
        dataxakademivalue: "",
        dataxtemavalue: [],
        dataxstatussubstansivalue: "",
        dataxstatuspelatihanvalue: "",
        dataxstatuspublishvalue: "",
        dataxprovinsivalue: "",
        dataxtahunvalue: "",
      },
      () => {
        this.handleReload("home", "", 1, 10);
      },
    );
  }
  handleChangeAkademiAction = (akademi_id) => {
    let dataxakademivalue = akademi_id;
    this.setState({ dataxakademivalue });
    const dataBody = { start: 1, rows: 100, id: akademi_id.value };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/cari_tema_byakademi",
        dataBody,
        this.configs,
      )
      .then((res) => {
        this.setState({ isDisabled: false });
        const optionx = res.data.result.Data;
        const dataxtema = [];
        dataxtema.push({ value: 0, label: "Semua" });
        optionx.map((data) =>
          dataxtema.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxtema });
        this.setState({ dataxtemavalue: [] });
      })
      .catch((error) => {
        const dataxtema = [];
        this.setState({ dataxtema });
        console.log(error);
      });
  };
  handleChangePenyelenggaraAction = (penyelenggara_id) => {
    let dataxpenyelenggaravalue = penyelenggara_id;
    this.setState({ dataxpenyelenggaravalue });
  };
  handleChangeTemaAction = (tema_id) => {
    let dataxtemavalue = tema_id;
    this.setState({ dataxtemavalue });
  };
  handleChangeStatusSubstansiAction = (status_substansi_id) => {
    let dataxstatussubstansivalue = status_substansi_id;
    this.setState({ dataxstatussubstansivalue });
  };
  handleChangeStatusPelatihanAction = (status_pelatihan_id) => {
    let dataxstatuspelatihanvalue = status_pelatihan_id;
    this.setState({ dataxstatuspelatihanvalue });
  };
  handleChangeStatusPublishAction = (status_publish_id) => {
    let dataxstatuspublishvalue = status_publish_id;
    this.setState({ dataxstatuspublishvalue });
  };
  handleChangeProvinsiAction = (provinsi_id) => {
    let dataxprovinsivalue = provinsi_id;
    this.setState({ dataxprovinsivalue });
  };
  handleChangeYearAction = (tahun_id) => {
    let dataxtahunvalue = tahun_id;
    this.setState({ dataxtahunvalue });
  };
  handleChangeSort = (column, sortDirection) => {
    this.setState({ numberrow: 1 });
    let sortByColumn = "";
    if (column.id == 2) {
      sortByColumn = "id_slug";
    } else if (column.id == 3) {
      sortByColumn = "pelatihan";
    } else if (column.id == 4) {
      sortByColumn = "pendaftaran_start";
    } else if (column.id == 5) {
      sortByColumn = "pelatihan_start";
    } else if (column.id == 6) {
      sortByColumn = "kuota_peserta";
    } else if (column.id == 7) {
      sortByColumn = "total_daftar_peserta";
    } else if (column.id == 8) {
      sortByColumn = "status_pelatihan";
    }
    this.setState({ dataxsortvalue: sortByColumn });
    this.setState({ dataxsortvalvalue: sortDirection }, () => {
      this.handleReload(
        this.state.from,
        this.state.param,
        1,
        this.state.newPerPage,
      );
    });
  };
  handlePageChange = (page) => {
    this.setState({ loading: true });
    this.handleReload(
      this.state.from,
      this.state.param,
      page,
      this.state.newPerPage,
    );
  };
  handlePerRowsChange = async (newPerPage, page) => {
    this.setState({ numberrow: 1 });
    this.setState({ loading: true });
    this.setState({ newPerPage: newPerPage });
    this.handleReload(this.state.from, this.state.param, page, newPerPage);
  };
  render() {
    let rowCounter = 1;
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Rekap Pendaftaran
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                  data-bs-toggle="modal"
                  data-bs-target="#filter"
                >
                  <i className="bi bi-sliders"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Filter
                  </span>
                </button>
                <a
                  href="#"
                  onClick={(e) => {
                    e.preventDefault();
                    this.exportRekapTableAction();
                  }}
                  className="btn btn-success btn-sm me-2"
                >
                  <i className="bi bi-cloud-download"></i>
                  Export
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Daftar Rekap Pendaftaran
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <div className="d-flex align-items-center position-relative my-1 me-2">
                            <span className="svg-icon svg-icon-1 position-absolute ms-6">
                              <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className="mh-50px"
                              >
                                <rect
                                  opacity="0.5"
                                  x="17.0365"
                                  y="15.1223"
                                  width="8.15546"
                                  height="2"
                                  rx="1"
                                  transform="rotate(45 17.0365 15.1223)"
                                  fill="currentColor"
                                ></rect>
                                <path
                                  d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                  fill="currentColor"
                                ></path>
                              </svg>
                            </span>
                            <input
                              type="text"
                              data-kt-user-table-filter="search"
                              className="form-control form-control-sm form-control-solid w-250px ps-14"
                              placeholder="Cari Pelatihan"
                              onKeyPress={this.handleKeyPress}
                              onChange={this.handleChangeSearch}
                            />
                          </div>
                          <div className="modal fade" tabIndex="-1" id="filter">
                            <div className="modal-dialog modal-lg">
                              <div className="modal-content">
                                <div className="modal-header">
                                  <h5 className="modal-title">
                                    <span className="svg-icon svg-icon-5 me-1">
                                      <i className="bi bi-sliders text-black"></i>
                                    </span>
                                    Filter Rekap Pendaftaran
                                  </h5>
                                  <div
                                    className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                    data-bs-dismiss="modal"
                                    aria-label="Close"
                                  >
                                    <span className="svg-icon svg-icon-2x">
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                      >
                                        <rect
                                          opacity="0.5"
                                          x="6"
                                          y="17.3137"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(-45 6 17.3137)"
                                          fill="currentColor"
                                        />
                                        <rect
                                          x="7.41422"
                                          y="6"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(45 7.41422 6)"
                                          fill="currentColor"
                                        />
                                      </svg>
                                    </span>
                                  </div>
                                </div>
                                <form
                                  action="#"
                                  onSubmit={this.handleClickSearch}
                                >
                                  <div className="modal-body">
                                    <div className="row">
                                      {/* <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label required">Nama Pelatihan</label>
                                        <input className="form-control form-control-sm" placeholder="Masukkan nama pelatihan" name="name_search"/>
                                      </div> */}
                                      <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label">
                                          Penyelenggara
                                        </label>
                                        <Select
                                          name="penyelenggara"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.state.dataxpenyelenggara
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm form-select-solid selectpicker p-0"
                                          options={
                                            this.state.dataxpenyelenggara
                                          }
                                          onChange={
                                            this.handleChangePenyelenggara
                                          }
                                          value={
                                            this.state.dataxpenyelenggaravalue
                                          }
                                        />
                                      </div>
                                      <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label">
                                          Akademi
                                        </label>
                                        <Select
                                          name="akademi_id"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.state.dataxakademi
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm form-select-solid selectpicker p-0"
                                          options={this.state.dataxakademi}
                                          onChange={this.handleChangeAkademi}
                                          value={this.state.dataxakademivalue}
                                        />
                                        {/* <select className="form-select form-select-sm selectpicker" data-control="select2" data-placeholder="Select an option">
                                          <option>Test</option>
                                        </select> */}
                                      </div>
                                      <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label">
                                          Tema
                                        </label>
                                        <Select
                                          name="tema_id"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.state.dataxtema
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm form-select-solid selectpicker p-0"
                                          options={this.state.dataxtema}
                                          isDisabled={this.state.isDisabled}
                                          onChange={this.handleChangeTema}
                                          value={this.state.dataxtemavalue}
                                        />
                                      </div>
                                      {/* <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label">Status Substansi</label>
                                        <Select
                                          name="status_substansi"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({inputValue}) => !inputValue ? this.substansistatus : "Data tidak tersedia"}
                                          className="form-select-sm form-select-solid selectpicker p-0"
                                          options={this.substansistatus}
                                        />
                                      </div> */}
                                      <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label">
                                          Status Pelatihan
                                        </label>
                                        <Select
                                          name="status_pelatihan"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.pelatihanstatus
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm form-select-solid selectpicker p-0"
                                          options={this.pelatihanstatus}
                                          onChange={
                                            this.handleChangeStatusPelatihan
                                          }
                                          value={
                                            this.state.dataxstatuspelatihanvalue
                                          }
                                        />
                                      </div>
                                      {/* <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label">Status Publish</label>
                                        <Select
                                          name="status_publish"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({inputValue}) => !inputValue ? this.optionstatus : "Data tidak tersedia"}
                                          className="form-select-sm form-select-solid selectpicker p-0"
                                          options={this.optionstatus}
                                        />
                                      </div> */}
                                      <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label">
                                          Provinsi
                                        </label>
                                        <Select
                                          name="provinsi"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.state.dataxprov
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm form-select-solid selectpicker p-0"
                                          options={this.state.dataxprov}
                                          onChange={this.handleChangeProvinsi}
                                          value={this.state.dataxprovinsivalue}
                                        />
                                      </div>
                                      <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label">
                                          Tahun
                                        </label>
                                        <Select
                                          name="year"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.state.optionyear
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm form-select-solid selectpicker p-0"
                                          options={this.state.optionyear}
                                          onChange={this.handleChangeYear}
                                          value={this.state.dataxtahunvalue}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                  <div className="modal-footer">
                                    <div className="d-flex justify-content-between">
                                      <button
                                        type="reset"
                                        className="btn btn-sm btn-light me-3"
                                        onClick={this.handleClickReset}
                                      >
                                        Reset
                                      </button>
                                      <button
                                        type="submit"
                                        className="btn btn-sm btn-primary"
                                        data-bs-dismiss="modal"
                                      >
                                        Apply Filter
                                      </button>
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <DataTable
                          columns={this.columns}
                          data={this.state.datax}
                          progressPending={this.state.loading}
                          progressComponent={this.customLoader}
                          highlightOnHover
                          pointerOnHover
                          pagination
                          paginationServer={true}
                          paginationTotalRows={this.state.totalRows}
                          paginationComponentOptions={{
                            selectAllRowsItem: true,
                            selectAllRowsItemText: "Semua",
                          }}
                          onChangeRowsPerPage={this.handlePerRowsChange}
                          onChangePage={this.handlePageChange}
                          customStyles={this.customStyles}
                          persistTableHead={true}
                          noDataComponent={
                            <div className="mt-5">Tidak Ada Data</div>
                          }
                          onSort={this.handleChangeSort}
                          sortServer
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
