import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import moment from "moment";
import { formatIndonesianDateRange } from "../FormHelper";

export default class RekappendaftaranviewContent extends React.Component {
  constructor(props) {
    super(props);
    Cookies.remove("pelatian_id");
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
  }
  state = {
    dataxakademi: [],
    dataxtema: [],
    dataxprov: [],
    dataxkab: [],
    dataxpenyelenggara: [],
    dataxmitra: [],
    dataxzonasi: [],
    dataxselform: [],
    dataxpelatihan: [],
    dataxlevelpelatihan: [],
    sellvlpelatihan: [],
    selakademi: [],
    seltema: [],
    selpenyelenggara: [],
    selmitra: [],
    selzonasi: [],
    selprovinsi: [],
    selkabupaten: [],
    seljudulform: [],
    valuecatatan: "",
    disabilities: [],
    dataxpreview: [],
    dataxparameter: [],
  };
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  componentDidMount() {
    moment.locale("id");
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    let segment_url = window.location.pathname.split("/");
    let urlSegmenttOne = segment_url[3];
    Cookies.set("pelatian_id", urlSegmenttOne);
    let data = {
      id: urlSegmenttOne,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/pelatihan/cari-rekappelatihan-id",
        data,
        this.configs,
      )
      .then((res) => {
        const dataxpelatihan = res.data.result.data_pelatihan[0];
        this.setState({ dataxpelatihan });

        const dataxpreview = res.data.result.form_pendaftaran;
        this.setState({ dataxpreview });

        const dataxparameter = res.data.result.parameter[0];
        this.setState({ dataxparameter });

        let disabilities = [];
        if (this.state.dataxpelatihan.umum == "1") {
          disabilities.push("Umum");
        }
        if (this.state.dataxpelatihan.tuna_netra == "1") {
          disabilities.push("Tuna Netra");
        }
        if (this.state.dataxpelatihan.tuna_rungu == "1") {
          disabilities.push("Tuna Rungu");
        }
        if (this.state.dataxpelatihan.tuna_daksa == "1") {
          disabilities.push("Tuna Daksa");
        }
        this.setState({ disabilities });

        //level pelatihan
        const dataLvl = {};
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/levelpelatihan",
            dataLvl,
            this.configs,
          )
          .then((res) => {
            const optionx = res.data.result.Data;
            const dataxlevelpelatihan = [];
            for (let i in optionx) {
              if (optionx[i].id == this.state.dataxpelatihan.level_pelatihan) {
                this.setState({
                  sellvlpelatihan: {
                    value: optionx[i].id,
                    label: optionx[i].name,
                  },
                });
              }
            }
            optionx.map((data) =>
              dataxlevelpelatihan.push({ value: data.id, label: data.name }),
            );
            this.setState({ dataxlevelpelatihan });
          });

        //akademi
        const dataAkademik = { start: 0, length: 100 };
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/akademi/list-akademi",
            dataAkademik,
            this.configs,
          )
          .then((res) => {
            const optionx = res.data.result.Data;
            const dataxakademi = [];
            for (let i in optionx) {
              if (optionx[i].id == this.state.dataxpelatihan.akademi_id) {
                this.setState({
                  selakademi: { value: optionx[i].id, label: optionx[i].name },
                });
              }
            }
            optionx.map((data) =>
              dataxakademi.push({ value: data.id, label: data.name }),
            );
            this.setState({ dataxakademi });

            //tema
            const dataBody = {
              start: 1,
              rows: 100,
              id: this.state.dataxpelatihan.akademi_id,
            };
            axios
              .post(
                process.env.REACT_APP_BASE_API_URI + "/cari_tema_byakademi",
                dataBody,
                this.configs,
              )
              .then((res) => {
                const optionx = res.data.result.Data;
                const dataxtema = [];
                for (let i in optionx) {
                  if (optionx[i].id == this.state.dataxpelatihan.tema_id) {
                    this.setState({
                      seltema: { value: optionx[i].id, label: optionx[i].name },
                    });
                  }
                }
                optionx.map((data) =>
                  dataxtema.push({ value: data.id, label: data.name }),
                );
                this.setState({ dataxtema });
              })
              .catch((error) => {
                const dataxtema = [];
                this.setState({ dataxtema });
                let messagex = error.response.data.result.Message;
                console.log(error);
                // swal
                //   .fire({
                //     title: messagex,
                //     icon: "warning",
                //     confirmButtonText: "Ok",
                //   })
                //   .then((result) => {
                //     if (result.isConfirmed) {
                //     }
                //   });
              });
          });

        //penyelenggara
        const dataPenyelenggara = {
          mulai: 0,
          limit: 9999,
          cari: "",
          sort: "id desc",
        };
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/list_satker",
            dataPenyelenggara,
            this.configs,
          )
          .then((res) => {
            const optionx = res.data.result.Data;
            const dataxpenyelenggara = [];
            for (let i in optionx) {
              if (optionx[i].id == this.state.dataxpelatihan.penyelenggara) {
                this.setState({
                  selpenyelenggara: {
                    value: optionx[i].id,
                    label: optionx[i].name,
                  },
                });
              }
            }
            optionx.map((data) =>
              dataxpenyelenggara.push({ value: data.id, label: data.name }),
            );
            this.setState({ dataxpenyelenggara });
          });

        //mitra
        const dataMitra = { start: 0, length: 100 };
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/umum/list-mitra",
            dataMitra,
            this.configs,
          )
          .then((res) => {
            const optionx = res.data.result.Data;
            const dataxmitra = [];
            for (let i in optionx) {
              if (optionx[i].id == this.state.dataxpelatihan.mitra_id) {
                this.setState({
                  selmitra: { value: optionx[i].id, label: optionx[i].name },
                });
              }
            }
            optionx.map((data) =>
              dataxmitra.push({ value: data.id, label: data.name }),
            );
            this.setState({ dataxmitra });
          });

        //zonasi
        const dataZonasi = { start: 1, length: 100 };
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/zonasi",
            dataZonasi,
            this.configs,
          )
          .then((res) => {
            const optionx = res.data.result.Data;
            const dataxzonasi = [];
            for (let i in optionx) {
              if (optionx[i].id == this.state.dataxpelatihan.zonasi) {
                this.setState({
                  selzonasi: { value: optionx[i].id, label: optionx[i].name },
                });
              }
            }
            optionx.map((data) =>
              dataxzonasi.push({ value: data.id, label: data.name }),
            );
            this.setState({ dataxzonasi });
          });

        //provinsi
        const dataProv = { start: 1, rows: 1000 };
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/provinsi",
            dataProv,
            this.configs,
          )
          .then((res) => {
            const optionx = res.data.result.Data;
            const dataxprov = [];
            for (let i in optionx) {
              if (optionx[i].id == this.state.dataxpelatihan.provinsi) {
                this.setState({
                  selprovinsi: { value: optionx[i].id, label: optionx[i].name },
                });
              }
            }
            optionx.map((data) =>
              dataxprov.push({ value: data.id, label: data.name }),
            );
            this.setState({ dataxprov });

            //kabupaten
            const dataKab = { id: this.state.dataxpelatihan.kabupaten };
            axios
              .post(
                process.env.REACT_APP_BASE_API_URI + "/kabupaten",
                dataKab,
                this.configs,
              )
              .then((res) => {
                const optionx = res.data.result.Data;
                const dataxkab = [];
                for (let i in optionx) {
                  if (optionx[i].id == this.state.dataxpelatihan.kabupaten) {
                    this.setState({
                      selkabupaten: {
                        value: optionx[i].id,
                        label: optionx[i].name,
                      },
                    });
                  }
                }
                optionx.map((data) =>
                  dataxkab.push({ value: data.id, label: data.name }),
                );
                this.setState({ dataxkab });
              });
          });

        //judul form
        const dataSelForm = { start: 0, length: 1000 };
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/daftarpeserta/list-formbuilder",
            dataSelForm,
            this.configs,
          )
          .then((res) => {
            const optionx = res.data.result.Data;
            const dataxselform = [];
            for (let i in optionx) {
              if (
                optionx[i].id ==
                this.state.dataxpelatihan.master_form_builder_id
              ) {
                this.setState({
                  seljudulform: {
                    value: optionx[i].id,
                    label: optionx[i].judul_form,
                  },
                });
              }
            }
            optionx.map((data) =>
              dataxselform.push({ value: data.id, label: data.judul_form }),
            );
            this.setState({ dataxselform });
          });

        const dataFormSubmit = new FormData();
        dataFormSubmit.append(
          "id",
          this.state.dataxpelatihan.master_form_builder_id,
        );
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/cari-formbuilder",
            dataFormSubmit,
            this.configs,
          )
          .then((res) => {
            const statux = res.data.result.Status;
            const messagex = res.data.result.Message;
            if (statux) {
              const dataxpreview = res.data.result.detail;
              this.setState({ dataxpreview });
            } else {
              swal
                .fire({
                  title: messagex,
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                  }
                });
            }
          });
      });
  }
  handleClickBatalAction(e) {
    window.location = "/pelatihan/rekappendaftaran";
  }
  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack"
          >
            <div
              data-kt-swapper="true"
              data-kt-swapper-mode="prepend"
              data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
              className="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0"
            >
              <h1 className="d-flex align-items-center text-dark fw-bolder fs-3 my-1">
                Pelatihan
              </h1>
              <span className="h-20px border-gray-200 border-start mx-4" />
              <ul className="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                <li className="breadcrumb-item text-muted">Pelatihan</li>
                <li className="breadcrumb-item">
                  <span className="bullet bg-gray-200 w-5px h-2px" />
                </li>
                <li className="breadcrumb-item text-muted">
                  Lihat Rekap Pendaftaran
                </li>
              </ul>
            </div>
            <div className="d-flex align-items-center py-1"></div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-7"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="card">
                      <div
                        className="stepper stepper-links d-flex flex-column"
                        id="kt_rekap_pendaftaran"
                      >
                        <div className="stepper-nav mb-5">
                          <div
                            className="stepper-item current"
                            data-kt-stepper-element="nav"
                            data-kt-stepper-action="step"
                          >
                            <h3 className="stepper-title">1. Data Pelatihan</h3>
                          </div>
                          <div
                            className="stepper-item"
                            data-kt-stepper-element="nav"
                            data-kt-stepper-action="step"
                          >
                            <h3 className="stepper-title">
                              2. Form Pendaftaran
                            </h3>
                          </div>
                          <div
                            className="stepper-item"
                            data-kt-stepper-element="nav"
                            data-kt-stepper-action="step"
                          >
                            <h3 className="stepper-title">3. Form Komitmen</h3>
                          </div>
                          <div
                            className="stepper-item"
                            data-kt-stepper-element="nav"
                            data-kt-stepper-action="step"
                          >
                            <h3 className="stepper-title">4. Parameter</h3>
                          </div>
                        </div>
                        <div className="modal-body scroll-y">
                          <form
                            action="#"
                            onSubmit={this.handleSubmit}
                            id="kt_rekap_pendaftaran_form"
                          >
                            <div
                              className="current"
                              data-kt-stepper-element="content"
                            >
                              <div
                                id="kt_content_container"
                                className="container-xxl"
                              >
                                <div className="card-body">
                                  <div className="card-title">
                                    <div className="card mb-12 mb-xl-8">
                                      <h2 className="me-3 mr-2">
                                        Data Pelatihan{" "}
                                        {this.state.dataxpelatihan.name}
                                      </h2>
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Program Digital Talent Scholarship
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {
                                            this.state.dataxpelatihan
                                              .program_dts
                                          }
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Nama Pelatihan
                                      </label>
                                      <div className="d-flex">
                                        <b>{this.state.dataxpelatihan.name}</b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Level Pelatihan
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {
                                            this.state.dataxpelatihan
                                              .level_pelatihan
                                          }
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Akademi
                                      </label>
                                      <div className="d-flex">
                                        <b>{this.state.selakademi.label}</b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">Tema</label>
                                      <div className="d-flex">
                                        <b>{this.state.seltema.label}</b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Metode Pelaksanaan
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {
                                            this.state.dataxpelatihan
                                              .metode_pelaksanaan
                                          }
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Penyelenggara
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {
                                            this.state.dataxpelatihan
                                              .penyelenggara
                                          }
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Mitra
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {this.state.dataxpelatihan.mitra_name}
                                        </b>
                                      </div>
                                    </div>
                                    <h4 className="my-5 d-flex align-items-center">
                                      Tanggal Pendaftaran dan Pelatihan
                                    </h4>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Tanggal Mulai Pendaftaran
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {moment(
                                            this.state.dataxpelatihan
                                              .pendaftaran_start,
                                            "DD-MM-YYYY HH:mm:ss",
                                          ).isValid()
                                            ? moment(
                                                this.state.dataxpelatihan
                                                  .pendaftaran_start,
                                                "DD-MM-YYYY HH:mm:ss",
                                              ).format("DD MMMM YYYY HH:mm:ss")
                                            : this.state.dataxpelatihan
                                                .pendaftaran_start}
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Tanggal Selesai Pendaftaran
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {moment(
                                            this.state.dataxpelatihan
                                              .pendaftaran_end,
                                            "DD-MM-YYYY HH:mm:ss",
                                          ).isValid()
                                            ? moment(
                                                this.state.dataxpelatihan
                                                  .pendaftaran_end,
                                                "DD-MM-YYYY HH:mm:ss",
                                              ).format("DD MMMM YYYY HH:mm:ss")
                                            : this.state.dataxpelatihan
                                                .pendaftaran_end}
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Tanggal Mulai Pelatihan
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {moment(
                                            this.state.dataxpelatihan
                                              .pelatihan_start,
                                            "DD-MM-YYYY HH:mm:ss",
                                          ).isValid()
                                            ? moment(
                                                this.state.dataxpelatihan
                                                  .pelatihan_start,
                                                "DD-MM-YYYY HH:mm:ss",
                                              ).format("DD MMMM YYYY HH:mm:ss")
                                            : this.state.dataxpelatihan
                                                .pelatihan_start}
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Tanggal Selesai Pelatihan
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {moment(
                                            this.state.dataxpelatihan
                                              .pelatihan_end,
                                            "DD-MM-YYYY HH:mm:ss",
                                          ).isValid()
                                            ? moment(
                                                this.state.dataxpelatihan
                                                  .pelatihan_end,
                                                "DD-MM-YYYY HH:mm:ss",
                                              ).format("DD MMMM YYYY HH:mm:ss")
                                            : this.state.dataxpelatihan
                                                .pelatihan_end}
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label">
                                        Deskripsi Pelatihan
                                      </label>
                                      <div className="d-flex">
                                        <b
                                          dangerouslySetInnerHTML={{
                                            __html:
                                              this.state.dataxpelatihan
                                                .deskripsi,
                                          }}
                                        ></b>
                                      </div>
                                    </div>
                                    <h4 className="my-5 d-flex align-items-center">
                                      Kuota dan Info Pelatihan
                                    </h4>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Target Kuota Pendaftar
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {
                                            this.state.dataxpelatihan
                                              .kuota_pendaftar
                                          }
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Target Kuota Peserta
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {
                                            this.state.dataxpelatihan
                                              .kuota_peserta
                                          }
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Komitmen
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {this.state.dataxpelatihan.komitmen}
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Status Kuota
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {
                                            this.state.dataxpelatihan
                                              .status_kuota
                                          }
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Sertifikasi
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {
                                            this.state.dataxpelatihan
                                              .sertifikasi
                                          }
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        LPJ Peserta
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {
                                            this.state.dataxpelatihan
                                              .lpj_peserta
                                          }
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Zonasi
                                      </label>
                                      <div className="d-flex">
                                        <b>{this.state.selzonasi.label}</b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Batch
                                      </label>
                                      <div className="d-flex">
                                        <b>{this.state.dataxpelatihan.batch}</b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Metode Pelatihan
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {
                                            this.state.dataxpelatihan
                                              .metode_pelatihan
                                          }
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Status Publish
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {this.state.dataxpelatihan
                                            .status_publish == 1
                                            ? "Publish"
                                            : this.state.dataxpelatihan
                                                  .status_publish == 0
                                              ? "Unpublish"
                                              : "Unknown"}
                                        </b>
                                      </div>
                                    </div>
                                    <h4 className="my-5 d-flex align-items-center">
                                      Lokasi Pelatihan
                                    </h4>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Provinsi
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {this.state.dataxpelatihan
                                            .metode_pelatihan == "Online"
                                            ? this.state.dataxpelatihan.provinsi
                                            : this.state.selprovinsi.label}
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Kota / Kabupaten
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {this.state.dataxpelatihan
                                            .metode_pelatihan == "Online"
                                            ? this.state.dataxpelatihan
                                                .kabupaten
                                            : this.state.selkabupaten.label}
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Alamat
                                      </label>
                                      <div className="d-flex">
                                        <b
                                          dangerouslySetInnerHTML={{
                                            __html:
                                              this.state.dataxpelatihan.alamat,
                                          }}
                                        ></b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Disabilitas
                                      </label>
                                      <div className="d-flex">
                                        <b>{this.state.disabilities.join()}</b>
                                      </div>
                                    </div>
                                    <h4 className="my-5 d-flex align-items-center">
                                      Alur Pendaftaran
                                    </h4>
                                    <div className="d-flex">
                                      <b>
                                        {
                                          this.state.dataxpelatihan
                                            .alur_pendaftaran
                                        }
                                      </b>
                                    </div>
                                    <h4 className="my-5 d-flex align-items-center">
                                      Berkas
                                    </h4>
                                    <div className="col-lg-12">
                                      <div className="mb-7 fv-row">
                                        <label className="form-label">
                                          File Upload Silabus
                                        </label>
                                        <div className="d-flex">
                                          <b>
                                            <a
                                              target="_blank"
                                              href={
                                                this.state.dataxpelatihan
                                                  .silabus
                                              }
                                            >
                                              {
                                                this.state.dataxpelatihan
                                                  .silabus
                                              }
                                            </a>
                                          </b>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div data-kt-stepper-element="content">
                              <div
                                id="kt_content_container"
                                className="container-xxl"
                              >
                                <div className="card-body">
                                  <div className="card-title">
                                    <div className="card mb-12 mb-xl-8">
                                      <h2 className="me-3 mr-2">
                                        Form Pendaftaran
                                      </h2>
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label">
                                        Judul Form
                                      </label>
                                      <div className="d-flex">
                                        <b>{this.state.seljudulform.label}</b>
                                      </div>
                                    </div>
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label">
                                        Isi Form
                                      </label>
                                      <div className="mr-3">
                                        {this.state.dataxpreview.map((data) => (
                                          <div
                                            dangerouslySetInnerHTML={{
                                              __html: data.html,
                                            }}
                                          ></div>
                                        ))}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div data-kt-stepper-element="content">
                              <div
                                id="kt_content_container"
                                className="container-xxl"
                              >
                                <div className="card-body">
                                  <div className="card-title">
                                    <div className="card mb-12 mb-xl-8">
                                      <h2 className="me-3 mr-2">
                                        Form Komitmen
                                      </h2>
                                    </div>
                                  </div>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label">
                                      Komitmen Peserta
                                    </label>
                                    <div className="d-flex">
                                      <b>
                                        {this.state.dataxpelatihan.komitmen}
                                      </b>
                                    </div>
                                  </div>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label">
                                      Deskripsi
                                    </label>
                                    <p className="text-dark">
                                      <div>
                                        <p>
                                          <strong>
                                            Dengan ini Saya menyatakan bahwa
                                          </strong>
                                          :
                                        </p>
                                        <ol>
                                          <li>
                                            Bersedia mengikuti seluruh tahapan
                                            pelatihan sejak awal hingga selesai;
                                          </li>
                                          <li>
                                            Bersedia menjadi calon Penerima
                                            Bantuan Pemerintah Digital Talent
                                            Scholarship Tahun 2022;
                                          </li>
                                          <li>
                                            Bersedia memenuhi persyaratan
                                            administratif serta Syarat dan
                                            Ketentuan yang berlaku;
                                          </li>
                                          <li>
                                            Bersedia memenuhi Kewajiban dan Tata
                                            Tertib sebagai peserta pelatihan;
                                          </li>
                                          <li>
                                            Bersedia menerima dan tidak akan
                                            mengganggu-gugat segala keputusan
                                            final Panitia Digital Talent
                                            Scholarship Tahun 2022;
                                          </li>
                                          <li>
                                            Bersedia memberikan informasi
                                            pribadi yang tercantum dalam form
                                            pendaftaran kepada Panitia Digital
                                            Talent Scholarship 2022 untuk
                                            kepentingan pelaksanaan pelatihan
                                            dan pasca pelatihan;
                                          </li>
                                          <li>
                                            Menaati segala ketentuan dan tata
                                            tertib yang diterapkan di lingkungan
                                            mitra penyelenggara;&nbsp;
                                          </li>
                                          <li>
                                            Mengerti dan setuju bahwa konten
                                            pelatihan digunakan hanya untuk
                                            kebutuhan Digital Talent Scholarship
                                            Kementerian Komunikasi dan
                                            Informatika. Segala konten pelatihan
                                            termasuk tidak terbatas pada soal
                                            tes substansi, soal kuis, soal mid
                                            exam, soal final exam, materi
                                            pelatihan, video, gambar dan kode
                                            ini mengandung Kekayaan Intelektual,
                                            peserta tunduk kepada undang-undang
                                            hak cipta, merek dagang atau hak
                                            kekayaan intelektual lainnya.
                                            Peserta dilarang untuk mereproduksi,
                                            memodifikasi, menyebarluaskan, atau
                                            mengeksploitasi konten ini dengan
                                            cara atau bentuk apapun tanpa
                                            persetujuan tertulis dari Panitia
                                            Digital Talent Scholarship
                                            Kementerian Komunikasi dan
                                            Informatika Republik Indonesia.
                                            Peserta yang terbukti melakukan
                                            pelanggaran ini akan dicabut Hak
                                            sebagai penerima beasiswa dan akan
                                            menerima konsekuensi sesuai aturan
                                            yang berlaku;
                                          </li>
                                          <li>
                                            Tidak terlibat dalam penyalahgunaan
                                            narkotika, obat-obatan terlarang,
                                            kriminal, dan paham radikal dan
                                            terorisme.
                                          </li>
                                        </ol>
                                        <p>
                                          Demikian Surat Pernyataan ini dibuat
                                          dengan sebenarnya secara sadar dan
                                          tanpa paksaan. Apabila&nbsp;
                                          dikemudian hari pernyataan ini
                                          terbukti tidak benar, maka saya
                                          bersedia untuk dicabut haknya sebagai
                                          peserta pelatihan dan menerima sanksi
                                          sesuai ketentuan Kementerian Kominfo.
                                        </p>
                                      </div>
                                    </p>
                                    <h6 className="font-weight-bolder pb-5 pt-4">
                                      Telah Menyatakan Menyetujui dengan
                                      sebenarnya secara sadar dan tanpa paksaan
                                    </h6>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div data-kt-stepper-element="content">
                              <div
                                id="kt_content_container"
                                className="container-xxl"
                              >
                                <div className="card-body">
                                  <div className="row">
                                    <div className="card-title">
                                      <div className="card mb-12 mb-xl-8">
                                        <h2 className="me-3 mr-2">Parameter</h2>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Test Substansi
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {
                                            this.state.dataxparameter
                                              .test_substansi
                                          }
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Tanggal
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {
                                            this.state.dataxparameter
                                              .tgl_test_substansi
                                          }
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Mid Test
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {this.state.dataxparameter.mid_test}
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Tanggal
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {moment(
                                            this.state.dataxparameter
                                              .tgl_mid_test,
                                          ).isValid()
                                            ? moment(
                                                this.state.dataxparameter
                                                  .tgl_mid_test,
                                              ).format("DD MMMM YYYY")
                                            : this.state.dataxparameter
                                                .tgl_mid_test}
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Survey
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {this.state.dataxparameter.survery}
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Tanggal
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {formatIndonesianDateRange(
                                            this.state.dataxparameter
                                              .tgl_survey,
                                          )}
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Sertifikat
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {this.state.dataxparameter.sertifikat}
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Tanggal
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {moment(
                                            this.state.dataxparameter
                                              .tgl_sertifikat,
                                          ).isValid()
                                            ? moment(
                                                this.state.dataxparameter
                                                  .tgl_sertifikat,
                                              ).format("DD MMMM YYYY")
                                            : this.state.dataxparameter
                                                .tgl_sertifikat}
                                        </b>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="text-center mb-5">
                              <button
                                onClick={this.handleClickBatal}
                                type="reset"
                                className="btn btn-danger btn-sm me-3 mr-2"
                              >
                                Kembali
                              </button>
                              <button
                                className="btn btn-primary btn-sm me-3 mr-2"
                                data-kt-stepper-action="previous"
                              >
                                Sebelumnya
                              </button>
                              <button
                                type="button"
                                className="btn btn-warning btn-sm"
                                data-kt-stepper-action="next"
                              >
                                Selanjutnya
                              </button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
