import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import * as FileSaver from "file-saver";
import * as XLSX from "xlsx";
import DataTable from "react-data-table-component";
import template from "./template/import_csv.csv";

export default class RekappendaftaranimportContent extends React.Component {
  constructor(props) {
    super(props);
    this.filePesertaRef = React.createRef(null);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleGenerateExcel = this.handleGenerateExcelAction.bind(this);
    this.handleSubmit = this.handleSubmitAction.bind(this);
    this.handleFileUpload = this.handleFileUploadAction.bind(this);
  }
  state = {
    errors: {},
    baseFile: null,
    datax: [],
    jadwalx: [],
    dataxakademi: [],
    dataxtema: [],
    dataxprov: [],
    dataxkab: [],
    dataxpenyelenggara: [],
    dataxmitra: [],
    dataxzonasi: [],
    dataxselform: [],
    dataxpelatihan: [],
    dataxlevelpelatihan: [],
    sellvlpelatihan: [],
    selakademi: [],
    seltema: [],
    selpenyelenggara: [],
    selmitra: [],
    selzonasi: [],
    selprovinsi: [],
    selkabupaten: [],
    seljudulform: [],
    valuekomitmen: "",
    valuecatatan: "",
    valuecatatan: "",
    review: 0,
    revisi: 0,
    disetujui: 0,
    berjalan: 0,
    selesai: 0,
    titleheader: "",
    numberrow: 1,
    valuetestsubstansi: 0,
    valuestatusberkas: 0,
    valuestatuspeserta: 0,
    valuesparam: "",
    statistikpendaftar: 0,
    statistikverified: 0,
    statistiklulustessubstansi: 0,
    statistikverifiedlulustestsubstansi: 0,
    statistikditerima: 0,
    // yang di import
    pelatihan: null,
    excelHeader: null,
    selectedFile: null,
    loading: false,
    importData: null,
    headerColumn: null,
  };

  capitalWord(str) {
    return str.replace(/\w\S*/g, function (kata) {
      const kataBaru = kata.slice(0, 1).toUpperCase() + kata.substr(1);
      return kataBaru;
    });
  }
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  customLoader = (
    <div style={{ padding: "24px" }}>
      <img src="/assets/media/loader/loader-biru.gif" />
    </div>
  );

  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
        //fontWeight: 'bold',
      },
    },
  };
  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    let segment_url = window.location.pathname.split("/");
    this.readBaseFile();
    if (
      window.location &&
      window.location?.search?.includes("nama_pelatihan")
    ) {
      let temp = new URLSearchParams(window.location.search).get(
        "nama_pelatihan",
      );
      this.setState({ pelatihan: { name: temp, id: segment_url[3] } });
    }
  }
  handleClickBatalAction(e) {
    window.location = "/pelatihan/rekappendaftaran";
  }
  handleGenerateExcelAction = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.get(
        process.env.REACT_APP_BASE_API_URI + "/template_import",
        {
          responseType: "blob",
        },
      );
      // create file link in browser's memory
      const href = URL.createObjectURL(response.data);

      // create "a" HTLM element with href to file & click
      const link = document.createElement("a");
      link.href = href;
      link.setAttribute("download", "template_download_peserta.xlsx"); //or any other extension
      document.body.appendChild(link);
      link.click();

      // clean up "a" element & remove ObjectURL
      document.body.removeChild(link);
      URL.revokeObjectURL(
        process.env.REACT_APP_BASE_API_URI + "/template_import",
      );
    } catch (err) {
      console.log(err);
      swal.fire({
        title: err.response?.data?.Message ?? "Gagal mengunduh file!",
        icon: "warning",
      });
    }
  };

  readBaseFile() {
    axios.get(template, { responseType: "blob" }).then((res) => {
      this.setState({ baseFile: res.data });
    });
  }
  // process CSV data
  checkFileEquality(inputFileHeader, onCheck) {
    if (this.state.baseFile == null) {
      onCheck(false, "Base file tidak ditemukan, pada saat pengecekan.");
      return;
    }
    const reader = new FileReader();
    reader.onload = (evt) => {
      const bstr = evt.target.result;
      const wb = XLSX.read(bstr, { type: "binary" });
      /* Get first worksheet */
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      /* Convert array of arrays */
      const data = XLSX.utils.sheet_to_csv(ws, { header: 1 });

      const dataStringLines = data.split(/\r\n|\n/);
      let headers = dataStringLines[0].split(
        /,(?![^"]*"(?:(?:[^"]*"){2})*[^"]*$)/,
      );
      headers = headers.map((el) => el.toLowerCase());
      if (inputFileHeader.length < headers.length) {
        onCheck(false, "File tidak sesuai dengan template yang disediakan.");
        return;
      } else {
        for (let i = 0; i < headers.length; i++) {
          if (headers[i] != inputFileHeader[i].name) {
            onCheck(
              false,
              "File tidak sesuai dengan template yang disediakan.",
            );
            return;
          }
        }
        onCheck(true);
        return;
      }
    };
    reader.readAsBinaryString(this.state.baseFile);
  }
  processData = (dataString) => {
    const dataStringLines = dataString.split(/\r\n|\n/);
    let headers = dataStringLines[0].split(
      /,(?![^"]*"(?:(?:[^"]*"){2})*[^"]*$)/,
    );
    headers = headers.map((el) => el.toLowerCase());

    const list = [];
    for (let i = 1; i < dataStringLines.length; i++) {
      const row = dataStringLines[i].split(
        /,(?![^"]*"(?:(?:[^"]*"){2})*[^"]*$)/,
      );
      if (headers && row.length == headers.length) {
        const obj = {};
        for (let j = 0; j < headers.length; j++) {
          let d = row[j];
          if (d.length > 0) {
            if (d[0] == '"') d = d.substring(1, d.length - 1);
            if (d[d.length - 1] == '"') d = d.substring(d.length - 2, 1);
          }
          if (headers[j]) {
            obj[headers[j]] = d;
          }
        }

        // remove the blank rows
        if (Object.values(obj).filter((x) => x).length > 0) {
          list.push(obj);
        }
      }
    }
    // prepare columns list from headers
    const columns = headers.map((c) => ({
      name: c,
      wrap: true,
      allowOverflow: false,
      selector: (row, index) => row[c],
    }));

    this.setState({ importData: list, headerColumn: columns });
  };

  resetError = () => {
    let errors = {};
    errors["file_peserta"] = "";
    this.setState({ errors: errors });
  };

  checkEmpty = (dataForm, fileNameField) => {
    let errors = {};
    let formIsValid = true;
    for (const field in fileNameField) {
      const nameAttribute = fileNameField[field];
      if (dataForm.get(nameAttribute).name == "") {
        errors[nameAttribute] = "Tidak Boleh Kosong";
        formIsValid = false;
      }
    }
    this.setState({ errors: errors });
    return !formIsValid;
  };

  handleFileUploadAction = (e) => {
    const reader = new FileReader();
    reader.onload = (evt) => {
      const bstr = evt.target.result;
      const wb = XLSX.read(bstr, {
        type: "binary",
        raw: true,
        cellText: false,
        cellDates: true,
      });
      /* Get first worksheet */
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      /* Convert array of arrays */
      const data = XLSX.utils.sheet_to_csv(ws, {
        header: 1,
        dateNF: 'd"/"m"/"yyyy',
      });
      this.processData(data);
    };
    reader.readAsBinaryString(this.state.selectedFile);
  };

  handleSubmitAction = async (e) => {
    e.preventDefault();
    const dataForm = new FormData(e.currentTarget);
    if (this.checkEmpty(dataForm, ["file_peserta"])) {
      return;
    }

    this.checkFileEquality(this.state.headerColumn, async (res, message) => {
      if (!res) {
        swal.fire({
          title: "Perhatian!",
          icon: "warning", // add html attribute if you want or remove
          text: message,
        });
      } else {
        swal.fire({
          title: "Mohon Tunggu!",
          icon: "info", // add html attribute if you want or remove
          allowOutsideClick: false,
          didOpen: () => {
            swal.showLoading();
          },
        });
        this.setState({ loading: true });
        const payload = this.state.importData.map((elem, index) => {
          const obj = {
            ...elem,
            tgl_lahir: elem["tanggal lahir (31/01/2000)"],
            email:
              elem["email"] && typeof elem["email"] == "string"
                ? elem["email"].trim()
                : "",
          };
          delete obj["tanggal lahir (31/01/2000)"];
          return obj;
        });

        try {
          const resp = await axios.post(
            process.env.REACT_APP_BASE_API_URI +
              "/rekappendaftaran/upload-excel-peserta",
            {
              pelatihan_id: this.state.pelatihan.id,
              user_id: Cookies.get("user_id"),
              param: JSON.stringify(payload),
            },
            this.configs,
          );
          swal
            .fire({
              title: resp.data?.result?.Message ?? "Berhasil menambahkan data.",
              icon: "success", // add html attribute if you want or remove
              allowOutsideClick: false,
            })
            .then((result) => {
              if (window !== undefined) {
                window.location.replace(
                  "/pelatihan/detail-rekappendaftaran/" +
                    this.state.pelatihan.id,
                );
              }
            });
        } catch (err) {
          const message =
            err.response?.data?.result?.Message ??
            "Terjadi kesalahan harap tunggu sejenak dan upload kembali.";
          swal.fire({
            title: message,
            icon: "error", // add html attribute if you want or remove
            allowOutsideClick: false,
          });
        }
      }
    });
  };
  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Rekap Pendaftaran
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={(e) => {
                    e.preventDefault();
                    swal
                      .fire({
                        title: "Apakah Anda Yakin?",
                        icon: "warning",
                        confirmButtonText: "Ya",
                        showCancelButton: true,
                        cancelButtonText: "Tidak",
                      })
                      .then((result) => {
                        if (result.isConfirmed) {
                          window.location =
                            "/pelatihan/detail-rekappendaftaran/" +
                            this.state.pelatihan.id;
                        }
                      });
                  }}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <form action="#" onSubmit={this.handleSubmit}>
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Import Peserta {this.state.pelatihan?.name}
                          </h1>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="row">
                          <div className="highlight bg-light-primary mt-7">
                            <div className="col-lg-12 mb-7 fv-row text-primary">
                              <h5 className="text-primary fs-5">Panduan</h5>
                              <p className="text-primary">
                                Sebelum melakukan import peserta, mohon untuk
                                membaca panduan berikut :
                              </p>
                              <ul>
                                <li>
                                  Silahkan unduh template untuk melakukan import
                                  pada link berikut{" "}
                                  <a
                                    href={template}
                                    className="btn btn-primary fw-semibold btn-sm py-1 px-2"
                                  >
                                    <i className="las la-cloud-download-alt fw-semibold me-1" />
                                    Download Template
                                  </a>
                                </li>
                                <li>Isi template sesuai format</li>
                                <li>
                                  Upload kembali file yang telah diisi pada form
                                  yang disediakan
                                </li>
                              </ul>
                            </div>
                          </div>
                          <div className="col-12 mt-7 mb-7 fv-row">
                            <label className="form-label required">
                              Upload Data Peserta
                            </label>
                            <input
                              type="file"
                              className="form-control form-control-sm mb-2"
                              name="file_peserta"
                              id="file_peserta"
                              accept=".csv"
                              ref={this.filePesertaRef}
                              onChange={(e) => {
                                this.setState(
                                  { selectedFile: e.target.files[0] },
                                  this.handleFileUpload,
                                );
                              }}
                            />
                            <small className="text-muted">
                              Format file (.csv)
                            </small>
                            <span style={{ color: "red" }}>
                              {this.state.errors["file_peserta"]}
                            </span>

                            {this.state.selectedFile && (
                              <div
                                className="alert alert-success alert-dismissible fade show mt-7"
                                role="alert"
                              >
                                <strong>{this.state.selectedFile?.name}</strong>
                                <button
                                  type="button"
                                  className="btn-close"
                                  data-bs-dismiss="alert"
                                  aria-label="Close"
                                  onClick={(e) => {
                                    e.preventDefault();
                                    this.filePesertaRef.current.value = null;
                                    this.setState({
                                      selectedFile: null,
                                      importData: null,
                                    });
                                  }}
                                ></button>
                              </div>
                            )}
                          </div>

                          {this.state.importData && this.state.headerColumn && (
                            <div className="col-12">
                              <DataTable
                                pagination
                                highlightOnHover
                                columns={this.state.headerColumn}
                                data={this.state.importData}
                              />
                            </div>
                          )}

                          <div className="row my-7 pt-7">
                            <div className="col-lg-12 mb-7 fv-row text-center">
                              <button
                                className="btn btn-light btn-md me-3"
                                onClick={(e) => {
                                  e.preventDefault();
                                  this.filePesertaRef.current.value = null;
                                  this.setState({
                                    selectedFile: null,
                                    importData: null,
                                  });
                                  this.resetError();
                                }}
                              >
                                Batal
                              </button>
                              <button
                                type="submit"
                                className="btn btn-primary btn-md"
                              >
                                <i className="fa fa-paper-plane me-1"></i>Simpan
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
