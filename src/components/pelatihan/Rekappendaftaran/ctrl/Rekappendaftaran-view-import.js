import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../Rekappendaftaran-Import-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const RekappendaftaranView = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default RekappendaftaranView;
