import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../Rekappendaftaran-Import-Score-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const RekappendaftaranViewImportScore = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default RekappendaftaranViewImportScore;
