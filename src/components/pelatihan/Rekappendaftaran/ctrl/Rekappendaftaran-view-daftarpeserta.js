import React from "react";
import Header from "../../../../components/Header";
import Breadcumb from "../../../../components/Breadcumb";
import Content from "../../Rekappendaftaran/Rekappendaftaran-View-DaftarPeserta-Content";
import SideNav from "../../../../components/SideNav";
import Footer from "../../../../components/Footer";

const RekappendaftaranViewDaftarPeserta = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default RekappendaftaranViewDaftarPeserta;
