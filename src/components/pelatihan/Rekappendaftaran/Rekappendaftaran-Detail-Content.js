import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import DataTable from "react-data-table-component";
import { Link } from "react-router-dom";
import * as FileSaver from "file-saver";
import {
  handleFormatDate,
  PESERTA_PELATIHAN_KEY,
  PELATIHAN_ID_KEY,
  capitalWord,
  dateRange,
  resolveStatusPelatihanBg,
  tagColorMapStatusPeserta,
  disabledStatus,
  provider,
} from "../Pelatihan/helper";
import * as XLSX from "xlsx";
import moment from "moment";
import ViewAlasanBatal from "./viewAlasanBatal";
import ShowUUPdp from "./showUUPdp";
import { fixBootstrapDropdown } from "./../../../utils/commonutil";

export default class RekappendaftarandetailContent extends React.Component {
  constructor(props) {
    super(props);
    Cookies.remove(PELATIHAN_ID_KEY);
    this.pindasPesertaCloseButtonRef = React.createRef(null);
    this.pindahModalRef = React.createRef(null);
    this.updateModelCloseButtonRef = React.createRef(null);
    this.updateModelCloseButtonRef2 = React.createRef(null);
    this.modalRef = React.createRef(null);
    this.handleChangeSort = this.handleChangeSortAction.bind(this);
    this.handlePendingChangeSort =
      this.handlePendingChangeSortAction.bind(this);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handlesubmitFilter = this.handlesubmitFilterAction.bind(this);
    this.handlesubmitFilterPending =
      this.handlesubmitFilterPendingAction.bind(this);
    this.exportPesertaTable = this.exportPesertaTableAction.bind(this);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleSearchEmpty = this.handleSearchEmptyAction.bind(this);
    this.handleChangeStatusTestSubstansi =
      this.handleChangeStatusTestSubstansiAction.bind(this);
    this.handleChangeStatusValidasi =
      this.handleChangeStatusValidasiAction.bind(this);
    this.handleChangeStatusBerkas =
      this.handleChangeStatusBerkasAction.bind(this);
    this.handleChangeStatusPeserta =
      this.handleChangeStatusPesertaAction.bind(this);
    this.handleChangeStatusPesertaUpdate =
      this.handleChangeStatusPesertaUpdateAction.bind(this);
    this.handleChangePelatihanTarget =
      this.handleChangePelatihanTargetAction.bind(this);
    this.handleClickReset = this.handleClickResetAction.bind(this);
    this.handleClickResetPending =
      this.handleClickResetPendingAction.bind(this);
    this.handleSubmitUpdateStatusBulk =
      this.handleSubmitUpdateStatusBulkAction.bind(this);
    this.exportPesertaPendingTable = this.exportPesertaTableAction.bind(this);
    this.updatePesertaBulk = this.handleSubmitPindahPesertaBulk.bind(this);
    this.handleUpdatePrediksiBulk =
      this.handleUpdatePrediksiBulkAction.bind(this);
  }

  state = {
    perPageOption: [10, 15, 20, 25, 30],
    selectedUser: null,
    selectableRowsEligible: [],
    selectableRowsIneligible: [],
    fields: {},
    errors: {},
    datax: [],
    jadwalx: [],
    dataxpelatihan: [],
    titleheader: "",
    numberrow: 1,
    statistikx: null,
    page: 1,
    newPerPage: 10,
    newPerPagePending: 10,
    searchQuery: "",
    valStatusTestSubstansi: [],
    valStatusBerkas: [],
    valStatusPeserta: [],
    valStatusPesertaUpdate: [],
    loading: false,
    pendingTableLoading: false,
    selectedTableRows: [],
    filterOptions: [],
    errors: {},
    dataxpending: [],
    searchPendingQuery: "",
    totalPendingRow: "",
    sort: "prediction",
    sort_val: "desc",
    sortpending: "name",
    sortpending_val: "asc",
    totalRows: 0,
    valStatusVlalidasi: [],
    isUpdateAll: false,
    isUpdateAllPrediksi: false,
    valPelatihanTarget: [],
    dataxtargetpelatihan: [],
    updateBulkOptions: [],
    toggleCleared: false,
    showPdpModal: false,
    tableToExport: "",
    jenisData: "",
    isRowChangeRef: false,
    isRowChangePendingRef: false,
    waktuPendaftaran: [],
    waktuPelatihan: [],
    preferedExt: "xlsx",
    columns: [
      {
        name: "No",
        center: true,
        width: "70px",
        cell: (row, index) => (
          <div>
            <span>{this.state.numberrow + index + 0}</span>
          </div>
        ),
      },
      {
        name: "Nama Peserta",
        sortable: true,
        width: "310px",
        cell: (row) => (
          <div>
            <a
              href="#"
              onClick={(e) => {
                e.preventDefault();
                const idPeserta = this.state.datax?.map((elem, index) => {
                  return { index: index + 1, id: elem.user_id };
                });
                Cookies.set(PESERTA_PELATIHAN_KEY, JSON.stringify(idPeserta));
                window.location.href =
                  "/pelatihan/view-rekappendaftaran-daftarpeserta/" +
                  row.user_id +
                  "/" +
                  row.master_form_builder_id;
              }}
              id={row.user_id}
              title="Detail"
              className="text-dark"
            >
              <label className="d-flex flex-stack my-2 cursor-pointer">
                <span className="d-flex align-items-center me-2">
                  <span className="symbol symbol-50px me-6">
                    <span className="symbol-label bg-light-primary">
                      <span className="svg-icon svg-icon-1 svg-icon-primary">
                        <img
                          src={
                            process.env.REACT_APP_BASE_API_URI +
                            "/download/get-file?path=" +
                            row.foto
                          }
                          alt=""
                          className="symbol-label"
                        />
                      </span>
                    </span>
                  </span>
                  <span className="d-flex flex-column">
                    {/* <h6 className="text-muted fs-7 fw-semibold mb-1">
                      {row.nik}
                    </h6>*/}
                    <h6 className="text-muted fs-7 fw-semibold mb-1">
                      {row.nomor_registrasi}
                    </h6>
                    <h6 className="fw-bolder fs-7 mb-1">
                      {this.capitalWord(row.name)}
                    </h6>
                    {/*<span className="text-muted fs-7 fw-semibold">
                      {row.email}
                    </span>*/}
                  </span>
                </span>
              </label>
            </a>
          </div>
        ),
      },
      {
        sortable: true,
        id: "test-substansi-score",
        center: true,
        width: "160px",
        cell: (row) =>
          row.status_tessubstansi.toLowerCase() == "sudah mengerjakan" ? (
            <div>
              <div className="fs-7">
                Score: <b>{row.nilai ?? "-"}</b>
                <br />
                Passing Grade: <b>{row.passing_grade ?? "-"}</b>
              </div>
              <div>
                <span
                  className={`badge ${
                    row.status_eligible == "Eligible"
                      ? "badge-light-success"
                      : "badge-light-danger"
                  }`}
                >
                  {this.capitalWord(row.status_eligible ?? "-")}
                </span>
              </div>
            </div>
          ) : (
            <div className="fs-7">
              <span className="badge badge-light-danger">
                {this.capitalWord(row.status_tessubstansi ?? "-")}
              </span>
            </div>
          ),
      },
      {
        name: "Test Substansi",
        id: "test-substansi-value",
        sortable: true,
        center: false,
        width: "160px",
        selector: (row) => row.status_eligible,
        cell: (row) =>
          row.status_tessubstansi.toLowerCase() == "sudah mengerjakan" ? (
            <div>
              <div className="fs-7">
                <i className="bi bi-files me-1"></i>
                {row.jawaban_benar + row.jawaban_salah ?? "-"} Soal
              </div>
              <div className="fs-7">
                <i className="bi bi-check-circle-fill text-success me-1"></i>
                {row.jawaban_benar ?? "-"} Benar
              </div>
              <div className="fs-7">
                <i className="bi bi-x-circle-fill text-danger me-1"></i>
                {row.jawaban_salah ?? "-"} Salah
              </div>
            </div>
          ) : (
            <div className="fs-7">
              <span className="badge badge-light-danger">
                {this.capitalWord(row.status_tessubstansi ?? "-")}
              </span>
            </div>
          ),
      },
      {
        sortable: true,
        id: "test-substansi-time",
        width: "160px",
        center: false,
        selector: (row) => row.status_eligible,
        cell: (row) =>
          row.status_tessubstansi.toLowerCase() == "sudah mengerjakan" ? (
            <div>
              <div className="d-block fs-7">
                <i className="bi bi-calendar me-1"></i>
                {handleFormatDate(
                  row.waktu_pengerjaan,
                  "DD MMM YYYY",
                  "YYYY-MM-DD",
                )}
              </div>
              <div className="d-block fs-7">
                <i className="bi bi-clock me-1"></i>
                {row.lama_ujian}
              </div>
            </div>
          ) : (
            <div className="fs-7">
              <span className="badge badge-light-danger">
                {this.capitalWord(row.status_tessubstansi ?? "-")}
              </span>
            </div>
          ),
      },
      {
        name: "Status Peserta",
        sortable: true,
        center: false,
        width: "280px",
        selector: (row) => row.status_peserta,
        cell: (row) => (
          <div>
            <div style={{ textAlign: "center" }}>
              <span
                className={`badge fs-7 badge-light-${
                  tagColorMapStatusPeserta[row.status_peserta] ?? "danger"
                }`}
              >
                {this.capitalWord(row.status_peserta ?? "-")}
              </span>
            </div>
          </div>
        ),
      },
      {
        name: "Tanggal Pendaftaran",
        sortable: true,
        width: "200px",
        cell: (row) => (
          <div>
            <span className="">
              {row.created_at
                ? moment(row.created_at, "YYYY-MM-DD HH:mm:ss").format(
                    "DD MMM YYYY HH:mm:ss",
                  )
                : "-"}
            </span>
          </div>
        ),
      },
      {
        name: "Prediksi Kelulusan",
        sortable: true,
        center: true,
        width: "180px",
        cell: (row) => (
          <div
            style={{ position: "relative", display: "inline-block" }}
            onMouseEnter={(e) => {
              const tooltip = document.getElementById("tooltip-text");
              tooltip.style.visibility = "visible";
              tooltip.style.opacity = 1;
              tooltip.style.top = e.clientY + 10 + "px";
              tooltip.style.left = e.clientX + "px";
            }}
            onMouseLeave={() => {
              const tooltip = document.getElementById("tooltip-text");
              tooltip.style.visibility = "hidden";
              tooltip.style.opacity = 0;
            }}
          >
            <span className="">
              {row.prediction !== null
                ? Math.round(row.prediction * 10000) / 100 + "%"
                : "-"}
            </span>
            <div
              id="tooltip-text"
              style={{
                visibility: "hidden",
                position: "fixed",
                backgroundColor: "#333",
                color: "#fff",
                textAlign: "center",
                borderRadius: "6px",
                padding: "5px",
                transform: "translateX(-60%)",
                opacity: 0,
                width: "190px",
                fontSize: "12px",
                transition: "visibility 0.3s, opacity 0.3s",
              }}
            >
              Prediksi menggunakan model{" "}
              <i style={{ color: "#fff", fontSize: "12px" }}>
                machine learning
              </i>{" "}
              berdasarkan profil peserta
            </div>
          </div>
        ),
      },
      {
        name: "Log",
        sortable: true,
        width: "150px",
        cell: (row) => (
          <div>
            <span className="fw-semibold fs-8">{row.updated_oleh ?? "-"}</span>
            <br />
            <span className="fs-8">
              {row.tgl_updated
                ? moment(row.tgl_updated).format("DD MMMM YYYY")
                : "-"}
            </span>
            <br />
            <span className="fs-8">
              {row.waktu_updated
                ? moment(row.waktu_updated, "HH:mm:ss").format("HH:mm:ss")
                : "-"}
            </span>
          </div>
        ),
      },
      {
        name: "Aksi",
        center: true,
        width: "150px",
        cell: (row) => (
          <div className="row">
            <a
              href="#"
              onClick={(e) => {
                e.preventDefault();
                const idPeserta = this.state.datax?.map((elem, index) => {
                  return { index: index + 1, id: elem.user_id };
                });
                Cookies.set(PESERTA_PELATIHAN_KEY, JSON.stringify(idPeserta));
                window.location.href =
                  "/pelatihan/view-rekappendaftaran-daftarpeserta/" +
                  row.user_id +
                  "/" +
                  row.master_form_builder_id;
              }}
              id={row.user_id}
              title="View"
              className="btn btn-icon btn-sm btn-primary me-1"
            >
              <i className="fa fa-eye"></i>
            </a>
            {row.status_peserta?.toLowerCase() == "pembatalan" && (
              <a
                href="#"
                id={row.id}
                title="Alasan Pembatalan"
                onClick={() => {
                  this.setState({ selectedUser: row });
                }}
                className="btn btn-icon btn-bg-danger btn-sm me-1"
              >
                <i className="bi bi-folder-check text-white"></i>
              </a>
            )}
          </div>
        ),
      },
    ],
  };
  capitalWord(str) {
    if (str && typeof str == "string") {
      return str.replace(/\w\S*/g, function (kata) {
        const kataBaru = kata.slice(0, 1).toUpperCase() + kata.substr(1);
        return kataBaru;
      });
    } else return str;
  }
  handleKeyPressAction(e) {
    if (e.key == "Enter") {
      if (e.target.name == "pendingSearch") {
        this.setState({ searchPendingQuery: e.target.value }, () => {
          this.handleReloadListUserPending(1, 10);
        });
      } else if (e.target.name == "search") {
        this.setState({ searchQuery: e.target.value }, () => {
          this.handleReloadListUser(1, 10);
        });
      }
    }
  }
  handleSearchEmptyAction(e) {
    if (e.target.value.length == 0 && e.target.name == "search") {
      this.setState({ searchQuery: "" }, () => {
        this.handleReloadListUser(1, 10);
      });
    } else if (e.target.value.length == 0 && e.target.name == "pendingSearch") {
      this.setState({ searchPendingQuery: "" }, () => {
        this.handleReloadListUserPending(1, 10);
      });
    }
  }
  handleChangeSortAction(column, sortDirection) {
    let server_name = "";
    if (column.name == "Nama Peserta") {
      server_name = "name";
    } else if (column.name == "Pelatihan Sebelumnya") {
      server_name = "jml_pelatian_sebelumnya";
    } else if (column.name == "Test Substansi") {
      server_name = "status_tessubstansi";
    } else if (column.name == "Status Peserta") {
      server_name = "status_eligible";
    } else if (column.name == "Updated") {
      server_name = "updated_oleh";
    } else if (column.name == "Hasil Test") {
      server_name = "jawaban_benar";
    } else if (column.name == "Tanggal Pendaftaran") {
      server_name = "created_at";
    } else if (column.name == "Prediksi Kelulusan") {
      server_name = "prediction";
    }

    this.setState(
      {
        sort: server_name,
        sort_val: sortDirection,
      },
      () => {
        this.handleReloadListUser(1, this.state.newPerPage);
      },
    );
  }

  isThrowed = false;
  handlePendingChangeSortAction(column, sortDirection) {
    let server_name = "";
    if (column.name == "Nama File") {
      server_name = "file_name";
    } else if (column.name == "Jumlah Data Valid") {
      server_name = "jml_valid";
    } else if (column.name == "Jumlah Data Tidak Valid") {
      server_name = "jml_tdk_valid";
    } else if (column.name == "Uploader") {
      server_name = "name";
    }

    this.setState(
      {
        sortpending: server_name,
        sortpending_val: sortDirection.toUpperCase(),
      },
      () => {
        this.handleReloadListUserPending(1, this.state.newPerPage);
      },
    );
  }
  handleClickResetAction() {
    this.setState(
      {
        valStatusBerkas: [],
        valStatusPeserta: [],
        valStatusTestSubstansi: [],
      },
      () => {
        this.handleReloadListUser();
      },
    );
  }
  handleClickResetPendingAction() {
    this.setState(
      {
        valStatusVlalidasi: [],
      },
      () => {
        this.handleReloadListUserPending();
      },
    );
  }
  // handlePendingPageChange = (page) => {
  //   this.setState({ pendingTableLoading: true });
  //   this.handleReloadListUserPending(page, this.state.newPerPagePending);
  // };

  // handlePendingPerRowsChange = async (newPerPage, page) => {
  //   this.setState({ pendingTableLoading: true });
  //   this.setState({ newPerPagePending: newPerPage });
  //   this.handleReloadListUserPending(page, newPerPage);
  // };

  handlePerRowsChangePending = async (arg1, arg2, srcEvent) => {
    if (srcEvent == "page-change") {
      this.setState({ pendingTableLoading: true }, () => {
        if (!this.state.isRowChangePendingRef) {
          this.handleReloadListUserPending(arg1, this.state.newPerPage);
        }
      });
    } else if (srcEvent == "row-change") {
      this.setState({ isRowChangePendingRef: true }, () => {
        this.handleReloadListUserPending(arg2, arg1);
      });
      this.setState({ pendingTableLoading: true, newPerPage: arg1 }, () => {
        this.setState({ isRowChangePendingRef: false });
      });
    }
  };

  handlePerRowsChange = async (arg1, arg2, srcEvent) => {
    if (srcEvent == "page-change") {
      this.setState({ loading: true }, () => {
        if (!this.state.isRowChangeRef) {
          this.handleReloadListUser(arg1, this.state.newPerPage);
        }
      });
    } else if (srcEvent == "row-change") {
      console.log(arg1, arg2);
      if (arg1 == "Semua") {
        arg1 = this.state.datax.totalRows;
      }
      this.setState({ isRowChangeRef: true }, () => {
        this.handleReloadListUser(arg2, arg1);
      });
      this.setState({ loading: true, newPerPage: arg1 }, () => {
        this.setState({ isRowChangeRef: false });
      });
    }
  };

  isPindahEligible = (status) => {
    return ![
      "pelatihan",
      "banned",
      "tdk. lulus administrasi",
      "tidak lulus administrasi",
      "tdk. lulus tes substansi",
      "tidak lulus tes substansi",
      "diterima",
      "pelatihan",
      "lulus pelatihan - kehadiran",
      "lulus pelatihan - nilai",
      "tdk. lulus pelatihan - kehadiran",
      "tidak lulus pelatihan - kehadiran",
      "tdk. lulus pelatihan - nilai",
      "tidak lulus pelatihan - nilai",
    ].includes(status?.toLowerCase());
  };
  handleSubmitPindahPesertaBulk(e) {
    e.preventDefault();
    const errors = {};
    if (!this.state.valPelatihanTarget.value) {
      errors["pelatihan_target"] = "Pelatihan target tidak boleh kosong.";
    }
    if (
      !this.state.isUpdateAll &&
      this.state.selectableRowsEligible.length == 0
    ) {
      errors["daftar_peserta_eligible"] =
        "Pilihan peseta eligible tidak boleh kosong.";
    }
    if (Object.keys(errors).length > 0) {
      this.setState({ errors: errors });
      return;
    }
    const payload = {
      pelatian_id_fr: this.state.dataxpelatihan.id_pelatihan,
      pelatian_id_to: this.state.valPelatihanTarget.value,
      created_by: Cookies.get("user_id"),
      status: this.state.isUpdateAll ? 1 : 0,
      users: [],
    };
    if (!this.state.isUpdateAll) {
      this.state.selectableRowsEligible.forEach((row) => {
        payload.users.push({
          userid: row.user_id,
        });
      });
    }
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/rekappendaftaran/simpan-pindah-peserta",
        [payload],
        this.configs,
      )
      .then((resp) => {
        if (resp.data.result.Success) {
          this.setState({ toggleCleared: !this.state.toggleCleared });
          swal
            .fire({
              title: "Berhasil memindahkan peserta",
              text: resp.data.result?.Message ?? "Berhasil melakukan update...",
              icon: "success",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.handleReloadListUser();
              }
            });
          this.pindasPesertaCloseButtonRef?.current.click();
        } else {
          this.isThrowed = true;
          throw Error(resp.data.result?.Message);
        }
      })

      .catch((err) => {
        this.setState({ toggleCleared: !this.state.toggleCleared });
        let messagex = this.isThrowed
          ? capitalWord(err.message)
          : err.data?.response?.result?.Message;
        swal
          .fire({
            title: "Terjadi kesalahan",
            text:
              messagex ??
              "Terjadi kesalahan saat melakukan pemindahan peserta pelatihan...",
            icon: "error",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            this.isThrowed = false;
            if (result.isConfirmed) {
              this.handleReloadListUser();
            }
          });
      });
  }
  handleSubmitUpdateStatusBulkAction(e) {
    e.preventDefault();
    const errors = {};
    if (!this.state.isUpdateAll && this.state.selectedTableRows.length == 0) {
      errors["daftar_peserta"] = "Pilihan peseta tidak boleh kosong.";
    }
    if (!this.state.valStatusPesertaUpdate.value) {
      errors["status_peserta"] = "Status tidak boleh kosong.";
    }
    if (Object.keys(errors).length > 0) {
      this.setState({ errors: errors });
      return;
    }
    const payload = {
      status_pilihan: this.state.isUpdateAll ? 1 : 0,
      status: this.state.valStatusPesertaUpdate["value"] ?? "",
      pelatihan_id: this.state.dataxpelatihan.id,
      reminder_berkas: 0,
      reminder_profile: 0,
      reminder_riwayat: 1,
      reminder_dokumen: 1,
      updatedby: Cookies.get("user_id"),
      categoryOptItems: [],
    };
    this.state.selectedTableRows.forEach((row) => {
      payload.categoryOptItems.push({
        id_user: row.user_id,
      });
    });
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/rekappendaftaran/update-peserta-pendaftaran-bulk",
        [payload],
        this.configs,
      )
      .then((resp) => {
        this.setState({ toggleCleared: !this.state.toggleCleared });
        swal
          .fire({
            title: "Update data berhasil",
            text: resp.data.result?.Message ?? "Berhasil melakukan update...",
            icon: "success",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
              this.handleReloadListUser();
            }
          });
        this.updateModelCloseButtonRef?.current.click();
      })
      .catch((err) => {
        this.setState({ toggleCleared: !this.state.toggleCleared });
        swal
          .fire({
            title: "Terjadi kesalahan",
            text:
              err.data?.response?.result?.Message ??
              "Terjadi kesalahan saat melakukan update...",
            icon: "error",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
              this.handleReloadListUser();
            }
          });
      });
  }
  handleChangeStatusValidasiAction(selectedOption) {
    this.setState({
      valStatusVlalidasi: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  }
  handleUpdatePrediksiBulkAction(e) {
    e.preventDefault();
    const errors = {};
    if (
      !this.state.isUpdateAllPrediksi &&
      this.state.selectedTableRows.length == 0
    ) {
      errors["daftar_peserta"] = "Checklist untuk memilih seluruh peserta";
    }
    if (Object.keys(errors).length > 0) {
      this.setState({ errors: errors });
      return;
    }

    const payload = {
      status_pilihan: this.state.isUpdateAllPrediksi ? 1 : 0,
      pelatihan_id: this.state.dataxpelatihan.id,
      tema: this.state.dataxpelatihan.tema,
      nomor_registrasi: [],
      alur_pendaftaran: this.state.jadwalx.alur_pendaftaran,
      status_peserta: [],
      tema_status: this.state.dataxpelatihan.status_pelatihan,
    };

    this.state.selectedTableRows.forEach((row) => {
      payload.nomor_registrasi.push(row.nomor_registrasi);
    });

    this.state.selectedTableRows.forEach((row) => {
      payload.status_peserta.push(row.status_tessubstansi);
    });

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/rekappendaftaran/update-prediksi-peserta-bulk",
        [payload],
        this.configs,
      )
      .then((resp) => {
        const message = resp.data.result.result?.Message;
        this.setState({ toggleCleared: !this.state.toggleCleared });
        if (
          message[0] === "Model Tidak Tersedia" ||
          message === "Method Not Allowed" ||
          message === "Prediksi Sudah Dilakukan Pada Semua Peserta" ||
          message === "Pelatihan Sudah Lewat Masa Pendaftaran atau Seleksi" ||
          message === "Peserta Belum Mengerjakan Tes Substansi"
        ) {
          swal
            .fire({
              title: "Update prediksi gagal",
              text: message,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.handleReloadListUser();
              }
            });
        } else {
          swal
            .fire({
              title: "Update prediksi berhasil",
              text: message ?? "Berhasil melakukan update...",
              icon: "success",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.handleReloadListUser();
              }
            });
        }
        // Empty the array
        payload.nomor_registrasi = [];
        this.state.selectedTableRows = [];
        this.updateModelCloseButtonRef2?.current.click();
      })
      .catch((err) => {
        this.setState({ toggleCleared: !this.state.toggleCleared });
        swal
          .fire({
            title: "Terjadi kesalahan",
            text:
              err.data?.response?.result?.Message ??
              "Terjadi kesalahan saat melakukan update...",
            icon: "error",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
              this.handleReloadListUser();
            }
          });
      });
    // Empty the array
    payload.nomor_registrasi = [];
    this.state.selectedTableRows = [];
    this.updateModelCloseButtonRef2?.current.click();
  }
  getImportedFile = (statusFile, fileName) => {
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/rekappendaftaran/list-unduh-import",
        {
          status: statusFile,
          file_name: fileName,
        },
        this.configs,
      )
      .then((res) => {
        const result = res.data.result;
        swal.close();
        if (result.Status) {
          //create xlsx file
          const excelData = result.result.map((row) => {
            row["Nama"] = row["name"];
            // row["Nik"] = row["nik"];
            // row["No_hp"] = row["no_hp"];
            // row["Tanggal Lahir (31/01/2000)"] = row["birthdate"];
            row["Status"] = row["status"];
            row["Deskripsi Kesalahan"] = row["description"];
            delete row["birthdate"];
            delete row["file_name"];
            delete row["status"];
            delete row["name"];
            delete row["nik"];
            delete row["no_hp"];
            delete row["description"];
          });
          this.exportToCSV(
            result.result,
            statusFile + "_" + fileName.split(".").length > 0
              ? fileName.split(".")[0]
              : fileName,
            "csv",
          );
        } else {
          throw Error(result.Message);
        }
      })
      .catch((err) => {
        console.log(err);
        const messagex = err.response?.data?.result?.Message ?? err.message;
        swal.fire({
          title: "Terjadi kesalahan",
          text: messagex ?? "Terjadi kegagalan saat mengambil data.",
          icon: "error",
        });
      });
  };

  handleChangeStatusTestSubstansiAction = (selectedOption) => {
    this.setState({
      valStatusTestSubstansi: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  };
  handleChangeStatusPesertaAction = (selectedOption) => {
    this.setState({
      valStatusPeserta: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  };
  handleChangeStatusPesertaUpdateAction = (selectedOption) => {
    this.setState({
      valStatusPesertaUpdate: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  };

  handleChangePelatihanTargetAction = (selectedOption) => {
    this.setState({
      valPelatihanTarget: selectedOption,
    });
  };
  handleChangeStatusBerkasAction = (selectedOption) => {
    this.setState({
      valStatusBerkas: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  customLoader = (
    <div style={{ padding: "24px" }}>
      <img src="/assets/media/loader/loader-biru.gif" />
    </div>
  );

  pendingColumns = [
    {
      name: "No",
      center: true,
      width: "70px",
      cell: (row, index) => (
        <div>
          <span>{this.state.numberrow + index + 0}</span>
        </div>
      ),
    },
    {
      name: "Nama File",
      sortable: true,
      width: "400px",
      cell: (row) => (
        <div>
          <span>
            <b>{this.capitalWord(row.file_name)}</b>
          </span>
        </div>
      ),
    },
    {
      name: "Data Valid",
      sortable: false,
      center: true,
      width: "200px",
      cell: (row) => (
        <div>
          <span className="badge badge-light-success">
            {this.capitalWord(row.jml_valid ?? 0)}
          </span>
        </div>
      ),
    },
    {
      name: "Data Tidak Valid",
      sortable: false,
      center: true,
      width: "200px",
      cell: (row) => (
        <div>
          <span className="badge badge-light-danger">
            {this.capitalWord(row.jml_tdk_valid ?? 0)}
          </span>
        </div>
      ),
    },
    {
      name: "Log",
      width: "200px",
      sortable: false,
      cell: (row) => (
        <div>
          <span className="fw-semibold fs-8">{row.name}</span>
          <br />
          <span className="fs-8">
            {handleFormatDate(
              row.created_at,
              "DD MMMM YYYY HH:mm:ss",
              "YYYY-MM-DD HH:mm:ss",
            )}
          </span>
          <br />
        </div>
      ),
    },
    {
      name: "Aksi",
      center: true,
      // width: "150px",
      cell: (row) => (
        <div className="row">
          {row.jml_tdk_valid > 0 && (
            <button
              href="#"
              id={row.user_id + "_1"}
              key={row.user_id + "_1"}
              title="Download Peserta Gagal Import"
              className="btn btn-icon w-30px h-30px me-3 btn-danger"
              onClick={() => {
                this.getImportedFile("tidak valid", row.file_name);
              }}
            >
              <i class="bi bi-download"></i>
            </button>
          )}
          {row.jml_valid > 0 && (
            <button
              href="#"
              id={row.user_id + "_2"}
              key={row.user_id + "_2"}
              title="Download Peserta Berhasil Import"
              className="btn btn-icon w-30px h-30px me-3 btn-success"
              onClick={() => {
                this.getImportedFile("valid", row.file_name);
              }}
            >
              <i class="bi bi-download"></i>
            </button>
          )}
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
        //fontWeight: 'bold',
      },
    },
  };

  fileType =
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
  fileExtension = ".csv";

  exportToCSV(apiData, fileName, extension) {
    const ws = XLSX.utils.json_to_sheet(apiData);
    const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
    const excelBuffer = XLSX.write(wb, {
      bookType: extension,
      type: "array",
      FS: ";",
      Props: {
        Subject: "Rekap pendaftaran",
        Tags: "Daftar peserta",
        Title: "Generated From Digitalent Scholarship",
        Comments: `By (${Cookies.get("user_name")} - ${Cookies.get(
          "user_id",
        )}) on ${moment().format("DD/MM/YYYY, HH:mm:ss")}`,
        Name: `From DTS By (${Cookies.get("user_id")}) on ${moment().format(
          "DD/MM/YYYY, HH:mm:ss",
        )}`,
      },
    });
    const data = new Blob([excelBuffer], { type: this.fileType });
    FileSaver.saveAs(data, fileName + "." + extension);
  }

  exportPesertaTableAction(name, extension) {
    const dataExport =
      name == "pending" ? this.state.dataxpending : this.state.datax;
    const excel = [];
    const userId = Cookies.get("user_id");
    const currtime = moment().format("YYYYMMDDHHmmss");
    dataExport.forEach((element, index) => {
      const keys = Object.keys(element);
      const rows = {};
      keys.forEach((key) => {
        if (
          !key.includes("code") &&
          !key.includes("created_by") &&
          !key.includes("updated_at") &&
          !key.includes("updated_by") &&
          !key.includes("address_ktp") &&
          !key.includes("provinsi_ktp") &&
          !key.includes("kota_ktp") &&
          !key.includes("kecamatan_ktp") &&
          !key.includes("kode_pos_ktp") &&
          !key.includes("kode_pos") &&
          !key.includes("kelurahan_ktp") &&
          !key.includes("keterangan_hits_capil") &&
          !key.includes("flag_cron") &&
          !key.includes("flag_capil") &&
          !key.includes("flag_email") &&
          !key.includes("flag_verify") &&
          !key.includes("nik") &&
          !key.includes("foto") &&
          !key.includes("hubungan") &&
          !key.includes("nama_kontak_darurat") &&
          !key.includes("nomor_handphone_darurat") &&
          !key.includes("tempat_lahir") &&
          !key.includes("alamat") &&
          !key.includes("address") &&
          !key.includes("jenjang") &&
          !key.includes("ipk") &&
          !key.includes("asal_pendidikan") &&
          !key.includes("program_studi") &&
          !key.includes("tahun_masuk") &&
          !key.includes("ijasah") &&
          !key.includes("waktu_updated") &&
          !key.includes("prediction")
        ) {
          // rows["No"] = index + 1;
          rows["Pelatihan ID_" + userId + "_" + currtime] = element.pelatian_id;
          rows["Nama Akademi_" + userId + "_" + currtime] =
            this.state.dataxpelatihan.akademi;
          rows["Nama Tema_" + userId + "_" + currtime] =
            this.state.dataxpelatihan.tema;
          rows["Nama Pelatihan_" + userId + "_" + currtime] =
            this.state.dataxpelatihan.pelatihan +
            "(Batch" +
            this.state.dataxpelatihan.batch +
            ")";
          rows["Penyelenggara_" + userId + "_" + currtime] =
            this.state.dataxpelatihan.penyelenggara;
          rows["Mitra_" + userId + "_" + currtime] =
            this.state.dataxpelatihan.nama_mitra;
          if (!key.includes("_id") && !key.includes("id_")) {
            const col = this.capitalWord(key).replaceAll("_", " ");
            if (key == "tgl_updated") {
              rows[col + "_" + userId + "_" + currtime] = moment(
                element[key],
              ).isValid()
                ? moment(element[key]).format("DD MMMM YYYY")
                : "-";
            } else if (key == "waktu_pengerjaan") {
              rows[col + "_" + userId + "_" + currtime] = moment(
                element[key],
                "HH:mm:ss",
              ).isValid()
                ? moment(element[key], "HH:mm:ss").format("HH:mm:ss")
                : "-";
            } else if (key == "jenis_kelamin") {
              rows[col + "_" + userId + "_" + currtime] =
                element[key] == 1 ? "Pria" : "Wanita";
            } else if (key == "name") {
              rows["Nama_" + userId + "_" + currtime] = element[key];
            } else if (key == "nomor_hp") {
              rows["Nomor Hp_" + userId + "_" + currtime] = element[key];
              rows["Provider_" + userId + "_" + currtime] =
                element[key] && provider[element[key].substring(0, 5)]
                  ? provider[element[key].substring(0, 5)]
                  : "";
            } else if (key == "email") {
              rows["Email_" + userId + "_" + currtime] = element[key];
            } else if (key == "tanggal_lahir") {
              rows["Usia_" + userId + "_" + currtime] =
                Math.floor(moment().diff(element[key], "years"), true) +
                "Tahun";
            } else if (key == "created_at") {
              rows["Tanggal Pendaftaran_" + userId + "_" + currtime] = moment(
                element[key],
                "YYYY-MM-DD HH:mm:ss",
              ).isValid()
                ? moment(element[key], "YYYY-MM-DD HH:mm:ss").format(
                    "DD MMM YYYY HH:mm:ss",
                  )
                : "-";
            } else {
              rows[col + "_" + userId + "_" + currtime] = this.capitalWord(
                element[key],
              );
            }
            rows["Prediksi Kelulusan_" + userId + "_" + currtime] =
              this.state.dataxpelatihan.prediction;

            if (
              typeof element[key] == "string" &&
              element[key]?.split(".").length > 1 &&
              element[key].includes("value_form/")
            ) {
              rows[col + "_" + userId + "_" + currtime] =
                process.env.REACT_APP_BASE_API_URI +
                "/download/get-file?path=" +
                element[key];
            }
          }
        }
      });
      excel.push(rows);
    });
    let tanggal = Date().toString();
    let split_tanggal = tanggal.split(" ");
    let text_tanggal =
      split_tanggal[1] +
      "_" +
      split_tanggal[2] +
      "_" +
      split_tanggal[3] +
      "_" +
      split_tanggal[4];
    const isPendingName = name == "pending" ? "Pending " : "";
    this.exportToCSV(
      excel,
      "Peserta Pelatihan " +
        isPendingName +
        this.state.dataxpelatihan.name +
        " " +
        text_tanggal,
      extension,
    );
  }

  fetchPelatihanAvalibalePindah(id_pelatihan) {
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/rekappendaftaran/list-pelatihan-pindah-peserta",
        //{ pelatihan_id: this.state.dataxpelatihan.id_pelatihan },
        { pelatihan_id: id_pelatihan },
        this.configs,
      )
      .then((res) => {
        if (res.data.result.Status) {
          const optionx = [];
          res.data.result.Data?.forEach((elem) => {
            if (elem) {
              optionx.push({
                label: elem.nama_pelatihan,
                value: elem.id,
              });
            }
          });
          this.setState({ dataxtargetpelatihan: optionx });
          swal.close();
        } else {
          throw Error(res.data.result.Message);
        }
      })
      .catch((err) => {
        const messagex =
          typeof err == "string" ? err : err.response?.data?.result?.Message;
        swal.fire({
          title: messagex
            ? "Pelatihan target tidak ditemukan!"
            : "Terjadi kesalahan!",
          icon: "warning",
        });
      });
  }

  componentDidUpdate() {
    fixBootstrapDropdown();
  }
  componentDidMount() {
    moment.locale("id");
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    let segment_url = window.location.pathname.split("/");
    let urlSegmentOne = segment_url[3];
    Cookies.set(PELATIHAN_ID_KEY, urlSegmentOne);
    let data = {
      id: urlSegmentOne,
    };

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/r-detail-rekap-pendaftaran-byid",
        data,
        this.configs,
      )
      .then((res) => {
        swal.close();
        const dataxpelatihan = res.data.result.data_pelatihan[0];
        this.setState({ dataxpelatihan });

        const jadwalx = res.data.result.jadwal_detail[0];
        if (jadwalx.alur_pendaftaran == "Administrasi") {
          const col = this.state.columns.filter(
            (elem) =>
              !elem.id?.includes("test-substansi") && elem.name != "Hasil Test",
          );
          this.setState({ columns: col });
        }
        this.setState({ jadwalx });
        this.setState({
          waktuPendaftaran: jadwalx.waktu_pendaftaran.split("-"),
          waktuPelatihan: jadwalx.waktu_pelatihan.split("-"),
        });

        const statistikx = res.data.result.rekap_pendaftaran[0];
        this.setState({ statistikx });

        //handle list user
        this.handleReloadListUser(1, 10);
        this.handleReloadListUserPending(1, 10);
      })
      .catch((err) => {
        console.error(err);
        swal.fire({
          title: err.response?.data?.result?.Message ?? "Terjadi kesalahan!",
          icon: "warning",
        });
      });

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/list-status-peserta-by-pelatihan",
        { pelatihan_id: urlSegmentOne },
        this.configs,
      )
      .then((resp) => {
        const updateBulkOptions = resp?.data.result.Data.map((elem) => {
          return {
            label: elem.name,
            value: elem.id,
          };
        }).filter((elem) => elem.label.toLowerCase() != "submit");
        this.setState({
          updateBulkOptions: updateBulkOptions,
        });
      })
      .catch((err) => {
        console.log(err);
      });
    axios
      .all([
        axios.post(
          process.env.REACT_APP_BASE_API_URI + "/umum/list-status-peserta",
          {},
          this.configs,
        ),
        axios.post(
          process.env.REACT_APP_BASE_API_URI + "/umum/list-status-substansi",
          {},
          this.configs,
        ),
        axios.post(
          process.env.REACT_APP_BASE_API_URI + "/umum/list-status-administrasi",
          {},
          this.configs,
        ),
        axios.post(
          process.env.REACT_APP_BASE_API_URI +
            "/rekappendaftaran/list-status-validasi",
          {},
          this.configs,
        ),
      ])
      .then((resps) => {
        const optionsTemp = [];
        resps.forEach((resp, index) => {
          if (resp.data?.result?.Data && resp.data?.result?.Data[0]) {
            const rawdData = resp.data.result.Data.map((elem) => {
              return {
                label:
                  "name" in elem
                    ? this.capitalWord(elem.name)
                    : this.capitalWord(elem.keterangan),
                value: "name" in elem ? elem.name : elem.id,
              };
            });
            rawdData.unshift({
              label: "Pilih Semua",
              value: index == 3 ? 99 : 0,
            });
            optionsTemp.push(rawdData);
          } else {
            optionsTemp.push([]);
          }
        });
        this.setState(
          {
            filterOptions: optionsTemp,
          },
          () => {
            console.log(optionsTemp);
          },
        );
      })
      .catch((err) => {
        console.error(err);
        swal.fire({
          title: err.response?.data?.result?.Message ?? "Terjadi kesalahan!",
          icon: "warning",
        });
      });
    this.modalRef?.current.addEventListener("hidden.bs.modal", (event) => {
      this.setState({ valStatusPesertaUpdate: [], errors: {} });
    });
  }
  handleReloadListUser(page, newPerPage) {
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - length_tmp;
    }
    let segment_url = window.location.pathname.split("/");
    let urlSegmentOne = segment_url[3];
    let dataListUser = {
      id: urlSegmentOne,
      mulai: start_tmp,
      limit: this.state.newPerPage,
      tes_substansi: this.state.valStatusTestSubstansi["value"] ?? 0,
      status_berkas: this.state.valStatusBerkas["value"] ?? 0,
      status_peserta: this.state.valStatusPeserta["value"] ?? 0,
      param: this.state.searchQuery,
      sort: this.state.sort + " " + this.state.sort_val,
      sertifikasi: 0,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/rekappendaftaran/detail-peserta-paging-v2",
        dataListUser,
        this.configs,
      )
      .then((res) => {
        const datax = res.data.result.pelatihan;
        this.setState({ numberrow: start_tmp + 1 });
        this.setState(
          { datax: datax, totalRows: res.data.result.Total, loading: false },
          () => {},
        );
      })
      .catch((err) => {
        console.error(err);
        swal
          .fire({
            title: "Terjadi kesalahan",
            text:
              err.data?.response?.result?.Message ??
              "Terjadi kesalahan saat mengambil data...",
            icon: "error",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
              this.handleReloadListUser();
            }
          });
      });
  }

  handleReloadListUserPending(page, newPerPage) {
    this.setState({ pendingTableLoading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - length_tmp;
    }
    let segment_url = window.location.pathname.split("/");
    let urlSegmentOne = segment_url[3];
    let paylaod = {
      pelatihan_id: urlSegmentOne,
      mulai: start_tmp,
      limit: this.state.newPerPagePending,
      param: this.state.searchPendingQuery,
      sort: this.state.sortpending,
      sort_val: this.state.sortpending_val,
      // status: this.state.valStatusVlalidasi.value ?? 99,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/rekappendaftaran/list-file-import",
        paylaod,
        this.configs,
      )
      .then((res) => {
        const dataxpending = res.data.result.result;
        this.setState({
          dataxpending: dataxpending,
          totalPendingRow: res.data.result.Total,
        });
        this.setState({ pendingTableLoading: false });
      })
      .catch((err) => {
        console.error(err);
        swal
          .fire({
            title: "Terjadi kesalahan",
            text:
              err.data?.response?.result?.Message ??
              "Terjadi kesalahan saat mengambil data...",
            icon: "error",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
              this.handleReloadListUser();
            }
          });
      });
  }
  handlesubmitFilterAction(e) {
    e.preventDefault();
    this.setState({ loading: true });
    this.handleReloadListUser(1, 10);
  }
  handlesubmitFilterPendingAction(e) {
    e.preventDefault();
    this.setState({ pendingTableLoading: true });
    this.handleReloadListUserPending(1, 10);
  }
  handleClickBatalAction(e) {
    window.location = "/pelatihan/rekappendaftaran";
  }
  render() {
    const Checkbox = React.forwardRef(({ onClick, ...rest }, ref) => {
      return (
        <>
          <div className="form-check" style={{ backgroundColor: "" }}>
            <input
              type="checkbox"
              className="form-check-input"
              ref={ref}
              onClick={onClick}
              {...rest}
            />
            <label className="form-check-label" id="booty-check" />
          </div>
        </>
      );
    });
    return (
      <div>
        <ViewAlasanBatal
          userData={this.state.selectedUser}
          onClose={() => {
            this.setState({
              selectedUser: null,
            });
          }}
        />
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Rekap Pendaftaran
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                  data-bs-toggle="modal"
                  data-bs-target="#filter"
                >
                  <i className="bi bi-sliders"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Filter
                  </span>
                </button>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Detail Rekap Pendaftaran
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <div className="d-flex align-items-center position-relative my-1 me-2">
                            <div className="dropdown d-inline">
                              <button
                                className="btn btn-light-primary btn-active-light-primary text-primary fw-bolder btn-sm dropdown-toggle fs-7"
                                type="button"
                                data-bs-toggle="dropdown"
                                aria-expanded="false"
                              >
                                <i className="bi bi-info-circle text-primary"></i>
                                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                  Info
                                </span>
                              </button>
                              <ul
                                className="dropdown-menu"
                                style={{
                                  position: "absolute",
                                  zIndex: "999999",
                                }}
                              >
                                <li>
                                  <a
                                    href={
                                      "/pelatihan/detail-pelatihan/" +
                                      this.state.dataxpelatihan.id
                                    }
                                    title="Info"
                                    className={`dropdown-item px-5 my-1`}
                                  >
                                    <i className="bi bi-info-circle text-dark text-hover-primary me-1"></i>
                                    Detail Pelatihan
                                  </a>
                                </li>
                                <li>
                                  <a
                                    href="#"
                                    data-bs-toggle="modal"
                                    data-bs-target="#rekap-stat"
                                    title="Statistik"
                                    className={`dropdown-item px-5 my-1`}
                                  >
                                    <i className="bi bi-bar-chart text-dark text-hover-primary me-1"></i>
                                    Statistik
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="mb-7">
                          <div className="d-flex align-items-center flex-wrap py-3">
                            <div className="symbol symbol-100px symbol-lg-120px me-4">
                              {this.state.dataxpelatihan.mitra_logo == null ? (
                                <img
                                  src={`/assets/media/logos/logo-kominfo.png`}
                                  alt=""
                                  height="100px"
                                  className="symbol-label"
                                />
                              ) : (
                                <img
                                  src={
                                    process.env.REACT_APP_BASE_API_URI +
                                    "/download/get-file?path=" +
                                    this.state.dataxpelatihan.mitra_logo +
                                    "&disk=dts-storage-partnership"
                                  }
                                  height="100px"
                                  alt=""
                                  className="symbol-label"
                                />
                              )}
                            </div>
                            <div className="flex-grow-1 mb-3">
                              <div className="justify-content-between align-items-start flex-wrap pt-6">
                                <span
                                  className={`badge badge-${resolveStatusPelatihanBg(
                                    this.state.dataxpelatihan.status_pelatihan,
                                  )}`}
                                >
                                  {this.state.dataxpelatihan.status_pelatihan}
                                </span>
                                <h1 className="align-items-center text-dark fw-bolder my-1 fs-4">
                                  {`${this.state.dataxpelatihan.id_slug} - ${this.state.dataxpelatihan.pelatihan} (Batch ${this.state.dataxpelatihan.batch})`}
                                </h1>
                                <p className="text-dark fs-7 mb-0">
                                  {this.state.dataxpelatihan.akademi} -{" "}
                                  <span className="text-muted fw-semibold fs-7 mb-0">
                                    {this.state.dataxpelatihan.tema}
                                  </span>
                                </p>
                              </div>
                            </div>
                          </div>
                          <div className="d-flex flex-wrap mt-5 fw-bold fs-6 mb-4 pe-2">
                            <span className="d-flex align-items-center fs-7 text-dark me-5 mb-3">
                              <span className="svg-icon svg-icon-4 me-1">
                                <i className="bi bi-recycle text-dark me-1"></i>
                                {this.state.jadwalx.alur_pendaftaran}
                              </span>
                            </span>
                            <span className="d-flex align-items-center fs-7 text-dark me-5 mb-3">
                              <span className="svg-icon svg-icon-4 me-1">
                                <i className="bi bi-book text-dark me-1"></i>
                                {capitalWord(
                                  this.state.dataxpelatihan.metode_pelatihan
                                    ?.toLowerCase()
                                    .replace(" ", " - "),
                                )}
                              </span>
                            </span>
                            <span className="d-flex align-items-center fs-7 text-dark me-5 mb-3">
                              <span className="svg-icon svg-icon-4 me-1">
                                <i className="bi bi-mic text-dark me-1"></i>
                                {this.state.dataxpelatihan.penyelenggara}
                              </span>
                            </span>
                            <span className="d-flex align-items-center fs-7 text-dark me-5 mb-3">
                              <span className="svg-icon svg-icon-4 me-1">
                                <i className="bi bi-person-video3 text-dark me-1"></i>
                                {this.state.dataxpelatihan.nama_mitra
                                  ? this.state.dataxpelatihan.nama_mitra
                                  : "Swakelola"}
                              </span>
                            </span>
                            <span className="d-flex align-items-center fs-7 text-dark me-5 mb-3">
                              <span className="svg-icon svg-icon-4 me-1">
                                <i className="bi bi-geo-alt text-dark me-1"></i>
                                {this.state.dataxpelatihan.metode_pelatihan ==
                                "Online"
                                  ? this.state.dataxpelatihan.metode_pelatihan
                                  : this.state.dataxpelatihan.kab_name}
                              </span>
                            </span>
                          </div>
                        </div>
                        <div className="card card-body border rounded-3">
                          <div className="row">
                            <div className="col-12 col-md-6 col-lg-3 card rounded icon-category icon-category-sm pb-3">
                              <div className="row align-items-center">
                                <div className="col-auto px-3">
                                  <div className="icon-h-p">
                                    <i className="bi bi-calendar2-plus text-primary fa-2x"></i>
                                  </div>
                                </div>
                                <div className="col px-3">
                                  <div className="card-body p-0">
                                    <h6 className="fs-7 mb-0 text-muted line-clamp-1">
                                      Pendaftaran
                                    </h6>
                                    <h6 className="mb-0 fs-7 line-clamp-1 hover-clamp-off">
                                      {this.state.waktuPendaftaran &&
                                      this.state.waktuPendaftaran.length ==
                                        2 ? (
                                        <b>
                                          {dateRange(
                                            this.state.waktuPendaftaran[0],
                                            this.state.waktuPendaftaran[1],
                                            "DD MMM YYYY",
                                          )}
                                        </b>
                                      ) : (
                                        <b>-</b>
                                      )}
                                    </h6>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-12 col-md-6 col-lg-3 card rounded icon-category icon-category-sm pb-3">
                              <div className="row align-items-center">
                                <div className="col-auto px-3">
                                  <div className="icon-h-p">
                                    <i className="bi bi-calendar2-week text-primary fa-2x"></i>
                                  </div>
                                </div>
                                <div className="col px-3">
                                  <div className="card-body p-0">
                                    <h6 className="fs-7 mb-0 text-muted line-clamp-1">
                                      Pelatihan
                                    </h6>
                                    <h6 className="mb-0 fs-7 line-clamp-1 hover-clamp-off">
                                      {this.state.waktuPelatihan &&
                                      this.state.waktuPelatihan.length == 2 ? (
                                        <b>
                                          {dateRange(
                                            this.state.waktuPelatihan[0],
                                            this.state.waktuPelatihan[1],
                                            "DD MMM YYYY",
                                          )}
                                        </b>
                                      ) : (
                                        <b>-</b>
                                      )}
                                    </h6>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-12 col-md-6 col-lg-3 card rounded icon-category icon-category-sm pb-3">
                              <div className="row align-items-center">
                                <div className="col-auto px-3">
                                  <div className="icon-h-p">
                                    <i className="bi bi-ticket text-primary fa-2x"></i>
                                  </div>
                                </div>
                                <div className="col px-3">
                                  <div className="card-body p-0">
                                    <h6 className="fs-7 mb-0 text-muted line-clamp-1">
                                      Kuota Peserta
                                    </h6>
                                    <h6 className="mb-0 fs-7 line-clamp-1 hover-clamp-off">
                                      {this.state.jadwalx.kuota_peserta} Orang
                                    </h6>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-12 col-md-6 col-lg-3 card rounded icon-category icon-category-sm pb-3">
                              <div className="row align-items-center">
                                <div className="col-auto px-3">
                                  <div className="icon-h-p">
                                    <i className="bi bi-people text-primary fa-2x"></i>
                                  </div>
                                </div>
                                <div className="col px-3">
                                  <div className="card-body p-0">
                                    <h6 className="fs-7 mb-0 text-muted line-clamp-1">
                                      Jml. Pendaftar
                                    </h6>
                                    <h6 className="mb-0 fs-7 line-clamp-1 hover-clamp-off">
                                      {this.state.statistikx?.jml_pendaftar}{" "}
                                      Orang
                                    </h6>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-12 mt-7">
                          <div className="card-header p-0">
                            <div className="card-title">
                              <div className="d-flex align-items-center position-relative my-1 me-2">
                                <span className="svg-icon svg-icon-1 position-absolute ms-6">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <rect
                                      opacity="0.5"
                                      x="17.0365"
                                      y="15.1223"
                                      width="8.15546"
                                      height="2"
                                      rx="1"
                                      transform="rotate(45 17.0365 15.1223)"
                                      fill="currentColor"
                                    ></rect>
                                    <path
                                      d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                      fill="currentColor"
                                    ></path>
                                  </svg>
                                </span>
                                <input
                                  type="text"
                                  name="search"
                                  data-kt-user-table-filter="search"
                                  className="form-control form-control-sm form-control-solid w-250px ps-14"
                                  placeholder="Cari Peserta"
                                  onKeyPress={this.handleKeyPress}
                                  onChange={this.handleSearchEmpty}
                                />
                              </div>
                            </div>
                            <div className="card-toolbar">
                              <div className="dropdown d-inline">
                                <button
                                  className="btn btn-light fw-bolder btn-sm dropdown-toggle fs-7 me-2"
                                  type="button"
                                  data-bs-toggle="dropdown"
                                  aria-expanded="false"
                                >
                                  <i className="bi bi-gear me-1"></i>
                                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                    Kelola Data
                                  </span>
                                </button>
                                <ul
                                  className="dropdown-menu"
                                  style={{
                                    position: "absolute",
                                    zIndex: "999999",
                                  }}
                                >
                                  {this.state.dataxpelatihan.status_pelatihan?.toLowerCase() !=
                                  "selesai" /*&&
                                    this.state.dataxpelatihan.status_pelatihan?.toLowerCase() !=
                                    "pelatihan"*/ ? (
                                    <li>
                                      <Link
                                        to={`/pelatihan/import-rekappendaftaran-daftarpeserta/${this.state.dataxpelatihan.id_pelatihan}?nama_pelatihan=${this.state.dataxpelatihan.name}`}
                                      >
                                        <button className="dropdown-item px-5 my-1">
                                          <i className="bi bi-cloud-download text-dark text-hover-primary me-1"></i>
                                          Import Data
                                        </button>
                                      </Link>
                                    </li>
                                  ) : (
                                    <li>
                                      <button
                                        className="dropdown-item px-5 my-1"
                                        disabled
                                      >
                                        <i className="bi bi-cloud-download text-dark text-hover-primary me-1"></i>
                                        Import Data
                                      </button>
                                    </li>
                                  )}
                                  <li>
                                    <button
                                      name="peserta"
                                      className="dropdown-item px-5 my-1"
                                      onClick={(e) => {
                                        this.setState({
                                          jenisData:
                                            "rekap pendaftaran peserta pelatihan export",
                                          showPdpModal: true,
                                          tableToExport: "peserta",
                                          preferedExt: "csv",
                                        });
                                      }}
                                    >
                                      <i className="bi bi-filetype-csv text-dark text-hover-primary me-1"></i>
                                      Export Data (csv)
                                    </button>
                                  </li>
                                  <li>
                                    <button
                                      name="peserta"
                                      className="dropdown-item px-5 my-1"
                                      onClick={(e) => {
                                        this.setState({
                                          jenisData:
                                            "rekap pendaftaran peserta pelatihan export",
                                          showPdpModal: true,
                                          tableToExport: "peserta",
                                          preferedExt: "xlsx",
                                        });
                                      }}
                                    >
                                      <i className="bi bi-file-excel text-dark text-hover-primary me-1"></i>
                                      Export Data (xlsx)
                                    </button>
                                  </li>
                                  <li>
                                    {this.state.dataxpelatihan.input_nilai ==
                                      1 && (
                                      <Link
                                        to={`/pelatihan/import-rekappendaftaran-score/${this.state.dataxpelatihan.id_pelatihan}?nama_pelatihan=${this.state.dataxpelatihan.name}`}
                                      >
                                        <button className="dropdown-item px-5 my-1">
                                          <i className="bi bi-journal-check text-dark text-hover-primary me-1"></i>
                                          Upload Nilai
                                        </button>
                                      </Link>
                                    )}
                                  </li>
                                </ul>
                              </div>

                              <button
                                className="btn btn-sm btn-flex btn-primary fw-bolder me-2"
                                href="#"
                                data-bs-toggle="modal"
                                data-bs-target="#update-bulk-modal"
                                onClick={() => {
                                  this.setState({
                                    isUpdateAll: false,
                                  });
                                }}
                              >
                                <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                                  <i className="bi bi-ui-checks me-1"></i>
                                </span>
                                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                  Update Status
                                </span>
                              </button>

                              {/*<div className="dropdown me-2">
                                <button
                                  className="btn btn-sm btn-flex btn-primary fw-bolder dropdown-toggle"
                                  href="#"
                                  role="button"
                                  id="dropdownMenuLink"
                                  data-bs-toggle="dropdown"
                                  aria-expanded="false"
                                >
                                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                                    <i className="bi bi-ui-checks me-1"></i>
                                  </span>
                                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                    Update Status
                                  </span>
                                </button>

                                <ul
                                  className="dropdown-menu"
                                  aria-labelledby="dropdownMenuLink"
                                >
                                  <li>
                                    <a
                                      href="#"
                                      className="dropdown-item  me-2"
                                      data-bs-toggle="modal"
                                      data-bs-target="#update-bulk-modal"
                                      onClick={() => {
                                        this.setState({
                                          isUpdateAll: false,
                                        });
                                      }}
                                    >
                                      Update Peserta Terpilih
                                    </a>
                                  </li>
                                  <li>
                                    <a
                                      href="#"
                                      className="dropdown-item  me-2"
                                      data-bs-toggle="modal"
                                      data-bs-target="#update-bulk-modal"
                                      onClick={() => {
                                        this.setState({
                                          isUpdateAll: true,
                                        });
                                      }}
                                    >
                                      Update Seluruh Peserta
                                    </a>
                                  </li>
                                </ul>
                              </div>*/}

                              {/* combo box pindah peserta */}

                              <a
                                href="#"
                                hidden
                                data-bs-toggle="modal"
                                data-bs-target="#pindah-peserta-modal"
                                onClick={() => {
                                  this.fetchPelatihanAvalibalePindah(
                                    Cookies.get("pelatian_id"),
                                  );
                                }}
                                ref={this.pindahModalRef}
                              ></a>
                              <div className="dropdown me-2">
                                <button
                                  className="btn btn-sm btn-flex btn-info fw-bolder dropdown-toggle"
                                  href="#"
                                  role="button"
                                  id="dropdownPindahPeserta"
                                  data-bs-toggle="dropdown"
                                  aria-expanded="false"
                                >
                                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                                    <i className="bi bi-arrow-left-right"></i>
                                  </span>
                                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                    Pindahkan Peserta
                                  </span>
                                </button>

                                <ul
                                  className="dropdown-menu"
                                  aria-labelledby="dropdownPindahPeserta"
                                >
                                  <li>
                                    <a
                                      href="#"
                                      className="dropdown-item  me-2"
                                      onClick={() => {
                                        this.setState(
                                          {
                                            isUpdateAll: false,
                                          },
                                          () => {
                                            this.pindahModalRef.current.click();
                                          },
                                        );
                                      }}
                                    >
                                      Pindahkan Peserta Terpilih
                                    </a>
                                  </li>
                                  <li>
                                    <a
                                      href="#"
                                      className="dropdown-item  me-2"
                                      onClick={() => {
                                        this.setState(
                                          {
                                            isUpdateAll: true,
                                          },
                                          () => {
                                            this.pindahModalRef.current.click();
                                          },
                                        );
                                      }}
                                    >
                                      Pindahkan Seluruh Peserta
                                    </a>
                                  </li>
                                </ul>
                              </div>

                              <button
                                className="btn btn-sm btn-flex btn-dark fw-bolder me-2"
                                href="#"
                                data-bs-toggle="modal"
                                data-bs-target="#prediksi-bulk-modal"
                                onClick={() => {
                                  this.setState({
                                    isUpdateAllPrediksi: false,
                                  });
                                }}
                              >
                                <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                                  <i className="bi bi-robot me-1"></i>
                                </span>
                                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                  Prediksi Kelulusan
                                </span>
                              </button>

                              <div
                                ref={this.modalRef}
                                className="modal fade"
                                tabIndex="-1"
                                id="prediksi-bulk-modal"
                              >
                                <div className="modal-dialog modal-lg">
                                  <div className="modal-content">
                                    <div className="modal-header">
                                      <h5 className="modal-title">
                                        <i className="bi bi-robot me-1"></i>
                                        Prediksi Kelulusan
                                      </h5>
                                      <div
                                        className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                        data-bs-dismiss="modal"
                                        aria-label="Close"
                                        ref={this.updateModelCloseButtonRef2}
                                      >
                                        <span className="svg-icon svg-icon-2x">
                                          <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                          >
                                            <rect
                                              opacity="0.5"
                                              x="6"
                                              y="17.3137"
                                              width="16"
                                              height="2"
                                              rx="1"
                                              transform="rotate(-45 6 17.3137)"
                                              fill="currentColor"
                                            />
                                            <rect
                                              x="7.41422"
                                              y="6"
                                              width="16"
                                              height="2"
                                              rx="1"
                                              transform="rotate(45 7.41422 6)"
                                              fill="currentColor"
                                            />
                                          </svg>
                                        </span>
                                      </div>
                                    </div>
                                    <form
                                      action="#"
                                      onSubmit={this.handleUpdatePrediksiBulk}
                                    >
                                      <div className="modal-body">
                                        <div className="row">
                                          {/* Ada Peserta Terpilih */}
                                          {!this.state.isUpdateAllPrediksi &&
                                            this.state.selectedTableRows
                                              .length > 0 && (
                                              <div className="col-12">
                                                <h5>
                                                  Hitung Prediksi Kelulusan
                                                  untuk peserta terpilih
                                                </h5>
                                                <div>
                                                  {this.state.selectedTableRows.map(
                                                    (elem, index) => (
                                                      <div
                                                        className="col-auto badge badge-sm bg-primary fade show m-1"
                                                        role="alert"
                                                      >
                                                        <strong>
                                                          {elem?.name}
                                                        </strong>
                                                      </div>
                                                    ),
                                                  )}
                                                </div>
                                              </div>
                                            )}
                                          {/* Tidak Ada Peserta Terpilih */}
                                          {!this.state.isUpdateAllPrediksi &&
                                            this.state.selectedTableRows
                                              .length == 0 && (
                                              <>
                                                <div
                                                  className="w-100 alert alert-danger fade show"
                                                  style={{
                                                    textAlign: "center",
                                                  }}
                                                  role="alert"
                                                >
                                                  <strong>
                                                    Belum ada peserta terpilih!
                                                  </strong>
                                                </div>
                                                <div className="form-check">
                                                  <input
                                                    type="checkbox"
                                                    className="form-check-input"
                                                    id="all-predict-check"
                                                    onChange={() => {
                                                      this.setState({
                                                        isUpdateAllPrediksi: true,
                                                      });
                                                    }}
                                                  />
                                                  <label
                                                    className="form-check-label"
                                                    htmlFor="all-predict-check"
                                                  >
                                                    Hitung Prediksi Kelulusan
                                                    untuk seluruh peserta
                                                  </label>
                                                </div>
                                                <span style={{ color: "red" }}>
                                                  {
                                                    this.state.errors[
                                                      "daftar_peserta"
                                                    ]
                                                  }
                                                </span>
                                              </>
                                            )}
                                          {this.state.isUpdateAllPrediksi &&
                                            this.state.selectedTableRows
                                              .length == 0 && (
                                              <>
                                                <div
                                                  className="w-100 alert alert-warning fade show m-1"
                                                  style={{
                                                    textAlign: "center",
                                                  }}
                                                  role="alert"
                                                >
                                                  <strong>
                                                    Update dilakukan pada semua
                                                    peserta pelatihan <br />"
                                                    {this.state.dataxpelatihan
                                                      .pelatihan +
                                                      " (Batch " +
                                                      this.state.dataxpelatihan
                                                        .batch +
                                                      ")"}
                                                    "
                                                  </strong>
                                                </div>
                                              </>
                                            )}
                                        </div>
                                        <div
                                          class="row alert alert-warning mt-5"
                                          role="alert"
                                        >
                                          <strong>
                                            <p>
                                              &#8226; Untuk alur pendaftaran
                                              Administrasi saja, Prediksi
                                              Kelulusan dapat dilakukan setelah
                                              peserta mendaftar
                                            </p>
                                            <p>
                                              &#8226; Untuk alur pendaftaran
                                              lainnya, Prediksi Kelulusan dapat
                                              dilakukan setelah peserta sudah
                                              mengerjakan Tes Substansi
                                            </p>
                                          </strong>
                                        </div>
                                      </div>
                                      <div className="modal-footer">
                                        <div className="d-flex justify-content-between">
                                          <button
                                            type="reset"
                                            className="btn btn-sm btn-light me-3"
                                            data-bs-dismiss="modal"
                                          >
                                            Batal
                                          </button>
                                          <button
                                            type="submit"
                                            className="btn btn-sm btn-primary"
                                          >
                                            Simpan
                                          </button>
                                        </div>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <div
                                className="modal fade"
                                tabIndex="-1"
                                id="rekap-stat"
                              >
                                <div className="modal-dialog modal-lg">
                                  <div className="modal-content">
                                    <div className="modal-header">
                                      <h5 className="modal-title">
                                        <span className="svg-icon svg-icon-5 me-1">
                                          <i className="bi bi-bar-chart text-black"></i>
                                        </span>{" "}
                                        Statistik
                                      </h5>
                                      <div
                                        className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                        data-bs-dismiss="modal"
                                        aria-label="Close"
                                      >
                                        <span className="svg-icon svg-icon-2x">
                                          <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                          >
                                            <rect
                                              opacity="0.5"
                                              x="6"
                                              y="17.3137"
                                              width="16"
                                              height="2"
                                              rx="1"
                                              transform="rotate(-45 6 17.3137)"
                                              fill="currentColor"
                                            />
                                            <rect
                                              x="7.41422"
                                              y="6"
                                              width="16"
                                              height="2"
                                              rx="1"
                                              transform="rotate(45 7.41422 6)"
                                              fill="currentColor"
                                            />
                                          </svg>
                                        </span>
                                      </div>
                                    </div>
                                    <div className="modal-body">
                                      <div className="row">
                                        <div className="col-12 mt-5 mb-6">
                                          <p className="text-dark fs-7 mb-0">
                                            {this.state.dataxpelatihan.akademi}
                                          </p>
                                          <h1 className="align-items-center text-dark fw-bolder my-1 fs-4">
                                            {this.state.dataxpelatihan.id_slug}{" "}
                                            -{" "}
                                            {
                                              this.state.dataxpelatihan
                                                .pelatihan
                                            }
                                          </h1>
                                          <span className="text-muted fw-semibold fs-7 mb-0">
                                            {this.state.dataxpelatihan.tema}
                                          </span>
                                        </div>
                                      </div>
                                      <div className="row">
                                        <div className="col-lg-4 mb-7 fv-row">
                                          <label className="form-label">
                                            Submit
                                          </label>
                                          <div className="d-flex">
                                            <b>
                                              {this.state.statistikx
                                                ? `${this.state.statistikx.jml_submit} Peserta`
                                                : "Belum ada data"}
                                            </b>
                                          </div>
                                        </div>
                                        <div className="col-lg-4 mb-7 fv-row">
                                          <label className="form-label">
                                            Pelatihan
                                          </label>
                                          <div className="d-flex">
                                            <b>
                                              {this.state.statistikx
                                                ? `${this.state.statistikx.jml_pelatihan} Peserta`
                                                : "Belum ada data"}
                                            </b>
                                          </div>
                                        </div>
                                        <div className="col-lg-4 mb-7 fv-row">
                                          <label className="form-label">
                                            Tidak Hadir
                                          </label>
                                          <div className="d-flex">
                                            <b>
                                              {this.state.statistikx
                                                ? `${this.state.statistikx.jml_tdkluluspelatihan_tidakhadir} Peserta`
                                                : "Belum ada data"}
                                            </b>
                                          </div>
                                        </div>
                                        <div className="col-lg-4 mb-7 fv-row">
                                          <label className="form-label">
                                            Tidak Lulus Administrasi
                                          </label>
                                          <div className="d-flex">
                                            {this.state.jadwalx
                                              .waktu_pelatihan && (
                                              <b>
                                                {this.state.statistikx
                                                  ? `${this.state.statistikx.jml_tdklulus_administrasi} Peserta`
                                                  : "Belum ada data"}
                                              </b>
                                            )}
                                          </div>
                                        </div>
                                        <div className="col-lg-4 mb-7 fv-row">
                                          <label className="form-label">
                                            Lulus Pelatihan - Kehadiran
                                          </label>
                                          <div className="d-flex">
                                            <b>
                                              {this.state.statistikx
                                                ? `${this.state.statistikx.jml_luluspelatihan_kehadiran} Peserta`
                                                : "Belum ada data"}
                                            </b>
                                          </div>
                                        </div>
                                        <div className="col-lg-4 mb-7 fv-row">
                                          <label className="form-label">
                                            Cadangan
                                          </label>
                                          <div className="d-flex">
                                            <b>
                                              {this.state.statistikx
                                                ? `${this.state.statistikx.jml_cadangan} Peserta`
                                                : "Belum ada data"}
                                            </b>
                                          </div>
                                        </div>
                                        <div className="col-lg-4 mb-7 fv-row">
                                          <label className="form-label">
                                            Test Substansi
                                          </label>
                                          <div className="d-flex">
                                            <b>
                                              {this.state.statistikx
                                                ? `${this.state.statistikx.jml_testsubstansi} Peserta`
                                                : "Belum ada data"}
                                            </b>
                                          </div>
                                        </div>
                                        <div className="col-lg-4 mb-7 fv-row">
                                          <label className="form-label">
                                            Lulus Pelatihan Nilai
                                          </label>
                                          <div className="d-flex">
                                            <b>
                                              {this.state.statistikx
                                                ? `${this.state.statistikx.jml_luluspelatihan_nilai} Peserta`
                                                : "Belum ada data"}
                                            </b>
                                          </div>
                                        </div>
                                        {/* sertifikasi status start */}
                                        <div className="col-lg-4 mb-7 fv-row">
                                          <label className="form-label">
                                            Berhak Sertifikasi
                                          </label>
                                          <div className="d-flex">
                                            <b>
                                              {this.state.statistikx
                                                ? `${this.state.statistikx.jml_berhak_sertifikasi} Peserta`
                                                : "Belum ada data"}
                                            </b>
                                          </div>
                                        </div>
                                        <div className="col-lg-4 mb-7 fv-row">
                                          <label className="form-label">
                                            Ikut Sertifikasi
                                          </label>
                                          <div className="d-flex">
                                            <b>
                                              {this.state.statistikx
                                                ? `${this.state.statistikx.jml_ikut_sertifikasi} Peserta`
                                                : "Belum ada data"}
                                            </b>
                                          </div>
                                        </div>
                                        <div className="col-lg-4 mb-7 fv-row">
                                          <label className="form-label">
                                            Lulus Sertifikasi
                                          </label>
                                          <div className="d-flex">
                                            <b>
                                              {this.state.statistikx
                                                ? `${this.state.statistikx.jml_lulus_sertifikasi} Peserta`
                                                : "Belum ada data"}
                                            </b>
                                          </div>
                                        </div>
                                        {/* sertifikasi status end */}
                                        <div className="col-lg-4 mb-7 fv-row">
                                          <label className="form-label">
                                            Mengundurkan Diri
                                          </label>
                                          <div className="d-flex">
                                            <b>
                                              {this.state.statistikx
                                                ? `${this.state.statistikx.jml_mengundurkandiri} Peserta`
                                                : "Belum ada data"}
                                            </b>
                                          </div>
                                        </div>
                                        <div className="col-lg-4 mb-7 fv-row">
                                          <label className="form-label">
                                            Tidak Lulus Tes Substansi
                                          </label>
                                          <div className="d-flex">
                                            <b>
                                              {this.state.statistikx
                                                ? `${this.state.statistikx.jml_tdklulus_testsubstansi} Peserta`
                                                : "Belum ada data"}
                                            </b>
                                          </div>
                                        </div>
                                        <div className="col-lg-4 mb-7 fv-row">
                                          <label className="form-label">
                                            Tidak Lulus Pelatihan - Kehadiran
                                          </label>
                                          <div className="d-flex">
                                            <b>
                                              {this.state.statistikx
                                                ? `${this.state.statistikx.jml_tdkluluspelatihan_kehadiran} Peserta`
                                                : "Belum ada data"}
                                            </b>
                                          </div>
                                        </div>
                                        <div className="col-lg-4 mb-7 fv-row">
                                          <label className="form-label">
                                            Pembatalan
                                          </label>
                                          <div className="d-flex">
                                            <b>
                                              {this.state.statistikx
                                                ? `${this.state.statistikx.jml_pembatalan} Peserta`
                                                : "Belum ada data"}
                                            </b>
                                          </div>
                                        </div>
                                        <div className="col-lg-4 mb-7 fv-row">
                                          <label className="form-label">
                                            Diterima
                                          </label>
                                          <div className="d-flex">
                                            <b>
                                              {this.state.statistikx
                                                ? `${this.state.statistikx.jml_diterima} Peserta`
                                                : "Belum ada data"}
                                            </b>
                                          </div>
                                        </div>
                                        <div className="col-lg-4 mb-7 fv-row">
                                          <label className="form-label">
                                            Tidak Lulus Pelatihan - Nilai
                                          </label>
                                          <div className="d-flex">
                                            <b>
                                              {this.state.statistikx
                                                ? `${this.state.statistikx.jml_tdkluluspelatihan_nilai} Peserta`
                                                : "Belum ada data"}
                                            </b>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div
                                className="modal fade"
                                tabIndex="-1"
                                id="filter"
                              >
                                <div className="modal-dialog modal-lg">
                                  <div className="modal-content">
                                    <div className="modal-header">
                                      <h5 className="modal-title">
                                        <span className="svg-icon svg-icon-5 me-1">
                                          <i className="bi bi-sliders text-black"></i>
                                        </span>
                                        Filter Rekap Pendaftar
                                      </h5>
                                      <div
                                        className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                        data-bs-dismiss="modal"
                                        aria-label="Close"
                                      >
                                        <span className="svg-icon svg-icon-2x">
                                          <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                          >
                                            <rect
                                              opacity="0.5"
                                              x="6"
                                              y="17.3137"
                                              width="16"
                                              height="2"
                                              rx="1"
                                              transform="rotate(-45 6 17.3137)"
                                              fill="currentColor"
                                            />
                                            <rect
                                              x="7.41422"
                                              y="6"
                                              width="16"
                                              height="2"
                                              rx="1"
                                              transform="rotate(45 7.41422 6)"
                                              fill="currentColor"
                                            />
                                          </svg>
                                        </span>
                                      </div>
                                    </div>
                                    <form
                                      action="#"
                                      onSubmit={this.handlesubmitFilter}
                                    >
                                      <div className="modal-body">
                                        <div className="row">
                                          <div className="col-lg-12 mb-7 fv-row">
                                            <label className="form-label">
                                              Status Tes Substansi
                                            </label>
                                            <Select
                                              name="status-test-substansi"
                                              placeholder="Silahkan pilih"
                                              value={
                                                this.state
                                                  .valStatusTestSubstansi
                                              }
                                              noOptionsMessage={({
                                                inputValue,
                                              }) =>
                                                !inputValue
                                                  ? this.state.filterOptions[1]
                                                  : "Data tidak tersedia"
                                              }
                                              className="form-select-sm form-select-solid selectpicker p-0"
                                              options={
                                                this.state.filterOptions[1]
                                              }
                                              onChange={
                                                this
                                                  .handleChangeStatusTestSubstansi
                                              }
                                            />
                                          </div>
                                          <div className="col-lg-12 mb-7 fv-row">
                                            <label className="form-label">
                                              Status Peserta
                                            </label>
                                            <Select
                                              name="status-peserta"
                                              placeholder="Silahkan pilih"
                                              value={
                                                this.state.valStatusPeserta
                                              }
                                              noOptionsMessage={() =>
                                                "Data tidak tersedia"
                                              }
                                              className="form-select-sm form-select-solid selectpicker p-0"
                                              options={
                                                this.state.filterOptions[0]
                                              }
                                              onChange={
                                                this.handleChangeStatusPeserta
                                              }
                                            />
                                          </div>
                                        </div>
                                      </div>
                                      <div className="modal-footer">
                                        <div className="d-flex justify-content-between">
                                          <button
                                            type="reset"
                                            className="btn btn-sm btn-light me-3"
                                            onClick={this.handleClickReset}
                                          >
                                            Reset
                                          </button>
                                          <button
                                            type="submit"
                                            className="btn btn-sm btn-primary"
                                            data-bs-dismiss="modal"
                                          >
                                            Apply Filter
                                          </button>
                                        </div>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <div
                                className="modal fade"
                                tabIndex="-1"
                                id="filter-pending"
                              >
                                <div className="modal-dialog modal-lg">
                                  <div className="modal-content">
                                    <div className="modal-header">
                                      <h5 className="modal-title">
                                        <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                                          <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                          >
                                            <path
                                              d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                                              fill="currentColor"
                                            />
                                          </svg>
                                        </span>
                                        Filter
                                      </h5>
                                      <div
                                        className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                        data-bs-dismiss="modal"
                                        aria-label="Close"
                                      >
                                        <span className="svg-icon svg-icon-2x">
                                          <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                          >
                                            <rect
                                              opacity="0.5"
                                              x="6"
                                              y="17.3137"
                                              width="16"
                                              height="2"
                                              rx="1"
                                              transform="rotate(-45 6 17.3137)"
                                              fill="currentColor"
                                            />
                                            <rect
                                              x="7.41422"
                                              y="6"
                                              width="16"
                                              height="2"
                                              rx="1"
                                              transform="rotate(45 7.41422 6)"
                                              fill="currentColor"
                                            />
                                          </svg>
                                        </span>
                                      </div>
                                    </div>
                                    <form
                                      action="#"
                                      onSubmit={this.handlesubmitFilterPending}
                                    >
                                      <div className="modal-body">
                                        <div className="row">
                                          <div className="col-lg-12 mb-7 fv-row">
                                            <label className="form-label">
                                              Status Import
                                            </label>
                                            <Select
                                              name="status-test-substansi"
                                              placeholder="Silahkan pilih"
                                              value={
                                                this.state.valStatusVlalidasi
                                              }
                                              noOptionsMessage={({
                                                inputValue,
                                              }) =>
                                                !inputValue
                                                  ? this.state.filterOptions[3]
                                                  : "Data tidak tersedia"
                                              }
                                              className="form-select-sm form-select-solid selectpicker p-0"
                                              options={
                                                this.state.filterOptions[3]
                                              }
                                              onChange={
                                                this.handleChangeStatusValidasi
                                              }
                                            />
                                          </div>
                                        </div>
                                      </div>
                                      <div className="modal-footer">
                                        <div className="d-flex justify-content-between">
                                          <button
                                            type="reset"
                                            className="btn btn-sm btn-danger me-3"
                                            onClick={this.handleClickReset}
                                          >
                                            Reset
                                          </button>
                                          <button
                                            type="submit"
                                            className="btn btn-sm btn-primary"
                                            data-bs-dismiss="modal"
                                          >
                                            Apply Filter
                                          </button>
                                        </div>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <div
                                ref={this.modalRef}
                                className="modal fade"
                                tabIndex="-1"
                                id="update-bulk-modal"
                              >
                                <div className="modal-dialog modal-lg">
                                  <div className="modal-content">
                                    <div className="modal-header">
                                      <h5 className="modal-title">
                                        <i className="bi bi-ui-checks text-dark me-1"></i>
                                        Update Status
                                      </h5>
                                      <div
                                        className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                        data-bs-dismiss="modal"
                                        aria-label="Close"
                                        ref={this.updateModelCloseButtonRef}
                                      >
                                        <span className="svg-icon svg-icon-2x">
                                          <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                          >
                                            <rect
                                              opacity="0.5"
                                              x="6"
                                              y="17.3137"
                                              width="16"
                                              height="2"
                                              rx="1"
                                              transform="rotate(-45 6 17.3137)"
                                              fill="currentColor"
                                            />
                                            <rect
                                              x="7.41422"
                                              y="6"
                                              width="16"
                                              height="2"
                                              rx="1"
                                              transform="rotate(45 7.41422 6)"
                                              fill="currentColor"
                                            />
                                          </svg>
                                        </span>
                                      </div>
                                    </div>
                                    <form
                                      action="#"
                                      onSubmit={
                                        this.handleSubmitUpdateStatusBulk
                                      }
                                    >
                                      <div className="modal-body">
                                        <div className="row">
                                          {!this.state.isUpdateAll &&
                                            this.state.selectedTableRows
                                              .length > 0 && (
                                              <div className="col-12">
                                                <h5>Daftar peserta terpilih</h5>
                                                <div>
                                                  {this.state.selectedTableRows.map(
                                                    (elem, index) => (
                                                      <div
                                                        className="col-auto badge badge-sm bg-primary fade show m-1"
                                                        role="alert"
                                                      >
                                                        <strong>
                                                          {elem?.name}
                                                        </strong>
                                                      </div>
                                                    ),
                                                  )}
                                                </div>
                                              </div>
                                            )}
                                          {!this.state.isUpdateAll &&
                                            this.state.selectedTableRows
                                              .length == 0 && (
                                              <>
                                                <div
                                                  className="w-100 alert alert-danger fade show m-1"
                                                  style={{
                                                    textAlign: "center",
                                                  }}
                                                  role="alert"
                                                >
                                                  <strong>
                                                    Belum ada peserta terpilih!
                                                  </strong>
                                                </div>
                                                <span style={{ color: "red" }}>
                                                  {
                                                    this.state.errors[
                                                      "daftar_peserta"
                                                    ]
                                                  }
                                                </span>
                                              </>
                                            )}
                                          {this.state.isUpdateAll && (
                                            <>
                                              <div
                                                className="w-100 alert alert-warning fade show m-1"
                                                style={{ textAlign: "center" }}
                                                role="alert"
                                              >
                                                <strong>
                                                  Update dilakukan terhadap
                                                  semua peserta pelatihan "
                                                  {
                                                    this.state.dataxpelatihan
                                                      .pelatihan
                                                  }
                                                  "
                                                </strong>
                                              </div>
                                            </>
                                          )}
                                          <div className="col-lg-12 mt-7 mb-7 fv-row">
                                            <label className="fw-semibold form-label required">
                                              Status Peserta{" "}
                                              <a
                                                href="#"
                                                data-bs-toggle="modal"
                                                data-bs-target="#filter-ket"
                                                className="ms-3 float-end text-primary fw-semibold"
                                              >
                                                <i className="bi bi-info-circle text-primary"></i>{" "}
                                                Mohon Dibaca
                                              </a>
                                            </label>
                                            <Select
                                              name="status-peserta"
                                              placeholder="Silahkan pilih"
                                              value={
                                                this.state
                                                  .valStatusPesertaUpdate
                                              }
                                              noOptionsMessage={() =>
                                                "Tidak ada data"
                                              }
                                              className="form-select-sm form-select-solid selectpicker p-0"
                                              options={
                                                this.state.updateBulkOptions
                                              }
                                              onChange={
                                                this
                                                  .handleChangeStatusPesertaUpdate
                                              }
                                            />
                                            <span style={{ color: "red" }}>
                                              {
                                                this.state.errors[
                                                  "status_peserta"
                                                ]
                                              }
                                            </span>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="modal-footer">
                                        <div className="d-flex justify-content-between">
                                          <button
                                            type="reset"
                                            className="btn btn-sm btn-light me-3"
                                            data-bs-dismiss="modal"
                                          >
                                            Batal
                                          </button>
                                          <button
                                            type="submit"
                                            className="btn btn-sm btn-primary"
                                          >
                                            Simpan
                                          </button>
                                        </div>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <div
                                className="modal fade"
                                tabIndex="-1"
                                id="filter-ket"
                              >
                                <div className="modal-dialog modal-md">
                                  <div className="modal-content">
                                    <div className="modal-header">
                                      <h5 className="modal-title">
                                        <span className="svg-icon svg-icon-5 me-1">
                                          <i className="bi bi-info-circle text-black"></i>
                                        </span>
                                        Panduan Memilih Status Pendaftar/Peserta
                                      </h5>
                                      <div
                                        className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                        data-bs-dismiss="modal"
                                        aria-label="Close"
                                      >
                                        <span className="svg-icon svg-icon-2x">
                                          <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                          >
                                            <rect
                                              opacity="0.5"
                                              x="6"
                                              y="17.3137"
                                              width="16"
                                              height="2"
                                              rx="1"
                                              transform="rotate(-45 6 17.3137)"
                                              fill="currentColor"
                                            />
                                            <rect
                                              x="7.41422"
                                              y="6"
                                              width="16"
                                              height="2"
                                              rx="1"
                                              transform="rotate(45 7.41422 6)"
                                              fill="currentColor"
                                            />
                                          </svg>
                                        </span>
                                      </div>
                                    </div>
                                    <div className="modal-body">
                                      <div className="row p-5">
                                        <p>
                                          Harap membaca keterangan status
                                          berikut. Setiap PIC Pelatihan{" "}
                                          <strong>Wajib Disiplin</strong> dalam
                                          melakukan update status peserta,
                                          <br />
                                          karena setiap status berdampak kepada
                                          siklus peserta pada aplikasi
                                        </p>
                                        <ol>
                                          <li className="mb-3">
                                            <h6 className="text-primary">
                                              Submit
                                            </h6>
                                            <span>
                                              Status default ketika Pendaftar
                                              melakukan pendaftaran pada
                                              pelatihan. Pendaftar dengan status
                                              ini <strong>tidak dapat</strong>{" "}
                                              melakukan pendaftaran ke pelatihan
                                              lain.
                                            </span>
                                          </li>
                                          <li className="mb-3">
                                            <h6 className="text-danger">
                                              Tidak Lulus Administrasi
                                            </h6>
                                            <span>
                                              Pendaftar dinyatakan Tidak Lulus
                                              Seleksi Administrasi. Setelah
                                              diubah ke status ini, Pendaftar{" "}
                                              <strong>dapat</strong> melakukan
                                              pendaftaran ke pelatihan lain.
                                            </span>
                                          </li>
                                          <li className="mb-3">
                                            <h6 className="text-primary">
                                              Tes Substansi
                                            </h6>
                                            <span>
                                              Pendaftar terpilih untuk mengikuti
                                              Tes Substansi. Pendaftar dengan
                                              status ini{" "}
                                              <strong>tidak dapat</strong>{" "}
                                              melakukan pendaftaran ke pelatihan
                                              lain.
                                            </span>
                                          </li>
                                          <li className="mb-3">
                                            <h6 className="text-danger">
                                              Tidak Lulus Tes Substansi
                                            </h6>
                                            <span>
                                              Pendaftar dinyatakan Tidak Lulus
                                              Tes Substansi. Pendaftar dengan
                                              status ini <strong>dapat</strong>{" "}
                                              melakukan pendaftaran ke pelatihan
                                              lain.
                                            </span>
                                          </li>
                                          <li className="mb-3">
                                            <h6 className="text-success">
                                              Lulus Tes Substansi
                                            </h6>
                                            <span>
                                              Pendaftar dinyatakan Lulus Tes
                                              Substansi. Pendaftar dengan status
                                              ini <strong>tidak dapat</strong>{" "}
                                              melakukan pendaftaran ke pelatihan
                                              lain.
                                            </span>
                                          </li>
                                          <li className="mb-3">
                                            <h6 className="text-success">
                                              Diterima
                                            </h6>
                                            <span>
                                              Pendaftar dinyatakan Diterima
                                              untuk mengikuti pelatihan.
                                              Pendaftar dengan status ini{" "}
                                              <strong>tidak dapat</strong>{" "}
                                              melakukan pendaftaran ke pelatihan
                                              lain.
                                            </span>
                                          </li>
                                          <li className="mb-3">
                                            <h6 className="text-success">
                                              Pelatihan
                                            </h6>
                                            <span>
                                              Peserta dinyatakan mengikuti
                                              pelatihan, selain dapat diupdate
                                              secara manual oleh PIC Pelatihan,
                                              <br />
                                              Sistem juga akan otomatis
                                              mengudpate seluruh pendaftar
                                              dengan status "Diterima" menjadi
                                              "Pelatihan " jika telah memasuki
                                              jadwal pelatihan.
                                              <br />
                                              Peserta dengan status ini{" "}
                                              <strong>tidak dapat</strong>{" "}
                                              melakukan pendaftaran ke pelatihan
                                              lain.
                                            </span>
                                          </li>
                                          <li className="mb-3">
                                            <h6 className="text-success">
                                              Lulus Pelatihan - Kehadiran
                                            </h6>
                                            <span>
                                              Peserta dinyatakan Lulus
                                              berdasrkan kehadiran (ini berlaku
                                              bagi pelatihan yang menentukan
                                              kelulusan berdasarkan kehadiran).
                                              Peserta dengan status ini{" "}
                                              <strong>dapat</strong> melakukan
                                              pendaftaran ke pelatihan lain.
                                            </span>
                                          </li>
                                          <li className="mb-3">
                                            <h6 className="text-success">
                                              Lulus Pelatihan - Nilai
                                            </h6>
                                            <span>
                                              Peserta dinyatakan Lulus
                                              berdasrkan nilai (ini berlaku bagi
                                              pelatihan yang menentukan
                                              kelulusan berdasarkan nilai).
                                              Peserta dengan status ini{" "}
                                              <strong>dapat</strong> melakukan
                                              pendaftaran ke pelatihan lain.
                                            </span>
                                          </li>
                                          <li className="mb-3">
                                            <h6 className="text-danger">
                                              Tidak Lulus Pelatihan - Nilai
                                            </h6>
                                            <span>
                                              Peserta dinyatakan Tidak Lulus
                                              berdasarkan nilai (ini berlaku
                                              bagi pelatihan yang menentukan
                                              kelulusan berdasarkan nilai).
                                              Peserta dengan status ini{" "}
                                              <strong>dapat</strong> melakukan
                                              pendaftaran ke pelatihan lain.
                                            </span>
                                          </li>
                                          <li className="mb-3">
                                            <h6 className="text-danger">
                                              Tidak Lulus Pelatihan - Kehadiran
                                            </h6>
                                            <span>
                                              Peserta dinyatakan Tidak Lulus
                                              berdasarkan kehadiran (ini berlaku
                                              bagi pelatihan yang menentukan
                                              kelulusan berdasarkan kehadiran).
                                              Peserta dengan status ini{" "}
                                              <strong>dapat</strong> melakukan
                                              pendaftaran ke pelatihan lain.
                                            </span>
                                          </li>
                                          <li className="mb-3">
                                            <h6 className="text-danger">
                                              Tidak Hadir
                                            </h6>
                                            <span>
                                              Peserta tidak mengikuti pelatihan.
                                              Peserta dengan status ini{" "}
                                              <strong>dapat</strong> melakukan
                                              pendaftaran ke pelatihan lain.
                                            </span>
                                          </li>
                                          <li className="mb-3">
                                            <h6 className="text-warning">
                                              Cadangan
                                            </h6>
                                            <span>
                                              Pendaftar dinyatakan sebagai
                                              Cadangan, ini berguna jika
                                              nantinya ybs menggantikan Peserta
                                              lain. Pendaftar dengan status
                                              Cadangan,{" "}
                                              <strong>tidak dapat</strong>{" "}
                                              mendaftar ke Pelatihan lain sampai
                                              statusnya diudpate oleh PIC
                                              Pelatihan menjadi status lain.
                                              Sistem juga akan otomatis
                                              mengudpate seluruh pendaftar
                                              dengan status "Cadangan" menjadi
                                              "Tidak Lulus Administrasi" jika
                                              jadwal pelatihan telah berakhir.
                                            </span>
                                          </li>
                                          <li className="mb-3">
                                            <h6 className="text-danger">
                                              Mengundurkan Diri
                                            </h6>
                                            <span>
                                              Peserta mengundurkan diri saat
                                              pelatihan berlangsung. Peserta
                                              dengan status ini{" "}
                                              <strong>dapat</strong> melakukan
                                              pendaftaran ke pelatihan lain.
                                            </span>
                                          </li>
                                          <li className="mb-3">
                                            <h6 className="text-danger">
                                              Pembatalan
                                            </h6>
                                            <span>
                                              Pendaftar membatalkan status
                                              Pendaftarannya. Pendaftar dapat
                                              melakukan pembatalan secara
                                              mandiri melalui akun
                                              masing-masing, selama masih pada
                                              jadwal Pendaftaran Pelatihan.
                                              Pendaftar dengan status ini{" "}
                                              <strong>dapat</strong> melakukan
                                              pendaftaran ke pelatihan lain.
                                            </span>
                                          </li>
                                        </ol>
                                      </div>
                                    </div>
                                    <div className="modal-footer">
                                      <div className="d-flex justify-content-between">
                                        <button
                                          type="reset"
                                          className="btn btn-sm btn-light me-3"
                                          data-bs-dismiss="modal"
                                          aria-label="Close"
                                        >
                                          Tutup
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              {/* pindah peserta modal */}
                              <div
                                className="modal fade"
                                tabIndex="-1"
                                id="pindah-peserta-modal"
                              >
                                <div className="modal-dialog modal-lg">
                                  <div className="modal-content">
                                    <div className="modal-header">
                                      <h5 className="modal-title">
                                        <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                                          <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                          >
                                            <path
                                              d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                                              fill="currentColor"
                                            />
                                          </svg>
                                        </span>
                                        Pindahkan peserta
                                      </h5>
                                      <div
                                        className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                        data-bs-dismiss="modal"
                                        aria-label="Close"
                                        ref={this.pindasPesertaCloseButtonRef}
                                      >
                                        <span className="svg-icon svg-icon-2x">
                                          <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                          >
                                            <rect
                                              opacity="0.5"
                                              x="6"
                                              y="17.3137"
                                              width="16"
                                              height="2"
                                              rx="1"
                                              transform="rotate(-45 6 17.3137)"
                                              fill="currentColor"
                                            />
                                            <rect
                                              x="7.41422"
                                              y="6"
                                              width="16"
                                              height="2"
                                              rx="1"
                                              transform="rotate(45 7.41422 6)"
                                              fill="currentColor"
                                            />
                                          </svg>
                                        </span>
                                      </div>
                                    </div>
                                    <form
                                      action="#"
                                      onSubmit={this.updatePesertaBulk}
                                    >
                                      <div className="modal-body">
                                        <div className="row">
                                          {!this.state.isUpdateAll &&
                                            this.state.selectedTableRows
                                              .length > 0 && (
                                              <>
                                                {this.state
                                                  .selectableRowsEligible
                                                  .length > 0 && (
                                                  <div>
                                                    <h5>
                                                      Daftar peserta terpilih
                                                    </h5>
                                                    <div className="fw-bolder">
                                                      Peserta Eligible Pindah
                                                      Pelatihan
                                                    </div>
                                                    <div className="col-12">
                                                      {this.state.selectableRowsEligible.map(
                                                        (elem, index) => (
                                                          <div
                                                            className="col-auto badge badge-sm bg-primary fade show m-1"
                                                            role="alert"
                                                          >
                                                            <strong>
                                                              {elem?.name}
                                                            </strong>
                                                          </div>
                                                        ),
                                                      )}
                                                    </div>
                                                  </div>
                                                )}
                                                {this.state
                                                  .selectableRowsIneligible
                                                  .length > 0 && (
                                                  <div className="mt-2">
                                                    <div className="fw-bolder">
                                                      <span className="fw-bolder">
                                                        Peserta Ineligible
                                                        Pindah Pelatihan
                                                      </span>
                                                    </div>
                                                    <div className="d-flex">
                                                      {this.state.selectableRowsIneligible.map(
                                                        (elem, index) => (
                                                          <span
                                                            className="alert alert-danger alert-sm fade show m-1"
                                                            role="alert"
                                                          >
                                                            <strong>
                                                              {elem?.name}
                                                            </strong>
                                                          </span>
                                                        ),
                                                      )}
                                                    </div>
                                                  </div>
                                                )}
                                              </>
                                            )}
                                          {!this.state.isUpdateAll &&
                                            this.state.selectableRowsEligible
                                              .length == 0 && (
                                              <>
                                                <span style={{ color: "red" }}>
                                                  {
                                                    this.state.errors[
                                                      "daftar_peserta_eligible"
                                                    ]
                                                  }
                                                </span>
                                              </>
                                            )}
                                          {!this.state.isUpdateAll &&
                                            this.state.selectedTableRows
                                              .length == 0 && (
                                              <>
                                                <div
                                                  className="w-100 alert alert-danger fade show m-1"
                                                  style={{
                                                    textAlign: "center",
                                                  }}
                                                  role="alert"
                                                >
                                                  <strong>
                                                    Belum ada peserta terpilih!
                                                  </strong>
                                                </div>
                                                <span style={{ color: "red" }}>
                                                  {
                                                    this.state.errors[
                                                      "daftar_peserta"
                                                    ]
                                                  }
                                                </span>
                                              </>
                                            )}
                                          {this.state.isUpdateAll && (
                                            <>
                                              <div
                                                className="w-100 alert alert-warning fade show m-1"
                                                style={{ textAlign: "center" }}
                                                role="alert"
                                              >
                                                <strong>
                                                  Update dilakukan terhadap
                                                  semua peserta pelatihan "
                                                  {
                                                    this.state.dataxpelatihan
                                                      .pelatihan
                                                  }
                                                  "
                                                </strong>
                                              </div>
                                            </>
                                          )}
                                          <div className="col-lg-12 mt-7 mb-7 fv-row">
                                            <label className="form-label">
                                              Pelatihan Target Pelatihan
                                            </label>
                                            <Select
                                              name="status-peserta"
                                              placeholder="Silahkan pilih"
                                              value={
                                                this.state.valPelatihanTarget
                                              }
                                              noOptionsMessage={() =>
                                                "Tidak ada pelatihan target."
                                              }
                                              className="form-select-sm form-select-solid selectpicker p-0"
                                              options={
                                                this.state.dataxtargetpelatihan
                                              }
                                              onChange={
                                                this.handleChangePelatihanTarget
                                              }
                                            />
                                            <span style={{ color: "red" }}>
                                              {
                                                this.state.errors[
                                                  "pelatihan_target"
                                                ]
                                              }
                                            </span>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="modal-footer">
                                        <div className="d-flex justify-content-between">
                                          <button
                                            type="reset"
                                            className="btn btn-sm btn-light me-3"
                                            data-bs-dismiss="modal"
                                          >
                                            Batal
                                          </button>
                                          <button
                                            type="submit"
                                            className="btn btn-sm btn-primary"
                                          >
                                            Simpan
                                          </button>
                                        </div>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <DataTable
                            columns={this.state.columns}
                            data={this.state.datax}
                            progressPending={this.state.loading}
                            progressComponent={this.customLoader}
                            paginationRowsPerPageOptions={
                              this.state.perPageOption
                            }
                            highlightOnHover
                            pointerOnHover
                            pagination
                            paginationServer
                            paginationTotalRows={this.state.totalRows}
                            paginationComponentOptions={{
                              selectAllRowsItem: true,
                              selectAllRowsItemText: "Semua",
                            }}
                            onChangeRowsPerPage={(
                              currentRowsPerPage,
                              currentPage,
                            ) => {
                              console.log(currentRowsPerPage, currentPage);
                              this.handlePerRowsChange(
                                currentRowsPerPage,
                                currentPage,
                                "row-change",
                              );
                            }}
                            onChangePage={(page, totalRows) => {
                              this.handlePerRowsChange(
                                page,
                                totalRows,
                                "page-change",
                              );
                            }}
                            customStyles={this.customStyles}
                            persistTableHead={true}
                            noDataComponent={
                              <div className="mt-5">Tidak Ada Data</div>
                            }
                            onSort={this.handleChangeSort}
                            selectableRows
                            selectableRowsComponent={Checkbox}
                            selectableRowDisabled={(row) =>
                              disabledStatus().includes(
                                row.status_peserta?.toLowerCase(),
                              )
                            }
                            onSelectedRowsChange={(evt) => {
                              this.setState({
                                selectedTableRows: evt.selectedRows,
                                selectableRowsIneligible:
                                  evt.selectedRows.filter(
                                    (elem) =>
                                      !this.isPindahEligible(
                                        elem.status_peserta,
                                      ),
                                  ),
                                selectableRowsEligible: evt.selectedRows.filter(
                                  (elem) =>
                                    this.isPindahEligible(elem.status_peserta),
                                ),
                              });
                            }}
                            clearSelectedRows={this.state.toggleCleared}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                {/* Data pending */}
                {(this.state.dataxpending &&
                  this.state.dataxpending[0] != null) ||
                this.state.searchPendingQuery != "" ? (
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Riwayat Import Data
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <div className="d-flex align-items-center position-relative my-1 me-2">
                            <span className="svg-icon svg-icon-1 position-absolute ms-6">
                              <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className="mh-50px"
                              >
                                <rect
                                  opacity="0.5"
                                  x="17.0365"
                                  y="15.1223"
                                  width="8.15546"
                                  height="2"
                                  rx="1"
                                  transform="rotate(45 17.0365 15.1223)"
                                  fill="currentColor"
                                ></rect>
                                <path
                                  d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                  fill="currentColor"
                                ></path>
                              </svg>
                            </span>
                            <input
                              type="text"
                              name="pendingSearch"
                              data-kt-user-table-filter="search"
                              className="form-control form-control-sm form-control-solid w-250px ps-14"
                              placeholder="Cari File"
                              onKeyPress={this.handleKeyPress}
                              onChange={this.handleSearchEmpty}
                            />
                          </div>
                          {/* <button
                            className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                            data-bs-toggle="modal"
                            data-bs-target="#filter-pending"
                          >
                            <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                              >
                                <path
                                  d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                                  fill="currentColor"
                                />
                              </svg>
                            </span>
                            Filter
                          </button>
                          <button
                            name="pending"
                            className="btn btn-sm btn-flex btn-light btn-info fw-bolder me-2"
                            onClick={() => {
                              this.setState({
                                jenisData:
                                  "rekap pendaftaran file import download",
                                showPdpModal: true,
                                tableToExport: "pending",
                              });
                            }}
                          >
                            <span className="svg-icon svg-icon-primary">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="24px"
                                height="24px"
                                viewBox="0 0 24 24"
                                version="1.1"
                              >
                                <g
                                  stroke="none"
                                  stroke-width="1"
                                  fill="none"
                                  fill-rule="evenodd"
                                >
                                  <polygon points="0 0 24 0 24 24 0 24" />
                                  <path
                                    d="M5.74714567,13.0425758 C4.09410362,11.9740356 3,10.1147886 3,8 C3,4.6862915 5.6862915,2 9,2 C11.7957591,2 14.1449096,3.91215918 14.8109738,6.5 L17.25,6.5 C19.3210678,6.5 21,8.17893219 21,10.25 C21,12.3210678 19.3210678,14 17.25,14 L8.25,14 C7.28817895,14 6.41093178,13.6378962 5.74714567,13.0425758 Z"
                                    fill="#000000"
                                    opacity="0.3"
                                  />
                                  <path
                                    d="M11.1288761,15.7336977 L11.1288761,17.6901712 L9.12120481,17.6901712 C8.84506244,17.6901712 8.62120481,17.9140288 8.62120481,18.1901712 L8.62120481,19.2134699 C8.62120481,19.4896123 8.84506244,19.7134699 9.12120481,19.7134699 L11.1288761,19.7134699 L11.1288761,21.6699434 C11.1288761,21.9460858 11.3527337,22.1699434 11.6288761,22.1699434 C11.7471877,22.1699434 11.8616664,22.1279896 11.951961,22.0515402 L15.4576222,19.0834174 C15.6683723,18.9049825 15.6945689,18.5894857 15.5161341,18.3787356 C15.4982803,18.3576485 15.4787093,18.3380775 15.4576222,18.3202237 L11.951961,15.3521009 C11.7412109,15.173666 11.4257142,15.1998627 11.2472793,15.4106128 C11.1708299,15.5009075 11.1288761,15.6153861 11.1288761,15.7336977 Z"
                                    fill="#000000"
                                    fill-rule="nonzero"
                                    transform="translate(11.959697, 18.661508) rotate(-90.000000) translate(-11.959697, -18.661508) "
                                  />
                                </g>
                              </svg>
                            </span>
                            Export
                          </button> */}
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="highlight bg-light-primary my-5">
                          <div className="col-lg-12 fv-row text-primary">
                            <h5 className="text-primary fs-5">Penting</h5>
                            <ul>
                              <li>
                                Data Calon / peserta yang telah diimport, wajib
                                untuk melengkapi data menlalui akun
                                masing-masing.
                              </li>
                              <li>
                                Peserta yang telah berhasil melengkapi data akan
                                terhitung sebagai pendaftar
                              </li>
                              <li>
                                Data Valid : Data peserta yang berhasil diimport
                              </li>
                              <li>
                                Data Tidak Valid : Data gagal diimport/terjadi
                                duplikasi/masih memiliki pelatihan berjalan
                              </li>
                            </ul>
                          </div>
                        </div>

                        <DataTable
                          columns={this.pendingColumns}
                          data={this.state.dataxpending}
                          progressPending={this.state.pendingTableLoading}
                          progressComponent={this.customLoader}
                          highlightOnHover
                          pointerOnHover
                          pagination
                          paginationServer
                          paginationTotalRows={this.state.totalPendingRow}
                          onChangeRowsPerPage={(
                            currentRowsPerPage,
                            currentPage,
                          ) => {
                            this.handlePerRowsChangePending(
                              currentRowsPerPage,
                              currentPage,
                              "row-change",
                            );
                          }}
                          onChangePage={(page, totalRows) => {
                            this.handlePerRowsChangePending(
                              page,
                              totalRows,
                              "page-change",
                            );
                          }}
                          customStyles={this.customStyles}
                          persistTableHead={true}
                          noDataComponent={
                            <div className="mt-5">Tidak Ada Data</div>
                          }
                          onSort={this.handlePendingChangeSort}
                        />
                      </div>
                    </div>
                  </div>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </div>
        </div>
        <ShowUUPdp
          file_id={this.state.dataxpelatihan.id_pelatihan}
          jenis={this.state.jenisData}
          show={this.state.showPdpModal}
          onResolve={() => {
            this.exportPesertaTableAction(
              this.state.tableToExport,
              this.state.preferedExt,
            );
            this.setState({ showPdpModal: false });
          }}
          onReject={() => {
            this.setState({ showPdpModal: false });
          }}
        />
      </div>
      // </div>
    );
  }
}
