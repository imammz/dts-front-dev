import React from "react";
import Header from "../../components/Header";
import Breadcumb from "../Breadcumb";
import Content from "../pelatihan/Pendaftaran-Content/Pendaftaran-Content";
import SideNav from "../../components/SideNav";
import Footer from "../../components/Footer";

const Pendaftaran = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default Pendaftaran;
