import React, { useState } from "react";
import Header from "../../Header";
import SideNav from "../../SideNav";
import Footer from "../../Footer";
import Swal from "sweetalert2";
import axios from "axios";
import withReactContent from "sweetalert2-react-content";
import { useParams, useNavigate } from "react-router-dom";
const PendaftaranAdd = () => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  let history = useNavigate();
  const [rows, setRows] = useState([{}]);
  const columnsArray = [
    "Nama Field",
    "Pilih Element",
    "Ukuran Font",
    "Option",
    "Data Option",
    "Req",
  ];
  const initialPendaftaranState = {
    id: null,
    judul: "",
  };
  const MySwal = withReactContent(Swal);
  //   ["name", "element", "size", "option", "data_option"]
  const columnsName = ["name"];
  const columnElement = ["element"];
  const columnSize = ["size"];
  const columnOption = ["option"];
  const columnDataOption = ["data_option"];
  const columnReq = ["req"];
  const [Pendaftaran, setPendaftaran] = useState(initialPendaftaranState);
  const handleAddRow = () => {
    const item = {};
    setRows([...rows, item]);
  };
  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setPendaftaran({ ...Pendaftaran, [name]: value });
  };
  const postResults = () => {
    console.log(rows);
  };
  const postResults_Save = () => {
    // numbers.map((number) => number * 2);
    console.log(rows.length);
    //   console.log(rows.map((rows)=>rows.name));
    let categoryOptItems = [];
    const listItems = rows.map((number) =>
      // number.forEach((item) => {
      //     categoryOptItems.push(item.name);
      // })
      categoryOptItems.push({
        judul: Pendaftaran.judul,
        name: number.name,
        element: number.element,
        size: number.size,
        option: number.option,
        data_option: number.data_option,
        required: 1,
        status: 1,
      }),
    );
    const formData = new FormData();
    formData.append("judul", Pendaftaran.judul);
    axios
      .post(process.env.REACT_APP_BASE_API_URI + "/create-frombuilder", {
        categoryOptItems,
      })
      .then(function (response) {
        MySwal.fire({
          title: <strong>Information!</strong>,
          html: <i>{response.data.result.Message}</i>,
          icon: "success",
        });
        console.log(response);
        history("/pelatihan/pendaftaran");
        // setCurrentAkademi(response.data.result.Data[0]);
      })
      .catch(function (error) {
        MySwal.fire({
          title: <strong>Information!</strong>,
          html: <i>{error.response.data.result.Message}</i>,
          icon: "warning",
        });
      });
  };
  const handleRemoveSpecificRow = (idx) => {
    const tempRows = [...rows];
    tempRows.splice(idx, 1);
    setRows(tempRows);
  };

  const updateState = (e) => {
    let prope = e.target.attributes.column.value;
    let index = e.target.attributes.index.value;
    let fieldValue = e.target.value;

    const tempRows = [...rows];
    const tempObj = rows[index];
    tempObj[prope] = fieldValue;
    tempRows[index] = tempObj;
    setRows(tempRows);
  };

  return (
    <div>
      <Header />
      <SideNav />
      <div
        className="wrapper d-flex flex-column flex-row-fluid"
        id="kt_wrapper"
        style={{ paddingTop: 8 }}
      >
        <div className="content d-flex flex-column flex-column-fluid">
          <div className="toolbar" id="kt_toolbar">
            <div
              id="kt_toolbar_container"
              className="container-fluid d-flex flex-stack"
            >
              <div
                data-kt-swapper="true"
                data-kt-swapper-mode="prepend"
                data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                className="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0"
              >
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-3 my-1">
                  {urlSegmentZero}
                </h1>
                <span className="h-20px border-gray-200 border-start mx-4" />
                <ul className="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                  <li className="breadcrumb-item text-muted">
                    <a
                      href="../../demo1/dist/index.html"
                      className="text-muted text-hover-primary"
                    >
                      {urlSegmentZero}
                    </a>
                  </li>
                  <li className="breadcrumb-item">
                    <span className="bullet bg-gray-200 w-5px h-2px" />
                  </li>
                  <li className="breadcrumb-item text-muted">
                    {urlSegmenttOne}
                  </li>
                  <li className="breadcrumb-item">
                    <span className="bullet bg-gray-200 w-5px h-2px" />
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div className="container-xl">
              <div className="card card-custom card-stretch">
                <div className="card-header">
                  <div className="card-title">
                    <div className="card card-custom gutter-b">
                      <div className="card-header">
                        <div className="card-title">
                          <h3 className="card-label">
                            Form Master Pendaftaran
                            <small></small>
                          </h3>
                          <div className="row align-items-center">
                            <div className="col text-right">
                              <button
                                type="reset"
                                className="btn btn-warning font-weight-bold btn-pill btn-sm"
                                data-bs-toggle="modal"
                                data-bs-target="#kt_modal_add_permission"
                              >
                                Harap Baca!
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="container">
                          <div className="row clearfix">
                            <div className="col-mt-12">
                              <div className="form-group">
                                <label className="fs-6 fw-bold form-label mb-5">
                                  <span className="required">Judul Form</span>
                                </label>
                                <input
                                  className="form-control form-control-sm font-size-h4"
                                  value={Pendaftaran.judul}
                                  onChange={handleInputChange}
                                  placeholder="Masukkan Judul Form"
                                  name="judul"
                                  id="judul"
                                />
                              </div>
                              <table
                                className="table table-bordered table-responsive"
                                id="tabx_logic"
                              >
                                <thead>
                                  <tr>
                                    {/* <th className="text-center"> # </th> */}
                                    {columnsArray.map((column, index) => (
                                      <th
                                        className="fs-5 form-label mb-2 "
                                        key={index}
                                      >
                                        {column}
                                      </th>
                                    ))}
                                    <th />
                                  </tr>
                                </thead>

                                <tbody>
                                  {rows.map((item, idx) => (
                                    <tr key={idx}>
                                      {/* <td>{idx + 1}</td> */}
                                      {columnsName.map((column, index) => (
                                        <td key={index}>
                                          <div className="form-group">
                                            <input
                                              type="text"
                                              column={column}
                                              value={rows[idx][column]}
                                              index={idx}
                                              placeholder="Field"
                                              className="form-control form-control-sm font-size-h4"
                                              onChange={(e) => updateState(e)}
                                            />
                                          </div>
                                        </td>
                                      ))}
                                      {columnElement.map((column, index) => (
                                        <td key={index}>
                                          <select
                                            value={rows[idx][column]}
                                            className="form-control  form-control-sm"
                                            data-placeholder="Select option"
                                            index={idx}
                                            column={column}
                                            data-allow-clear="true"
                                            id="pilih_element[]"
                                            name="pilih_element"
                                            onChange={(e) => updateState(e)}
                                          >
                                            <option value={1}>Select</option>
                                            <option value={2}>Checkbox</option>
                                            <option value={2}>Text</option>
                                            <option value={2}>Text Area</option>
                                            <option value={2}>Radio</option>
                                            <option value={2}>
                                              File Image
                                            </option>
                                            <option value={2}>
                                              Input Date
                                            </option>
                                          </select>
                                        </td>
                                      ))}
                                      {columnSize.map((column, index) => (
                                        <td key={index}>
                                          <select
                                            value={rows[idx][column]}
                                            className="form-control  form-control-sm"
                                            data-placeholder="Select option"
                                            index={idx}
                                            column={column}
                                            data-allow-clear="true"
                                            id="pilih_element[]"
                                            name="pilih_element"
                                            onChange={(e) => updateState(e)}
                                          >
                                            <option value={1}>
                                              Half-Width
                                            </option>
                                            <option value={2}>
                                              Full-Width
                                            </option>
                                          </select>
                                        </td>
                                      ))}
                                      {columnOption.map((column, index) => (
                                        <td key={index}>
                                          <select
                                            value={rows[idx][column]}
                                            className="form-control  form-control-sm"
                                            data-placeholder="Select option"
                                            index={idx}
                                            column={column}
                                            data-allow-clear="true"
                                            id="pilih_element[]"
                                            name="pilih_element"
                                            onChange={(e) => updateState(e)}
                                          >
                                            <option value={1}>Manual</option>
                                            <option value={2}>
                                              Select Reference
                                            </option>
                                          </select>
                                        </td>
                                      ))}
                                      {columnDataOption.map((column, index) => (
                                        <td key={index}>
                                          <select
                                            value={rows[idx][column]}
                                            className="form-control  form-control-sm"
                                            data-placeholder="Select option"
                                            index={idx}
                                            column={column}
                                            data-allow-clear="true"
                                            id="pilih_element[]"
                                            name="pilih_element"
                                            onChange={(e) => updateState(e)}
                                          >
                                            <option value={1}>Publish</option>
                                            <option value={2}>Unpublish</option>
                                          </select>
                                        </td>
                                      ))}
                                      {columnReq.map((column, index) => (
                                        <td key={index}>
                                          <input
                                            type="checkbox"
                                            value={rows[idx][column]}
                                            index={idx}
                                            column={column}
                                            name="Checkboxes15_1"
                                            onChange={(e) => updateState(e)}
                                          />
                                          <span></span>
                                        </td>
                                      ))}

                                      <td>
                                        <span
                                          onClick={() =>
                                            handleRemoveSpecificRow(idx)
                                          }
                                        >
                                          <i className="fas fa-trash action"></i>
                                        </span>
                                      </td>
                                    </tr>
                                  ))}
                                </tbody>
                              </table>
                              <div className="form-group">
                                <button
                                  onClick={handleAddRow}
                                  className="btn btn-light btn-text-primary btn-hover-text-success font-weight-bold btn-sm"
                                >
                                  Tambah Field
                                </button>
                                &nbsp;&nbsp;
                                <button
                                  onClick={postResults}
                                  className="btn btn-light btn-text-success btn-hover-text-success font-weight-bold btn-sm"
                                >
                                  Save Results
                                </button>
                                &nbsp;&nbsp;
                                <button
                                  onClick={postResults_Save}
                                  className="btn btn-light btn-text-primary btn-hover-text-success font-weight-bold btn-sm"
                                >
                                  Simpan
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className="modal fade"
        id="kt_modal_add_permission"
        tabIndex={-1}
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered mw-650px">
          <div className="modal-content">
            <div className="modal-header">
              <h2 className="fw-bolder"> Data yang sudah diisi peserta!</h2>
              <div
                className="btn btn-icon btn-sm btn-active-icon-primary"
                data-kt-permissions-modal-action="close"
              >
                <span className="svg-icon svg-icon-1">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={24}
                    height={24}
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <rect
                      opacity="0.5"
                      x={6}
                      y="17.3137"
                      width={16}
                      height={2}
                      rx={1}
                      transform="rotate(-45 6 17.3137)"
                      fill="black"
                    />
                    <rect
                      x="7.41422"
                      y={6}
                      width={16}
                      height={2}
                      rx={1}
                      transform="rotate(45 7.41422 6)"
                      fill="black"
                    />
                  </svg>
                </span>
              </div>
            </div>
            <div className="modal-body scroll-y mx-5 mx-xl-15 my-7">
              <form
                id="kt_modal_add_permission_form"
                className="form"
                action="#"
              >
                Data Diri Foto Profil, Nama Lengkap, Email, NIK, Jenis Kelamin,
                Nomor Handphone, Agama, Tempat dan Tanggal Lahir, Kontak Darurat
                (Nama Lengkap, Nomor Handphone, Hubungan), File KTP Alamat KTP
                Alamat Lengkap, Provinsi, Kota/Kabupaten, Kecamatan,
                Desa/Kelurahan, Kode Pos Alamat Domisili Alamat Lengkap,
                Provinsi, Kota/Kabupaten, Kecamatan, Desa/Kelurahan, Kode Pos
                Pendidikan Terakhir Jenjang Pendidikan : TK, SD, SMP, SMA : Asal
                Sekolah, Tahun Masuk, File Ijazah D3, S1, S2, S3 : Asal
                Perguruan Tinggi, Program Studi, IPK, Tahun Masuk, File Ijazah
                Pekerjaan Status Pekerjaan : Bekerja : Pekerjaan,
                Perusahaan/Institut Tempat Bekerja, Penghasilan Tidak Bekerja
                Pelajar/Mahasiswa: Sekolah/Perguruan Tinggi, Tahun Masuk
                {/*end::Actions*/}
              </form>
              {/*end::Form*/}
            </div>
            {/*end::Modal body*/}
          </div>
          {/*end::Modal content*/}
        </div>
        {/*end::Modal dialog*/}
      </div>
      <Footer />
    </div>
  );
};

export default PendaftaranAdd;
