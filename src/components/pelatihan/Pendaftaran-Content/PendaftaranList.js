import React, { useState, useEffect, useMemo, useRef } from "react";
import PendaftaranService from "../../../service/PendaftaranService";
import { useTable } from "react-table";
import { GlobalFilter, DefaultFilterForColumn } from "../../Filter";
import Header from "../../Header";
import SideNav from "../../SideNav";
import Footer from "../../Footer";
import { useHistory, useNavigate, useParams } from "react-router-dom";
import {
  useFilters,
  useGlobalFilter,
} from "react-table/dist/react-table.development";

const PendaftaranList = (props) => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  const [Pendaftaran, setPendaftaran] = useState([]);
  const [searchTitle, setSearchTitle] = useState("");
  const PendaftaranRef = useRef();
  PendaftaranRef.current = Pendaftaran;
  const history = useNavigate();
  useEffect(() => {
    retrievePendaftaran();
  }, []);

  const onChangeSearchTitle = (e) => {
    console.log(e);
    const searchTitle = e.target.value;
    setSearchTitle(searchTitle);
  };
  const retrievePendaftaran = () => {
    PendaftaranService.getAll()
      .then((response) => {
        console.log(response.data.result.Data);
        setPendaftaran(response.data.result.Data);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const refreshList = () => {
    retrievePendaftaran();
  };
  const removeAllPendaftaran = () => {
    PendaftaranService.removeAll()
      .then((response) => {
        console.log(response);
        refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const deletePendaftaran = (rowIndex) => {
    const id = PendaftaranRef.current[rowIndex].id;
    PendaftaranService.remove(id)
      .then((response) => {
        window.location.href = "";
        let newPendaftaran = [...PendaftaranRef.current];
        newPendaftaran.splice(rowIndex, 1);
        setPendaftaran(newPendaftaran);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const handleShow = (cell) => {
    console.log(cell?.row?.original);
    console.log(cell?.row?.original.name);
    history("/pelatihan/pendaftaran/content/" + cell?.row?.original.name);
  };
  const columns = useMemo(
    () => [
      {
        Header: "No",
        accessor: "",
        className: "fs-5 form-label mb-2 ",
      },
      {
        Header: "ID Pendaftaran",
        accessor: "id",
        className: "fs-5 form-label mb-2 ",
        sortable: true,
      },
      {
        Header: "Nama Form Pendaftaran",
        accessor: "judul_form",
        className: "fs-5 form-label mb-2 ",
      },
      {
        Title: "status",
        accessor: "status",
        className: "fs-5 form-label mb-2 ",
        Cell: (props) => {
          return (
            <select
              className="form-select form-select-solid"
              data-kt-select2="true"
              data-placeholder="Select option"
              data-dropdown-parent="#kt_menu_61484c5a8da38"
              data-allow-clear="true"
            >
              <option />
              <option value={1}>Publish</option>
              <option value={0}>Unpublish</option>
            </select>
          );
        },
      },
      {
        Header: "Aksi",
        accessor: "actions",
        Cell: (props) => {
          const rowIdx = props.row.nama;
          return (
            <div>
              {/* <span onClick={() => openPendaftaran(rowIdx)}>
                                <i className="far fa-edit action mr-2"></i>
                            </span> */}
              <span onClick={() => handleShow(props)}>
                <i className="far fa-edit action mr-2"></i>
              </span>
              <span onClick={() => deletePendaftaran(rowIdx)}>
                <i className="fas fa-trash action"></i>
              </span>
            </div>
          );
        },
      },
    ],
    [],
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    state,
    prepareRow,
    setGlobalFilter,
    preGlobalFilteredRows,
  } = useTable(
    {
      columns,
      data: Pendaftaran,
      defaultColumn: { Filter: DefaultFilterForColumn },
    },
    useFilters,
    useGlobalFilter,
  );
  return (
    <div>
      <Header />
      <SideNav />
      <div className="toolbar" id="kt_toolbar">
        <div
          id="kt_toolbar_container"
          className="container-fluid d-flex flex-stack my-2"
        >
          <div className="d-flex align-items-start my-2">
            <div>
              <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                <span className="me-3">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={24}
                    height={25}
                    viewBox="0 0 24 25"
                    fill="#009ef7"
                  >
                    <path
                      opacity="0.3"
                      d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                      fill="#009ef7"
                    />
                    <path
                      d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                      fill="#009ef7"
                    />
                  </svg>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Pelatihan
                  <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                </span>
                Master Form Pendaftaran
              </h1>
            </div>
          </div>

          <div className="d-flex align-items-end my-2">
            <div>
              <a
                href="/pelatihan/pendaftaran/add"
                className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                data-bs-toggle="modal"
                data-bs-target="#filter"
              >
                <i className="bi bi-sliders"></i>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Filter
                </span>
              </a>
              <button
                className="btn btn-success fw-bolder btn-sm"
                data-kt-menu-trigger="click"
                data-kt-menu-placement="bottom-end"
                data-kt-menu-flip="top-end"
              >
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Kelola Pelatihan
                </span>
                <i className="bi bi-chevron-down ms-1"></i>
              </button>
              <div
                className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-7 w-auto "
                data-kt-menu="true"
              >
                <div>
                  <div className="menu-item">
                    <a
                      href="/pelatihan/tambah-pelatihan"
                      className="menu-link px-5"
                    >
                      <i className="bi bi-plus-circle text-dark text-hover-primary me-1"></i>
                      Tambah Pelatihan
                    </a>
                  </div>
                  <div className="menu-item">
                    <a
                      href="#"
                      onClick={() => {
                        this.exportTableAction();
                      }}
                      className="menu-link px-5"
                    >
                      <i className="bi bi-cloud-upload text-dark text-hover-primary me-1"></i>
                      Export Rekap Pelatihan
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className="wrapper d-flex flex-column flex-row-fluid"
        id="kt_wrapper"
        style={{ paddingTop: 8 }}
      >
        <div
          className="content d-flex flex-column flex-column-fluid"
          id="kt_content"
        >
          <div className="toolbar" id="kt_toolbar">
            <div
              id="kt_toolbar_container"
              className="container-fluid d-flex flex-stack"
            >
              <div
                data-kt-swapper="true"
                data-kt-swapper-mode="prepend"
                data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                className="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0"
              >
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-3 my-1">
                  {urlSegmentZero}
                </h1>
                <span className="h-20px border-gray-200 border-start mx-4" />
                <ul className="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                  <li className="breadcrumb-item text-muted">
                    <a
                      href="../../demo1/dist/index.html"
                      className="text-muted text-hover-primary"
                    >
                      {urlSegmentZero}
                    </a>
                  </li>
                  <li className="breadcrumb-item">
                    <span className="bullet bg-gray-200 w-5px h-2px" />
                  </li>
                  <li className="breadcrumb-item text-muted">
                    {urlSegmenttOne}
                  </li>
                  <li className="breadcrumb-item">
                    <span className="bullet bg-gray-200 w-5px h-2px" />
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container-xxl">
              <div className="card card-custom card-stretch">
                <div className="card-header mt-12">
                  <div className="card-title">
                    <div className="card card-custom gutter-b">
                      <div className="card-header">
                        <div className="card-title">
                          <h3 className="card-label">
                            List Form Master Pendaftaran
                            <small></small>
                          </h3>
                          <div className="row align-items-center">
                            <div className="col text-right">
                              <a
                                href="/pelatihan/pendaftaran/add"
                                className="btn btn-warning font-weight-bold btn-pill btn-sm"
                              >
                                {/*begin::Svg Icon | path: icons/duotune/general/gen035.svg*/}
                                <span className="svg-icon svg-icon-3">
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width={24}
                                    height={24}
                                    viewBox="0 0 24 24"
                                    fill="none"
                                  >
                                    <rect
                                      opacity="0.3"
                                      x={2}
                                      y={2}
                                      width={20}
                                      height={20}
                                      rx={5}
                                      fill="black"
                                    />
                                    <rect
                                      x="10.8891"
                                      y="17.8033"
                                      width={12}
                                      height={2}
                                      rx={1}
                                      transform="rotate(-90 10.8891 17.8033)"
                                      fill="black"
                                    />
                                    <rect
                                      x="6.01041"
                                      y="10.9247"
                                      width={12}
                                      height={2}
                                      rx={1}
                                      fill="black"
                                    />
                                  </svg>
                                </span>
                                {/*end::Svg Icon*/}Tambah Master Pendaftaran
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="card-body pt-0 mt-4">
                        <div className="col-md-4">
                          <div className="input-group mb-3">
                            <GlobalFilter
                              preGlobalFilteredRows={preGlobalFilteredRows}
                              globalFilter={state.globalFilter}
                              setGlobalFilter={setGlobalFilter}
                            />
                          </div>
                        </div>

                        <div className="card-body pt-0">
                          <table
                            className="table table-bordered table-responsive"
                            {...getTableProps()}
                          >
                            <thead>
                              {headerGroups.map((headerGroup) => (
                                <tr {...headerGroup.getHeaderGroupProps()}>
                                  {headerGroup.headers.map((column) => (
                                    <th
                                      className="fs-5 form-label mb-2 "
                                      {...column.getHeaderProps()}
                                    >
                                      {column.render("Header")}
                                    </th>
                                  ))}
                                </tr>
                              ))}
                            </thead>
                            <tbody
                              className="fs-5 form-label mb-2 "
                              {...getTableBodyProps()}
                            >
                              {rows.map((row, i) => {
                                prepareRow(row);
                                return (
                                  <tr {...row.getRowProps()}>
                                    {row.cells.map((cell) => {
                                      return (
                                        <td {...cell.getCellProps()}>
                                          {cell.render("Cell")}
                                        </td>
                                      );
                                    })}
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                        </div>
                        {/* <div className="col-md-8">
                                        <button className="btn btn-sm btn-danger" onClick={removeAllPendaftaran}>
                                        Remove All
                                        </button>
                                    </div> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </div>

    // <Footer/>
  );
};

export default PendaftaranList;
