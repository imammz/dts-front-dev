import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import moment from "moment";
import {
  handleFormatDate,
  resolveBgStatusSubstansi,
  dateRange,
  FRONT_URL,
  resolveStatusPelatihanBg,
} from "../Pelatihan/helper";
import RenderFormElement from "../Pelatihan/RenderFormElement";
import { isExceptionRole } from "../../AksesHelper";

export default class ReviewpelatihanContent extends React.Component {
  constructor(props) {
    super(props);
    Cookies.remove("pelatian_id");
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleClickSetuju = this.handleClickSetujuAction.bind(this);
    this.handleClickTolak = this.handleClickTolakAction.bind(this);
    this.handleClickAjukanRevisi =
      this.handleClickAjukanRevisiAction.bind(this);
    this.handleChangeCatatan = this.handleChangeCatatanAction.bind(this);
    this.state = {
      fields: {},
      isLoading: false,
      errors: {},
      dataxakademi: [],
      dataxtema: [],
      dataxprov: [],
      dataxkab: [],
      dataxpenyelenggara: [],
      dataxmitra: [],
      dataxzonasi: [],
      dataxselform: [],
      dataxpelatihan: [],
      dataxlevelpelatihan: [],
      sellvlpelatihan: [],
      selakademi: [],
      seltema: [],
      selpenyelenggara: [],
      selmitra: [],
      selzonasi: [],
      selprovinsi: [],
      selkabupaten: [],
      seljudulform: [],
      valuekomitmen: "",
      valuecatatan: "",
      disabilities: [],
      selstatuspublish: [],
      flagstatuspublishedit: false,
      dataxpreview: {
        detail: [],
        utama: [],
      },
      detailsilabus: [],
      listCatatan: [],
    };
    this.formDatax = new FormData();
  }
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  optionstatus = [
    { value: "1", label: "Publish" },
    { value: "0", label: "Unpublish" },
    { value: "2", label: "Unlisted" },
  ];
  componentDidMount() {
    moment.locale("id");
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    let segment_url = window.location.pathname.split("/");
    let urlSegmenttOne = segment_url[3];
    Cookies.set("pelatian_id", urlSegmenttOne);
    let data = {
      id: urlSegmenttOne,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/pelatihanz/findpelatihan2",
        data,
        this.configs,
      )
      .then((res) => {
        const dataxpelatihan = res.data.result.Data[0];
        this.setState({ dataxpelatihan });
        this.setState({
          valuekomitmen: this.state.dataxpelatihan.komitmen_deskripsi,
        });

        let disabilities = [];
        if (this.state.dataxpelatihan.umum == "1") {
          disabilities.push("Umum");
        }
        if (this.state.dataxpelatihan.tuna_netra == "1") {
          disabilities.push("Tuna Netra");
        }
        if (this.state.dataxpelatihan.tuna_rungu == "1") {
          disabilities.push("Tuna Rungu");
        }
        if (this.state.dataxpelatihan.tuna_daksa == "1") {
          disabilities.push("Tuna Daksa");
        }
        this.setState({ disabilities });

        //level pelatihan
        const dataLvl = {};
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/levelpelatihan",
            dataLvl,
            this.configs,
          )
          .then((res) => {
            const optionx = res.data.result.Data;
            const dataxlevelpelatihan = [];
            for (let i in optionx) {
              if (optionx[i].id == this.state.dataxpelatihan.level_pelatihan) {
                this.setState({
                  sellvlpelatihan: {
                    value: optionx[i].id,
                    label: optionx[i].name,
                  },
                });
              }
            }
            optionx.map((data) =>
              dataxlevelpelatihan.push({ value: data.id, label: data.name }),
            );
            this.setState({ dataxlevelpelatihan });
          });

        //kabupaten
        const dataKab = { id: this.state.dataxpelatihan.kabupaten };
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/kabupaten",
            dataKab,
            this.configs,
          )
          .then((res) => {
            const optionx = res.data.result.Data;
            const dataxkab = [];
            for (let i in optionx) {
              if (optionx[i].id == this.state.dataxpelatihan.kabupaten) {
                this.setState({
                  selkabupaten: {
                    value: optionx[i].id,
                    label: optionx[i].name,
                  },
                });
                break;
              }
            }
            optionx.map((data) =>
              dataxkab.push({ value: data.id, label: data.name }),
            );
            this.setState({ dataxkab });
          });
        // });

        const dataFormSubmit = new FormData();
        dataFormSubmit.append(
          "id",
          this.state.dataxpelatihan.form_pendaftaran_id,
        );
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/cari-formbuilder",
            dataFormSubmit,
            this.configs,
          )
          .then((res) => {
            const statux = res.data.result.Status;
            const messagex = res.data.result.Message;
            if (statux) {
              const dataxpreview = res.data.result;
              this.setState({
                dataxpreview: {
                  detail: dataxpreview.detail,
                  utama: dataxpreview.utama,
                },
              });
            } else {
              swal
                .fire({
                  title: messagex,
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                  }
                });
            }
          });
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/detail_silabus",
            { id: dataxpelatihan.id_silabus },
            this.configs,
          )
          .then((res) => {
            const statux = res.data.result.Status;
            const messagex = res.data.result.Message;
            if (statux) {
              const detailsilabus = {
                nama: res.data.result.NamaSilabus,
                totalJp: res.data.result.Jml_jampelajaran,
                silabus: res.data.result.DataSilabus,
              };
              this.setState({ detailsilabus });
            } else {
              swal
                .fire({
                  title: messagex,
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                  }
                });
            }
          });
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/pelatihanz/viewCatRevisi",
            {
              pelatian_id: dataxpelatihan.id,
            },
            this.configs,
          )
          .then((res) => {
            const statux = res.data.result.Status;
            const messagex = res.data.result.Message;
            if (statux) {
              const listCatatan = res.data.result.Data ?? [];
              this.setState({ listCatatan });
            } else {
              swal
                .fire({
                  title: messagex,
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                  }
                });
            }
          });
      });
  }
  handleClickSetujuAction(e) {
    if (this.handleValidationUpdateStatus(e)) {
      swal
        .fire({
          title: "Apakah anda yakin menyetujui?",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
        })
        .then((result) => {
          if (result.isConfirmed) {
            this.setState({ isLoading: true });
            const dataFormSubmit = new FormData();
            dataFormSubmit.append("pelatian_id", Cookies.get("pelatian_id"));
            dataFormSubmit.append(
              "status_publish",
              this.state.valueupdatestatus,
            );

            axios
              .post(
                process.env.REACT_APP_BASE_API_URI + "/ispublish",
                dataFormSubmit,
                this.configs,
              )
              .then((res) => {
                this.setState({ isLoading: true });
                const statux = res.data.result.Status;
                const messagex = res.data.result.Message;
                if (statux) {
                  axios
                    .post(
                      process.env.REACT_APP_BASE_API_URI + "/revapprove",
                      dataFormSubmit,
                      this.configs,
                    )
                    .then((res) => {
                      this.setState({ isLoading: true });
                      const statux = res.data.result.Status;
                      const messagex = res.data.result.Data;
                      if (statux == true || statux == "true") {
                        swal
                          .fire({
                            title: messagex,
                            icon: "success",
                            confirmButtonText: "Ok",
                          })
                          .then((result) => {
                            if (result.isConfirmed) {
                              window.location = "/pelatihan/reviewpelatihan";
                            }
                          });
                      } else {
                        this.setState({ isLoading: false });
                        swal
                          .fire({
                            title: messagex,
                            icon: "error",
                            confirmButtonText: "Ok",
                          })
                          .then((result) => {});
                      }
                    })
                    .catch((error) => {
                      this.setState({ isLoading: false });
                      const messagex = error.response.data.result.Message;
                      swal
                        .fire({
                          title: messagex,
                          icon: "error",
                          confirmButtonText: "Ok",
                        })
                        .then((result) => {
                          if (result.isConfirmed) {
                          }
                        });
                    });
                } else {
                  this.setState({ isLoading: false });
                  swal
                    .fire({
                      title: messagex,
                      icon: "error",
                      confirmButtonText: "Ok",
                    })
                    .then((result) => {
                      if (result.isConfirmed) {
                      }
                    });
                }
              })
              .catch((error) => {
                this.setState({ isLoading: false });
                const messagex = error.response.data.result.Message;
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              });
          }
        });
    }
  }
  handleClickTolakAction(e) {
    swal
      .fire({
        title: "Apakah anda yakin menolak?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          const dataFormSubmit = new FormData();
          dataFormSubmit.append("pelatian_id", Cookies.get("pelatian_id"));
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/revtolak",
              dataFormSubmit,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Data;
              if (statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      window.location = "/pelatihan/reviewpelatihan";
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            })
            .catch((error) => {
              const messagex = error.response.data.result.Message;
              swal
                .fire({
                  title: messagex,
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                  }
                });
            });
        }
      });
  }
  handleValidationCatatan(e) {
    let errors = {};
    let formIsValid = true;
    if (this.state.valuecatatan == "") {
      formIsValid = false;
      errors["catatan"] = "Tidak boleh kosong";
    }
    this.setState({ errors: errors });
    return formIsValid;
  }
  handleValidationUpdateStatus(e) {
    let errors = {};
    let formIsValid = true;
    if (!this.state.flagstatuspublishedit) {
      formIsValid = false;
      errors["status_publish"] = "Tidak boleh kosong";
    }
    this.setState({ errors: errors });
    return formIsValid;
  }
  handleChangeCatatanAction(e) {
    this.setState({ valuecatatan: e.target.value });
  }
  handleClickAjukanRevisiAction(e) {
    if (this.handleValidationCatatan(e)) {
      swal
        .fire({
          title: "Apakah anda yakin merevisi?",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
        })
        .then((result) => {
          if (result.isConfirmed) {
            this.setState({ isLoading: true });
            const dataFormSubmit = new FormData();
            dataFormSubmit.append("pelatian_id", Cookies.get("pelatian_id"));
            dataFormSubmit.append("revisi", this.state.valuecatatan);
            axios
              .post(
                process.env.REACT_APP_BASE_API_URI + "/revrevisi",
                dataFormSubmit,
                this.configs,
              )
              .then((res) => {
                this.setState({ isLoading: true });
                const statux = res.data.result.Status;
                const messagex = res.data.result.Message;
                if (statux) {
                  swal
                    .fire({
                      title: messagex,
                      icon: "success",
                      confirmButtonText: "Ok",
                    })
                    .then((result) => {
                      if (result.isConfirmed) {
                        window.location = "/pelatihan/reviewpelatihan";
                      }
                    });
                } else {
                  this.setState({ isLoading: false });
                  swal
                    .fire({
                      title: messagex,
                      icon: "warning",
                      confirmButtonText: "Ok",
                    })
                    .then((result) => {
                      if (result.isConfirmed) {
                      }
                    });
                }
              })
              .catch((error) => {
                this.setState({ isLoading: false });
                const messagex = error.response.data.result.Message;
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              });
          }
        });
    }
  }
  handleClickBatalAction(e) {
    window.history.back();
  }
  handleChangeStatusPublishAction = (selectedOption) => {
    this.setState({ flagstatuspublishedit: true });
    this.setState({
      selstatuspublish: {
        value: selectedOption.value,
        label: selectedOption.label,
      },
    });
    this.setState({ valueupdatestatus: selectedOption.value });
  };
  render() {
    return (
      <div>
        <input
          type="hidden"
          name="csrf-token"
          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
        />
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Review Pelatihan
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Detail Review Pelatihan
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <div className="d-flex align-items-center position-relative my-1">
                            {isExceptionRole() ? (
                              this.state.dataxpelatihan.status_substansi !=
                              "Disetujui" ? (
                                <>
                                  <div className="dropdown d-inline">
                                    <button
                                      className="btn btn-info fw-bolder btn-sm dropdown-toggle fs-7 me-2"
                                      type="button"
                                      data-bs-toggle="dropdown"
                                      aria-expanded="false"
                                    >
                                      <i className="bi bi-pencil me-1"></i>
                                      <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                        Review
                                      </span>
                                    </button>
                                    <ul
                                      className="dropdown-menu"
                                      style={{
                                        position: "absolute",
                                        zIndex: "999999",
                                      }}
                                    >
                                      <li>
                                        <button
                                          type="button"
                                          className="dropdown-item px-5 my-1"
                                          data-bs-toggle="modal"
                                          data-bs-target="#kt_modal_status"
                                        >
                                          <span className="svg-icon svg-icon-5 text-dark me-1">
                                            <i className="bi bi-check-circle me-1"></i>
                                          </span>
                                          <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                            Approve
                                          </span>
                                        </button>
                                      </li>
                                      <li>
                                        <button
                                          type="button"
                                          className="dropdown-item px-5 my-1"
                                          data-kt-menu-dismiss="true"
                                          data-bs-toggle="modal"
                                          data-bs-target="#kt_modal_3"
                                        >
                                          <span className="svg-icon svg-icon-5 text-dark me-1">
                                            <i className="bi bi-pencil me-1"></i>
                                          </span>
                                          <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                            Revisi
                                          </span>
                                        </button>
                                      </li>
                                      <li>
                                        <button
                                          onClick={this.handleClickTolak}
                                          type="button"
                                          className="dropdown-item px-5 my-1"
                                        >
                                          <span className="svg-icon svg-icon-5 text-dark me-1">
                                            <i className="bi bi-x-circle me-1"></i>
                                          </span>
                                          <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                            Tolak
                                          </span>
                                        </button>
                                      </li>
                                    </ul>
                                  </div>
                                </>
                              ) : (
                                ""
                              )
                            ) : (
                              ""
                            )}
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="mb-7">
                          <div className="d-flex flex-wrap py-3">
                            <div className="symbol symbol-100px symbol-lg-120px me-4">
                              {this.state.dataxpelatihan.mitra_logo == null ? (
                                <img
                                  src={`/assets/media/logos/logo-kominfo.png`}
                                  alt=""
                                  height="100px"
                                  className="symbol-label"
                                />
                              ) : (
                                <img
                                  src={
                                    process.env.REACT_APP_BASE_API_URI +
                                    "/download/get-file?path=" +
                                    this.state.dataxpelatihan.mitra_logo +
                                    "&disk=dts-storage-partnership"
                                  }
                                  height="100px"
                                  alt=""
                                  className="symbol-label"
                                />
                              )}
                              {/* <div className="symbol symbol-100px symbol-lg-120px">
                              <img
                                src={this.state.dataxmitra.agency_logo}
                                alt=""
                                className="img-fluid me-4"
                              />
                            </div> */}
                            </div>
                            <div className="flex-grow-1 mb-3">
                              <div className="justify-content-between align-items-start flex-wrap pt-6">
                                <span
                                  className={`badge badge-${resolveStatusPelatihanBg(
                                    this.state.dataxpelatihan.status_pelatihan,
                                  )}`}
                                >
                                  {this.state.dataxpelatihan.status_pelatihan}
                                </span>
                                <h1 className="align-items-center text-dark fw-bolder my-1 fs-4">
                                  {this.state.dataxpelatihan.slug_pelatian_id} -{" "}
                                  {this.state.dataxpelatihan.pelatihan} (Batch{" "}
                                  {this.state.dataxpelatihan.batch})
                                </h1>
                                <p className="text-dark fs-7 mb-0">
                                  {this.state.dataxpelatihan.akademi} -{" "}
                                  <span className="text-muted fw-semibold fs-7 mb-0">
                                    {this.state.dataxpelatihan.tema}
                                  </span>
                                </p>
                              </div>
                            </div>
                          </div>
                          <div className="d-flex flex-wrap fw-bold mt-5 fs-6 mb-4 pe-2">
                            <span className="d-flex align-items-center fs-7 text-dark me-5 mb-3">
                              <span className="svg-icon svg-icon-4 me-1">
                                <i className="bi bi-recycle text-dark me-1"></i>
                                {this.state.dataxpelatihan.alur_pendaftaran}
                              </span>
                            </span>
                            <span className="d-flex align-items-center fs-7 text-dark me-5 mb-3">
                              <span className="svg-icon svg-icon-4 me-1">
                                <i className="bi bi-mic text-dark me-1"></i>
                                {this.state.dataxpelatihan.penyelenggara}
                              </span>
                            </span>
                            <span className="d-flex align-items-center fs-7 text-dark me-5 mb-3">
                              <span className="svg-icon svg-icon-4 me-1">
                                <i className="bi bi-person-video3 text-dark me-1"></i>
                                {this.state.dataxpelatihan.metode_pelaksanaan ==
                                "Mitra"
                                  ? this.state.dataxpelatihan.mitra_name
                                  : this.state.dataxpelatihan
                                      .metode_pelaksanaan}
                              </span>
                            </span>
                            <span className="d-flex align-items-center fs-7 text-dark me-5 mb-3">
                              <span className="svg-icon svg-icon-4 me-1">
                                <i className="bi bi-geo-alt text-dark me-1"></i>
                                {this.state.dataxpelatihan.metode_pelatihan ==
                                "Online"
                                  ? this.state.dataxpelatihan.metode_pelatihan
                                  : this.state.selkabupaten.label}
                              </span>
                            </span>
                          </div>
                        </div>
                        <div className="d-flex border-bottom p-0">
                          <ul
                            className="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap"
                            style={{
                              overflowX: "auto",
                              overflowY: "hidden",
                              display: "flex",
                              whiteSpace: "nowrap",
                              marginBottom: 0,
                            }}
                          >
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary active"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_1"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                  Informasi Pelatihan
                                </span>
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary false"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_2"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                  Jadwal
                                </span>
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary false"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_3"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                  Silabus
                                </span>
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary false"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_4"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                  Form Pendafatran
                                </span>
                              </a>
                            </li>
                          </ul>
                        </div>
                        <div className="tab-content" id="detail-account-tab">
                          {/* tab 1 */}
                          <div
                            className="tab-pane fade show active"
                            id="kt_tab_pane_1"
                            role="tabpanel"
                          >
                            <div className="row mt-7 pb-7">
                              <div className="row">
                                <div className="col-lg-6 my-3 fv-row">
                                  <label className="form-label">
                                    Program Digital Talent Scholarship
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {this.state.dataxpelatihan.program_dts ==
                                      1
                                        ? "Ya"
                                        : this.state.dataxpelatihan
                                              .program_dts == 0
                                          ? "Tidak"
                                          : "-"}
                                    </b>
                                  </div>
                                </div>
                                <div className="col-lg-6 my-3 fv-row">
                                  <label className="form-label">
                                    Batch Pelatihan
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {this.state.dataxpelatihan.batch ?? "-"}
                                    </b>
                                  </div>
                                </div>
                                <div className="col-lg-6 my-3 fv-row">
                                  <label className="form-label">
                                    Metode Pelatihan
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {
                                        this.state.dataxpelatihan
                                          .metode_pelatihan
                                      }
                                    </b>
                                  </div>
                                </div>
                                <div className="col-lg-6 my-3 fv-row">
                                  <label className="form-label">Zonasi</label>
                                  <div className="d-flex">
                                    <b>{this.state.dataxpelatihan.zonasi}</b>
                                  </div>
                                </div>
                                <div className="col-lg-6 my-3 fv-row">
                                  <label className="form-label">
                                    Level Pelatihan
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {
                                        this.state.dataxpelatihan
                                          .level_pelatihan
                                      }
                                    </b>
                                  </div>
                                </div>
                                <div className="col-lg-6 my-3 fv-row">
                                  <label className="form-label">
                                    Sertifikasi
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {this.state.dataxpelatihan.sertifikasi}
                                    </b>
                                  </div>
                                </div>
                                <div className="col-lg-6 my-3 fv-row">
                                  <label className="form-label">
                                    Klasifikasi Peserta
                                  </label>
                                  <div className="d-flex">
                                    <b>{this.state.disabilities.join(", ")}</b>
                                  </div>
                                </div>
                                {this.state.dataxpelatihan
                                  .pelaksana_assement_name &&
                                  this.state.dataxpelatihan
                                    .pelaksana_assement_name != "" && (
                                    <div className="col-lg-6 my-3 fv-row">
                                      <label className="form-label">
                                        Institusi / LSP yang melakukan uji
                                        kompetensi
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {
                                            this.state.dataxpelatihan
                                              .pelaksana_assement_name
                                          }
                                        </b>
                                      </div>
                                    </div>
                                  )}
                                <div className="col-lg-6 my-3 fv-row">
                                  <label className="form-label">
                                    LPJ Peserta
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {this.state.dataxpelatihan.lpj_peserta}
                                    </b>
                                  </div>
                                </div>
                                <div className="col-lg-6 my-3 fv-row">
                                  <label className="form-label">
                                    Link Pelatihan
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {this.state.dataxpelatihan.id ? (
                                        <a
                                          target="_blank"
                                          href={`${
                                            process.env
                                              .REACT_APP_BASE_USER_URI ??
                                            FRONT_URL
                                          }/pelatihan/${
                                            this.state.dataxpelatihan.id
                                          }`}
                                        >
                                          {`${
                                            process.env
                                              .REACT_APP_BASE_USER_URI ??
                                            FRONT_URL
                                          }/pelatihan/${
                                            this.state.dataxpelatihan.id
                                          }`}
                                        </a>
                                      ) : (
                                        <a href="#"> - </a>
                                      )}
                                    </b>
                                  </div>
                                </div>
                                <div className="col-lg-12 mx-3 bg-light p-6 rounded my-5 fv-row">
                                  <label className="form-label">
                                    Deskripsi Pelatihan
                                  </label>
                                  <div className="d-flex">
                                    <strong
                                      className="fs-7"
                                      dangerouslySetInnerHTML={{
                                        __html:
                                          this.state.dataxpelatihan.deskripsi,
                                      }}
                                    ></strong>
                                  </div>
                                </div>
                                <div className="col-lg-12">
                                  <div className="mb-7 fv-row">
                                    <label className="form-label">
                                      Silabus
                                    </label>
                                    <div className="d-flex">
                                      <b>
                                        <a
                                          target="_blank"
                                          href={
                                            this.state.dataxpelatihan.silabus
                                          }
                                        >
                                          Download
                                        </a>
                                      </b>
                                    </div>
                                  </div>
                                </div>

                                <div>
                                  <div className="my-8 border-top mx-0"></div>
                                </div>
                                <h2 className="fs-5 text-muted mb-3">
                                  Kuota Pelatihan
                                </h2>
                                {this.state.dataxpelatihan.kuota_pendaftar !=
                                  "999999" && (
                                  <div className="col-lg-4 my-3 fv-row">
                                    <label className="form-label">
                                      Target Kuota Pendaftar
                                    </label>
                                    <div className="d-flex">
                                      <b>
                                        {
                                          this.state.dataxpelatihan
                                            .kuota_pendaftar
                                        }{" "}
                                        Peserta
                                      </b>
                                    </div>
                                  </div>
                                )}
                                <div className="col-lg-4 my-3 fv-row">
                                  <label className="form-label">
                                    Target Kuota Peserta
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {this.state.dataxpelatihan.kuota_peserta}{" "}
                                      Peserta
                                    </b>
                                  </div>
                                </div>
                                <div className="col-lg-4 my-3 fv-row">
                                  <label className="form-label">
                                    Status Kuota
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {this.state.dataxpelatihan.status_kuota}
                                    </b>
                                  </div>
                                </div>
                                <div>
                                  <div className="my-8 border-top mx-0"></div>
                                </div>
                                <h2 className="fs-5 text-muted mb-3">
                                  Lokasi Pelatihan
                                </h2>
                                {this.state.dataxpelatihan.metode_pelaksanaan
                                  ?.toLowerCase()
                                  ?.includes("offline") ? (
                                  <>
                                    <div className="col-lg-12 my-3 fv-row">
                                      <label className="form-label">
                                        Alamat
                                      </label>
                                      <div className="d-flex">
                                        <b
                                          dangerouslySetInnerHTML={{
                                            __html:
                                              this.state.dataxpelatihan.alamat,
                                          }}
                                        ></b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 my-3 fv-row">
                                      <label className="form-label">
                                        Provinsi
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {this.state.dataxpelatihan.nm_prov}
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 my-3 fv-row">
                                      <label className="form-label">
                                        Kota / Kabupaten
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {this.state.dataxpelatihan
                                            .metode_pelatihan == "Online"
                                            ? this.state.dataxpelatihan
                                                .metode_pelatihan
                                            : this.state.selkabupaten.label}
                                        </b>
                                      </div>
                                    </div>
                                  </>
                                ) : (
                                  <div className="col-lg-6 my-3 fv-row">
                                    <div className="d-flex">
                                      <b>
                                        {
                                          this.state.dataxpelatihan
                                            .metode_pelatihan
                                        }
                                      </b>
                                    </div>
                                  </div>
                                )}
                                {this.state.dataxpelatihan.status_substansi ==
                                  "Revisi" && (
                                  <>
                                    <div>
                                      <div className="my-8 border-top mx-0"></div>
                                    </div>
                                    <h2 className="fs-5 text-muted mb-3">
                                      Catatan Revisi
                                    </h2>
                                    <div className="col-lg-12 my-3 fv-row">
                                      {/* <div className="mb-7 fv-row">
                                            <div className="d-flex">
                                              <em className="fw-bolder">
                                                "{this.state.listCatatan.revisi}
                                                "
                                              </em>
                                            </div>
                                          </div> */}
                                      {this.state.listCatatan.length > 0 ? (
                                        this.state.listCatatan.map((elem) => (
                                          <div className="d-flex align-items-center bg-light-warning rounded p-5 mb-2">
                                            <span className="svg-icon svg-icon-warning me-5">
                                              <span className="svg-icon svg-icon-1">
                                                <svg
                                                  width="24"
                                                  height="24"
                                                  viewBox="0 0 24 24"
                                                  fill="none"
                                                  xmlns="http://www.w3.org/2000/svg"
                                                  className="mh-50px"
                                                >
                                                  <path
                                                    opacity="0.3"
                                                    d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                                    fill="#ffc700"
                                                  ></path>
                                                  <path
                                                    d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                                    fill="#ffc700"
                                                  ></path>
                                                </svg>
                                              </span>
                                            </span>
                                            <div className="flex-grow-1 me-2">
                                              <a
                                                href="#"
                                                className="fw-bolder text-gray-800 fs-6"
                                              >
                                                {elem.revisi}
                                              </a>
                                              <span className="fw d-block">
                                                {handleFormatDate(
                                                  elem.created_at,
                                                )}
                                              </span>
                                            </div>
                                          </div>
                                        ))
                                      ) : (
                                        <>
                                          <em className="fw-bolder">
                                            Tidak ada catatan!
                                          </em>
                                        </>
                                      )}
                                    </div>
                                  </>
                                )}

                                {this.state.dataxpelatihan.status_substansi ==
                                  "Disetujui" && (
                                  <a
                                    href="#"
                                    onClick={this.handleClickRejectForceMajure}
                                    className="btn btn-flex btn-danger mt-8 px-6"
                                  >
                                    <i className="bi bi-stop-circle fs-2x "></i>
                                    <span className="d-flex flex-column align-items-start ms-2">
                                      <span className="fs-3 fw-bolder">
                                        BATALKAN PELATIHAN
                                      </span>
                                      <span className="fs-7">
                                        Harap perhatikan. Tombol ini hanya
                                        digunakan saat terjadi Force Majure
                                      </span>
                                    </span>
                                  </a>
                                )}
                              </div>
                            </div>
                          </div>

                          <div
                            className="tab-pane fade"
                            id="kt_tab_pane_2"
                            role="tabpanel"
                          >
                            <div className="row mt-7 pt-5 pb-7">
                              <h2 className="fs-5 text-muted mb-3">
                                Jadwal Pendaftaran dan Pelatihan
                              </h2>
                              <div className="col-lg-12 mb-7 fv-row">
                                <label className="form-label">
                                  Alur Seleksi
                                </label>
                                <div className="d-flex">
                                  <strong>
                                    {this.state.dataxpelatihan.alur_pendaftaran}
                                  </strong>
                                </div>
                              </div>
                              <div className="col-lg-6 mb-7 fv-row">
                                <label className="form-label">
                                  Pendaftaran
                                </label>
                                <div className="d-flex">
                                  <strong>
                                    {dateRange(
                                      this.state.dataxpelatihan
                                        .pendaftaran_start,
                                      this.state.dataxpelatihan.pendaftaran_end,
                                    )}
                                  </strong>
                                </div>
                                <p className="fs-7 my-0 fw-semibold text-muted">
                                  {dateRange(
                                    this.state.dataxpelatihan.pendaftaran_start,
                                    this.state.dataxpelatihan.pendaftaran_end,
                                    "DD-MM-YYYY HH:mm:ss",
                                    "HH:mm:ss",
                                  )}
                                </p>
                              </div>
                              <div className="col-lg-6 mb-7 fv-row">
                                <label className="form-label">Pelatihan</label>
                                <div className="d-flex">
                                  <strong>
                                    {dateRange(
                                      this.state.dataxpelatihan.pelatihan_start,
                                      this.state.dataxpelatihan.pelatihan_end,
                                    )}
                                  </strong>
                                </div>
                                <p className="fs-7 my-0 fw-semibold text-muted">
                                  {dateRange(
                                    this.state.dataxpelatihan.pelatihan_start,
                                    this.state.dataxpelatihan.pelatihan_end,
                                    "DD-MM-YYYY HH:mm:ss",
                                    "HH:mm:ss",
                                  )}
                                </p>
                              </div>
                              {this.state.dataxpelatihan.alur_pendaftaran &&
                                this.state.dataxpelatihan.alur_pendaftaran !==
                                  "Administrasi" && (
                                  <>
                                    <div>
                                      <div className="my-8 border-top mx-0"></div>
                                    </div>
                                    <h2 className="fs-5 text-muted mb-3">
                                      Seleksi Adminsitrasi & Tes Substansi
                                    </h2>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Seleksi Administrasi
                                      </label>
                                      <div className="d-flex">
                                        <strong>
                                          {handleFormatDate(
                                            this.state.dataxpelatihan
                                              .administrasi_mulai,
                                            this.state.dataxpelatihan
                                              .administrasi_selesai,
                                          )}
                                        </strong>
                                      </div>
                                      <p className="fs-7 my-0 fw-semibold text-muted">
                                        {dateRange(
                                          this.state.dataxpelatihan
                                            .administrasi_mulai,
                                          this.state.dataxpelatihan
                                            .administrasi_selesai,
                                          "DD-MM-YYYY HH:mm:ss",
                                          "HH:mm:ss",
                                        )}
                                      </p>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Seleksi Test Substansi
                                      </label>
                                      <div className="d-flex">
                                        <strong>
                                          {handleFormatDate(
                                            this.state.dataxpelatihan
                                              .subtansi_mulai,
                                            this.state.dataxpelatihan
                                              .subtansi_selesai,
                                          )}
                                        </strong>
                                      </div>
                                      <p className="fs-7 my-0 fw-semibold text-muted">
                                        {dateRange(
                                          this.state.dataxpelatihan
                                            .subtansi_mulai,
                                          this.state.dataxpelatihan
                                            .subtansi_selesai,
                                          "DD-MM-YYYY HH:mm:ss",
                                          "HH:mm:ss",
                                        )}
                                      </p>
                                    </div>
                                  </>
                                )}
                              {this.state.dataxpelatihan.midtest_mulai && (
                                <>
                                  <div>
                                    <div className="my-8 border-top mx-0"></div>
                                  </div>
                                  <h2 className="fs-5 text-muted mb-3">
                                    Mid Test
                                  </h2>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Tgl. Midtest
                                    </label>
                                    <div className="d-flex">
                                      <b>
                                        {dateRange(
                                          this.state.dataxpelatihan
                                            .midtest_mulai,
                                          this.state.dataxpelatihan
                                            .midtest_selesai,
                                        )}
                                      </b>
                                    </div>
                                    <p className="fs-7 my-0 fw-semibold text-muted">
                                      {dateRange(
                                        this.state.dataxpelatihan.midtest_mulai,
                                        this.state.dataxpelatihan
                                          .midtest_selesai,
                                        "DD-MM-YYYY HH:mm:ss",
                                        "HH:mm:ss",
                                      )}
                                    </p>
                                  </div>
                                </>
                              )}
                            </div>
                          </div>

                          <div
                            className="tab-pane fade"
                            id="kt_tab_pane_3"
                            role="tabpanel"
                          >
                            <div className="row mt-7 pt-5 pb-7">
                              <div className="col-lg-12 mb-7 fv-row">
                                {this.state.detailsilabus && (
                                  <div className="mb-5">
                                    <h2 className="text-muted fs-5 mb-0">
                                      {this.state.dataxpelatihan.judul_silabus}
                                    </h2>
                                    <span className="text-muted fw-semibold fs-6">
                                      {` Total : ${this.state.detailsilabus.totalJp} JP`}
                                    </span>
                                  </div>
                                )}
                                <div className="mr-3">
                                  <ul className="list-group list-group-numbered">
                                    {this.state.detailsilabus &&
                                      this.state.detailsilabus.silabus &&
                                      this.state.detailsilabus.silabus.map(
                                        (elem, index) => (
                                          <li
                                            key={
                                              "silabus_" + elem.id + "_" + index
                                            }
                                            className="list-group-item fs-7 fw-bolder d-flex justify-content-between align-items-start"
                                          >
                                            <div className="ms-2 me-auto">
                                              <div>
                                                <h6 className="fw-bolder mb-0">
                                                  {elem.materi}
                                                </h6>
                                              </div>
                                              <span className="text-muted fw-semibold fs-7">
                                                {elem.jam_pelajaran} JP
                                              </span>
                                            </div>
                                          </li>
                                        ),
                                      )}
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div
                            className="tab-pane fade"
                            id="kt_tab_pane_4"
                            role="tabpanel"
                          >
                            <div className="row mt-7 pb-7">
                              <div className="col-lg-12 mb-7 fv-row">
                                {this.state.dataxpreview.utama?.length == 1 && (
                                  <h2 className="fs-5 text-muted mt-5 mb-0">
                                    {
                                      this.state.dataxpreview.utama[0]
                                        .judul_form
                                    }
                                  </h2>
                                )}
                                <div className="mt-5">
                                  {this.state.dataxpreview.detail?.length > 0 &&
                                    this.state.dataxpreview?.detail[0] !=
                                      null && (
                                      <RenderFormElement
                                        formElements={
                                          this.state.dataxpreview.detail
                                        }
                                      />
                                    )}
                                </div>
                                <div>
                                  <div className="my-8 border-top mx-0"></div>
                                </div>
                                <h2 className="fs-5 text-muted mb-3">
                                  Komitmen
                                </h2>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <div className="d-flex">
                                    <b
                                      dangerouslySetInnerHTML={{
                                        __html:
                                          this.state.dataxpelatihan
                                            .komitmen_deskripsi,
                                      }}
                                    ></b>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="modal fade" tabindex="-1" id="kt_modal_3">
            <div className="modal-dialog">
              <div className="modal-content position-absolute">
                <div className="modal-header">
                  <h5 id="preview_title" className="modal-title">
                    Catatan Revisi
                  </h5>
                  <div
                    className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                  >
                    <span className="svg-icon svg-icon-2x"></span>
                  </div>
                </div>
                <div className="modal-body">
                  <div className="col-lg-12 mb-7 fv-row">
                    <label className="form-label required">Isi Catatan</label>
                    <textarea
                      className="form-control form-control-sm"
                      placeholder="Masukkan catatan"
                      name="catatan"
                      onChange={this.handleChangeCatatan}
                    ></textarea>
                    <span style={{ color: "red" }}>
                      {this.state.errors["catatan"]}
                    </span>
                  </div>
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-sm btn-light"
                    data-bs-dismiss="modal"
                  >
                    Batal
                  </button>
                  <button
                    onClick={this.handleClickAjukanRevisi}
                    type="button"
                    className="btn btn-primary btn-sm me-3 mr-2"
                  >
                    {this.state.isLoading ? (
                      <>
                        <span
                          className="spinner-border spinner-border-sm me-2"
                          role="status"
                          aria-hidden="true"
                        ></span>
                        <span className="sr-only">Loading...</span>Loading...
                      </>
                    ) : (
                      <>Ajukan Revisi</>
                    )}
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="modal fade" tabindex="-1" id="kt_modal_status">
            <div className="modal-dialog">
              <div className="modal-content position-absolute">
                <div className="modal-header">
                  <h5 id="preview_title" className="modal-title">
                    Update Status
                  </h5>
                  <div
                    className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                  >
                    <span className="svg-icon svg-icon-2x"></span>
                  </div>
                </div>
                <div className="modal-body">
                  <div className="col-lg-12 mb-7 fv-row">
                    <label className="form-label">Status Publish</label>
                    <Select
                      name="status_publish"
                      placeholder="Silahkan pilih"
                      noOptionsMessage={({ inputValue }) =>
                        !inputValue ? this.optionstatus : "Data tidak tersedia"
                      }
                      className="form-select-sm form-select-solid selectpicker p-0"
                      options={this.optionstatus}
                      value={this.state.selstatuspublish}
                      onChange={this.handleChangeStatusPublishAction}
                    />
                    <span style={{ color: "red" }}>
                      {this.state.errors["status_publish"]}
                    </span>
                  </div>
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-light btn-sm"
                    data-bs-dismiss="modal"
                  >
                    Batal
                  </button>
                  <button
                    onClick={this.handleClickSetuju}
                    type="button"
                    className="btn btn-primary btn-sm me-3 mr-2"
                  >
                    {this.state.isLoading ? (
                      <>
                        <span
                          className="spinner-border spinner-border-sm me-2"
                          role="status"
                          aria-hidden="true"
                        ></span>
                        <span className="sr-only">Loading...</span>Loading...
                      </>
                    ) : (
                      <>Update Status</>
                    )}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
