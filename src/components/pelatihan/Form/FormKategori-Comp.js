import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import Select from "react-select";
import {
  indonesianTimeFormat,
  indonesianDateFormat,
  capitalizeFirstLetter,
  timeDifference,
  dateDifference,
} from "../../publikasi/helper";
import { handleMinMax } from "../FormHelper";
import { useParams } from "react-router-dom";

export default class FormKategoriComp extends React.Component {
  constructor(props) {
    super(props);

    this.handleChangeKategori = this.handleChangeKategoriAction.bind(this);
    this.handleSubmit = this.handleSubmitAction.bind(this);
    this.back = this.backAction.bind(this);
  }

  state = {
    datax: [],
    kategori: "",
    isLoading: false,
    id_form_kategori: 0,
    error_kategori: "",
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    let segment_url = window.location.pathname.split("/");
    let urlSegmenttOne = segment_url[3];

    this.setState({ id_form_kategori: urlSegmenttOne });

    if (this.props.type === "update") {
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI +
            "/daftarpeserta/master-kategori-form-get",
          { id_form_kategori: urlSegmenttOne },
          this.configs,
        )
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            console.log(messagex);
            this.setState({ datax: res.data.result.Data });
            this.setState({ kategori: res.data.result.Data[0].kategori });
          }
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    }
  }

  backAction = () => {
    window.location = "/pelatihan/kategori-form";
  };

  handleChangeKategoriAction = (e) => {
    this.setState({ kategori: capitalizeFirstLetter(e.target.value) });

    if (e.target.value.length > 2) {
      this.setState({ error_kategori: "" });
    } else {
      this.setState({
        error_kategori: "Tidak boleh kosong dan terlalu singkat",
      });
    }
  };

  handleSubmitAction = () => {
    let url = "";
    let formData = {};
    this.setState({ isLoading: true });

    if (this.props.type === "insert") {
      url = "/daftarpeserta/master-kategori-form-tambah";
      formData = { kategori: this.state.kategori };
    } else if (this.props.type === "update") {
      url = "/daftarpeserta/master-kategori-form-edit";
      formData = {
        kategori: this.state.kategori,
        id_form_kategori: this.state.id_form_kategori,
      };
    }

    console.log(formData);

    if (this.state.kategori.length < 3) {
      swal
        .fire({
          title: "Nama kategori tidak boleh kosong atau terlalu singkat",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          this.setState({ datax: [] });
          this.setState({ loading: false });
          if (result.isConfirmed) {
          }
          this.setState({ isLoading: false });
        });

      this.setState({
        error_kategori: "Tidak boleh kosong dan terlalu singkat",
      });
    } else {
      axios
        .post(process.env.REACT_APP_BASE_API_URI + url, formData, this.configs)
        .then((res) => {
          this.setState({ isLoading: true });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  this.backAction();
                }
              });
          } else {
            swal
              .fire({
                title: "Tidak lolos validasi, Nama Kategori Sudah Tersedia",
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                this.setState({ datax: [] });
                this.setState({ loading: false });
                this.setState({ isLoading: false });
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    }
  };

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Form Element
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.back}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Tambah Kategori Form Element
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="col-lg-12 mb-7 fv-row">
                        <label className="form-label required">
                          Nama Kategori
                        </label>
                        <input
                          className="form-control form-control-sm"
                          name="kategori"
                          value={this.state.kategori}
                          onChange={this.handleChangeKategori}
                        />
                        <span style={{ color: "red" }}>
                          {this.state.error_kategori}
                        </span>
                      </div>
                      <div className="form-group fv-row pt-7 mb-7">
                        <div className="d-flex justify-content-center mb-7">
                          <button
                            onClick={this.back}
                            type="reset"
                            className="btn btn-md btn-light me-3"
                            data-kt-menu-dismiss="true"
                          >
                            Batal
                          </button>
                          <button
                            disabled={this.state.isLoading}
                            type="submit"
                            className="btn btn-primary btn-md"
                            onClick={this.handleSubmit}
                          >
                            {this.state.isLoading ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm me-2"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Loading...</span>
                                Loading...
                              </>
                            ) : (
                              <>
                                <i className="fa fa-paper-plane me-1"></i>Simpan
                              </>
                            )}
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
