import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import Select from "react-select";
import {
  indonesianTimeFormat,
  indonesianDateFormat,
  capitalizeFirstLetter,
  timeDifference,
  dateDifference,
} from "../../publikasi/helper";
import { handleMinMax } from "../../pelatihan/FormHelper";
import Swal from "sweetalert2";
import { cekPermition, logout } from "../../AksesHelper";

export default class FormRepoView extends React.Component {
  constructor(props) {
    super(props);

    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleClickNoFilter = this.handleClickNoFilterAction.bind(this);
    this.handleClickFilterPublish =
      this.handleClickFilterPublishAction.bind(this);
    this.handleClickFilter = this.handleClickFilterAction.bind(this);
    this.handleClickReset = this.handleClickResetAction.bind(this);
    this.handleChangeStatusFilter =
      this.handleChangeStatusFilterAction.bind(this);
    this.handleSort = this.handleSortAction.bind(this);
    this.handleClickFilterUnpublish =
      this.handleClickFilterUnpublishAction.bind(this);
    this.handleShowDelete = this.handleShowDeleteAction.bind(this);

    this.handleChangekategoriFilter =
      this.handleChangekategoriFilterAction.bind(this);
    this.handleChangeElementFilter =
      this.handleChangeElementFilterAction.bind(this);
  }

  segment_url = window.location.pathname.split("/");
  urlSegmenttOne = this.segment_url[2];
  urlSegmentZero = this.segment_url[1];

  state = {
    datax: [],
    loading: true,
    totalRows: 0,
    newPerPage: 10,

    tempLastNumber: 0,
    currentPage: 0,
    statusPublish: "",
    valStatusFilter: [],

    pratinjau: [],
    column: "id",
    sortDirection: "desc",
    searchText: "",

    kategoriList: [],
    kategoriPilih: "",
    kategoriVal: "",

    elementList: [],
    elementVal: "",
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  status = [
    { value: "", label: "Pilih Semua" },
    { value: 1, label: "Publish" },
    { value: 0, label: "Tidak Publish" },
  ];

  columns = [
    {
      name: "No",
      center: true,
      cell: (row, index) => this.state.tempLastNumber + index + 1,
      width: "70px",
    },
    {
      Header: "Nama Form Element",
      accessor: "name",
      className: "min-w-300px mw-300px",
      name: "Nama Form Element",
      sortable: true,
      grow: 6,
      cell: (row, index) => {
        return (
          <label className="d-flex flex-stack mb- mt-1">
            <span className="d-flex align-items-center me-2">
              <span className="d-flex flex-column">
                <a
                  href={"/pelatihan/element/preview/" + row.id}
                  className="text-dark"
                >
                  <span className="text-muted fs-7 fw-semibold">
                    {" "}
                    {row.element}
                  </span>
                  <h6
                    className="fw-bolder fs-7"
                    style={{
                      overflow: "hidden",
                      whiteSpace: "wrap",
                      textOverflow: "ellipses",
                    }}
                  >
                    {row.name}
                  </h6>
                </a>
              </span>
            </span>
          </label>
        );
      },
    },
    {
      Header: "Kategori Form Element",
      accessor: "kategori",
      className: "min-w-200px mw-200px",
      name: "Kategori Form Element",
      sortable: true,
      grow: 3,

      selector: (row, index) => capitalizeFirstLetter(row.kategori),
      cell: (row, index) => {
        return <span className="d-flex flex-column my-2">{row.kategori}</span>;
      },
    },

    {
      name: "Penggunaan",
      sortable: true,
      accessor: "jml_form",
      center: false,
      className: "min-w-300px mw-300px",
      grow: 3,
      selector: (row, index) => row.jml_form,
      cell: (row) => (
        <div>
          <span className="badge badge-light-primary fs-7 m-1">
            {row.jml_form}
          </span>{" "}
          Form Pendaftaran
          <br />
          <span className="badge badge-light-primary fs-7 m-1">
            {row.jml_pelatihan}
          </span>{" "}
          Pelatihan
        </div>
      ),
    },

    {
      name: "Aksi",
      center: true,
      grow: 3,
      cell: (row) => (
        <div>
          <a
            title="Lihat"
            className="btn btn-icon btn-primary btn-sm me-1"
            onClick={() => this.handleShowPreview(row)}
          >
            <i className="bi bi-eye-fill"></i>
          </a>

          {row.jml_pelatihan < 1 &&
          row.jml_form < 1 &&
          cekPermition().manage === 1 ? (
            <>
              <a
                title="Edit"
                className="btn btn-icon btn-warning btn-sm me-1"
                onClick={() => this.handleShow(row)}
              >
                <i className="bi bi-gear-fill"></i>
              </a>
              <a
                title="Hapus"
                className="btn btn-icon btn-danger btn-sm me-1"
                onClick={() => this.handleShowDelete(row)}
              >
                <i className="fas fa-trash-alt"></i>
              </a>
            </>
          ) : (
            <span></span>
          )}
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };
  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    this.handleReload();
    this.retriveKategoriForm();
    this.retriveElementForm();

    if (cekPermition().view !== 1) {
      Swal.fire({
        title: "Akses Tidak Diizinkan",
        html: "<i> Anda akan kembali kehalaman login </i>",
        icon: "warning",
      }).then(() => {
        logout();
      });
    }
  }

  handleShow = (cell) => {
    window.location = "/pelatihan/element/edit/" + cell.id;
  };

  handleShowPreview = (cell) => {
    window.location = "/pelatihan/element/preview/" + cell.id;
  };

  handleClickNoFilterAction(e) {
    this.handleClickHeaderFilterAction({
      status: "",
    });
  }

  handleClickFilterPublishAction(e) {
    this.handleClickHeaderFilterAction({
      status: 1,
    });
  }

  handleShowDeleteAction = (cell) => {
    console.log(cell);

    Swal.fire({
      title: "Apakah anda yakin ?",
      text: "Akan menghapus Element " + cell.file_name + " ? ",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#d33",
      cancelButtonColor: "#3085d6",
      confirmButtonText: "Ya, hapus!",
      cancelButtonText: "Tidak",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/hapus-repo",
            {
              id: cell.id,
            },
          )
          .then((response) => {
            console.log(response);
            if (response.status === 200) {
              Swal.fire({
                title: "<strong>Berhasil dihapus</strong>",
                html: "<i>" + cell.file_name + " berhasil dihapus </i>",
                icon: "success",
              });
              this.handleReload();
            }
          });
      }
    });
  };

  handleClickFilterUnpublishAction(e) {
    this.handleClickHeaderFilterAction({
      status: 0,
    });
  }

  handleClickResetAction() {
    this.setState({ valStatusFilter: [] });
    this.setState(
      {
        elementPilih: "",
        kategoriPilih: "",
        elementVal: "",
        kategoriVal: "",
      },
      () => {
        this.handleReload();
      },
    );
  }
  handleChangeStatusFilterAction = (selectedOption) => {
    this.setState({
      valStatusFilter: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
    this.setState({ kategoriPilih: selectedOption.value });
  };

  handleChangekategoriFilterAction = (selectedOption) => {
    this.setState({
      kategoriPilih: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  };

  handleChangeElementFilterAction = (selectedOption) => {
    this.setState({
      elementPilih: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  };

  retriveKategoriForm = () => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/daftarpeserta/list-kategori-form",
      )
      .then((response) => {
        if (response.status === 200) {
          this.setState({ kategoriList: response.data.result.Data }, () => {
            console.log(this.state.kategoriList);
          });
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  retriveElementForm = () => {
    this.setState({
      elementList: [
        { value: "select", label: "Select" },
        { value: "checkbox", label: "Checkbox" },
        { value: "text", label: "Text" },
        { value: "text-numeric", label: "Text Format Numeric" },
        { value: "text-email", label: "Text Format Email" },
        { value: "text-URL", label: "Text Format URL" },
        { value: "textarea", label: "Text Area" },
        { value: "radiogroup", label: "Radio" },
        { value: "datepicker", label: "Input Date" },
        { value: "file-doc", label: "File Upload Dokumen" },
        { value: "fileimage", label: "File Upload Gambar" },
        { value: "trigered", label: "Triger" },
      ],
    });

    console.log(this.state.elementList);
  };

  handleClickHeaderFilterAction = (payload) => {
    this.setState({ loading: true });
    this.setState({ statusPublish: payload.status });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/filter-form-repo",
        {
          status: payload.status,
          start: 0,
          length: this.state.newPerPage,
          search: this.state.searchText,
          sort_by: this.state.column,
          sort_val: this.state.sortDirection.toUpperCase(),
        },
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          const datax =
            res.data.result.Data[0] == null ? [] : res.data.result.Data;
          this.setState({ currentPage: 1 });
          this.setState({ tempLastNumber: 0 });
          this.setState({ datax });
          this.setState({ totalRows: res.data.result.TotalLength });
          this.setState({ loading: false });
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                // this.handleClickResetAction();
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      })
      .catch((error) => {
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  };

  handleClickFilterAction(e) {
    const dataForm = new FormData(e.currentTarget);
    e.preventDefault();
    this.setState({ loading: true });
    const element = dataForm.get("element");
    dataForm.append("start", 0);
    dataForm.append("length", this.state.newPerPage);
    dataForm.append("search", this.state.searchText);
    dataForm.append("sort_by", this.state.column);
    dataForm.append("sort_val", this.state.sortDirection);
    if (element == null) {
      this.handleReload();
    } else {
      dataForm.append("status", element);

      this.setState({
        kategoriVal: dataForm.get("kategori"),
        elementVal: dataForm.get("element"),
      });

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/filter-form-repo",
          dataForm,
          this.configs,
        )
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            const datax =
              res.data.result.Data[0] == null ? [] : res.data.result.Data;
            this.setState({ currentPage: 1 });
            this.setState({ tempLastNumber: 0 });
            this.setState({ datax });
            this.setState({ totalRows: res.data.result.TotalLength });
            this.setState({ loading: false });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                this.setState({ datax: [] });
                this.setState({ loading: false });
                if (result.isConfirmed) {
                  this.setState(
                    {
                      elementPilih: "",
                      kategoriPilih: "",
                    },
                    () => {
                      this.handleReload();
                    },
                  );
                }
              });
          }
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  this.setState(
                    {
                      elementPilih: "",
                      kategoriPilih: "",
                    },
                    () => {
                      this.handleReload();
                    },
                  );
                }
              });
          }
        });
    }
  }

  handleReload(page, newPerPage) {
    console.log("reload");
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });
    const dataBody = {
      start: start_tmp,
      length: length_tmp,
      status: this.state.statusPublish,
      kategori: this.state.kategoriVal,
      element: this.state.elementVal,
      search: this.state.searchText,
      sort_by: this.state.column,
      sort_val: this.state.sortDirection.toUpperCase(),
    };

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/filter-form-repo",
        dataBody,
        this.configs,
      )
      .then((res) => {
        this.setState({ loading: false });
        const datax = res.data.result.Data;
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statusx) {
          this.setState({ datax });
          this.setState({ loading: false });
          this.setState({ totalRows: res.data.result.TotalLength });
          this.setState({ currentPage: page });
        } else {
          this.setState({ statusPublish: "" });
          this.setState({ searchText: "" });
          this.handleReload();
        }
      });
  }

  handlePageChange = (page) => {
    this.setState({ loading: true });
    this.handleReload(page, this.state.newPerPage);
  };
  handlePerRowsChange = async (newPerPage, page) => {
    this.setState({ loading: true });
    this.setState({ newPerPage: newPerPage });
    this.handleReload(page, newPerPage);
  };

  handleSortAction(column, sortDirection) {
    let server_name = "";
    if (column.name == "Nama Form Element") {
      server_name = "name";
    } else if (column.name == "Kategori Form Element") {
      server_name = "kategori";
    } else if (column.name == "Penggunaan") {
      server_name = "jml_form";
    }

    this.setState(
      {
        column: server_name,
        sortDirection: sortDirection,
      },
      () => {
        this.handleReload(1, this.state.newPerPage);
      },
    );
  }

  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    this.setState({ searchText });
    if (e.key == "Enter") {
      if (searchText == "") {
        this.handleReload();
      } else {
        this.setState({ loading: true });
        let data = {
          search: searchText,
          start: 0,
          length: this.state.newPerPage,
          sort_by: this.state.column,
          sort_val: this.state.sortDirection,
          status: this.state.statusPublish,
        };
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/filter-form-repo",
            data,
            this.configs,
          )
          .then((res) => {
            const statux = res.data.result.Status;
            const messagex = res.data.result.Message;
            if (statux) {
              const datax =
                res.data.result.Data[0] == null ? [] : res.data.result.Data;
              this.setState({ currentPage: 1 });
              this.setState({ tempLastNumber: 0 });
              this.setState({ datax });
              this.setState({ totalRows: res.data.result.TotalLength });
              this.setState({ loading: false });
            } else {
              swal
                .fire({
                  title: messagex,
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  this.setState({ datax: [] });
                  this.setState({ loading: false });
                });
            }
          })
          .catch((error) => {
            let statux = error.response.data.result.Status;
            let messagex = error.response.data.result.Message;
            if (!statux) {
              swal
                .fire({
                  title: messagex,
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                  }
                });
            }
          });
      }
    }
  }

  render() {
    let rowCounter = 1;
    const styleImg = {
      width: "40px",
      height: "30px",
    };
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Master Form Element
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                  data-bs-toggle="modal"
                  data-bs-target="#filter"
                >
                  <i className="bi bi-sliders"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Filter
                  </span>
                </button>

                {cekPermition().manage === 1 ? (
                  <>
                    <a
                      href="/pelatihan/element/add/old"
                      className="btn btn-success fw-bolder btn-sm"
                    >
                      <i className="bi bi-plus-circle"></i>
                      <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                        Tambah Element
                      </span>
                    </a>
                  </>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Master Form Element
                        </h1>
                      </div>
                      <div className="card-toolbar">
                        <div className="d-flex align-items-center position-relative my-1 me-2">
                          <span className="svg-icon svg-icon-1 position-absolute ms-6">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                              className="mh-50px"
                            >
                              <rect
                                opacity="0.5"
                                x="17.0365"
                                y="15.1223"
                                width="8.15546"
                                height="2"
                                rx="1"
                                transform="rotate(45 17.0365 15.1223)"
                                fill="currentColor"
                              ></rect>
                              <path
                                d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                fill="currentColor"
                              ></path>
                            </svg>
                          </span>
                          <input
                            type="text"
                            data-kt-user-table-filter="search"
                            className="form-control form-control-sm form-control-solid w-250px ps-14"
                            placeholder="Cari Form Element"
                            onChange={(e) => {
                              if (e.currentTarget.value?.length == 0) {
                                this.setState({ searchText: "" }, () => {
                                  this.handleReload();
                                });
                              }
                            }}
                            onKeyPress={this.handleKeyPress}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="table-responsive">
                        <DataTable
                          columns={this.columns}
                          data={this.state.datax}
                          progressPending={this.state.loading}
                          highlightOnHover
                          pointerOnHover
                          pagination
                          paginationServer
                          paginationTotalRows={this.state.totalRows}
                          paginationComponentOptions={{
                            selectAllRowsItem: true,
                            selectAllRowsItemText: "Semua",
                          }}
                          paginationDefaultPage={this.state.currentPage}
                          onChangeRowsPerPage={this.handlePerRowsChange}
                          onChangePage={this.handlePageChange}
                          customStyles={this.customStyles}
                          persistTableHead={true}
                          onSort={this.handleSort}
                          noDataComponent={
                            <div className="mt-5">Tidak Ada Data</div>
                          }
                          sortServer
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Pratinjau */}
        <div className="modal fade" tabindex="-1" id="pratinjau">
          <div className="modal-dialog modal-xl">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Pratinjau Form Repository</h5>
              </div>
              <div className="modal-body">
                <div className="d-flex justify-content-start align-items-center"></div>
                <div></div> <br />
                {/* Preview widget */}
                <div className="col-12"></div>
                <div className="m-5" key={this.state.pratinjau.id}></div>
              </div>

              <div className="modal-footer">
                <button
                  className="btn btn-danger btn-sm"
                  data-bs-dismiss="modal"
                >
                  Close
                </button>
                <a
                  href={"/site-management/edit-userdts/"}
                  className="btn btn-primary btn-sm"
                >
                  Edit
                </a>
              </div>
            </div>
          </div>
        </div>
        {/* Filter */}
        <div className="modal fade" tabindex="-1" id="filter">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">
                  <span className="svg-icon svg-icon-5 me-1">
                    <i className="bi bi-sliders text-black"></i>
                  </span>
                  Filter Data Form Element
                </h5>
                <div
                  className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <span className="svg-icon svg-icon-2x">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <rect
                        opacity="0.5"
                        x="6"
                        y="17.3137"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(-45 6 17.3137)"
                        fill="currentColor"
                      />
                      <rect
                        x="7.41422"
                        y="6"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(45 7.41422 6)"
                        fill="currentColor"
                      />
                    </svg>
                  </span>
                </div>
              </div>
              <form action="#" onSubmit={this.handleClickFilter}>
                <input
                  type="hidden"
                  name="csrf-token"
                  value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                />
                <div className="modal-body">
                  <div className="col-lg-12  fv-row form-group mb-7">
                    <label className="form-label">Jenis Element</label>
                    <Select
                      id="element"
                      name="element"
                      value={this.state.elementPilih}
                      placeholder="Silahkan pilih Status"
                      noOptionsMessage={({ inputValue }) =>
                        !inputValue
                          ? this.state.elementList
                          : "Data tidak tersedia"
                      }
                      className="form-select-sm selectpicker p-0"
                      options={this.state.elementList}
                      onChange={this.handleChangeElementFilter}
                    />
                  </div>

                  <div className="col-lg-12  fv-row form-group mb-7">
                    <label className="form-label">Kategori</label>
                    <Select
                      id="kategori"
                      name="kategori"
                      value={this.state.kategoriPilih}
                      placeholder="Silahkan pilih Kategori"
                      noOptionsMessage={({ inputValue }) =>
                        !inputValue
                          ? this.state.kategoriList
                          : "Data tidak tersedia"
                      }
                      className="form-select-sm selectpicker p-0"
                      options={this.state.kategoriList}
                      onChange={this.handleChangekategoriFilter}
                    />
                  </div>
                </div>
                <div className="modal-footer">
                  <div className="d-flex justify-content-between">
                    <button
                      type="reset"
                      className="btn btn-sm btn-light me-3"
                      onClick={this.handleClickReset}
                    >
                      Reset
                    </button>
                    <button
                      type="submit"
                      className="btn btn-sm btn-primary"
                      data-bs-dismiss="modal"
                    >
                      Apply Filter
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
