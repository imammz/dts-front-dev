import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";
import Content from "../FormKategori-Comp";

const FormKategoriTambah = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content type="insert" />
      <Footer />
    </div>
  );
};

export default FormKategoriTambah;
