import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";
import Content from "../FormKategori-Comp";

const FormKategoriEdit = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content type="update" />
      <Footer />
    </div>
  );
};

export default FormKategoriEdit;
