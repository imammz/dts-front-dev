import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import {
  dateRange,
  resolveStatusPelatihanBg,
  zeroDecimalValue,
} from "./helper";
import RenderFormElement from "./RenderFormElement";
import { Link } from "react-router-dom";
import { capitalWord, handleFormatDate, FRONT_URL } from "./helper";

export default class PelatihanViewContent extends React.Component {
  constructor(props) {
    super(props);
    Cookies.remove("pelatian_id");
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleClickRejectForceMajure =
      this.handleClickRejectForceMajureAction.bind(this);
    this.state = {
      dataxakademi: [],
      dataxtema: [],
      dataxprov: [],
      dataxkab: [],
      dataxpenyelenggara: [],
      dataxmitra: [],
      dataxzonasi: [],
      dataxselform: [],
      dataxpelatihan: [],
      dataxlevelpelatihan: [],
      sellvlpelatihan: [],
      selakademi: [],
      seltema: [],
      selpenyelenggara: [],
      selmitra: [],
      selzonasi: [],
      selprovinsi: [],
      selkabupaten: [],
      seljudulform: [],
      valuekomitmen: "",
      valuecatatan: "",
      disabilities: [],
      dataxpreview: {
        detail: [],
        utama: [],
      },
      detailsilabus: {
        nama: "",
        totalJp: "",
        isiSilabus: [],
      },
      catatanterakhir: [],
    };
  }
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    let segment_url = window.location.pathname.split("/");
    let urlSegmenttOne = segment_url[3];
    Cookies.set("pelatian_id", urlSegmenttOne);
    let data = {
      id: urlSegmenttOne,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/pelatihanz/findpelatihan2",
        data,
        this.configs,
      )
      .then((res) => {
        const dataxpelatihan = res.data.result.Data[0];
        this.setState({ dataxpelatihan });
        this.setState({
          valuekomitmen: this.state.dataxpelatihan.komitmen_deskripsi,
        });

        let disabilities = [];
        if (this.state.dataxpelatihan.umum == "1") {
          disabilities.push("Umum");
        }
        if (this.state.dataxpelatihan.tuna_netra == "1") {
          disabilities.push("Tuna Netra");
        }
        if (this.state.dataxpelatihan.tuna_rungu == "1") {
          disabilities.push("Tuna Rungu");
        }
        if (this.state.dataxpelatihan.tuna_daksa == "1") {
          disabilities.push("Tuna Daksa");
        }
        this.setState({ disabilities });

        //level pelatihan
        const dataLvl = {};
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/levelpelatihan",
            dataLvl,
            this.configs,
          )
          .then((res) => {
            const optionx = res.data.result.Data;
            const dataxlevelpelatihan = [];
            for (let i in optionx) {
              if (optionx[i].id == this.state.dataxpelatihan.level_pelatihan) {
                this.setState({
                  sellvlpelatihan: {
                    value: optionx[i].id,
                    label: optionx[i].name,
                  },
                });
              }
            }
            optionx.map((data) =>
              dataxlevelpelatihan.push({ value: data.id, label: data.name }),
            );
            this.setState({ dataxlevelpelatihan });
          });

        //kabupaten
        const dataKab = { id: this.state.dataxpelatihan.kabupaten };
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/kabupaten",
            dataKab,
            this.configs,
          )
          .then((res) => {
            const optionx = res.data.result.Data;
            const dataxkab = [];
            for (let i in optionx) {
              if (optionx[i].id == this.state.dataxpelatihan.kabupaten) {
                this.setState({
                  selkabupaten: {
                    value: optionx[i].id,
                    label: optionx[i].name,
                  },
                });
                break;
              }
            }
            optionx.map((data) =>
              dataxkab.push({ value: data.id, label: data.name }),
            );
            this.setState({ dataxkab });
          });
        // });

        const dataFormSubmit = new FormData();
        dataFormSubmit.append(
          "id",
          this.state.dataxpelatihan.form_pendaftaran_id,
        );
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/cari-formbuilder",
            dataFormSubmit,
            this.configs,
          )
          .then((res) => {
            const statux = res.data.result.Status;
            const messagex = res.data.result.Message;
            if (statux) {
              const dataxpreview = res.data.result;
              this.setState({
                dataxpreview: {
                  detail: dataxpreview.detail,
                  utama: dataxpreview.utama,
                },
              });
            } else {
              swal
                .fire({
                  title: messagex,
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                  }
                });
            }
          });
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/detail_silabus",
            { id: dataxpelatihan.id_silabus },
            this.configs,
          )
          .then((res) => {
            const statux = res.data.result.Status;
            const messagex = res.data.result.Message;
            if (statux) {
              const result = res.data.result;
              this.setState({
                detailsilabus: {
                  nama: result.NamaSilabus,
                  totalJp: result.Jml_jampelajaran,
                  isiSilabus: result.DataSilabus,
                },
              });
            } else {
              swal
                .fire({
                  title: messagex,
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                  }
                });
            }
          });
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/pelatihanz/viewCatRevisi",
            {
              pelatian_id: dataxpelatihan.id,
            },
            this.configs,
          )
          .then((res) => {
            const statux = res.data.result.Status;
            const messagex = res.data.result.Message;
            if (statux) {
              const catatanterakhir = res.data.result.Data.at(-1);
              this.setState({ catatanterakhir });
            } else {
              swal
                .fire({
                  title: messagex,
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                  }
                });
            }
          });
      });
  }

  handleDelete(idx) {
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          let data = {
            id: idx,
          };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/delpelatihan",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (true == statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      window.location = "/pelatihan/pelatihan";
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload(false, this.state.param, 1, 10);
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            });
        }
      });
  }

  handleClickRejectForceMajureAction(e) {
    swal
      .fire({
        title: "Apakah anda yakin ingin membatalkan pelatihan?",
        text: "Status Pendaftaran Peserta Juga Akan Dibatalkan",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, batalkan!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          let data = {
            pelatian_id: this.state.dataxpelatihan.id,
          };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/pelatihanz/cancel",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Data;
              if (statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      window.location.reload();
                    }
                  });
              } else {
                throw Error(messagex);
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex =
                typeof error == "string"
                  ? error
                  : error.response?.data?.result?.Message;
              if (!statux) {
                swal
                  .fire({
                    title:
                      messagex ??
                      "Terjadi kesalahan saat akan membatalkan pelatihan!",
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            });
        }
      });
  }

  handleClickBatalAction(e) {
    window.history.back();
  }
  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  List Pelatihan
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>

                <Link
                  to={`/pelatihan/edit-pelatihan/${this.state.dataxpelatihan.id}`}
                  className="btn btn-warning fw-bolder btn-sm me-2"
                >
                  <i className="bi bi-gear-fill"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Edit
                  </span>
                </Link>

                <button
                  className="btn btn-sm btn-danger btn-active-light-info me-2"
                  onClick={() =>
                    this.handleDelete(this.state.dataxpelatihan.id)
                  }
                >
                  <i className="bi bi-trash-fill text-white"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Detail Pelatihan
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <div className="d-flex align-items-center position-relative my-1">
                            <a
                              href={
                                "/pelatihan/detail-rekappendaftaran/" +
                                this.state.dataxpelatihan.id
                              }
                              className="btn btn-light-primary fw-bolder btn-sm fs-7 me-2"
                              type="button"
                            >
                              <i className="bi bi-people me-1"></i>
                              <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                Rekap Pendaftaran
                              </span>
                            </a>
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="mb-7">
                          <div className="d-flex flex-wrap py-3">
                            <div className="symbol symbol-100px symbol-lg-120px me-4">
                              {this.state.dataxpelatihan.mitra_logo == null ? (
                                <img
                                  src={`/assets/media/logos/logo-kominfo.png`}
                                  alt=""
                                  height="100px"
                                  className="symbol-label"
                                />
                              ) : (
                                <img
                                  src={
                                    process.env.REACT_APP_BASE_API_URI +
                                    "/download/get-file?path=" +
                                    this.state.dataxpelatihan.mitra_logo +
                                    "&disk=dts-storage-partnership"
                                  }
                                  height="100px"
                                  alt=""
                                  className="symbol-label"
                                />
                              )}
                            </div>
                            <div className="flex-grow-1 mb-3">
                              <div className="justify-content-between align-items-start flex-wrap pt-6">
                                <span
                                  className={`badge badge-${resolveStatusPelatihanBg(
                                    this.state.dataxpelatihan.status_pelatihan,
                                  )}`}
                                >
                                  {this.state.dataxpelatihan.status_pelatihan}
                                </span>
                                <h1 className="align-items-center text-dark fw-bolder my-1 fs-4">
                                  {this.state.dataxpelatihan.slug_pelatian_id} -{" "}
                                  {this.state.dataxpelatihan.pelatihan} (Batch{" "}
                                  {this.state.dataxpelatihan.batch})
                                </h1>
                                <p className="text-dark fs-7 mb-0">
                                  {this.state.dataxpelatihan.akademi} -{" "}
                                  <span className="text-muted fw-semibold fs-7 mb-0">
                                    {this.state.dataxpelatihan.tema}
                                  </span>
                                </p>
                              </div>
                            </div>
                          </div>
                          <div className="d-flex flex-wrap fw-bold mt-5 fs-6 mb-4 pe-2">
                            <span className="d-flex align-items-center fs-7 text-dark me-5 mb-3">
                              <span className="svg-icon svg-icon-4 me-1">
                                <i className="bi bi-recycle text-dark me-1"></i>
                                {this.state.dataxpelatihan.alur_pendaftaran}
                              </span>
                            </span>
                            <span className="d-flex align-items-center fs-7 text-dark me-5 mb-3">
                              <span className="svg-icon svg-icon-4 me-1">
                                <i className="bi bi-book text-dark me-1"></i>
                                {capitalWord(
                                  this.state.dataxpelatihan.metode_pelatihan
                                    ?.toLowerCase()
                                    .replace(" ", " - "),
                                )}
                              </span>
                            </span>
                            <span className="d-flex align-items-center fs-7 text-dark me-5 mb-3">
                              <span className="svg-icon svg-icon-4 me-1">
                                <i className="bi bi-mic text-dark me-1"></i>
                                {this.state.dataxpelatihan.penyelenggara}
                              </span>
                            </span>
                            <span className="d-flex align-items-center fs-7 text-dark me-5 mb-3">
                              <span className="svg-icon svg-icon-4 me-1">
                                <i className="bi bi-person-video3 text-dark me-1"></i>
                                {this.state.dataxpelatihan.metode_pelaksanaan ==
                                "Mitra"
                                  ? this.state.dataxpelatihan.mitra_name
                                  : this.state.dataxpelatihan
                                      .metode_pelaksanaan}
                              </span>
                            </span>
                            <span className="d-flex align-items-center fs-7 text-dark me-5 mb-3">
                              <span className="svg-icon svg-icon-4 me-1">
                                <i className="bi bi-geo-alt text-dark me-1"></i>
                                {this.state.dataxpelatihan.metode_pelatihan ==
                                "Online"
                                  ? this.state.dataxpelatihan.metode_pelatihan
                                  : this.state.selkabupaten.label}
                              </span>
                            </span>
                          </div>
                        </div>
                        <div className="d-flex border-bottom p-0">
                          <ul
                            className="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap"
                            style={{
                              overflowX: "auto",
                              overflowY: "hidden",
                              display: "flex",
                              whiteSpace: "nowrap",
                              marginBottom: 0,
                            }}
                          >
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary active"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_1"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                  Informasi Pelatihan
                                </span>
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary false"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_2"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                  Jadwal
                                </span>
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary false"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_3"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                  Silabus
                                </span>
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary false"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_4"
                              >
                                <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                  Form Pendaftaran
                                </span>
                              </a>
                            </li>
                          </ul>
                        </div>
                        <div className="tab-content" id="detail-account-tab">
                          {/* tab 1 */}
                          <div
                            className="tab-pane fade show active"
                            id="kt_tab_pane_1"
                            role="tabpanel"
                          >
                            <div className="row mt-7 pb-7">
                              <div className="row">
                                <div className="col-lg-6 my-3 fv-row">
                                  <label className="form-label">
                                    Kategori Program Pelatihan
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {this.state.dataxpelatihan.program_dts ==
                                      1
                                        ? "Digital Talent Scholarship"
                                        : this.state.dataxpelatihan
                                              .program_dts == 0
                                          ? "Lainnya"
                                          : "-"}
                                    </b>
                                  </div>
                                </div>
                                <div className="col-lg-6 my-3 fv-row">
                                  <label className="form-label">
                                    Batch Pelatihan
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {this.state.dataxpelatihan.batch ?? "-"}
                                    </b>
                                  </div>
                                </div>
                                <div className="col-lg-6 my-3 fv-row">
                                  <label className="form-label">
                                    Metode Pelaksanaan
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {
                                        this.state.dataxpelatihan
                                          .metode_pelaksanaan
                                      }
                                    </b>
                                  </div>
                                </div>
                                <div className="col-lg-6 my-3 fv-row">
                                  <label className="form-label">Zonasi</label>
                                  <div className="d-flex">
                                    <b>{this.state.dataxpelatihan.zonasi}</b>
                                  </div>
                                </div>
                                <div className="col-lg-6 my-3 fv-row">
                                  <label className="form-label">
                                    Level Pelatihan
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {
                                        this.state.dataxpelatihan
                                          .level_pelatihan
                                      }
                                    </b>
                                  </div>
                                </div>
                                <div className="col-lg-6 my-3 fv-row">
                                  <label className="form-label">
                                    Sertifikasi
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {this.state.dataxpelatihan.sertifikasi}
                                    </b>
                                  </div>
                                </div>
                                <div className="col-lg-6 my-3 fv-row">
                                  <label className="form-label">
                                    Klasifikasi Peserta
                                  </label>
                                  <div className="d-flex">
                                    <b>{this.state.disabilities.join(", ")}</b>
                                  </div>
                                </div>
                                {this.state.dataxpelatihan
                                  .pelaksana_assement_name &&
                                  this.state.dataxpelatihan
                                    .pelaksana_assement_name != "" && (
                                    <div className="col-lg-6 my-3 fv-row">
                                      <label className="form-label">
                                        Institusi / LSP yang melakukan uji
                                        kompetensi
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {
                                            this.state.dataxpelatihan
                                              .pelaksana_assement_name
                                          }
                                        </b>
                                      </div>
                                    </div>
                                  )}
                                <div className="col-lg-6 my-3 fv-row">
                                  <label className="form-label">
                                    Link Pelatihan
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {this.state.dataxpelatihan.id ? (
                                        <a
                                          target="_blank"
                                          href={`${
                                            process.env
                                              .REACT_APP_BASE_USER_URI ??
                                            FRONT_URL
                                          }/pelatihan/${
                                            this.state.dataxpelatihan.id
                                          }`}
                                        >
                                          {`${
                                            process.env
                                              .REACT_APP_BASE_USER_URI ??
                                            FRONT_URL
                                          }/pelatihan/${
                                            this.state.dataxpelatihan.id
                                          }`}
                                        </a>
                                      ) : (
                                        <a href="#"> - </a>
                                      )}
                                    </b>
                                  </div>
                                </div>
                                <div className="col-lg-12 bg-light mx-3 p-6 rounded my-5 fv-row">
                                  <label className="form-label">
                                    Deskripsi Pelatihan
                                  </label>
                                  <div className="d-flex">
                                    <strong
                                      dangerouslySetInnerHTML={{
                                        __html:
                                          this.state.dataxpelatihan.deskripsi,
                                      }}
                                    ></strong>
                                  </div>
                                </div>
                                <div className="col-lg-12">
                                  <div className="mb-7 fv-row">
                                    <label className="form-label">
                                      Silabus
                                    </label>
                                    <div className="d-flex">
                                      <b>
                                        <a
                                          target="_blank"
                                          href={
                                            this.state.dataxpelatihan.silabus
                                          }
                                        >
                                          Download
                                        </a>
                                      </b>
                                    </div>
                                  </div>
                                </div>
                                <div>
                                  <div className="my-8 border-top mx-0"></div>
                                </div>
                                <h2 className="fs-5 text-muted mb-3">
                                  Kuota Pelatihan
                                </h2>
                                {this.state.dataxpelatihan.kuota_pendaftar !=
                                  "999999" && (
                                  <div className="col-lg-4 my-3 fv-row">
                                    <label className="form-label">
                                      Target Kuota Pendaftar
                                    </label>
                                    <div className="d-flex">
                                      <b>
                                        {
                                          this.state.dataxpelatihan
                                            .kuota_pendaftar
                                        }{" "}
                                        Peserta
                                      </b>
                                    </div>
                                  </div>
                                )}
                                <div className="col-lg-4 my-3 fv-row">
                                  <label className="form-label">
                                    Target Kuota Peserta
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {this.state.dataxpelatihan.kuota_peserta}{" "}
                                      Peserta
                                    </b>
                                  </div>
                                </div>
                                <div className="col-lg-4 my-3 fv-row">
                                  <label className="form-label">
                                    Status Kuota
                                  </label>
                                  <div className="d-flex">
                                    <b>
                                      {this.state.dataxpelatihan.status_kuota}
                                    </b>
                                  </div>
                                </div>
                                <div>
                                  <div className="my-8 border-top mx-0"></div>
                                </div>
                                <h2 className="fs-5 text-muted mb-3">
                                  Lokasi Pelatihan
                                </h2>
                                {this.state.dataxpelatihan.metode_pelaksanaan
                                  ?.toLowerCase()
                                  ?.includes("offline") ? (
                                  <>
                                    <div className="col-lg-12 my-3 fv-row">
                                      <label className="form-label">
                                        Alamat
                                      </label>
                                      <div className="d-flex">
                                        <b
                                          dangerouslySetInnerHTML={{
                                            __html:
                                              this.state.dataxpelatihan.alamat,
                                          }}
                                        ></b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 my-3 fv-row">
                                      <label className="form-label">
                                        Provinsi
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {this.state.dataxpelatihan.nm_prov}
                                        </b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 my-3 fv-row">
                                      <label className="form-label">
                                        Kota / Kabupaten
                                      </label>
                                      <div className="d-flex">
                                        <b>
                                          {this.state.dataxpelatihan
                                            .metode_pelatihan == "Online"
                                            ? this.state.dataxpelatihan
                                                .metode_pelatihan
                                            : this.state.selkabupaten.label}
                                        </b>
                                      </div>
                                    </div>
                                  </>
                                ) : (
                                  <div className="col-lg-6 my-3 fv-row">
                                    <div className="d-flex">
                                      <b>
                                        {
                                          this.state.dataxpelatihan
                                            .metode_pelatihan
                                        }
                                      </b>
                                    </div>
                                  </div>
                                )}
                                <div>
                                  <div className="my-8 border-top mx-0"></div>
                                </div>
                                <h2 className="fs-5 text-muted mb-5">Log</h2>
                                <div className="col-lg-12">
                                  <div className="mb-7 fv-row">
                                    <div className="d-flex align-items-center bg-light-success rounded p-5 mb-2">
                                      <span className="svg-icon svg-icon-success me-5">
                                        <span className="svg-icon svg-icon-1">
                                          <i className="fa fa-plus-circle fa-1x text-success"></i>
                                        </span>
                                      </span>
                                      <div className="flex-grow-1 me-2">
                                        <h6 className="fs-7 text-success">
                                          Dibuat oleh
                                        </h6>
                                        <a
                                          href="#"
                                          className="fw-bold text-gray-800 text-hover-success fs-6"
                                        >
                                          {this.state.dataxpelatihan.created_by}
                                        </a>
                                        <span className="text-muted fw-semibold d-block">
                                          {handleFormatDate(
                                            this.state.dataxpelatihan
                                              .created_at,
                                            "DD MMMM YYYY, HH:mm:ss",
                                            "DD-MM-YYYY, HH:mm:ss",
                                          )}
                                        </span>
                                      </div>
                                    </div>
                                    <div className="d-flex align-items-center bg-light-primary rounded p-5 mb-2">
                                      <span class="svg-icon svg-icon-primary svg-icon-1 me-5">
                                        <i className="fa fa-cog fa-1x text-primary"></i>
                                      </span>
                                      <div className="flex-grow-1 me-2">
                                        <h6 className="fs-7 text-primary">
                                          Diubah oleh
                                        </h6>
                                        <a
                                          href="#"
                                          className="fw-bold text-gray-800 text-hover-success fs-6"
                                        >
                                          {this.state.dataxpelatihan
                                            .updated_by ?? "-"}
                                        </a>
                                        <span className="text-muted fw-semibold d-block">
                                          {handleFormatDate(
                                            this.state.dataxpelatihan
                                              .updated_at,
                                            "DD MMMM YYYY, HH:mm:ss",
                                            "DD-MM-YYYY, HH:mm:ss",
                                          )}
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                {this.state.dataxpelatihan.status_substansi ==
                                  "Revisi" && (
                                  <>
                                    <div>
                                      <div className="my-8 border-top mx-0"></div>
                                    </div>
                                    <h2 className="fs-5 text-muted mb-3">
                                      Catatan Revisi
                                    </h2>
                                    <div className="col-lg-12 my-3 fv-row">
                                      <div className="d-flex align-items-center bg-light-warning rounded p-5 mb-2">
                                        <span className="svg-icon svg-icon-warning me-5">
                                          <span className="svg-icon svg-icon-1">
                                            <svg
                                              width="24"
                                              height="24"
                                              viewBox="0 0 24 24"
                                              fill="none"
                                              xmlns="http://www.w3.org/2000/svg"
                                              className="mh-50px"
                                            >
                                              <path
                                                opacity="0.3"
                                                d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                                fill="#ffc700"
                                              ></path>
                                              <path
                                                d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                                fill="#ffc700"
                                              ></path>
                                            </svg>
                                          </span>
                                        </span>
                                        <div className="flex-grow-1 me-2">
                                          <span className="fw-bolder text-gray-800 fs-6">
                                            {this.state.catatanterakhir.revisi}
                                          </span>
                                          <span className="fw d-block">
                                            tampilkan tgl revisi disini
                                          </span>
                                        </div>
                                      </div>
                                    </div>
                                  </>
                                )}

                                {this.state.dataxpelatihan.status_substansi ==
                                  "Disetujui" && (
                                  <>
                                    {this.state.dataxpelatihan
                                      .status_pelatihan == "Dibatalkan" ? (
                                      <div
                                        className="alert alert-primary fade show mt-1"
                                        role="alert"
                                      >
                                        <span className="alert-link">
                                          Pelatihan sudah dibatalkan pada
                                          tanggal{" "}
                                          <strong>
                                            {handleFormatDate(
                                              this.state.dataxpelatihan
                                                .updated_at,
                                              "DD MMMM YYYY, HH:mm:ss",
                                            )}
                                          </strong>{" "}
                                          oleh{" "}
                                          <strong>
                                            {
                                              this.state.dataxpelatihan
                                                .updated_by
                                            }
                                          </strong>
                                        </span>
                                      </div>
                                    ) : this.state.dataxpelatihan
                                        .status_pelatihan == "Selesai" ? (
                                      <div
                                        className="alert alert-primary fade show mt-1 d-flex align-items-center"
                                        role="alert"
                                      >
                                        <i
                                          className="bi bi-check-circle fs-2x me-2"
                                          style={{ color: "black" }}
                                        ></i>

                                        <div className="alert-link">
                                          Pelatihan telah selesai pada tanggal{" "}
                                          <strong>
                                            {handleFormatDate(
                                              this.state.dataxpelatihan
                                                .pelatihan_end,
                                              "DD MMMM YYYY, HH:mm:ss",
                                              "DD-MM-YYYY HH:mm:ss",
                                            )}
                                          </strong>
                                        </div>
                                      </div>
                                    ) : (
                                      <a
                                        href="#"
                                        onClick={
                                          this.handleClickRejectForceMajure
                                        }
                                        className="btn btn-flex btn-danger px-6"
                                      >
                                        <i className="bi bi-stop-circle fs-2x "></i>
                                        <span className="d-flex flex-column align-items-start ms-2">
                                          <span className="fs-3 fw-bolder">
                                            BATALKAN PELATIHAN
                                          </span>
                                          <span className="fs-7">
                                            Harap perhatikan. Tombol ini hanya
                                            digunakan saat terjadi Force Majure
                                          </span>
                                        </span>
                                      </a>
                                    )}
                                  </>
                                )}
                              </div>
                            </div>
                          </div>

                          <div
                            className="tab-pane fade"
                            id="kt_tab_pane_2"
                            role="tabpanel"
                          >
                            <div className="row mt-7 pt-5 pb-7">
                              <h2 className="fs-5 text-muted mb-3">
                                Jadwal Pendaftaran dan Pelatihan
                              </h2>
                              <div className="col-lg-12 mb-7 fv-row">
                                <label className="form-label">
                                  Alur Seleksi
                                </label>
                                <div className="d-flex">
                                  <strong>
                                    {this.state.dataxpelatihan.alur_pendaftaran}
                                  </strong>
                                </div>
                              </div>
                              <div className="col-lg-6 mb-7 fv-row">
                                <label className="form-label">
                                  Pendaftaran
                                </label>
                                <div className="d-flex">
                                  <strong>
                                    {dateRange(
                                      this.state.dataxpelatihan
                                        .pendaftaran_start,
                                      this.state.dataxpelatihan.pendaftaran_end,
                                    )}
                                  </strong>
                                </div>
                                <p className="fs-7 my-0 fw-semibold text-muted">
                                  {dateRange(
                                    this.state.dataxpelatihan.pendaftaran_start,
                                    this.state.dataxpelatihan.pendaftaran_end,
                                    "DD-MM-YYYY HH:mm:ss",
                                    "HH:mm:ss",
                                  )}
                                </p>
                              </div>
                              <div className="col-lg-6 mb-7 fv-row">
                                <label className="form-label">Pelatihan</label>
                                <div className="d-flex">
                                  <strong>
                                    {dateRange(
                                      this.state.dataxpelatihan.pelatihan_start,
                                      this.state.dataxpelatihan.pelatihan_end,
                                    )}
                                  </strong>
                                </div>
                                <p className="fs-7 my-0 fw-semibold text-muted">
                                  {dateRange(
                                    this.state.dataxpelatihan.pelatihan_start,
                                    this.state.dataxpelatihan.pelatihan_end,
                                    "DD-MM-YYYY HH:mm:ss",
                                    "HH:mm:ss",
                                  )}
                                </p>
                              </div>
                              {this.state.dataxpelatihan.alur_pendaftaran &&
                                this.state.dataxpelatihan.alur_pendaftaran !==
                                  "Administrasi" && (
                                  <>
                                    <div>
                                      <div className="my-8 border-top mx-0"></div>
                                    </div>
                                    <h2 className="fs-5 text-muted mb-3">
                                      Seleksi Adminsitrasi & Tes Substansi
                                    </h2>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Seleksi Administrasi
                                      </label>
                                      <div className="d-flex">
                                        <strong>
                                          {dateRange(
                                            this.state.dataxpelatihan
                                              .administrasi_mulai,
                                            this.state.dataxpelatihan
                                              .administrasi_selesai,
                                          )}
                                        </strong>
                                      </div>
                                      <p className="fs-7 my-0 fw-semibold text-muted">
                                        {dateRange(
                                          this.state.dataxpelatihan
                                            .administrasi_mulai,
                                          this.state.dataxpelatihan
                                            .administrasi_selesai,
                                          "DD-MM-YYYY HH:mm:ss",
                                          "HH:mm:ss",
                                        )}
                                      </p>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Seleksi Test Substansi
                                      </label>
                                      <div className="d-flex">
                                        <strong>
                                          {dateRange(
                                            this.state.dataxpelatihan
                                              .subtansi_mulai,
                                            this.state.dataxpelatihan
                                              .subtansi_selesai,
                                          )}
                                        </strong>
                                      </div>
                                      <p className="fs-7 my-0 fw-semibold text-muted">
                                        {dateRange(
                                          this.state.dataxpelatihan
                                            .subtansi_mulai,
                                          this.state.dataxpelatihan
                                            .subtansi_selesai,
                                          "DD-MM-YYYY HH:mm:ss",
                                          "HH:mm:ss",
                                        )}
                                      </p>
                                    </div>
                                  </>
                                )}
                              {this.state.dataxpelatihan.midtest_mulai && (
                                <>
                                  <div>
                                    <div className="my-8 border-top mx-0"></div>
                                  </div>
                                  <h2 className="fs-5 text-muted mb-3">
                                    Mid Test
                                  </h2>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Tgl. Midtest
                                    </label>
                                    <div className="d-flex">
                                      <b>
                                        {dateRange(
                                          this.state.dataxpelatihan
                                            .midtest_mulai,
                                          this.state.dataxpelatihan
                                            .midtest_selesai,
                                        )}
                                      </b>
                                    </div>
                                    <p className="fs-7 my-0 fw-semibold text-muted">
                                      {dateRange(
                                        this.state.dataxpelatihan.midtest_mulai,
                                        this.state.dataxpelatihan
                                          .midtest_selesai,
                                        "DD-MM-YYYY HH:mm:ss",
                                        "HH:mm:ss",
                                      )}
                                    </p>
                                  </div>
                                </>
                              )}
                            </div>
                          </div>

                          <div
                            className="tab-pane fade"
                            id="kt_tab_pane_3"
                            role="tabpanel"
                          >
                            <div className="row mt-5 pb-7">
                              <div className="col-lg-12 mb-7 fv-row">
                                {this.state.detailsilabus && (
                                  <div className="mb-5">
                                    <h5
                                      className="modal-title mt-5 mb-6"
                                      id="preview_title"
                                    >
                                      {this.state.dataxpelatihan.judul_silabus}
                                      <span className="text-muted d-block small">
                                        ID :{" "}
                                        {this.state.dataxpelatihan.id_silabus} |{" "}
                                        {` Total : ${zeroDecimalValue(
                                          this.state.detailsilabus.totalJp,
                                        )} Jam Pelajaran`}
                                      </span>
                                    </h5>
                                  </div>
                                )}
                                <div className="mr-3">
                                  <ul className="list-group list-group-numbered">
                                    {this.state.detailsilabus &&
                                      this.state.detailsilabus.isiSilabus &&
                                      this.state.detailsilabus.isiSilabus.map(
                                        (elem, index) => (
                                          <li
                                            key={
                                              "silabus_" + elem.id + "_" + index
                                            }
                                            className="list-group-item fs-7 fw-bolder d-flex justify-content-between align-items-start"
                                          >
                                            <div className="ms-2 me-auto">
                                              <div>
                                                <h6 className="fw-bolder mb-0">
                                                  {elem.materi}
                                                </h6>
                                              </div>
                                              <span className="text-muted fw-semibold fs-7">
                                                {zeroDecimalValue(
                                                  elem.jam_pelajaran,
                                                )}{" "}
                                                JP
                                              </span>
                                            </div>
                                          </li>
                                        ),
                                      )}
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div
                            className="tab-pane fade"
                            id="kt_tab_pane_4"
                            role="tabpanel"
                          >
                            <div className="row mt-7 pb-7">
                              <div className="col-lg-12 mb-7 fv-row">
                                {this.state.dataxpreview.utama?.length == 1 && (
                                  <h2 className="fs-5 text-muted mt-5 mb-0">
                                    {
                                      this.state.dataxpreview.utama[0]
                                        .judul_form
                                    }
                                  </h2>
                                )}
                                <div className="mt-5">
                                  {this.state.dataxpreview.detail?.length > 0 &&
                                    this.state.dataxpreview?.detail[0] !=
                                      null && (
                                      <RenderFormElement
                                        formElements={
                                          this.state.dataxpreview.detail
                                        }
                                      />
                                    )}
                                </div>
                                <div>
                                  <div className="my-8 border-top mx-0"></div>
                                </div>
                                <h2 className="fs-5 text-muted mb-0">
                                  Komitmen
                                </h2>
                                <span class="fs-7 fw-semibold text-muted mb-3">
                                  Komitmen / Perjanjian/ Ketentuan Pelatihan
                                </span>
                                <div className="col-lg-12 mb-7 mt-5 fv-row">
                                  <div className="d-flex">
                                    <b
                                      dangerouslySetInnerHTML={{
                                        __html:
                                          this.state.dataxpelatihan
                                            .komitmen_deskripsi,
                                      }}
                                    ></b>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
