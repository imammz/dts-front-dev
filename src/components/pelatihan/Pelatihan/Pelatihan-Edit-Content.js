import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import PelatihanPendaftaranEdit from "./PendaftaranDataListKanban";
import Flatpickr from "react-flatpickr";
import { Indonesian } from "flatpickr/dist/l10n/id.js";
import moment from "moment";
import FormElement from "./RenderFormElement";
import {
  fp_inc,
  capitalWord,
  handleFormatDate,
  zeroDecimalValue,
} from "./helper";
import { numericOnly } from "../../publikasi/helper";
import Editor from "@arbor-dev/ckeditor5-arbor-custom";
export default class PelatihanContent extends React.Component {
  constructor(props) {
    super(props);
    Cookies.remove("pelatian_id");
    this.submitFormRef = React.createRef(null);
    this.viewPelatihanRef = React.createRef(null);
    this.savedFormRef = React.createRef(null);
    this.fileSilabusRef = React.createRef(null);
    this.handleClickBatal = this.handleReject.bind(this);
    this.handleChangeKomitmen = this.handleChangeKomitmenAction.bind(this);
    this.handleChangeSertifikasi =
      this.handleChangeSertifikasiAction.bind(this);
    // this.handleChangeSilabus = this.handleChangedSilabus.bind(this);
    this.handleClickMetode = this.handleClickedMetodeAction.bind(this);
    this.handleChangeLPJ = this.handleChangeLPJAction.bind(this);
    this.handleClickMetodePelaksanaan =
      this.handleClickMetodePelaksanaanAction.bind(this);
    this.handleClickProgramDts = this.handleClickProgramDtsAction.bind(this);
    this.handleChangeDeskripsiKomitmen =
      this.handleChangedDeskripsiKomitmen.bind(this);
    this.handleClickTambahForm = this.handleClickTambahFormAction.bind(this);
    this.handleClickRefForm = this.handleClickRefFormAction.bind(this);
    this.handleSubmit = this.handleSubmitAction.bind(this);
    this.handleChangeLvlPelatihan =
      this.handleChangeLvlPelatihanAction.bind(this);
    this.handleChangeAkademi = this.handleChangeAkademiAction.bind(this);
    this.handleChangeTema = this.handleChangeTemaAction.bind(this);
    this.handleChangePenyelenggara =
      this.handleChangePenyelenggaraAction.bind(this);
    this.handleChangeMitra = this.handleChangeMitraAction.bind(this);
    this.handleChangeZonasi = this.handleChangeZonasiAction.bind(this);
    this.handleChangeProv = this.handleChangeProvAction.bind(this);
    this.handleChangePelaksanaAssesments =
      this.handleChangePelaksanaAssesmentsAction.bind(this);
    this.handleChangeKabupaten = this.handleChangeKabupatenAction.bind(this);
    this.handleChangeDescription =
      this.handleChangeDescriptionAction.bind(this);
    this.handleChangeAlamat = this.handleChangeAlamatAction.bind(this);
    this.handleClickRejectForceMajure =
      this.handleClickRejectForceMajureAction.bind(this);

    this.handleChangeBatch = this.handleChangeBatchAction.bind(this);
    this.handleChangeDisabilitasUmum =
      this.handleChangeDisabilitasUmumAction.bind(this);
    this.handleChangeDisabilitasTunaNetra =
      this.handleChangeDisabilitasTunaNetraAction.bind(this);
    this.handleChangeDisabilitasTunaRungu =
      this.handleChangeDisabilitasTunaRunguAction.bind(this);
    this.handleChangeDisabilitasTunaDaksa =
      this.handleChangeDisabilitasTunaDaksaAction.bind(this);
    this.handleChangeAlurPendaftaran =
      this.handleChangeAlurPendaftaranAction.bind(this);
    this.handleChangeApakahMidTest =
      this.handleChangeApakahMidTestAction.bind(this);
    this.handleChangeSilabus = this.handleChangeSilabusAction.bind(this);
    this.handleClickAddSilabus = this.handleClickAddSilabusAction.bind(this);
    this.handleClickSaveSilabus = this.handleClickSaveSilabusAction.bind(this);
    this.handleClickDeleteSilabus =
      this.handleClickDeleteSilabusAction.bind(this);
    this.handleChangeProgram = this.handleChangeProgramAction.bind(this);
    this.handleChangeJudulForm = this.handleChangeJudulFormAction.bind(this);
    this.handleChangeStatusKuota =
      this.handleChangeStatusKuotaAction.bind(this);
    this.handleChangeURL = this.handleChangeURLAction.bind(this);
    this.handleChangeNilai = this.handleChangeNilaiAction.bind(this);
    this.handleOptionChange = this.handleOptionChangeAction.bind(this);

    this.state = {
      fields: {},
      errors: {},
      isLoading: false,
      dataxakademi: [],
      dataxtema: [],
      dataxtemavalue: "",
      dataxprov: [],
      dataxkab: [],
      dataxpenyelenggara: [],
      dataxmitra: [],
      dataxzonasi: [],
      silabusfile: [],
      showing: false,
      showingdeskripsi: false,
      showingembed: true,
      showingnonembed: true,
      showingmetodepelaksanaan: true,
      dataxselform: [],
      valuekomitmen: "",
      valuedeskripsikomitmen: "",
      valuejudulform: "",
      dataxpelatihan: [],
      dataxlevelpelatihan: [],
      dataxpreview: [],
      dataxpreviewtitle: "",
      sellvlpelatihan: [],
      selakademi: [],
      seltema: [],
      selpenyelenggara: [],
      selmitra: [],
      selzonasi: [],
      selprovinsi: [],
      selkabupaten: [],
      seljudulform: [],
      selstatuspublish: [],
      selSertifikasiList: [],
      showSelForm: true,
      showEditForm: true,
      selectValueHidden: "",
      resSelectValueHidden: "",
      deskripsi: "",
      flagstatuspublishedit: false,
      url_vicon: "",
      input_nilai: null,
      valuestatuspublish: "",
      valuestatussubstansi: "",
      valuestatuspelatihan: "",
      showingMidTestDate: false,
      showingSelSilabus: true,
      dataxselusesilabus: [],
      showViewSilabus: false,
      detailSilabus: [],
      pelatihanBySilabus: [],
      selpelaksanaassesments: "",
      pelaksanaassesmentsList: [],
      isiSilabus: [
        {
          id: 1,
          materi: "",
          jam_pelajaran: "",
        },
      ],
      showBtnNextW2: true,
      formPendaftaran: [],
      fileSilabus: null,
      showViewForm: false,
      wsml_themes: [],
      isKuotaPendaftarVisible: true,
    };
    this.formDatax = new FormData();
  }
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  optionstatus = [
    { value: "1", label: "Publish" },
    { value: "0", label: "Unpublish" },
    { value: "2", label: "Unlisted" },
  ];

  handleConvertAPIdate(date) {
    return moment(date, "DD-MM-YYYY HH:mm:ss").isValid()
      ? moment(date, "DD-MM-YYYY HH:mm:ss").toISOString()
      : date;
  }

  handleChangePelaksanaAssesmentsAction(e) {
    this.setState({
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        pelaksana_assement_id: e.currentTarget.value,
      },
    });
  }
  async componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    let segment_url = window.location.pathname.split("/");
    let urlSegmenttOne = segment_url[3];
    let data = {
      id: urlSegmenttOne,
    };
    try {
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });
      const pelatihan = await axios.post(
        process.env.REACT_APP_BASE_API_URI + "/pelatihanz/findpelatihan2",
        data,
        this.configs,
      );
      const dataxpelatihan = pelatihan.data.result.Data[0];
      // tanggal2 pelatihan
      document.getElementsByName("komitmen_deskripsi_hidden")[0].value =
        dataxpelatihan.komitmen_deskripsi ?? "";
      document.getElementsByName("alamat_hidden")[0].value =
        dataxpelatihan.alamat ?? "";
      document.getElementsByName("deskripsi_hidden")[0].value =
        dataxpelatihan.deskripsi ?? "";
      dataxpelatihan.pendaftaran_start = this.handleConvertAPIdate(
        dataxpelatihan.pendaftaran_start,
      );
      dataxpelatihan.pendaftaran_end = this.handleConvertAPIdate(
        dataxpelatihan.pendaftaran_end,
      );
      dataxpelatihan.pelatihan_start = this.handleConvertAPIdate(
        dataxpelatihan.pelatihan_start,
      );
      dataxpelatihan.pelatihan_end = this.handleConvertAPIdate(
        dataxpelatihan.pelatihan_end,
      );
      dataxpelatihan.midtest_mulai = this.handleConvertAPIdate(
        dataxpelatihan.midtest_mulai,
      );
      // tanggal2 substansi & administrasi
      dataxpelatihan.administrasi_mulai = this.handleConvertAPIdate(
        dataxpelatihan.administrasi_mulai,
      );
      dataxpelatihan.administrasi_selesai = this.handleConvertAPIdate(
        dataxpelatihan.administrasi_selesai,
      );
      dataxpelatihan.subtansi_mulai = this.handleConvertAPIdate(
        dataxpelatihan.subtansi_mulai,
      );
      dataxpelatihan.subtansi_selesai = this.handleConvertAPIdate(
        dataxpelatihan.subtansi_selesai,
      );
      this.setState(
        {
          dataxpelatihan: dataxpelatihan,
          valuekomitmen: dataxpelatihan.komitmen,
          valuedeskripsikomitmen: dataxpelatihan.komitmen_deskripsi,
          selakademi: {
            value: dataxpelatihan.akademi_id,
            label: dataxpelatihan.akademi,
          },
          seltema: {
            value: dataxpelatihan.tema_id,
            label: dataxpelatihan.tema,
          },
          selmitra: {
            value: dataxpelatihan.mitra_id,
            label: dataxpelatihan.mitra_name,
          },
          selpenyelenggara: {
            value: dataxpelatihan.id_penyelenggara,
            label: dataxpelatihan.penyelenggara,
          },
          sellvlpelatihan: {
            value: dataxpelatihan.level_pelatihan_id,
            label: dataxpelatihan.level_pelatihan,
          },
          selprovinsi: {
            value: dataxpelatihan.provinsi,
            label: "",
          },
          selkabupaten: {
            value: dataxpelatihan.kabupaten,
            label: "",
          },
          seljudulform: {
            value: dataxpelatihan.form_pendaftaran_id,
            label: "",
          },
          showingMidTestDate: dataxpelatihan.midtest_mulai != null,
          selnamasilabus: {
            value: dataxpelatihan.id_silabus,
            label: dataxpelatihan.judul_silabus,
            jpsilabus: 0,
          },
          showing: dataxpelatihan.metode_pelatihan == "Online" ? false : true,
        },
        () => {},
      );
      Cookies.remove("form_pendaftaran_id");
      Cookies.set(
        "form_pendaftaran_id",
        this.state.dataxpelatihan.form_pendaftaran_id,
      );

      const formPendaftaranResp = await axios.post(
        process.env.REACT_APP_BASE_API_URI + "/cari-formbuilder",
        {
          id: dataxpelatihan.form_pendaftaran_id,
        },
      );
      this.setState(
        { formPendaftaran: formPendaftaranResp.data.result },
        () => {},
      );

      //   //level pelatihan
      const m_level_pelatihan = axios.post(
        process.env.REACT_APP_BASE_API_URI + "/levelpelatihan",
        {},
        this.configs,
      );
      // akademi
      const m_akademi = axios.post(
        process.env.REACT_APP_BASE_API_URI + "/akademi/list-akademi",
        { start: 0, length: 100, status: "publish" },
        this.configs,
      );
      //penyelenggara
      const m_penyelenggara = axios.post(
        process.env.REACT_APP_BASE_API_URI + "/list_satker",
        { mulai: 0, limit: 100, cari: "", sort: "id desc" },
        this.configs,
      );
      // mitra
      const m_mitra = axios.post(
        process.env.REACT_APP_BASE_API_URI + "/mitra/list-mitra-s3",
        {
          start: 0,
          length: 100,
          sort: "id",
          sort_val: "desc",
          param: "",
          status: 1,
          tahun: new Date().getFullYear(),
        },
        this.configs,
      );
      // zonasi
      const m_zonasi = axios.post(
        process.env.REACT_APP_BASE_API_URI +
          "/masterdata/API_List_Master_Zonasi",
        { mulai: 0, limit: 100, cari: "", sort: "id desc" },
        this.configs,
      );
      //provinsi
      const m_provinsi = axios.post(
        process.env.REACT_APP_BASE_API_URI + "/provinsi",
        { start: 1, rows: 1000 },
        this.configs,
      );
      //judul form
      const m_form_builder = axios.post(
        process.env.REACT_APP_BASE_API_URI + "/selformpendaftaran",
        { param: "" },
        this.configs,
      );
      const m_sertifikasi = axios.post(
        process.env.REACT_APP_BASE_API_URI + "/pelatihanz/sertifikasi_list",
        {},
        this.configs,
      );
      const m_silabus = axios.post(
        process.env.REACT_APP_BASE_API_URI + "/list_silabus",
        {
          mulai: 0,
          limit: 1000,
          id_akademi:
            this.state.selakademi?.value == null
              ? 0
              : this.state.selakademi?.value,
          id_tema:
            this.state.seltema?.value == null ? 0 : this.state.seltema?.value,
          cari: "",
          sort: "id desc",
        },
        this.configs,
      );

      const m_pelaksana_assesments = axios.post(
        process.env.REACT_APP_BASE_API_URI + "/referensi/pelaksana_assements",
        {},
        this.configs,
      );

      const resps = await axios.all([
        m_level_pelatihan,
        m_akademi,
        m_penyelenggara,
        m_zonasi,
        m_provinsi,
        m_form_builder,
        m_sertifikasi,
        m_silabus,
        m_mitra,
        m_pelaksana_assesments,
      ]);

      let optionx = resps[0].data.result.Data;
      const dataxlevelpelatihan = [];
      optionx.map((data) =>
        dataxlevelpelatihan.push({
          value: data.id,
          label: data.name + " (" + data.nilai + ")",
        }),
      );
      this.setState({ dataxlevelpelatihan });

      optionx = resps[1].data.result.Data;
      const dataxakademi = [];
      optionx.map((data) =>
        dataxakademi.push({ value: data.id, label: data.name }),
      );
      this.setState({ dataxakademi });

      optionx = resps[2].data.result.Data;
      const dataxpenyelenggara = [];
      optionx.map((data) =>
        dataxpenyelenggara.push({ value: data.id, label: data.name }),
      );
      this.setState({ dataxpenyelenggara });

      optionx = resps[3].data.result.Data;
      const dataxzonasi = [];
      const zonasi = optionx.filter(
        (zonasi) => zonasi.nama_zonasi == dataxpelatihan.zonasi,
      )[0];
      if (zonasi) {
        this.setState({
          selzonasi: { label: zonasi.nama_zonasi, value: zonasi.id },
        });
      }
      optionx.map((data) =>
        dataxzonasi.push({ value: data.id, label: data.nama_zonasi }),
      );
      this.setState({ dataxzonasi });

      optionx = resps[4].data.result.Data;
      const dataxprov = [];
      optionx.map((data) =>
        dataxprov.push({ value: data.id, label: data.name }),
      );
      this.setState({ dataxprov });

      optionx = resps[5].data.result.Data;
      const dataxselform = [];
      optionx.map((data) =>
        dataxselform.push({ value: data.id, label: data.judul_form }),
      );
      this.setState({ dataxselform });

      this.setState({ selSertifikasiList: resps[6].data.result.Data });
      optionx = resps[6].data.result.Data;
      const selSertifikasi = optionx.filter(
        (elem) => elem.name == dataxpelatihan.sertifikasi,
      );
      if (selSertifikasi.length > 0) {
        this.setState({
          dataxpelatihan: {
            ...this.state.dataxpelatihan,
            sertifikasi: selSertifikasi[0].id,
          },
        });
      }

      optionx = resps[7].data.result.Data;
      const dataxselusesilabus = [];
      optionx.map((data) =>
        dataxselusesilabus.push({ value: data.id, label: data.judul_silabus }),
      );
      this.setState({ dataxselusesilabus });

      optionx = resps[8].data.result.Data;
      const dataxmitra = [];
      optionx.map((data) =>
        dataxmitra.push({ value: data.id, label: data.nama_mitra }),
      );
      this.setState({ dataxmitra });

      this.setState({ pelaksanaassesmentsList: resps[9].data.result.Data });
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/cari_tema_byakademi",
          { start: 1, rows: 100, id: dataxpelatihan.akademi_id },
          this.configs,
        )
        .then((res) => {
          const optionx = res.data.result.Data;
          const dataxtema = [];
          optionx.map((data) =>
            dataxtema.push({ value: data.id, label: data.name }),
          );
          this.setState({ dataxtema });
        })
        .catch((err) => {
          console.log(err);
        });

      if (this.state.dataxpelatihan.provinsi !== "online") {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/kabupaten",
            { kdprop: this.state.dataxpelatihan.provinsi },
            this.configs,
          )
          .then((res) => {
            optionx = res.data.result.Data;
            const dataxkab = [];
            optionx.map((data) =>
              dataxkab.push({ value: data.id, label: data.name }),
            );
            this.setState({ dataxkab });
          })
          .catch((err) => {
            console.log(err);
          });
      }

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/detail_silabus",
          { id: dataxpelatihan.id_silabus },
          this.configs,
        )
        .then((res) => {
          this.setState({
            detailSilabus: {
              id: res.data.result.id_Silabus,
              nama_silabus: res.data.result.NamaSilabus,
              totalJp: res.data.result.Jml_jampelajaran,
            },
            isiSilabus: res.data.result.DataSilabus.map((elem, index) => {
              return {
                jam_pelajaran: elem.jam_pelajaran,
                materi: elem.materi,
                id_materi: elem.id + "_" + index,
              };
            }),
            pelatihanBySilabus: res.data.result.DataPelatihan,
            showViewSilabus: true,
            selnamasilabus: {
              ...this.state.selnamasilabus,
              jpsilabus: res.data.result?.Jml_jampelajaran,
            },
          });
        })
        .catch((err) => {
          console.log(err);
        });

      // WSML
      axios
        .get(
          process.env.REACT_APP_BASE_API_URI + "/wsml/get-themes",
          this.configs,
        )
        .then((res) => {
          const wsml_themes_tmp = res.data.result.result.Message.results.data;
          this.setState({ wsml_themes: wsml_themes_tmp }, () => {
            // Callback function to ensure the state is updated before further actions
            this.handleInitialCheck();
          });
        });

      //initial awal
      if (dataxpelatihan.komitmen === "Iya") {
        this.setState({ showingdeskripsi: true });
      }
      if (dataxpelatihan.metode_pelaksanaan === "Swakelola") {
        this.setState({ showingmetodepelaksanaan: false });
      }
      this.setState({
        selstatuspublish: {
          value: dataxpelatihan.status_publish,
          label:
            dataxpelatihan.status_publish == "1"
              ? "Publish"
              : dataxpelatihan.status_publish == "0"
                ? "Unpublish"
                : "Unlisted",
        },
      });
      this.setState({
        valuestatussubstansi: dataxpelatihan.status_substansi,
        valuestatuspublish: dataxpelatihan.status_publish,
        valuestatuspelatihan: dataxpelatihan.status_pelatihan,
      });
      this.reloadStepper();
      swal.close();
    } catch (err) {
      this.reloadStepper();
      console.log(err);
      const messagex =
        err.response?.data?.result?.Message ??
        "Terjadi kesalahan saat mengambil data.";
      swal
        .fire({
          title: messagex,
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  //initial WSML
  handleInitialCheck() {
    if (this.state.seltema.value !== "") {
      const label = this.state.seltema.label;

      // Check if the model is ready
      this.setState(
        { kuotapendaftar: this.state.dataxpelatihan.kuota_pendaftar },
        () => {
          if (this.state.wsml_themes.includes(label)) {
            this.setState({ isKuotaPendaftarVisible: false });
          } else {
            this.setState({ isKuotaPendaftarVisible: true });
          }
        },
      );
    }
  }

  handleChangeProgramAction = (e) => {
    // Cookies.set("program_dts", e.currentTarget.value, 1);
    this.setState({
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        program_dts: e.currentTarget.value,
      },
    });
  };

  handleChangeURLAction = (e) => {
    this.setState({
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        url_vicon: e.currentTarget.value,
      },
    });
  };

  handleChangeNilaiAction = (e) => {
    this.setState({
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        input_nilai: e.currentTarget.value,
      },
    });
  };

  handleChangeBatchAction = (e) => {
    this.setState({
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        batch: e.currentTarget.value,
      },
    });
  };

  handleChangeDisabilitasUmumAction = (e) => {
    this.setState({
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        umum: e.target.checked ? e.currentTarget.value : 0,
      },
    });
  };

  handleChangeAlurPendaftaranAction = (e) => {
    this.setState(
      {
        dataxpelatihan: {
          ...this.state.dataxpelatihan,
          alur_pendaftaran: e.currentTarget.value,
        },
      },
      () => {
        console.log("liar state", this.state.dataxpelatihan.alur_pendaftaran);
        if (
          this.state.dataxpelatihan.alur_pendaftaran ==
          "Test Substansi - Administrasi"
        ) {
          console.log("dalam state");
          this.setState({
            dataxpelatihan: {
              ...this.state.dataxpelatihan,
              subtansi_mulai: this.state.dataxpelatihan.pendaftaran_start,
              subtansi_selesai: this.state.dataxpelatihan.pendaftaran_end,
            },
          });
        }
      },
    );
  };

  handleChangeDisabilitasTunaNetraAction = (e) => {
    this.setState({
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        tuna_netra: e.target.checked ? e.currentTarget.value : 0,
      },
    });
  };

  handleChangeApakahMidTestAction = (e) => {
    if (e.currentTarget.value == "Iya") {
      this.setState({ showingMidTestDate: true });
    } else {
      this.setState({ showingMidTestDate: false });
    }
  };

  handleChangeDisabilitasTunaRunguAction = (e) => {
    this.setState({
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        tuna_rungu: e.target.checked ? e.currentTarget.value : 0,
      },
    });
  };

  handleChangeDisabilitasTunaDaksaAction = (e) => {
    this.setState({
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        tuna_daksa: e.target.checked ? e.currentTarget.value : 0,
      },
    });
  };

  handleChangeStatusKuotaAction = (e) => {
    this.setState({
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        status_kuota: e.currentTarget.value,
      },
    });
  };

  handleChangeSertifikasiAction(e) {
    console.log(e);
    this.setState({
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        sertifikasi: e.currentTarget.value,
      },
    });
  }

  handleChangeLPJAction(e) {
    this.setState({
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        lpj_peserta: e.currentTarget.value,
      },
    });
  }

  // handleChanged(e) {
  //   let config = {
  //     headers: {
  //       Authorization: "Bearer 19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp",
  //     },
  //   };
  //   const dataKab = { id: e.target.value };
  //   axios
  //     .post(process.env.REACT_APP_BASE_API_URI + "/kabupaten", dataKab, config)
  //     .then((res) => {
  //       const dataxkab = res.data.result.Data;
  //       this.setState({ dataxkab });
  //     });
  // }
  handleChangeKomitmenAction(e) {
    this.setState({
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        komitmen: e.target.value,
      },
    });
    if (e.target.value == "Iya") {
      this.setState({ showingdeskripsi: true });
    } else {
      this.setState({ showingdeskripsi: false });
    }
  }
  handleChangedDeskripsiKomitmen(value) {
    this.setState({ valuedeskripsikomitmen: value });
  }

  resetError() {
    let errors = {};
    errors["name_pelatihan"] = "";
    errors["deskripsi"] = "";
    this.setState({ errors: errors });
  }
  handleChangedSilabus(e) {
    const silabusfile = e.target.files[0];
    this.formDatax.append("silabus", silabusfile);
  }
  handleClickedMetodeAction(e) {
    this.setState({
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        metode_pelatihan: e.target.value,
      },
    });
    if (e.target.value == "Online") {
      this.setState({ showing: false });
    } else {
      this.setState({ showing: true });
    }
  }
  handleClickMetodePelaksanaanAction(e) {
    this.setState({
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        metode_pelaksanaan: e.target.value,
      },
    });
    if (e.target.value == "Swakelola") {
      this.setState({ showingmetodepelaksanaan: false });
    } else {
      this.setState({ showingmetodepelaksanaan: true });
    }
  }
  handleClickProgramDtsAction(e) {}
  handleDate(dateString) {
    if (moment(dateString).isValid()) {
      return moment(dateString).format("YYYY-MM-DD HH:mm:ss");
    }
    return dateString;
  }
  handleClickTambahFormAction(e) {
    this.setState({ showingembed: true });
    this.setState({ showingnonembed: false });
  }
  handleClickRefFormAction(e) {
    const dataSelForm = { start: 0, length: 1000 };
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/list-formbuilder",
        dataSelForm,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxselform = [];
        for (let i in optionx) {
          if (optionx[i].id == this.state.dataxpelatihan.form_pendaftaran_id) {
            this.setState({
              seljudulform: {
                value: optionx[i].id,
                label: optionx[i].judul_form,
              },
            });
          }
        }
        optionx.map((data) =>
          dataxselform.push({ value: data.id, label: data.judul_form }),
        );
        this.setState({ dataxselform });
        swal.close();
      })
      .catch((err) => {
        swal.fire({
          title: err.response.data.result.Message ?? "Terjadi kesalahan!",
          icon: "warning",
        });
      });
  }
  handleClickAddSilabusAction = () => {
    let lastId =
      this.state.isiSilabus[this.state.isiSilabus.length - 1].id_materi.split(
        "_",
      );
    lastId[1] = parseInt(lastId[1]) + 1;
    this.setState({
      isiSilabus: [
        ...this.state.isiSilabus,
        {
          id_materi: lastId.join("_"),
          jam_pelajaran: "",
          materi: "",
        },
      ],
    });
  };
  handleInputSilabus = (field, payload, id) => {
    this.state.isiSilabus.map((elem) => {
      if (elem.id_materi == id) {
        return (elem[field] = payload);
      }
    });
  };

  simpanPerubahanSilabus() {
    // create silabus data
    const jsonBody = [
      {
        id_silabus: this.state.selnamasilabus.value,
        jml_jp: this.state.selnamasilabus?.jpsilabus,
        judul: this.state.selnamasilabus?.label,
        id_user: Cookies.get("user_id"),
        id_akademi: this.state.dataxpelatihan.akademi_id,
        detail_json: this.state.isiSilabus.map((elem) => {
          return {
            name: elem.materi,
            jam_pelajaran: elem.jam_pelajaran,
          };
        }),
      },
    ];

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/update_silabus",
        jsonBody,
        this.configs,
      )
      .then((res) => {
        return axios.post(
          process.env.REACT_APP_BASE_API_URI + "/detail_silabus",
          {
            id: this.state.selnamasilabus.value,
          },
          this.configs,
        );
      })
      .then((res) => {
        if (res.data.result.Status) {
          this.setState({
            detailSilabus: {
              id: res.data.result.id_Silabus,
              nama_silabus: res.data.result.NamaSilabus,
              totalJp: res.data.result.Jml_jampelajaran,
            },
            pelatihanBySilabus: res.data.result.DataPelatihan,
            selnamasilabus: {
              ...this.state.selnamasilabus,
              label: res.data.result.NamaSilabus ?? "",
              jpsilabus: res.data.result.Jml_jampelajaran ?? 0,
            },
            isiSilabus: res.data.result.DataSilabus.map((elem, index) => {
              return {
                jam_pelajaran: elem.jam_pelajaran,
                materi: elem.materi,
                id_materi: elem.id + "_" + index,
              };
            }),
            showBtnNextW2: true,
            showingSelSilabus: true,
          });
        } else {
          throw Error(res.data.result.Message);
        }
        return axios.post(
          process.env.REACT_APP_BASE_API_URI + "/list_silabus",
          {
            mulai: 0,
            limit: 1000,
            id_akademi:
              this.state.selakademi?.value == null
                ? 0
                : this.state.selakademi?.value,
            id_tema:
              this.state.seltema?.value == null ? 0 : this.state.seltema?.value,
            cari: "",
            sort: "id desc",
          },
          this.configs,
        );
      })
      .then((res) => {
        if (res.data.result.Status) {
          const dataxselusesilabus = [];
          res.data.result.Data.map((data) =>
            dataxselusesilabus.push({
              value: data.id,
              label: data.judul_silabus,
            }),
          );
          this.setState({ dataxselusesilabus }, () => {
            swal.fire({
              title: res.data.result.Message ?? "Berhasil menyimpan data!",
              icon: "success",
            });
          });
        }
      })
      .catch((err) => {
        swal.fire({
          title: err.response.data.result.Message ?? "Terjadi kesalahan!",
          icon: "warning",
        });
      });
  }

  handleClickSaveSilabusAction = () => {
    if (this.state.pelatihanBySilabus.length > 0) {
      this.viewPelatihanRef.current.click();
    } else {
      this.simpanPerubahanSilabus();
    }
  };
  handleClickDeleteSilabusAction = (index) => {
    let fields = this.state.isiSilabus;
    fields.splice(index, 1);
    this.setState({ isiSilabus: fields });
  };

  reloadStepper() {
    const stepper = window?.document?.getElementById("stepper-script");
    if (stepper) {
      stepper.remove();
    }
    const script = window?.document?.createElement("script");
    script.setAttribute("id", "stepper-script");
    script.async = true;
    script.src = "/assets/js/custom/modals/pelatihan-edit.js";

    //For head
    // document.head.appendChild(script);

    // For body
    window?.document?.body.appendChild(script);

    // For component
    // this.div.appendChild(script);
  }

  handleReject(e) {
    window.history.back();
  }
  handleSubmitAction(e) {
    e.preventDefault();
    this.setState({ isLoading: true });

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    const dataForm = new FormData(e.currentTarget);
    const dataFormSubmit = new FormData();
    let pendaftaran_mulai = this.handleDate(
      this.state.dataxpelatihan.pendaftaran_start,
    );
    let pendaftaran_selesai = this.handleDate(
      this.state.dataxpelatihan.pendaftaran_end,
    );
    let pelatihan_mulai = this.handleDate(
      this.state.dataxpelatihan.pelatihan_start,
    );
    let pelatihan_selesai = this.handleDate(
      this.state.dataxpelatihan.pelatihan_end,
    );
    dataFormSubmit.append("id", this.state.dataxpelatihan.id);
    dataFormSubmit.append("program_dts", this.state.dataxpelatihan.program_dts);
    dataFormSubmit.append("input_nilai", this.state.dataxpelatihan.input_nilai);
    dataFormSubmit.append("name", this.state.dataxpelatihan.pelatihan);
    dataFormSubmit.append(
      "level_pelatihan",
      this.state.dataxpelatihan.level_pelatihan_id,
    );
    dataFormSubmit.append("akademi_id", this.state.dataxpelatihan.akademi_id);
    dataFormSubmit.append("tema_id", this.state.dataxpelatihan.tema_id);
    dataFormSubmit.append(
      "metode_pelaksanaan",
      this.state.dataxpelatihan.metode_pelaksanaan,
    );
    if (this.state.dataxpelatihan.metode_pelaksanaan == "Swakelola") {
      dataFormSubmit.append("mitra", 0);
    } else {
      dataFormSubmit.append("mitra", this.state.dataxpelatihan.mitra_id);
    }
    dataFormSubmit.append(
      "id_penyelenggara",
      this.state.dataxpelatihan.id_penyelenggara,
    );
    dataFormSubmit.append(
      "deskripsi",
      this.state.dataxpelatihan.deskripsi.replaceAll("'", "''"),
    );
    dataFormSubmit.append("pendaftaran_mulai", pendaftaran_mulai);
    dataFormSubmit.append("pendaftaran_selesai", pendaftaran_selesai);
    dataFormSubmit.append("pelatihan_mulai", pelatihan_mulai);
    dataFormSubmit.append("pelatihan_selesai", pelatihan_selesai);
    // tanggal subtansi
    if (this.state.dataxpelatihan.alur_pendaftaran != "Administrasi") {
      dataFormSubmit.append(
        "administrasi_mulai",
        this.handleDate(this.state.dataxpelatihan.administrasi_mulai),
      );
      dataFormSubmit.append(
        "administrasi_selesai",
        this.handleDate(this.state.dataxpelatihan.administrasi_selesai),
      );
      dataFormSubmit.append(
        "subtansi_mulai",
        this.handleDate(this.state.dataxpelatihan.subtansi_mulai),
      );
      dataFormSubmit.append(
        "subtansi_selesai",
        this.handleDate(this.state.dataxpelatihan.subtansi_selesai),
      );
    }
    dataFormSubmit.append(
      "kuota_pendaftar",
      this.state.dataxpelatihan.kuota_pendaftar,
    );
    dataFormSubmit.append(
      "kuota_peserta",
      this.state.dataxpelatihan.kuota_peserta,
    );
    dataFormSubmit.append(
      "status_kuota",
      this.state.dataxpelatihan.status_kuota,
    );
    dataFormSubmit.append(
      "alur_pendaftaran",
      this.state.dataxpelatihan.alur_pendaftaran,
    );
    dataFormSubmit.append("sertifikasi", this.state.dataxpelatihan.sertifikasi);
    dataFormSubmit.append(
      "pelaksana_assement_id",
      this.state.dataxpelatihan.pelaksana_assement_id ?? 0,
    );
    dataFormSubmit.append(
      "lpj_peserta",
      this.state.dataxpelatihan.lpj_peserta ?? "Tidak",
    );
    dataFormSubmit.append(
      "metode_pelatihan",
      this.state.dataxpelatihan.metode_pelatihan,
    );
    if (this.state.dataxpelatihan.metode_pelatihan?.includes("Offline")) {
      dataFormSubmit.append(
        "alamat",
        this.state.dataxpelatihan.alamat.replaceAll("'", "''"),
      );
      dataFormSubmit.append("provinsi", this.state.dataxpelatihan.provinsi);
      dataFormSubmit.append("kabupaten", this.state.dataxpelatihan.kabupaten);
    } else {
      dataFormSubmit.append("alamat", "online");
      dataFormSubmit.append("provinsi", "online");
      dataFormSubmit.append("kabupaten", "online");
    }
    if (this.state.dataxpelatihan.metode_pelatihan?.includes("Online")) {
      dataFormSubmit.append("url_vicon", this.state.dataxpelatihan.url_vicon);
    }
    dataFormSubmit.append("zonasi", this.state.selzonasi.value);
    dataFormSubmit.append("batch", this.state.dataxpelatihan.batch);
    dataFormSubmit.append("umum", this.state.dataxpelatihan.umum);
    dataFormSubmit.append("tuna_netra", this.state.dataxpelatihan.tuna_netra);
    dataFormSubmit.append("tuna_rungu", this.state.dataxpelatihan.tuna_rungu);
    dataFormSubmit.append("tuna_daksa", this.state.dataxpelatihan.tuna_daksa);
    // file silabus
    if (this.state.fileSilabus) {
      dataFormSubmit.append("silabus", this.state.fileSilabus.file);
    }
    dataFormSubmit.append(
      "status_substansi",
      this.state.dataxpelatihan.status_substansi,
    );
    dataFormSubmit.append(
      "status_pelatihan",
      this.state.dataxpelatihan.status_pelatihan,
    );
    if (this.state.showingMidTestDate) {
      dataFormSubmit.append(
        "midtest_mulai",
        this.handleDate(this.state.dataxpelatihan.midtest_mulai),
      );
    }
    dataFormSubmit.append(
      "silabus_header_id",
      this.state.detailSilabus?.id ?? this.state.detailSilabus.id,
    );
    dataFormSubmit.append("user_by", Cookies.get("user_id"));

    dataFormSubmit.append("komitmen", this.state.dataxpelatihan.komitmen);
    // dataFormSubmit.append(
    //   "deskripsi_komitmen",
    //   this.state.dataxpelatihan.komitmen == "Iya"
    //     ? this.state.valuedeskripsikomitmen.replaceAll("'", "''")
    //     : ""
    // );
    dataFormSubmit.append(
      "deskripsi_komitmen",
      this.state.valuedeskripsikomitmen.replaceAll("'", "''"),
    );
    dataFormSubmit.append(
      "form_pendaftaran_id",
      this.state.dataxpelatihan.form_pendaftaran_id,
    );
    dataFormSubmit.append(
      "pelatian_id",
      this.state.dataxpelatihan.pelatian_data_id,
    );
    dataFormSubmit.append("status_publish", this.state.selstatuspublish.value);

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/editpelatihanz",
        dataFormSubmit,
        this.configs,
      )
      .then((res) => {
        this.setState({ isLoading: false });
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          swal
            .fire({
              title: messagex,
              icon: "success",
              allowOutsideClick: false,
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                window.location = "/pelatihan/pelatihan";
              }
            });
        } else {
          this.setState({ isLoading: false });
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      })
      .catch((error) => {
        this.setState({ isLoading: false });
        let messagex = "";
        if (error.response?.data?.result?.Message !== undefined) {
          messagex = error.response.data?.result?.Message;
        } else {
          messagex = this.parsingFormErrorValidation(error.response.data);
        }
        swal
          .fire({
            title: "Terjadi kesalahan.",
            html: messagex,
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
            }
          });
      });
  }

  cariFormBuilder(idForm) {
    const dataFormSubmit = new FormData();
    dataFormSubmit.append("id", idForm);
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/cari-formbuilder",
        dataFormSubmit,
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          const dataxpreview = res.data.result.detail;
          const dataxpreviewtitle = res.data.result.utama[0].judul_form;

          this.setState({
            dataxpreview,
            dataxpreviewtitle,
            formPendaftaran: res.data.result,
            showViewForm: true,
          });
          swal.close();
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                //this.handleReload();
              }
            });
        }
      })
      .catch((err) => {
        swal.fire({
          title:
            err.response.data.result.Message ??
            "Terjadi kesalahan saat mengambil form",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      });
  }

  handleChangeJudulFormAction = (e) => {
    const { value, options, selectedIndex } = e.target;
    this.setState({
      seljudulform: {
        value: value,
        label: options[selectedIndex].innerHTML,
      },
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        form_pendaftaran_id: value,
      },
    });
    this.setState({ showBtnNextW2: true });

    this.cariFormBuilder(value);
  };

  isSertifikasi = (id) => {
    const sertifikasi = this.state.selSertifikasiList.filter(
      (elem) => elem.id == id && elem.name !== "Tidak Ada",
    );
    return sertifikasi.length > 0;
  };
  handleChangeLvlPelatihanAction = (e) => {
    const { options, value, selectedIndex } = e.target;
    this.setState({
      sellvlpelatihan: {
        value: value,
        label: options[selectedIndex].innerHTML,
      },
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        level_pelatihan: options[selectedIndex].innerHTML,
        level_pelatihan_id: value,
      },
    });
  };
  handleChangeAkademiAction = (e) => {
    const { options, value, selectedIndex } = e.target;
    this.setState({
      selakademi: { value: value, label: options[selectedIndex].innerHTML },
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        akademi: options[selectedIndex].innerHTML,
        akademi_id: value,
      },
    });
    const dataBody = { start: 1, rows: 100, id: value };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/cari_tema_byakademi",
        dataBody,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxtema = [];
        for (let i in optionx) {
          if (optionx[i].id == this.state.dataxpelatihan.tema_id) {
            this.setState({
              seltema: { value: optionx[i].id, label: optionx[i].name },
            });
          }
        }
        optionx.map((data) =>
          dataxtema.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxtema });
        this.setState({ seltema: [] });
      })
      .catch((error) => {
        const dataxtema = [];
        this.setState({ dataxtema });
        this.setState({ seltema: [] });
        let messagex = error.response.data.result.Message;
        console.log(error);
        // swal
        //   .fire({
        //     title: messagex,
        //     icon: "warning",
        //     confirmButtonText: "Ok",
        //   })
        //   .then((result) => {
        //     if (result.isConfirmed) {
        //     }
        //   });
      });
  };
  handleChangeTemaAction = (e) => {
    const { options, value, selectedIndex } = e.target;
    this.setState({
      seltema: { value: value, label: options[selectedIndex].innerHTML },
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        tema: options[selectedIndex].innerHTML,
        tema_id: value,
      },
    });
  };
  handleOptionChangeAction = (e) => {
    const selectedValue = parseInt(e, 10);
    let label = "";
    const matchingObject = this.state.dataxtema.find(
      (opt) => opt.value === selectedValue,
    );

    if (matchingObject) {
      label = matchingObject.label;
    }

    // Check if the model is ready
    if (this.state.wsml_themes.includes(label)) {
      // console.log(`${label} is in the list.`);
      this.setState({ kuotapendaftar: "999999" }, () => {
        // This code will run after kuotapendaftar has been updated
        this.setState({ isKuotaPendaftarVisible: false });
      });
    } else {
      // console.log(`${label} is in the list.`);
      this.setState({ kuotapendaftar: "" }, () => {
        // This code will run after kuotapendaftar has been updated
        this.setState({ isKuotaPendaftarVisible: true });
      });
    }
  };
  // handleChangeTemaAction = (selectedOption) => {
  //   this.setState({seltema:{value:selectedOption.value, label:selectedOption.label}});
  // }
  handleChangePenyelenggaraAction = (e) => {
    const { options, value, selectedIndex } = e.target;
    this.setState({
      selpenyelenggara: {
        value: value,
        label: options[selectedIndex].innerHTML,
      },
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        penyelenggara: options[selectedIndex].innerHTML,
        id_penyelenggara: value,
      },
    });
  };
  handleChangeMitraAction = (e) => {
    const { options, value, selectedIndex } = e.target;
    this.setState({
      selmitra: { value: value, label: options[selectedIndex].innerHTML },
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        mitra_name: options[selectedIndex].innerHTML,
        mitra_id: value,
      },
    });
  };
  handleChangeZonasiAction = (e) => {
    const { options, value, selectedIndex } = e.target;
    this.setState({
      selzonasi: { value: value, label: options[selectedIndex].innerHTML },
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        zonasi: options[selectedIndex].innerHTML,
        zonasi_id: value,
      },
    });
  };
  handleChangeProvAction = (e) => {
    const { options, value, selectedIndex } = e.target;
    this.setState({
      selprovinsi: { value: value, label: options[selectedIndex].innerHTML },
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        nm_prov: options[selectedIndex].innerHTML,
        provinsi: value,
      },
    });
    const dataKab = { kdprop: value };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/kabupaten",
        dataKab,
        this.configs,
      )
      .then((res) => {
        this.setState({ isDisabledKab: false });
        const optionx = res.data.result.Data;
        const dataxkab = [];
        optionx.map((data) =>
          dataxkab.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxkab });
        this.setState({ selkabupaten: [] });
      });
  };
  handleChangeKabupatenAction = (e) => {
    const { options, value, selectedIndex } = e.target;
    this.setState({
      selkabupaten: {
        value: value,
        label: options[selectedIndex].innerHTML,
      },
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        nm_kabupaten: options[selectedIndex].innerHTML,
        kabupaten: value,
      },
    });
  };
  handleChangeDescriptionAction(value) {
    document.getElementsByName("deskripsi_hidden")[0].value = value;
    this.setState({
      deskripsi: value,
      dataxpelatihan: { ...this.state.dataxpelatihan, deskripsi: value },
    });
  }

  handleDelete(id) {
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          let data = {
            id: id,
          };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/delpelatihan",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (true == statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    window.location.href = "/pelatihan/pelatihan";
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {});
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            });
        }
      });
  }

  handleChangeSilabusAction(e) {
    const { value, options, selectedIndex } = e.target;
    this.setState({
      dataxpelatihan: {
        ...this.state.dataxpelatihan,
        id_silabus: value,
        judul_silabus: options[selectedIndex].innerHTML,
      },
      selnamasilabus: {
        ...this.state.selnamasilabus,
        value: value,
        label: options[selectedIndex].innerHTML,
      },
    });
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/detail_silabus",
        {
          id: value,
        },
        this.configs,
      )
      .then((res) => {
        this.setState(
          {
            detailSilabus: {
              id: res.data.result.id_Silabus,
              nama_silabus: res.data.result.NamaSilabus,
              totalJp: res.data.result.Jml_jampelajaran,
            },
            isiSilabus: res.data.result.DataSilabus.map((elem, index) => {
              return {
                jam_pelajaran: elem.jam_pelajaran,
                materi: elem.materi,
                id_materi: elem.id + "_" + index,
              };
            }),
            pelatihanBySilabus: res.data.result.DataPelatihan,
            showBtnNextW2: true,
            selnamasilabus: {
              ...this.state.selnamasilabus,
              jpsilabus: res.data.result.Jml_jampelajaran ?? 0,
            },
          },
          () => {
            this.setState({ showViewSilabus: true });
            swal.close();
          },
        );
      })
      .catch((err) => {
        swal.fire({
          title:
            err.response?.data?.result?.Message ??
            "Terjadi kesalahan saat mengambil silabus",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      });
  }

  handleChangeAlamatAction(value) {
    document.getElementsByName("alamat_hidden")[0].value = value;
    this.setState({
      dataxpelatihan: { ...this.state.dataxpelatihan, alamat: value },
    });
  }
  handleChangeStatusPublishAction = (e) => {
    const { value, selectedIndex, options } = e.target;
    this.setState({ flagstatuspublishedit: true });
    this.setState({
      selstatuspublish: {
        value: value,
        label: options[selectedIndex].innerHTML,
      },
    });
  };
  handleClickRejectForceMajureAction(e) {
    swal
      .fire({
        title: "Apakah anda yakin ingin membatalkan pelatihan?",
        text: "Status Pendaftaran Peserta Juga Akan Dibatalkan",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, batalkan!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          let data = {
            pelatian_id: this.state.dataxpelatihan.id,
          };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/pelatihanz/cancel",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Data;
              if (statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    allowOutsideClick: false,
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      window.location = "/pelatihan/pelatihan";
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            });
        }
      });
  }
  render() {
    return (
      <div>
        <a
          href="#"
          hidden
          data-bs-toggle="modal"
          data-bs-target="#view-pelatihan"
          ref={this.viewPelatihanRef}
        ></a>
        <div className="modal fade" tabindex="-1" id="view-pelatihan">
          <div className="modal-dialog modal-md">
            <div className="modal-content">
              <div className="modal-header pe-0 pb-0">
                <div className="row w-100">
                  <div className="col-12 d-flex w-100 justify-content-between">
                    <h5 className="modal-title">Perhatian</h5>
                    <div
                      className="btn btn-icon btn-sm btn-active-light-primary"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                    >
                      <span className="svg-icon svg-icon-2x">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                          fill="none"
                        >
                          <rect
                            opacity="0.5"
                            x="6"
                            y="17.3137"
                            width="16"
                            height="2"
                            rx="1"
                            transform="rotate(-45 6 17.3137)"
                            fill="currentColor"
                          />
                          <rect
                            x="7.41422"
                            y="6"
                            width="16"
                            height="2"
                            rx="1"
                            transform="rotate(45 7.41422 6)"
                            fill="currentColor"
                          />
                        </svg>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="modal-body">
                <div className="card">
                  <div className="row justify-content-start">
                    <div className="col-12">
                      <h4 className="fw-bolder">
                        {capitalWord(this.state.detailSilabus.nama_silabus)}{" "}
                        <span className="text-muted small">
                          ({zeroDecimalValue(this.state.detailSilabus.totalJp)}{" "}
                          Jam pelajaran)
                        </span>
                      </h4>
                      {this.state.pelatihanBySilabus.length > 0 &&
                        this.state.pelatihanBySilabus != null && (
                          <span className="text-muted">
                            Daftar pelatihan yang menggunakan silabus
                          </span>
                        )}
                    </div>
                    <div className="col-12">
                      {this.state.pelatihanBySilabus.length > 0 &&
                      this.state.pelatihanBySilabus != null ? (
                        this.state.pelatihanBySilabus.map((elem) => (
                          <div className="d-flex align-items-center bg-light-primary rounded p-5 mb-2">
                            <span className="svg-icon svg-icon-primary me-5">
                              <span className="svg-icon svg-icon-1">
                                <svg
                                  width="24"
                                  height="24"
                                  viewBox="0 0 24 24"
                                  fill="none"
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="mh-50px"
                                >
                                  <path
                                    opacity="0.3"
                                    d="M21.25 18.525L13.05 21.825C12.35 22.125 11.65 22.125 10.95 21.825L2.75 18.525C1.75 18.125 1.75 16.725 2.75 16.325L4.04999 15.825L10.25 18.325C10.85 18.525 11.45 18.625 12.05 18.625C12.65 18.625 13.25 18.525 13.85 18.325L20.05 15.825L21.35 16.325C22.35 16.725 22.35 18.125 21.25 18.525ZM13.05 16.425L21.25 13.125C22.25 12.725 22.25 11.325 21.25 10.925L13.05 7.62502C12.35 7.32502 11.65 7.32502 10.95 7.62502L2.75 10.925C1.75 11.325 1.75 12.725 2.75 13.125L10.95 16.425C11.65 16.725 12.45 16.725 13.05 16.425Z"
                                    fill="currentColor"
                                  ></path>
                                  <path
                                    d="M11.05 11.025L2.84998 7.725C1.84998 7.325 1.84998 5.925 2.84998 5.525L11.05 2.225C11.75 1.925 12.45 1.925 13.15 2.225L21.35 5.525C22.35 5.925 22.35 7.325 21.35 7.725L13.05 11.025C12.45 11.325 11.65 11.325 11.05 11.025Z"
                                    fill="currentColor"
                                  ></path>
                                </svg>
                              </span>
                            </span>
                            <div className="flex-grow-1 me-2">
                              <a
                                href="#"
                                className="fw-bold text-gray-800 text-hover-success fs-6"
                              >
                                {elem.nama_pelatihan}
                              </a>
                              <span className="text-muted fw-semibold d-block">
                                {`${elem.nama_tema} - ${elem.nama_akademi}`}
                              </span>
                            </div>
                          </div>
                        ))
                      ) : (
                        <div
                          className="text-muted"
                          style={{ textAlign: "center" }}
                        >
                          Tidak ada pelatihan yang menggunakan silabus
                          <em>"{this.state.detailSilabus.nama_silabus}"</em>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>

              <div className="modal-footer">
                <button
                  className="btn btn-light btn-sm"
                  data-bs-dismiss="modal"
                >
                  Close
                </button>
                <button
                  className="btn btn-light btn-sm btn-primary"
                  data-bs-dismiss="modal"
                  onClick={() => {
                    this.simpanPerubahanSilabus();
                  }}
                >
                  Ya, Simpan
                </button>
              </div>
            </div>
          </div>
        </div>
        <input
          type="hidden"
          name="csrf-token"
          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
        />
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Edit Pelatihan
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>

                <button
                  className="btn btn-sm btn-danger btn-active-light-info me-2"
                  onClick={() => {
                    this.handleDelete(this.state.dataxpelatihan.id);
                  }}
                >
                  <i className="bi bi-trash-fill text-white"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div
                    className="stepper stepper-links mb-n3"
                    id="kt_pelatihan_edit_stepper"
                  >
                    <div className="col-lg-12 mt-7">
                      <div className="card border">
                        <div className="card-header">
                          <div className="card-title">
                            <div className="stepper-nav flex-wrap mb-n4">
                              <div
                                className="stepper-item ms-0 me-3 my-2 current"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label ms-0">
                                    <h3 className="stepper-title fs-6">
                                      Informasi Pelatihan
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Jadwal
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Silabus
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Form Pendaftaran
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Form Komitmen
                                    </h3>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="card-body">
                          <form
                            action="#"
                            onSubmit={this.handleSubmit}
                            id="kt_pelatihan_edit_form"
                          >
                            <div
                              className="flex-column current"
                              data-kt-stepper-element="content"
                            >
                              <div className="row mt-7">
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Kategori Program Pelatihan
                                  </label>
                                  <div className="d-flex">
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="program_dts"
                                        value={1}
                                        id="program_dts1"
                                        onChange={this.handleChangeProgram}
                                        checked={
                                          this.state.dataxpelatihan
                                            .program_dts == "1"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="program_dts1"
                                      >
                                        Digital Talent Scholarship
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="program_dts"
                                        value={0}
                                        id="program_dts2"
                                        onChange={this.handleChangeProgram}
                                        checked={
                                          this.state.dataxpelatihan
                                            .program_dts == "0"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="program_dts2"
                                      >
                                        Lainnya
                                      </label>
                                    </div>
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["program_dts"]}
                                  </span>
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Nama Pelatihan
                                  </label>
                                  <input
                                    className="form-control form-control-sm"
                                    placeholder="Masukkan nama pelatihan"
                                    name="name_pelatihan"
                                    defaultValue={
                                      this.state.dataxpelatihan.pelatihan ?? ""
                                    }
                                    onBlur={(e) => {
                                      this.setState({
                                        dataxpelatihan: {
                                          ...this.state.dataxpelatihan,
                                          pelatihan: e.target.value,
                                        },
                                      });
                                    }}
                                  />
                                  <span className="text-muted fs-8 d-block">
                                    Nama tidak perlu mengandung batch karena
                                    batch akan otomatis tampil sesuai isian pada
                                    field batch
                                  </span>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["name_pelatihan"]}
                                  </span>
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Batch Pelatihan
                                  </label>
                                  <input
                                    type="number"
                                    className="form-control form-control-sm"
                                    placeholder="Masukkan batch"
                                    name="batch"
                                    defaultValue={
                                      this.state.dataxpelatihan.batch
                                    }
                                    onBlur={(e) => {
                                      this.setState({
                                        dataxpelatihan: {
                                          ...this.state.dataxpelatihan,
                                          batch: e.target.value,
                                        },
                                      });
                                    }}
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["batch"]}
                                  </span>
                                </div>

                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Pilih Akademi
                                  </label>
                                  {/* <Select
                                      id="id_akademi"
                                      name="akademi_id"
                                      placeholder="Silahkan pilih"
                                      noOptionsMessage={({ inputValue }) =>
                                        !inputValue
                                          ? this.state.dataxakademi
                                          : "Data tidak tersedia"
                                      }
                                      className="form-select-sm selectpicker p-0"
                                      options={this.state.dataxakademi}
                                      value={this.state.selakademi}
                                      onChange={this.handleChangeAkademi}
                                    /> */}
                                  <select
                                    className="form-select form-select-sm"
                                    name="akademi_id"
                                    value={this.state.selakademi?.value ?? ""}
                                    onChange={this.handleChangeAkademi}
                                  >
                                    <option
                                      value=""
                                      style={{ display: "none" }}
                                    >
                                      Silahkan pilih
                                    </option>
                                    {this.state.dataxakademi &&
                                      this.state.dataxakademi.map(
                                        (opt, index) => (
                                          <option
                                            key={`akademi_${index}`}
                                            value={opt.value}
                                          >
                                            {opt.label}
                                          </option>
                                        ),
                                      )}
                                  </select>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["akademi_id"]}
                                  </span>
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Pilih Tema
                                  </label>
                                  {/* <Select
                                      name="tema_id"
                                      placeholder="Silahkan pilih"
                                      noOptionsMessage={({ inputValue }) =>
                                        !inputValue
                                          ? this.state.dataxtema
                                          : "Data tidak tersedia"
                                      }
                                      className="form-select-sm selectpicker p-0"
                                      options={this.state.dataxtema}
                                      //isDisabled={this.state.isDisabled}
                                      value={this.state.seltema}
                                      onChange={this.handleChangeTema}
                                    /> */}
                                  <select
                                    className="form-select form-select-sm"
                                    name="tema"
                                    value={this.state.seltema?.value ?? ""}
                                    // onChange={this.handleChangeTema}
                                    onChange={(event) => {
                                      this.handleChangeTema(event);
                                      // Add the function you want to run here
                                      this.handleOptionChange(
                                        event.target.value,
                                      );
                                    }}
                                    disabled={this.state.dataxtema.length <= 0}
                                  >
                                    <option
                                      value=""
                                      style={{ display: "none" }}
                                    >
                                      Silahkan pilih
                                    </option>
                                    {this.state.dataxtema &&
                                      this.state.dataxtema.map((opt, index) => (
                                        <option
                                          key={`tema_${index}`}
                                          value={opt.value}
                                        >
                                          {opt.label}
                                        </option>
                                      ))}
                                  </select>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["tema"]}
                                  </span>
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Deskripsi Pelatihan
                                  </label>
                                  {/* <textarea className="form-control form-control-sm" placeholder="Masukkan deksripsi pelatihan" name="deskripsi" value={this.state.fields["deskripsi"]}></textarea> */}
                                  <CKEditor
                                    editor={Editor}
                                    data={
                                      this.state.dataxpelatihan.deskripsi ?? ""
                                    }
                                    name="deskripsi"
                                    onReady={(editor) => {}}
                                    config={{
                                      ckfinder: {
                                        // Upload the images to the server using the CKFinder QuickUpload command.
                                        uploadUrl:
                                          process.env.REACT_APP_BASE_API_URI +
                                          "/publikasi/ckeditor-upload-image",
                                      },
                                    }}
                                    onBlur={(event, editor) => {
                                      const data = editor.getData();
                                      this.handleChangeDescription(data);
                                    }}
                                    onFocus={(event, editor) => {}}
                                  />
                                  <input
                                    type="hidden"
                                    name="deskripsi_hidden"
                                    // value={this.state.dataxpelatihan.deskripsi}
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["deskripsi"]}
                                  </span>
                                </div>
                                <div className="col-lg-12">
                                  <div className="mb-7 fv-row">
                                    <div
                                      id="upload_silabus_container"
                                      style={{
                                        display:
                                          this.state.dataxpelatihan.silabus ==
                                          null
                                            ? ""
                                            : "none",
                                      }}
                                    >
                                      <label className="form-label required">
                                        Upload Silabus
                                      </label>
                                      <input
                                        type="file"
                                        className="form-control form-control-sm mb-2"
                                        name="upload_silabus"
                                        accept=".pdf"
                                        ref={this.fileSilabusRef}
                                        onChange={(e) => {
                                          const fileSilabusInput = {
                                            file:
                                              e.currentTarget.files[0] ?? null,
                                            namaFile:
                                              e.currentTarget.files[0]?.name ??
                                              "",
                                          };
                                          this.setState({
                                            fileSilabus: fileSilabusInput,
                                            dataxpelatihan: {
                                              ...this.state.dataxpelatihan,
                                              silabus:
                                                fileSilabusInput.namaFile,
                                            },
                                          });
                                        }}
                                      />
                                      <small className="text-muted">
                                        Format File (.pdf), Max 10 MB
                                      </small>
                                      <br />
                                    </div>
                                    {this.state.dataxpelatihan.silabus && (
                                      <div
                                        className="alert alert-success alert-dismissible fade show mt-1"
                                        role="alert"
                                      >
                                        File silabus: &nbsp;
                                        {!this.state.fileSilabus ? (
                                          <a
                                            href={
                                              this.state.dataxpelatihan.silabus
                                            }
                                            className="alert-link"
                                            target="_blank"
                                          >
                                            <strong>
                                              {
                                                this.state.dataxpelatihan
                                                  .silabus
                                              }
                                            </strong>
                                          </a>
                                        ) : (
                                          <strong>
                                            {this.state.dataxpelatihan.silabus}
                                          </strong>
                                        )}
                                        <button
                                          type="button"
                                          className="btn-close"
                                          data-bs-dismiss="alert"
                                          aria-label="Close"
                                          onClick={(e) => {
                                            e.preventDefault();
                                            this.fileSilabusRef.current.value =
                                              null;
                                            this.setState({
                                              dataxpelatihan: {
                                                ...this.state.dataxpelatihan,
                                                silabus: null,
                                              },
                                            });
                                          }}
                                        ></button>
                                      </div>
                                    )}
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["upload_silabus"]}
                                    </span>
                                  </div>
                                </div>
                                <div>
                                  <div className="my-8 border-top mx-0"></div>
                                </div>
                                <h2 className="fs-5 text-muted mb-3">
                                  Level & Sertifikasi Pelatihan
                                </h2>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Level Pelatihan
                                  </label>
                                  <select
                                    className="form-select form-select-sm"
                                    name="level_pelatihan"
                                    value={
                                      this.state.sellvlpelatihan?.value ?? ""
                                    }
                                    onChange={this.handleChangeLvlPelatihan}
                                  >
                                    <option
                                      value=""
                                      style={{ display: "none" }}
                                    >
                                      Silahkan pilih
                                    </option>
                                    {this.state.dataxlevelpelatihan &&
                                      this.state.dataxlevelpelatihan.map(
                                        (opt, index) => (
                                          <option
                                            key={`level_${index}`}
                                            value={opt.value}
                                          >
                                            {opt.label}
                                          </option>
                                        ),
                                      )}
                                  </select>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["level_pelatihan"]}
                                  </span>
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Sertifikasi
                                  </label>
                                  <div className="d-flex">
                                    {this.state.selSertifikasiList?.length >
                                      0 &&
                                      this.state.selSertifikasiList.map(
                                        (elem) => (
                                          <div
                                            key={elem.id}
                                            className="form-check form-check-sm form-check-custom form-check-solid me-5"
                                          >
                                            <input
                                              className="form-check-input"
                                              type="radio"
                                              name="sertifikasi"
                                              value={elem.id}
                                              id={"sertifikasi_" + elem.id}
                                              onChange={
                                                this.handleChangeSertifikasi
                                              }
                                              checked={
                                                this.state.dataxpelatihan
                                                  .sertifikasi == elem.id
                                              }
                                            />
                                            <label
                                              className="form-check-label"
                                              htmlFor={"sertifikasi_" + elem.id}
                                            >
                                              {elem.name}
                                            </label>
                                          </div>
                                        ),
                                      )}
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["sertifikasi"]}
                                  </span>
                                </div>

                                <div
                                  className="col-lg-12 mb-7 fv-row"
                                  style={{
                                    display: this.isSertifikasi(
                                      this.state.dataxpelatihan.sertifikasi,
                                    )
                                      ? ""
                                      : "none",
                                  }}
                                >
                                  <label className="form-label required">
                                    Institusi / LSP yang melakukan uji
                                    kompetensi
                                  </label>
                                  <select
                                    className="form-select form-select-sm"
                                    name="pelaksana_assesment"
                                    value={
                                      this.state.dataxpelatihan
                                        .pelaksana_assement_id ?? ""
                                    }
                                    onChange={
                                      this.handleChangePelaksanaAssesments
                                    }
                                  >
                                    <option
                                      value=""
                                      style={{ display: "none" }}
                                    >
                                      Silahkan pilih
                                    </option>
                                    {this.state.pelaksanaassesmentsList &&
                                      this.state.pelaksanaassesmentsList.map(
                                        (opt, index) => (
                                          <option
                                            key={`pelaksana_assesment_${index}`}
                                            value={opt.id}
                                          >
                                            {opt.value}
                                          </option>
                                        ),
                                      )}
                                  </select>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["pelaksana_assesment"]}
                                  </span>
                                </div>

                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Apakah Sertifikat Completion/Kelulusan
                                    mencantumkan nilai?
                                  </label>
                                  <div className="d-flex">
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="input_nilai"
                                        value={1}
                                        id="input_nilai1"
                                        onChange={this.handleChangeNilai}
                                        checked={
                                          this.state.dataxpelatihan
                                            .input_nilai == 1
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="input_nilai1"
                                      >
                                        Iya
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="input_nilai"
                                        value={0}
                                        id="input_nilai2"
                                        onChange={this.handleChangeNilai}
                                        checked={
                                          this.state.dataxpelatihan
                                            .input_nilai == 0
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="input_nilai2"
                                      >
                                        Tidak
                                      </label>
                                    </div>
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["input_nilai"]}
                                  </span>
                                </div>
                                {/* <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    LPJ Peserta
                                  </label>
                                  <div className="d-flex">
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="lpj_peserta"
                                        value="Iya"
                                        id="lpj_peserta1"
                                        onChange={this.handleChangeLPJ}
                                        checked={
                                          this.state.dataxpelatihan
                                            .lpj_peserta === "Iya"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="lpj_peserta1"
                                      >
                                        Iya
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="lpj_peserta"
                                        value="Tidak"
                                        id="lpj_peserta2"
                                        onChange={this.handleChangeLPJ}
                                        checked={
                                          this.state.dataxpelatihan
                                            .lpj_peserta === "Tidak"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="lpj_peserta2"
                                      >
                                        Tidak
                                      </label>
                                    </div>
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["lpj_peserta"]}
                                  </span>
                                </div> */}

                                <div>
                                  <div className="my-8 border-top mx-0"></div>
                                </div>
                                <h2 className="fs-5 text-muted mb-5">
                                  Pelaksanaan Pelatihan
                                </h2>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Kategori Pelaksanaan
                                  </label>
                                  <div className="d-flex">
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="metode_pelaksanaan"
                                        value="Swakelola"
                                        id="metode_pelaksanaan1"
                                        onChange={
                                          this.handleClickMetodePelaksanaan
                                        }
                                        checked={
                                          this.state.dataxpelatihan
                                            .metode_pelaksanaan == "Swakelola"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="metode_pelaksanaan1"
                                      >
                                        Swakelola
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="metode_pelaksanaan"
                                        value="Mitra"
                                        id="metode_pelaksanaan2"
                                        onChange={
                                          this.handleClickMetodePelaksanaan
                                        }
                                        checked={
                                          this.state.dataxpelatihan
                                            .metode_pelaksanaan == "Mitra"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="metode_pelaksanaan2"
                                      >
                                        Mitra
                                      </label>
                                    </div>
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["metode_pelaksanaan"]}
                                  </span>
                                </div>
                                <div
                                  style={{
                                    display: this.state.showingmetodepelaksanaan
                                      ? "block"
                                      : "none",
                                  }}
                                >
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Pilih Mitra
                                    </label>
                                    {/* <Select
                                        name="mitra"
                                        placeholder="Silahkan pilih"
                                        noOptionsMessage={({ inputValue }) =>
                                          !inputValue
                                            ? this.state.dataxmitra
                                            : "Data tidak tersedia"
                                        }
                                        className="form-select-sm selectpicker p-0"
                                        options={this.state.dataxmitra}
                                        value={this.state.selmitra}
                                        onChange={this.handleChangeMitra}
                                      /> */}
                                    <select
                                      className="form-select form-select-sm"
                                      name="mitra"
                                      value={this.state.selmitra?.value ?? ""}
                                      onChange={this.handleChangeMitra}
                                    >
                                      <option
                                        value=""
                                        style={{ display: "none" }}
                                      >
                                        Silahkan pilih
                                      </option>
                                      {this.state.dataxmitra &&
                                        this.state.dataxmitra.map(
                                          (opt, index) => (
                                            <option
                                              key={`mitra_${index}`}
                                              value={opt.value}
                                            >
                                              {opt.label}
                                            </option>
                                          ),
                                        )}
                                    </select>
                                  </div>
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Pilih Penyelenggara
                                  </label>
                                  {/* <Select
                                      name="penyelenggara"
                                      placeholder="Silahkan pilih"
                                      noOptionsMessage={({ inputValue }) =>
                                        !inputValue
                                          ? this.state.dataxpenyelenggara
                                          : "Data tidak tersedia"
                                      }
                                      className="form-select-sm selectpicker p-0"
                                      options={this.state.dataxpenyelenggara}
                                      value={this.state.selpenyelenggara}
                                      onChange={this.handleChangePenyelenggara}
                                    /> */}
                                  <select
                                    className="form-select form-select-sm"
                                    name="penyelenggara"
                                    value={
                                      this.state.selpenyelenggara?.value ?? ""
                                    }
                                    onChange={this.handleChangePenyelenggara}
                                  >
                                    <option
                                      value=""
                                      style={{ display: "none" }}
                                    >
                                      Silahkan pilih
                                    </option>
                                    {this.state.dataxpenyelenggara &&
                                      this.state.dataxpenyelenggara.map(
                                        (opt, index) => (
                                          <option
                                            key={`penyelenggara_${index}`}
                                            value={opt.value}
                                          >
                                            {opt.label}
                                          </option>
                                        ),
                                      )}
                                  </select>
                                  {/* <select className="form-select form-select-sm selectpicker" data-control="select2" data-placeholder="Select an option" name="penyelenggara">
                                      {
                                        this.state.dataxpenyelenggara.map(data =>
                                          <option value={data.id} selected={this.state.dataxpelatihan.penyelenggara == data.id ? true : false}>{data.name}</option>
                                        )
                                      }
                                    </select> */}
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Metode Pelatihan
                                  </label>
                                  <div className="d-flex">
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="metode_pelatihan"
                                        value="Offline"
                                        id="metode_pelatihan1"
                                        onChange={this.handleClickMetode}
                                        checked={
                                          this.state.dataxpelatihan
                                            .metode_pelatihan === "Offline"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="metode_pelatihan1"
                                      >
                                        Offline
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="metode_pelatihan"
                                        value="Online"
                                        id="metode_pelatihan2"
                                        onChange={this.handleClickMetode}
                                        checked={
                                          this.state.dataxpelatihan
                                            .metode_pelatihan === "Online"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="metode_pelatihan2"
                                      >
                                        Online
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="metode_pelatihan"
                                        value="Online & Offline"
                                        id="metode_pelatihan3"
                                        onChange={this.handleClickMetode}
                                        checked={
                                          this.state.dataxpelatihan
                                            .metode_pelatihan ===
                                          "Online & Offline"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="metode_pelatihan3"
                                      >
                                        Online & Offline
                                      </label>
                                    </div>
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["metode_pelatihan"]}
                                  </span>
                                </div>
                                <div
                                  style={{
                                    display:
                                      this.state.dataxpelatihan.metode_pelatihan?.includes(
                                        "Online",
                                      )
                                        ? ""
                                        : "none",
                                  }}
                                >
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      URL Video Conferene
                                    </label>
                                    <input
                                      className="form-control form-control-sm"
                                      placeholder="Masukkan URL Vicon"
                                      name="url_vicon"
                                      defaultValue={
                                        this.state.dataxpelatihan.url_vicon ??
                                        ""
                                      }
                                      onBlur={(e) => {
                                        this.setState({
                                          dataxpelatihan: {
                                            ...this.state.dataxpelatihan,
                                            url_vicon: e.target.value,
                                          },
                                        });
                                      }}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["url_vicon"]}
                                    </span>
                                  </div>
                                </div>
                                <div
                                  style={{
                                    display: this.state.showing
                                      ? "block"
                                      : "none",
                                  }}
                                >
                                  <div>
                                    <div className="my-8 border-top mx-0"></div>
                                  </div>
                                  <h2 className="fs-5 text-muted mb-5">
                                    Lokasi Pelatihan
                                  </h2>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Provinsi
                                    </label>
                                    <select
                                      className="form-select form-select-sm"
                                      name="provinsi_field"
                                      value={
                                        this.state.selprovinsi?.value ?? ""
                                      }
                                      onChange={this.handleChangeProv}
                                    >
                                      <option
                                        value=""
                                        style={{ display: "none" }}
                                      >
                                        Silahkan pilih
                                      </option>
                                      {this.state.dataxprov &&
                                        this.state.dataxprov.map(
                                          (opt, index) => (
                                            <option
                                              key={`provinsi_${index}`}
                                              value={opt.value}
                                            >
                                              {opt.label}
                                            </option>
                                          ),
                                        )}
                                    </select>
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["provinsi_field"]}
                                    </span>
                                  </div>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Kota / Kabupaten
                                    </label>
                                    {/* <Select
                                        name="kota_kabupaten"
                                        placeholder="Silahkan pilih"
                                        noOptionsMessage={({ inputValue }) =>
                                          !inputValue
                                            ? this.state.dataxkab
                                            : "Data tidak tersedia"
                                        }
                                        className="form-select-sm selectpicker p-0"
                                        options={this.state.dataxkab}
                                        value={this.state.selkabupaten}
                                        onChange={this.handleChangeKabupaten}
                                      /> */}
                                    <select
                                      className="form-select form-select-sm"
                                      name="kota_kabupaten"
                                      value={
                                        this.state.selkabupaten?.value ?? ""
                                      }
                                      onChange={this.handleChangeKabupaten}
                                    >
                                      <option
                                        value=""
                                        style={{ display: "none" }}
                                      >
                                        Silahkan pilih
                                      </option>
                                      {this.state.dataxkab &&
                                        this.state.dataxkab.map(
                                          (opt, index) => (
                                            <option
                                              key={`kabupaten_${index}`}
                                              value={opt.value}
                                            >
                                              {opt.label}
                                            </option>
                                          ),
                                        )}
                                    </select>
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["kota_kabupaten"]}
                                    </span>
                                  </div>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Alamat
                                    </label>
                                    {/* <textarea className="form-control form-control-sm" placeholder="Masukkan alamat" name="alamat" defaultValue={this.state.dataxpelatihan.alamat}></textarea> */}
                                    <CKEditor
                                      editor={Editor}
                                      data={
                                        this.state.dataxpelatihan.alamat ?? ""
                                      }
                                      name="alamat"
                                      onReady={(editor) => {}}
                                      config={{
                                        ckfinder: {
                                          // Upload the images to the server using the CKFinder QuickUpload command.
                                          uploadUrl:
                                            process.env.REACT_APP_BASE_API_URI +
                                            "/publikasi/ckeditor-upload-image",
                                        },
                                      }}
                                      onBlur={(event, editor) => {
                                        const data = editor.getData();
                                        this.handleChangeAlamat(data);
                                      }}
                                      onFocus={(event, editor) => {}}
                                    />
                                    <input
                                      type="hidden"
                                      name="alamat_hidden"
                                      // value={this.state.dataxpelatihan.alamat}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["alamat"]}
                                    </span>
                                  </div>
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Klasifikasi Peserta
                                  </label>
                                  <div className="d-flex">
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="checkbox"
                                        name="disabilitas"
                                        value={1}
                                        id="umum"
                                        onChange={
                                          this.handleChangeDisabilitasUmum
                                        }
                                        checked={
                                          this.state.dataxpelatihan.umum === "1"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="umum"
                                      >
                                        Umum
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="checkbox"
                                        name="disabilitas"
                                        value={1}
                                        id="tuna_netra"
                                        onChange={
                                          this.handleChangeDisabilitasTunaNetra
                                        }
                                        checked={
                                          this.state.dataxpelatihan
                                            .tuna_netra === "1"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="tuna_netra"
                                      >
                                        Tuna Netra
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="checkbox"
                                        name="disabilitas"
                                        value={1}
                                        id="tuna_rungu"
                                        onChange={
                                          this.handleChangeDisabilitasTunaRungu
                                        }
                                        checked={
                                          this.state.dataxpelatihan
                                            .tuna_rungu === "1"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="tuna_rungu"
                                      >
                                        Tuna Rungu
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      <input
                                        className="form-check-input"
                                        type="checkbox"
                                        value={1}
                                        name="disabilitas"
                                        onChange={
                                          this.handleChangeDisabilitasTunaDaksa
                                        }
                                        checked={
                                          this.state.dataxpelatihan
                                            .tuna_daksa === "1"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="tuna_daksa"
                                      >
                                        Tuna Daksa
                                      </label>
                                    </div>
                                  </div>
                                </div>
                                {/* <div className="col-lg-12">
                                    <div className="mb-7 fv-row">
                                      <label className="form-label">Upload Logo (Optional)</label>
                                      <input type="file" className="form-control form-control-sm mb-2" name="upload_logo" accept=".png, .jpg, .jpeg, .svg" />
                                      <small className="text-muted">Format Image (.png/.jpg/.jpeg/.svg), Max 2048</small>
                                    </div>
                                  </div>
                                  <div className="col-lg-12">
                                    <div className="mb-7 fv-row">
                                      <label className="form-label">Upload Thumbnail (Optional)</label>
                                      <input type="file" className="form-control form-control-sm mb-2" name="upload_thumbnail" accept=".png, .jpg, .jpeg, .svg" />
                                      <small className="text-muted">Format Image (.png/.jpg/.jpeg/.svg), Max 2048</small>
                                    </div>
                                  </div> */}
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Zonasi
                                  </label>
                                  {/* <Select
                                      name="zonasi"
                                      placeholder="Silahkan pilih"
                                      noOptionsMessage={({ inputValue }) =>
                                        !inputValue
                                          ? this.state.dataxzonasi
                                          : "Data tidak tersedia"
                                      }
                                      className="form-select-sm selectpicker p-0"
                                      options={this.state.dataxzonasi}
                                      value={this.state.selzonasi}
                                      onChange={this.handleChangeZonasi}
                                    /> */}
                                  <select
                                    className="form-select form-select-sm"
                                    name="zonasi"
                                    value={this.state.selzonasi?.value ?? ""}
                                    onChange={this.handleChangeZonasi}
                                  >
                                    <option
                                      value=""
                                      style={{ display: "none" }}
                                    >
                                      Silahkan pilih
                                    </option>
                                    {this.state.dataxzonasi &&
                                      this.state.dataxzonasi.map(
                                        (opt, index) => (
                                          <option
                                            key={`zonasi_${index}`}
                                            value={opt.value}
                                          >
                                            {opt.label}
                                          </option>
                                        ),
                                      )}
                                  </select>
                                  <span className="text-muted">
                                    Jika terbuka untuk seluruh Indonesia, Pilih
                                    Zonasi Nasional
                                  </span>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["zonasi"]}
                                  </span>
                                </div>
                                <div>
                                  <div className="my-8 border-top mx-0"></div>
                                </div>
                                <h2 className="fs-5 text-muted mb-3">
                                  Kuota Pelatihan
                                </h2>
                                {/* <div className="col-lg-6 mb-7 fv-row">
                                  <label className="form-label required">
                                    Target Kuota Pendaftar
                                  </label>
                                  <input
                                    className="form-control form-control-sm"
                                    placeholder="Masukkan target kuota pendaftar"
                                    name="kuota_target_pendaftar"
                                    id="kuota_pendaftar"
                                    defaultValue={
                                      this.state.dataxpelatihan.kuota_pendaftar
                                    }
                                    onBlur={(e) => {
                                      this.setState({
                                        dataxpelatihan: {
                                          ...this.state.dataxpelatihan,
                                          kuota_pendaftar: e.target.value,
                                        },
                                      });
                                    }}
                                  />
                                  <span style={{ color: "red" }}>
                                    {
                                      this.state.errors[
                                        "kuota_target_pendaftar"
                                      ]
                                    }
                                  </span>
                                </div> */}
                                {this.state.isKuotaPendaftarVisible ? (
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label required">
                                      Target Kuota Pendaftar
                                    </label>
                                    <input
                                      id="kuota_pendaftar"
                                      className="form-control form-control-sm"
                                      placeholder="Masukkan target kuota pendaftar"
                                      name="kuota_target_pendaftar"
                                      defaultValue={
                                        // this.state.dataxpelatihan.kuota_pendaftar
                                        this.state.kuotapendaftar
                                      }
                                      onBlur={(e) => {
                                        this.setState({
                                          dataxpelatihan: {
                                            ...this.state.dataxpelatihan,
                                            kuota_pendaftar: e.target.value,
                                          },
                                        });
                                      }}
                                    />
                                    <span style={{ color: "red" }}>
                                      {
                                        this.state.errors[
                                          "kuota_target_pendaftar"
                                        ]
                                      }
                                    </span>
                                  </div>
                                ) : null}
                                <div className="col-lg-6 mb-7 fv-row">
                                  <label className="form-label required">
                                    Target Kuota Peserta
                                  </label>
                                  <input
                                    className="form-control form-control-sm"
                                    placeholder="Masukkan target kuota peserta"
                                    name="kuota_target_peserta"
                                    id="kuota_peserta"
                                    defaultValue={
                                      this.state.dataxpelatihan.kuota_peserta
                                    }
                                    onBlur={(e) => {
                                      this.setState({
                                        dataxpelatihan: {
                                          ...this.state.dataxpelatihan,
                                          kuota_peserta: e.target.value,
                                        },
                                      });
                                    }}
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["kuota_target_peserta"]}
                                  </span>
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Status Kuota
                                  </label>
                                  <div className="d-flex">
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="status_kuota"
                                        value="Available"
                                        id="status_kuota1"
                                        onChange={this.handleChangeStatusKuota}
                                        checked={
                                          this.state.dataxpelatihan
                                            .status_kuota === "Available"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="status_kuota1"
                                      >
                                        Available
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="status_kuota"
                                        value="Full"
                                        id="status_kuota2"
                                        onChange={this.handleChangeStatusKuota}
                                        checked={
                                          this.state.dataxpelatihan
                                            .status_kuota === "Full"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="status_kuota2"
                                      >
                                        Full
                                      </label>
                                    </div>
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["status_kuota"]}
                                  </span>
                                </div>

                                {this.state.dataxpelatihan.status_substansi ===
                                "Disetujui" ? (
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label">
                                      Status Publish
                                    </label>
                                    {/* <Select
                                        name="status_publish"
                                        placeholder="Silahkan pilih"
                                        noOptionsMessage={({ inputValue }) =>
                                          !inputValue
                                            ? this.optionstatus
                                            : "Data tidak tersedia"
                                        }
                                        className="form-select-sm form-select-solid selectpicker p-0"
                                        options={this.optionstatus}
                                        value={this.state.selstatuspublish}
                                        onChange={
                                          this.handleChangeStatusPublishAction
                                        }
                                      /> */}
                                    <select
                                      className="form-select form-select-sm"
                                      name="status_publish"
                                      value={
                                        this.state.selstatuspublish?.value ?? ""
                                      }
                                      onChange={
                                        this.handleChangeStatusPublishAction
                                      }
                                      disabled={
                                        this.optionstatus?.length &&
                                        this.optionstatus[0] != null
                                          ? false
                                          : true
                                      }
                                    >
                                      <option
                                        value=""
                                        style={{ display: "none" }}
                                      >
                                        Silahkan pilih
                                      </option>
                                      {this.optionstatus &&
                                        this.optionstatus[0] != null &&
                                        this.optionstatus.map((opt, index) => (
                                          <option
                                            key={`publish_${index}`}
                                            value={opt.value}
                                          >
                                            {opt.label}
                                          </option>
                                        ))}
                                    </select>
                                  </div>
                                ) : (
                                  ""
                                )}
                                <div className="col-12">
                                  {this.state.dataxpelatihan.status_substansi ==
                                    "Disetujui" &&
                                    this.state.dataxpelatihan.revisi > 0 && (
                                      <div
                                        className="alert alert-warning fade show mt-1"
                                        role="alert"
                                      >
                                        <a href="#" className="alert-link">
                                          Silabus telah mengalami revisi
                                          sebanyak{" "}
                                          {this.state.dataxpelatihan.revisi}x
                                        </a>
                                      </div>
                                    )}

                                  {this.state.dataxpelatihan.status_substansi ==
                                    "Disetujui" && (
                                    <>
                                      {this.state.dataxpelatihan
                                        .status_pelatihan == "Dibatalkan" ? (
                                        <div
                                          className="alert alert-primary fade show mt-1"
                                          role="alert"
                                        >
                                          <span className="alert-link">
                                            Pelatihan sudah dibatalkan pada
                                            tanggal{" "}
                                            <strong>
                                              {handleFormatDate(
                                                this.state.dataxpelatihan
                                                  .updated_at,
                                                "DD MMMM YYYY, HH:mm:ss",
                                              )}
                                            </strong>{" "}
                                            oleh{" "}
                                            <strong>
                                              {
                                                this.state.dataxpelatihan
                                                  .updated_by
                                              }
                                            </strong>
                                          </span>
                                        </div>
                                      ) : this.state.dataxpelatihan
                                          .status_pelatihan != "Selesai" &&
                                        this.state.dataxpelatihan
                                          .status_pelatihan != "Dibatalkan" ? (
                                        <a
                                          href="#"
                                          onClick={
                                            this.handleClickRejectForceMajure
                                          }
                                          className="btn btn-flex btn-danger px-6 w-100"
                                        >
                                          <i className="bi bi-stop-circle fs-2x "></i>
                                          <span className="d-flex flex-column align-items-start ms-2">
                                            <span className="fs-3 fw-bolder">
                                              BATALKAN PELATIHAN
                                            </span>
                                            <span className="fs-7">
                                              Harap perhatikan. Tombol ini hanya
                                              digunakan saat terjadi Force
                                              Majure
                                            </span>
                                          </span>
                                        </a>
                                      ) : (
                                        <div
                                          className="alert alert-primary fade show mt-1 d-flex align-items-center"
                                          role="alert"
                                        >
                                          <i
                                            className="bi bi-check-circle fs-2x me-2"
                                            style={{ color: "black" }}
                                          ></i>

                                          <div className="alert-link">
                                            Pelatihan telah selesai pada tanggal{" "}
                                            <strong>
                                              {handleFormatDate(
                                                this.state.dataxpelatihan
                                                  .pelatihan_end,
                                                "DD MMMM YYYY, HH:mm:ss",
                                                "YYYY-MM-DD HH:mm:ss",
                                              )}
                                            </strong>
                                          </div>
                                        </div>
                                      )}
                                    </>
                                  )}
                                </div>
                              </div>
                            </div>

                            <div
                              className="flex-column"
                              data-kt-stepper-element="content"
                            >
                              <div className="row mt-7">
                                <h2 className="fs-5 text-muted mb-5">
                                  Alur Seleksi
                                  <label
                                    htmlFor="form-label"
                                    className="required"
                                  ></label>
                                </h2>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <div className="mb-0">
                                    <label className="d-flex flex-stack mb-5 cursor-pointer">
                                      <span className="d-flex align-items-center me-2">
                                        <span className="symbol symbol-50px me-6">
                                          <span className="symbol-label">
                                            <span className="svg-icon svg-icon-1 svg-icon-gray-600">
                                              <i className="fa fa-file-signature fa-2x"></i>
                                            </span>
                                          </span>
                                        </span>
                                        <span className="d-flex flex-column">
                                          <span className="fw-bolder text-gray-800 text-hover-primary fs-7">
                                            Administrasi
                                          </span>
                                          <span className="fs-7 fw-bold text-muted">
                                            Seleksi pelatihan hanya berdasarkan
                                            administrasi/berkas pendaftaran
                                          </span>
                                        </span>
                                      </span>
                                      <span className="form-check form-check-custom form-check-solid">
                                        <input
                                          className="form-check-input"
                                          type="radio"
                                          name="alur_pendaftaran"
                                          value="Administrasi"
                                          onChange={
                                            this.handleChangeAlurPendaftaran
                                          }
                                          checked={
                                            this.state.dataxpelatihan
                                              .alur_pendaftaran ===
                                            "Administrasi"
                                          }
                                        />
                                      </span>
                                    </label>
                                    <label className="d-flex flex-stack mb-5 cursor-pointer">
                                      <span className="d-flex align-items-center me-2">
                                        <span className="symbol symbol-50px me-6">
                                          <span className="symbol-label">
                                            <span className="svg-icon svg-icon-1 svg-icon-gray-600">
                                              <i className="fa fa-clipboard-list fa-2x"></i>
                                            </span>
                                          </span>
                                        </span>
                                        <span className="d-flex flex-column">
                                          <span className="fw-bolder text-gray-800 text-hover-primary fs-7">
                                            Administrasi{" "}
                                            <i className="bi bi-chevron-right mx-2"></i>{" "}
                                            Test Substansi
                                          </span>
                                          <span className="fs-7 fw-bold text-muted">
                                            Setelah Seleksi administrasi akan
                                            diadakan test substansi bagi peserta
                                            yang lolos
                                          </span>
                                        </span>
                                      </span>
                                      <span className="form-check form-check-custom form-check-solid">
                                        <input
                                          className="form-check-input"
                                          type="radio"
                                          name="alur_pendaftaran"
                                          value="Administrasi - Test Substansi"
                                          onChange={
                                            this.handleChangeAlurPendaftaran
                                          }
                                          checked={
                                            this.state.dataxpelatihan
                                              .alur_pendaftaran ===
                                            "Administrasi - Test Substansi"
                                          }
                                        />
                                      </span>
                                    </label>
                                    <label className="d-flex flex-stack mb-5 cursor-pointer">
                                      <span className="d-flex align-items-center me-2">
                                        <span className="symbol symbol-50px me-6">
                                          <span className="symbol-label">
                                            <span className="svg-icon svg-icon-1 svg-icon-gray-600">
                                              <i className="fa fa-clipboard-check fa-2x"></i>
                                            </span>
                                          </span>
                                        </span>
                                        <span className="d-flex flex-column">
                                          <span className="fw-bolder text-gray-800 text-hover-primary fs-7">
                                            Test Substansi{" "}
                                            <i className="bi bi-chevron-right mx-2"></i>{" "}
                                            Administrasi
                                          </span>
                                          <span className="fs-7 fw-bold text-muted">
                                            Setelah melakukan pendaftaran
                                            peserta langsung mengerjakan Test
                                            Substansi, lalu dilakukan seleksi
                                            Administrasi
                                          </span>
                                        </span>
                                      </span>
                                      <span className="form-check form-check-custom form-check-solid">
                                        <input
                                          className="form-check-input"
                                          type="radio"
                                          name="alur_pendaftaran"
                                          value="Test Substansi - Administrasi"
                                          onChange={
                                            this.handleChangeAlurPendaftaran
                                          }
                                          checked={
                                            this.state.dataxpelatihan
                                              .alur_pendaftaran ===
                                            "Test Substansi - Administrasi"
                                          }
                                        />
                                      </span>
                                    </label>
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["alur_pendaftaran"]}
                                  </span>
                                </div>
                                <h2 className="fs-5 text-muted mb-5">
                                  Jadwal Pendaftaran dan Pelatihan
                                </h2>
                                <div className="col-lg-6 mb-7 fv-row">
                                  <label className="form-label required">
                                    Tgl. Mulai Pendaftaran
                                  </label>
                                  <Flatpickr
                                    options={{
                                      locale: Indonesian,
                                      dateFormat: "d F Y H:i",
                                      enableTime: "true",
                                      time_24hr: true,
                                      // minDate:
                                      //   this.state.dataxpelatihan
                                      //     .pendaftaran_start,
                                    }}
                                    className="form-control form-control-sm"
                                    placeholder="Masukkan tanggal event"
                                    name="pendaftaran_tanggal_mulai"
                                    value={
                                      this.state.dataxpelatihan
                                        .pendaftaran_start
                                    }
                                    // disabled={["pelatihan", "selesai"].includes(
                                    //   this.state.dataxpelatihan.status_pelatihan?.toLowerCase()
                                    // )}
                                    onChange={(value, dateString) => {
                                      if (
                                        this.state.dataxpelatihan
                                          .alur_pendaftaran ==
                                        "Test Substansi - Administrasi"
                                      ) {
                                        this.setState({
                                          dataxpelatihan: {
                                            ...this.state.dataxpelatihan,
                                            subtansi_mulai: value[0],
                                          },
                                        });
                                      }
                                      this.setState({
                                        dataxpelatihan: {
                                          ...this.state.dataxpelatihan,
                                          pendaftaran_start: value[0],
                                        },
                                      });
                                    }}
                                  />
                                  <span style={{ color: "red" }}>
                                    {
                                      this.state.errors[
                                        "pendaftaran_tanggal_mulai"
                                      ]
                                    }
                                  </span>
                                </div>
                                <div className="col-lg-6 mb-7 fv-row">
                                  <label className="form-label required">
                                    Tgl. Selesai Pendaftaran
                                  </label>

                                  <Flatpickr
                                    options={{
                                      locale: Indonesian,
                                      dateFormat: "d F Y H:i",
                                      enableTime: "true",
                                      time_24hr: true,
                                      minDate:
                                        this.state.dataxpelatihan
                                          .pendaftaran_start,
                                    }}
                                    className="form-control form-control-sm"
                                    placeholder="Masukkan tanggal event"
                                    name="pendaftaran_tanggal_selesai"
                                    value={
                                      this.state.dataxpelatihan.pendaftaran_end
                                    }
                                    // disabled={["pelatihan", "selesai"].includes(
                                    //   this.state.dataxpelatihan.status_pelatihan?.toLowerCase()
                                    // )}
                                    onChange={(value) => {
                                      if (
                                        this.state.dataxpelatihan
                                          .alur_pendaftaran ==
                                        "Test Substansi - Administrasi"
                                      ) {
                                        this.setState({
                                          dataxpelatihan: {
                                            ...this.state.dataxpelatihan,
                                            subtansi_selesai: value[0],
                                          },
                                        });
                                      }
                                      this.setState({
                                        dataxpelatihan: {
                                          ...this.state.dataxpelatihan,
                                          pendaftaran_end: value[0],
                                        },
                                      });
                                    }}
                                  />
                                  <span style={{ color: "red" }}>
                                    {
                                      this.state.errors[
                                        "pendaftaran_tanggal_selesai"
                                      ]
                                    }
                                  </span>
                                </div>
                                <div className="col-lg-6 mb-7 fv-row">
                                  <label className="form-label required">
                                    Tgl. Mulai Pelatihan
                                  </label>
                                  <Flatpickr
                                    options={{
                                      locale: Indonesian,
                                      dateFormat: "d F Y H:i",
                                      time_24hr: true,
                                      enableTime: "true",
                                      minDate:
                                        this.state.dataxpelatihan
                                          .pendaftaran_end ?? "",
                                    }}
                                    className="form-control form-control-sm"
                                    placeholder="Masukkan tanggal event"
                                    name="pelatihan_tanggal_mulai"
                                    value={
                                      this.state.dataxpelatihan.pelatihan_start
                                    }
                                    // disabled={["pelatihan", "selesai"].includes(
                                    //   this.state.dataxpelatihan.status_pelatihan?.toLowerCase()
                                    // )}
                                    onChange={(value) => {
                                      this.setState({
                                        dataxpelatihan: {
                                          ...this.state.dataxpelatihan,
                                          pelatihan_start: value[0],
                                        },
                                      });
                                    }}
                                  />
                                  <span style={{ color: "red" }}>
                                    {
                                      this.state.errors[
                                        "pelatihan_tanggal_mulai"
                                      ]
                                    }
                                  </span>
                                </div>
                                <div className="col-lg-6 mb-7 fv-row">
                                  <label className="form-label required">
                                    Tgl. Selesai Pelatihan
                                  </label>
                                  <Flatpickr
                                    options={{
                                      locale: Indonesian,
                                      dateFormat: "d F Y H:i",
                                      time_24hr: true,
                                      enableTime: "true",
                                      minDate:
                                        this.state.dataxpelatihan
                                          .pelatihan_start ?? "",
                                    }}
                                    className="form-control form-control-sm"
                                    placeholder="Masukkan tanggal event"
                                    name="pelatihan_tanggal_selesai"
                                    value={
                                      this.state.dataxpelatihan.pelatihan_end
                                    }
                                    // disabled={["pelatihan", "selesai"].includes(
                                    //   this.state.dataxpelatihan.status_pelatihan?.toLowerCase()
                                    // )}
                                    onChange={(value) => {
                                      this.setState({
                                        dataxpelatihan: {
                                          ...this.state.dataxpelatihan,
                                          pelatihan_end: value[0],
                                        },
                                      });
                                    }}
                                  />
                                  <span style={{ color: "red" }}>
                                    {
                                      this.state.errors[
                                        "pelatihan_tanggal_selesai"
                                      ]
                                    }
                                  </span>
                                </div>

                                <div
                                  style={{
                                    display:
                                      this.state.dataxpelatihan
                                        .alur_pendaftaran != "Administrasi"
                                        ? ""
                                        : "none",
                                  }}
                                >
                                  <h2 className="fs-5 text-muted mb-5">
                                    Seleksi Administrasi dan Substansi
                                  </h2>
                                  {/* Administrasi - Test */}
                                  <div
                                    className="row"
                                    style={{
                                      display:
                                        this.state.dataxpelatihan
                                          .alur_pendaftaran ==
                                        "Administrasi - Test Substansi"
                                          ? ""
                                          : "none",
                                    }}
                                  >
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label required">
                                        Tgl. Mulai Administrasi
                                      </label>
                                      <Flatpickr
                                        options={{
                                          locale: Indonesian,
                                          dateFormat: "d F Y H:i",
                                          time_24hr: true,
                                          enableTime: "true",
                                          minDate:
                                            this.state.dataxpelatihan
                                              .pendaftaran_start ?? "",
                                          maxDate: this.state.dataxpelatihan
                                            .pelatihan_start
                                            ? fp_inc(
                                                new Date(
                                                  this.state.dataxpelatihan.pelatihan_start,
                                                ),
                                                -1,
                                              )
                                            : "",
                                        }}
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan tanggal mulai administrasi"
                                        name="adm_test_administrasi_tanggal_mulai"
                                        value={
                                          this.state.dataxpelatihan
                                            .administrasi_mulai
                                        }
                                        onChange={(value) => {
                                          this.setState({
                                            dataxpelatihan: {
                                              ...this.state.dataxpelatihan,
                                              administrasi_mulai: value[0],
                                            },
                                          });
                                        }}
                                      />
                                      <span style={{ color: "red" }}>
                                        {
                                          this.state.errors[
                                            "adm_test_administrasi_tanggal_mulai"
                                          ]
                                        }
                                      </span>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label required">
                                        Tgl. Selesai Administrasi
                                      </label>
                                      <Flatpickr
                                        options={{
                                          locale: Indonesian,
                                          dateFormat: "d F Y H:i",
                                          time_24hr: true,
                                          enableTime: "true",
                                          minDate:
                                            this.state.dataxpelatihan
                                              .administrasi_mulai ?? "",
                                          maxDate: this.state.dataxpelatihan
                                            .pelatihan_start
                                            ? fp_inc(
                                                new Date(
                                                  this.state.dataxpelatihan.pelatihan_start,
                                                ),
                                                -1,
                                              )
                                            : "",
                                        }}
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan tanggal selesai administrasi"
                                        name="adm_test_administrasi_tanggal_selesai"
                                        value={
                                          this.state.dataxpelatihan
                                            .administrasi_selesai
                                        }
                                        onChange={(value) => {
                                          this.setState({
                                            dataxpelatihan: {
                                              ...this.state.dataxpelatihan,
                                              administrasi_selesai: value[0],
                                            },
                                          });
                                        }}
                                      />
                                      <span style={{ color: "red" }}>
                                        {
                                          this.state.errors[
                                            "adm_test_administrasi_tanggal_selesai"
                                          ]
                                        }
                                      </span>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label required">
                                        Tgl. Mulai Test Substansi
                                      </label>
                                      <Flatpickr
                                        options={{
                                          locale: Indonesian,
                                          dateFormat: "d F Y H:i",
                                          time_24hr: true,
                                          enableTime: "true",
                                          minDate:
                                            this.state.dataxpelatihan
                                              .administrasi_selesai ?? "",
                                          maxDate:
                                            this.state.dataxpelatihan
                                              .pelatihan_start,
                                          // ? fp_inc(
                                          //     new Date(
                                          //       this.state.dataxpelatihan.pelatihan_start
                                          //     ),
                                          //     -1
                                          //   )
                                          // : "",
                                        }}
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan tanggal mulai test substansi"
                                        name="adm_test_testsubstansi_tanggal_mulai"
                                        value={
                                          this.state.dataxpelatihan
                                            .subtansi_mulai
                                        }
                                        onChange={(value) => {
                                          this.setState({
                                            dataxpelatihan: {
                                              ...this.state.dataxpelatihan,
                                              subtansi_mulai: value[0],
                                            },
                                          });
                                        }}
                                      />
                                      <span style={{ color: "red" }}>
                                        {
                                          this.state.errors[
                                            "adm_test_testsubstansi_tanggal_mulai"
                                          ]
                                        }
                                      </span>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label required">
                                        Tgl. Selesai Test Substansi
                                      </label>
                                      <Flatpickr
                                        options={{
                                          locale: Indonesian,
                                          dateFormat: "d F Y H:i",
                                          time_24hr: true,
                                          enableTime: "true",
                                          minDate:
                                            this.state.dataxpelatihan
                                              .subtansi_mulai ?? "",
                                          maxDate:
                                            this.state.dataxpelatihan
                                              .pelatihan_start,
                                        }}
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan tanggal mulai test substansi"
                                        name="adm_test_testsubstansi_tanggal_selesai"
                                        value={
                                          this.state.dataxpelatihan
                                            .subtansi_selesai
                                        }
                                        onChange={(value) => {
                                          this.setState({
                                            dataxpelatihan: {
                                              ...this.state.dataxpelatihan,
                                              subtansi_selesai: value[0],
                                            },
                                          });
                                        }}
                                      />
                                      <span style={{ color: "red" }}>
                                        {
                                          this.state.errors[
                                            "adm_test_testsubstansi_tanggal_selesai"
                                          ]
                                        }
                                      </span>
                                    </div>
                                  </div>

                                  <div
                                    className="row"
                                    style={{
                                      display:
                                        this.state.dataxpelatihan
                                          .alur_pendaftaran ==
                                        "Test Substansi - Administrasi"
                                          ? ""
                                          : "none",
                                    }}
                                  >
                                    {/* Test substansi - administrasi */}
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label required">
                                        Tgl. Mulai Test Substansi
                                      </label>
                                      <Flatpickr
                                        disabled
                                        options={{
                                          locale: Indonesian,
                                          dateFormat: "d F Y H:i",
                                          time_24hr: true,
                                          enableTime: "true",
                                          // minDate:
                                          //   this.state.dataxpelatihan
                                          //     .pendaftaran_start ?? "",
                                          // maxDate:
                                          //   this.state.dataxpelatihan
                                          //     .pendaftaran_start ?? "",
                                        }}
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan tanggal mulai test substansi"
                                        name="test_adm_testsubstansi_tanggal_mulai"
                                        value={
                                          this.state.dataxpelatihan
                                            .subtansi_mulai
                                        }
                                        onChange={(value) => {
                                          this.setState({
                                            dataxpelatihan: {
                                              ...this.state.dataxpelatihan,
                                              subtansi_mulai: value[0],
                                            },
                                          });
                                        }}
                                      />
                                      <span style={{ color: "red" }}>
                                        {
                                          this.state.errors[
                                            "test_adm_testsubstansi_tanggal_mulai"
                                          ]
                                        }
                                      </span>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label required">
                                        Tgl. Selesai Test Substansi
                                      </label>
                                      <Flatpickr
                                        disabled
                                        options={{
                                          locale: Indonesian,
                                          dateFormat: "d F Y H:i",
                                          time_24hr: true,
                                          enableTime: "true",
                                          // minDate:
                                          //   this.state.dataxpelatihan
                                          //     .subtansi_mulai ?? "",
                                          // maxDate:
                                          //   this.state.dataxpelatihan
                                          //     .pelatihan_start,
                                          // ? fp_inc(
                                          //     new Date(
                                          //       this.state.dataxpelatihan.pelatihan_start
                                          //     ),
                                          //     -1
                                          //   )
                                          // : "",
                                        }}
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan tanggal selesai test substansi"
                                        name="test_adm_testsubstansi_tanggal_selesai"
                                        value={
                                          this.state.dataxpelatihan
                                            .subtansi_selesai
                                        }
                                        onChange={(value) => {
                                          this.setState({
                                            dataxpelatihan: {
                                              ...this.state.dataxpelatihan,
                                              subtansi_selesai: value[0],
                                            },
                                          });
                                        }}
                                      />
                                      <span style={{ color: "red" }}>
                                        {
                                          this.state.errors[
                                            "test_adm_testsubstansi_tanggal_selesai"
                                          ]
                                        }
                                      </span>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label required">
                                        Tgl. Mulai Administrasi
                                      </label>
                                      {/* <input
                                              id="datetime_test_adm_admstart"
                                              className="form-control form-control-sm"
                                              placeholder="Masukkan tanggal mulai administrasi"
                                              name="test_adm_administrasi_tanggal_mulai"
                                              onChange={
                                                this
                                                  .handleAdministrasiTanggalMulai
                                              }
                                            /> */}
                                      <Flatpickr
                                        options={{
                                          locale: Indonesian,
                                          dateFormat: "d F Y H:i",
                                          time_24hr: true,
                                          enableTime: "true",
                                          minDate:
                                            this.state.dataxpelatihan
                                              .pendaftaran_start ?? "",
                                          maxDate:
                                            this.state.dataxpelatihan
                                              .pelatihan_start ?? "",
                                        }}
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan tanggal mulai administrasi"
                                        name="test_adm_administrasi_tanggal_mulai"
                                        value={
                                          this.state.dataxpelatihan
                                            .administrasi_mulai
                                        }
                                        onChange={(value) => {
                                          this.setState({
                                            dataxpelatihan: {
                                              ...this.state.dataxpelatihan,
                                              administrasi_mulai: value[0],
                                            },
                                          });
                                        }}
                                      />
                                      <span style={{ color: "red" }}>
                                        {
                                          this.state.errors[
                                            "test_adm_administrasi_tanggal_mulai"
                                          ]
                                        }
                                      </span>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label required">
                                        Tgl. Selesai Administrasi
                                      </label>
                                      {/* <input
                                              id="datetime_test_adm_admend"
                                              className="form-control form-control-sm"
                                              placeholder="Masukkan tanggal selesai administrasi"
                                              name="test_adm_administrasi_tanggal_selesai"
                                              onChange={
                                                this
                                                  .handleAdministrasiTanggalSelesai
                                              }
                                            /> */}
                                      <Flatpickr
                                        options={{
                                          locale: Indonesian,
                                          dateFormat: "d F Y H:i",
                                          time_24hr: true,
                                          enableTime: "true",
                                          minDate:
                                            this.state.dataxpelatihan
                                              .administrasi_mulai ?? "",
                                          maxDate:
                                            this.state.dataxpelatihan
                                              .pelatihan_start ?? "",
                                        }}
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan tanggal selesai administrasi"
                                        name="test_adm_administrasi_tanggal_selesai"
                                        value={
                                          this.state.dataxpelatihan
                                            .administrasi_selesai
                                        }
                                        onChange={(value) => {
                                          this.setState({
                                            dataxpelatihan: {
                                              ...this.state.dataxpelatihan,
                                              administrasi_selesai: value[0],
                                            },
                                          });
                                        }}
                                      />
                                      <span style={{ color: "red" }}>
                                        {
                                          this.state.errors[
                                            "test_adm_administrasi_tanggal_selesai"
                                          ]
                                        }
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Apakah ada Mid Test?
                                  </label>
                                  <div className="d-flex">
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="apakah_mid_test"
                                        value="Iya"
                                        id="apakah_mid_test1"
                                        onChange={
                                          this.handleChangeApakahMidTest
                                        }
                                        checked={this.state.showingMidTestDate}
                                        // checked={this.showingMidTestDate}
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="apakah_mid_test1"
                                      >
                                        Iya
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="apakah_mid_test"
                                        value="Tidak"
                                        id="apakah_mid_test2"
                                        onChange={
                                          this.handleChangeApakahMidTest
                                        }
                                        checked={!this.state.showingMidTestDate}
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="apakah_mid_test2"
                                      >
                                        Tidak
                                      </label>
                                    </div>
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["apakah_mid_test"]}
                                  </span>
                                </div>
                                <div
                                  style={{
                                    display: this.state.showingMidTestDate
                                      ? ""
                                      : "none",
                                  }}
                                >
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Tgl. Mid Test
                                    </label>
                                    {/* <input
                                            id="datetime9"
                                            className="form-control form-control-sm"
                                            placeholder="Masukkan tanggal mid test"
                                            name="midtest_tanggal"
                                          /> */}
                                    <Flatpickr
                                      options={{
                                        locale: Indonesian,
                                        dateFormat: "d F Y H:i",
                                        time_24hr: true,
                                        enableTime: "true",
                                        minDate:
                                          this.state.dataxpelatihan
                                            .pelatihan_start ?? "",
                                        maxDate:
                                          this.state.dataxpelatihan
                                            .pelatihan_end ?? "",
                                      }}
                                      className="form-control form-control-sm"
                                      placeholder="Masukkan tanggal mid test"
                                      name="midtest_tanggal"
                                      value={
                                        this.state.dataxpelatihan.midtest_mulai
                                      }
                                      onChange={(value) => {
                                        this.setState({
                                          dataxpelatihan: {
                                            ...this.state.dataxpelatihan,
                                            midtest_mulai: value[0],
                                          },
                                        });
                                      }}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["midtest_tanggal"]}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div
                              className="flex-column"
                              data-kt-stepper-element="content"
                            >
                              <div className="row mt-7">
                                <div
                                  id="div_selsilabus"
                                  style={{
                                    display: !this.state.showingSelSilabus
                                      ? "none"
                                      : "block",
                                  }}
                                >
                                  <div className="row">
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label required">
                                        Nama Silabus
                                      </label>
                                      <select
                                        className="form-select form-select-sm"
                                        name="select_silabus"
                                        value={
                                          this.state.selnamasilabus?.value ?? ""
                                        }
                                        onChange={this.handleChangeSilabus}
                                        disabled={
                                          (this.state.dataxselusesilabus
                                            .length ||
                                            this.state.dataxselusesilabus[0] !=
                                              null) &&
                                          this.state.dataxpelatihan.status_substansi?.toLowerCase() !=
                                            "disetujui"
                                            ? false
                                            : true
                                        }
                                      >
                                        <option
                                          value=""
                                          style={{ display: "none" }}
                                        >
                                          Silahkan pilih
                                        </option>
                                        {this.state.dataxselusesilabus &&
                                          this.state.dataxselusesilabus[0] !=
                                            null &&
                                          this.state.dataxselusesilabus.map(
                                            (opt, index) => (
                                              <option
                                                key={`usesilabus_${index}`}
                                                value={opt.value}
                                              >
                                                {opt.value} - {opt.label}
                                              </option>
                                            ),
                                          )}
                                      </select>
                                      <br />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["judul_form"]}
                                      </span>
                                    </div>
                                  </div>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <div className="mr-3 mb-5">
                                      <div className="mb-5">
                                        <h5
                                          className="modal-title mt-5 mb-6"
                                          id="preview_title"
                                        >
                                          {
                                            this.state.detailSilabus
                                              .nama_silabus
                                          }
                                          <span className="text-muted d-block small">
                                            ID : {this.state.detailSilabus.id} |{" "}
                                            {` Total : ${zeroDecimalValue(
                                              this.state.detailSilabus.totalJp,
                                            )} Jam Pelajaran`}
                                          </span>
                                        </h5>
                                      </div>
                                    </div>
                                    <div className="mr-3">
                                      <ul className="list-group list-group-numbered">
                                        {this.state.isiSilabus.map(
                                          (elem, index) => (
                                            <li
                                              key={
                                                "silabus_" +
                                                elem.id +
                                                "_" +
                                                index
                                              }
                                              className="list-group-item fs-7 fw-bolder d-flex justify-content-between align-items-start"
                                            >
                                              <div className="ms-2 me-auto">
                                                <div>
                                                  <h6 className="fw-bolder mb-0">
                                                    {elem.materi}
                                                  </h6>
                                                </div>
                                                <span className="text-muted fw-semibold fs-7">
                                                  {zeroDecimalValue(
                                                    elem.jam_pelajaran,
                                                  )}{" "}
                                                  JP
                                                </span>
                                              </div>
                                            </li>
                                          ),
                                        )}
                                      </ul>
                                    </div>
                                  </div>
                                  {/* <div className="row text-center my-7">
                                    <div className="d-flex justify-content-center">
                                      {isExceptionRole() &&
                                        this.state.dataxpelatihan.status_substansi?.toLowerCase() !=
                                          "disetujui" && (
                                          <a
                                            href="#"
                                            className={`btn btn-secondary btn-sm`}
                                            onClick={(e) => {
                                              this.setState({
                                                showingSelSilabus: false,
                                                showResetButton: true,
                                                showBtnNextW2: true,
                                              });
                                            }}
                                            title={`Edit Silabus ${this.state.detailSilabus.nama_silabus}`}
                                          >
                                            Edit
                                          </a>
                                        )}
                                    </div>
                                  </div> */}
                                </div>

                                <div
                                  id="div_createsilabus"
                                  className="col-lg-12 mb-7 fv-row pt-0"
                                  style={{
                                    display: !this.state.showingSelSilabus
                                      ? ""
                                      : "none",
                                  }}
                                >
                                  <div className="card-title">
                                    <div className="row">
                                      <div className="col-lg-12">
                                        <div className="d-flex justify-content-between align-items-start flex-wrap pt-6">
                                          <div className="d-flex flex-column">
                                            <h2 className="fs-5 text-muted mb-3">
                                              Edit Silabus
                                            </h2>
                                          </div>
                                          <div className="d-flex">
                                            <a
                                              href="#"
                                              className="btn btn-secondary btn-sm"
                                              onClick={() => {
                                                this.setState({
                                                  showingSelSilabus: true,
                                                });
                                              }}
                                              title="Pilih Silabus Lain"
                                            >
                                              Batal
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label required">
                                        Nama Silabus
                                      </label>
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan nama silabus"
                                        name="nama_silabus"
                                        onBlur={(e) => {
                                          this.setState({
                                            selnamasilabus: {
                                              ...this.state.selnamasilabus,
                                              label: e.target.value,
                                            },
                                          });
                                        }}
                                        defaultValue={
                                          this.state.selnamasilabus?.label
                                        }
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["nama_silabus"]}
                                      </span>
                                    </div>
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label required">
                                        Total Jam Pelajaran (JP)
                                      </label>
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan total jam pelajaran"
                                        onBlur={(e) => {
                                          this.setState({
                                            selnamasilabus: {
                                              ...this.state.selnamasilabus,
                                              jpsilabus: e.target.value,
                                            },
                                          });
                                        }}
                                        onChange={(e) => {
                                          numericOnly(e.target);
                                        }}
                                        id="total_jp"
                                        name="total_jp"
                                        defaultValue={
                                          this.state.selnamasilabus?.jpsilabus
                                        }
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["total_jp"]}
                                      </span>
                                    </div>
                                    <h2 className="fs-5 text-muted mb-3">
                                      Materi
                                    </h2>
                                    {this.state.isiSilabus.map(
                                      (elem, index) => {
                                        return (
                                          <div className="row align-items-top mb-5 trigger-silabus">
                                            <div
                                              className={`${
                                                index > 0
                                                  ? "col-lg-6"
                                                  : "col-lg-6"
                                              } fw-row`}
                                            >
                                              <label className="form-label required">
                                                Judul Materi
                                              </label>
                                              <input
                                                className="form-control form-control-sm silabus"
                                                placeholder="Masukkan Judul Materi"
                                                defaultValue={elem.materi}
                                                onBlur={(evt) => {
                                                  this.handleInputSilabus(
                                                    "materi",
                                                    evt.target.value,
                                                    elem.id_materi,
                                                  );
                                                }}
                                                key={`isi_silabus_${elem.id_materi}`}
                                                name={`isi_silabus_${elem.id_materi}`}
                                              />
                                              <span
                                                style={{ color: "red" }}
                                              ></span>
                                            </div>
                                            <div
                                              className={`${
                                                index > 0
                                                  ? "col-lg-5"
                                                  : "col-lg-6"
                                              } fw-row`}
                                            >
                                              <label className="form-label required">
                                                Jumlah JP
                                              </label>
                                              <input
                                                className="form-control form-control-sm silabus numeric-silabus"
                                                placeholder="Masukkan Jumlah JP"
                                                key={`jp_silabus_${elem.id_materi}`}
                                                name={`jp_silabus_${elem.id_materi}`}
                                                defaultValue={
                                                  elem.jam_pelajaran
                                                }
                                                onBlur={(evt) => {
                                                  this.handleInputSilabus(
                                                    "jam_pelajaran",
                                                    evt.target.value,
                                                    elem.id_materi,
                                                  );
                                                }}
                                                onChange={(e) => {
                                                  numericOnly(e.target);
                                                }}
                                              />
                                              <span
                                                style={{ color: "red" }}
                                              ></span>
                                            </div>
                                            {index != 0 && (
                                              <div className="col-lg-1">
                                                <div className="mt-7 fv-row">
                                                  <a
                                                    title="Hapus"
                                                    onClick={() => {
                                                      this.handleClickDeleteSilabusAction(
                                                        index,
                                                      );
                                                    }}
                                                    className="btn btn-icon btn-sm btn-danger w-lg-100"
                                                  >
                                                    <i className="bi bi-trash-fill"></i>
                                                  </a>
                                                </div>
                                              </div>
                                            )}
                                          </div>
                                        );
                                      },
                                    )}
                                    <div className="row text-center">
                                      <div className="col-9">
                                        <a
                                          onClick={this.handleClickAddSilabus}
                                          className="btn btn-light text-success d-block fw-semibold btn-sm me-3 mr-2"
                                        >
                                          <i className="bi bi-plus-circle text-success"></i>
                                          Tambah Materi
                                        </a>
                                      </div>
                                      <div className="col-3">
                                        <a
                                          onClick={this.handleClickSaveSilabus}
                                          className="btn btn-success fw-semibold btn-sm"
                                          hidden
                                          id="simpan-silabus"
                                        >
                                          <i className="fa fa-paper-plane me-1"></i>
                                          Simpan Silabus
                                        </a>
                                        <a
                                          data-kt-stepper-action="simpan-silabus"
                                          className="btn btn-success fw-semibold btn-sm"
                                        >
                                          <i className="fa fa-paper-plane me-1"></i>
                                          Simpan Silabus
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div
                              className="flex-column"
                              data-kt-stepper-element="content"
                            >
                              <div className="row mt-7">
                                <input
                                  id="saved-from-flag"
                                  type="text"
                                  className="d-none"
                                  ref={this.savedFormRef}
                                  value={
                                    this.state.dataxpelatihan
                                      .form_pendaftaran_id != null
                                  }
                                />
                                <div id="div_changeform">
                                  <PelatihanPendaftaranEdit
                                    props={this.state.formPendaftaran}
                                    formMaster={this.state.dataxselform}
                                    onDeleteForm={() => {
                                      this.savedFormRef.current.value = "";
                                      this.setState({
                                        showBtnNextW2: false,
                                        dataxpelatihan: {
                                          ...this.state.dataxpelatihan,
                                          form_pendaftaran_id: null,
                                        },
                                      });
                                    }}
                                    statusSubstansi={this.state.dataxpelatihan.status_substansi?.toLowerCase()}
                                    idSlug={this.state.dataxpelatihan.slug}
                                    onSave={(data) => {
                                      this.setState(
                                        {
                                          showBtnNextW2:
                                            !this.state.showBtnNextW2,
                                          dataxpelatihan: {
                                            ...this.state.dataxpelatihan,
                                            form_pendaftaran_id: data[0]?.pid,
                                          },
                                        },
                                        () => {
                                          this.setState({
                                            showBtnNextW2: true,
                                          });
                                        },
                                      );
                                    }}
                                  />
                                </div>
                              </div>
                            </div>

                            <div
                              className="flex-column"
                              data-kt-stepper-element="content"
                            >
                              <div className="row mt-7">
                                <div className="col-lg-12 mb-7 fv-row d-none">
                                  <label className="form-label required">
                                    Komitmen Peserta
                                  </label>
                                  <div className="d-flex">
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="komitmen_peserta"
                                        value="Iya"
                                        id="komitmen_peserta1"
                                        checked={true}
                                        // checked={
                                        //   this.state.dataxpelatihan.komitmen ===
                                        //   "Iya"
                                        // }
                                        onChange={this.handleChangeKomitmen}
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="komitmen_peserta1"
                                      >
                                        Iya
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="komitmen_peserta"
                                        value="Tidak"
                                        id="komitmen_peserta2"
                                        checked={false}
                                        // checked={
                                        //   this.state.dataxpelatihan.komitmen ===
                                        //   "Tidak"
                                        // }
                                        onChange={this.handleChangeKomitmen}
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="komitmen_peserta2"
                                      >
                                        Tidak
                                      </label>
                                    </div>
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["komitmen_peserta"]}
                                  </span>
                                </div>
                                <div
                                  style={{
                                    display: this.state.showingdeskripsi
                                      ? "block"
                                      : "block",
                                  }}
                                >
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Komitmen / Perjanjian/ Ketentuan Pelatihan
                                    </label>
                                    {/* <textarea id="kt_docs_ckeditor_classic" className="form-control form-control-sm" placeholder="Masukkan deskripsi" name="komitmen_deskripsi" defaultValue={this.state.dataxpelatihan.komitmen_deskripsi} onChange={this.handleChangeDeskripsiKomitmen}></textarea> */}
                                    <CKEditor
                                      editor={Editor}
                                      data={
                                        this.state.dataxpelatihan
                                          .komitmen_deskripsi ?? ""
                                      }
                                      name="komitmen_deskripsi"
                                      onReady={(editor) => {}}
                                      config={{
                                        ckfinder: {
                                          // Upload the images to the server using the CKFinder QuickUpload command.
                                          uploadUrl:
                                            process.env.REACT_APP_BASE_API_URI +
                                            "/publikasi/ckeditor-upload-image",
                                        },
                                      }}
                                      onChange={(event, editor) => {}}
                                      onBlur={(event, editor) => {
                                        const data = editor.getData();
                                        document.getElementsByName(
                                          "komitmen_deskripsi_hidden",
                                        )[0].value = "ada";

                                        this.handleChangeDeskripsiKomitmen(
                                          data,
                                        );
                                      }}
                                      onFocus={(event, editor) => {}}
                                    />
                                    <input
                                      type="hidden"
                                      name="komitmen_deskripsi_hidden"
                                      // value={
                                      //   this.state.dataxpelatihan
                                      //     .komitmen_deskripsi ?? ""
                                      // }
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["komitmen_deskripsi"]}
                                    </span>
                                  </div>
                                </div>
                                <div className="text-center">
                                  {/* <button onClick={this.handleClickBatal} className="btn btn-secondary btn-sm me-3 mr-2">Daftar Pelatihan</button> */}
                                  {/* <button onClick={this.handleClick3} type="button" className="btn btn-primary btn-sm me-3" data-kt-permissions-modal-action="submit">Simpan</button> */}
                                  {/* <button type="button" className="btn btn-lg btn-primary" className="indicator-label">Simpan</button> */}
                                </div>
                              </div>
                            </div>

                            <div className="text-center border-top pt-10 my-7">
                              <button
                                className="btn btn-light btn-md me-2"
                                data-kt-stepper-action="previous"
                                onClick={() => {
                                  this.setState({ showBtnNextW2: true });
                                }}
                              >
                                <i className="fa fa-chevron-left"></i>Sebelumnya
                              </button>

                              <button
                                type="button"
                                className="btn btn-primary btn-md me-2 d-none"
                                data-kt-stepper-action="submit"
                                ref={this.submitFormRef}
                              ></button>

                              <button
                                disabled={this.state.isLoading}
                                type="button"
                                id="dummy-submit"
                                className="btn btn-primary btn-md me-2 d-none"
                                onClick={() => {
                                  swal
                                    .fire({
                                      title: "Apakah anda yakin?",
                                      text: "Pastikan data Pelatihan telah benar, Silabus dan Form Pendaftaran tidak dapat diubah jika nantinya telah disetujui Admin Akademi",
                                      icon: "warning", // add html attribute if you want or remove
                                      showCancelButton: true,
                                      confirmButtonColor: "#3085d6",
                                      cancelButtonColor: "#d33",
                                      confirmButtonText: "Ya, Simpan",
                                      cancelButtonText: "Batal",
                                      allowOutsideClick: false,
                                    })
                                    .then((result) => {
                                      if (result.isConfirmed) {
                                        this.submitFormRef.current?.click();
                                      }
                                    });
                                }}
                              >
                                {this.state.isLoading ? (
                                  <>
                                    <span
                                      className="spinner-border spinner-border-sm me-2"
                                      role="status"
                                      aria-hidden="true"
                                    ></span>
                                    <span className="sr-only">Loading...</span>
                                    Loading...
                                  </>
                                ) : (
                                  <>
                                    <i className="fa fa-paper-plane me-1"></i>
                                    Simpan
                                  </>
                                )}
                              </button>

                              <button
                                type="submit"
                                className="btn btn-primary btn-md me-2 d-none"
                                id="submit-form"
                              ></button>

                              <div
                                id="next_id"
                                style={{
                                  display: this.state.showBtnNextW2
                                    ? "inline"
                                    : "none",
                                }}
                              >
                                <button
                                  type="button"
                                  className="btn btn-primary btn-md me-2"
                                  data-kt-stepper-action="next"
                                  onClick={(e) => {
                                    if (
                                      (Cookies.get("step") == 2 &&
                                        this.state.showOptionButtonSilabus &&
                                        this.state.detailSilabus.id != "") ||
                                      (Cookies.get("step") == 3 &&
                                        this.state.showOptionButton &&
                                        this.state.dataxpreview)
                                    ) {
                                      this.setState({ showBtnNextW2: false });
                                      if (Cookies.get("step") == 3) {
                                        this.handleClickRefFormAction();
                                      }
                                    } else {
                                      this.setState({ showBtnNextW2: true });
                                    }
                                  }}
                                >
                                  <i className="fa fa-chevron-right"></i>
                                  Lanjutkan
                                </button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="modal fade" tabindex="-1" id="kt_modal_3">
          <div className="modal-dialog modal-xl">
            <div className="modal-content position-absolute">
              <div className="modal-header">
                <h5 id="preview_title" className="modal-title">
                  {this.state.dataxpreviewtitle}
                </h5>
                <div
                  className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <span className="svg-icon svg-icon-2x"></span>
                </div>
              </div>
              <div className="modal-body">
                {this.state.dataxpreview.map((data) => (
                  <div dangerouslySetInnerHTML={{ __html: data.html }}></div>
                ))}
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-light"
                  data-bs-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
