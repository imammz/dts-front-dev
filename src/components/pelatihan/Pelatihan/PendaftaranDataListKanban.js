import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import Swal from "sweetalert2";
import Board, { moveCard, addCard } from "@asseinfo/react-kanban";
import withReactContent from "sweetalert2-react-content";
import { useParams, useNavigate, withRouter } from "react-router-dom";
import "../../Pendaftaran-Content/sytle_kaban_una.css";
import "@asseinfo/react-kanban/dist/styles.css";
import { capitalizeFirstLetter } from "../../publikasi/helper";
import { loadTrigred_rekursive } from "../../pelatihan/FormHelper";
import Cookies from "js-cookie";

const PendaftaranDataLKanban = ({
  props = null,
  statusSubstansi = null,
  formMaster = [],
  idSlug,
  onSave,
  onDeleteForm = () => {},
}) => {
  var alertHapus = 0;
  let dataForm = [];
  let judulForm = [];
  let statusPublish = 0;

  const initialPendaftaranState = {
    id: "0",
    judul: "",
  };

  const judulRef = useRef(null);
  // var eleRequired = useRef([]);

  const [RepoSize, setRepoSize] = useState([]);
  const [FormElement, setFormElement] = useState([]);
  const [JudulFormPendaftaran, setJudul] = useState("");
  const [StatusFormPendaftaran, setStatus] = useState(1);
  const [focusId, setFocusId] = useState({ id: "" });
  var [IdFormPendaftaran, setIdFormPendaftaran] = useState("");
  const [valuePrev, setValuePrev] = useState(0);
  const [kategoriOption, setKategoriOption] = useState([]);
  const [kategoriForm, setKategoriForm] = useState("");
  const [selectedFormMaster, setSelectedFormMaster] = useState(null);
  const [currentForm, setCurrentForm] = useState(null);
  const [formDeleted, setFormDeleted] = useState(false);
  const [loading, setLoading] = useState(false);
  const MySwal = withReactContent(Swal);
  // let getForm = [];

  var checkElement = [];

  const retriveRepository = (filter = null, isonDelete = false) => {
    if (isonDelete) {
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });
    }
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/list-repo-v2",
        {
          start: "0",
          length: "50",
          cari: filter == null ? "" : filter,
          sort: "name",
          sort_val: "ASC",
        },
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + Cookies.get("token"),
          },
        },
      )
      .then(function (response) {
        if (isonDelete) {
          Swal.close();
        }
        if (response.status === 200) {
          let repo = response.data.result;
          if (repo.Status === true) {
            let columns = [];
            repo.Data.map((number) =>
              columns.push({
                id: number.id,
                title: number.name,
                element: number.element,
                maxlength: number.maxlength,
                placeholder: number.placeholder,
                min: number.min,
                max: number.max,
                required: "",
                size: number.size,
                option: number.option,
                data_option: number.data_option,
                className: number.className,
                html: number.html,
                description: number.name,
                name: number.name,
                file_name: number.file_name,
                child: number.child,
                span: number.span,
                hapus: false,
                ref_values: number.ref_values,
                kategori: number.kategori,
                id_form_kategori: number.id_form_kategori,
              }),
            );
            setRepoSize(columns);
          } else {
            setRepoSize([]);
          }
        }
      })
      .catch((err) => {
        const message = err.response?.data?.result?.Message;
        Swal.fire({
          title: "Perhatian!",
          text: message ?? "Form element tidak ditemukan",
          icon: "warning",
        }).then((result) => {
          if (result.isConfirmed) {
            retriveRepository();
            setKategoriForm("");
          }
        });
        setRepoSize([]);
      });
  };

  const getFormMaster = (e) => {
    const { value, options, selectedIndex } = e.target;
    setSelectedFormMaster({
      value: value,
      label: options[selectedIndex].innerHTML,
    });
    Swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      },
    });
    const dataFormSubmit = new FormData();
    dataFormSubmit.append("id", value);
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/cari-formbuilder",
        dataFormSubmit,
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + Cookies.get("token"),
          },
        },
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          const valueForm = res.data.result;
          retriveRepository_filter(valueForm);
          Swal.close();
        } else {
          Swal.fire({
            title: messagex,
            icon: "warning",
            confirmButtonText: "Ok",
          }).then((result) => {
            if (result.isConfirmed) {
              //this.handleReload();
            }
          });
        }
      })
      .catch((err) => {
        console.error(err);
        Swal.fire({
          title:
            err.response?.data?.result?.Message ??
            "Terjadi kesalahan saat mengambil form",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      });
  };

  const fetchKategori = () => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/daftarpeserta/list-kategori-form",
      )
      .then(function (response) {
        if (response.status == 200) {
          setKategoriOption(response.data.result.Data);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const board = {
    columns: [
      {
        id: 1,
        title: "Daftar Form Element",
        cards: RepoSize,
      },
      {
        id: 2,
        title: "Canvas Form Pendaftaran",
        cards: FormElement,
      },
    ],
  };

  useEffect(() => {
    retriveRepository();
  }, []);

  useEffect(() => {
    if (FormElement.length > 0 && focusId.id != "") {
      window?.document
        .querySelector(
          '[data-rbd-drag-handle-draggable-id="' + focusId.id + '"]',
        )
        ?.scrollIntoView();
    }
    // console.log("Focus Id:", focusId);
  }, [focusId]);

  function changeFocusId(id) {
    // window?.document
    //   .querySelector('[data-rbd-drag-handle-draggable-id="' + id + '"]')
    //   ?.scrollIntoView();
    setFocusId({ id: id });
  }

  useEffect(() => {
    // if (filter != null) {
    retriveRepository_filter(props);
    // }
    fetchKategori();
  }, [props]);

  function safeRequired(value, id) {
    if (value) {
      checkElement[id] = "required";
    } else {
      checkElement[id] = "";
    }

    changeFocusId(id);
    let send = [];

    FormElement.map((ress, index) => {
      send.push({
        judul: judulRef.current.value,
        id: ress.id,
        name: ress.name,
        description: ress.description,
        title: ress.title,
        element: ress.element,
        status: StatusFormPendaftaran,
        className: ress.className,
        maxlength: ress.maxlength,
        placeholder: ress.placeholder,
        required:
          id == ress.id ? (value == true ? "required" : "") : ress.required,
        option: ress.option,
        data_option: ress.data_option,
        size: ress.size,
        min: ress.min,
        max: ress.max,
        html: ress.html,
        id_repository: ress.id,
        span: ress.span,
        file_name: ress.file_name,
        child: ress.child,
        ref_values: ress.ref_values,
        hapus: true,
        kategori: ress.kategori,
        id_form_kategori: ress.id_form_kategori,
      });
    });
    // console.log("form element:", FormElement);

    // FormElement.forEach((ress, index) => {
    //   if (ress.id == id && value == true) {
    //     ress.required = "required";
    //   }
    // });

    setFormElement(send);

    // eleRequired.current.value = send;
    // console.log("dari safe required", FormElement);
  }

  const retriveRepository_filter = (form) => {
    if (form && form.Status) {
      let columns = [];
      const listItem = form.detail.map((number) =>
        columns.push({
          id: number.id,
          title: number.name,
          name: number.name,
          description: number.name,
          element: number.element,
          maxlength: number.maxlength,
          placeholder: number.placeholder,
          min: number.min,
          max: number.max,
          required: number.required,
          size: number.size,
          option: number.option,
          data_option: number.data_option,
          className: number.className,
          html: number.html,
          file_name: number.file_name,
          child: number.child,
          ref_values: number.ref_values,
          span: number.span,
          hapus: true,
          id_form_kategori: number.id_form_kategori,
          kategori: number.kategori,
        }),
      );

      setFormElement(columns);
      // getForm = columns;
      // retriveRepository();
      if (props) {
        setStatus(form.utama[0].status);
        // setJudul(props.utama[0].judul_form);
        setIdFormPendaftaran(form.utama[0].id);
        judulRef.current.value = form.utama[0].judul_form;
      }

      dataForm = columns;
    } else {
    }
  };

  function PreviewForm() {
    return (
      <div
        className="modal fade"
        tabindex="-1"
        id="preview-form"
        key={valuePrev}
      >
        <div className="modal-dialog modal-md">
          <div className="modal-content">
            <div className="modal-header pe-0 pb-0">
              <div className="row w-100">
                <div className="col-12 d-flex w-100 justify-content-between">
                  <h5 className="modal-title">
                    Preview: {judulRef.current?.value}
                    <span className={"badge badge-light-primary fs-7 m-1"}>
                      {StatusFormPendaftaran == 1
                        ? "Publish"
                        : StatusFormPendaftaran == 0
                          ? "Unpublish"
                          : "Belum ada data"}
                    </span>
                  </h5>
                  <div
                    className="btn btn-icon btn-sm btn-active-light-primary"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                  >
                    <span className="svg-icon svg-icon-2x">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                      >
                        <rect
                          opacity="0.5"
                          x="6"
                          y="17.3137"
                          width="16"
                          height="2"
                          rx="1"
                          transform="rotate(-45 6 17.3137)"
                          fill="currentColor"
                        />
                        <rect
                          x="7.41422"
                          y="6"
                          width="16"
                          height="2"
                          rx="1"
                          transform="rotate(45 7.41422 6)"
                          fill="currentColor"
                        />
                      </svg>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className="container mt-4">
              {FormElement &&
                FormElement.map((elem) => (
                  <div className="pb-5">{loadTrigred_rekursive(elem)}</div>
                ))}
            </div>

            <div className="modal-footer">
              <a
                className="btn btn-light btn-sm"
                href="#"
                data-bs-dismiss="modal"
                onClick={(e) => {
                  e.preventDefault();
                }}
              >
                Close
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }

  function UncontrolledBoard() {
    let cek = 0;
    return (
      <>
        <Board
          allowRemoveLane
          disableColumnDrag
          allowAddCard
          allowRemoveCard
          onLaneRemove={console.log}
          onCardRemove={(board, column, card) => {
            console.log(card);
            alertHapus = 1;

            if (
              window.confirm(
                "Apakah anda yakin akan menghapus Elemen " + card.title + " ? ",
              )
            ) {
              MySwal.fire({
                title: <strong>Berhasil dihapus!</strong>,
                html: <i>Elemen {card.title} dihapus</i>,
                allowOutsideClick: false,
                icon: "success",
              });

              card.hapus = false;
              console.log(board.columns[0].cards.unshift(card));
              let idx = 0;
              FormElement.forEach((elem, index) => {
                if (elem.id == card.id) {
                  idx = index;
                  return;
                }
                FormElement.splice(idx, 1);
              });
            } else {
              console.log(board.columns[1].cards.unshift(card));
            }

            //  addCard(board,1,card)
          }}
          onLaneRename={console.log}
          onCardDragEnd={(board, card, source, destination) => {
            if (destination.toColumnId == 2) {
              card.hapus = true;
              // console.log(card.hapus);
            } else {
              card.hapus = false;
            }
            //setFormSave(board.columns[1].cards);

            // console.log(board.columns[1].cards);

            dataForm = board.columns[1].cards;

            setRepoSize(board.columns[0].cards);
            setFormElement(board.columns[1].cards);
            changeFocusId(card.id);

            // setFormData(board.columns[1].cards);
          }}
          initialBoard={board}
          onCardNew={console.log}
          renderCard={(card, { removeCard, dragging }) => (
            <div>
              <div
                dragging={dragging}
                className="react-kanban-card"
                style={{ minWidth: "350px", width: "100%" }}
              >
                <div className="row">
                  <div className="col-lg-12">
                    <div className="card card-custom">
                      {card.hapus ? (
                        <div className="d-flex justify-content-end">
                          <a
                            onClick={removeCard}
                            className="btn btn-danger btn-hover-danger btn-sm btn-icon"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                          >
                            <i className="fa la-trash text-white mr-5"></i>
                          </a>
                        </div>
                      ) : (
                        <span> </span>
                      )}

                      {card.kategori && (
                        <span className="badge badge-light-success fs-7 m-1">
                          {card.kategori}
                        </span>
                      )}
                      {loadTrigred_rekursive(card)}

                      {card.hapus ? (
                        <div
                          className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                          style={{ marginTop: "6px" }}
                        >
                          <input
                            className="form-check-input h-20px w-30px"
                            type="checkbox"
                            checked={card.required == "required" ? true : false}
                            onChange={(event) => {
                              safeRequired(event.target.checked, card.id);
                              // setFocusId(card.id);
                              // console.log("required:", event, card.id);
                            }}
                          />
                          <label
                            className="form-check-label"
                            for="flexSwitch20x30"
                          >
                            required
                          </label>
                        </div>
                      ) : (
                        <div>
                          {" "}
                          <br />{" "}
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
        />
      </>
    );
  }

  function safeForm() {
    console.log("save form pendaftaran");
    let send = { categoryOptItems: [] };
    let msg = "";
    if (FormElement.length == 0) {
      if (FormElement.length == 0) {
        msg += "<span> Element Belum diisi </span> <br/>";
      }

      MySwal.fire({
        title: <strong> Form Pendaftaran Gagal Disimpan </strong>,
        html: msg,
        icon: "warning",
      });
    } else {
      const required = FormElement.filter(
        (elem) => elem.required == "required",
      );
      if (required.length == 0) {
        msg +=
          "<span> Minimal harus terdapat satu isian wajib (required). </span> <br/>";

        MySwal.fire({
          title: <strong> Form Pendaftaran Gagal Disimpan </strong>,
          html: msg,
          icon: "warning",
        });
        return;
      }
      FormElement.map((ress, index) => {
        const elem = {
          id: ress.id,
          name: ress.name,
          description: ress.description,
          title: ress.title,
          judul: formDeleted
            ? "Form Pendaftaran_" + idSlug?.toUpperCase() + "[id]"
            : judulRef.current.value,
          element: ress.element,
          status: 1,
          className: ress.className,
          maxlength: ress.maxlength,
          placeholder: ress.placeholder,
          required: ress.required,
          option: ress.option,
          data_option: ress.data_option,
          size: ress.size,
          min: ress.min,
          max: ress.max,
          html: ress.html,
          file_name: ress.file_name,
          ref_values: [],
          child: ress.child,
          span: ress.span,
          id_repository: ress.id,
          kategori: ress.kategori,
          id_form_kategori: ress.id_form_kategori,
        };
        if (props == null) {
          elem["judul"] = capitalizeFirstLetter(judulRef.current.value);
        }
        send.categoryOptItems.push(elem);
      });
      if (props != null) {
        send["idform"] = IdFormPendaftaran;
        send["judul"] = props.utama[0].judul_form;
        send["status"] = props.utama[0].status;
      }
      if (currentForm != null) {
        send["idform"] = currentForm[0].pid;
        send["judul"] = capitalizeFirstLetter(judulRef.current.value);
        send["status"] = StatusFormPendaftaran;
      }
      if (formDeleted) {
        delete send["idform"];
        send["judul"] = "Form Pendaftaran_" + idSlug?.toUpperCase() + "[id]";
        // send["title"] = "Form Pendaftaran_" + idSlug?.toUpperCase() + "[id]";
      }
      let url =
        process.env.REACT_APP_BASE_API_URI +
        ((props != null || currentForm) && !formDeleted
          ? "/daftarpeserta/editjson-formbuilder"
          : "/daftarpeserta/createjson-formbuilder");

      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });
      setLoading(true);
      axios
        .post(url, send, {
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + Cookies.get("token"),
          },
        })
        .then(function (response) {
          setLoading(false);
          if (response.data.result.Status) {
            MySwal.fire({
              title: <strong>Berhasil simpan!</strong>,
              html: (
                <i>
                  Berhasil {props || currentForm ? "mengubah" : "membuat"}{" "}
                  {formDeleted
                    ? "Form Pendaftaran_" + idSlug?.toUpperCase() + "[id]"
                    : judulRef.current.value}
                </i>
              ),
              allowOutsideClick: false,
              icon: "success",
            }).then(() => {
              setCurrentForm(response.data.result.Data);
              onSave(response.data.result.Data);
              if (formDeleted) {
                setFormDeleted(false);
              }
            });
          } else {
            throw new Error(response.data.result.Message);
          }
        })
        .catch(function (error) {
          setLoading(false);
          MySwal.fire({
            title: <strong>Information!</strong>,
            html: (
              <i>
                {" "}
                {typeof error.message == "string"
                  ? error.message
                  : "Gagal Simpan"}
              </i>
            ),
            icon: "warning",
          });
          console.log(error);
        });
    }
  }

  function safeJudul(args) {
    judulForm = args;

    setJudul(args);
  }

  function back() {
    window.location = "/pelatihan/pendaftaran";
  }

  function changeStatus(args) {
    statusPublish = args;
    setStatus(args);
  }

  function changeKategori(args) {
    setKategoriForm(args);
    retriveRepository(args);
  }

  const [Pendaftaran, setPendaftaran] = useState(initialPendaftaranState);

  return (
    <div>
      <PreviewForm />
      <div className="mb-6">
        <div className="row">
          {!props && (
            <>
              <div className="col-12 bg-light-warning text-warning mx-3 mb-5 fw-semibold rounded p-5 fs-7">
                Pada saat membuat form pendaftaran, minimal harus terdapat 1
                isian wajib (required)
              </div>

              <div className="col-lg-12 fv-row d-none">
                <label for="form-master" className="form-label">
                  Form Master <br />
                  <span className="small text-muted">
                    (Buat form berdasarkan master yang ada)
                  </span>
                </label>
                <select
                  className="form-select form-select-sm"
                  data-placeholder="Pilih Form Master"
                  placeholder="Pilih Form Master"
                  id="form-master"
                  data-allow-clear="true"
                  name="form_master"
                  onChange={(e) => {
                    getFormMaster(e);
                  }}
                >
                  <option value="" style={{ display: "none" }}>
                    Silahkan pilih
                  </option>
                  {formMaster &&
                    formMaster.map((opt, index) => (
                      <option key={`level_${index}`} value={opt.value}>
                        {opt.label}
                      </option>
                    ))}
                </select>
              </div>
            </>
          )}
          <div className="col-lg-12 fv-row mt-5 d-none">
            <label
              for="exampleFormControlInput1"
              className="required form-label"
            >
              Judul Form
            </label>
            <input
              type="text"
              id="judul"
              ref={judulRef}
              disabled
              value={
                props
                  ? props.judul_form
                  : `Form Pendaftaran_${
                      props ? idSlug : idSlug?.toUpperCase() + "[id]"
                    }`
              }
              name="judul_form_builder"
              className="form-control form-control-sm"
              placeholder="Masukan Judul Form ...."
            />
          </div>
          {/* <div className="col-lg-12 fv-row mt-5">
            <label
              for="exampleFormControlInput2"
              className="required form-label"
            >
              Status Publish
            </label>
            <select
              onChange={(event) => changeStatus(event.target.value)}
              value={StatusFormPendaftaran}
              className="form-select form-select-sm"
              data-placeholder="Pilih Status Publish"
              placeholder="Pilih Status Publish"
              data-allow-clear="true"
              name="status_publish"
            >
              <option value="" style={{ display: "none" }}>
                Silahkan pilih
              </option>
              <option value={1}>Publish</option>
              <option value={0}>Unpublish</option>
            </select>
          </div> */}
          <div className="col-lg-12">
            <div className="d-flex justify-content-between align-items-center flex-wrap pt-6 px-6">
              {!formDeleted && props != null && (
                <div className="d-flex">
                  <h5>{props?.utama && props?.utama[0].judul_form}</h5>
                </div>
              )}
              <div className="d-flex">
                {!formDeleted && props != null && (
                  <a
                    href="#"
                    className={`btn btn-danger btn-sm ${
                      statusSubstansi?.toLowerCase() == "disetujui"
                        ? "disabled"
                        : ""
                    }`}
                    onClick={() => {
                      props = null;
                      setFormElement([]);
                      onDeleteForm();
                      setFormDeleted(true);
                      retriveRepository(null, true);
                    }}
                    title="Buat form pendaftaran baru"
                  >
                    <i className="fa la-trash text-white mr-5"></i>
                    Hapus dan Buat Form Baru
                  </a>
                )}
              </div>
            </div>
          </div>
          {(formDeleted || props == null) && (
            <div>
              <h2 className="fs-5 mt-3 text-muted mb-3">
                Drag atau geser Element ke dalam Canvas
              </h2>
              <div className="col-lg-12 mb-7 fv-row">
                <label className="form-label">Kategori Form Element</label>
                <select
                  onChange={(event) => changeKategori(event.target.value)}
                  className="form-select form-select-sm"
                  data-placeholder="Pilih Kategori"
                  placeholder="Pilih Kategori"
                  data-allow-clear="true"
                  value={kategoriForm}
                  name="kategori_form"
                  disabled={kategoriOption.length == 0}
                >
                  <option value="" selected>
                    Semua Kategori
                  </option>
                  {kategoriOption.map((elem) => (
                    <option value={elem.value}>{elem.label}</option>
                  ))}
                </select>
              </div>
            </div>
          )}
        </div>
      </div>

      <div className="mb-10">
        {alertHapus == 1 ? (
          <div className="alert alert-danger" role="alert">
            <div className="alert-text">
              {" "}
              Element {alertHapus} berhasil dihapus{" "}
            </div>
          </div>
        ) : (
          <div> </div>
        )}
      </div>
      {(formDeleted || props == null) && <UncontrolledBoard />}
      {!formDeleted && props != null && (
        <div className="container mt-4">
          {FormElement &&
            FormElement.map((elem) => (
              <div className="pb-5">{loadTrigred_rekursive(elem)}</div>
            ))}
        </div>
      )}
      <div className="text-center m-10 mb-5">
        <a
          href="#"
          className="btn btn-light btn-sm me-2"
          data-kt-stepper-action="preview-form"
          data-bs-toggle="modal"
          data-bs-target="#preview-form"
          id="click-preview"
          onClick={() => {
            setValuePrev(valuePrev + 1);
          }}
        >
          <i className="fa fa-eye"></i>
          Preview Form
        </a>

        <button
          // href="#"
          type="button"
          className={`btn btn-success btn-sm ${loading ? "disabled" : ""} ${
            formDeleted || props == null ? "" : "d-none"
          }`}
          data-kt-stepper-action="simpan-form"
          id="click-ok-coba-dummy"
        >
          <i className="fa fa-paper-plane"></i>
          Simpan Form Pendaftaran
        </button>

        <a
          style={{ display: "none" }}
          href="#"
          className="btn btn-primary btn-sm"
          id="simpan-form"
          onClick={safeForm}
        ></a>
      </div>
    </div>
  );
};

export default PendaftaranDataLKanban;
