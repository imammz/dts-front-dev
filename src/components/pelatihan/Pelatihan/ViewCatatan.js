import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import "../../Pendaftaran-Content/sytle_kaban_una.css";
import "@asseinfo/react-kanban/dist/styles.css";
import moment from "moment";

const ViewCatatan = ({ pelatihanId = null, configs, onClose }) => {
  const viewCatatanTriggerRef = useRef(null);
  const catatanModalRef = useRef(null);
  const [listCatatan, setListCatatan] = useState([]);

  const MySwal = withReactContent(Swal);

  useEffect(() => {
    if (pelatihanId == null) {
      moment.locale("id");
      return;
    }
    MySwal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        MySwal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/pelatihanz/viewCatRevisi",
        {
          pelatian_id: pelatihanId,
        },
        configs,
      )
      .then((result) => {
        const response = result.data.result;
        if (response.Status) {
          setListCatatan(response.Data);
          MySwal.close();
          viewCatatanTriggerRef.current.click();
        } else {
          throw Error(response.Message);
        }
      })
      .catch((err) => {
        console.log(err);
        const message =
          typeof err == "string"
            ? err
            : err.response.data?.result?.Message
              ? err.response.data?.result?.Message
              : "Terjadi kesalahan.";

        MySwal.fire({
          title: message,
          icon: "warning",
          confirmButtonText: "Ok",
        });
      });
  }, [pelatihanId]);

  useEffect(() => {
    catatanModalRef.current.addEventListener("hidden.bs.modal", onClose);
    return () => {
      catatanModalRef.current.removeEventListener("hidden.bs.modal", onClose);
    };
  }, []);

  return (
    <div>
      <a
        href="#"
        hidden
        data-bs-toggle="modal"
        data-bs-target="#view-catatan"
        ref={viewCatatanTriggerRef}
      ></a>
      <div
        className="modal fade"
        tabindex="-1"
        id="view-catatan"
        ref={catatanModalRef}
      >
        <div className="modal-dialog modal-md">
          <div className="modal-content">
            <div className="modal-header pe-0 pb-0">
              <div className="row w-100">
                <div className="col-12 d-flex w-100 justify-content-between">
                  <h5 className="modal-title">Catatan Revisi</h5>
                  <div
                    className="btn btn-icon btn-sm btn-active-light-primary"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                  >
                    <span className="svg-icon svg-icon-2x">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                      >
                        <rect
                          opacity="0.5"
                          x="6"
                          y="17.3137"
                          width="16"
                          height="2"
                          rx="1"
                          transform="rotate(-45 6 17.3137)"
                          fill="currentColor"
                        />
                        <rect
                          x="7.41422"
                          y="6"
                          width="16"
                          height="2"
                          rx="1"
                          transform="rotate(45 7.41422 6)"
                          fill="currentColor"
                        />
                      </svg>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className="container mt-4">
              {listCatatan.length > 0 ? (
                listCatatan.map((elem) => (
                  <div className="d-flex align-items-center bg-light-warning rounded p-5 mb-2">
                    <span className="svg-icon svg-icon-warning me-5">
                      <span className="svg-icon svg-icon-1">
                        <svg
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                          className="mh-50px"
                        >
                          <path
                            opacity="0.3"
                            d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                            fill="#ffc700"
                          ></path>
                          <path
                            d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                            fill="#ffc700"
                          ></path>
                        </svg>
                      </span>
                    </span>
                    <div className="flex-grow-1 me-2">
                      <a href="#" className="fw-bolder text-gray-800 fs-6">
                        {elem.revisi}
                      </a>
                      <span className="fw d-block">
                        {moment(elem.created_at).isValid()
                          ? moment(elem.created_at).format(
                              "DD MMMM YYYY, HH:mm:ss",
                            )
                          : "-"}
                      </span>
                    </div>
                  </div>
                ))
              ) : (
                <>
                  <em className="fw-bolder">Tidak ada catatan!</em>
                </>
              )}
            </div>

            <div className="modal-footer">
              <button className="btn btn-light btn-sm" data-bs-dismiss="modal">
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ViewCatatan;
