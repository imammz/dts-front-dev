import React, { useState, useEffect, useRef } from "react";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import "../../Pendaftaran-Content/sytle_kaban_una.css";
import "@asseinfo/react-kanban/dist/styles.css";
import { loadTrigred_rekursive } from "../FormHelper";

const RenderFormElement = ({ formElements = [] }) => {
  const MySwal = withReactContent(Swal);

  return (
    <>
      {formElements?.length > 0 &&
        // Render Form Pisah biar bisa reusable di edit
        formElements.map((elem) => (
          <div className="pb-5">{loadTrigred_rekursive(elem)}</div>
        ))}
    </>
  );
};

export default RenderFormElement;
