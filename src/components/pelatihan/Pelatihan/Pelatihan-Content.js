import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import DataTable from "react-data-table-component";
import moment from "moment";
import ViewCatatan from "./ViewCatatan";
import { handleFormatDate, exportToCSV, dateRange } from "./helper";
import { capitalWord } from "../Pelatihan/helper";
import { fixBootstrapDropdown } from "./../../../utils/commonutil";
import {
  isSuperAdmin,
  isAdminAkademi,
} from "./../../../components/AksesHelper";

export default class PelatihanContent extends React.Component {
  constructor(props) {
    super(props);
    // this.handleClickDelete = this.handleDelete.bind(this);
    this.viewCatatanTriggerRef = React.createRef(null);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleChangeSearch = this.handleChangeSearchAction.bind(this);
    this.handleClickSearch = this.handleClickSearchAction.bind(this);
    this.handleClickReset = this.handleClickResetAction.bind(this);
    this.handleClickReview = this.handleClickReviewAction.bind(this);
    this.handleAllRow = this.handleClickResetAction.bind(this);
    this.handleClickRevisi = this.handleClickRevisiAction.bind(this);
    this.handleClickDisetujui = this.handleClickDisetujuiAction.bind(this);
    this.handleClickBerjalan = this.handleClickBerjalanAction.bind(this);
    this.handleClickSelesai = this.handleClickSelesaiAction.bind(this);
    this.handleClickDitolak = this.handleClickDitolakAction.bind(this);
    this.handleClickDibatalkan = this.handleClickDibatalkanAction.bind(this);
    // this.handleClickExport = this.handleClickExportAction.bind(this);
    this.handleChangePenyelenggara =
      this.handleChangePenyelenggaraAction.bind(this);
    this.handleChangeAkademi = this.handleChangeAkademiAction.bind(this);
    this.handleChangeTema = this.handleChangeTemaAction.bind(this);
    this.handleChangeStatusSubstansi =
      this.handleChangeStatusSubstansiAction.bind(this);
    this.handleChangeStatusPelatihan =
      this.handleChangeStatusPelatihanAction.bind(this);
    this.handleChangeStatusPublish =
      this.handleChangeStatusPublishAction.bind(this);
    this.handleChangeProvinsi = this.handleChangeProvinsiAction.bind(this);
    this.handleChangeYear = this.handleChangeYearAction.bind(this);
  }
  state = {
    isFilter: false,
    datax: [],
    dataxpenyelenggaravalue: "",
    dataxakademivalue: "",
    dataxtemavalue: "",
    dataxstatussubstansivalue: "",
    dataxstatussubstansiheadervalue: "",
    dataxstatuspelatihanvalue: "",
    dataxstatuspublishvalue: "",
    dataxprovinsivalue: "",
    dataxstatuspelatihanheadervalue: "",
    dataxtahunvalue: "",
    review: 0,
    revisi: 0,
    disetujui: 0,
    berjalan: 0,
    selesai: 0,
    ditolak: 0,
    dibatalkan: 0,
    isDisabled: true,
    dataxpenyelenggara: [],
    loading: true,
    totalRows: 0,
    newPerPage: 10,
    numberrow: 1,
    from: "home",
    issearch: false,
    dataxsortvalue: null,
    dataxsortvalvalue: null,
    currentyear: new Date().getFullYear(),
    url_download: "",
    param: "",
    clickbox: false,
    listCatatan: [],
    substansistatus: [],
    selectedRowId: null,
    isRowChangeRef: false,
    totalDataAll: 0,
  };
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  optionstatus = [
    { value: "99", label: "Semua" },
    { value: "1", label: "Publish" },
    { value: "0", label: "Unpublish" },
    { value: "2", label: "Unlisted" },
  ];
  substansistatus = [
    { value: "Review", label: "Review" },
    { value: "Revisi", label: "Revisi" },
    { value: "Disetujui", label: "Disetujui" },
    { value: "Ditolak", label: "Ditolak" },
  ];
  pelatihanstatus = [
    { value: "0", label: "Semua" },
    { value: "Review Substansi", label: "Review Substansi" },
    { value: "Menunggu Pendaftaran", label: "Menunggu Pendaftaran" },
    { value: "Pendaftaran", label: "Pendaftaran" },
    { value: "Seleksi", label: "Seleksi" },
    { value: "Pelatihan", label: "Pelatihan" },
    { value: "Selesai", label: "Selesai" },
    { value: "Dibatalkan", label: "Dibatalkan" },
  ];
  generateYear() {
    let year = new Date().getFullYear();
    let range = year - 2019;
    let optionyear = [];
    optionyear.push({ value: 0, label: "Semua" });
    for (let i = 0; i <= range; i++) {
      optionyear.push({ value: 2019 + i, label: 2019 + i });
    }
    this.setState({ optionyear: optionyear });
  }
  customLoader = (
    <div style={{ padding: "24px" }}>
      <img src="/assets/media/loader/loader-biru.gif" />
    </div>
  );
  columns = [
    {
      name: "No",
      center: true,
      width: "70px",
      cell: (row, index) => (
        <div>
          <span>{this.state.numberrow + index + 0}</span>
          <br />
        </div>
      ),
    },
    {
      name: "ID",
      width: "100px",
      sortable: true,
      selector: (row) => (
        <div>
          <span className="fs-7">{row.slug_pelatian_id}</span>
        </div>
      ),
    },
    {
      name: "Nama Pelatihan",
      sortable: true,
      selector: (row) => capitalWord(row.pelatihan),
      width: "350px",
      cell: (row) => (
        <div>
          <a
            href={"/pelatihan/detail-pelatihan/" + row.id}
            id={row.id}
            title="Detail"
            className="text-dark"
          >
            <label className="d-flex flex-stack my-2 cursor-pointer">
              <span className="d-flex align-items-center me-2">
                <span className="symbol symbol-50px me-6">
                  <span className="symbol-label bg-light-primary">
                    <span className="svg-icon svg-icon-1 svg-icon-primary">
                      {row.mitra_logo == null ? (
                        <img
                          src={`/assets/media/logos/logo-kominfo.png`}
                          alt=""
                          height="100px"
                          className="symbol-label"
                        />
                      ) : (
                        <img
                          src={
                            process.env.REACT_APP_BASE_API_URI +
                            "/download/get-file?path=" +
                            row.mitra_logo +
                            "&disk=dts-storage-partnership"
                          }
                          alt=""
                          className="symbol-label"
                        />
                      )}
                    </span>
                  </span>
                </span>
                <span className="d-flex flex-column my-2">
                  <span className="text-muted fs-7 fw-semibold">
                    {row.metode_pelatihan}
                  </span>
                  <span
                    className="fw-bolder fs-7"
                    style={{
                      overflow: "hidden",
                      whiteSpace: "wrap",
                      textOverflow: "ellipses",
                    }}
                  >
                    {`${capitalWord(row.pelatihan)} (Batch ${row.batch})`}
                  </span>
                  <h6 className="text-muted fs-7 fw-semibold mb-1">
                    {row.penyelenggara}
                  </h6>
                </span>
              </span>
            </label>
          </a>
        </div>
      ),
    },
    {
      name: "Alur Pendaftaran",
      sortable: true,
      cell: (row) => (
        <div>
          <span
            className="fs-7"
            style={{
              overflow: "hidden",
              whiteSpace: "wrap",
              textOverflow: "ellipses",
            }}
          >
            {capitalWord(row.alur_pendaftaran)}
          </span>
        </div>
      ),
    },
    {
      name: "Lokasi",
      sortable: true,
      width: "200px",
      cell: (row) => (
        <div>
          <span className="d-flex flex-column my-2">
            {row.metode_pelatihan?.toLowerCase()?.includes("offline") ? (
              <span
                className="fs-7"
                style={{
                  overflow: "hidden",
                  whiteSpace: "wrap",
                  textOverflow: "ellipses",
                }}
              >
                {`${capitalWord(row.nm_kab?.toLowerCase())}`}
              </span>
            ) : (
              <span
                className="fs-7"
                style={{
                  overflow: "hidden",
                  whiteSpace: "wrap",
                  textOverflow: "ellipses",
                }}
              >
                {capitalWord(row.metode_pelatihan)}
              </span>
            )}
          </span>
        </div>
      ),
    },
    {
      name: "Jadwal Pendaftaran",
      sortable: true,
      width: "190px",
      selector: (row) => row.pendaftaran_start,
      cell: (row) => (
        <div>
          <span className="fs-7">
            {dateRange(
              row.pendaftaran_start,
              row.pendaftaran_end,
              "DD-MM-YYYY HH:mm:ss",
              "DD MMM YYYY",
            )}
          </span>
          <p className="fs-8 my-0 fw-semibold text-muted">
            {dateRange(
              row.pendaftaran_start,
              row.pendaftaran_end,
              "DD-MM-YYYY HH:mm:ss",
              "HH:mm:ss",
            )}
          </p>
        </div>
      ),
    },
    {
      name: "Jadwal Pelatihan",
      sortable: true,
      width: "190px",
      selector: (row) => row.pelatihan_start,
      cell: (row) => (
        <div>
          <span className="fs-7">
            {dateRange(
              row.pelatihan_start,
              row.pelatihan_end,
              "DD-MM-YYYY HH:mm:ss",
              "DD MMM YYYY",
            )}
          </span>
          <p className="fs-8 my-0 fw-semibold text-muted">
            {dateRange(
              row.pelatihan_start,
              row.pelatihan_end,
              "DD-MM-YYYY HH:mm:ss",
              "HH:mm:ss",
            )}
          </p>
        </div>
      ),
    },
    {
      name: "Status Review",
      sortable: true,
      center: true,
      selector: (row) => row.status_substansi,
      width: "150px",
      cell: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.status_substansi == "Disetujui"
                ? "success"
                : row.status_substansi == "Ditolak"
                  ? "danger"
                  : row.status_substansi == "Revisi"
                    ? "warning"
                    : "primary") +
              " fs-7 m-1"
            }
          >
            {capitalWord(row.status_substansi)}
          </span>
        </div>
      ),
    },
    {
      name: "Status Publish",
      sortable: true,
      selector: (row) => row.status_publish,
      width: "150px",
      cell: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.status_publish == "1"
                ? "success"
                : row.status_publish == "0"
                  ? "danger"
                  : "warning") +
              " fs-7 m-1"
            }
          >
            {row.status_publish == "1"
              ? "Publish"
              : row.status_publish == "0"
                ? "Unpublish"
                : "Unlisted"}
          </span>
        </div>
      ),
    },
    {
      name: "Pelaksanaan",
      sortable: true,
      center: true,
      selector: (row) => row.status_pelatihan,
      width: "150px",
      cell: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.status_pelatihan == "Selesai"
                ? "success"
                : row.status_pelatihan == "Dibatalkan"
                  ? "danger"
                  : row.status_pelatihan == "Pelatihan"
                    ? "warning"
                    : "primary") +
              " fs-7 m-1"
            }
          >
            {capitalWord(row.status_pelatihan)}
          </span>
        </div>
      ),
    },
    {
      name: "Aksi",
      center: false,
      width: "220px",
      cell: (row) => (
        <div>
          <div className="dropdown d-inline">
            <button
              className="btn btn-primary fw-bolder btn-sm dropdown-toggle fs-7 me-1"
              type="button"
              data-bs-toggle="dropdown"
              aria-expanded="false"
            >
              <i className="bi bi-eye-fill text-white"></i>
              <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                Detail
              </span>
            </button>
            <ul
              className="dropdown-menu"
              style={{ position: "absolute", zIndex: "999999" }}
            >
              <li>
                <a
                  href={"/pelatihan/detail-pelatihan/" + row.id}
                  id={row.id}
                  title="View"
                  className="dropdown-item px-5 my-1"
                >
                  <i className="bi bi-eye text-dark text-hover-primary me-1"></i>
                  Detail
                </a>
              </li>
              <li>
                <a
                  href={"/pelatihan/detail-rekappendaftaran/" + row.id}
                  id={row.id}
                  title="Pendaftar"
                  className="dropdown-item px-5 my-1"
                >
                  <i className="bi bi-people text-dark text-hover-primary me-1"></i>
                  Rekap Pendaftar
                </a>
              </li>
              <li>
                <a
                  href={"/pelatihan/edit-pelatihan/" + row.id}
                  id={row.id}
                  title="Edit"
                  className="dropdown-item px-5 my-1"
                >
                  <i className="bi bi-gear text-dark text-hover-primary me-1"></i>
                  Edit
                </a>
              </li>
              <li>
                {row.jml_lms > 0 ? (
                  <>
                    <a
                      href={row.lms_akses}
                      id={row.id}
                      title="LMS"
                      target="_blank"
                      className="dropdown-item px-5 my-1"
                    >
                      <i className="bi bi-book text-dark text-hover-primary me-1"></i>
                      LMS
                    </a>
                  </>
                ) : (
                  <>
                    <span
                      id={row.id}
                      title="LMS"
                      className="dropdown-item px-5 my-1 text-muted"
                    >
                      <i className="bi bi-book text-muted text-hover-primary me-1"></i>
                      LMS
                    </span>
                  </>
                )}
              </li>
            </ul>
          </div>

          {row.status_substansi == "Revisi" && (
            <a
              href="#"
              id={row.id}
              title="Revisi"
              onClick={() => {
                this.setState({
                  selectedRowId: row.id,
                });
              }}
              className="btn btn-sm btn-icon btn-info me-1"
            >
              <i className="fa fa-comment text-white"></i>
            </a>
          )}
          <a
            href="#"
            id={row.id}
            onClick={() => {
              this.handleDelete(row);
            }}
            title="Hapus"
            className="btn btn-sm btn-icon btn-danger me-1"
          >
            <i className="bi bi-trash-fill text-white"></i>
          </a>
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };
  componentDidUpdate() {
    fixBootstrapDropdown();
  }
  componentDidMount() {
    moment.locale("id");
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    const dataPenyelenggara = {
      mulai: 0,
      limit: 9999,
      cari: "",
      sort: "id desc",
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list_satker",
        dataPenyelenggara,
        this.configs,
      )
      .then((res) => {
        const options = res.data.result.Data;
        const dataxpenyelenggara = [];
        if (options.length > 0) {
          dataxpenyelenggara.push({ value: 0, label: "Semua" });
        }
        options.map((data) =>
          dataxpenyelenggara.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxpenyelenggara }, () => {
          // if (isAdminAkademi()) {
          //   this.setState(
          //     {
          //       dataxpenyelenggaravalue: {
          //         value: Cookies.get("satker_id"),
          //         label: Cookies.get("satker_name"),
          //       },
          //     },
          //     () => {
          //       console.log(
          //         "penyelenggara id:",
          //         this.state.dataxpenyelenggaravalue
          //       );
          //     }
          //   );
          // }
        });
      });
    const dataAkademik = { start: 0, length: 1000, status: "publish" };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/akademi/list-akademi",
        dataAkademik,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxakademi = [];
        dataxakademi.push({ value: 0, label: "Semua" });
        optionx.map((data) =>
          dataxakademi.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxakademi });
      });
    const dataProv = { start: 1, rows: 1000 };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/provinsi",
        dataProv,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxprov = [];
        dataxprov.push({ value: 0, label: "Semua" });
        optionx.map((data) =>
          dataxprov.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxprov });
      });
    this.handleReload(this.state.from, this.state.param, 1, 10);
    this.generateYear();
    this.setState({
      url_download:
        process.env.REACT_APP_BASE_API_URI +
        "/report-pelatihan?sta_pub=0&sta_sub=0&sta_pel=0&penyelenggara=0&akademi=0&tema=0&provinsi=0&tahun=" +
        this.state.currentyear,
    });
  }

  exportTableAction() {
    const excel = [];

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    // ambil semua data
    let dataBody = {
      mulai: 0,
      limit: 99999999,
      id_penyelenggara:
        this.state.dataxpenyelenggaravalue.value == null
          ? 0
          : this.state.dataxpenyelenggaravalue.value,
      id_akademi:
        this.state.dataxakademivalue.value == null
          ? 0
          : this.state.dataxakademivalue.value,
      id_tema:
        this.state.dataxtemavalue.value == null
          ? 0
          : this.state.dataxtemavalue.value,
      // status_substansi:
      //   this.state.dataxstatussubstansivalue.value == null
      //     ? 0
      //     : this.state.dataxstatussubstansivalue.value,
      status_substansi: this.state.isFilter
        ? this.state.dataxstatussubstansiheadervalue.value ?? 0
        : this.state.dataxstatussubstansivalue.value == null
          ? 0
          : this.state.dataxstatussubstansivalue.value,
      status_pelatihan: this.state.isFilter
        ? this.state.dataxstatussubstansiheadervalue.value == "Revisi"
          ? "Review Substansi"
          : 0
        : this.state.dataxstatussubstansivalue.value == "Revisi"
          ? "Review Substansi"
          : this.state.dataxstatuspelatihanvalue.value == null
            ? 0
            : this.state.dataxstatuspelatihanvalue.value,
      // status_pelatihan:
      //   this.state.dataxstatuspelatihanvalue.value == null
      //     ? 0
      //     : this.state.dataxstatuspelatihanvalue.value,
      status_publish:
        this.state.dataxstatuspublishvalue.value == null
          ? 99
          : this.state.dataxstatuspublishvalue.value,
      provinsi:
        this.state.dataxprovinsivalue.value == null
          ? 0
          : this.state.dataxprovinsivalue.value,
      param: this.state.param,
      sort:
        this.state.dataxsortvalue == null
          ? this.state.issearch
            ? "pelatihan"
            : "id_pelatihan"
          : this.state.dataxsortvalue,
      sortval:
        this.state.dataxsortvalvalue == null
          ? this.state.issearch
            ? "ASC"
            : "DESC"
          : this.state.dataxsortvalvalue.toUpperCase(),
      id_silabus: 0,
    };
    if (this.state.dataxtahunvalue.value == null) {
      dataBody.tahun = this.state.currentyear;
    } else {
      dataBody.tahun = this.state.dataxtahunvalue.value;
    }

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/pelatihan",
        dataBody,
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          console.log(res.data.result.Data);
          const datax = res.data.result.Data;
          datax?.forEach((element, i) => {
            const keys = Object.keys(element);
            const rows = {};
            keys.forEach((key) => {
              rows["No"] = i + 1;
              rows["Id Pelatihan"] = element.slug_pelatian_id;
              rows["Pelatihan"] = element.pelatihan;
              rows["Penyelenggara"] = element.penyelenggara;
              rows["Metode Pelatihan"] = element.metode_pelatihan;
              rows["Pendaftaran Mulai"] = handleFormatDate(
                element.pendaftaran_start,
                "DD MMMM YYYY HH:mm:ss",
              );
              rows["Pendaftaran Selesai"] = handleFormatDate(
                element.pendaftaran_end,
                "DD MMMM YYYY HH:mm:ss",
              );
              rows["Pelatihan Mulai"] = handleFormatDate(
                element.pelatihan_start,
                "DD MMMM YYYY HH:mm:ss",
              );
              rows["Pelatihan Selesai"] = handleFormatDate(
                element.pelatihan_end,
                "DD MMMM YYYY HH:mm:ss",
              );
              rows["Status Publish"] =
                element.status_publish == 0 ? "Unpublish" : "Publish";
              rows["Status Substansi"] = element.status_substansi;
              rows["Status Pelatihan"] = element.status_pelatihan;
              rows["Created By"] = element.created_by;
              rows["Created at"] = handleFormatDate(
                element.created_at,
                "DD MMMM YYYY HH:mm:ss",
              );
              rows["Updated By"] = element.updated_by;
              rows["Updated at"] = handleFormatDate(
                element.updated_at,
                "DD MMMM YYYY HH:mm:ss",
              );
            });
            excel.push(rows);
          });
          let tanggal = Date().toString();
          let split_tanggal = tanggal.split(" ");
          let text_tanggal =
            split_tanggal[1] +
            "_" +
            split_tanggal[2] +
            "_" +
            split_tanggal[3] +
            "_" +
            split_tanggal[4];
          exportToCSV(excel, "List Pelatihan " + text_tanggal);
        } else {
          swal.fire({
            title: messagex,
            icon: "warning",
            confirmButtonText: "Ok",
          });
        }
        swal.close();
      })
      .catch((error) => {
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      });
  }

  handleReload(filter = false, param, page, newPerPage) {
    this.setState({ loading: true, isFilter: filter });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - length_tmp;
    }
    let dataBody = {
      mulai: start_tmp,
      limit: length_tmp,
      id_penyelenggara:
        this.state.dataxpenyelenggaravalue.value == null
          ? 0
          : this.state.dataxpenyelenggaravalue.value,
      id_akademi:
        this.state.dataxakademivalue.value == null
          ? 0
          : this.state.dataxakademivalue.value,
      id_tema:
        this.state.dataxtemavalue.value == null
          ? 0
          : this.state.dataxtemavalue.value,
      status_substansi: filter
        ? this.state.dataxstatussubstansiheadervalue.value ?? 0
        : this.state.dataxstatuspelatihanheadervalue == "Pelatihan" ||
            this.state.dataxstatuspelatihanvalue == "Pelatihan"
          ? "Disetujui"
          : this.state.dataxstatussubstansivalue.value == null
            ? 0
            : this.state.dataxstatussubstansivalue.value,
      status_pelatihan: filter
        ? this.state.dataxstatuspelatihanheadervalue.value == "Pelatihan"
          ? this.state.dataxstatuspelatihanheadervalue.value
          : this.state.dataxstatussubstansiheadervalue.value == "Revisi"
            ? "Review Substansi"
            : 0
        : this.state.dataxstatussubstansivalue.value == "Revisi"
          ? "Review Substansi"
          : this.state.dataxstatuspelatihanvalue.value == null
            ? 0
            : this.state.dataxstatuspelatihanvalue.value,
      status_publish:
        this.state.dataxstatuspublishvalue.value == null
          ? 99
          : this.state.dataxstatuspublishvalue.value,
      provinsi:
        this.state.dataxprovinsivalue.value == null
          ? 0
          : this.state.dataxprovinsivalue.value,
      param: param,
      sort:
        this.state.dataxsortvalue == null
          ? this.state.issearch
            ? "pelatihan"
            : "id_pelatihan"
          : this.state.dataxsortvalue,
      sortval:
        this.state.dataxsortvalvalue == null
          ? this.state.issearch
            ? "ASC"
            : "DESC"
          : this.state.dataxsortvalvalue.toUpperCase(),
      id_silabus: 0,
    };
    if (this.state.dataxtahunvalue.value == null) {
      dataBody.tahun = this.state.currentyear;
    } else {
      dataBody.tahun = this.state.dataxtahunvalue.value;
    }

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/pelatihan",
        dataBody,
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          this.setState({ numberrow: start_tmp + 1 });
          const datax = res.data.result.Data;
          this.setState({
            datax: datax,
            totalRows: res.data.result.TotalData,
            review: res.data.result.TotalDataReview,
            revisi: res.data.result.TotalDataRevisi,
            disetujui: res.data.result.TotalDataDisetujui,
            berjalan: res.data.result.TotalDataBerjalan,
            selesai: res.data.result.TotalDataSelesai,
            ditolak: res.data.result.TotalDataDitolak,
            dibatalkan: res.data.result.TotalDataBatal,
            totalDataAll: res.data.result.TotalDataAll,
          });
          this.setState({ loading: false });
          this.setState({});
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }

        if (!this.state.clickbox) {
          dataBody.mulai = 0;
          dataBody.limit = 10;
        }
      })
      .catch((error) => {
        console.log(error);
        let statux = error.response?.data?.result?.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      });
  }
  handleDelete(row) {
    const idx = row.id;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          let data = {
            id: idx,
          };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/delpelatihan",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (true == statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload(false, this.state.param, 1, 10);
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload(false, this.state.param, 1, 10);
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            });
        }
      });
  }
  doSearch(searchText) {
    this.setState({ param: searchText }, () => {
      if (searchText == "") {
        this.handleReload(false, this.state.param, 1, 10);
      } else {
        this.handleReload(false, searchText, 1, 10);
      }
    });
  }
  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    this.setState({ param: searchText });
    this.setState({ numberrow: 1 });
    this.setState({ issearch: true });
    if (e.key === "Enter") {
      this.setState({ loading: true }, () => {
        this.doSearch(searchText);
      });
    }
  }
  handleClickSearchAction(e) {
    e.preventDefault();
    this.setState({ loading: true });
    this.handleReload(false, this.state.param, 1, 10);
  }
  handleClickReviewAction(e) {
    e.preventDefault();
    this.setState(
      {
        dataxstatussubstansiheadervalue: {
          value: "Review",
          name: "Review",
        },
      },
      () => {
        this.handleReload(true, "", 1, this.state.newPerPage);
      },
    );
  }
  handleClickRevisiAction(e) {
    e.preventDefault();
    this.setState(
      {
        dataxstatussubstansiheadervalue: {
          value: "Revisi",
          name: "Revisi",
        },
      },
      () => {
        this.handleReload(true, "", 1, this.state.newPerPage);
      },
    );
  }
  handleClickDisetujuiAction(e) {
    e.preventDefault();
    this.setState(
      {
        dataxstatussubstansiheadervalue: {
          value: "Disetujui",
          name: "Disetujui",
        },
      },
      () => {
        this.handleReload(true, "", 1, this.state.newPerPage);
      },
    );
  }
  handleClickBerjalanAction(e) {
    e.preventDefault();
    this.setState(
      {
        dataxstatussubstansiheadervalue: {
          value: "Disetujui",
          name: "Disetujui",
        },
        dataxstatuspelatihanheadervalue: {
          value: "Pelatihan",
          name: "Pelatihan",
        },
      },
      () => {
        this.handleReload(true, "", 1, this.state.newPerPage);
      },
    );
  }
  handleClickSelesaiAction(e) {
    e.preventDefault();
    this.setState(
      {
        dataxstatussubstansiheadervalue: {
          value: 0,
          name: 0,
        },
        dataxstatuspelatihanvalue: {
          value: "Selesai",
          name: "Selesai",
        },
      },
      () => {
        this.handleReload(true, "", 1, this.state.newPerPage);
      },
    );
  }
  handleClickDitolakAction(e) {
    e.preventDefault();
    this.setState(
      {
        dataxstatussubstansiheadervalue: {
          value: 0,
          name: 0,
        },
        dataxstatuspelatihanvalue: {
          value: "Ditolak",
          name: "Ditolak",
        },
      },
      () => {
        this.handleReload(true, "", 1, this.state.newPerPage);
      },
    );
  }
  handleClickDibatalkanAction(e) {
    e.preventDefault();
    this.setState(
      {
        dataxstatussubstansiheadervalue: {
          value: 0,
          name: 0,
        },
        dataxstatuspelatihanvalue: {
          value: "Dibatalkan",
          name: "Dibatalkan",
        },
      },
      () => {
        this.handleReload(true, "", 1, this.state.newPerPage);
      },
    );
  }
  // handleClickExportAction() {
  //   axios.get(
  //     process.env.REACT_APP_BASE_API_URI + "/report-pelatihan",
  //     this.configs
  //   );
  // }
  handleChangeAkademiAction = (akademi_id) => {
    let dataxakademivalue = akademi_id;
    this.setState({ dataxakademivalue });
    const dataBody = { start: 1, rows: 100, id: akademi_id.value };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/cari_tema_byakademi",
        dataBody,
        this.configs,
      )
      .then((res) => {
        this.setState({ isDisabled: false });
        const optionx = res.data.result.Data;
        const dataxtema = [
          {
            value: 0,
            label: "Semua",
          },
        ];
        optionx.map((data) =>
          dataxtema.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxtema });
      })
      .catch((error) => {
        const dataxtema = [];
        this.setState({ dataxtema });
        let messagex = error.response.data.result.Message;
        console.log(error);
        // swal
        //   .fire({
        //     title: messagex,
        //     icon: "warning",
        //     confirmButtonText: "Ok",
        //   })
        //   .then((result) => {
        //     if (result.isConfirmed) {
        //     }
        //   });
      });
  };
  handleChangePenyelenggaraAction = (penyelenggara_id) => {
    let dataxpenyelenggaravalue = penyelenggara_id;
    this.setState({ dataxpenyelenggaravalue });
  };
  handleChangeTemaAction = (tema_id) => {
    let dataxtemavalue = tema_id;
    this.setState({ dataxtemavalue });
  };
  handleChangeStatusSubstansiAction = (status_substansi) => {
    this.setState({
      dataxstatussubstansivalue: status_substansi,
    });
  };
  handleChangeStatusPelatihanAction = (status_pelatihan) => {
    // let dataxstatuspelatihanvalue = status_pelatihan_id;
    this.setState({
      dataxstatuspelatihanvalue: status_pelatihan,
    });
  };
  handleChangeStatusPublishAction = (status_publish_id) => {
    let dataxstatuspublishvalue = status_publish_id;
    this.setState({
      dataxstatuspublishvalue,
    });
  };
  handleChangeProvinsiAction = (provinsi_id) => {
    let dataxprovinsivalue = provinsi_id;
    this.setState({ dataxprovinsivalue });
  };
  handleChangeYearAction = (tahun_id) => {
    let dataxtahunvalue = tahun_id;
    this.setState({ dataxtahunvalue });
  };
  handleClickResetAction(e) {
    e.preventDefault();
    this.setState(
      {
        numberrow: 1,
        dataxpenyelenggaravalue: "",
        dataxakademivalue: "",
        dataxtemavalue: "",
        dataxstatussubstansivalue: "",
        dataxstatussubstansiheadervalue: "",
        dataxstatuspelatihanvalue: "",
        dataxstatuspublishvalue: "",
        dataxprovinsivalue: "",
        dataxtahunvalue: "",
      },
      () => {
        this.setState({
          url_download:
            process.env.REACT_APP_BASE_API_BE +
            "/report-pelatihan?sta_pub=0&sta_sub=0&sta_pel=0&penyelenggara=0&akademi=0&tema=0&provinsi=0&tahun=" +
            this.state.currentyear,
        });
        this.handleReload(false, this.state.param, 1, 10);
      },
    );
  }
  handleChangeSort = (column, sortDirection) => {
    this.setState({ numberrow: 1 });
    let sortByColumn = "";
    if (column.id == 2) {
      sortByColumn = "slug_pelatian_id";
    } else if (column.id == 3) {
      sortByColumn = "pelatihan";
    } else if (column.id == 4) {
      sortByColumn = "alur_pendaftaran";
    } else if (column.id == 5) {
      sortByColumn = "nm_kab";
    } else if (column.id == 6) {
      sortByColumn = "pendaftaran_start";
    } else if (column.id == 7) {
      sortByColumn = "pelatihan_start";
    } else if (column.id == 8) {
      sortByColumn = "status_substansi";
    } else if (column.id == 9) {
      sortByColumn = "status_publish";
    } else if (column.id == 10) {
      sortByColumn = "status_pelatihan";
    }
    // else if (column.id == 9) {
    //   sortByColumn = "created_by";
    // } else if (column.id == 10) {
    //   sortByColumn = "updated_by";
    // }
    this.setState({ dataxsortvalue: sortByColumn });
    this.setState({ dataxsortvalvalue: sortDirection }, () => {
      this.handleReload(
        this.state.from,
        this.state.param,
        1,
        this.state.newPerPage,
      );
    });
  };
  handlePerRowsChange = async (arg1, arg2, srcEvent) => {
    if (srcEvent == "page-change") {
      this.setState({ loading: true }, () => {
        if (!this.state.isRowChangeRef) {
          this.handleReload(
            false,
            this.state.param,
            arg1,
            this.state.newPerPage,
          );
        }
      });
    } else if (srcEvent == "row-change") {
      this.setState({ isRowChangeRef: true }, () => {
        this.handleReload(false, this.state.param, arg2, arg1);
      });
      this.setState({ loading: true, newPerPage: arg1 }, () => {
        this.setState({ isRowChangeRef: false });
      });
    }
  };
  handleChangeSearchAction(e) {
    const searchText = e.currentTarget.value;
    this.setState({ issearch: false });
    this.setState({ param: searchText });
    if (searchText == "") {
      this.setState({ loading: true }, () => {
        this.handleReload("home", "", 1, 10);
      });
    }
  }
  render() {
    return (
      <div>
        <ViewCatatan
          pelatihanId={this.state.selectedRowId}
          configs={this.configs}
          onClose={() => {
            this.setState({
              selectedRowId: null,
            });
          }}
        />

        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  List Pelatihan
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                  data-bs-toggle="modal"
                  data-bs-target="#filter"
                >
                  <i className="bi bi-sliders"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Filter
                  </span>
                </button>
                <button
                  className="btn btn-success fw-bolder btn-sm"
                  data-kt-menu-trigger="click"
                  data-kt-menu-placement="bottom-end"
                  data-kt-menu-flip="top-end"
                >
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kelola Pelatihan
                  </span>
                  <i className="bi bi-chevron-down ms-1"></i>
                </button>
                <div
                  className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-7 w-auto "
                  data-kt-menu="true"
                >
                  <div>
                    <div className="menu-item">
                      <a
                        href="/pelatihan/tambah-pelatihan"
                        className="menu-link px-5"
                      >
                        <i className="bi bi-plus-circle text-dark text-hover-primary me-1"></i>
                        Tambah Pelatihan
                      </a>
                    </div>
                    <div className="menu-item">
                      <a
                        href="#"
                        onClick={() => {
                          this.exportTableAction();
                        }}
                        className="menu-link px-5"
                      >
                        <i className="bi bi-cloud-upload text-dark text-hover-primary me-1"></i>
                        Export Rekap Pelatihan
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-7"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 col-md-12 col-sm-12">
                    <div className="row">
                      {/*<div className="col-xl-3 mb-3">
                        <a
                          href="#"
                          onClick={this.handleAllRow}
                          className="card hoverable"
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-primary mb-3 mt-1">
                                <span class="svg-icon svg-icon-primary svg-icon-3x">
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24px"
                                    height="24px"
                                    viewBox="0 0 24 24"
                                    version="1.1"
                                  >
                                    <g
                                      stroke="none"
                                      stroke-width="1"
                                      fill="none"
                                      fill-rule="evenodd"
                                    >
                                      <rect
                                        x="0"
                                        y="0"
                                        width="24"
                                        height="24"
                                      />
                                      <path
                                        d="M3.5,21 L20.5,21 C21.3284271,21 22,20.3284271 22,19.5 L22,8.5 C22,7.67157288 21.3284271,7 20.5,7 L10,7 L7.43933983,4.43933983 C7.15803526,4.15803526 6.77650439,4 6.37867966,4 L3.5,4 C2.67157288,4 2,4.67157288 2,5.5 L2,19.5 C2,20.3284271 2.67157288,21 3.5,21 Z"
                                        fill="#000000"
                                        opacity="0.3"
                                      />
                                      <path
                                        d="M10.875,16.75 C10.6354167,16.75 10.3958333,16.6541667 10.2041667,16.4625 L8.2875,14.5458333 C7.90416667,14.1625 7.90416667,13.5875 8.2875,13.2041667 C8.67083333,12.8208333 9.29375,12.8208333 9.62916667,13.2041667 L10.875,14.45 L14.0375,11.2875 C14.4208333,10.9041667 14.9958333,10.9041667 15.3791667,11.2875 C15.7625,11.6708333 15.7625,12.2458333 15.3791667,12.6291667 L11.5458333,16.4625 C11.3541667,16.6541667 11.1145833,16.75 10.875,16.75 Z"
                                        fill="#000000"
                                      />
                                    </g>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.totalDataAll}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Total Pelatihan
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>*/}
                      <div className="col-6 col-lg-3 mb-3">
                        <a
                          href="#"
                          onClick={this.handleClickReview}
                          className="card hoverable"
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-primary mb-3 mt-1">
                                <span className="svg-icon svg-icon-primary svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <path
                                      opacity="0.3"
                                      d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM12.5 18C12.5 17.4 12.6 17.5 12 17.5H8.5C7.9 17.5 8 17.4 8 18C8 18.6 7.9 18.5 8.5 18.5L12 18C12.6 18 12.5 18.6 12.5 18ZM16.5 13C16.5 12.4 16.6 12.5 16 12.5H8.5C7.9 12.5 8 12.4 8 13C8 13.6 7.9 13.5 8.5 13.5H15.5C16.1 13.5 16.5 13.6 16.5 13ZM12.5 8C12.5 7.4 12.6 7.5 12 7.5H8C7.4 7.5 7.5 7.4 7.5 8C7.5 8.6 7.4 8.5 8 8.5H12C12.6 8.5 12.5 8.6 12.5 8Z"
                                      fill="currentColor"
                                    ></path>
                                    <rect
                                      x="7"
                                      y="17"
                                      width="6"
                                      height="2"
                                      rx="1"
                                      fill="currentColor"
                                    ></rect>
                                    <rect
                                      x="7"
                                      y="12"
                                      width="10"
                                      height="2"
                                      rx="1"
                                      fill="currentColor"
                                    ></rect>
                                    <rect
                                      x="7"
                                      y="7"
                                      width="6"
                                      height="2"
                                      rx="1"
                                      fill="currentColor"
                                    ></rect>
                                    <path
                                      d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z"
                                      fill="currentColor"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.review}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Review
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-6 col-lg-3 mb-3">
                        <a
                          href="#"
                          onClick={this.handleClickRevisi}
                          className="card hoverable"
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-warning mb-3 mt-1">
                                <span className="svg-icon svg-icon-warning svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <path
                                      opacity="0.3"
                                      d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                                      fill="#ffc700"
                                    ></path>
                                    <path
                                      d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                                      fill="#ffc700"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.revisi}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Revisi
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-6 col-lg-3 mb-3">
                        <a
                          href="#"
                          onClick={this.handleClickDisetujui}
                          className="card hoverable"
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-success mb-3 mt-1">
                                <span className="svg-icon svg-icon-success svg-icon-3x">
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24px"
                                    height="24px"
                                    viewBox="0 0 24 24"
                                    className="mh-50px"
                                  >
                                    <path
                                      d="M10.0813 3.7242C10.8849 2.16438 13.1151 2.16438 13.9187 3.7242V3.7242C14.4016 4.66147 15.4909 5.1127 16.4951 4.79139V4.79139C18.1663 4.25668 19.7433 5.83365 19.2086 7.50485V7.50485C18.8873 8.50905 19.3385 9.59842 20.2758 10.0813V10.0813C21.8356 10.8849 21.8356 13.1151 20.2758 13.9187V13.9187C19.3385 14.4016 18.8873 15.491 19.2086 16.4951V16.4951C19.7433 18.1663 18.1663 19.7433 16.4951 19.2086V19.2086C15.491 18.8873 14.4016 19.3385 13.9187 20.2758V20.2758C13.1151 21.8356 10.8849 21.8356 10.0813 20.2758V20.2758C9.59842 19.3385 8.50905 18.8873 7.50485 19.2086V19.2086C5.83365 19.7433 4.25668 18.1663 4.79139 16.4951V16.4951C5.1127 15.491 4.66147 14.4016 3.7242 13.9187V13.9187C2.16438 13.1151 2.16438 10.8849 3.7242 10.0813V10.0813C4.66147 9.59842 5.1127 8.50905 4.79139 7.50485V7.50485C4.25668 5.83365 5.83365 4.25668 7.50485 4.79139V4.79139C8.50905 5.1127 9.59842 4.66147 10.0813 3.7242V3.7242Z"
                                      fill="#50cd89"
                                    ></path>
                                    <path
                                      className="permanent"
                                      d="M14.8563 9.1903C15.0606 8.94984 15.3771 8.9385 15.6175 9.14289C15.858 9.34728 15.8229 9.66433 15.6185 9.9048L11.863 14.6558C11.6554 14.9001 11.2876 14.9258 11.048 14.7128L8.47656 12.4271C8.24068 12.2174 8.21944 11.8563 8.42911 11.6204C8.63877 11.3845 8.99996 11.3633 9.23583 11.5729L11.3706 13.4705L14.8563 9.1903Z"
                                      fill="white"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.disetujui}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Disetujui
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-6 col-lg-3 mb-3">
                        <a
                          href="#"
                          onClick={this.handleClickDitolak}
                          className="card hoverable"
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-danger mb-3 mt-1">
                                <span className="svg-icon svg-icon-danger svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="#dc3545"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <rect
                                      opacity="0.3"
                                      x="2"
                                      y="2"
                                      width="20"
                                      height="20"
                                      rx="10"
                                      fill="#f1416c"
                                    ></rect>
                                    <rect
                                      x="7"
                                      y="15.3137"
                                      width="12"
                                      height="2"
                                      rx="1"
                                      transform="rotate(-45 7 15.3137)"
                                      fill="#dc3545"
                                    ></rect>
                                    <rect
                                      x="8.41422"
                                      y="7"
                                      width="12"
                                      height="2"
                                      rx="1"
                                      transform="rotate(45 8.41422 7)"
                                      fill="#dc3545"
                                    ></rect>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.ditolak}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Ditolak
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                      {/*<div className="col-xl-3 mb-3">
                        <a
                          href="#"
                          onClick={this.handleClickBerjalan}
                          className="card hoverable"
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-success mb-3 mt-1">
                                <span className="svg-icon svg-icon-success svg-icon-3x">
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24px"
                                    height="24px"
                                    viewBox="0 0 24 24"
                                    className="mh-50px"
                                  >
                                    <path
                                      d="M10.0813 3.7242C10.8849 2.16438 13.1151 2.16438 13.9187 3.7242V3.7242C14.4016 4.66147 15.4909 5.1127 16.4951 4.79139V4.79139C18.1663 4.25668 19.7433 5.83365 19.2086 7.50485V7.50485C18.8873 8.50905 19.3385 9.59842 20.2758 10.0813V10.0813C21.8356 10.8849 21.8356 13.1151 20.2758 13.9187V13.9187C19.3385 14.4016 18.8873 15.491 19.2086 16.4951V16.4951C19.7433 18.1663 18.1663 19.7433 16.4951 19.2086V19.2086C15.491 18.8873 14.4016 19.3385 13.9187 20.2758V20.2758C13.1151 21.8356 10.8849 21.8356 10.0813 20.2758V20.2758C9.59842 19.3385 8.50905 18.8873 7.50485 19.2086V19.2086C5.83365 19.7433 4.25668 18.1663 4.79139 16.4951V16.4951C5.1127 15.491 4.66147 14.4016 3.7242 13.9187V13.9187C2.16438 13.1151 2.16438 10.8849 3.7242 10.0813V10.0813C4.66147 9.59842 5.1127 8.50905 4.79139 7.50485V7.50485C4.25668 5.83365 5.83365 4.25668 7.50485 4.79139V4.79139C8.50905 5.1127 9.59842 4.66147 10.0813 3.7242V3.7242Z"
                                      fill="#50cd89"
                                    ></path>
                                    <path
                                      className="permanent"
                                      d="M14.8563 9.1903C15.0606 8.94984 15.3771 8.9385 15.6175 9.14289C15.858 9.34728 15.8229 9.66433 15.6185 9.9048L11.863 14.6558C11.6554 14.9001 11.2876 14.9258 11.048 14.7128L8.47656 12.4271C8.24068 12.2174 8.21944 11.8563 8.42911 11.6204C8.63877 11.3845 8.99996 11.3633 9.23583 11.5729L11.3706 13.4705L14.8563 9.1903Z"
                                      fill="white"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.berjalan}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Berjalan
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>*/}
                      {/*<div className="col-12 rounded-xl mb-7 ml-md-3 col-md">
                        <a
                          href="#"
                          onClick={this.handleClickSelesai}
                          className="card hoverable"
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-success mb-3 mt-1">
                                <span className="svg-icon svg-icon-success svg-icon-3x">
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24px"
                                    height="24px"
                                    viewBox="0 0 24 24"
                                    className="mh-50px"
                                  >
                                    <path
                                      d="M10.0813 3.7242C10.8849 2.16438 13.1151 2.16438 13.9187 3.7242V3.7242C14.4016 4.66147 15.4909 5.1127 16.4951 4.79139V4.79139C18.1663 4.25668 19.7433 5.83365 19.2086 7.50485V7.50485C18.8873 8.50905 19.3385 9.59842 20.2758 10.0813V10.0813C21.8356 10.8849 21.8356 13.1151 20.2758 13.9187V13.9187C19.3385 14.4016 18.8873 15.491 19.2086 16.4951V16.4951C19.7433 18.1663 18.1663 19.7433 16.4951 19.2086V19.2086C15.491 18.8873 14.4016 19.3385 13.9187 20.2758V20.2758C13.1151 21.8356 10.8849 21.8356 10.0813 20.2758V20.2758C9.59842 19.3385 8.50905 18.8873 7.50485 19.2086V19.2086C5.83365 19.7433 4.25668 18.1663 4.79139 16.4951V16.4951C5.1127 15.491 4.66147 14.4016 3.7242 13.9187V13.9187C2.16438 13.1151 2.16438 10.8849 3.7242 10.0813V10.0813C4.66147 9.59842 5.1127 8.50905 4.79139 7.50485V7.50485C4.25668 5.83365 5.83365 4.25668 7.50485 4.79139V4.79139C8.50905 5.1127 9.59842 4.66147 10.0813 3.7242V3.7242Z"
                                      fill="#50cd89"
                                    ></path>
                                    <path
                                      className="permanent"
                                      d="M14.8563 9.1903C15.0606 8.94984 15.3771 8.9385 15.6175 9.14289C15.858 9.34728 15.8229 9.66433 15.6185 9.9048L11.863 14.6558C11.6554 14.9001 11.2876 14.9258 11.048 14.7128L8.47656 12.4271C8.24068 12.2174 8.21944 11.8563 8.42911 11.6204C8.63877 11.3845 8.99996 11.3633 9.23583 11.5729L11.3706 13.4705L14.8563 9.1903Z"
                                      fill="white"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.selesai}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Selesai
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>*/}
                      {/* <div className="col-12 rounded-xl mb-7 ml-md-3 col-md">
                        <a href="#" onClick={this.handleClickReview} className="card bg-warning hoverable">
                          <div className="card-body">
                            <span className="svg-icon svg-icon-white svg-icon-3x ms-n1">
                              <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className="mh-50px"><path opacity="0.3" d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z" fill="currentColor"></path>
                                <path d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z" fill="currentColor"></path>
                              </svg>
                            </span>
                            <div className="text-inverse-warning fw-bolder fs-2 mb-2 mt-5">{this.state.review}</div>
                            <div className="fw-bold text-inverse-warning fs-7">Review</div>
                          </div>
                        </a>
                      </div> */}
                      {/* <div className="col-12 rounded-xl mb-7 ml-md-3 col-md">
                        <a href="#" onClick={this.handleClickRevisi} className="card bg-danger hoverable">
                          <div className="card-body">
                            <span className="svg-icon svg-icon-white svg-icon-3x ms-n1">
                              <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className="mh-50px">
                                <rect x="8" y="9" width="3" height="10" rx="1.5" fill="currentColor"></rect><rect opacity="0.5" x="13" y="5" width="3" height="14" rx="1.5" fill="currentColor">
                                </rect>
                                <rect x="18" y="11" width="3" height="8" rx="1.5" fill="currentColor">
                                </rect>
                                <rect x="3" y="13" width="3" height="6" rx="1.5" fill="currentColor">
                                </rect>
                              </svg>
                            </span>
                            <div className="text-inverse-dark fw-bolder fs-2 mb-2 mt-5">{this.state.revisi}</div>
                            <div className="fw-bold text-inverse-dark fs-7">Revisi</div>
                          </div>
                        </a>
                      </div> */}
                      {/* <div className="col-12 rounded-xl mb-7 ml-md-3 col-md">
                        <a href="#" onClick={this.handleClickDisetujui} className="card bg-success hoverable">
                          <div className="card-body">
                            <span className="svg-icon svg-icon-white svg-icon-3x ms-n1">
                              <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className="mh-50px">
                                <path opacity="0.3" d="M18 21.6C16.3 21.6 15 20.3 15 18.6V2.50001C15 2.20001 14.6 1.99996 14.3 2.19996L13 3.59999L11.7 2.3C11.3 1.9 10.7 1.9 10.3 2.3L9 3.59999L7.70001 2.3C7.30001 1.9 6.69999 1.9 6.29999 2.3L5 3.59999L3.70001 2.3C3.50001 2.1 3 2.20001 3 3.50001V18.6C3 20.3 4.3 21.6 6 21.6H18Z" fill="currentColor">
                                </path>
                                <path d="M12 12.6H11C10.4 12.6 10 12.2 10 11.6C10 11 10.4 10.6 11 10.6H12C12.6 10.6 13 11 13 11.6C13 12.2 12.6 12.6 12 12.6ZM9 11.6C9 11 8.6 10.6 8 10.6H6C5.4 10.6 5 11 5 11.6C5 12.2 5.4 12.6 6 12.6H8C8.6 12.6 9 12.2 9 11.6ZM9 7.59998C9 6.99998 8.6 6.59998 8 6.59998H6C5.4 6.59998 5 6.99998 5 7.59998C5 8.19998 5.4 8.59998 6 8.59998H8C8.6 8.59998 9 8.19998 9 7.59998ZM13 7.59998C13 6.99998 12.6 6.59998 12 6.59998H11C10.4 6.59998 10 6.99998 10 7.59998C10 8.19998 10.4 8.59998 11 8.59998H12C12.6 8.59998 13 8.19998 13 7.59998ZM13 15.6C13 15 12.6 14.6 12 14.6H10C9.4 14.6 9 15 9 15.6C9 16.2 9.4 16.6 10 16.6H12C12.6 16.6 13 16.2 13 15.6Z" fill="currentColor"></path>
                                <path d="M15 18.6C15 20.3 16.3 21.6 18 21.6C19.7 21.6 21 20.3 21 18.6V12.5C21 12.2 20.6 12 20.3 12.2L19 13.6L17.7 12.3C17.3 11.9 16.7 11.9 16.3 12.3L15 13.6V18.6Z" fill="currentColor">
                                </path>
                              </svg>
                            </span>
                            <div className="text-inverse-dark fw-bolder fs-2 mb-2 mt-5">{this.state.disetujui}</div>
                            <div className="fw-bold text-inverse-dark fs-7">Disetujui</div>
                          </div>
                        </a>
                      </div> */}
                      {/* <div className="col-12 rounded-xl mb-7 ml-md-3 col-md">
                        <a href="#" onClick={this.handleClickBerjalan} className="card bg-info hoverable">
                          <div className="card-body">
                            <span className="svg-icon svg-icon-white svg-icon-3x ms-n1">
                              <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className="mh-50px">
                                <path opacity="0.3" d="M10.9607 12.9128H18.8607C19.4607 12.9128 19.9607 13.4128 19.8607 14.0128C19.2607 19.0128 14.4607 22.7128 9.26068 21.7128C5.66068 21.0128 2.86071 18.2128 2.16071 14.6128C1.16071 9.31284 4.96069 4.61281 9.86069 4.01281C10.4607 3.91281 10.9607 4.41281 10.9607 5.01281V12.9128Z" fill="currentColor"></path>
                                <path d="M12.9607 10.9128V3.01281C12.9607 2.41281 13.4607 1.91281 14.0607 2.01281C16.0607 2.21281 17.8607 3.11284 19.2607 4.61284C20.6607 6.01284 21.5607 7.91285 21.8607 9.81285C21.9607 10.4129 21.4607 10.9128 20.8607 10.9128H12.9607Z" fill="currentColor"></path>
                              </svg>
                            </span>
                            <div className="text-inverse-info fw-bolder fs-2 mb-2 mt-5">{this.state.berjalan}</div>
                            <div className="fw-bold text-inverse-info fs-7">Berjalan</div>
                          </div>
                        </a>
                      </div> */}
                      {/* <div className="col-12 rounded-xl mb-7 ml-md-3 col-md">
                        <a href="#" onClick={this.handleClickSelesai} className="card bg-primary hoverable">
                          <div className="card-body">
                            <span className="svg-icon svg-icon-white svg-icon-3x ms-n1">
                              <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className="mh-50px">
                                <path opacity="0.3" d="M14 12V21H10V12C10 11.4 10.4 11 11 11H13C13.6 11 14 11.4 14 12ZM7 2H5C4.4 2 4 2.4 4 3V21H8V3C8 2.4 7.6 2 7 2Z" fill="currentColor"></path>
                                <path d="M21 20H20V16C20 15.4 19.6 15 19 15H17C16.4 15 16 15.4 16 16V20H3C2.4 20 2 20.4 2 21C2 21.6 2.4 22 3 22H21C21.6 22 22 21.6 22 21C22 20.4 21.6 20 21 20Z" fill="currentColor"></path>
                              </svg>
                            </span>
                            <div className="text-inverse-info fw-bolder fs-2 mb-2 mt-5">{this.state.selesai}</div>
                            <div className="fw-bold text-inverse-info fs-7">Selesai</div>
                          </div>
                        </a>
                      </div> */}
                      {/*<div className="col-12 rounded-xl mb-7 ml-md-3 col-md">
                        <a
                          href="#"
                          onClick={this.handleClickDibatalkan}
                          className="card hoverable"
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-danger mb-3 mt-1">
                                <span className="svg-icon svg-icon-danger svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="#dc3545"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <rect
                                      opacity="0.3"
                                      x="2"
                                      y="2"
                                      width="20"
                                      height="20"
                                      rx="10"
                                      fill="#f1416c"
                                    ></rect>
                                    <rect
                                      x="7"
                                      y="15.3137"
                                      width="12"
                                      height="2"
                                      rx="1"
                                      transform="rotate(-45 7 15.3137)"
                                      fill="#dc3545"
                                    ></rect>
                                    <rect
                                      x="8.41422"
                                      y="7"
                                      width="12"
                                      height="2"
                                      rx="1"
                                      transform="rotate(45 8.41422 7)"
                                      fill="#dc3545"
                                    ></rect>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.dibatalkan}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Dibatalkan
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>*/}
                    </div>
                  </div>
                  <div className="col-lg-12 pt-2">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            List Rekap Pelatihan
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <div className="d-flex align-items-center position-relative my-1 me-2">
                            <span className="svg-icon svg-icon-1 position-absolute ms-6">
                              <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className="mh-50px"
                              >
                                <rect
                                  opacity="0.5"
                                  x="17.0365"
                                  y="15.1223"
                                  width="8.15546"
                                  height="2"
                                  rx="1"
                                  transform="rotate(45 17.0365 15.1223)"
                                  fill="currentColor"
                                ></rect>
                                <path
                                  d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                  fill="currentColor"
                                ></path>
                              </svg>
                            </span>
                            <input
                              type="text"
                              data-kt-user-table-filter="search"
                              className="form-control form-control-sm form-control-solid w-250px ps-14"
                              placeholder="Cari Pelatihan"
                              onKeyPress={this.handleKeyPress}
                              onChange={this.handleChangeSearch}
                            />
                          </div>
                          {/* <button className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2" data-bs-toggle="modal" data-bs-target="#filter">
                            <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z" fill="currentColor" />
                              </svg>
                            </span>
                            Filter
                          </button> */}
                          <div className="modal fade" tabindex="-1" id="filter">
                            <div className="modal-dialog modal-lg">
                              <div className="modal-content">
                                <div className="modal-header">
                                  <h5 className="modal-title">
                                    <span className="svg-icon svg-icon-5 me-1">
                                      <i className="bi bi-sliders text-black"></i>
                                    </span>
                                    Filter Data Pelatihan
                                  </h5>
                                  <div
                                    className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                    data-bs-dismiss="modal"
                                    aria-label="Close"
                                  >
                                    <span className="svg-icon svg-icon-2x">
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                      >
                                        <rect
                                          opacity="0.5"
                                          x="6"
                                          y="17.3137"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(-45 6 17.3137)"
                                          fill="currentColor"
                                        />
                                        <rect
                                          x="7.41422"
                                          y="6"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(45 7.41422 6)"
                                          fill="currentColor"
                                        />
                                      </svg>
                                    </span>
                                  </div>
                                </div>
                                <form
                                  action="#"
                                  onSubmit={this.handleClickSearch}
                                >
                                  <div className="modal-body">
                                    <div className="row">
                                      {/* <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label required">Nama Pelatihan</label>
                                        <input className="form-control form-control-sm" placeholder="Masukkan nama pelatihan" name="name_search" onKeyPress={this.handleKeyPress} />
                                      </div> */}
                                      <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label">
                                          Penyelenggara
                                        </label>
                                        <Select
                                          name="penyelenggara"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({ inputValue }) =>
                                            "Data tidak tersedia"
                                          }
                                          className="form-select-sm form-select-solid selectpicker p-0"
                                          options={
                                            this.state.dataxpenyelenggara
                                          }
                                          onChange={
                                            this.handleChangePenyelenggara
                                          }
                                          value={
                                            this.state.dataxpenyelenggaravalue
                                          }
                                        />
                                      </div>
                                      <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label">
                                          Akademi
                                        </label>
                                        <Select
                                          name="akademi_id"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.state.dataxakademi
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm form-select-solid selectpicker p-0"
                                          options={this.state.dataxakademi}
                                          onChange={this.handleChangeAkademi}
                                          value={this.state.dataxakademivalue}
                                        />
                                        {/* <select className="form-select form-select-sm selectpicker" data-control="select2" data-placeholder="Select an option">
                                          <option>Test</option>
                                        </select> */}
                                      </div>
                                      <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label">
                                          Tema
                                        </label>
                                        <Select
                                          name="tema_id"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.state.dataxtema
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm form-select-solid selectpicker p-0"
                                          options={this.state.dataxtema}
                                          isDisabled={this.state.isDisabled}
                                          onChange={this.handleChangeTema}
                                          value={this.state.dataxtemavalue}
                                        />
                                      </div>
                                      <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label">
                                          Status Substansi
                                        </label>
                                        <Select
                                          name="status_substansi"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.substansistatus
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm form-select-solid selectpicker p-0"
                                          options={this.substansistatus}
                                          onChange={
                                            this.handleChangeStatusSubstansi
                                          }
                                          value={
                                            this.state.dataxstatussubstansivalue
                                          }
                                        />
                                      </div>
                                      <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label">
                                          Status Pelatihan
                                        </label>
                                        <Select
                                          name="status_pelatihan"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.pelatihanstatus
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm form-select-solid selectpicker p-0"
                                          options={this.pelatihanstatus}
                                          onChange={
                                            this.handleChangeStatusPelatihan
                                          }
                                          value={
                                            this.state.dataxstatuspelatihanvalue
                                          }
                                        />
                                      </div>
                                      <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label">
                                          Status Publish
                                        </label>
                                        <Select
                                          name="status_publish"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.optionstatus
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm form-select-solid selectpicker p-0"
                                          options={this.optionstatus}
                                          onChange={
                                            this.handleChangeStatusPublish
                                          }
                                          value={
                                            this.state.dataxstatuspublishvalue
                                          }
                                        />
                                      </div>
                                      <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label">
                                          Provinsi
                                        </label>
                                        <Select
                                          name="provinsi"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.state.dataxprov
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm form-select-solid selectpicker p-0"
                                          options={this.state.dataxprov}
                                          onChange={this.handleChangeProvinsi}
                                          value={this.state.dataxprovinsivalue}
                                        />
                                      </div>
                                      <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label">
                                          Tahun
                                        </label>
                                        <Select
                                          name="year"
                                          placeholder="Silahkan pilih"
                                          noOptionsMessage={({ inputValue }) =>
                                            !inputValue
                                              ? this.state.optionyear
                                              : "Data tidak tersedia"
                                          }
                                          className="form-select-sm form-select-solid selectpicker p-0"
                                          options={this.state.optionyear}
                                          onChange={this.handleChangeYear}
                                          value={this.state.dataxtahunvalue}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                  <div className="modal-footer">
                                    <div className="d-flex justify-content-between">
                                      <button
                                        type="reset"
                                        className="btn btn-sm btn-light me-3"
                                        onClick={this.handleClickReset}
                                      >
                                        Reset
                                      </button>
                                      <button
                                        type="submit"
                                        className="btn btn-sm btn-primary"
                                        data-bs-dismiss="modal"
                                      >
                                        Apply Filter
                                      </button>
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <DataTable
                          columns={this.columns}
                          data={this.state.datax}
                          progressPending={this.state.loading}
                          progressComponent={this.customLoader}
                          highlightOnHover
                          pointerOnHover
                          pagination
                          paginationServer={true}
                          paginationTotalRows={this.state.totalRows}
                          paginationComponentOptions={{
                            selectAllRowsItem: true,
                            selectAllRowsItemText: "Semua",
                          }}
                          onChangeRowsPerPage={(
                            currentRowsPerPage,
                            currentPage,
                          ) => {
                            this.handlePerRowsChange(
                              currentRowsPerPage,
                              currentPage,
                              "row-change",
                            );
                          }}
                          onChangePage={(page, totalRows) => {
                            this.handlePerRowsChange(
                              page,
                              totalRows,
                              "page-change",
                            );
                          }}
                          customStyles={this.customStyles}
                          persistTableHead={true}
                          noDataComponent={
                            <div className="mt-5">Tidak Ada Data</div>
                          }
                          onSort={this.handleChangeSort}
                          sortServer
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
