import React, { useState } from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import PelatihanPendaftaranContentAdd from "./PendaftaranDataListKanban";
import moment from "moment";
import Flatpickr from "react-flatpickr";
import { Indonesian } from "flatpickr/dist/l10n/id.js";
import { fp_inc, zeroDecimalValue } from "./helper";
import { numericOnly } from "../../publikasi/helper";
import {
  isSuperAdmin,
  isAdminAkademi,
  isExceptionRole,
} from "./../../../components/AksesHelper";
import RenderFormElement from "./RenderFormElement";
let segment_url = window.location.pathname.split("/");
import Editor from "@arbor-dev/ckeditor5-arbor-custom";
export default class PelatihanContent extends React.Component {
  constructor(props) {
    super(props);
    Cookies.remove("pelatian_id");
    this.submitFormRef = React.createRef(null);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleClickMetode = this.handleClickMetodeAction.bind(this);
    this.handleClickMetodePelaksanaan =
      this.handleClickMetodePelaksanaanAction.bind(this);
    this.handleClickRefForm = this.handleClickRefFormAction.bind(this);
    this.handleClickRefSilabusForm =
      this.handleClickRefSilabusFormAction.bind(this);
    this.handleChangeAkademi = this.handleChangeAkademiAction.bind(this);
    this.handleChangeTema = this.handleChangeTemaAction.bind(this);
    this.handleChangeProv = this.handleChangeProvAction.bind(this);
    this.handleChangeKabupaten = this.handleChangeKabupatenAction.bind(this);
    this.handleChangeKomitmen = this.handleChangeKomitmenAction.bind(this);
    this.handleChangeSilabus = this.handleChangeSilabusAction.bind(this);
    this.handleChangeDeskripsiKomitmen =
      this.handleChangeDeskripsiKomitmenAction.bind(this);
    this.handleKuotaPendaftar = this.handleKuotaPendaftarAction.bind(this);
    this.handleKuotaPeserta = this.handleKuotaPesertaAction.bind(this);
    this.handleSubmit = this.handleSubmitAction.bind(this);
    this.handleClickSelectForm = this.handleClickSelectFormAction.bind(this);
    this.handleClickCreateForm = this.handleClickCreateFormAction.bind(this);
    this.handleClickSelectSilabus =
      this.handleClickSelectSilabusAction.bind(this);
    this.handleClickCreateSilabus =
      this.handleClickCreateSilabusAction.bind(this);
    this.handleChangeDescription =
      this.handleChangeDescriptionAction.bind(this);
    this.handleChangeAlamat = this.handleChangeAlamatAction.bind(this);
    this.handleChangeLvlPelatihan =
      this.handleChangeLvlPelatihanAction.bind(this);
    this.handleChangeProgram = this.handleChangeProgramAction.bind(this);
    this.handleChangeNilai = this.handleChangeNilaiAction.bind(this);
    this.handleChangeNama = this.handleChangeNamaAction.bind(this);
    this.handleChangeURL = this.handleChangeURLAction.bind(this);
    this.handleChangeMitra = this.handleChangeMitraAction.bind(this);
    this.handleChangePenyelenggara =
      this.handleChangePenyelenggaraAction.bind(this);
    this.handlePendaftaranTanggalMulai =
      this.handlePendaftaranTanggalMulaiAction.bind(this);
    this.handlePendaftaranTanggalSelesai =
      this.handlePendaftaranTanggalSelesaiAction.bind(this);
    this.handlePelatihanTanggalMulai =
      this.handlePelatihanTanggalMulaiAction.bind(this);
    this.handlePelatihanTanggalSelesai =
      this.handlePelatihanTanggalSelesaiAction.bind(this);
    this.handleAdministrasiTanggalMulai =
      this.handleAdministrasiTanggalMulaiAction.bind(this);
    this.handleAdministrasiTanggalSelesai =
      this.handleAdministrasiTanggalSelesaiAction.bind(this);
    this.handleTestSubstansiTanggalMulai =
      this.handleTestSubstansiTanggalMulaiAction.bind(this);
    this.handleTestSubstansiTanggalSelesai =
      this.handleTestSubstansiTanggalSelesaiAction.bind(this);
    this.handleChangeStatusKuota =
      this.handleChangeStatusKuotaAction.bind(this);
    this.handleChangeSertifikasi =
      this.handleChangeSertifikasiAction.bind(this);
    this.handleChangeLpjPeserta = this.handleChangeLpjPesertaAction.bind(this);
    this.handleChangeZonasi = this.handleChangeZonasiAction.bind(this);
    this.handleChangeBatch = this.handleChangeBatchAction.bind(this);
    this.handleChangeDisabilitasUmum =
      this.handleChangeDisabilitasUmumAction.bind(this);
    this.handleChangeDisabilitasTunaNetra =
      this.handleChangeDisabilitasTunaNetraAction.bind(this);
    this.handleChangeDisabilitasTunaRungu =
      this.handleChangeDisabilitasTunaRunguAction.bind(this);
    this.handleChangeDisabilitasTunaDaksa =
      this.handleChangeDisabilitasTunaDaksaAction.bind(this);
    this.handleChangeAlurPendaftaran =
      this.handleChangeAlurPendaftaranAction.bind(this);

    this.handleClickResetButton = this.handleClickResetButtonAction.bind(this);
    this.handleChangeApakahMidTest =
      this.handleChangeApakahMidTestAction.bind(this);
    this.handleClickAddSilabus = this.handleClickAddSilabusAction.bind(this);
    this.handleClickSaveSilabus = this.handleClickSaveSilabusAction.bind(this);
    this.handleClickDeleteSilabus =
      this.handleClickDeleteSilabusAction.bind(this);
    this.handleChangeJudulForm = this.handleChangeJudulFormAction.bind(this);
    this.handleChangePelaksanaAssesments =
      this.handleChangePelaksanaAssesmentsAction.bind(this);
    this.fileSilabusRef = React.createRef(null);
    this.handleOptionChange = this.handleOptionChangeAction.bind(this);

    this.state = {
      isLoading: false,
      prevData: [],
      fields: {},
      errors: {},
      dataxakademi: [],
      dataxtema: [],
      dataxprov: [],
      dataxkab: [],
      dataxpenyelenggara: [],
      dataxmitra: [],
      dataxzonasi: [],
      silabusfile: [],
      showing: true,
      showingdeskripsi: false,
      showingembed: true,
      showingmetodepelaksanaan: true,
      showingMidTestDate: false,
      dataxselform: [],
      dataxselusesilabus: [],
      seljudulform: [],
      valuekomitmen: "Iya",
      valuedeskripsikomitmen: "",
      valuejudulform: {},
      dataxlevelpelatihan: [],
      dataxpreview: [],
      dataxpreviewtitle: "",
      selectedOption: null,
      akademi_id: null,
      isDisabled: true,
      isDisabledKab: true,
      kuotapendaftar: "",
      kuotapeserta: "",
      showingAlurPendaftaranDate: false,
      showSelForm: false,
      showCreateForm: false,
      showOptionButton: true,
      showViewForm: false,
      selectValueHidden: "",
      resSelectValueHidden: "",
      deskripsihidden: "",
      deskripsiPelatihan: "",
      alamat: "",
      filex: "",
      showingSelSilabus: true,
      selakademiobj: [],
      showingCreateSilabus: "",
      // filexname: "",
      // isian forms
      sellvlpelatihan: [],
      selakademi: [],
      seltema: [],
      selpenyelenggara: [],
      selmitra: [],
      selzonasi: [],
      selprovinsi: [],
      selkabupaten: [],
      showBtnNextW2: true,
      valueIndexStepper: "",
      valueFormBuilderId: "",
      valueFormBuilderName: "",
      alurPendaftaran: "",
      pendaftaranTanggalMulai: "",
      pendaftaranTanggalSelesai: "",
      pelatihanTanggalMulai: "",
      pelatihanTanggalSelesai: "",
      administrasiTanggalMulai: "",
      administrasiTanggalSelesai: "",
      testsubstansiTanggalMulai: "",
      testsubstansiTanggalSelesai: "",
      midTestTanggal: "",
      selnamasilabus: {},
      seltotaljpsilabus: "",
      pelaksana_assesment_id: "",
      pelaksanaassesmentsList: [],
      silabusList: [
        {
          id: 1,
          isi_silabus: "",
          jp_silabus: "",
        },
      ],
      detailSilabus: {
        nama: "",
        totalJp: "",
        id: "",
        isiSilabus: [],
      },
      showViewSilabus: false,
      selSertifikasiList: [],
      // showOptionButtonSilabus: true,
      metode_pelatihan: "",
      metode_pelaksanaan: "",
      input_nilai: null,
      program_dts: null,
      name_pelatihan: "",
      url_vicon: "",
      status_kuota: "",
      sertifikasi: "",
      batch: "",
      disabilitas_umum: null,
      disabilitas_tuna_netra: null,
      disabilitas_tuna_rungu: null,
      disabilitas_tuna_daksa: null,
      silabus_header_id: null,
      pelatian_id: null,
      saved: false,
      fileSilabus: null,
      wsml_themes: [],
      isKuotaPendaftarVisible: true,
    };
    this.formDatax = new FormData();
  }
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    moment.locale("id");
    Cookies.remove("step", { path: "/" });
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    window.addEventListener("beforeunload", this.onUnload);
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    const dataAkademik = { start: 0, length: 100, status: "publish" };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/akademi/list-akademi",
        dataAkademik,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxakademi = [];
        optionx.map((data) =>
          dataxakademi.push({
            value: data.id,
            label: data.name,
            slug: data.slug,
          }),
        );
        this.setState({ dataxakademi });
        return axios.post(
          process.env.REACT_APP_BASE_API_URI + "/pelatihanz/sertifikasi_list",
          {},
          this.configs,
        );
        //tema
      })
      .then((res) => {
        this.setState({ selSertifikasiList: res.data.result.Data });
        this.reloadStepper();
        swal.close();
      });
    const dataProv = { start: 1, rows: 1000 };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/provinsi",
        dataProv,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxprov = [];
        optionx.map((data) =>
          dataxprov.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxprov });

        const dataKab = { kdprop: this.state.provinsi };
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/kabupaten",
            dataKab,
            this.configs,
          )
          .then((res) => {
            this.setState({ isDisabledKab: false });
            const optionx = res.data.result.Data;
            const dataxkab = [];
            optionx.map((data) =>
              dataxkab.push({ value: data.id, label: data.name }),
            );
            this.setState({ dataxkab });
          });
      });
    const dataSelForm = { start: 0, length: 1000 };
    const dataLvl = {};
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/levelpelatihan",
        dataLvl,
        this.configs,
      )
      .then((res) => {
        const options = res.data.result.Data;
        const dataxlevelpelatihan = [];
        options.map((data) =>
          dataxlevelpelatihan.push({
            value: data.id,
            label: data.name + " (" + data.nilai + ")",
          }),
        );
        this.setState({ dataxlevelpelatihan });
      });
    const dataPenyelenggara = {
      mulai: 0,
      limit: 999999,
      cari: "",
      sort: "id desc",
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list_satker",
        dataPenyelenggara,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxpenyelenggara = [];
        optionx.map((data) =>
          dataxpenyelenggara.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxpenyelenggara });
      });

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/mitra/list-mitra-s3",
        {
          start: 0,
          length: 100,
          sort: "id",
          sort_val: "desc",
          param: "",
          status: 1,
          tahun: new Date().getFullYear(),
        },
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxmitra = [];
        optionx.map((data) =>
          dataxmitra.push({ value: data.id, label: data.nama_mitra }),
        );
        this.setState({ dataxmitra });
      });
    const dataZonasi = { mulai: 0, limit: 100, sort: "id desc", cari: "" };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/masterdata/API_List_Master_Zonasi",
        dataZonasi,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxzonasi = [];
        optionx.map((data) =>
          dataxzonasi.push({ value: data.id, label: data.nama_zonasi }),
        );
        this.setState({ dataxzonasi });
      });

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/referensi/pelaksana_assements",
        {},
        this.configs,
      )
      .then((res) => {
        this.setState({ pelaksanaassesmentsList: res.data.result.Data });
      });

    // WSML
    axios
      .get(
        process.env.REACT_APP_BASE_API_URI + "/wsml/get-themes",
        this.configs,
      )
      .then((res) => {
        const wsml_themes_tmp = res.data.result.result.Message.results.data;
        this.setState({ wsml_themes: wsml_themes_tmp });
      });
  }
  componentWillUnmount() {
    window.removeEventListener("beforeunload", this.onUnload);
  }

  reloadStepper() {
    const stepper = window?.document?.getElementById("stepper-script");
    if (stepper) {
      stepper.remove();
    }
    const script = window?.document?.createElement("script");
    script.setAttribute("id", "stepper-script");
    script.async = true;
    script.src = "/assets/js/custom/modals/pelatihan-create.js";

    //For head
    // document.head.appendChild(script);

    // For body
    window?.document?.body.appendChild(script);

    // For component
    // this.div.appendChild(script);
  }

  onUnload = (event) => {
    if (!this.state.saved) {
      event.preventDefault();
      event.returnValue = "Pelatihan-Tambah";
    }
  };

  onUnloadWill = (e) => {
    e.preventDefault();
    e.returnValue = "Pelatihan-Tambah";
  };

  handleChangePelaksanaAssesmentsAction(e) {
    this.setState({ pelaksana_assesment_id: e.currentTarget.value });
  }
  isSertifikasi = (id) => {
    const sertifikasi = this.state.selSertifikasiList.filter(
      (elem) => elem.id == id && elem.name !== "Tidak Ada",
    );
    return sertifikasi.length > 0;
  };
  handleChangeProvAction = (e) => {
    const { value, options, selectedIndex } = e.target;
    this.setState({
      selprovinsi: { value: value, label: options[selectedIndex].innerHTML },
    });
    // Cookies.set("provinsi", value, 1);
    const dataKab = { kdprop: value };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/kabupaten",
        dataKab,
        this.configs,
      )
      .then((res) => {
        this.setState({ isDisabledKab: false });
        const optionx = res.data.result.Data;
        const dataxkab = [];
        optionx.map((data) =>
          dataxkab.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxkab });
        this.setState({ selkabupaten: [] });
      });
  };
  handleChangeKabupatenAction = (e) => {
    const { value, options, selectedIndex } = e.target;
    this.setState({
      selkabupaten: {
        value: value,
        label: options[selectedIndex].innerHTML,
      },
    });
    // Cookies.set("kabupaten", value, 1);
  };
  handleChangeKomitmenAction(e) {
    // Cookies.set("komitmen_peserta", e.target.value, 1);
    this.setState({ valuekomitmen: e.target.value });
    if (e.target.value == "Iya") {
      this.setState({ showingdeskripsi: true });
    } else {
      this.setState({ showingdeskripsi: false });
    }
  }
  handleChangeDeskripsiKomitmenAction(value) {
    document.getElementsByName("deskripsi_komitmen_hidden")[0].value = value;
    this.setState({ valuedeskripsikomitmen: value });
    // Cookies.set("deskripsi_komitmen", value, 1);
  }

  resetError() {
    let errors = {};
    errors["name"] = "";
    errors["deskripsi"] = "";
    this.setState({ errors: errors });
  }
  handleChangeSilabusAction(e) {
    const { value, options, selectedIndex } = e.target;
    this.setState({
      selnamasilabus: { value: value, label: options[selectedIndex] },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/detail_silabus",
        {
          id: value,
        },
        this.configs,
      )
      .then((res) => {
        this.setState(
          {
            detailSilabus: {
              id: res.data.result.id_Silabus,
              nama: res.data.result.NamaSilabus,
              totalJp: res.data.result.Jml_jampelajaran,
              isiSilabus: res.data.result.DataSilabus,
            },
            showBtnNextW2: true,
          },
          () => {
            this.setState({ showViewSilabus: true });
          },
        );
      })
      .catch((err) => {
        swal.fire({
          title:
            err.response?.data?.result?.Message ??
            "Terjadi kesalahan saat mengambil silabus",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      });
  }
  handleClickMetodeAction(e) {
    this.setState({ metode_pelatihan: e.target.value });
    if (e.target.value == "Online") {
      this.setState({ showing: false });
    } else {
      this.setState({ showing: true });
    }
  }
  handleClickMetodePelaksanaanAction(e) {
    this.setState({ metode_pelaksanaan: e.target.value });
    if (e.target.value == "Swakelola") {
      this.setState({ showingmetodepelaksanaan: false });
    } else {
      this.setState({ showingmetodepelaksanaan: true });
    }
  }
  handleDate(dateString) {
    if (moment(dateString, "DD MMMM YYYY HH:mm").isValid()) {
      return moment(dateString, "DD MMMM YYYY HH:mm").format(
        "YYYY-MM-DD HH:mm:ss",
      );
    }
    return dateString;
  }

  handleClickBatalAction(e) {
    window.history.back();
  }
  handleChangeJudulFormAction = (e) => {
    const { value, options, selectedIndex } = e.target;
    this.setState({
      valuejudulform: { value: value, label: options[selectedIndex].innerHTML },
    });
    this.setState({
      selectValueHidden: {
        value: value,
        label: options[selectedIndex].innerHTML,
      },
    });
    this.setState({
      seljudulform: {
        value: value,
        label: options[selectedIndex].innerHTML,
      },
    });
    // Cookies.set("judul_form", value, 1);
    this.setState({ valueFormBuilderId: value });
    this.setState({ showBtnNextW2: true });

    const dataFormSubmit = new FormData();
    dataFormSubmit.append("id", value);
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/cari-formbuilder",
        dataFormSubmit,
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          const dataxpreview = res.data.result.detail;
          const dataxpreviewtitle = res.data.result.utama[0].judul_form;
          this.setState({ dataxpreview });
          this.setState({ dataxpreviewtitle });
          this.setState({ showViewForm: true });
          swal.close();
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                //this.handleReload();
              }
            });
        }
      })
      .catch((err) => {
        console.error(err);
        swal.fire({
          title:
            err.response?.data?.result?.Message ??
            "Terjadi kesalahan saat mengambil form",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      });
  };
  handleClickRefFormAction() {
    const dataSelForm = { param: "" };
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/selformpendaftaran",
        dataSelForm,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxselform = [];
        optionx.map((data) =>
          dataxselform.push({ value: data.id, label: data.judul_form }),
        );
        this.setState({ dataxselform }, () => {});
        swal.close();
      })
      .catch((err) => {
        swal.fire({
          title:
            err.response.data.result.Message ??
            "Terjadi kesalahan saat mengambil silabus",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      });
  }
  handleClickRefSilabusFormAction() {
    const dataSelForm = {
      mulai: 0,
      limit: 1000,
      id_akademi:
        this.state.selakademi?.value == null ? 0 : this.state.selakademi?.value,
      id_tema:
        this.state.seltema?.value == null ? 0 : this.state.seltema?.value,
      cari: "",
      sort: "id desc",
    };
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list_silabus",
        dataSelForm,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxselusesilabus = [];
        optionx.map((data) =>
          dataxselusesilabus.push({
            value: data.id,
            label: data.judul_silabus,
          }),
        );
        this.setState({ dataxselusesilabus }, () => {
          swal.close();
        });
      })
      .catch((err) => {
        swal.fire({
          title:
            err.response.data.result.Message ??
            "Terjadi kesalahan saat mengambil silabus",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      });
  }
  handleChangeAkademiAction = (e) => {
    const { options, value, selectedIndex } = e.target;
    this.setState({
      selakademi: { value: value, label: options[selectedIndex].innerHTML },
      selakademiobj: this.state.dataxakademi.filter(
        (elem) => elem.value == value,
      ),
    });

    // Cookies.set("akademi_id", value, 1);
    const dataBody = { start: 1, rows: 100, id: value };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/cari_tema_byakademi",
        dataBody,
        this.configs,
      )
      .then((res) => {
        this.setState({ isDisabled: false });
        const optionx = res.data.result.Data;
        const dataxtema = [];
        optionx.map((data) =>
          dataxtema.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxtema });
        this.setState({ seltema: [] });
      })
      .catch((error) => {
        const dataxtema = [];
        this.setState({ dataxtema });
        this.setState({ seltema: [] });
        let messagex = error.response.data.result.Message;
        swal
          .fire({
            title: messagex,
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
            }
          });
      });
  };
  handleChangeTemaAction = (e) => {
    const { options, value, selectedIndex } = e.target;
    this.setState(
      {
        seltema: { value: value, label: options[selectedIndex].innerHTML },
      },
      () => {
        this.handleClickRefSilabusForm();
      },
    );
    // Cookies.set("tema_id", value, 1);
  };
  handleOptionChangeAction = (e) => {
    const selectedValue = parseInt(e, 10);
    let label = "";
    const matchingObject = this.state.dataxtema.find(
      (opt) => opt.value === selectedValue,
    );

    if (matchingObject) {
      label = matchingObject.label;
    }
    // console.log(this.state.wsml_themes);

    // Check if the model is ready
    if (this.state.wsml_themes.includes(label)) {
      // console.log(`${label} is in the list.`);
      this.setState({ kuotapendaftar: "999999" }, () => {
        // This code will run after kuotapendaftar has been updated
        this.setState({ isKuotaPendaftarVisible: false });
      });
    } else {
      // console.log(`${label} is in the list.`);
      this.setState({ kuotapendaftar: "" }, () => {
        // This code will run after kuotapendaftar has been updated
        this.setState({ isKuotaPendaftarVisible: true });
      });
    }
  };
  handleKuotaPendaftarAction(e) {
    this.setState({ kuotapendaftar: e.target.value });
    // Cookies.set("kuota_target_pendaftar", e.currentTarget.value, 1);
  }

  handleSubmitAction(e) {
    e.preventDefault();
    this.setState({ isLoading: true });

    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    const dataForm = new FormData(e.currentTarget);
    const dataFormSubmit = new FormData();
    let pendaftaran_mulai = this.handleDate(this.state.pendaftaranTanggalMulai);
    let pendaftaran_selesai = this.handleDate(
      this.state.pendaftaranTanggalSelesai,
    );
    let pelatihan_mulai = this.handleDate(this.state.pelatihanTanggalMulai);
    let pelatihan_selesai = this.handleDate(this.state.pelatihanTanggalSelesai);
    let tanggal_midtest = this.handleDate(this.state.midTestTanggal);
    dataFormSubmit.append("program_dts", this.state.program_dts);
    dataFormSubmit.append("input_nilai", this.state.input_nilai);
    dataFormSubmit.append("name", this.state.name_pelatihan);
    dataFormSubmit.append("level_pelatihan", this.state.sellvlpelatihan.value);
    dataFormSubmit.append("akademi_id", this.state.selakademi.value);
    dataFormSubmit.append("tema_id", this.state.seltema.value);
    dataFormSubmit.append("metode_pelaksanaan", this.state.metode_pelaksanaan);
    dataFormSubmit.append("user_by", Cookies.get("user_id"));
    if (dataForm.get("metode_pelaksanaan") == "Swakelola") {
      dataFormSubmit.append("mitra", 0);
    } else {
      dataFormSubmit.append("mitra", this.state.selmitra.value);
    }
    dataFormSubmit.append(
      "id_penyelenggara",
      this.state.selpenyelenggara.value,
    );
    dataFormSubmit.append(
      "deskripsi",
      this.state.deskripsiPelatihan.replaceAll("'", "''"),
    );
    dataFormSubmit.append("pendaftaran_mulai", pendaftaran_mulai);
    dataFormSubmit.append("pendaftaran_selesai", pendaftaran_selesai);
    dataFormSubmit.append("pelatihan_mulai", pelatihan_mulai);
    dataFormSubmit.append("pelatihan_selesai", pelatihan_selesai);
    if (this.state.showingMidTestDate) {
      dataFormSubmit.append("midtest_mulai", tanggal_midtest);
    }
    // file silabus
    dataFormSubmit.append("silabus", this.state.fileSilabus.file);
    // midtest
    dataFormSubmit.append("kuota_pendaftar", this.state.kuotapendaftar);
    dataFormSubmit.append("kuota_peserta", this.state.kuotapeserta);
    dataFormSubmit.append("status_kuota", this.state.status_kuota);
    dataFormSubmit.append("alur_pendaftaran", this.state.alurPendaftaran);
    // tanggal subtansi
    if (this.state.showingAlurPendaftaranDate) {
      dataFormSubmit.append(
        "administrasi_mulai",
        this.handleDate(this.state.administrasiTanggalMulai),
      );
      dataFormSubmit.append(
        "administrasi_selesai",
        this.handleDate(this.state.administrasiTanggalSelesai),
      );
      dataFormSubmit.append(
        "subtansi_mulai",
        this.handleDate(this.state.testsubstansiTanggalMulai),
      );
      dataFormSubmit.append(
        "subtansi_selesai",
        this.handleDate(this.state.testsubstansiTanggalSelesai),
      );
    }
    dataFormSubmit.append("sertifikasi", this.state.sertifikasi);
    dataFormSubmit.append(
      "pelaksana_assement_id",
      this.state.pelaksana_assesment_id ?? 0,
    );
    dataFormSubmit.append("lpj_peserta", "Tidak");
    dataFormSubmit.append("metode_pelatihan", this.state.metode_pelatihan);
    if (this.state.metode_pelatihan?.includes("Offline")) {
      dataFormSubmit.append("alamat", this.state.alamat.replaceAll("'", "''"));
      dataFormSubmit.append("provinsi", dataForm.get("provinsi"));
      dataFormSubmit.append("kabupaten", dataForm.get("kota_kabupaten"));
    } else {
      dataFormSubmit.append("alamat", "online");
      dataFormSubmit.append("provinsi", "online");
      dataFormSubmit.append("kabupaten", "online");
    }
    if (this.state.metode_pelatihan?.includes("Online")) {
      dataFormSubmit.append("url_vicon", this.state.url_vicon);
    }
    dataFormSubmit.append("zonasi", this.state.selzonasi.value);
    dataFormSubmit.append("batch", this.state.batch);
    dataFormSubmit.append("umum", this.state.disabilitas_umum ?? 0);
    dataFormSubmit.append("tuna_netra", this.state.disabilitas_tuna_netra ?? 0);
    dataFormSubmit.append("tuna_rungu", this.state.disabilitas_tuna_rungu ?? 0);
    dataFormSubmit.append("tuna_daksa", this.state.disabilitas_tuna_daksa ?? 0);
    dataFormSubmit.append("silabus_header_id", this.state.detailSilabus?.id);
    dataFormSubmit.append("status_publish", "0");
    dataFormSubmit.append("status_substansi", "Review");
    dataFormSubmit.append("status_pelatihan", "Review Substansi");
    dataFormSubmit.append(
      "form_pendaftaran_id",
      this.state.valuejudulform.value,
    );
    dataFormSubmit.append("komitmen", this.state.valuekomitmen);
    dataFormSubmit.append(
      "deskripsi_komitmen",
      this.state.valuedeskripsikomitmen.replaceAll("'", "''"),
    );

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/addpelatihanx",
        dataFormSubmit,
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Data;
        if (statux) {
          this.setState({ isLoading: false });
          swal
            .fire({
              title: messagex,
              icon: "success",
              allowOutsideClick: false,
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ saved: true }, () => {
                  window.location = "/pelatihan/pelatihan";
                });
              }
            });
        } else {
          this.setState({ isLoading: false });
          messagex = this.parsingFormErrorValidation(res.data.data);
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      })
      .catch((error) => {
        this.setState({ isLoading: false });
        let messagex = "";
        if (error.response?.data?.result?.Message !== undefined) {
          messagex = error.response.data?.result?.Message;
        } else {
          messagex = this.parsingFormErrorValidation(error.response.data);
        }
        swal
          .fire({
            title: "Terjadi kesalahan.",
            html: messagex,
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
            }
          });
      });
  }

  parsingFormErrorValidation(obj) {
    let message = "";
    Object.keys(obj.data).forEach((key) => {
      const errArray = obj.data[key];
      errArray.forEach((elem) => {
        message = message + `<div>${elem}</div>`;
      });
    });
    return message;
  }

  convertFileToBase64(file) {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      window.localStorage.setItem("upload_silabus_size", file.size);
      this.setState({
        fileSilabus: {
          ...this.state.fileSilabus,
          base64: reader.result,
        },
      });
    };
  }

  handleClickSelectFormAction(e) {
    this.setState(
      {
        showOptionButton: false,
        showCreateForm: false,
        showSelForm: true,
        showBtnNextW2: false,
        valueFormBuilderId: 0,
      },
      () => {
        // this.handleClickRefFormAction(e);
      },
    );
  }
  handleClickCreateFormAction(e) {
    this.setState({
      showOptionButton: false,
      showCreateForm: true,
      showSelForm: false,
      selectValueHidden: "",
      showBtnNextW2: false,
      valueFormBuilderName: 0,
    });
  }
  handleClickSelectSilabusAction(e) {
    const dataSelForm = {
      mulai: 0,
      limit: 1000,
      id_akademi:
        this.state.selakademi?.value == null ? 0 : this.state.selakademi?.value,
      id_tema:
        this.state.seltema?.value == null ? 0 : this.state.seltema?.value,
      cari: "",
      sort: "id desc",
    };
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    Cookies.set("silabusTabShowed", "select", 1);
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list_silabus",
        dataSelForm,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxselusesilabus = [];
        optionx.forEach((data) =>
          dataxselusesilabus.push({
            value: data.id,
            label: data.judul_silabus,
          }),
        );
        this.setState({ dataxselusesilabus }, () => {
          swal.close();
        });
      })
      .catch((err) => {
        swal.fire({
          title:
            err.response.data.result.Message ??
            "Terjadi kesalahan saat mengambil silabus",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      });
  }
  handleClickCreateSilabusAction(e) {
    Cookies.set("silabusTabShowed", "create", 1);
    this.setState({
      // showOptionButtonSilabus: false,
      showingCreateSilabus: true,
      // showingSelSilabus: false,
    });
  }
  handleChangeDescriptionAction(value) {
    document.getElementsByName("deskripsi_hidden")[0].value = value;
    this.setState({ deskripsiPelatihan: value });
    // Cookies.set("deskripsi", value, 1);
  }
  handleChangeAlamatAction(value) {
    this.setState({ alamat: value });
    document.getElementsByName("alamat_hidden")[0].value = value;
    // Cookies.set("alamat", value, 1);
  }
  handleChangeLvlPelatihanAction = (e) => {
    const { options, value, selectedIndex } = e.target;
    this.setState({
      sellvlpelatihan: {
        value: value,
        label: options[selectedIndex].innerHTML,
      },
    });
    // Cookies.set("level_pelatihan", e.target.value, 1);
  };
  handleChangeNilaiAction = (e) => {
    this.setState({ input_nilai: e.target.value });
  };
  handleChangeProgramAction = (e) => {
    // Cookies.set("program_dts", e.currentTarget.value, 1);
    this.setState({ program_dts: e.target.value });
  };
  handleChangeNamaAction = (e) => {
    this.setState({ name_pelatihan: e.currentTarget.value });
    // Cookies.set("name_pelatihan", e.currentTarget.value, 1);
  };
  handleChangeURLAction = (e) => {
    this.setState({ url_vicon: e.currentTarget.value });
  };
  handleChangeMitraAction = (e) => {
    const { options, value, selectedIndex } = e.target;
    this.setState({
      selmitra: { value: value, label: options[selectedIndex].innerHTML },
    });
    // Cookies.set("mitra", value, 1);
  };
  handleChangePenyelenggaraAction = (e) => {
    const { options, value, selectedIndex } = e.target;
    this.setState({
      selpenyelenggara: {
        value: value,
        label: options[selectedIndex].innerHTML,
      },
    });
    // Cookies.set("penyelenggara", value, 1);
  };

  handlePendaftaranTanggalMulaiAction = (e) => {
    this.setState({ pendaftaranTanggalMulai: e.currentTarget.value });
  };
  handlePendaftaranTanggalSelesaiAction = (e) => {
    this.setState({ pendaftaranTanggalSelesai: e.currentTarget.value });
    // Cookies.set("pendaftaran_tanggal_selesai", e.currentTarget.value, 1);
  };
  handlePelatihanTanggalMulaiAction = (e) => {
    this.setState({ pelatihanTanggalMulai: e.currentTarget.value });
    // Cookies.set("pelatihan_tanggal_mulai", e.currentTarget.value, 1);
  };
  handlePelatihanTanggalSelesaiAction = (e) => {
    this.setState({ pelatihanTanggalSelesai: e.currentTarget.value });
    // Cookies.set("pelatihan_tanggal_selesai", e.currentTarget.value, 1);
  };
  handleAdministrasiTanggalMulaiAction = (e) => {
    this.setState({ administrasiTanggalMulai: e.currentTarget.value });
    // Cookies.set("administrasi_tanggal_mulai", e.currentTarget.value, 1);
  };
  handleAdministrasiTanggalSelesaiAction = (e) => {
    this.setState({ administrasiTanggalSelesai: e.currentTarget.value });
    // Cookies.set("administrasi_tanggal_selesai", e.currentTarget.value, 1);
  };
  handleTestSubstansiTanggalMulaiAction = (e) => {
    this.setState({ testsubstansiTanggalMulai: e.currentTarget.value });
    // Cookies.set("testsubstansi_tanggal_mulai", e.currentTarget.value, 1);
  };
  handleTestSubstansiTanggalSelesaiAction = (e) => {
    this.setState({ testsubstansiTanggalMulai: e.currentTarget.value });
    // Cookies.set("testsubstansi_tanggal_selesai", e.currentTarget.value, 1);
  };
  handleKuotaPesertaAction = (e) => {
    this.setState({ kuotapeserta: e.target.value });
    // Cookies.set("kuota_target_peserta", e.currentTarget.value, 1);
  };
  handleChangeStatusKuotaAction = (e) => {
    this.setState({ status_kuota: e.currentTarget.value });
    // Cookies.set("status_kuota", e.currentTarget.value, 1);
  };
  handleChangeSertifikasiAction = (e) => {
    this.setState({ sertifikasi: e.currentTarget.value });
    // Cookies.set("sertifikasi", e.currentTarget.value, 1);
  };
  handleChangeLpjPesertaAction = (e) => {
    this.setState({ lpj_peserta: e.currentTarget.value });
    // Cookies.set("lpj_peserta", e.currentTarget.value, 1);
  };
  handleChangeZonasiAction = (e) => {
    const { options, value, selectedIndex } = e.target;
    this.setState({
      selzonasi: { value: value, label: options[selectedIndex].innerHTML },
    });
    // Cookies.set("zonasi", value, 1);
  };
  handleChangeBatchAction = (e) => {
    this.setState({ batch: e.target.value });
    // Cookies.set("batch", e.currentTarget.value, 1);
  };
  handleChangeDisabilitasUmumAction = (e) => {
    this.setState({
      disabilitas_umum: e.target.checked ? e.currentTarget.value : 0,
    });
    // Cookies.set("umum", e.currentTarget.checked, 1);
    if (e.currentTarget.checked) {
      document.getElementsByName("disabilitas_hidden")[0].value = 1;
    } else {
      document.getElementsByName("disabilitas_hidden")[0].value = -1;
    }
  };
  handleChangeDisabilitasTunaNetraAction = (e) => {
    this.setState({
      disabilitas_tuna_netra: e.target.checked ? e.currentTarget.value : 0,
    });
    // Cookies.set("tuna_netra", e.currentTarget.checked, 1);
    if (e.currentTarget.checked) {
      document.getElementsByName("disabilitas_hidden")[0].value = 2;
    } else {
      document.getElementsByName("disabilitas_hidden")[0].value = -1;
    }
  };
  handleChangeDisabilitasTunaRunguAction = (e) => {
    this.setState({
      disabilitas_tuna_rungu: e.target.checked ? e.currentTarget.value : 0,
    });
    // Cookies.set("tuna_rungu", e.currentTarget.checked, 1);
    if (e.currentTarget.checked) {
      document.getElementsByName("disabilitas_hidden")[0].value = 3;
    } else {
      document.getElementsByName("disabilitas_hidden")[0].value = -1;
    }
  };
  handleChangeDisabilitasTunaDaksaAction = (e) => {
    this.setState({
      disabilitas_tuna_daksa: e.target.checked ? e.currentTarget.value : 0,
    });
    // Cookies.set("tuna_daksa", e.currentTarget.checked, 1);
    if (e.currentTarget.checked) {
      document.getElementsByName("disabilitas_hidden")[0].value = 4;
    } else {
      document.getElementsByName("disabilitas_hidden")[0].value = -1;
    }
  };
  handleChangeAlurPendaftaranAction = (e) => {
    this.setState({ alurPendaftaran: e.currentTarget.value });
    // Cookies.set("alur_pendaftaran", e.currentTarget.value, 1);
    if (e.currentTarget.value == "Administrasi") {
      this.setState({ showingAlurPendaftaranDate: false }, () => {});
    } else {
      this.setState({ showingAlurPendaftaranDate: true }, () => {
        if (this.state.alurPendaftaran == "Test Substansi - Administrasi") {
          this.setState({
            testsubstansiTanggalMulai: this.administrasiTanggalMulai,
            testsubstansiTanggalSelesai: this.administrasiTanggalSelesai,
          });
        }
      });
    }
  };
  handleChangeApakahMidTestAction = (e) => {
    if (e.currentTarget.value == "Iya") {
      this.setState({ showingMidTestDate: true });
    } else {
      this.setState({ showingMidTestDate: false });
    }
  };
  handleClickAddSilabusAction = () => {
    const lastId = this.state.silabusList[this.state.silabusList.length - 1].id;
    this.setState({
      silabusList: [
        ...this.state.silabusList,
        {
          id: lastId + 1,
          jp_silabus: "",
          isi_silabus: "",
        },
      ],
    });
  };
  handleInputSilabus = (field, payload, id) => {
    this.state.silabusList.map((elem) => {
      if (elem.id == id) {
        return (elem[field] = payload);
      }
    });
  };
  handleClickSaveSilabusAction = () => {
    // create silabus data
    const jsonBody = [
      {
        jml_jp: this.state.seltotaljpsilabus,
        judul: this.state.selnamasilabus,
        id_user: Cookies.get("user_id"),
        id_akademi: this.state.akademi_id,
        detail_json: this.state.silabusList.map((elem) => {
          return {
            name: elem.isi_silabus,
            jam_pelajaran: elem.jp_silabus,
          };
        }),
      },
    ];
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/create_silabus",
        jsonBody,
        this.configs,
      )
      .then((res) => {
        swal.fire({
          title: res.data.result.Message ?? "Berhasil menyimpan data!",
          allowOutsideClick: false,
          icon: "success",
        });
        this.setState({ detailSilabus: res.data.result.Data });
      })
      .catch((err) => {
        swal.fire({
          title: err.response.data.result.Message ?? "Terjadi kesalahan!",
          icon: "warning",
        });
      });
    this.setState({
      showBtnNextW2: true,
    });
  };
  handleClickDeleteSilabusAction = (index) => {
    let fields = this.state.silabusList;
    fields.splice(index, 1);
    this.setState({ silabusList: fields });
  };
  handleClickResetButtonAction = (e) => {
    this.setState({
      showOptionButton: true,
      // showOptionButtonSilabus: true,
      showCreateForm: false,
      showSelForm: false,
      showingCreateSilabus: false,
      // showingSelSilabus: false,
      showBtnNextW2: false,
      valueFormBuilderId: "",
    });
  };
  render() {
    return (
      <div>
        <input
          type="hidden"
          name="csrf-token"
          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
        />
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  List Pelatihan
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div
                    className="stepper stepper-links mb-n3"
                    id="kt_create_account_stepper"
                  >
                    <div className="col-lg-12 mt-7">
                      <div className="card border">
                        <div className="card-header">
                          <div className="card-title">
                            <div className="stepper-nav flex-wrap mb-n4">
                              <div
                                className="stepper-item ms-0 me-3 my-2 current"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label ms-0">
                                    <h3 className="stepper-title fs-6">
                                      Informasi Pelatihan
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Jadwal
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Silabus
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Form Pendaftaran
                                    </h3>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="stepper-item mx-3 my-2"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <div className="stepper-wrapper d-flex align-items-center">
                                  <div className="stepper-label">
                                    <h3 className="stepper-title fs-6">
                                      Form Komitmen
                                    </h3>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="card-body">
                          <form
                            action="#"
                            onSubmit={this.handleSubmit}
                            id="kt_create_account_form"
                          >
                            <div
                              className="flex-column current"
                              data-kt-stepper-element="content"
                            >
                              <div className="row mt-7">
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Kategori Program Pelatihan
                                  </label>
                                  <div className="d-flex">
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="program_dts"
                                        value={1}
                                        id="program_dts1"
                                        onChange={this.handleChangeProgram}
                                        defaultChecked={
                                          this.state.program_dts === 1
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="program_dts1"
                                      >
                                        Digital Talent Scholarship
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="program_dts"
                                        value={0}
                                        id="program_dts2"
                                        onChange={this.handleChangeProgram}
                                        defaultChecked={
                                          this.state.program_dts === 0
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="program_dts2"
                                      >
                                        Lainnya
                                      </label>
                                    </div>
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["program_dts"]}
                                  </span>
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Nama Pelatihan
                                  </label>
                                  <input
                                    className="form-control form-control-sm"
                                    placeholder="Masukkan nama pelatihan"
                                    name="name_pelatihan"
                                    defaultValue={
                                      this.state.name_pelatihan ?? ""
                                    }
                                    onBlur={this.handleChangeNama}
                                  />
                                  <span className="text-muted fs-8 d-block">
                                    Nama tidak perlu mengandung batch karena
                                    batch akan otomatis tampil sesuai isian pada
                                    field batch
                                  </span>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["name_pelatihan"]}
                                  </span>
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Batch Pelatihan
                                  </label>
                                  <input
                                    className="form-control form-control-sm"
                                    placeholder="Masukkan batch"
                                    name="batch"
                                    // value={this.state.batch ?? ""}
                                    onBlur={this.handleChangeBatch}
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["batch"]}
                                  </span>
                                </div>

                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Pilih Akademi
                                  </label>
                                  {/* <Select
                                      id="id_akademi"
                                      name="akademi_id"
                                      placeholder="Silahkan pilih"
                                      noOptionsMessage={({ inputValue }) =>
                                        !inputValue
                                          ? this.state.dataxakademi
                                          : "Data tidak tersedia"
                                      }
                                      className="form-select-sm selectpicker p-0"
                                      options={this.state.dataxakademi}
                                      value={this.state.selakademi}
                                      onChange={this.handleChangeAkademi}
                                    /> */}
                                  <select
                                    className="form-select form-select-sm"
                                    name="akademi_id"
                                    value={this.state.selakademi?.value ?? ""}
                                    onChange={this.handleChangeAkademi}
                                  >
                                    <option
                                      value=""
                                      style={{ display: "none" }}
                                    >
                                      Silahkan pilih
                                    </option>
                                    {this.state.dataxakademi &&
                                      this.state.dataxakademi.map(
                                        (opt, index) => (
                                          <option
                                            key={`akademi_${index}`}
                                            value={opt.value}
                                          >
                                            {opt.label}
                                          </option>
                                        ),
                                      )}
                                  </select>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["akademi_id"]}
                                  </span>
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Pilih Tema
                                  </label>
                                  {/* <Select
                                      name="tema_id"
                                      placeholder="Silahkan pilih"
                                      noOptionsMessage={({ inputValue }) =>
                                        !inputValue
                                          ? this.state.dataxtema
                                          : "Data tidak tersedia"
                                      }
                                      className="form-select-sm selectpicker p-0"
                                      options={this.state.dataxtema}
                                      //isDisabled={this.state.isDisabled}
                                      value={this.state.seltema}
                                      onChange={this.handleChangeTema}
                                    /> */}
                                  <select
                                    className="form-select form-select-sm"
                                    name="tema"
                                    value={this.state.seltema?.value ?? ""}
                                    // onChange={this.handleChangeTema}
                                    onChange={(event) => {
                                      this.handleChangeTema(event);
                                      // Add the function you want to run here
                                      this.handleOptionChange(
                                        event.target.value,
                                      );
                                    }}
                                    disabled={this.state.dataxtema.length <= 0}
                                  >
                                    <option
                                      value=""
                                      style={{ display: "none" }}
                                    >
                                      Silahkan pilih
                                    </option>
                                    {this.state.dataxtema &&
                                      this.state.dataxtema.map((opt, index) => (
                                        <option
                                          key={`tema_${index}`}
                                          value={opt.value}
                                        >
                                          {opt.label}
                                        </option>
                                      ))}
                                  </select>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["tema"]}
                                  </span>
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Deskripsi Pelatihan
                                  </label>
                                  {/* <textarea className="form-control form-control-sm" placeholder="Masukkan deksripsi pelatihan" name="deskripsi" value={this.state.fields["deskripsi"]}></textarea> */}
                                  <CKEditor
                                    editor={Editor}
                                    data={this.state.deskripsiPelatihan}
                                    name="deskripsi"
                                    onReady={(editor) => {}}
                                    config={{
                                      ckfinder: {
                                        // Upload the images to the server using the CKFinder QuickUpload command.
                                        uploadUrl:
                                          process.env.REACT_APP_BASE_API_URI +
                                          "/publikasi/ckeditor-upload-image",
                                      },
                                    }}
                                    onBlur={(event, editor) => {
                                      const data = editor.getData();
                                      this.handleChangeDescription(data);
                                    }}
                                    onFocus={(event, editor) => {}}
                                  />
                                  <input
                                    type="hidden"
                                    name="deskripsi_hidden"
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["deskripsi"]}
                                  </span>
                                </div>
                                <div className="col-lg-12">
                                  <div className="mb-7 fv-row">
                                    <div
                                      style={{
                                        display:
                                          this.state.fileSilabus == null
                                            ? ""
                                            : "none",
                                      }}
                                    >
                                      <label className="form-label required">
                                        Upload Silabus
                                      </label>
                                      <input
                                        type="file"
                                        className="form-control form-control-sm mb-2"
                                        name="upload_silabus"
                                        accept=".pdf"
                                        ref={this.fileSilabusRef}
                                        onChange={(e) => {
                                          this.setState(
                                            {
                                              fileSilabus: {
                                                file:
                                                  e.currentTarget.files[0] ??
                                                  null,
                                                namaFile:
                                                  e.currentTarget.files[0]
                                                    ?.name ?? "",
                                              },
                                            },
                                            () => {
                                              this.convertFileToBase64(
                                                this.state.fileSilabus.file,
                                              );
                                            },
                                          );
                                        }}
                                      />
                                      <small className="text-muted">
                                        Format File (.pdf), Max 10 MB
                                      </small>
                                      <br />
                                    </div>
                                    {this.state.fileSilabus && (
                                      <div
                                        className="alert alert-success alert-dismissible fade show mt-1"
                                        role="alert"
                                      >
                                        <strong>
                                          {this.state.fileSilabus?.namaFile}
                                        </strong>
                                        <button
                                          type="button"
                                          className="btn-close"
                                          data-bs-dismiss="alert"
                                          aria-label="Close"
                                          onClick={(e) => {
                                            e.preventDefault();
                                            this.setState({
                                              fileSilabus: null,
                                            });
                                            this.fileSilabusRef.current.value =
                                              null;
                                          }}
                                        ></button>
                                      </div>
                                    )}
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["upload_silabus"]}
                                    </span>
                                  </div>
                                </div>
                                <div>
                                  <div className="my-8 border-top mx-0"></div>
                                </div>
                                <h2 className="fs-5 text-muted mb-3">
                                  Level & Sertifikasi Pelatihan
                                </h2>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Level Pelatihan
                                  </label>
                                  <select
                                    className="form-select form-select-sm"
                                    name="level_pelatihan"
                                    value={
                                      this.state.sellvlpelatihan?.value ?? ""
                                    }
                                    onChange={this.handleChangeLvlPelatihan}
                                  >
                                    <option
                                      value=""
                                      style={{ display: "none" }}
                                    >
                                      Silahkan pilih
                                    </option>
                                    {this.state.dataxlevelpelatihan &&
                                      this.state.dataxlevelpelatihan.map(
                                        (opt, index) => (
                                          <option
                                            key={`level_${index}`}
                                            value={opt.value}
                                          >
                                            {opt.label}
                                          </option>
                                        ),
                                      )}
                                  </select>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["level_pelatihan"]}
                                  </span>
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Sertifikasi
                                  </label>
                                  <div className="d-flex">
                                    {this.state.selSertifikasiList?.length >
                                      0 &&
                                      this.state.selSertifikasiList.map(
                                        (elem) => (
                                          <div
                                            key={elem.id}
                                            className="form-check form-check-sm form-check-custom form-check-solid me-5"
                                          >
                                            <input
                                              className="form-check-input"
                                              type="radio"
                                              name="sertifikasi"
                                              value={elem.id}
                                              id={"sertifikasi" + elem.id}
                                              onChange={
                                                this.handleChangeSertifikasi
                                              }
                                              defaultChecked={
                                                this.state.sertifikasi ==
                                                elem.id
                                                  ? "true"
                                                  : ""
                                              }
                                            />
                                            <label
                                              className="form-check-label"
                                              htmlFor={"sertifikasi" + elem.id}
                                            >
                                              {elem.name}
                                            </label>
                                          </div>
                                        ),
                                      )}
                                    {/* <div className="form-check form-check-sm form-check-custom form-check-solid me-5"> */}
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["sertifikasi"]}
                                  </span>
                                </div>

                                <div
                                  className="col-lg-12 mb-7 fv-row"
                                  style={{
                                    display:
                                      this.state.sertifikasi !== "" &&
                                      this.isSertifikasi(this.state.sertifikasi)
                                        ? ""
                                        : "none",
                                  }}
                                >
                                  <label className="form-label required">
                                    Institusi / LSP yang melakukan uji
                                    kompetensi
                                  </label>
                                  <select
                                    className="form-select form-select-sm"
                                    name="pelaksana_assesment"
                                    value={
                                      this.state.pelaksana_assesment_id ?? ""
                                    }
                                    onChange={
                                      this.handleChangePelaksanaAssesments
                                    }
                                  >
                                    <option
                                      value=""
                                      style={{ display: "none" }}
                                    >
                                      Silahkan pilih
                                    </option>
                                    {this.state.pelaksanaassesmentsList &&
                                      this.state.pelaksanaassesmentsList.map(
                                        (opt, index) => (
                                          <option
                                            key={`pelaksana_assesment_${index}`}
                                            value={opt.id}
                                          >
                                            {opt.value}
                                          </option>
                                        ),
                                      )}
                                  </select>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["pelaksana_assesment"]}
                                  </span>
                                </div>

                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Apakah Sertifikat Completion/Kelulusan
                                    mencantumkan nilai?
                                  </label>
                                  <div className="d-flex">
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="input_nilai"
                                        value={1}
                                        id="input_nilai1"
                                        onChange={this.handleChangeNilai}
                                        defaultChecked={
                                          this.state.input_nilai === 1
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="input_nilai1"
                                      >
                                        Iya
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="input_nilai"
                                        value={0}
                                        id="input_nilai2"
                                        onChange={this.handleChangeNilai}
                                        defaultChecked={
                                          this.state.input_nilai === 0
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="input_nilai2"
                                      >
                                        Tidak
                                      </label>
                                    </div>
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["input_nilai"]}
                                  </span>
                                </div>
                                {/* <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    LPJ Peserta
                                  </label>
                                  <div className="d-flex">
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="lpj_peserta"
                                        value="Iya"
                                        id="lpj_peserta1"
                                        onChange={this.handleChangeLpjPeserta}
                                        defaultChecked={
                                          this.state.lpj_peserta === "Iya"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="lpj_peserta1"
                                      >
                                        Iya
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="lpj_peserta"
                                        value="Tidak"
                                        id="lpj_peserta2"
                                        onChange={this.handleChangeLpjPeserta}
                                        defaultChecked={
                                          this.state.lpj_peserta === "Tidak"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="lpj_peserta2"
                                      >
                                        Tidak
                                      </label>
                                    </div>
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["lpj_peserta"]}
                                  </span>
                                </div> */}

                                <div>
                                  <div className="my-8 border-top mx-0"></div>
                                </div>
                                <h2 className="fs-5 text-muted mb-5">
                                  Pelaksanaan Pelatihan
                                </h2>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Kategori Pelaksanaan
                                  </label>
                                  <div className="d-flex">
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="metode_pelaksanaan"
                                        value="Swakelola"
                                        id="metode_pelaksanaan1"
                                        onClick={
                                          this.handleClickMetodePelaksanaan
                                        }
                                        defaultChecked={
                                          this.state.metode_pelaksanaan ==
                                          "Swakelola"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="metode_pelaksanaan1"
                                      >
                                        Swakelola
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="metode_pelaksanaan"
                                        value="Mitra"
                                        id="metode_pelaksanaan2"
                                        onClick={
                                          this.handleClickMetodePelaksanaan
                                        }
                                        defaultChecked={
                                          this.state.metode_pelaksanaan ==
                                          "Mitra"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="metode_pelaksanaan2"
                                      >
                                        Mitra
                                      </label>
                                    </div>
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["metode_pelaksanaan"]}
                                  </span>
                                </div>
                                <div
                                  className="col-lg-12 mb-7 fv-row"
                                  style={{
                                    display:
                                      this.state.metode_pelaksanaan == "Mitra"
                                        ? ""
                                        : "none",
                                  }}
                                >
                                  <label className="form-label required">
                                    Pilih Mitra
                                  </label>
                                  {/* <Select
                                        name="mitra"
                                        placeholder="Silahkan pilih"
                                        noOptionsMessage={({ inputValue }) =>
                                          !inputValue
                                            ? this.state.dataxmitra
                                            : "Data tidak tersedia"
                                        }
                                        className="form-select-sm selectpicker p-0"
                                        options={this.state.dataxmitra}
                                        value={this.state.selmitra}
                                        onChange={this.handleChangeMitra}
                                      /> */}
                                  <select
                                    className="form-select form-select-sm"
                                    name="mitra"
                                    value={this.state.selmitra?.value ?? ""}
                                    onChange={this.handleChangeMitra}
                                  >
                                    <option
                                      value=""
                                      style={{ display: "none" }}
                                    >
                                      Silahkan pilih
                                    </option>
                                    {this.state.dataxmitra &&
                                      this.state.dataxmitra.map(
                                        (opt, index) => (
                                          <option
                                            key={`mitra_${index}`}
                                            value={opt.value}
                                          >
                                            {opt.label}
                                          </option>
                                        ),
                                      )}
                                  </select>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["mitra"]}
                                  </span>
                                </div>
                                <div
                                  className="col-lg-12 mb-7 fv-row"
                                  style={{
                                    display:
                                      this.state.metode_pelaksanaan == ""
                                        ? "none"
                                        : "",
                                  }}
                                >
                                  <label className="form-label required">
                                    Pilih Penyelenggara
                                  </label>
                                  {/* <Select
                                      name="penyelenggara"
                                      placeholder="Silahkan pilih"
                                      noOptionsMessage={({ inputValue }) =>
                                        !inputValue
                                          ? this.state.dataxpenyelenggara
                                          : "Data tidak tersedia"
                                      }
                                      className="form-select-sm selectpicker p-0"
                                      options={this.state.dataxpenyelenggara}
                                      value={this.state.selpenyelenggara}
                                      onChange={this.handleChangePenyelenggara}
                                    /> */}
                                  <select
                                    className="form-select form-select-sm"
                                    name="penyelenggara"
                                    value={
                                      this.state.selpenyelenggara?.value ?? ""
                                    }
                                    onChange={this.handleChangePenyelenggara}
                                  >
                                    <option
                                      value=""
                                      style={{ display: "none" }}
                                    >
                                      Silahkan pilih
                                    </option>
                                    {this.state.dataxpenyelenggara &&
                                      this.state.dataxpenyelenggara.map(
                                        (opt, index) => (
                                          <option
                                            key={`penyelenggara_${index}`}
                                            value={opt.value}
                                          >
                                            {opt.label}
                                          </option>
                                        ),
                                      )}
                                  </select>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["penyelenggara"]}
                                  </span>
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Metode Pelatihan
                                  </label>
                                  <div className="d-flex">
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      {this.state.metode_pelatihan ===
                                      "Offline" ? (
                                        <input
                                          className="form-check-input"
                                          type="radio"
                                          name="metode_pelatihan"
                                          value="Offline"
                                          id="metode_pelatihan1"
                                          onClick={this.handleClickMetode}
                                          defaultChecked="true"
                                        />
                                      ) : (
                                        <input
                                          className="form-check-input"
                                          type="radio"
                                          name="metode_pelatihan"
                                          value="Offline"
                                          id="metode_pelatihan1"
                                          onClick={this.handleClickMetode}
                                        />
                                      )}
                                      <label
                                        className="form-check-label"
                                        htmlFor="metode_pelatihan1"
                                      >
                                        Offline
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      {this.state.metode_pelatihan ===
                                      "Online" ? (
                                        <input
                                          className="form-check-input"
                                          type="radio"
                                          name="metode_pelatihan"
                                          value="Online"
                                          id="metode_pelatihan2"
                                          onClick={this.handleClickMetode}
                                          defaultChecked="true"
                                        />
                                      ) : (
                                        <input
                                          className="form-check-input"
                                          type="radio"
                                          name="metode_pelatihan"
                                          value="Online"
                                          id="metode_pelatihan2"
                                          onClick={this.handleClickMetode}
                                        />
                                      )}
                                      <label
                                        className="form-check-label"
                                        htmlFor="metode_pelatihan2"
                                      >
                                        Online
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      {this.state.metode_pelatihan ===
                                      "Online & Offline" ? (
                                        <input
                                          className="form-check-input"
                                          type="radio"
                                          name="metode_pelatihan"
                                          value="Online & Offline"
                                          id="metode_pelatihan3"
                                          onClick={this.handleClickMetode}
                                          defaultChecked="true"
                                        />
                                      ) : (
                                        <input
                                          className="form-check-input"
                                          type="radio"
                                          name="metode_pelatihan"
                                          value="Online & Offline"
                                          id="metode_pelatihan3"
                                          onClick={this.handleClickMetode}
                                        />
                                      )}
                                      <label
                                        className="form-check-label"
                                        htmlFor="metode_pelatihan3"
                                      >
                                        Online & Offline
                                      </label>
                                    </div>
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["metode_pelatihan"]}
                                  </span>
                                </div>
                                <div
                                  style={{
                                    display:
                                      this.state.metode_pelatihan?.includes(
                                        "Online",
                                      )
                                        ? ""
                                        : "none",
                                  }}
                                >
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      URL Video Conferene
                                    </label>
                                    <input
                                      className="form-control form-control-sm"
                                      placeholder="Masukkan URL Vicon"
                                      name="url_vicon"
                                      defaultValue={this.state.url_vicon ?? ""}
                                      onBlur={this.handleChangeURL}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["url_vicon"]}
                                    </span>
                                  </div>
                                </div>
                                <div
                                  style={{
                                    display:
                                      this.state.metode_pelatihan?.includes(
                                        "Offline",
                                      )
                                        ? ""
                                        : "none",
                                  }}
                                >
                                  <h4 className="my-5 d-flex align-items-center">
                                    Lokasi Pelatihan
                                  </h4>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Provinsi
                                    </label>
                                    <select
                                      className="form-select form-select-sm"
                                      name="provinsi"
                                      value={
                                        this.state.selprovinsi?.value ?? ""
                                      }
                                      onChange={this.handleChangeProv}
                                    >
                                      <option
                                        value=""
                                        style={{ display: "none" }}
                                      >
                                        Silahkan pilih
                                      </option>
                                      {this.state.dataxprov &&
                                        this.state.dataxprov.map(
                                          (opt, index) => (
                                            <option
                                              key={`zonasi_${index}`}
                                              value={opt.value}
                                            >
                                              {opt.label}
                                            </option>
                                          ),
                                        )}
                                    </select>
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["provinsi"]}
                                    </span>
                                  </div>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Kota / Kabupaten
                                    </label>
                                    {/* <Select
                                        name="kota_kabupaten"
                                        placeholder="Silahkan pilih"
                                        noOptionsMessage={({ inputValue }) =>
                                          !inputValue
                                            ? this.state.dataxkab
                                            : "Data tidak tersedia"
                                        }
                                        className="form-select-sm selectpicker p-0"
                                        options={this.state.dataxkab}
                                        isDisabled={this.state.isDisabledKab}
                                        value={this.state.selkabupaten}
                                        onChange={this.handleChangeKabupaten}
                                      /> */}
                                    <select
                                      className="form-select form-select-sm"
                                      name="kota_kabupaten"
                                      value={
                                        this.state.selkabupaten?.value ?? ""
                                      }
                                      onChange={this.handleChangeKabupaten}
                                      disabled={
                                        this.state.dataxkab.length
                                          ? false
                                          : true
                                      }
                                    >
                                      <option
                                        value=""
                                        style={{ display: "none" }}
                                      >
                                        Silahkan pilih
                                      </option>
                                      {this.state.dataxkab &&
                                        this.state.dataxkab.map(
                                          (opt, index) => (
                                            <option
                                              key={`zonasi_${index}`}
                                              value={opt.value}
                                            >
                                              {opt.label}
                                            </option>
                                          ),
                                        )}
                                    </select>
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["kota_kabupaten"]}
                                    </span>
                                  </div>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Alamat
                                    </label>
                                    {/* <textarea className="form-control form-control-sm" placeholder="Masukkan alamat" name="alamat" value={this.state.fields["alamat"]}></textarea> */}
                                    <CKEditor
                                      editor={Editor}
                                      data={this.state.alamat}
                                      name="alamat"
                                      onReady={(editor) => {}}
                                      config={{
                                        ckfinder: {
                                          // Upload the images to the server using the CKFinder QuickUpload command.
                                          uploadUrl:
                                            process.env.REACT_APP_BASE_API_URI +
                                            "/publikasi/ckeditor-upload-image",
                                        },
                                      }}
                                      onBlur={(event, editor) => {
                                        const data = editor.getData();
                                        this.handleChangeAlamat(data);
                                      }}
                                      onFocus={(event, editor) => {}}
                                    />
                                    <input type="hidden" name="alamat_hidden" />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["alamat"]}
                                    </span>
                                  </div>
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Klasifikasi Peserta
                                  </label>
                                  <div className="d-flex">
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="checkbox"
                                        name="disabilitas"
                                        value={1}
                                        id="umum"
                                        onChange={
                                          this.handleChangeDisabilitasUmum
                                        }
                                        defaultChecked={
                                          this.state.disabilitas_umum === 1
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="umum"
                                      >
                                        Umum
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="checkbox"
                                        name="disabilitas"
                                        value={1}
                                        id="tuna_netra"
                                        onChange={
                                          this.handleChangeDisabilitasTunaNetra
                                        }
                                        defaultChecked={
                                          this.state.disabilitas_tuna_netra ===
                                          1
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="tuna_netra"
                                      >
                                        Tuna Netra
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="checkbox"
                                        name="disabilitas"
                                        value={1}
                                        id="tuna_rungu"
                                        onChange={
                                          this.handleChangeDisabilitasTunaRungu
                                        }
                                        defaultChecked={
                                          this.state.disabilitas_tuna_rungu ===
                                          1
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="tuna_rungu"
                                      >
                                        Tuna Rungu
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      <input
                                        className="form-check-input"
                                        type="checkbox"
                                        name="disabilitas"
                                        value="Tuna Daksa"
                                        id="tuna_daksa"
                                        onChange={
                                          this.handleChangeDisabilitasTunaDaksa
                                        }
                                        defaultChecked={
                                          this.state.disabilitas_tuna_daksa ===
                                          1
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="tuna_daksa"
                                      >
                                        Tuna Daksa
                                      </label>
                                    </div>
                                  </div>
                                  <input
                                    type="hidden"
                                    name="disabilitas_hidden"
                                  />
                                </div>
                                {/* <div className="col-lg-12">
                                    <div className="mb-7 fv-row">
                                      <label className="form-label">Upload Logo (Optional)</label>
                                      <input type="file" className="form-control form-control-sm mb-2" name="upload_logo" accept=".png, .jpg, .jpeg, .svg" />
                                      <small className="text-muted">Format Image (.png/.jpg/.jpeg/.svg), Max 2048</small>
                                    </div>
                                  </div>
                                  <div className="col-lg-12">
                                    <div className="mb-7 fv-row">
                                      <label className="form-label">Upload Thumbnail (Optional)</label>
                                      <input type="file" className="form-control form-control-sm mb-2" name="upload_thumbnail" accept=".png, .jpg, .jpeg, .svg" />
                                      <small className="text-muted">Format Image (.png/.jpg/.jpeg/.svg), Max 2048</small>
                                    </div>
                                  </div> */}
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Zonasi
                                  </label>
                                  {/* <Select
                                      name="zonasi"
                                      placeholder="Silahkan pilih"
                                      noOptionsMessage={({ inputValue }) =>
                                        !inputValue
                                          ? this.state.dataxzonasi
                                          : "Data tidak tersedia"
                                      }
                                      className="form-select-sm selectpicker p-0"
                                      options={this.state.dataxzonasi}
                                      value={this.state.selzonasi}
                                      onChange={this.handleChangeZonasi}
                                    /> */}
                                  <select
                                    className="form-select form-select-sm"
                                    name="zonasi"
                                    value={this.state.selzonasi?.value ?? ""}
                                    onChange={this.handleChangeZonasi}
                                  >
                                    <option
                                      value=""
                                      style={{ display: "none" }}
                                    >
                                      Silahkan pilih
                                    </option>
                                    {this.state.dataxzonasi &&
                                      this.state.dataxzonasi.map(
                                        (opt, index) => (
                                          <option
                                            key={`zonasi_${index}`}
                                            value={opt.value}
                                          >
                                            {opt.label}
                                          </option>
                                        ),
                                      )}
                                  </select>
                                  <span className="text-muted">
                                    Jika terbuka untuk seluruh Indonesia, Pilih
                                    Zonasi Nasional
                                  </span>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["zonasi"]}
                                  </span>
                                </div>
                                <div>
                                  <div className="my-8 border-top mx-0"></div>
                                </div>
                                <h2 className="fs-5 text-muted mb-3">
                                  Kuota Pelatihan
                                </h2>
                                {this.state.isKuotaPendaftarVisible ? (
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label required">
                                      Target Kuota Pendaftar
                                    </label>
                                    <input
                                      id="kuota_pendaftar"
                                      className="form-control form-control-sm"
                                      placeholder="Masukkan target kuota pendaftar"
                                      name="kuota_target_pendaftar"
                                      defaultValue={
                                        this.state.kuotapendaftar ?? ""
                                      }
                                      onBlur={this.handleKuotaPendaftar}
                                    />
                                    <span style={{ color: "red" }}>
                                      {
                                        this.state.errors[
                                          "kuota_target_pendaftar"
                                        ]
                                      }
                                    </span>
                                  </div>
                                ) : null}
                                <div className="col-lg-6 mb-7 fv-row">
                                  <label className="form-label required">
                                    Target Kuota Peserta
                                  </label>
                                  <input
                                    onBlur={this.handleKuotaPeserta}
                                    className="form-control form-control-sm"
                                    placeholder="Masukkan target kuota peserta"
                                    name="kuota_target_peserta"
                                    defaultValue={this.state.kuotapeserta ?? ""}
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["kuota_target_peserta"]}
                                  </span>
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Status Kuota
                                  </label>
                                  <div className="d-flex">
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="status_kuota"
                                        value="Available"
                                        id="status_kuota1"
                                        onChange={this.handleChangeStatusKuota}
                                        defaultChecked={
                                          this.state.status_kuota ===
                                          "Available"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="status_kuota1"
                                      >
                                        Available
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="status_kuota"
                                        value="Full"
                                        id="status_kuota2"
                                        onChange={this.handleChangeStatusKuota}
                                        defaultChecked={
                                          this.state.status_kuota === "Full"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="status_kuota2"
                                      >
                                        Full
                                      </label>
                                    </div>
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["status_kuota"]}
                                  </span>
                                </div>
                              </div>
                            </div>

                            <div
                              className="flex-column"
                              data-kt-stepper-element="content"
                            >
                              <div className="row mt-7">
                                <h2 className="fs-5 text-muted mb-5">
                                  Alur Seleksi
                                  <label
                                    htmlFor="form-label"
                                    className="required"
                                  ></label>
                                </h2>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <div className="mb-0">
                                    <label className="d-flex flex-stack mb-5 cursor-pointer">
                                      <span className="d-flex align-items-center me-2">
                                        <span className="symbol symbol-50px me-6">
                                          <span className="symbol-label">
                                            <span className="svg-icon svg-icon-1 svg-icon-gray-600">
                                              <i className="fa fa-file-signature fa-2x"></i>
                                            </span>
                                          </span>
                                        </span>
                                        <span className="d-flex flex-column">
                                          <span className="fw-bolder text-gray-800 text-hover-primary fs-7">
                                            Administrasi
                                          </span>
                                          <span className="fs-7 fw-bold text-muted">
                                            Seleksi pelatihan hanya berdasarkan
                                            administrasi/berkas pendaftaran
                                          </span>
                                        </span>
                                      </span>
                                      <span className="form-check form-check-custom form-check-solid">
                                        <input
                                          className="form-check-input"
                                          type="radio"
                                          name="alur_pendaftaran"
                                          value="Administrasi"
                                          onChange={
                                            this.handleChangeAlurPendaftaran
                                          }
                                          checked={
                                            this.state.alurPendaftaran ===
                                            "Administrasi"
                                          }
                                        />
                                      </span>
                                    </label>
                                    <label className="d-flex flex-stack mb-5 cursor-pointer">
                                      <span className="d-flex align-items-center me-2">
                                        <span className="symbol symbol-50px me-6">
                                          <span className="symbol-label">
                                            <span className="svg-icon svg-icon-1 svg-icon-gray-600">
                                              <i className="fa fa-clipboard-list fa-2x"></i>
                                            </span>
                                          </span>
                                        </span>
                                        <span className="d-flex flex-column">
                                          <span className="fw-bolder text-gray-800 text-hover-primary fs-7">
                                            Administrasi{" "}
                                            <i className="bi bi-chevron-right mx-2"></i>{" "}
                                            Test Substansi
                                          </span>
                                          <span className="fs-7 fw-bold text-muted">
                                            Setelah Seleksi administrasi akan
                                            diadakan test substansi bagi peserta
                                            yang lolos
                                          </span>
                                        </span>
                                      </span>
                                      <span className="form-check form-check-custom form-check-solid">
                                        <input
                                          className="form-check-input"
                                          type="radio"
                                          name="alur_pendaftaran"
                                          value="Administrasi - Test Substansi"
                                          onChange={
                                            this.handleChangeAlurPendaftaran
                                          }
                                          checked={
                                            this.state.alurPendaftaran ===
                                            "Administrasi - Test Substansi"
                                          }
                                        />
                                      </span>
                                    </label>
                                    <label className="d-flex flex-stack mb-5 cursor-pointer">
                                      <span className="d-flex align-items-center me-2">
                                        <span className="symbol symbol-50px me-6">
                                          <span className="symbol-label">
                                            <span className="svg-icon svg-icon-1 svg-icon-gray-600">
                                              <i className="fa fa-clipboard-check fa-2x"></i>
                                            </span>
                                          </span>
                                        </span>
                                        <span className="d-flex flex-column">
                                          <span className="fw-bolder text-gray-800 text-hover-primary fs-7">
                                            Test Substansi{" "}
                                            <i className="bi bi-chevron-right mx-2"></i>{" "}
                                            Administrasi
                                          </span>
                                          <span className="fs-7 fw-bold text-muted">
                                            Setelah melakukan pendaftaran
                                            peserta langsung mengerjakan Test
                                            Substansi, lalu dilakukan seleksi
                                            Administrasi
                                          </span>
                                        </span>
                                      </span>
                                      <span className="form-check form-check-custom form-check-solid">
                                        <input
                                          className="form-check-input"
                                          type="radio"
                                          name="alur_pendaftaran"
                                          value="Test Substansi - Administrasi"
                                          onChange={
                                            this.handleChangeAlurPendaftaran
                                          }
                                          checked={
                                            this.state.alurPendaftaran ===
                                            "Test Substansi - Administrasi"
                                          }
                                        />
                                      </span>
                                    </label>
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["alur_pendaftaran"]}
                                  </span>
                                </div>
                                <div>
                                  <div className="my-8 border-top mx-0"></div>
                                </div>
                                <h2 className="fs-5 text-muted mb-5">
                                  Jadwal Pendaftaran dan Pelatihan
                                </h2>
                                <div className="col-lg-6 mb-7 fv-row">
                                  <label className="form-label required">
                                    Tgl. Mulai Pendaftaran
                                  </label>
                                  <Flatpickr
                                    options={{
                                      locale: Indonesian,
                                      dateFormat: "d F Y H:i",
                                      enableTime: "true",
                                      time_24hr: true,
                                      minDate: "",
                                    }}
                                    className="form-control form-control-sm"
                                    placeholder="Masukkan tanggal pendaftaran"
                                    name="pendaftaran_tanggal_mulai"
                                    value={this.state.pendaftaranTanggalMulai}
                                    onChange={(value, dateString) => {
                                      if (
                                        this.state.alurPendaftaran ==
                                        "Test Substansi - Administrasi"
                                      ) {
                                        this.setState({
                                          testsubstansiTanggalMulai: value[0],
                                        });
                                      }
                                      this.setState({
                                        pendaftaranTanggalMulai: value[0],
                                      });
                                    }}
                                  />
                                  <span style={{ color: "red" }}>
                                    {
                                      this.state.errors[
                                        "pendaftaran_tanggal_mulai"
                                      ]
                                    }
                                  </span>
                                </div>
                                <div className="col-lg-6 mb-7 fv-row">
                                  <label className="form-label required">
                                    Tgl Selesai Pendaftaran
                                  </label>

                                  <Flatpickr
                                    options={{
                                      locale: Indonesian,
                                      dateFormat: "d F Y H:i",
                                      enableTime: "true",
                                      time_24hr: true,
                                      minDate:
                                        this.state.pendaftaranTanggalMulai ??
                                        "",
                                    }}
                                    className="form-control form-control-sm"
                                    placeholder="Masukkan tanggal selesai pendaftaran"
                                    name="pendaftaran_tanggal_selesai"
                                    value={this.state.pendaftaranTanggalSelesai}
                                    onChange={(value) => {
                                      if (
                                        this.state.alurPendaftaran ==
                                        "Test Substansi - Administrasi"
                                      ) {
                                        this.setState({
                                          testsubstansiTanggalSelesai: value[0],
                                        });
                                      }
                                      this.setState({
                                        pendaftaranTanggalSelesai: value[0],
                                      });
                                    }}
                                  />
                                  <span style={{ color: "red" }}>
                                    {
                                      this.state.errors[
                                        "pendaftaran_tanggal_selesai"
                                      ]
                                    }
                                  </span>
                                </div>
                                <div className="col-lg-6 mb-7 fv-row">
                                  <label className="form-label required">
                                    Tgl. Mulai Pelatihan
                                  </label>
                                  <Flatpickr
                                    options={{
                                      locale: Indonesian,
                                      dateFormat: "d F Y H:i",
                                      time_24hr: true,
                                      enableTime: "true",
                                      minDate:
                                        this.state.pendaftaranTanggalSelesai ??
                                        "",
                                    }}
                                    className="form-control form-control-sm"
                                    placeholder="Masukkan tanggal pelatihan"
                                    name="pelatihan_tanggal_mulai"
                                    value={this.state.pelatihanTanggalMulai}
                                    onChange={(value) => {
                                      this.setState({
                                        pelatihanTanggalMulai: value[0],
                                      });
                                    }}
                                  />
                                  <span style={{ color: "red" }}>
                                    {
                                      this.state.errors[
                                        "pelatihan_tanggal_mulai"
                                      ]
                                    }
                                  </span>
                                </div>
                                <div className="col-lg-6 mb-7 fv-row">
                                  <label className="form-label required">
                                    Tgl. Selesai Pelatihan
                                  </label>
                                  <Flatpickr
                                    options={{
                                      locale: Indonesian,
                                      dateFormat: "d F Y H:i",
                                      time_24hr: true,
                                      enableTime: "true",
                                      minDate:
                                        this.state.pelatihanTanggalMulai ?? "",
                                    }}
                                    className="form-control form-control-sm"
                                    placeholder="Masukkan tanggal pelatihan"
                                    name="pelatihan_tanggal_selesai"
                                    value={this.state.pelatihanTanggalSelesai}
                                    onChange={(value) => {
                                      this.setState({
                                        pelatihanTanggalSelesai: value[0],
                                      });
                                    }}
                                  />
                                  <span style={{ color: "red" }}>
                                    {
                                      this.state.errors[
                                        "pelatihan_tanggal_selesai"
                                      ]
                                    }
                                  </span>
                                </div>

                                <div
                                  style={{
                                    display: this.state
                                      .showingAlurPendaftaranDate
                                      ? ""
                                      : "none",
                                  }}
                                >
                                  <div>
                                    <div className="my-8 border-top mx-0"></div>
                                  </div>
                                  <h2 className="fs-5 text-muted mb-5">
                                    Seleksi Administrasi & Test Substansi
                                  </h2>
                                  {/* Administrasi - Test */}
                                  <div
                                    className="row"
                                    style={{
                                      display:
                                        this.state.alurPendaftaran ==
                                        "Administrasi - Test Substansi"
                                          ? ""
                                          : "none",
                                    }}
                                  >
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Tgl. Mulai Administrasi
                                      </label>
                                      <Flatpickr
                                        options={{
                                          locale: Indonesian,
                                          dateFormat: "d F Y H:i",
                                          time_24hr: true,
                                          enableTime: "true",
                                          minDate:
                                            this.state
                                              .pendaftaranTanggalMulai ?? "",
                                          maxDate: this.state
                                            .pelatihanTanggalMulai
                                            ? fp_inc(
                                                new Date(
                                                  this.state.pelatihanTanggalMulai,
                                                ),
                                                -1,
                                              )
                                            : "",
                                        }}
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan tanggal mulai administrasi"
                                        name="adm_test_administrasi_tanggal_mulai"
                                        value={
                                          this.state.administrasiTanggalMulai
                                        }
                                        onChange={(value) => {
                                          this.setState({
                                            administrasiTanggalMulai: value[0],
                                          });
                                        }}
                                      />
                                      <span style={{ color: "red" }}>
                                        {
                                          this.state.errors[
                                            "adm_test_administrasi_tanggal_mulai"
                                          ]
                                        }
                                      </span>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Tgl. Selesai Administrasi
                                      </label>
                                      <Flatpickr
                                        options={{
                                          locale: Indonesian,
                                          dateFormat: "d F Y H:i",
                                          time_24hr: true,
                                          enableTime: "true",
                                          minDate:
                                            this.state
                                              .administrasiTanggalMulai ?? "",
                                          maxDate: this.state
                                            .pelatihanTanggalMulai
                                            ? fp_inc(
                                                new Date(
                                                  this.state.pelatihanTanggalMulai,
                                                ),
                                                -1,
                                              )
                                            : "",
                                        }}
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan tanggal selesai administrasi"
                                        name="adm_test_administrasi_tanggal_selesai"
                                        value={
                                          this.state.administrasiTanggalSelesai
                                        }
                                        onChange={(value) => {
                                          this.setState({
                                            administrasiTanggalSelesai:
                                              value[0],
                                          });
                                        }}
                                      />
                                      <span style={{ color: "red" }}>
                                        {
                                          this.state.errors[
                                            "adm_test_administrasi_tanggal_selesai"
                                          ]
                                        }
                                      </span>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Tgl. Mulai Test Substansi
                                      </label>
                                      <Flatpickr
                                        options={{
                                          locale: Indonesian,
                                          dateFormat: "d F Y H:i",
                                          time_24hr: true,
                                          enableTime: "true",
                                          minDate:
                                            this.state
                                              .administrasiTanggalSelesai ?? "",
                                          maxDate:
                                            this.state.pelatihanTanggalMulai,
                                          // ? fp_inc(
                                          //     new Date(
                                          //       this.state.pelatihanTanggalMulai
                                          //     ),
                                          //     -1
                                          //   )
                                          // : "",
                                        }}
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan tanggal mulai test substansi"
                                        name="adm_test_testsubstansi_tanggal_mulai"
                                        value={
                                          this.state.testsubstansiTanggalMulai
                                        }
                                        onChange={(value) => {
                                          this.setState({
                                            testsubstansiTanggalMulai: value[0],
                                          });
                                        }}
                                      />
                                      <span style={{ color: "red" }}>
                                        {
                                          this.state.errors[
                                            "adm_test_testsubstansi_tanggal_mulai"
                                          ]
                                        }
                                      </span>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Tgl. Selesai Test Substansi
                                      </label>
                                      <Flatpickr
                                        options={{
                                          locale: Indonesian,
                                          dateFormat: "d F Y H:i",
                                          time_24hr: true,
                                          enableTime: "true",
                                          minDate:
                                            this.state
                                              .testsubstansiTanggalMulai ?? "",
                                          maxDate:
                                            this.state.pelatihanTanggalMulai,
                                        }}
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan tanggal mulai test substansi"
                                        name="adm_test_testsubstansi_tanggal_selesai"
                                        value={
                                          this.state.testsubstansiTanggalSelesai
                                        }
                                        onChange={(value) => {
                                          this.setState({
                                            testsubstansiTanggalSelesai:
                                              value[0],
                                          });
                                        }}
                                      />
                                      <span style={{ color: "red" }}>
                                        {
                                          this.state.errors[
                                            "adm_test_testsubstansi_tanggal_selesai"
                                          ]
                                        }
                                      </span>
                                    </div>
                                  </div>

                                  <div
                                    className="row"
                                    style={{
                                      display:
                                        this.state.alurPendaftaran ==
                                        "Test Substansi - Administrasi"
                                          ? ""
                                          : "none",
                                    }}
                                  >
                                    {/* Test substansi - administrasi */}
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Tgl. Mulai Test Substansi
                                      </label>
                                      <Flatpickr
                                        disabled
                                        options={{
                                          locale: Indonesian,
                                          dateFormat: "d F Y H:i",
                                          time_24hr: true,
                                          enableTime: "true",
                                        }}
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan tanggal mulai test substansi"
                                        name="test_adm_testsubstansi_tanggal_mulai"
                                        value={
                                          this.state.testsubstansiTanggalMulai
                                        }
                                        onChange={(value) => {
                                          this.setState({
                                            testsubstansiTanggalMulai: value[0],
                                          });
                                        }}
                                      />
                                      <span style={{ color: "red" }}>
                                        {
                                          this.state.errors[
                                            "test_adm_testsubstansi_tanggal_mulai"
                                          ]
                                        }
                                      </span>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Tgl. Selesai Test Substansi
                                      </label>
                                      <Flatpickr
                                        disabled
                                        options={{
                                          locale: Indonesian,
                                          dateFormat: "d F Y H:i",
                                          time_24hr: true,
                                          enableTime: "true",
                                          minDate:
                                            this.state
                                              .testsubstansiTanggalMulai ?? "",
                                          maxDate:
                                            this.state.pelatihanTanggalMulai,
                                          // ? fp_inc(
                                          //     new Date(
                                          //       this.state.pelatihanTanggalMulai
                                          //     ),
                                          //     -1
                                          //   )
                                          // : "",
                                        }}
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan tanggal selesai test substansi"
                                        name="test_adm_testsubstansi_tanggal_selesai"
                                        value={
                                          this.state.testsubstansiTanggalSelesai
                                        }
                                        onChange={(value) => {
                                          this.setState({
                                            testsubstansiTanggalSelesai:
                                              value[0],
                                          });
                                        }}
                                      />
                                      <span style={{ color: "red" }}>
                                        {
                                          this.state.errors[
                                            "test_adm_testsubstansi_tanggal_selesai"
                                          ]
                                        }
                                      </span>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Tgl. Mulai Seleksi Administrasi
                                      </label>
                                      <Flatpickr
                                        options={{
                                          locale: Indonesian,
                                          dateFormat: "d F Y H:i",
                                          time_24hr: true,
                                          enableTime: "true",
                                          minDate:
                                            this.state
                                              .pendaftaranTanggalMulai ?? "",
                                          maxDate:
                                            this.state.pelatihanTanggalMulai ??
                                            "",
                                        }}
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan tanggal mulai administrasi"
                                        name="test_adm_administrasi_tanggal_mulai"
                                        value={
                                          this.state.administrasiTanggalMulai
                                        }
                                        onChange={(value) => {
                                          this.setState({
                                            administrasiTanggalMulai: value[0],
                                          });
                                        }}
                                      />
                                      <span style={{ color: "red" }}>
                                        {
                                          this.state.errors[
                                            "test_adm_administrasi_tanggal_mulai"
                                          ]
                                        }
                                      </span>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Tgl. Selesai Seleksi Administrasi
                                      </label>
                                      <Flatpickr
                                        options={{
                                          locale: Indonesian,
                                          dateFormat: "d F Y H:i",
                                          time_24hr: true,
                                          enableTime: "true",
                                          minDate:
                                            this.state
                                              .administrasiTanggalMulai ?? "",
                                          maxDate:
                                            this.state.pelatihanTanggalMulai ??
                                            "",
                                        }}
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan tanggal selesai administrasi"
                                        name="test_adm_administrasi_tanggal_selesai"
                                        value={
                                          this.state.administrasiTanggalSelesai
                                        }
                                        onChange={(value) => {
                                          this.setState({
                                            administrasiTanggalSelesai:
                                              value[0],
                                          });
                                        }}
                                      />
                                      <span style={{ color: "red" }}>
                                        {
                                          this.state.errors[
                                            "test_adm_administrasi_tanggal_selesai"
                                          ]
                                        }
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Apakah ada Mid Test?
                                  </label>
                                  <div className="d-flex">
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="apakah_mid_test"
                                        value="Iya"
                                        id="apakah_mid_test1"
                                        onChange={
                                          this.handleChangeApakahMidTest
                                        }
                                        // checked={this.showingMidTestDate}
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="apakah_mid_test1"
                                      >
                                        Iya
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        name="apakah_mid_test"
                                        value="Tidak"
                                        id="apakah_mid_test2"
                                        onChange={
                                          this.handleChangeApakahMidTest
                                        }
                                        // checked={!this.showingMidTestDate}
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="apakah_mid_test2"
                                      >
                                        Tidak
                                      </label>
                                    </div>
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["apakah_mid_test"]}
                                  </span>
                                </div>
                                <div
                                  style={{
                                    display: this.state.showingMidTestDate
                                      ? ""
                                      : "none",
                                  }}
                                >
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Tgl. Mid Test
                                    </label>
                                    {/* <input
                                            id="datetime9"
                                            className="form-control form-control-sm"
                                            placeholder="Masukkan tanggal mid test"
                                            name="midtest_tanggal"
                                          /> */}
                                    <Flatpickr
                                      options={{
                                        locale: Indonesian,
                                        dateFormat: "d F Y H:i",
                                        time_24hr: true,
                                        enableTime: "true",
                                        minDate:
                                          this.state.pelatihanTanggalMulai ??
                                          "today",
                                        maxDate:
                                          this.state.pelatihanTanggalSelesai ??
                                          "",
                                      }}
                                      className="form-control form-control-sm"
                                      placeholder="Masukkan tanggal mid test"
                                      name="midtest_tanggal"
                                      value={this.state.midTestTanggal}
                                      onChange={(value) => {
                                        this.setState({
                                          midTestTanggal: value[0],
                                        });
                                      }}
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["midtest_tanggal"]}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div
                              className="flex-column"
                              data-kt-stepper-element="content"
                            >
                              <div className="row mt-7">
                                {/* {this.state.showOptionButtonSilabus && (
                                  <div id="btn_choose_form" className="row">
                                    <div className="col-lg-6">
                                      <a
                                        className="btn btn-outline btn-outline-dashed btn-outline-default btn-hover-rise p-7 d-flex align-items-center my-7"
                                        onClick={this.handleClickSelectSilabus}
                                      >
                                        <span className="svg-icon svg-icon-3x">
                                          <svg
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                            xmlns="http://www.w3.org/2000/svg"
                                            className="mh-50px"
                                          >
                                            <path
                                              opacity="0.3"
                                              d="M14 2H6C4.89543 2 4 2.89543 4 4V20C4 21.1046 4.89543 22 6 22H18C19.1046 22 20 21.1046 20 20V8L14 2Z"
                                              fill="black"
                                            ></path>
                                            <path
                                              d="M20 8L14 2V6C14 7.10457 14.8954 8 16 8H20Z"
                                              fill="black"
                                            ></path>
                                            <rect
                                              x="13.6993"
                                              y="13.6656"
                                              width="4.42828"
                                              height="1.73089"
                                              rx="0.865447"
                                              transform="rotate(45 13.6993 13.6656)"
                                              fill="black"
                                            ></rect>
                                            <path
                                              d="M15 12C15 14.2 13.2 16 11 16C8.8 16 7 14.2 7 12C7 9.8 8.8 8 11 8C13.2 8 15 9.8 15 12ZM11 9.6C9.68 9.6 8.6 10.68 8.6 12C8.6 13.32 9.68 14.4 11 14.4C12.32 14.4 13.4 13.32 13.4 12C13.4 10.68 12.32 9.6 11 9.6Z"
                                              fill="black"
                                            ></path>
                                          </svg>
                                        </span>
                                        <span className="d-block fw-bold text-start">
                                          <span className="text-dark fw-bolder d-block fs-4 mb-2">
                                            Master Silabus
                                          </span>
                                          <span className="text-gray-400 fw-bold fs-6">
                                            Pilih silabus dari master silabus.
                                          </span>
                                        </span>
                                      </a>
                                    </div>
                                    <div className="col-lg-6">
                                      <a
                                        className={`btn btn-outline btn-outline-dashed btn-outline-default btn-hover-rise p-7 d-flex align-items-center my-7 ${
                                          isExceptionRole() ? "" : "disabled"
                                        }`}
                                        onClick={this.handleClickCreateSilabus}
                                      >
                                        <span className="svg-icon svg-icon-3x ms-n1">
                                          <svg
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                            xmlns="http://www.w3.org/2000/svg"
                                            className="mh-50px"
                                          >
                                            <path
                                              opacity="0.3"
                                              d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM16 13.5L12.5 13V10C12.5 9.4 12.6 9.5 12 9.5C11.4 9.5 11.5 9.4 11.5 10L11 13L8 13.5C7.4 13.5 7 13.4 7 14C7 14.6 7.4 14.5 8 14.5H11V18C11 18.6 11.4 19 12 19C12.6 19 12.5 18.6 12.5 18V14.5L16 14C16.6 14 17 14.6 17 14C17 13.4 16.6 13.5 16 13.5Z"
                                              fill="black"
                                            ></path>
                                            <rect
                                              x="11"
                                              y="19"
                                              width="10"
                                              height="2"
                                              rx="1"
                                              transform="rotate(-90 11 19)"
                                              fill="black"
                                            ></rect>
                                            <rect
                                              x="7"
                                              y="13"
                                              width="10"
                                              height="2"
                                              rx="1"
                                              fill="black"
                                            ></rect>
                                            <path
                                              d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z"
                                              fill="black"
                                            ></path>
                                          </svg>
                                        </span>
                                        <span className="d-block fw-bold text-start">
                                          <span className="text-dark fw-bolder d-block fs-4 mb-2">
                                            Buat Silabus
                                          </span>
                                          <span className="text-gray-400 fw-bold fs-6">
                                            Buat silabus pelatihan.
                                          </span>
                                        </span>
                                      </a>
                                    </div>
                                  </div>
                                )} */}
                                <div
                                  id="div_selsilabus"
                                  style={{
                                    display: this.state.showingSelSilabus
                                      ? ""
                                      : "none",
                                  }}
                                >
                                  <div className="row">
                                    <div className="col-lg-12 mb-4">
                                      <div className="d-flex justify-content-between align-items-start flex-wrap pt-6">
                                        <div className="d-flex flex-column">
                                          <h2 className="fs-5 text-muted">
                                            Silabus
                                          </h2>
                                        </div>
                                        <div className="d-flex">
                                          <a
                                            href="#"
                                            className="btn btn-secondary btn-sm me-2"
                                            onClick={
                                              this.handleClickRefSilabusForm
                                            }
                                            title="Refresh silabus"
                                          >
                                            <span className="svg-icon m-0">
                                              <i className="bi bi-arrow-clockwise"></i>
                                            </span>
                                            Refresh
                                          </a>
                                          {/* <a
                                            href="#"
                                            className="btn btn-secondary btn-sm me-2"
                                            onClick={(e) => {
                                              this.setState({
                                                showingSelSilabus: false,
                                                showViewSilabus: false,
                                                showOptionButtonSilabus: true,
                                                showBtnNextW2: false,
                                                selnamasilabus: {},
                                                detailSilabus: {
                                                  id: "",
                                                  isi_silabus: "",
                                                  jp_silabus: "",
                                                },
                                              });
                                            }}
                                          >
                                            Batal
                                          </a> */}
                                        </div>
                                      </div>
                                    </div>
                                    <div className="bg-light-warning col-12 text-warning mx-3 mb-5 fw-semibold rounded p-5 fs-7">
                                      Silabus akan terhubung dengan master
                                      referensi. Apabila silabus yang menajdi
                                      referensi diubah maka silabus pelatihan
                                      ini akan ikut berubah.
                                    </div>
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label required">
                                        Nama Silabus
                                      </label>
                                      <select
                                        className="form-select form-select-sm"
                                        name="select_silabus"
                                        value={
                                          this.state.selnamasilabus?.value ?? ""
                                        }
                                        onChange={this.handleChangeSilabus}
                                        disabled={
                                          this.state.dataxselusesilabus.length
                                            ? false
                                            : true
                                        }
                                      >
                                        <option
                                          value=""
                                          style={{ display: "none" }}
                                        >
                                          Silahkan pilih
                                        </option>
                                        {this.state.dataxselusesilabus &&
                                          this.state.dataxselusesilabus.map(
                                            (opt, index) => (
                                              <option
                                                key={`usesilabus_${index}`}
                                                value={opt.value}
                                              >
                                                {opt.value} - {opt.label}
                                              </option>
                                            ),
                                          )}
                                      </select>
                                      <br />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["judul_form"]}
                                      </span>
                                    </div>
                                  </div>
                                  <div
                                    style={{
                                      display: this.state.showViewSilabus
                                        ? "block"
                                        : "none",
                                    }}
                                    className="col-lg-12 mb-7 fv-row"
                                  >
                                    <div className="mr-3 mb-5">
                                      {this.state.detailSilabus && (
                                        <h5
                                          className="modal-title mt-5 mb-6"
                                          id="preview_title"
                                        >
                                          {this.state.detailSilabus.nama}
                                          <span className="text-muted d-block small">
                                            ID : {this.state.detailSilabus.id} |
                                            Total :{" "}
                                            {` ${zeroDecimalValue(
                                              this.state.detailSilabus.totalJp,
                                            )} Jam Pelajaran`}
                                          </span>
                                        </h5>
                                      )}
                                    </div>
                                    <div className="mr-3">
                                      <ul className="list-group list-group-numbered">
                                        {this.state.detailSilabus.isiSilabus &&
                                          this.state.detailSilabus.isiSilabus
                                            .length > 0 &&
                                          this.state.detailSilabus.isiSilabus.map(
                                            (elem, index) => (
                                              <li
                                                key={"silabus_" + index}
                                                className="list-group-item d-flex justify-content-between align-items-start"
                                              >
                                                <div className="ms-2 me-auto">
                                                  <div className="fw-bold">
                                                    {elem.materi}
                                                  </div>
                                                  Jam pelajaran:
                                                  {zeroDecimalValue(
                                                    elem.jam_pelajaran,
                                                  )}
                                                </div>
                                              </li>
                                            ),
                                          )}
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                                {/* <div
                                  id="div_createsilabus"
                                  className="col-lg-12 mb-7 fv-row pt-0"
                                  style={{
                                    display: this.state.showingCreateSilabus
                                      ? ""
                                      : "none",
                                  }}
                                >
                                  <div className="row">
                                    <div className="col-lg-12 mb-4">
                                      <div className="d-flex justify-content-between align-items-start flex-wrap pt-6">
                                        <div className="d-flex flex-column">
                                          <h2 className="fs-5 text-muted">
                                            Silabus
                                          </h2>
                                        </div>
                                        <div className="d-flex">
                                          <a
                                            href="#"
                                            className="btn btn-secondary btn-sm ms-2"
                                            onClick={(e) => {
                                              this.setState({
                                                showingCreateSilabus: false,
                                                showViewSilabus: false,
                                                showOptionButtonSilabus: true,
                                                showBtnNextW2: false,
                                                selnamasilabus: {},
                                                detailSilabus: {
                                                  id: "",
                                                  isi_silabus: "",
                                                  jp_silabus: "",
                                                },
                                                silabusList: [
                                                  {
                                                    id: 1,
                                                    isi_silabus: "",
                                                    jp_silabus: "",
                                                  },
                                                ],
                                              });
                                            }}
                                          >
                                            Batal
                                          </a>
                                        </div>
                                      </div>
                                    </div>

                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label required">
                                        Nama Silabus
                                      </label>
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan nama silabus"
                                        name="nama_silabus"
                                        onBlur={(e) => {
                                          this.setState({
                                            selnamasilabus: e.target.value,
                                          });
                                        }}
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["nama_silabus"]}
                                      </span>
                                    </div>
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label required">
                                        Total Jam Pelajaran (JP)
                                      </label>
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan total jam pelajaran"
                                        onBlur={(e) => {
                                          this.setState({
                                            seltotaljpsilabus: e.target.value,
                                          });
                                        }}
                                        onChange={(e) => {
                                          numericOnly(e.target);
                                        }}
                                        id="total_jp"
                                        name="total_jp"
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["total_jp"]}
                                      </span>
                                    </div>
                                    <h2 className="fs-5 text-muted mb-3">
                                      Materi
                                    </h2>
                                    {this.state.silabusList.map(
                                      (elem, index) => {
                                        return (
                                          <div className="row align-items-top mb-5 trigger-silabus">
                                            <div
                                              className={`${
                                                index > 0
                                                  ? "col-lg-6"
                                                  : "col-lg-6"
                                              } fw-row`}
                                            >
                                              <label className="form-label required">
                                                Judul Materi
                                              </label>
                                              <input
                                                className="form-control form-control-sm silabus"
                                                placeholder="Masukkan Judul Materi"
                                                defaultValue={elem.isi_silabus}
                                                onBlur={(evt) => {
                                                  this.handleInputSilabus(
                                                    "isi_silabus",
                                                    evt.target.value,
                                                    elem.id
                                                  );
                                                }}
                                                key={`isi_silabus_${elem.id}`}
                                                name={`isi_silabus_${elem.id}`}
                                              />
                                              <span
                                                style={{ color: "red" }}
                                              ></span>
                                            </div>
                                            <div
                                              className={`${
                                                index > 0
                                                  ? "col-lg-5"
                                                  : "col-lg-6"
                                              } fw-row`}
                                            >
                                              <label className="form-label required">
                                                Jumlah JP
                                              </label>
                                              <input
                                                className="form-control form-control-sm silabus numeric-silabus"
                                                placeholder="Masukkan Jumlah JP"
                                                key={`jp_silabus_${elem.id}`}
                                                name={`jp_silabus_${elem.id}`}
                                                defaultValue={elem.jp_silabus}
                                                onBlur={(evt) => {
                                                  this.handleInputSilabus(
                                                    "jp_silabus",
                                                    evt.target.value,
                                                    elem.id
                                                  );
                                                }}
                                                onChange={(e) => {
                                                  numericOnly(e.target);
                                                }}
                                              />
                                              <span
                                                style={{ color: "red" }}
                                              ></span>
                                            </div>
                                            {index != 0 && (
                                              <div className="col-lg-1">
                                                <div className="mt-7 fv-row">
                                                  <a
                                                    title="Hapus"
                                                    onClick={() => {
                                                      this.handleClickDeleteSilabusAction(
                                                        index
                                                      );
                                                    }}
                                                    className="btn btn-icon btn-sm btn-danger w-lg-100"
                                                  >
                                                    <i className="bi bi-trash-fill"></i>
                                                  </a>
                                                </div>
                                              </div>
                                            )}
                                          </div>
                                        );
                                      }
                                    )}
                                    <div className="row text-center">
                                      <div className="col-9">
                                        <a
                                          onClick={this.handleClickAddSilabus}
                                          className="btn btn-light text-success d-block fw-semibold btn-sm me-3 mr-2"
                                        >
                                          <i className="bi bi-plus-circle text-success"></i>
                                          Tambah Materi
                                        </a>
                                      </div>
                                      <div className="col-3">
                                        <a
                                          onClick={this.handleClickSaveSilabus}
                                          className="btn btn-success fw-semibold btn-sm"
                                          hidden
                                          id="simpan-silabus"
                                        >
                                          <i className="fa fa-paper-plane ME-1"></i>
                                          Simpan Silabus
                                        </a>
                                        <a
                                          data-kt-stepper-action="simpan-silabus"
                                          className="btn btn-success fw-semibold btn-sm"
                                        >
                                          <i className="fa fa-paper-plane ME-1"></i>
                                          Simpan Silabus
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                </div> */}
                              </div>
                            </div>

                            <div
                              className="flex-column"
                              data-kt-stepper-element="content"
                            >
                              <div className="row mt-7">
                                <input
                                  type="hidden"
                                  name="index_stepper"
                                  defaultValue={
                                    this.state.valueIndexStepper ?? ""
                                  }
                                />
                                <input
                                  type="hidden"
                                  name="form_builder_id"
                                  defaultValue={
                                    this.state.valueFormBuilderId ?? ""
                                  }
                                />
                                <input
                                  type="hidden"
                                  name="form_builder_name"
                                  defaultValue={
                                    this.state.valueFormBuilderName ?? ""
                                  }
                                />
                                <div
                                  id="btn_choose_form"
                                  className="row"
                                  style={{
                                    display: this.state.showOptionButton
                                      ? "flex"
                                      : "none",
                                  }}
                                >
                                  <div id="btn_choose_form" className="row">
                                    <div className="col-lg-6">
                                      <a
                                        className="btn btn-outline btn-outline-dashed btn-outline-default btn-hover-rise p-7 d-flex align-items-center my-7"
                                        onClick={this.handleClickSelectForm}
                                      >
                                        <span className="svg-icon svg-icon-3x">
                                          <svg
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                            xmlns="http://www.w3.org/2000/svg"
                                            className="mh-50px"
                                          >
                                            <path
                                              opacity="0.3"
                                              d="M14 2H6C4.89543 2 4 2.89543 4 4V20C4 21.1046 4.89543 22 6 22H18C19.1046 22 20 21.1046 20 20V8L14 2Z"
                                              fill="black"
                                            ></path>
                                            <path
                                              d="M20 8L14 2V6C14 7.10457 14.8954 8 16 8H20Z"
                                              fill="black"
                                            ></path>
                                            <rect
                                              x="13.6993"
                                              y="13.6656"
                                              width="4.42828"
                                              height="1.73089"
                                              rx="0.865447"
                                              transform="rotate(45 13.6993 13.6656)"
                                              fill="black"
                                            ></rect>
                                            <path
                                              d="M15 12C15 14.2 13.2 16 11 16C8.8 16 7 14.2 7 12C7 9.8 8.8 8 11 8C13.2 8 15 9.8 15 12ZM11 9.6C9.68 9.6 8.6 10.68 8.6 12C8.6 13.32 9.68 14.4 11 14.4C12.32 14.4 13.4 13.32 13.4 12C13.4 10.68 12.32 9.6 11 9.6Z"
                                              fill="black"
                                            ></path>
                                          </svg>
                                        </span>
                                        <span className="d-block fw-bold text-start">
                                          <span className="text-dark fw-bolder d-block fs-4 mb-2">
                                            Pilih Form Pendaftaran
                                          </span>
                                          <span className="text-gray-400 fw-bold fs-6">
                                            Pilih form pendaftaran dari
                                            repositori.
                                          </span>
                                        </span>
                                      </a>
                                    </div>
                                    <div className="col-lg-6">
                                      <a
                                        className="btn btn-outline btn-outline-dashed btn-outline-default btn-hover-rise p-7 d-flex align-items-center my-7"
                                        onClick={this.handleClickCreateForm}
                                      >
                                        <span className="svg-icon svg-icon-3x ms-n1">
                                          <svg
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                            xmlns="http://www.w3.org/2000/svg"
                                            className="mh-50px"
                                          >
                                            <path
                                              opacity="0.3"
                                              d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM16 13.5L12.5 13V10C12.5 9.4 12.6 9.5 12 9.5C11.4 9.5 11.5 9.4 11.5 10L11 13L8 13.5C7.4 13.5 7 13.4 7 14C7 14.6 7.4 14.5 8 14.5H11V18C11 18.6 11.4 19 12 19C12.6 19 12.5 18.6 12.5 18V14.5L16 14C16.6 14 17 14.6 17 14C17 13.4 16.6 13.5 16 13.5Z"
                                              fill="black"
                                            ></path>
                                            <rect
                                              x="11"
                                              y="19"
                                              width="10"
                                              height="2"
                                              rx="1"
                                              transform="rotate(-90 11 19)"
                                              fill="black"
                                            ></rect>
                                            <rect
                                              x="7"
                                              y="13"
                                              width="10"
                                              height="2"
                                              rx="1"
                                              fill="black"
                                            ></rect>
                                            <path
                                              d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z"
                                              fill="black"
                                            ></path>
                                          </svg>
                                        </span>
                                        <span className="d-block fw-bold text-start">
                                          <span className="text-dark fw-bolder d-block fs-4 mb-2">
                                            Buat Form Baru
                                          </span>
                                          <span className="text-gray-400 fw-bold fs-6">
                                            Buat form pendaftaran dengan form
                                            builder.
                                          </span>
                                        </span>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                                <div
                                  id="div_selform"
                                  style={{
                                    display: this.state.showSelForm
                                      ? ""
                                      : "none",
                                  }}
                                >
                                  <div className="row">
                                    <div className="col-lg-12 mb-4">
                                      <div className="d-flex justify-content-between align-items-start flex-wrap pt-6">
                                        <div className="d-flex flex-column">
                                          <h2 className="fs-5 text-muted">
                                            Form Pendaftaran
                                          </h2>
                                        </div>
                                        <div className="d-flex">
                                          <a
                                            href="#"
                                            className="btn btn-secondary btn-sm me-2"
                                            onClick={this.handleClickRefForm}
                                            title="Refresh form"
                                          >
                                            <span className="svg-icon m-0">
                                              <i className="bi bi-arrow-clockwise"></i>
                                            </span>
                                            Refresh
                                          </a>
                                          <a
                                            href="#"
                                            className="btn btn-secondary btn-sm me-2"
                                            onClick={(e) => {
                                              this.setState({
                                                showSelForm: false,
                                                showOptionButton: true,
                                                showBtnNextW2: false,
                                                // selnamasilabus: ""
                                                valuejudulform: {},
                                              });
                                            }}
                                          >
                                            Batal
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                                    <div className="bg-light-warning col-12 text-warning mx-3 mb-5 fw-semibold rounded p-5 fs-7">
                                      Form Pendaftaran akan terhubung dengan
                                      master form.
                                    </div>
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label required">
                                        Judul Form
                                      </label>
                                      <select
                                        className="form-select form-select-sm"
                                        name="judul_form"
                                        disabled={
                                          this.state.dataxselform.length <= 0
                                        }
                                        value={
                                          this.state.valuejudulform?.value ?? ""
                                        }
                                        onChange={this.handleChangeJudulForm}
                                      >
                                        <option
                                          value=""
                                          style={{ display: "none" }}
                                        >
                                          Silahkan pilih
                                        </option>
                                        {this.state.dataxselform &&
                                          this.state.dataxselform.map(
                                            (opt, index) => (
                                              <option
                                                key={`level_${index}`}
                                                value={opt.value}
                                              >
                                                {opt.label}
                                              </option>
                                            ),
                                          )}
                                      </select>
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["judul_form"]}
                                      </span>
                                    </div>
                                  </div>
                                  <div
                                    style={{
                                      display: this.state.showViewForm
                                        ? "block"
                                        : "none",
                                    }}
                                    className="col-lg-12 mb-7 fv-row"
                                  >
                                    <label className="form-label">
                                      Isi Form
                                    </label>
                                    <div className="mr-3 border rounded p-5">
                                      <h5
                                        id="preview_title"
                                        className="modal-title mb-7"
                                      >
                                        {this.state.dataxpreviewtitle}
                                      </h5>
                                      {this.state.dataxpreview?.length > 0 && (
                                        <RenderFormElement
                                          formElements={this.state.dataxpreview}
                                        />
                                      )}
                                    </div>
                                  </div>
                                </div>
                                <div
                                  id="div_createform"
                                  // id="embedform"
                                  className="col-lg-12 mb-7 fv-row pt-0"
                                  style={{
                                    display: this.state.showCreateForm
                                      ? ""
                                      : "none",
                                  }}
                                >
                                  <div className="col-lg-12 mb-4">
                                    <div className="d-flex justify-content-between align-items-start flex-wrap pt-6">
                                      <div className="d-flex flex-column">
                                        <h2 className="fs-5 text-muted">
                                          Form Pendaftaran
                                        </h2>
                                      </div>
                                      <div className="d-flex">
                                        <a
                                          href="#"
                                          className="btn btn-secondary btn-sm me-2"
                                          onClick={this.handleClickRefForm}
                                          title="Refresh form"
                                        >
                                          <span className="svg-icon m-0">
                                            <i className="bi bi-arrow-clockwise"></i>
                                          </span>
                                          Refresh
                                        </a>
                                        <a
                                          href="#"
                                          className="btn btn-secondary btn-sm me-2"
                                          onClick={(e) => {
                                            this.setState({
                                              showSelForm: false,
                                              showOptionButton: true,
                                              showBtnNextW2: false,
                                              // selnamasilabus: ""
                                              valuejudulform: {},
                                            });
                                          }}
                                        >
                                          Batal
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                  <PelatihanPendaftaranContentAdd
                                    props={null}
                                    formMaster={this.state.dataxselform}
                                    idSlug={this.state.selakademiobj[0]?.slug}
                                    onSave={(data) => {
                                      if (data.length) {
                                        this.setState({
                                          valuejudulform: {
                                            label: null,
                                            value: data[0].pid ?? "",
                                          },
                                          showBtnNextW2: true,
                                        });
                                      }
                                    }}
                                  />
                                </div>
                              </div>
                            </div>

                            <div
                              className="flex-column"
                              data-kt-stepper-element="content"
                            >
                              <div className="row mt-7">
                                <div className="col-lg-12 mb-7 fv-row d-none">
                                  <label className="form-label required">
                                    Komitmen Peserta
                                  </label>
                                  <div className="d-flex">
                                    <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        disabled
                                        name="komitmen_peserta"
                                        value="Iya"
                                        id="komitmen_peserta1"
                                        onChange={this.handleChangeKomitmen}
                                        checked={
                                          this.state.valuekomitmen === "Iya"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="komitmen_peserta1"
                                      >
                                        Iya
                                      </label>
                                    </div>
                                    <div className="form-check form-check-sm form-check-custom form-check-solid">
                                      <input
                                        className="form-check-input"
                                        type="radio"
                                        disabled
                                        name="komitmen_peserta"
                                        value="Tidak"
                                        id="komitmen_peserta2"
                                        onChange={this.handleChangeKomitmen}
                                        checked={
                                          this.state.valuekomitmen == "Tidak"
                                        }
                                      />
                                      <label
                                        className="form-check-label"
                                        htmlFor="komitmen_peserta2"
                                      >
                                        Tidak
                                      </label>
                                    </div>
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["komitmen_peserta"]}
                                  </span>
                                </div>
                                <div>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Komitmen / Perjanjian/ Ketentuan Pelatihan
                                    </label>
                                    {/* <textarea id="kt_docs_ckeditor_classic" className="form-control form-control-sm" placeholder="Masukkan deskripsi" name="deskripsi_komitmen" value={this.state.fields["deskripsi"]} onChange={this.handleChangeDeskripsiKomitmen}></textarea> */}
                                    <CKEditor
                                      editor={Editor}
                                      data={this.state.valuedeskripsikomitmen}
                                      placeholder="Masukkan deskripsi komitmen"
                                      name="deskripsi_komitmen"
                                      onReady={(editor) => {}}
                                      config={{
                                        ckfinder: {
                                          // Upload the images to the server using the CKFinder QuickUpload command.
                                          uploadUrl:
                                            process.env.REACT_APP_BASE_API_URI +
                                            "/publikasi/ckeditor-upload-image",
                                        },
                                      }}
                                      onBlur={(event, editor) => {
                                        const data = editor.getData();
                                        this.handleChangeDeskripsiKomitmen(
                                          data,
                                        );
                                      }}
                                      // onBlur={(event, editor) => {}}
                                      onFocus={(event, editor) => {}}
                                    />
                                    <input
                                      type="hidden"
                                      name="deskripsi_komitmen_hidden"
                                    />
                                    <span style={{ color: "red" }}>
                                      {this.state.errors["deskripsi_komitmen"]}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className="text-center border-top pt-10 my-7">
                              <button
                                className="btn btn-light btn-md me-2"
                                data-kt-stepper-action="previous"
                                onClick={() => {
                                  this.setState({ showBtnNextW2: true });
                                }}
                              >
                                <i className="fa fa-chevron-left"></i>Sebelumnya
                              </button>

                              <button
                                type="button"
                                className="btn btn-primary btn-md me-2 d-none"
                                data-kt-stepper-action="submit"
                                ref={this.submitFormRef}
                              ></button>

                              <button
                                disabled={this.state.isLoading}
                                type="button"
                                id="dummy-submit"
                                className="btn btn-primary btn-md me-2 d-none"
                                onClick={() => {
                                  swal
                                    .fire({
                                      title: "Apakah anda yakin?",
                                      text: "Pastikan data Pelatihan telah benar, Silabus dan Form Pendaftaran tidak dapat diubah jika nantinya telah disetujui Admin Akademi",
                                      icon: "warning", // add html attribute if you want or remove
                                      showCancelButton: true,
                                      confirmButtonColor: "#3085d6",
                                      cancelButtonColor: "#d33",
                                      confirmButtonText: "Ya, Simpan",
                                      cancelButtonText: "Batal",
                                      allowOutsideClick: false,
                                    })
                                    .then((result) => {
                                      if (result.isConfirmed) {
                                        this.submitFormRef.current?.click();
                                      }
                                    });
                                }}
                              >
                                {this.state.isLoading ? (
                                  <>
                                    <span
                                      className="spinner-border spinner-border-sm me-2"
                                      role="status"
                                      aria-hidden="true"
                                    ></span>
                                    <span className="sr-only">Loading...</span>
                                    Loading...
                                  </>
                                ) : (
                                  <>
                                    <i className="fa fa-paper-plane me-1"></i>
                                    Simpan
                                  </>
                                )}
                              </button>

                              <button
                                type="submit"
                                className="btn btn-primary btn-md me-2 d-none"
                                id="submit-form"
                              ></button>

                              <div
                                id="next_id"
                                style={{
                                  display: this.state.showBtnNextW2
                                    ? "inline"
                                    : "none",
                                }}
                              >
                                <button
                                  type="button"
                                  className="btn btn-primary btn-md me-2"
                                  data-kt-stepper-action="next"
                                  onClick={(e) => {
                                    if (
                                      (Cookies.get("step") == 2 &&
                                        // this.state.showOptionButtonSilabus &&
                                        this.state.detailSilabus.id != "") ||
                                      (Cookies.get("step") == 3 &&
                                        this.state.showOptionButton &&
                                        !this.state.valuejudulform.value)
                                    ) {
                                      this.setState({ showBtnNextW2: false });
                                      if (Cookies.get("step") == 3) {
                                        this.handleClickRefFormAction();
                                      }
                                    } else {
                                      this.setState({ showBtnNextW2: true });
                                    }
                                  }}
                                >
                                  <i className="fa fa-chevron-right"></i>
                                  Lanjutkan
                                </button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="modal fade" tabIndex="-1" id="kt_modal_3">
          <div className="modal-dialog">
            <div className="modal-content position-absolute">
              <div className="modal-header">
                <h5 id="preview_title" className="modal-title">
                  {this.state.dataxpreviewtitle}
                </h5>
                <div
                  className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <span className="svg-icon svg-icon-2x"></span>
                </div>
              </div>
              <div className="modal-body">
                {this.state.dataxpreview.map((data) => (
                  <div dangerouslySetInnerHTML={{ __html: data.html }}></div>
                ))}
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-light"
                  data-bs-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
