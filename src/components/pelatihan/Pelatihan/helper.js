import * as FileSaver from "file-saver";
import * as XLSX from "xlsx";
import moment from "moment";
import Cookies from "js-cookie";

export function fp_inc(dateObj, days) {
  return new Date(
    dateObj.getFullYear(),
    dateObj.getMonth(),
    dateObj.getDate() + (typeof days === "string" ? parseInt(days, 10) : days),
    dateObj.getHours(),
    dateObj.getMinutes(),
  );
}

export function dateRangeWithSeparator(
  date,
  separator = "-",
  originFormat = "DD-MM-YYYY HH:mm:ss",
  outputFormat = "DD MMM YYYY",
) {
  let [startDate = "", endDate = ""] = date.split(separator);
  return dateRange(
    startDate.trim(),
    endDate.trim(),
    originFormat,
    outputFormat,
  );
}

export function dateRange(
  startDate,
  endDate,
  originFormat = "DD-MM-YYYY HH:mm:ss",
  outputFormat = "DD MMM YYYY",
) {
  moment.locale("id");
  startDate = startDate
    ?.replace("May", "Mei")
    .replace("Oct", "Okt")
    .replace("Dec", "Des")
    .replace("Aug", "Agt");
  endDate = endDate
    ?.replace("May", "Mei")
    .replace("Oct", "Okt")
    .replace("Dec", "Des")
    .replace("Aug", "Agt");
  if (
    moment(startDate, originFormat).isValid() &&
    moment(endDate, originFormat).isValid()
  ) {
    if (outputFormat.includes("HH:mm:ss")) {
      return (
        moment(startDate, originFormat).format(outputFormat) +
        " - " +
        moment(endDate, originFormat).format(outputFormat)
      );
    }
    let dt1 = moment(startDate, originFormat).locale("id");
    let dt2 = moment(endDate, originFormat).locale("id");

    let f1 = outputFormat;
    let f2 = outputFormat;

    let dy = dt1.year() == dt2.year();
    let dm = dt1.month() == dt2.month();
    let dd = dt1.date() == dt2.date();

    if (dy) {
      f1 = f1.replace(" YYYY", "");
      if (dm) {
        f1 = f1.replace(" MMM", "");
        if (dd) {
          return dt2.format(f2);
        }
      }
    }

    return dt1.format(f1) + " - " + dt2.format(f2);
  }
  return "Invalid dates";
}

export function handleFormatDate(
  dateString,
  format = "DD-MM-YYYY HH:mm:ss",
  base = "DD-MM-YYYY HH:mm:ss",
) {
  moment.locale("id");
  return moment(dateString, base).isValid()
    ? moment(dateString, base).format(format)
    : "-";
}

export function capitalWord(str) {
  if (str == null) {
    return "";
  }
  return str.replace(/\w\S*/g, function (kata) {
    const kataBaru = kata.slice(0, 1).toUpperCase() + kata.substr(1);
    return kataBaru;
  });
}

export function exportToCSV(
  apiData,
  fileName,
  fileType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8",
  fileExtension = ".xlsx",
) {
  const ws = XLSX.utils.json_to_sheet(apiData);
  const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
  const excelBuffer = XLSX.write(wb, {
    bookType: "xlsx",
    type: "array",
    Props: {
      Subject: "Pelatihan",
      Tags: "Pelatihan detail",
      Title: "Generated From Digitalent Scholarship",
      Comments: `By (${Cookies.get("user_name")} - ${Cookies.get(
        "user_id",
      )}) on ${moment().format("DD/MM/YYYY, HH:mm:ss")}`,
      Name: `From DTS By (${Cookies.get("user_id")}) on ${moment().format(
        "DD/MM/YYYY, HH:mm:ss",
      )}`,
    },
  });
  const data = new Blob([excelBuffer], { type: fileType });
  FileSaver.saveAs(data, fileName + fileExtension);
}
export const resolveBgStatusSubstansi = (status, prepend = "") => {
  let s = status?.toLowerCase() || "";
  let color = "secondary";
  switch (s) {
    case "ditolak":
      color = "danger";
      break;

    case "disetujui":
      color = "success";
      break;

    case "revisi":
      color = "warning";
      break;

    case "review":
      color = "primary";
      break;
  }
  return prepend + color;
};
export const tagColorMapStatusPeserta = {
  "Lulus Pelatihan - Kehadiran": "success",
  "Lulus Pelatihan - Nilai": "success",
  "Lulus Tes Substansi": "success",
  Diterima: "success",
  "Tidak Lulus Pelatihan - Nilai": "danger",
  "Tidak Lulus Pelatihan - Kehadiran": "danger",
  "Tidak Lulus Pelatihan - Tidak Hadir": "danger",
  "Tidak Lulus Tes Substansi": "danger",
  "Tidak Lulus Administrasi": "danger",
  "Tidak Hadir": "danger",
  Banned: "danger",
  "Belum Melengkapi": "warning",
  Cadangan: "warning",
  Pembatalan: "danger",
  "Mengundurkan Diri": "danger",
  Pelatihan: "primary",
  Submit: "primary",
  "Tes Substansi": "primary",
  "Berhak Sertifikasi": "success",
  "Ikut Sertifikasi": "success",
  "Lulus Sertifikasi": "success",
};
export const resolveStatusPelatihanBg = (status, prepend = "") => {
  let s = status?.toLowerCase() || "";
  let color = "secondary";
  switch (s) {
    case "review substansi":
      color = "secondary";
      break;

    case "menunggu pendaftaran":
      color = "light-warning";
      break;

    case "pendaftaran":
      color = "light-warning";
      break;

    case "seleksi":
      color = "light-primary";
      break;

    case "pelatihan":
      color = "light-primary";
      break;

    case "selesai":
      color = "light-success";
      break;

    case "dibatalkan":
      color = "light-danger";
      break;
  }
  return prepend + color;
};

export const disabledStatus = () => [
  "pembatalan",
  "tidak hadir",
  "mengundurkan diri",
];

export function zeroDecimalValue(value) {
  return value - value == 0 ? normalDecimals(value) : value;
}

function normalDecimals(value) {
  if (Math.floor(value) === value) return 0;

  var str = value.toString();

  if (str.split(".")[1] == 0) {
    return value | 0;
  } else {
    return value;
  }
}

export const provider = {
  62852: "Telkomsel",
  62853: "Telkomsel",
  62813: "Telkomsel",
  62821: "Telkomsel",
  62822: "Telkomsel",
  62812: "Telkomsel",
  62811: "Telkomsel",
  62851: "Telkomsel",

  62814: "Indosat",
  62815: "Indosat",
  62816: "Indosat",
  62855: "Indosat",
  62856: "Indosat",
  62857: "Indosat",
  62858: "Indosat",

  62817: "XL",
  62818: "XL",
  62819: "XL",
  62859: "XL",
  62877: "XL",
  62878: "XL",

  62831: "Axis",
  62832: "Axis",
  62833: "Axis",
  62838: "Axis",

  62895: "Tri",
  62896: "Tri",
  62897: "Tri",
  62898: "Tri",
  62899: "Tri",

  62881: "Smartfren",
  62882: "Smartfren",
  62883: "Smartfren",
  62884: "Smartfren",
  62885: "Smartfren",
  62886: "Smartfren",
  62887: "Smartfren",
  62888: "Smartfren",
  62889: "Smartfren",
};

export const FRONT_URL = "https://front-user.dev.sdmdigital.id";
export const PESERTA_PELATIHAN_KEY = "rekap-pendaftaran-peserta-pelatihan";
export const PELATIHAN_ID_KEY = "pelatian_id";
