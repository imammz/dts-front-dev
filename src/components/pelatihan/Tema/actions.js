import axios from "axios";
import Cookies from "js-cookie";
import Swal from "sweetalert2";

const configs = {
  headers: {
    Authorization: "Bearer " + Cookies.get("token"),
  },
};

const checkToken = () => {
  return new Promise(async (resolve, reject) => {
    if (Cookies.get("token") == null) {
      const { isConfirmed } = await Swal.fire({
        title: "Unauthenticated.",
        text: "Silahkan login ulang",
        icon: "warning",
        confirmButtonText: "Ok",
      });

      if (isConfirmed) return reject();
    }

    resolve();
  });
};

export const fetchTema = async ({
  id_akademi,
  start = 0,
  length = 10,
  status = "all",
  param = "",
  sort = "id",
  sort_val = "desc",
}) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        var payload = new URLSearchParams();
        payload.append("id_akademi", id_akademi);
        payload.append("status", status);
        payload.append("start", start);
        payload.append("rows", length);
        payload.append("cari", param);
        payload.append("sort", sort);
        payload.append("sort_val", sort_val.toUpperCase());

        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/list_tema_filter2",
            payload,
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let {
              TotalData,
              Data,
              Status = false,
              Message = "",
            } = result || {};

            if (Status)
              resolve({
                total: TotalData || 0,
                data: Data,
              });
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchAkademi = async ({
  tahun = "0",
  start = 0,
  length = 10,
  status = "99",
  param = "",
  sort = "id",
  sort_val = "desc",
}) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/list-akademi-s3",
            {
              tahun,
              start,
              length,
              status,
              param,
              sort,
              sort_val: sort_val,
            },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Total, Data, Status = false, Message = "" } = result || {};

            if (Status)
              resolve({
                total: Total,
                data: Data,
              });
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchStatus = async () => {
  return new Promise((resolve, reject) => {
    resolve([
      { value: "1", label: "Publish" },
      { value: "0", label: "Unpublish" },
    ]);
  });
};

export const deleteTema = async ({ id }) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/hapus_tema",
            { id },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Message);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};
