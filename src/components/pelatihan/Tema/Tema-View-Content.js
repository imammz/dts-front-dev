import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";

export default class TemaContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickDelete = this.handleClickDeleteAction.bind(this);
    this.handleClick = this.handleClickAction.bind(this);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleChangeDescription =
      this.handleChangeDescriptionAction.bind(this);
    this.state = {
      fields: {},
      errors: {},
      datax: [],
      dataxtema: [],
      selakademi: [],
      selstatus: [],
      deskripsi: "",
    };
  }
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  optionstatus = [
    { value: "1", label: "Publish" },
    { value: "0", label: "Unpublish" },
  ];
  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    let segment_url = window.location.pathname.split("/");
    let urlSegmenttOne = segment_url[3];
    Cookies.set("tema_id", urlSegmenttOne);
    let data = {
      id: urlSegmenttOne,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/cari_tema",
        data,
        this.configs,
      )
      .then((res) => {
        const dataxtema = res.data.result.Data[0];
        this.setState({ dataxtema });

        const dataAkademik = { start: 0, length: 100, status: "publish" };
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/akademi/list-akademi",
            dataAkademik,
            this.configs,
          )
          .then((res) => {
            const optionx = res.data.result.Data;
            const datax = [];
            for (let i in optionx) {
              if (optionx[i].id === this.state.dataxtema.akademi_id) {
                this.setState({
                  selakademi: {
                    value: optionx[i].id,
                    label: optionx[i].name,
                  },
                });
              }
            }
            optionx.map((data) =>
              datax.push({ value: data.id, label: data.name }),
            );
            this.setState({ datax });
            this.setState({
              selstatus: {
                value: this.state.dataxtema.status,
                label:
                  this.state.dataxtema.status == "1" ? "Publish" : "Unpublish",
              },
            });
          });
      });
  }
  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    let errors = {};
    let formIsValid = true;

    if (dataForm.get("name") == "") {
      formIsValid = false;
      errors["name"] = "Tidak boleh kosong";
    }
    if (this.state.deskripsi == "") {
      formIsValid = false;
      errors["deskripsi"] = "Tidak boleh kosong";
    }
    this.setState({ errors: errors });
    return formIsValid;
  }
  resetError() {
    let errors = {};
    errors["name"] = "";
    errors["deskripsi"] = "";
    this.setState({ errors: errors });
  }
  handleClickAction(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      let updatex = 0;
      if (
        this.state.dataxtema.deskripsi != dataForm.get("deskripsi") ||
        this.state.dataxtema.status != dataForm.get("status")
      ) {
        updatex = 1;
      }
      let data = {
        update: updatex,
        id: Cookies.get("tema_id"),
        akademi_id: dataForm.get("akademi_id"),
        name: dataForm.get("name"),
        deskripsi: this.state.deskripsi,
        status: dataForm.get("status"),
      };
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/update_tema",
          data,
          this.configs,
        )
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/pelatihan/tema";
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
    }
  }
  handleClickBatalAction(e) {
    window.location = "/pelatihan/tema";
  }
  handleChangeAkademiAction = (selectedOption) => {
    this.setState({
      selakademi: { value: selectedOption.value, label: selectedOption.label },
    });
  };
  handleChangeStatusAction = (selectedOption) => {
    this.setState({
      selstatus: { value: selectedOption.value, label: selectedOption.label },
    });
  };
  handleChangeDescriptionAction(value) {
    this.state.deskripsi = value;
  }
  handleClickDeleteAction(e) {
    const idx = e.currentTarget.id;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "info",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          let data = { id: idx };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/hapus_tema",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.StatusCode;
              const messagex = res.data.result.Message;
              if ("200" == statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      window.location = "/pelatihan/tema";
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "error",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload();
                    }
                  });
              }
            });
        }
      });
  }
  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Tema
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
                <a
                  href={"/pelatihan/edit-tema/" + Cookies.get("tema_id")}
                  className="btn btn-sm btn-warning btn-active-light-info me-2"
                >
                  <i className="bi bi-gear-fill text-white"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Edit
                  </span>
                </a>
                <a
                  href="#"
                  id={Cookies.get("tema_id")}
                  onClick={this.handleClickDelete}
                  className="btn btn-sm btn-danger btn-active-light-info me-2"
                >
                  <i className="bi bi-trash-fill text-white"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-5">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Detail Tema
                          </h1>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="row mt-6 pb-5">
                          <div className="col-lg-6 mb-7 fv-row">
                            <label className="form-label fw-bolder">
                              Nama Tema
                            </label>
                            <div className="d-flex">
                              {this.state.dataxtema.name}
                            </div>
                          </div>
                          <div className="col-lg-6 mb-7 fv-row">
                            <label className="form-label fw-bolder">
                              Akademi
                            </label>
                            <div className="d-flex">
                              {this.state.selakademi.label}
                            </div>
                          </div>
                          <div className="col-lg-12 mb-7 fv-row">
                            <label className="form-label fw-bolder">
                              Deskripsi
                            </label>
                            <div className="d-flex">
                              <p
                                dangerouslySetInnerHTML={{
                                  __html: this.state.dataxtema.deskripsi,
                                }}
                              ></p>
                            </div>
                          </div>
                          <div className="col-lg-12 mb-7 fv-row">
                            <label className="form-label fw-bolder">
                              Status
                            </label>
                            <div className="d-flex">
                              <p>{this.state.selstatus.label}</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
