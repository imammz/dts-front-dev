import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import DataTable from "react-data-table-component";
import { deleteTema, fetchAkademi, fetchStatus, fetchTema } from "./actions";
import moment from "moment";
import Swal from "sweetalert2";
import { cekPermition } from "../../AksesHelper";

export default class TemaContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickDelete = this.handleClickDeleteAction.bind(this);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleChangeSearch = this.handleChangeSearchAction.bind(this);
    this.handleClickSearch = this.handleClickSearchAction.bind(this);
    this.handleClickReset = this.handleClickResetAction.bind(this);
    this.handleChangeAkademi = this.handleChangeAkademiAction.bind(this);
    this.handleChangeStatusPublish =
      this.handleChangeStatusPublishAction.bind(this);
  }
  capitalWord(str) {
    return str.replace(/\w\S*/g, function (kata) {
      const kataBaru = kata.slice(0, 1).toUpperCase() + kata.substr(1);
      return kataBaru;
    });
  }
  state = {
    datax: [],
    dataxakademi: [],
    dataxakademivalue: "",
    dataxstatusvalue: "",
    // loading: true,
    rowSkeletons: 20,
    dataxsearch: "",
    newPerPage: 10,
    numberrow: 1,
    param: "",

    totalAkademi: 0,
    dataAkademi: [],
    loadingAkademi: false,
    dataStatus: [],

    total: 0,
    data: [],
    loading: false,

    currentPage: 1,
    currentRowsPerPage: 10,
    searchKeyword: 0,
    sortField: "id",
    sortDir: "desc",

    selectedAkademi: 0,
    selectedStatus: null,
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  optionstatus = [
    { value: "1", label: "Publish" },
    { value: "0", label: "Unpublish" },
  ];

  fetch = async () => {
    try {
      this.setState({ loading: true });

      const { total, data } = await fetchTema({
        id_akademi: this.state.selectedAkademi,
        start: (this.state.currentPage - 1) * this.state.currentRowsPerPage,
        length: this.state.currentRowsPerPage,
        param: this.state.searchKeyword,
        status: this.state.selectedStatus,
        sort: this.state.sortField,
        sort_val: this.state.sortDir,
      });

      this.setState({ total, data, loading: false });
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });

      this.setState({ total: 0, data: [], loading: false });
    }
  };

  fetchAkademi = async () => {
    try {
      this.setState({ loadingAkademi: true });
      const { total, data } = await fetchAkademi({
        tahun: moment().year(),
        start: 0,
        length: 10000,
        status: 1,
      });
      this.setState({
        totalAkademi: total,
        dataAkademi: data.map(({ id, name }) => ({ value: id, label: name })),
        loadingAkademi: false,
      });
    } catch (err) {}
  };

  fetchStatus = async () => {
    try {
      const data = await fetchStatus();
      this.setState({ dataStatus: data });
    } catch (err) {}
  };

  onRemove = async (id) => {
    try {
      const { isConfirmed = false } = await Swal.fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      });

      if (isConfirmed) {
        Swal.fire({
          title: "Mohon Tunggu!",
          icon: "info",
          allowOutsideClick: false,
          didOpen: () => Swal.showLoading(),
        });

        const message = await deleteTema({ id });

        Swal.close();

        await Swal.fire({
          title: message || "Tema berhasil dihapus",
          icon: "success",
          confirmButtonText: "Ok",
        });

        this.setState({ searchKeyword: 0 }, () => this.fetch());
      }

      // throw new Error("API delete belum tersedia");
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  componentDidMount() {
    this.fetch();
    this.fetchAkademi();
    this.fetchStatus();
  }

  handleClickDeleteAction(e) {
    const idx = e.currentTarget.id;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "info",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          let data = { id: idx };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/hapus_tema",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.StatusCode;
              const messagex = res.data.result.Message;
              if ("200" == statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload();
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "error",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload();
                    }
                  });
              }
            });
        }
      });
  }

  handleReload(from, param, page, newPerPage) {
    console.log(
      "from :" +
        from +
        ", param :" +
        param +
        ", page :" +
        page +
        ", newPerPage :" +
        newPerPage,
    );
    this.setState({ loading: true });
    let start_tmp = 1;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - length_tmp + 1;
      this.setState({ numberrow: start_tmp });
    } else {
      this.setState({ numberrow: 1 });
    }
    const dataBody = { start: start_tmp, rows: length_tmp };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/list_tema",
        dataBody,
        this.configs,
      )
      .then((res) => {
        const datax = res.data.result.Data;
        this.setState({ datax });
        this.setState({ loading: false });
        this.setState({ totalRows: res.data.result.TotalLength[0].jml_data });
      });
  }

  doSearch(searchText) {
    if (searchText == "") {
      this.handleReload();
    } else {
      let data = { text: searchText };
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/cari_tema_text",
          data,
          this.configs,
        )
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            const datax = res.data.result.Data;
            this.setState({ datax });
            this.setState({ loading: false });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  this.setState({ loading: false });
                }
              });
          }
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  this.setState({ datax: [] });
                  this.setState({ loading: false });
                }
              });
          }
        });
    }
  }
  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    this.setState({ numberrow: 1 });
    if (e.key === "Enter") {
      this.setState({ loading: true }, () => {
        this.doSearch(searchText);
      });
    }
  }
  handleClickSearchAction(e) {
    e.preventDefault();
    this.setState({ loading: true });
    const dataForm = new FormData(e.currentTarget);
    let akademiId = dataForm.get("akademi_id");
    let statusId = dataForm.get("status");
    if (akademiId === "" && statusId === "") {
      this.setState({ numberrow: 1 });
      this.handleReload(1, 10);
    } else {
      if (akademiId === "") {
        akademiId = 0;
      }
      if (statusId === "") {
        statusId = "null";
      }
      let data = { akademi_id: akademiId, status: statusId, start: 0, rows: 9 };
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/list_tema_filter2",
          data,
          this.configs,
        )
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            this.setState({ numberrow: 1 });
            const datax = res.data.result.Data;
            this.setState({ datax });
            this.setState({ loading: false });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  this.setState({ loading: false });
                }
              });
          }
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  this.setState({ datax: [] });
                  this.setState({ loading: false });
                }
              });
          }
        });
    }
  }
  handleChangeAkademiAction = (akademi_id) => {
    let dataxakademivalue = akademi_id;
    this.setState({ dataxakademivalue });
  };
  handleChangeStatusPublishAction = (status_id) => {
    let dataxstatusvalue = status_id;
    this.setState({ dataxstatusvalue });
  };
  handleClickResetAction(e) {
    e.preventDefault();
    this.setState({ numberrow: 1 });
    this.handleReload(1, 10);
    this.setState({ dataxakademivalue: "" });
    this.setState({ dataxstatusvalue: "" });
  }

  handlePageChange = (page, totalRows) => {
    this.setState({ currentPage: page }, () => this.fetch());
  };

  handlePerRowsChange = (currentRowsPerPage, currentPage) => {
    this.setState({ currentRowsPerPage, currentPage }, () => this.fetch());
  };

  onSort = ({ sortField }, sortDir) => {
    this.setState({ sortField, sortDir }, () => this.fetch());
  };

  onFilterSearch = (searchKeyword) => {
    this.setState({ searchKeyword, currentPage: 1 }, () => this.fetch());
  };

  handleChangeSearchAction(e) {
    const searchText = e.currentTarget.value;
    this.setState({ numberrow: 1 });
    if (searchText == "") {
      this.setState({ loading: true }, () => {
        this.handleReload();
      });
    } else {
      this.setState({ param: searchText, loading: true }, () => {
        this.handleReload("home", searchText, 1, this.state.newPerPage);
      });
    }
  }
  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Tema
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                  data-bs-toggle="modal"
                  data-bs-target="#filter"
                >
                  <i className="bi bi-sliders"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Filter
                  </span>
                </button>

                {cekPermition().manage === 1 ? (
                  <>
                    <a
                      href="/pelatihan/tambah-tema"
                      className="btn btn-success fw-bolder btn-sm"
                    >
                      <i className="bi bi-plus-circle"></i>
                      <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                        Tambah Tema
                      </span>
                    </a>
                  </>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Tema Pelatihan
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          {/* <div> */}
                          <span className="svg-icon svg-icon-1 position-absolute ms-6">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                              className="mh-50px"
                            >
                              <rect
                                opacity="0.5"
                                x="17.0365"
                                y="15.1223"
                                width="8.15546"
                                height="2"
                                rx="1"
                                transform="rotate(45 17.0365 15.1223)"
                                fill="#a1a5b7"
                              ></rect>
                              <path
                                d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                fill="#a1a5b7"
                              ></path>
                            </svg>
                          </span>
                          <input
                            type="text"
                            className="form-control form-control-sm form-control-solid w-250px ps-14"
                            placeholder="Cari Tema"
                            onKeyPress={(e) => {
                              if (e.key == "Enter")
                                this.onFilterSearch(e.target.value || 0);
                            }}
                            onChange={(e) => {
                              if (!e.target.value) this.onFilterSearch(0);
                            }}
                          />

                          <div className="modal fade" tabindex="-1" id="filter">
                            <div className="modal-dialog modal-md">
                              <div className="modal-content">
                                <div className="modal-header py-3">
                                  <h5 className="modal-title">
                                    <span className="svg-icon svg-icon-5 me-1">
                                      <i className="bi bi-sliders text-black"></i>
                                    </span>
                                    Filter Data Tema
                                  </h5>
                                  <div
                                    className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                    data-bs-dismiss="modal"
                                    aria-label="Close"
                                  >
                                    <span className="svg-icon svg-icon-2x">
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                      >
                                        <rect
                                          opacity="0.5"
                                          x="6"
                                          y="17.3137"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(-45 6 17.3137)"
                                          fill="currentColor"
                                        />
                                        <rect
                                          x="7.41422"
                                          y="6"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(45 7.41422 6)"
                                          fill="currentColor"
                                        />
                                      </svg>
                                    </span>
                                  </div>
                                </div>
                                <div className="modal-body">
                                  <div className="row">
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label">
                                        Akademi
                                      </label>
                                      <Select
                                        name="akademi_id"
                                        placeholder="Silahkan pilih"
                                        // noOptionsMessage={({ inputValue }) =>
                                        //   !inputValue
                                        //     ? this.state.dataxakademi
                                        //     : "Data tidak tersedia"
                                        // }
                                        className="form-select-sm form-select-solid selectpicker p-0"
                                        options={this.state.dataAkademi}
                                        value={
                                          this.state.dataAkademi.find(
                                            ({ value }) =>
                                              value ==
                                              this.state.selectedAkademi,
                                          ) || null
                                        }
                                        onChange={({ value }) =>
                                          this.setState({
                                            selectedAkademi: value,
                                          })
                                        }
                                      />
                                    </div>
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label">
                                        Status
                                      </label>
                                      <Select
                                        name="status"
                                        placeholder="Silahkan pilih"
                                        // noOptionsMessage={({ inputValue }) =>
                                        //   !inputValue
                                        //     ? this.state.datax
                                        //     : "Data tidak tersedia"
                                        // }
                                        className="form-select-sm selectpicker p-0"
                                        options={this.state.dataStatus}
                                        value={
                                          this.state.dataStatus.find(
                                            ({ value }) =>
                                              value ==
                                              this.state.selectedStatus,
                                          ) || null
                                        }
                                        onChange={({ value }) =>
                                          this.setState({
                                            selectedStatus: value,
                                          })
                                        }
                                      />
                                    </div>
                                  </div>
                                </div>
                                <div className="modal-footer py-3">
                                  <div className="d-flex justify-content-between">
                                    <button
                                      type="reset"
                                      className="btn btn-sm btn-light me-3"
                                      onClick={() => {
                                        this.setState(
                                          {
                                            currentPage: 1,
                                            selectedAkademi: 0,
                                            selectedStatus: null,
                                          },
                                          () => this.fetch(),
                                        );
                                      }}
                                    >
                                      Reset
                                    </button>
                                    <button
                                      type="submit"
                                      className="btn btn-sm btn-primary"
                                      onClick={() => {
                                        this.setState({ currentPage: 1 }, () =>
                                          this.fetch(),
                                        );
                                      }}
                                      // data-bs-dismiss="modal"
                                    >
                                      Apply Filter
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="table-responsive">
                          <DataTable
                            columns={[
                              {
                                name: "No",
                                center: true,
                                width: "70px",
                                cell: (row, index) => {
                                  return (
                                    <span>
                                      {(this.state.currentPage - 1) *
                                        this.state.currentRowsPerPage +
                                        index +
                                        1}
                                    </span>
                                  );
                                },
                              },
                              {
                                name: "Tema",
                                className: "min-w-300px mw-300px",
                                sortable: true,
                                sortField: "name",
                                width: "300px",
                                grow: 6,
                                selector: (row) => this.capitalWord(row.name),
                                cell: (row) => (
                                  <span className="d-flex flex-column my-2">
                                    <a
                                      href={"/pelatihan/view-tema/" + row.id}
                                      className="text-dark"
                                    >
                                      <span
                                        className="fw-bolder fs-7"
                                        style={{
                                          overflow: "hidden",
                                          whiteSpace: "wrap",
                                          textOverflow: "ellipses",
                                        }}
                                      >
                                        {this.capitalWord(row.name)}
                                      </span>
                                    </a>
                                  </span>
                                ),
                              },
                              {
                                name: "Akademi",
                                className: "min-w-200px min-h-50px mw-200px",
                                sortable: true,
                                sortField: "nama_akademi",
                                width: "250px",
                                grow: 6,
                                selector: (row) =>
                                  this.capitalWord(row.nama_akademi),
                                cell: (row) => (
                                  <span className="d-flex flex-column">
                                    <span
                                      className="fs-7"
                                      style={{
                                        overflow: "hidden",
                                        whiteSpace: "wrap",
                                        textOverflow: "ellipses",
                                      }}
                                    >
                                      {this.capitalWord(row.nama_akademi)}
                                    </span>
                                  </span>
                                ),
                              },
                              {
                                name: "Pelatihan",
                                sortable: true,
                                sortField: "jml_pelatihan",
                                center: true,
                                grow: 2,
                                className: "min-w-100px",
                                width: "120px",
                                selector: (row) => row.jml_pelatihan,
                                cell: (row) => {
                                  return (
                                    <span
                                      className="badge badge-light-primary fs-7 m-1"
                                      style={{
                                        overflow: "hidden",
                                        whiteSpace: "wrap",
                                        textOverflow: "ellipses",
                                      }}
                                    >
                                      {row.jml_pelatihan}
                                    </span>
                                  );
                                },
                              },
                              {
                                name: "Status",
                                sortable: true,
                                sortField: "status",
                                center: true,
                                className: "min-w-100px",
                                grow: 2,
                                selector: (row) => row.status,
                                cell: (row) => (
                                  <span className="d-flex flex-column">
                                    <span
                                      className={
                                        "badge badge-light-" +
                                        (row.status == 1
                                          ? "success"
                                          : row.status == 0
                                            ? "danger"
                                            : "warning") +
                                        " fs-7 m-1"
                                      }
                                    >
                                      {row.status == 1
                                        ? "Publish"
                                        : row.status == 0
                                          ? "Unpublish"
                                          : "Unlisted"}
                                    </span>
                                  </span>
                                ),
                              },
                              {
                                Header: "Aksi",
                                accessor: "actions",
                                name: "Aksi",
                                grow: 3,
                                center: true,
                                allowOverflow: true,
                                sortable: false,
                                width: "200px",
                                cell: (row) => (
                                  <div>
                                    <a
                                      href={"/pelatihan/view-tema/" + row.id}
                                      id={row.id}
                                      title="Lihat"
                                      alt="Lihat"
                                      className="btn btn-icon btn-bg-primary btn-sm me-1"
                                    >
                                      <i className="bi bi-eye-fill text-white"></i>
                                    </a>

                                    {cekPermition().manage === 1 ? (
                                      <>
                                        <a
                                          href={
                                            "/pelatihan/edit-tema/" + row.id
                                          }
                                          id={row.id}
                                          title="Edit"
                                          alt="Edit"
                                          className="btn btn-icon btn-bg-warning btn-sm me-1"
                                        >
                                          <i className="bi bi-gear-fill text-white"></i>
                                        </a>

                                        <a
                                          href="#"
                                          id={row.id}
                                          onClick={() => this.onRemove(row.id)}
                                          title="Hapus"
                                          alt="Hapus"
                                          className={`btn btn-icon btn-bg-danger btn-sm me-1 ${
                                            row.jml_pelatihan > 0
                                              ? "disabled"
                                              : ""
                                          }`}
                                        >
                                          <i className="bi bi-trash-fill text-white"></i>
                                        </a>
                                      </>
                                    ) : (
                                      <></>
                                    )}
                                  </div>
                                ),
                              },
                            ]}
                            data={this.state.data}
                            progressPending={this.state.loading}
                            progressComponent={
                              <div style={{ padding: "24px" }}>
                                <img src="/assets/media/loader/loader-biru.gif" />
                              </div>
                            }
                            highlightOnHover
                            pointerOnHover
                            pagination
                            paginationServer
                            paginationTotalRows={this.state.total}
                            paginationComponentOptions={{
                              selectAllRowsItem: true,
                              selectAllRowsItemText: "Semua",
                            }}
                            onChangeRowsPerPage={this.handlePerRowsChange}
                            onChangePage={this.handlePageChange}
                            onSort={this.onSort}
                            customStyles={{
                              headCells: {
                                style: {
                                  background: "rgb(243, 246, 249)",
                                },
                              },
                            }}
                            persistTableHead={true}
                            noDataComponent={
                              <div className="mt-5">Tidak Ada Data</div>
                            }
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
