import React from "react";
import Header from "../../../../components/Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../Tema-Edit-Content";
import SideNav from "../../../../components/SideNav";
import Footer from "../../../../components/Footer";

const Akademi = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default Akademi;
