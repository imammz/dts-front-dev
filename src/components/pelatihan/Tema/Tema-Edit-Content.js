import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Editor from "@arbor-dev/ckeditor5-arbor-custom";

export default class TemaContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickDelete = this.handleClickDeleteAction.bind(this);
    this.handleClick = this.handleClickAction.bind(this);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleChangeDescription =
      this.handleChangeDescriptionAction.bind(this);
    this.state = {
      fields: {},
      errors: {},
      datax: [],
      isLoading: false,
      dataxtema: [],
      selakademi: [],
      selstatus: [],
      deskripsi: "",
    };
  }
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  optionstatus = [
    { value: "1", label: "Publish" },
    { value: "0", label: "Unpublish" },
  ];
  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    let segment_url = window.location.pathname.split("/");
    let urlSegmenttOne = segment_url[3];
    Cookies.set("tema_id", urlSegmenttOne);
    let data = {
      id: urlSegmenttOne,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/cari_tema",
        data,
        this.configs,
      )
      .then((res) => {
        const dataxtema = res.data.result.Data[0];
        this.setState({ dataxtema });

        const dataAkademik = { start: 0, length: 1000, status: "publish" };
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/akademi/list-akademi",
            dataAkademik,
            this.configs,
          )
          .then((res) => {
            const optionx = res.data.result.Data;
            const datax = [];
            for (let i in optionx) {
              if (optionx[i].id === this.state.dataxtema.akademi_id) {
                this.setState({
                  selakademi: { value: optionx[i].id, label: optionx[i].name },
                });
              }
            }
            optionx.map((data) =>
              datax.push({ value: data.id, label: data.name }),
            );
            this.setState({ datax });
            this.setState({
              selstatus: {
                value: this.state.dataxtema.status,
                label:
                  this.state.dataxtema.status == "1" ? "Publish" : "Unpublish",
              },
            });
          });
      });
  }
  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    let errors = {};
    let formIsValid = true;

    if (dataForm.get("name") == "") {
      formIsValid = false;
      errors["name"] = "Tidak boleh kosong";
    }
    if (this.state.deskripsi == "") {
      formIsValid = false;
      errors["deskripsi"] = "Tidak boleh kosong";
    }
    this.setState({ errors: errors });
    return formIsValid;
  }
  resetError() {
    let errors = {};
    errors["name"] = "";
    errors["deskripsi"] = "";
    this.setState({ errors: errors });
  }
  handleClickAction(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      let updatex = 0;
      if (
        this.state.dataxtema.deskripsi != dataForm.get("deskripsi") ||
        this.state.dataxtema.status != dataForm.get("status")
      ) {
        updatex = 1;
      }
      let data = {
        update: updatex,
        id: Cookies.get("tema_id"),
        akademi_id: dataForm.get("akademi_id"),
        name: dataForm.get("name"),
        deskripsi: this.state.deskripsi.replaceAll("'", "''"),
        status: dataForm.get("status"),
      };
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/update_tema",
          data,
          this.configs,
        )
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            this.setState({ isLoading: false });
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/pelatihan/tema";
                }
              });
          } else {
            this.setState({ isLoading: false });
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      this.setState({ isLoading: false });
    }
  }
  handleClickBatalAction(e) {
    window.location = "/pelatihan/tema";
  }
  handleChangeAkademiAction = (selectedOption) => {
    this.setState({
      selakademi: { value: selectedOption.value, label: selectedOption.label },
    });
  };
  handleChangeStatusAction = (selectedOption) => {
    this.setState({
      selstatus: { value: selectedOption.value, label: selectedOption.label },
    });
  };
  handleChangeDescriptionAction(value) {
    this.state.deskripsi = value;
  }
  handleClickDeleteAction(e) {
    const idx = e.currentTarget.id;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "info",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          let data = { id: idx };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/hapus_tema",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.StatusCode;
              const messagex = res.data.result.Message;
              if ("200" == statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      window.location = "/pelatihan/tema";
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "error",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload();
                    }
                  });
              }
            });
        }
      });
  }
  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Tema
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
                <a
                  href="#"
                  id={Cookies.get("tema_id")}
                  onClick={this.handleClickDelete}
                  className="btn btn-sm btn-danger btn-active-light-info me-2"
                >
                  <i className="bi bi-trash-fill text-white"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Edit Tema
                          </h1>
                        </div>
                      </div>
                      <div className="card-body">
                        <form
                          id="kt_modal_add_permission_form"
                          className="form"
                          action="#"
                          onSubmit={this.handleClick}
                        >
                          <div className="col-lg-12 mb-7 fv-row">
                            <label className="form-label required">
                              Akademi
                            </label>
                            <Select
                              name="akademi_id"
                              placeholder="Silahkan pilih"
                              noOptionsMessage={({ inputValue }) =>
                                !inputValue
                                  ? this.state.datax
                                  : "Data tidak tersedia"
                              }
                              className="form-select-sm selectpicker p-0"
                              isOptionSelected={
                                this.state.dataxtema.akademi_id ==
                                this.state.datax.value
                              }
                              options={this.state.datax}
                              value={this.state.selakademi}
                              onChange={this.handleChangeAkademiAction}
                            />
                          </div>
                          <div className="col-lg-12 mb-7 fv-row">
                            <label className="form-label required">
                              Nama Tema
                            </label>
                            <input
                              type="text"
                              className="form-control form-control-sm"
                              placeholder="Masukkan nama tema"
                              name="name"
                              defaultValue={this.state.dataxtema.name}
                            />
                            <span className="text-muted fs-8 d-block">
                              Tema tidak boleh mengandung tahun
                            </span>
                            <span style={{ color: "red" }}>
                              {this.state.errors["name"]}
                            </span>
                          </div>
                          <div className="col-lg-12 mb-7 fv-row">
                            <label className="form-label required">
                              Deskripsi
                            </label>
                            {/* <textarea className="form-control form-control-sm" placeholder="Masukkan deskripsi" name="deskripsi" defaultValue={this.state.dataxtema.deskripsi} /> */}
                            <CKEditor
                              editor={Editor}
                              data={this.state.dataxtema.deskripsi}
                              name="deskripsi"
                              onReady={(editor) => {}}
                              config={{
                                ckfinder: {
                                  // Upload the images to the server using the CKFinder QuickUpload command.
                                  uploadUrl:
                                    process.env.REACT_APP_BASE_API_URI +
                                    "/pelatihan/ckeditor-upload",
                                },
                              }}
                              onChange={(event, editor) => {
                                const data = editor.getData();
                                // console.log( { event, editor, data } );
                                this.handleChangeDescription(data);
                              }}
                              onBlur={(event, editor) => {
                                // console.log( 'Blur.', editor );
                              }}
                              onFocus={(event, editor) => {
                                // console.log( 'Focus.', editor );
                              }}
                            />
                            <span style={{ color: "red" }}>
                              {this.state.errors["deskripsi"]}
                            </span>
                          </div>
                          <div className="col-lg-12 mb-7 fv-row">
                            <label className="form-label required">
                              Status
                            </label>
                            <Select
                              name="status"
                              placeholder="Silahkan pilih"
                              noOptionsMessage={({ inputValue }) =>
                                !inputValue
                                  ? this.state.datax
                                  : "Data tidak tersedia"
                              }
                              className="form-select-sm selectpicker p-0"
                              options={this.optionstatus}
                              value={this.state.selstatus}
                              onChange={this.handleChangeStatusAction}
                            />
                          </div>
                          <div className="form-group fv-row pt-7 mb-7">
                            <div className="d-flex justify-content-center mb-7">
                              <button
                                onClick={this.handleClickBatal}
                                type="reset"
                                className="btn btn-md btn-light me-3"
                                data-kt-menu-dismiss="true"
                              >
                                Batal
                              </button>
                              <button
                                disabled={this.state.isLoading}
                                type="submit"
                                className="btn btn-primary btn-md"
                                id="submitQuestion1"
                              >
                                {this.state.isLoading ? (
                                  <>
                                    <span
                                      className="spinner-border spinner-border-sm me-2"
                                      role="status"
                                      aria-hidden="true"
                                    ></span>
                                    <span className="sr-only">Loading...</span>
                                    Loading...
                                  </>
                                ) : (
                                  <>
                                    <i className="fa fa-paper-plane me-1"></i>
                                    Simpan
                                  </>
                                )}
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                {/*end::Modal - Update permissions*/}
                {/*end::Modals*/}
              </div>
              {/*end::Container*/}
            </div>
            {/*end::Post*/}
          </div>
        </div>
      </div>
    );
  }
}
