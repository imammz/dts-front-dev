import React, { Component } from "react";
import { capitalizeFirstLetter } from "../publikasi/helper";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import moment from "moment";
import "moment/locale/id";
import Flatpickr from "react-flatpickr";
import { Indonesian } from "flatpickr/dist/l10n/id.js";

const MySwal = withReactContent(Swal);

export function loadTrigered_val(
  triggered_name,
  data_option,
  key_triggered_name,
) {
  data_option.split(";").map((row_opsi) => {
    console.log(row_opsi + " = " + key_triggered_name);

    try {
      if (row_opsi == key_triggered_name) {
        document.getElementById(
          triggered_name + "#" + key_triggered_name,
        ).style.display = "block";
      } else {
        document.getElementById(triggered_name + "#" + row_opsi).style.display =
          "none";
      }
    } catch (err) {
      console.log(err);
    }
  });
}

export function loadTrigred_rekursive(val) {
  let isShown = val.triggered == 1 ? false : true;

  // console.log(val)
  return (
    <div
      className="alert bg-gray-100 alert-custom alert-default rounded-3 mt-1 border-dashed mb-3"
      id={val.triggered_name + "#" + val.key_triggered_name}
      style={{ display: isShown ? "block" : "none" }}
    >
      {val.element == "trigered" ? (
        <>
          <div className="row">
            <div className="col-lg-12">
              <div className="form-group my-3">
                <div
                  className="row align-items-end aos-init aos-animate"
                  data-aos="fade-up"
                >
                  <div className="$size mb-1">
                    <label className={val.className}>
                      {" "}
                      <b>{val.name}</b>{" "}
                      {val.required == "required" ? (
                        <span className="pl-5  align-self-end  font-weight-bold font-size-lg text-danger">
                          {" "}
                          *{" "}
                        </span>
                      ) : (
                        <span></span>
                      )}
                    </label>
                  </div>
                </div>
                <div className="d-flex">
                  <select
                    className="form-control form-control-sm form-select-sm selectpicker"
                    onChange={(event) =>
                      loadTrigered_val(
                        val.name,
                        val.data_option,
                        event.target.value,
                      )
                    }
                    data-control="select2"
                    data-placeholder="Select an option"
                    name={val.file_name}
                  >
                    <option value=""> -- Pilih -- </option>
                    {val.data_option.split(";").map((row_opsi, idx_opsi) => (
                      <option value={capitalizeFirstLetter(row_opsi)}>
                        {capitalizeFirstLetter(row_opsi)}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
            </div>

            <div className="col-lg-12 mt-n3">
              <>
                {" "}
                <sub className="form-caption fw-semibold text-muted">
                  {" "}
                  {capitalizeFirstLetter(val.span)}{" "}
                </sub>{" "}
              </>
              <div className="row">
                <div className="col-lg-12">
                  <br />
                </div>
              </div>
            </div>
          </div>

          {val.child != undefined ? (
            val.child.map((valChild) => <>{loadTrigred_rekursive(valChild)}</>)
          ) : (
            <span></span>
          )}
        </>
      ) : val.element == "textarea" ? (
        <>
          <div className="row">
            <div className="col-lg-12">
              <div className="form-group my-3">
                <div
                  className="row align-items-end aos-init aos-animate"
                  data-aos="fade-up"
                >
                  <div className={val.size + " mb-1"}>
                    <label className="{val.className}">
                      <b>{val.name}</b>{" "}
                      {val.required == "required" ? (
                        <span className="pl-5 align-self-end  font-weight-bold font-size-lg text-danger">
                          {" "}
                          *{" "}
                        </span>
                      ) : (
                        <span></span>
                      )}
                    </label>
                  </div>
                </div>
                <textarea
                  className="form-control form-control-sm"
                  placeholder={"Masukkan" + val.name}
                  name={val.file_name}
                  rows="10"
                  id={val.file_name}
                  // required={val.required}
                ></textarea>
                <sub className="form-caption fw-semibold my-2 text-muted">
                  {" "}
                  {capitalizeFirstLetter(val.span)}{" "}
                </sub>
              </div>
            </div>
          </div>
        </>
      ) : val.element == "radiogroup" ? (
        <>
          <div className="row">
            <div className="col-lg-12">
              <div className="form-group my-3">
                <div
                  className="row align-items-end aos-init aos-animate"
                  data-aos="fade-up"
                >
                  <div className={val.size + " mb-1"}>
                    <label className="{val.className}">
                      <span className="pl-5  align-self-end text-red font-weight-medium font-size-lg">
                        &nbsp;
                      </span>
                      <b>{val.name}</b>{" "}
                      {val.required == "required" ? (
                        <span className="pl-5  align-self-end  font-weight-bold font-size-lg text-danger">
                          {" "}
                          *{" "}
                        </span>
                      ) : (
                        <span></span>
                      )}
                    </label>
                  </div>
                </div>
                <div>
                  <div className="row">
                    {val.option == "referensi"
                      ? Array.isArray(val.ref_values)
                        ? val.ref_values.map((row_opsi, idx_opsi) => (
                            <>
                              <div className="col-lg-3">
                                <div className="form-check form-check-sm form-check-custom form-check-solid me-3">
                                  <input
                                    className="form-check-input"
                                    type="radio"
                                    name={val.file_name}
                                    value={capitalizeFirstLetter(row_opsi.id)}
                                    id={val.file_name}
                                  />
                                  <label
                                    className="form-check-label font-size-sm"
                                    for={val.file_name}
                                  >
                                    {capitalizeFirstLetter(row_opsi.value)}
                                  </label>
                                </div>
                              </div>
                            </>
                          ))
                        : null
                      : val.data_option.split(";").map((row_opsi, idx_opsi) => (
                          <>
                            <div className="col-lg-3">
                              <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                <input
                                  className="form-check-input"
                                  type="radio"
                                  name={val.file_name}
                                  value={capitalizeFirstLetter(row_opsi)}
                                  id={val.file_name}
                                />
                                <label
                                  className="form-check-label font-size-sm"
                                  for={val.file_name}
                                >
                                  {capitalizeFirstLetter(row_opsi)}
                                </label>
                              </div>
                            </div>
                          </>
                        ))}
                    <sub className="form-caption fw-semibold my-3 text-muted">
                      {" "}
                      {capitalizeFirstLetter(val.span)}{" "}
                    </sub>
                  </div>
                </div>
              </div>
            </div>{" "}
          </div>
        </>
      ) : val.element == "uploadfiles" ? (
        <>
          <div className="row">
            <div className="col-lg-12">
              <div className="{val.size} mb-7">
                <label for="{val.className}" style="display:none">
                  <b>{val.name}</b>
                  {val.required == "required" ? (
                    <span className="pl-5  align-self-end  font-weight-bold font-size-lg text-danger">
                      {" "}
                      *{" "}
                    </span>
                  ) : (
                    <span></span>
                  )}
                </label>
                <br />
                <a
                  href={val.data_option}
                  name={val.file_name}
                  id={val.file_name}
                  // required={val.required}
                >
                  Download
                </a>

                <div className="row">
                  <div className="col-lg-4">
                    <span className="form-caption font-size-sm text-italic my-2 text-primary">
                      {" "}
                      <sup>
                        {" "}
                        <strong>{capitalizeFirstLetter(val.span)}</strong>{" "}
                      </sup>{" "}
                    </span>
                  </div>
                  <div className="col-lg-8" style={{ textAlign: "right" }}>
                    {val.data_option != "" ? (
                      <span>
                        <a
                          href={
                            process.env.REACT_APP_BASE_API_URI +
                            "/download/get-file?path=" +
                            val.data_option +
                            "&disk=dts-storage-pelatihan"
                          }
                          target={"_blank"}
                          className="text-primary mr-3"
                        >
                          <i className="icon-sm fas fa-download"></i>{" "}
                          <b> Unduh Template {val.data_option.lenght} </b>{" "}
                        </a>
                      </span>
                    ) : (
                      <></>
                    )}
                  </div>
                </div>

                <div className="invalid-feedback"></div>
              </div>
            </div>{" "}
          </div>
        </>
      ) : val.element == "datepicker" ? (
        <>
          <div className="row">
            <div className="col-lg-12">
              <div className="form-group my-3">
                <div
                  className="row align-items-end aos-init aos-animate"
                  data-aos="fade-up"
                >
                  <div className={val.size + " mb-1"}>
                    <label>
                      <b>{val.name}</b>{" "}
                      {val.required == "required" ? (
                        <span className="pl-5  align-self-end  font-weight-bold font-size-lg text-danger">
                          {" "}
                          *{" "}
                        </span>
                      ) : (
                        <span></span>
                      )}
                    </label>

                    <Flatpickr
                      options={{
                        locale: Indonesian,
                        dateFormat: "d F Y",
                        enableTime: "false",
                      }}
                      className="form-control form-control-sm"
                      placeholder={"Masukan tanggal"}
                      name={val.file_name}
                      id={val.file_name}
                    />

                    <sub className="form-caption fw-semibold my-2 text-muted">
                      {" "}
                      {capitalizeFirstLetter(val.span)}{" "}
                    </sub>
                    <div className="invalid-feedback"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : val.element == "checkbox" ? (
        <>
          <div className="row">
            <div className="col-lg-12">
              <div className="form-group my-3">
                <div
                  className="row align-items-end aos-init aos-animate"
                  data-aos="fade-up"
                >
                  <div className={val.size + " mb-1"}>
                    <label>
                      <b>{val.name}</b>{" "}
                      {val.required == "required" ? (
                        <span className="pl-5  align-self-end  font-weight-bold font-size-lg text-danger">
                          {" "}
                          *{" "}
                        </span>
                      ) : (
                        <span></span>
                      )}
                    </label>
                  </div>
                </div>
                <div>
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="form-check form-check-sm form-check-custom form-check-solid me-5 row">
                        {val.option === "referensi"
                          ? Array.isArray(val.ref_values)
                            ? val.ref_values.map((row_opsi, idx_opsi) => (
                                <>
                                  <div
                                    className="col-lg-3"
                                    style={{ paddingTop: "5px" }}
                                  >
                                    <input
                                      className="form-check-input"
                                      type="checkbox"
                                      name={val.file_name}
                                      value={capitalizeFirstLetter(row_opsi.id)}
                                      id={val.file_name}
                                    />
                                    <label
                                      className="form-check-label"
                                      for={val.file_name}
                                    >
                                      {capitalizeFirstLetter(row_opsi.value)}
                                    </label>
                                  </div>
                                </>
                              ))
                            : null
                          : val.data_option
                              .split(";")
                              .map((row_opsi, idx_opsi) => (
                                <>
                                  <div
                                    className="col-lg-3"
                                    style={{ paddingTop: "5px" }}
                                  >
                                    <input
                                      className="form-check-input"
                                      type="checkbox"
                                      name={val.file_name}
                                      value={capitalizeFirstLetter(row_opsi)}
                                      id={val.file_name}
                                    />
                                    <label
                                      className="form-check-label"
                                      for={val.file_name}
                                    >
                                      {capitalizeFirstLetter(row_opsi)}
                                    </label>
                                  </div>
                                </>
                              ))}
                      </div>
                      <sub className="form-caption fw-semibold my-2 text-muted">
                        {" "}
                        {capitalizeFirstLetter(val.span)}{" "}
                      </sub>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : val.element == "fileimage" ? (
        <>
          <div className="row">
            <div className="col-lg-12">
              <div className="{val.size} my-3">
                <div className="row">
                  <div className="col-lg-8">
                    <label className="mb-1">
                      <b>{val.name}</b>{" "}
                      {val.required == "required" ? (
                        <span className="pl-5  align-self-end  \font-weight-bold font-size-lg text-danger">
                          {" "}
                          *{" "}
                        </span>
                      ) : (
                        <span></span>
                      )}{" "}
                    </label>
                  </div>
                  <div className="col-lg-4" style={{ textAlign: "right" }}>
                    {val.data_option != "" ? (
                      <span>
                        <a
                          href={
                            process.env.REACT_APP_BASE_API_URI +
                            "/download/get-file?path=" +
                            val.data_option +
                            "&disk=dts-storage-pelatihan"
                          }
                          target={"_blank"}
                          className="text-primary  mr-3"
                        >
                          <i className="icon-sm fas fa-download"></i>{" "}
                          <b> Unduh Template {val.data_option.lenght} </b>{" "}
                        </a>
                      </span>
                    ) : (
                      <></>
                    )}
                  </div>
                </div>

                <input
                  type="file"
                  className="form-control form-control-sm mb-1"
                  name={val.file_name}
                  id={val.file_name}
                  // required={val.required}
                  accept=".png, .jpg, .jpeg, .svg"
                />

                <div className="row">
                  <div className="col-lg-12">
                    <sub className="form-caption fw-semibold text-muted">
                      {" "}
                      {capitalizeFirstLetter(val.span)}{" "}
                    </sub>
                  </div>
                </div>

                <div className="invalid-feedback"></div>
              </div>
            </div>
          </div>
        </>
      ) : val.element == "file-doc" ? (
        <>
          <div className="row">
            <div className="col-lg-12 my-3">
              <div className="{val.size} mb-1">
                <div className="row">
                  <div className="col-lg-8 mb-1">
                    <label>
                      <b>{val.name}</b>{" "}
                      {val.required == "required" ? (
                        <span className="pl-5  align-self-end  font-weight-bold font-size-lg text-danger">
                          {" "}
                          *{" "}
                        </span>
                      ) : (
                        <span></span>
                      )}{" "}
                    </label>
                  </div>
                  <div className="col-lg-4" style={{ textAlign: "right" }}>
                    {val.data_option != "" ? (
                      <span>
                        <a
                          href={
                            process.env.REACT_APP_BASE_API_URI +
                            "/download/get-file?path=" +
                            val.data_option +
                            "&disk=dts-storage-pelatihan"
                          }
                          target={"_blank"}
                          className="text-primary font-size-sm mr-3"
                        >
                          <i className="icon-sm text-primary fas fa-download"></i>{" "}
                          <b> Unduh Template {val.data_option.lenght} </b>{" "}
                        </a>
                      </span>
                    ) : (
                      <></>
                    )}
                  </div>
                </div>
                <input
                  type="file"
                  className="form-control form-control-sm mb-1"
                  name={val.file_name}
                  id={val.file_name}
                  // required={val.required}
                  accept=".pdf"
                />

                <div className="row">
                  <div className="col-lg-12">
                    <sub className="form-caption fw-semibold my-2 text-muted">
                      {" "}
                      {capitalizeFirstLetter(val.span)}{" "}
                    </sub>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : val.element == "text" ? (
        <>
          <div className="row">
            <div className="col-lg-12">
              <div className="form-group my-3">
                <div
                  className="row align-items-end aos-init aos-animate"
                  data-aos="fade-up"
                >
                  <div className={val.size + " mb-1"}>
                    <label className="mb-0">
                      <b>{val.name}</b>{" "}
                      {val.required == "required" ? (
                        <span className="pl-5  align-self-end  font-weight-bold font-size-lg text-danger">
                          {" "}
                          *{" "}
                        </span>
                      ) : (
                        <span></span>
                      )}{" "}
                    </label>
                    &nbsp;
                    <span
                      className="form-caption font-size-sm text-italic my-2"
                      style={{ color: "#f55" }}
                    >
                      {" "}
                      <strong>
                        {" "}
                        <sub id={"sub_" + val.file_name}> </sub>{" "}
                      </strong>{" "}
                    </span>
                  </div>
                </div>
                <input
                  className="form-control form-control-sm"
                  placeholder={"Masukkan  " + val.name}
                  name={val.file_name}
                  id={val.file_name}
                  onChange={(event) =>
                    handleMinMax(
                      val.min,
                      val.max,
                      event.target.value,
                      val.file_name,
                    )
                  }
                />
                <sub className="form-caption fw-semibold my-2 text-muted">
                  {" "}
                  {capitalizeFirstLetter(val.span)}{" "}
                </sub>
              </div>
            </div>
          </div>
        </>
      ) : val.element == "text-email" ? (
        <>
          <div className="row">
            <div className="col-lg-12">
              <div className="form-group my-3">
                <div
                  className="row align-items-end aos-init aos-animate"
                  data-aos="fade-up"
                >
                  <div className={val.size + " mb-1"}>
                    <label>
                      <b>{val.name}</b>{" "}
                      {val.required == "required" ? (
                        <span className="pl-5  align-self-end  font-weight-bold font-size-lg text-danger">
                          {" "}
                          *{" "}
                        </span>
                      ) : (
                        <span></span>
                      )}{" "}
                    </label>
                    &nbsp;
                    <span
                      className="form-caption font-size-sm text-italic my-2"
                      style={{ color: "#f55" }}
                    >
                      {" "}
                      <strong>
                        {" "}
                        <sub id={"sub_" + val.file_name}> </sub>{" "}
                      </strong>{" "}
                    </span>
                  </div>
                </div>
                <input
                  className="form-control form-control-sm"
                  placeholder={"Masukkan  " + val.name}
                  name={val.file_name}
                  type="email"
                  id={val.file_name}
                  onChange={(event) =>
                    handleMinMax(
                      val.min,
                      val.max,
                      event.target.value,
                      val.file_name,
                      val.element,
                    )
                  }
                />
                <sub className="form-caption fw-semibold my-2 text-muted">
                  {" "}
                  {capitalizeFirstLetter(val.span)}{" "}
                </sub>
              </div>
            </div>
          </div>
        </>
      ) : val.element == "text-numeric" ? (
        <>
          <div className="row">
            <div className="col-lg-12">
              <div className="form-group my-3">
                <div
                  className="row align-items-end aos-init aos-animate"
                  data-aos="fade-up"
                >
                  <div className={val.size + " mb-1"}>
                    <label>
                      <b>{val.name}</b>{" "}
                      {val.required == "required" ? (
                        <span className="pl-5  align-self-end  font-weight-bold font-size-lg text-danger">
                          {" "}
                          *{" "}
                        </span>
                      ) : (
                        <span></span>
                      )}{" "}
                    </label>
                    &nbsp;
                    <span
                      className="form-caption font-size-sm text-italic my-2"
                      style={{ color: "#f55" }}
                    >
                      {" "}
                      <strong>
                        {" "}
                        <sub id={"sub_" + val.file_name}> </sub>{" "}
                      </strong>{" "}
                    </span>
                  </div>
                </div>
                <input
                  className="form-control form-control-sm"
                  placeholder={"Masukkan  " + val.name}
                  name={val.file_name}
                  type="number"
                  id={val.file_name}
                  onChange={(event) =>
                    handleMinMax(
                      val.min,
                      val.max,
                      event.target.value,
                      val.file_name,
                    )
                  }
                />

                <sub className="form-caption fw-semibold my-2 text-muted">
                  {" "}
                  {capitalizeFirstLetter(val.span)}{" "}
                </sub>
              </div>
            </div>
          </div>
        </>
      ) : val.element == "text-URL" ? (
        <>
          <div className="row">
            <div className="col-lg-12">
              <div className="form-group my-3">
                <div
                  className="row align-items-end aos-init aos-animate"
                  data-aos="fade-up"
                >
                  <div className={val.size + " mb-1"}>
                    <label>
                      <b>{val.name}</b>{" "}
                      {val.required == "required" ? (
                        <span className="pl-5  align-self-end  font-weight-bold font-size-lg text-danger">
                          {" "}
                          *{" "}
                        </span>
                      ) : (
                        <span></span>
                      )}{" "}
                    </label>
                    &nbsp;
                    <span
                      className="form-caption font-size-sm text-italic my-2"
                      style={{ color: "#f55" }}
                    >
                      {" "}
                      <strong>
                        {" "}
                        <sub id={"sub_" + val.file_name}> </sub>{" "}
                      </strong>{" "}
                    </span>
                  </div>
                </div>
                <input
                  className="form-control form-control-sm"
                  placeholder={"Masukkan  " + val.name}
                  name={val.file_name}
                  type="url"
                  id={val.file_name}
                  onChange={(event) =>
                    handleMinMax(
                      val.min,
                      val.max,
                      event.target.value,
                      val.file_name,
                      val.element,
                    )
                  }
                />
                <sub className="form-caption fw-semibold my-2 text-muted">
                  {" "}
                  {capitalizeFirstLetter(val.span)}{" "}
                </sub>
              </div>
            </div>
          </div>
        </>
      ) : val.element == "select" ? (
        <>
          <div className="row">
            <div className="col-lg-12">
              <div className="form-group my-3">
                <div
                  className="row align-items-end aos-init aos-animate"
                  data-aos="fade-up"
                >
                  <div className={val.size + " mb-1"}>
                    <label className="{val.className}">
                      <span className="pl-5  align-self-end text-red font-weight-medium font-size-lg">
                        &nbsp;
                      </span>
                      <b>{val.name}</b>{" "}
                      {val.required == "required" ? (
                        <span className="pl-5  align-self-end  font-weight-bold font-size-lg text-danger">
                          {" "}
                          *{" "}
                        </span>
                      ) : (
                        <span></span>
                      )}
                    </label>
                  </div>
                </div>
                <div>
                  <select
                    className="form-control form-control-sm form-select-sm selectpicker"
                    // required={val.required}
                    data-control="select2"
                    data-placeholder="Select an option"
                    name={val.file_name}
                  >
                    <option> -- Pilih {val.name} -- </option>

                    {val.option == "referensi"
                      ? Array.isArray(val.ref_values)
                        ? val.ref_values.map((row_opsi, idx_opsi) => (
                            <option value={capitalizeFirstLetter(row_opsi.id)}>
                              {capitalizeFirstLetter(row_opsi.value)}
                            </option>
                          ))
                        : null
                      : val.data_option
                          .split(";")
                          .map((row_opsi, idx_opsi) => (
                            <option value={capitalizeFirstLetter(row_opsi)}>
                              {capitalizeFirstLetter(row_opsi)}
                            </option>
                          ))}
                  </select>
                  <sub className="form-caption fw-semibold my-2 text-muted">
                    {" "}
                    {capitalizeFirstLetter(val.span)}{" "}
                  </sub>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : (
        <>
          <div dangerouslySetInnerHTML={{ __html: val.html }}></div>
          <span
            style={{ marginTop: "-20px" }}
            className="form-caption font-size-sm text-italic my-2 text-muted"
          >
            {" "}
            <sup>
              {" "}
              <strong>{capitalizeFirstLetter(val.span)}</strong>{" "}
            </sup>{" "}
          </span>
        </>
      )}
    </div>
  );
}

export function handleMinMax(min, max, value, id, element = "") {
  let val = value.length;

  if (min != "" && min != 0 && min != undefined) {
    if (val < min) {
      document.getElementById(id).style.backgroundColor = "#ffdede";
      document.getElementById("sub_" + id).innerHTML =
        "(Minimal " + min + " karakter)";

      return false;
    } else {
      document.getElementById(id).style.backgroundColor = "#ffffff";
      document.getElementById("sub_" + id).innerHTML = "";
    }
  }

  if (max != "" && max != 0 && max != undefined) {
    if (val > max) {
      MySwal.fire({
        title: <strong>Perhatian!</strong>,
        html: <i>Tidak boleh lebih dari {max} karakter </i>,
        icon: "warning",
      });
      document.getElementById(id).style.backgroundColor = "#ffdede";
      document.getElementById(id).value = value.substring(0, max);
      document.getElementById("sub_" + id).innerHTML =
        "(Maksimal " + max + " karakter)";
      return false;
    } else {
      document.getElementById(id).style.backgroundColor = "#ffffff";
      document.getElementById("sub_" + id).innerHTML = "";
    }
  }

  if (element == "text-email") {
    handleEmail(value, id);
  }

  if (element == "text-URL") {
    handleURL(value, id);
  }
}

export function handleEmail(value, id) {
  var validRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

  if (value != "") {
    if (value.match(validRegex)) {
      document.getElementById(id).style.backgroundColor = "#ffffff";
      document.getElementById("sub_" + id).innerHTML = "";

      return true;
    } else {
      document.getElementById(id).style.backgroundColor = "#ffdede";
      document.getElementById("sub_" + id).innerHTML = "Format email salah";

      return false;
    }
  } else {
    document.getElementById(id).style.backgroundColor = "#ffffff";
    document.getElementById("sub_" + id).innerHTML = "";
  }
}

export function handleURL(value, id) {
  var validRegex =
    /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/;

  if (value != "") {
    if (value.match(validRegex)) {
      document.getElementById(id).style.backgroundColor = "#ffffff";
      document.getElementById("sub_" + id).innerHTML = "";

      return true;
    } else {
      document.getElementById(id).style.backgroundColor = "#ffdede";
      document.getElementById("sub_" + id).innerHTML = "Format URL salah";

      return false;
    }
  } else {
    document.getElementById(id).style.backgroundColor = "#ffffff";
    document.getElementById("sub_" + id).innerHTML = "";
  }
}

export function formatIndonesianDateRange(dateRange, separator = "-") {
  moment.locale("id");
  try {
    return (
      moment(dateRange.split(separator)[0]).format("DD MMMM YYYY") +
      " - " +
      moment(dateRange.split(separator)[1]).format("DD MMMM YYYY")
    );
  } catch (err) {
    return dateRange;
  }
}
