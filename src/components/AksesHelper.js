import React, { Component } from "react";

import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import "moment/locale/id";
import Cookies from "js-cookie";

const MySwal = withReactContent(Swal);
const Menus = JSON.parse(localStorage.getItem("dataMenus"));

export function cekPermition(segment_url_param = null) {
  let akses = {};
  let segment_url = window.location.pathname.split("/");

  if (segment_url_param != null) {
    segment_url = segment_url_param;
  }

  Menus.map((row) => {
    row.child.map((rowChild) => {
      if ("/" + segment_url[1] + "/" + segment_url[2] === rowChild.link) {
        akses = {
          menu: rowChild.menu_name,
          view: rowChild.view,
          manage: rowChild.manage,
          publish: rowChild.publish,
        };
      }

      rowChild.child.map((rowChild2) => {
        if ("/" + segment_url[1] + "/" + segment_url[2] === rowChild2.link) {
          akses = {
            menu: rowChild2.menu_name,
            view: rowChild2.view,
            manage: rowChild2.manage,
            publish: rowChild2.publish,
          };
        }
      });
    });
  });

  return akses;
}

export function logout() {
  Cookies.remove("partner_id");
  Cookies.remove("token");
  Cookies.remove("user_id");
  Cookies.remove("user_name");
  Cookies.remove("user_role");
  Cookies.remove("menus");
  Cookies.remove("mrole_id");
  Cookies.remove("satker_id");
  Cookies.remove("satker_name");

  window.location.href = "/";
}

export const isSuperAdmin = () => {
  return Cookies.get("mrole_id") == 1;
};
export const isAdminAkademi = () => {
  return Cookies.get("mrole_id") == 107;
};
export const isExceptionRole = () => {
  return (
    Cookies.get("mrole_id") == 107 ||
    Cookies.get("mrole_id") == 1 ||
    Cookies.get("mrole_id") == 109 ||
    Cookies.get("mrole_id") == 110 ||
    Cookies.get("mrole_id") == 86
  );
};
export const isAdministrator = () => {
  return Cookies.get("user_role") == "Administrator";
};
