import Header from "../../../components/Header";
import SideNav from "../../../components/SideNav";
import Footer from "../../../components/Footer";
import Content from "./content/ListKerjasama";
import { useEffect } from "react";
import { cekPermition, logout } from "../../AksesHelper";
import Swal from "sweetalert2";

function KerjasamaReview() {
  useEffect(() => {
    if (cekPermition().view !== 1) {
      Swal.fire({
        title: "Akses Tidak Diizinkan",
        html: "<i> Anda akan kembali kehalaman login </i>",
        icon: "warning",
      }).then(() => {
        logout();
      });
    }
  }, []);

  return (
    <div>
      <Header></Header>
      <SideNav></SideNav>
      <Content></Content>
      <Footer></Footer>
    </div>
  );
}

export default KerjasamaReview;
