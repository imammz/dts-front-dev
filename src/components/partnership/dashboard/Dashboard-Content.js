import React, { useEffect, useRef, useState } from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import DataTable from "react-data-table-component";

class DashboardContent extends React.Component {
  constructor(props) {
    super(props);
  }
  capitalWord(str) {
    return str.replace(/\w\S*/g, function (kata) {
      const kataBaru = kata.slice(0, 1).toUpperCase() + kata.substr(1);
      return kataBaru;
    });
  }
  state = {
    API_Dashboard: [],
  };
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  customLoader = (
    <div style={{ padding: "24px" }}>
      <img src="/assets/media/loader/loader-biru.gif" />
    </div>
  );

  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };
  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    const dataPenyelenggara = {};
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/masterKerjasama/API_Dashboard",
        dataPenyelenggara,
        this.configs,
      )
      .then((res) => {
        const API_Dashboard = res.data.result.Data;
        this.setState({ API_Dashboard });
      });
  }
  handleReload(from, param, page, newPerPage) {
    console.log(
      "from :" +
        from +
        ", param :" +
        param +
        ", page :" +
        page +
        ", newPerPage :" +
        newPerPage,
    );
    console.log("numberrow :" + this.state.numberrow);
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - 10;
      this.setState({ numberrow: start_tmp + 1 });
    }

    if ("home" == from) {
      this.setState({ from: "home" });
      let dataBody = {
        mulai: start_tmp,
        limit: length_tmp,
        id_penyelenggara:
          this.state.dataxpenyelenggaravalue.value == null
            ? 0
            : this.state.dataxpenyelenggaravalue.value,
        id_akademi:
          this.state.dataxakademivalue.value == null
            ? 0
            : this.state.dataxakademivalue.value,
        id_tema:
          this.state.dataxtemavalue.value == null
            ? 0
            : this.state.dataxtemavalue.value,
        status_substansi:
          this.state.dataxstatussubstansivalue.value == null
            ? 0
            : this.state.dataxstatussubstansivalue.value,
        status_pelatihan:
          this.state.dataxstatuspelatihanvalue.value == null
            ? 0
            : this.state.dataxstatuspelatihanvalue.value,
        status_publish:
          this.state.dataxstatuspublishvalue.value == null
            ? 99
            : this.state.dataxstatuspublishvalue.value,
        provinsi:
          this.state.dataxprovinsivalue.value == null
            ? 0
            : this.state.dataxprovinsivalue.value,
        param: param,
        sort:
          this.state.dataxsortvalue == null
            ? this.state.issearch
              ? "pelatihan"
              : "id_pelatihan"
            : this.state.dataxsortvalue,
        sortval:
          this.state.dataxsortvalvalue == null
            ? this.state.issearch
              ? "ASC"
              : "DESC"
            : this.state.dataxsortvalvalue.toUpperCase(),
      };
      if (this.state.dataxtahunvalue.value == null) {
        dataBody.tahun = this.state.currentyear;
      } else {
        dataBody.tahun = this.state.dataxtahunvalue.value;
      }

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/pelatihan",
          dataBody,
          this.configs,
        )
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            this.setState({ numberrow: start_tmp + 1 });
            const datax = res.data.result.Data;
            this.setState({ datax: datax });
            this.setState({ loading: false });
            this.setState({ totalRows: res.data.result.TotalData });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  this.setState({ datax: [] });
                  this.setState({ loading: false });
                }
              });
          }

          let review = 0;
          let revisi = 0;
          let disetujui = 0;
          let ditolak = 0;

          //review
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/boxdash1x",
              dataBody,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                review = res.data.result.TotalData;
                this.setState({ review });
              } else {
                this.setState({ review });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                this.setState({ review });
              }
            });

          //revisi
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/boxdash2x",
              dataBody,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                revisi = res.data.result.TotalData;
                this.setState({ revisi });
              } else {
                this.setState({ revisi });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                this.setState({ revisi });
              }
            });

          //disetujui
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/boxdash3x",
              dataBody,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                disetujui = res.data.result.TotalData;
                this.setState({ disetujui });
              } else {
                this.setState({ disetujui });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                this.setState({ disetujui });
              }
            });

          //ditolak
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/boxdash4x",
              dataBody,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                ditolak = res.data.result.TotalData;
                this.setState({ ditolak });
              } else {
                this.setState({ ditolak });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                this.setState({ ditolak });
              }
            });
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  this.setState({ datax: [] });
                  this.setState({ loading: false });
                }
              });
          }
        });
    } else if ("review" == from) {
      this.setState({ from: "review" });
      let dataFilter = {
        mulai: start_tmp,
        limit: length_tmp,
        id_penyelenggara:
          this.state.dataxpenyelenggaravalue.value == null
            ? 0
            : this.state.dataxpenyelenggaravalue.value,
        id_akademi:
          this.state.dataxakademivalue.value == null
            ? 0
            : this.state.dataxakademivalue.value,
        id_tema:
          this.state.dataxtemavalue.value == null
            ? 0
            : this.state.dataxtemavalue.value,
        status_substansi:
          this.state.dataxstatussubstansivalue.value == null
            ? 0
            : this.state.dataxstatussubstansivalue.value,
        status_pelatihan:
          this.state.dataxstatuspelatihanvalue.value == null
            ? 0
            : this.state.dataxstatuspelatihanvalue.value,
        status_publish:
          this.state.dataxstatuspublishvalue.value == null
            ? 99
            : this.state.dataxstatuspublishvalue.value,
        provinsi:
          this.state.dataxprovinsivalue.value == null
            ? 0
            : this.state.dataxprovinsivalue.value,
        param: this.state.param,
        sort:
          this.state.dataxsortvalue == null
            ? "id_pelatihan"
            : this.state.dataxsortvalue,
        sortval:
          this.state.dataxsortvalvalue == null
            ? "DESC"
            : this.state.dataxsortvalvalue.toUpperCase(),
      };
      if (this.state.dataxtahunvalue.value == null) {
        dataFilter.tahun = this.state.currentyear;
      } else {
        dataFilter.tahun = this.state.dataxtahunvalue.value;
      }
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/boxdash1x",
          dataFilter,
          this.configs,
        )
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            this.setState({ numberrow: start_tmp + 1 });
            const datax = res.data.result.Data;
            this.setState({ loading: false });
            this.setState({ datax });
            this.setState({ totalRows: res.data.result.TotalData });
          } else {
            this.setState({ loading: false });
            this.setState({ datax: [] });
          }
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            this.setState({ loading: false });
            this.setState({ datax: [] });
          }
        });
    } else if ("revisi" == from) {
      this.setState({ from: "revisi" });
      let dataFilter = {
        mulai: start_tmp,
        limit: length_tmp,
        id_penyelenggara:
          this.state.dataxpenyelenggaravalue.value == null
            ? 0
            : this.state.dataxpenyelenggaravalue.value,
        id_akademi:
          this.state.dataxakademivalue.value == null
            ? 0
            : this.state.dataxakademivalue.value,
        id_tema:
          this.state.dataxtemavalue.value == null
            ? 0
            : this.state.dataxtemavalue.value,
        status_substansi:
          this.state.dataxstatussubstansivalue.value == null
            ? 0
            : this.state.dataxstatussubstansivalue.value,
        status_pelatihan:
          this.state.dataxstatuspelatihanvalue.value == null
            ? 0
            : this.state.dataxstatuspelatihanvalue.value,
        status_publish:
          this.state.dataxstatuspublishvalue.value == null
            ? 99
            : this.state.dataxstatuspublishvalue.value,
        provinsi:
          this.state.dataxprovinsivalue.value == null
            ? 0
            : this.state.dataxprovinsivalue.value,
        param: this.state.param,
        sort:
          this.state.dataxsortvalue == null
            ? "id_pelatihan"
            : this.state.dataxsortvalue,
        sortval:
          this.state.dataxsortvalvalue == null
            ? "DESC"
            : this.state.dataxsortvalvalue.toUpperCase(),
      };
      if (this.state.dataxtahunvalue.value == null) {
        dataFilter.tahun = this.state.currentyear;
      } else {
        dataFilter.tahun = this.state.dataxtahunvalue.value;
      }
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/boxdash2x",
          dataFilter,
          this.configs,
        )
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            this.setState({ numberrow: start_tmp + 1 });
            const datax = res.data.result.Data;
            this.setState({ loading: false });
            this.setState({ datax });
            this.setState({ totalRows: res.data.result.TotalData });
          } else {
            this.setState({ loading: false });
            this.setState({ datax: [] });
          }
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            this.setState({ loading: false });
            this.setState({ datax: [] });
          }
        });
    } else if ("disetujui" == from) {
      this.setState({ from: "disetujui" });
      let dataFilter = {
        mulai: start_tmp,
        limit: length_tmp,
        id_penyelenggara:
          this.state.dataxpenyelenggaravalue.value == null
            ? 0
            : this.state.dataxpenyelenggaravalue.value,
        id_akademi:
          this.state.dataxakademivalue.value == null
            ? 0
            : this.state.dataxakademivalue.value,
        id_tema:
          this.state.dataxtemavalue.value == null
            ? 0
            : this.state.dataxtemavalue.value,
        status_substansi:
          this.state.dataxstatussubstansivalue.value == null
            ? 0
            : this.state.dataxstatussubstansivalue.value,
        status_pelatihan:
          this.state.dataxstatuspelatihanvalue.value == null
            ? 0
            : this.state.dataxstatuspelatihanvalue.value,
        status_publish:
          this.state.dataxstatuspublishvalue.value == null
            ? 99
            : this.state.dataxstatuspublishvalue.value,
        provinsi:
          this.state.dataxprovinsivalue.value == null
            ? 0
            : this.state.dataxprovinsivalue.value,
        param: this.state.param,
        sort:
          this.state.dataxsortvalue == null
            ? "id_pelatihan"
            : this.state.dataxsortvalue,
        sortval:
          this.state.dataxsortvalvalue == null
            ? "DESC"
            : this.state.dataxsortvalvalue.toUpperCase(),
      };
      if (this.state.dataxtahunvalue.value == null) {
        dataFilter.tahun = this.state.currentyear;
      } else {
        dataFilter.tahun = this.state.dataxtahunvalue.value;
      }
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/boxdash3x",
          dataFilter,
          this.configs,
        )
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            this.setState({ numberrow: start_tmp + 1 });
            const datax = res.data.result.Data;
            this.setState({ loading: false });
            this.setState({ datax });
            this.setState({ totalRows: res.data.result.TotalData });
          } else {
            this.setState({ loading: false });
            this.setState({ datax: [] });
          }
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            this.setState({ loading: false });
            this.setState({ datax: [] });
          }
        });
    } else if ("ditolak" == from) {
      this.setState({ from: "ditolak" });
      let dataFilter = {
        mulai: start_tmp,
        limit: length_tmp,
        id_penyelenggara:
          this.state.dataxpenyelenggaravalue.value == null
            ? 0
            : this.state.dataxpenyelenggaravalue.value,
        id_akademi:
          this.state.dataxakademivalue.value == null
            ? 0
            : this.state.dataxakademivalue.value,
        id_tema:
          this.state.dataxtemavalue.value == null
            ? 0
            : this.state.dataxtemavalue.value,
        status_substansi:
          this.state.dataxstatussubstansivalue.value == null
            ? 0
            : this.state.dataxstatussubstansivalue.value,
        status_pelatihan:
          this.state.dataxstatuspelatihanvalue.value == null
            ? 0
            : this.state.dataxstatuspelatihanvalue.value,
        status_publish:
          this.state.dataxstatuspublishvalue.value == null
            ? 99
            : this.state.dataxstatuspublishvalue.value,
        provinsi:
          this.state.dataxprovinsivalue.value == null
            ? 0
            : this.state.dataxprovinsivalue.value,
        param: this.state.param,
        sort:
          this.state.dataxsortvalue == null
            ? "id_pelatihan"
            : this.state.dataxsortvalue,
        sortval:
          this.state.dataxsortvalvalue == null
            ? "DESC"
            : this.state.dataxsortvalvalue.toUpperCase(),
      };
      if (this.state.dataxtahunvalue.value == null) {
        dataFilter.tahun = this.state.currentyear;
      } else {
        dataFilter.tahun = this.state.dataxtahunvalue.value;
      }
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/boxdash4x",
          dataFilter,
          this.configs,
        )
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            this.setState({ numberrow: start_tmp + 1 });
            const datax = res.data.result.Data;
            this.setState({ loading: false });
            this.setState({ datax });
            this.setState({ totalRows: res.data.result.TotalData });
          } else {
            this.setState({ loading: false });
            this.setState({ datax: [] });
          }
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            this.setState({ loading: false });
            this.setState({ datax: [] });
          }
        });
    }
  }
  doSearch(searchText) {
    if (searchText == "") {
      this.handleReload("home", this.state.param, 1, 10);
    } else {
      this.handleReload("home", searchText, 1, 10);
      // let data = {
      //   param: searchText
      // }
      // axios.post(process.env.REACT_APP_BASE_API_URI + '/findpelatihan1x', data, this.configs)
      //   .then(res => {
      //     const statux = res.data.result.Status;
      //     const messagex = res.data.result.Message;
      //     if (statux) {
      //       const datax = res.data.result.Data;
      //       this.setState({ datax });
      //       this.setState({ loading:false });
      //     } else {
      //       swal.fire({
      //         title: messagex,
      //         icon: 'warning',
      //         confirmButtonText: 'Ok'
      //       }).then((result) => {
      //         if (result.isConfirmed) {
      //           this.setState({ loading:false });
      //         }
      //       });
      //     }
      //   }).catch((error) => {
      //     let statux = error.response.data.result.Status;
      //     let messagex = error.response.data.result.Message;
      //     if(!statux){
      //       swal.fire({
      //           title: messagex,
      //           icon: 'warning',
      //           confirmButtonText: 'Ok'
      //       }).then((result) => {
      //           if (result.isConfirmed) {
      //             this.setState({ datax:[] });
      //             this.setState({ loading:false });
      //           }
      //       });
      //     }
      //   });
    }
  }
  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    this.setState({ param: searchText });
    this.setState({ numberrow: 1 });
    this.setState({ issearch: true });
    if (e.key === "Enter") {
      this.setState({ loading: true }, () => {
        this.doSearch(searchText);
      });
    }
  }
  handleClickSearchAction(e) {
    e.preventDefault();
    // const dataForm = new FormData(e.currentTarget);
    // let penyelenggara = dataForm.get('penyelenggara');
    // let akademiId = dataForm.get('akademi_id');
    // let temaId = dataForm.get('tema_id');
    // let statusSubstansi = dataForm.get('status_substansi');
    // let statusPelatihan = dataForm.get('status_pelatihan');
    // let statusPublish = dataForm.get('status_publish');
    // let provinsi = dataForm.get('provinsi');
    // if (penyelenggara === '' && akademiId === '' &&
    //     temaId === '' && statusSubstansi === '' &&
    //     statusPelatihan === '' && statusPublish === '' &&
    //     provinsi === '') {
    //   this.setState({ numberrow:1 });
    //   this.handleReload(1, 10);
    // } else {
    //   if (penyelenggara === '') {
    //     penyelenggara = 0;
    //   }
    //   if (akademiId === '') {
    //     akademiId = 0;
    //   }
    //   if (temaId === '' || temaId == null) {
    //     temaId = 0;
    //   }
    //   if (statusSubstansi === '') {
    //     statusSubstansi = 0;
    //   }
    //   if (statusPelatihan === '' || statusPelatihan === null) {
    //     statusPelatihan = 0;
    //   }
    //   if (statusPublish === '' || statusPublish === null) {
    //     statusPublish = 99;
    //   }
    //   if (provinsi === '') {
    //     provinsi = 0;
    //   }
    //   let data = {
    //     mulai:0,
    //     limit: 100,
    //     id_penyelenggara: penyelenggara,
    //     id_akademi: akademiId,
    //     id_tema: temaId,
    //     status_substansi: statusSubstansi,
    //     status_pelatihan: statusPelatihan,
    //     status_publish: statusPublish,
    //     provinsi: provinsi
    //   }
    //   axios.post(process.env.REACT_APP_BASE_API_URI+'/pelatihanx', data, this.configs)
    //     .then(res => {
    //       const statux = res.data.result.Status;
    //       const messagex = res.data.result.Message;
    //       if(statux){
    //         this.setState({ numberrow:1 });
    //         const datax = res.data.result.Data;
    //         this.setState({ datax });
    //         this.setState({ loading:false });
    //       }else{
    //         swal.fire({
    //           title: messagex,
    //           icon: 'warning',
    //           confirmButtonText: 'Ok'
    //         }).then((result) => {
    //           if (result.isConfirmed) {
    //             this.setState({ loading:false });
    //           }
    //         });
    //       }
    //   }).catch((error) => {
    //     let statux = error.response.data.result.Status;
    //     let messagex = error.response.data.result.Message;
    //     if(!statux){
    //       swal.fire({
    //           title: messagex,
    //           icon: 'warning',
    //           confirmButtonText: 'Ok'
    //       }).then((result) => {
    //           if (result.isConfirmed) {
    //             this.setState({ datax:[] });
    //             this.setState({ loading:false });
    //           }
    //       });
    //     }
    //   });
    // }
    this.handleReload("home", this.state.param, 1, 10);
  }
  handleReview(paramx) {
    let data = {
      mulai: 0,
      limit: 100,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/boxdash1",
        data,
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        let review = 0;
        if (statux) {
          const datax = res.data.result.Data;
          if ("first" == paramx) {
            review = res.data.result.TotalData;
            this.setState({ review });
          } else {
            this.setState({ datax });
          }
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      })
      .catch((error) => {
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }
  handleRevisi(paramx) {
    let data = {
      mulai: 0,
      limit: 100,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/boxdash2",
        data,
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        let revisi = 0;
        if (statux) {
          const datax = res.data.result.Data;
          if ("first" == paramx) {
            revisi = res.data.result.TotalData;
            this.setState({ revisi });
          } else {
            this.setState({ datax });
          }
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      })
      .catch((error) => {
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }
  handleDisetujui(paramx) {
    let data = {
      mulai: 0,
      limit: 100,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/boxdash3",
        data,
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        let disetujui = 0;
        if (statux) {
          const datax = res.data.result.Data;
          if ("first" == paramx) {
            disetujui = res.data.result.TotalData;
            this.setState({ disetujui });
          } else {
            this.setState({ datax });
          }
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      })
      .catch((error) => {
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }
  handleDitolak(paramx) {
    let data = {
      mulai: 0,
      limit: 100,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/boxdash4",
        data,
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        let ditolak = 0;
        if (statux) {
          const datax = res.data.result.Data;
          if ("first" == paramx) {
            ditolak = res.data.result.TotalData;
            this.setState({ ditolak });
          } else {
            this.setState({ datax });
          }
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      })
      .catch((error) => {
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }
  handleClickReviewAction(e) {
    e.preventDefault();
    this.handleReload("review", "", 1, 10);
  }
  handleClickRevisiAction(e) {
    e.preventDefault();
    this.handleReload("revisi", "", 1, 10);
  }
  handleClickDisetujuiAction(e) {
    e.preventDefault();
    this.handleReload("disetujui", "", 1, 10);
  }
  handleClickDitolakAction(e) {
    e.preventDefault();
    this.handleReload("ditolak", "", 1, 10);
  }
  handleChangeAkademiAction = (akademi_id) => {
    let dataxakademivalue = akademi_id;
    this.setState({ dataxakademivalue });
    const dataBody = { start: 1, rows: 100, id: akademi_id.value };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/cari_tema_byakademi",
        dataBody,
        this.configs,
      )
      .then((res) => {
        this.setState({ isDisabled: false });
        const optionx = res.data.result.Data;
        const dataxtema = [];
        optionx.map((data) =>
          dataxtema.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxtema });
      })
      .catch((error) => {
        const dataxtema = [];
        this.setState({ dataxtema });
        let messagex = error.response.data.result.Message;
        swal
          .fire({
            title: messagex,
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
            }
          });
      });
  };
  handleChangePenyelenggaraAction = (penyelenggara_id) => {
    let dataxpenyelenggaravalue = penyelenggara_id;
    this.setState({ dataxpenyelenggaravalue });
  };
  handleChangeTemaAction = (tema_id) => {
    let dataxtemavalue = tema_id;
    this.setState({ dataxtemavalue });
  };
  handleChangeStatusSubstansiAction = (status_substansi_id) => {
    let dataxstatussubstansivalue = status_substansi_id;
    this.setState({ dataxstatussubstansivalue });
  };
  handleChangeStatusPelatihanAction = (status_pelatihan_id) => {
    let dataxstatuspelatihanvalue = status_pelatihan_id;
    this.setState({ dataxstatuspelatihanvalue });
  };
  handleChangeStatusPublishAction = (status_publish_id) => {
    let dataxstatuspublishvalue = status_publish_id;
    this.setState({ dataxstatuspublishvalue });
  };
  handleChangeProvinsiAction = (provinsi_id) => {
    let dataxprovinsivalue = provinsi_id;
    this.setState({ dataxprovinsivalue });
  };
  handleChangeYearAction = (tahun_id) => {
    let dataxtahunvalue = tahun_id;
    this.setState({ dataxtahunvalue });
  };
  handleClickResetAction(e) {
    e.preventDefault();
    this.setState(
      {
        numberrow: 1,
        dataxpenyelenggaravalue: "",
        dataxakademivalue: "",
        dataxtemavalue: "",
        dataxstatussubstansivalue: "",
        dataxstatuspelatihanvalue: "",
        dataxstatuspublishvalue: "",
        dataxprovinsivalue: "",
        dataxtahunvalue: "",
      },
      () => {
        this.setState({ isfilter: false });
        this.handleReload("home", this.state.param, 1, 10);
      },
    );
  }
  handlePageChange = (page) => {
    console.log("handlePageChange");
    this.setState({ loading: true });
    this.handleReload(
      this.state.from,
      this.state.param,
      page,
      this.state.newPerPage,
    );
  };
  handlePerRowsChange = async (newPerPage, page) => {
    console.log("handlePerRowsChange");
    console.log("newPerPage: " + newPerPage + ", page: " + page);
    console.log("numberrow : " + this.state.numberrow);
    this.setState({ loading: true });
    this.setState({ newPerPage: newPerPage });
    this.handleReload(this.state.from, this.state.param, page, newPerPage);
  };
  handleChangeSearchAction(e) {
    const searchText = e.currentTarget.value;
    this.setState({ numberrow: 1 });
    if (searchText == "") {
      this.setState({ loading: true }, () => {
        this.handleReload();
      });
    }
  }
  render() {
    let rowCounter = 1;
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack"
          >
            <div
              data-kt-swapper="true"
              data-kt-swapper-mode="prepend"
              data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
              className="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0"
            >
              <h1 className="d-flex align-items-center text-dark fw-bolder fs-3 my-1">
                Dashboard
              </h1>
              <span className="h-20px border-gray-200 border-start mx-4" />
              <ul className="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                <li className="breadcrumb-item text-muted">Partnership</li>
                <li className="breadcrumb-item">
                  <span className="bullet bg-gray-200 w-5px h-2px" />
                </li>
                <li className="breadcrumb-item text-muted">Dashboard</li>
              </ul>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-7"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 col-md-12 col-sm-12">
                    <div className="row">
                      <div className="col-xl-6">
                        <a
                          href="#"
                          onClick={this.handleClickRevisi}
                          className="card bg-primary hoverable"
                        >
                          <div className="card-body">
                            <span className="svg-icon svg-icon-white svg-icon-3x ms-n1">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                              >
                                <path
                                  opacity="0.3"
                                  d="M18 21.6C16.3 21.6 15 20.3 15 18.6V2.50001C15 2.20001 14.6 1.99996 14.3 2.19996L13 3.59999L11.7 2.3C11.3 1.9 10.7 1.9 10.3 2.3L9 3.59999L7.70001 2.3C7.30001 1.9 6.69999 1.9 6.29999 2.3L5 3.59999L3.70001 2.3C3.50001 2.1 3 2.20001 3 3.50001V18.6C3 20.3 4.3 21.6 6 21.6H18Z"
                                  fill="white"
                                ></path>
                                <path
                                  d="M12 12.6H11C10.4 12.6 10 12.2 10 11.6C10 11 10.4 10.6 11 10.6H12C12.6 10.6 13 11 13 11.6C13 12.2 12.6 12.6 12 12.6ZM9 11.6C9 11 8.6 10.6 8 10.6H6C5.4 10.6 5 11 5 11.6C5 12.2 5.4 12.6 6 12.6H8C8.6 12.6 9 12.2 9 11.6ZM9 7.59998C9 6.99998 8.6 6.59998 8 6.59998H6C5.4 6.59998 5 6.99998 5 7.59998C5 8.19998 5.4 8.59998 6 8.59998H8C8.6 8.59998 9 8.19998 9 7.59998ZM13 7.59998C13 6.99998 12.6 6.59998 12 6.59998H11C10.4 6.59998 10 6.99998 10 7.59998C10 8.19998 10.4 8.59998 11 8.59998H12C12.6 8.59998 13 8.19998 13 7.59998ZM13 15.6C13 15 12.6 14.6 12 14.6H10C9.4 14.6 9 15 9 15.6C9 16.2 9.4 16.6 10 16.6H12C12.6 16.6 13 16.2 13 15.6Z"
                                  fill="white"
                                ></path>
                                <path
                                  d="M15 18.6C15 20.3 16.3 21.6 18 21.6C19.7 21.6 21 20.3 21 18.6V12.5C21 12.2 20.6 12 20.3 12.2L19 13.6L17.7 12.3C17.3 11.9 16.7 11.9 16.3 12.3L15 13.6V18.6Z"
                                  fill="white"
                                ></path>
                              </svg>
                            </span>
                            <div className="text-inverse-dark fw-bolder fs-2 mb-2 mt-5">
                              {this.state.API_Dashboard.total_mitra}
                            </div>
                            <div className="fw-bold text-inverse-dark fs-7">
                              Total Mitra
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-xl-6">
                        <a
                          href="#"
                          onClick={this.handleClickDisetujui}
                          className="card bg-success hoverable"
                        >
                          <div className="card-body">
                            <span className="svg-icon svg-icon-white svg-icon-3x ms-n1">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                              >
                                <path
                                  opacity="0.3"
                                  d="M14 12V21H10V12C10 11.4 10.4 11 11 11H13C13.6 11 14 11.4 14 12ZM7 2H5C4.4 2 4 2.4 4 3V21H8V3C8 2.4 7.6 2 7 2Z"
                                  fill="white"
                                ></path>
                                <path
                                  d="M21 20H20V16C20 15.4 19.6 15 19 15H17C16.4 15 16 15.4 16 16V20H3C2.4 20 2 20.4 2 21C2 21.6 2.4 22 3 22H21C21.6 22 22 21.6 22 21C22 20.4 21.6 20 21 20Z"
                                  fill="white"
                                ></path>
                              </svg>
                            </span>
                            <div className="text-inverse-dark fw-bolder fs-2 mb-2 mt-5">
                              {this.state.API_Dashboard.total_kerjasama}
                            </div>
                            <div className="fw-bold text-inverse-dark fs-7">
                              Total Kerjasama
                            </div>
                          </div>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-6 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title" style={{ paddingTop: 15 }}>
                          <h4 className="me-3 mr-2">
                            Berdasarkan Pengajuan Aktif dan Tidak Aktif
                          </h4>
                        </div>
                        <div className="card-toolbar"></div>
                      </div>
                      <div className="card-body">
                        <div className="d-flex flex-wrap">
                          <div className="position-relative d-flex flex-center h-175px w-175px me-15 mb-7">
                            <div className="position-absolute translate-middle start-50 top-50 d-flex flex-column flex-center">
                              <span className="fs-2qx fw-bolder">
                                {this.state.API_Dashboard
                                  .total_kerjasama_aktif +
                                  this.state.API_Dashboard
                                    .total_kerjasama_tidak_aktif}
                              </span>
                              <span className="fs-6 fw-bold text-gray-400">
                                Total
                              </span>
                            </div>
                            <canvas id="project_overview_chart"></canvas>
                          </div>
                          <div className="d-flex flex-column justify-content-center flex-row-fluid pe-11 mb-5">
                            <div className="d-flex fs-6 fw-bold align-items-center mb-3">
                              <div className="bullet bg-primary me-3"></div>
                              <div className="text-gray-400">
                                Pengajuan Aktif
                              </div>
                              <div className="ms-auto fw-bolder text-gray-700">
                                {this.state.API_Dashboard.total_kerjasama_aktif}
                              </div>
                            </div>
                            <div className="d-flex fs-6 fw-bold align-items-center">
                              <div className="bullet bg-gray-300 me-3"></div>
                              <div className="text-gray-400">
                                Pengajuan Tidak Aktif
                              </div>
                              <div className="ms-auto fw-bolder text-gray-700">
                                {
                                  this.state.API_Dashboard
                                    .total_kerjasama_tidak_aktif
                                }
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-6 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title" style={{ paddingTop: 15 }}>
                          <h4 className="me-3 mr-2">
                            Berdasarkan Pengajuan Akan Berakhir dan Ditolak
                          </h4>
                        </div>
                        <div className="card-toolbar"></div>
                        <div style={{ paddingBottom: 15 }}>
                          {this.state.API_Dashboard
                            .total_kerjasama_akan_berakhir +
                            this.state.API_Dashboard
                              .total_kerjasama_ditolak}{" "}
                          Total Akan Berakhir dan Ditolak
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="d-flex flex-wrap">
                          <div className="position-relative d-flex flex-center h-175px w-175px me-15 mb-7">
                            <div className="position-absolute translate-middle start-50 top-50 d-flex flex-column flex-center">
                              <span className="fs-2qx fw-bolder">
                                {this.state.API_Dashboard
                                  .total_kerjasama_akan_berakhir +
                                  this.state.API_Dashboard
                                    .total_kerjasama_ditolak}{" "}
                              </span>
                              <span className="fs-6 fw-bold text-gray-400">
                                Total
                              </span>
                            </div>
                            <canvas id="project_overview_end_chart"></canvas>
                          </div>
                          <div className="d-flex flex-column justify-content-center flex-row-fluid pe-11 mb-5">
                            <div className="d-flex fs-6 fw-bold align-items-center mb-3">
                              <div className="bullet bg-primary me-3"></div>
                              <div className="text-gray-400">
                                Pengajuan Akan Berakhir
                              </div>
                              <div className="ms-auto fw-bolder text-gray-700">
                                {
                                  this.state.API_Dashboard
                                    .total_kerjasama_akan_berakhir
                                }
                              </div>
                            </div>
                            <div className="d-flex fs-6 fw-bold align-items-center">
                              <div className="bullet bg-gray-300 me-3"></div>
                              <div className="text-gray-400">
                                Pengajuan Ditolak
                              </div>
                              <div className="ms-auto fw-bolder text-gray-700">
                                {
                                  this.state.API_Dashboard
                                    .total_kerjasama_ditolak
                                }
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const Dashboard = () => {
  // useState
  const [API_Dashboard, setAPI_Dashboard] = useState({
    total_mitra: 0,
    total_mitra_aktif: 0,
    total_mitra_tidak_aktif: 0,
    total_pengajuan_review: 0,
    total_pengajuan_revisi: 0,
    total_pengajuan_ditolak: 0,
    total_pengajuan_disetujui: 0,
    total_kerjasama: 0,
    total_kerjasama_aktif: 0,
    total_kerjasama_tidak_aktif: 0,
    total_kerjasama_akan_berakhir: 0,
    total_kerjasama_ditolak: 0,
  });

  const [API_Pengajuan, setAPI_Pengajuan] = useState({
    review: 2,
    revisi: 3,
    ditolak: 0,
    disetujui: 1,
  });
  const [ChartDougnut, setChartDougnut] = useState();
  const [ChartBar, setChartBar] = useState();

  const color = {
    review: "rgb(54, 162, 235)",
    revise: "rgb(255, 205, 86)",
    ditolak: "rgb(255, 99, 132)",
    distejui: "rgb(75, 192, 192)",
    reviewFill: "rgba(54, 162, 235, 0.2)",
    reviseFill: "rgba(255, 205, 86, 0.2)",
    ditolakFill: "rgba(255, 99, 132, 0.2)",
    distejuiFill: "rgba(75, 192, 192, 0.2)",
  };
  const configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  useEffect(() => {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    const dataPenyelenggara = {};
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/masterKerjasama/API_Dashboard",
        dataPenyelenggara,
        configs,
      )
      .then((res) => {
        const API_Dashboard = res.data.result.Data;
        setAPI_Dashboard({ ...API_Dashboard });
      });
  }, []);

  useEffect(() => {
    if (ChartDougnut && API_Dashboard) {
      // console.log("Update chart")
      removeDataFromChart(ChartDougnut);
      addDataToChart(
        ChartDougnut,
        "Kerjasama Aktif",
        API_Dashboard.total_kerjasama_aktif,
      );
      addDataToChart(
        ChartDougnut,
        "Kerjasama Aktif",
        API_Dashboard.total_kerjasama_tidak_aktif,
      );
    }
  }, [ChartDougnut, API_Dashboard]);

  useEffect(() => {
    if (ChartBar && API_Pengajuan) {
      removeDataFromChart(ChartBar);
      removeDataFromChart(ChartBar);
      removeDataFromChart(ChartBar);
      removeDataFromChart(ChartBar);
      // ChartBar.
      addDataToChart(ChartBar, "Review", API_Dashboard.total_pengajuan_review);
      addDataToChart(ChartBar, "Revisi", API_Dashboard.total_pengajuan_revisi);
      addDataToChart(
        ChartBar,
        "Ditolak",
        API_Dashboard.total_pengajuan_ditolak,
      );
      addDataToChart(
        ChartBar,
        "Disetujui",
        API_Dashboard.total_pengajuan_disetujui,
      );
    }
  }, [ChartBar, API_Dashboard]);

  // set chart func
  useEffect(() => {
    // console.log(window.Chart)
    if (window.Chart) {
      setChartDougnut(
        new window.Chart(document.getElementById("chart_aktif_nonaktif"), {
          type: "doughnut",
          data: {
            labels: ["Kerjasama Aktif", "Kerjasama Non Aktif"],
            datasets: [
              {
                data: [
                  API_Dashboard.total_mitra_aktif,
                  API_Dashboard.total_kerjasama_tidak_aktif,
                ],
                backgroundColor: ["#0095E8", "#7E8299"],
                hoverOffset: 4,
              },
            ],
          },
          options: {
            chart: {
              fontFamily: "inherit",
            },
            cutoutPercentage: 75,
            responsive: !0,
            maintainAspectRatio: !1,
            cutout: "75%",
            title: {
              display: !1,
            },
            animation: {
              animateScale: !0,
              animateRotate: !0,
            },
            tooltips: {
              enabled: !0,
              intersect: !1,
              mode: "nearest",
              bodySpacing: 5,
              yPadding: 10,
              xPadding: 10,
              caretPadding: 0,
              displayColors: !1,
              backgroundColor: "#20D489",
              titleFontColor: "#ffffff",
              cornerRadius: 4,
              footerSpacing: 0,
              titleSpacing: 0,
            },
            plugins: {
              legend: {
                display: !1,
              },
            },
          },
        }),
      );
      setChartBar(
        new window.Chart(document.getElementById("chart_pengajuan"), {
          type: "bar",
          data: {
            labels: ["Review", "Revisi", "Ditolak", "Disetujui"],
            // datasets: [{ data: [10, 20, 30, 40] }]

            datasets: [
              {
                label: "Pengajuan Kerjasama",
                data: [
                  API_Dashboard.total_pengajuan_review,
                  API_Dashboard.total_pengajuan_revisi,
                  API_Dashboard.total_pengajuan_ditolak,
                  API_Dashboard.total_pengajuan_disetujui,
                ],
                backgroundColor: [
                  color.reviewFill,
                  color.reviseFill,
                  color.ditolakFill,
                  color.distejuiFill,
                ],
                borderColor: [
                  color.review,
                  color.revise,
                  color.ditolak,
                  color.distejui,
                ],
                borderWidth: 1,
              },
            ],
          },
          options: {
            scales: {
              y: {
                // padding:10,
                beginAtZero: true,
                ticks: {
                  stepSize: 1,
                },
              },
            },
          },
        }),
      );
    }
  }, [window.Chart]);

  function addDataToChart(chart, label, data) {
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
      dataset.data.push(data);
    });
    chart.update();
  }

  function removeDataFromChart(chart) {
    chart.data.labels.pop();
    chart.data.datasets.forEach((dataset) => {
      dataset.data.pop();
    });
    chart.update();
  }

  return (
    <div>
      <div className="toolbar" id="kt_toolbar">
        <div
          id="kt_toolbar_container"
          className="container-fluid d-flex flex-stack"
        >
          <div
            data-kt-swapper="true"
            data-kt-swapper-mode="prepend"
            data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
            className="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0"
          >
            <h1 className="d-flex align-items-center text-dark fw-bolder fs-3 my-1">
              Dashboard
            </h1>
            <span className="h-20px border-gray-200 border-start mx-4" />
            <ul className="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
              <li className="breadcrumb-item text-muted">Partnership</li>
              <li className="breadcrumb-item">
                <span className="bullet bg-gray-200 w-5px h-2px" />
              </li>
              <li className="breadcrumb-item text-muted">Dashboard</li>
            </ul>
          </div>
        </div>
      </div>
      <div
        className="wrapper d-flex flex-column flex-row-fluid pt-7"
        id="kt_wrapper"
      >
        <div
          className="content d-flex flex-column flex-column-fluid pt-0"
          id="kt_content"
        >
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container-xxl">
              <div className="row">
                <div className="col-lg-12 col-md-12 col-sm-12">
                  <div className="row">
                    <div class="col-xl-4">
                      <a
                        href="#"
                        onClick={() => {}}
                        class="card bg-dark hoverable mb-4"
                      >
                        <div class="card-body">
                          <span class="svg-icon svg-icon-white svg-icon-3x ms-n1">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                            >
                              <path
                                opacity="0.3"
                                d="M14 12V21H10V12C10 11.4 10.4 11 11 11H13C13.6 11 14 11.4 14 12ZM7 2H5C4.4 2 4 2.4 4 3V21H8V3C8 2.4 7.6 2 7 2Z"
                                fill="white"
                              ></path>
                              <path
                                d="M21 20H20V16C20 15.4 19.6 15 19 15H17C16.4 15 16 15.4 16 16V20H3C2.4 20 2 20.4 2 21C2 21.6 2.4 22 3 22H21C21.6 22 22 21.6 22 21C22 20.4 21.6 20 21 20Z"
                                fill="white"
                              ></path>
                            </svg>
                          </span>
                          <div class="text-inverse-dark fw-bolder fs-2 mb-2 mt-5">
                            {API_Dashboard.total_pengajuan_disetujui +
                              API_Dashboard.total_pengajuan_ditolak +
                              API_Dashboard.total_pengajuan_review +
                              API_Dashboard.total_pengajuan_revisi}
                          </div>
                          <div class="fw-bold text-inverse-dark fs-7">
                            Total Pengajuan Kerjasama
                          </div>
                        </div>
                      </a>
                    </div>
                    <div class="col-xl-4">
                      <a
                        href="#"
                        onClick={() => {}}
                        class="card bg-success hoverable mb-4"
                      >
                        <div class="card-body">
                          <span class="svg-icon svg-icon-white svg-icon-3x ms-n1">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                            >
                              <path
                                opacity="0.3"
                                d="M18 21.6C16.3 21.6 15 20.3 15 18.6V2.50001C15 2.20001 14.6 1.99996 14.3 2.19996L13 3.59999L11.7 2.3C11.3 1.9 10.7 1.9 10.3 2.3L9 3.59999L7.70001 2.3C7.30001 1.9 6.69999 1.9 6.29999 2.3L5 3.59999L3.70001 2.3C3.50001 2.1 3 2.20001 3 3.50001V18.6C3 20.3 4.3 21.6 6 21.6H18Z"
                                fill="white"
                              ></path>
                              <path
                                d="M12 12.6H11C10.4 12.6 10 12.2 10 11.6C10 11 10.4 10.6 11 10.6H12C12.6 10.6 13 11 13 11.6C13 12.2 12.6 12.6 12 12.6ZM9 11.6C9 11 8.6 10.6 8 10.6H6C5.4 10.6 5 11 5 11.6C5 12.2 5.4 12.6 6 12.6H8C8.6 12.6 9 12.2 9 11.6ZM9 7.59998C9 6.99998 8.6 6.59998 8 6.59998H6C5.4 6.59998 5 6.99998 5 7.59998C5 8.19998 5.4 8.59998 6 8.59998H8C8.6 8.59998 9 8.19998 9 7.59998ZM13 7.59998C13 6.99998 12.6 6.59998 12 6.59998H11C10.4 6.59998 10 6.99998 10 7.59998C10 8.19998 10.4 8.59998 11 8.59998H12C12.6 8.59998 13 8.19998 13 7.59998ZM13 15.6C13 15 12.6 14.6 12 14.6H10C9.4 14.6 9 15 9 15.6C9 16.2 9.4 16.6 10 16.6H12C12.6 16.6 13 16.2 13 15.6Z"
                                fill="white"
                              ></path>
                              <path
                                d="M15 18.6C15 20.3 16.3 21.6 18 21.6C19.7 21.6 21 20.3 21 18.6V12.5C21 12.2 20.6 12 20.3 12.2L19 13.6L17.7 12.3C17.3 11.9 16.7 11.9 16.3 12.3L15 13.6V18.6Z"
                                fill="white"
                              ></path>
                            </svg>
                          </span>
                          <div class="text-inverse-dark fw-bolder fs-2 mb-2 mt-5">
                            {API_Dashboard.total_kerjasama_aktif}
                          </div>
                          <div class="fw-bold text-inverse-dark fs-7">
                            Total Kerjasama Aktif
                          </div>
                        </div>
                      </a>
                    </div>
                    <div class="col-xl-4">
                      <a
                        href="#"
                        onClick={() => {}}
                        class="card bg-danger hoverable mb-4"
                      >
                        <div class="card-body">
                          <span class="svg-icon svg-icon-white svg-icon-3x ms-n1">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                            >
                              <path
                                opacity="0.3"
                                d="M18 21.6C16.3 21.6 15 20.3 15 18.6V2.50001C15 2.20001 14.6 1.99996 14.3 2.19996L13 3.59999L11.7 2.3C11.3 1.9 10.7 1.9 10.3 2.3L9 3.59999L7.70001 2.3C7.30001 1.9 6.69999 1.9 6.29999 2.3L5 3.59999L3.70001 2.3C3.50001 2.1 3 2.20001 3 3.50001V18.6C3 20.3 4.3 21.6 6 21.6H18Z"
                                fill="white"
                              ></path>
                              <path
                                d="M12 12.6H11C10.4 12.6 10 12.2 10 11.6C10 11 10.4 10.6 11 10.6H12C12.6 10.6 13 11 13 11.6C13 12.2 12.6 12.6 12 12.6ZM9 11.6C9 11 8.6 10.6 8 10.6H6C5.4 10.6 5 11 5 11.6C5 12.2 5.4 12.6 6 12.6H8C8.6 12.6 9 12.2 9 11.6ZM9 7.59998C9 6.99998 8.6 6.59998 8 6.59998H6C5.4 6.59998 5 6.99998 5 7.59998C5 8.19998 5.4 8.59998 6 8.59998H8C8.6 8.59998 9 8.19998 9 7.59998ZM13 7.59998C13 6.99998 12.6 6.59998 12 6.59998H11C10.4 6.59998 10 6.99998 10 7.59998C10 8.19998 10.4 8.59998 11 8.59998H12C12.6 8.59998 13 8.19998 13 7.59998ZM13 15.6C13 15 12.6 14.6 12 14.6H10C9.4 14.6 9 15 9 15.6C9 16.2 9.4 16.6 10 16.6H12C12.6 16.6 13 16.2 13 15.6Z"
                                fill="white"
                              ></path>
                              <path
                                d="M15 18.6C15 20.3 16.3 21.6 18 21.6C19.7 21.6 21 20.3 21 18.6V12.5C21 12.2 20.6 12 20.3 12.2L19 13.6L17.7 12.3C17.3 11.9 16.7 11.9 16.3 12.3L15 13.6V18.6Z"
                                fill="white"
                              ></path>
                            </svg>
                          </span>
                          <div class="text-inverse-dark fw-bolder fs-2 mb-2 mt-5">
                            {API_Dashboard.total_kerjasama_tidak_aktif}
                          </div>
                          <div class="fw-bold text-inverse-dark fs-7">
                            Total Kerjasama Non Aktif
                          </div>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
                <div className="col-lg-6 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title" style={{ paddingTop: 15 }}>
                        <h4 className="me-3 mr-2">
                          Total Mitra bedasarkan status aktif dan non aktif
                        </h4>
                      </div>
                      <div className="card-toolbar"></div>
                    </div>
                    <div className="card-body">
                      <div className="d-flex flex-wrap">
                        <div className="position-relative d-flex flex-center h-175px w-175px me-15 mb-7">
                          <div className="position-absolute translate-middle start-50 top-50 d-flex flex-column flex-center">
                            <span className="fs-2qx fw-bolder">
                              {API_Dashboard.total_mitra}
                            </span>
                            <span className="fs-6 fw-bold text-gray-400">
                              Total
                            </span>
                          </div>
                          <canvas id="chart_aktif_nonaktif"></canvas>
                        </div>
                        <div className="d-flex flex-column justify-content-center flex-row-fluid pe-11 mb-5">
                          <div className="d-flex fs-6 fw-bold align-items-center mb-3">
                            <div className="bullet bg-primary me-3"></div>
                            <div className="text-gray-400">Mitra Aktif</div>
                            <div className="ms-auto fw-bolder text-gray-700">
                              {API_Dashboard.total_mitra_aktif}
                            </div>
                          </div>
                          <div className="d-flex fs-6 fw-bold align-items-center">
                            <div className="bullet bg-gray-300 me-3"></div>
                            <div className="text-gray-400">Mitra Non Aktif</div>
                            <div className="ms-auto fw-bolder text-gray-700">
                              {API_Dashboard.total_mitra_tidak_aktif}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-6 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title" style={{ paddingTop: 15 }}>
                        <h4 className="me-3 mr-2">
                          Pengajuan Kerjasama Berdasarkan Status
                        </h4>
                      </div>
                      <div className="card-toolbar"></div>
                    </div>
                    <div className="card-body">
                      <div className="d-flex flex-wrap">
                        <div className="position-relative d-flex flex-center me-11 mb-7">
                          <canvas id="chart_pengajuan"></canvas>
                        </div>
                        <div className="d-flex flex-column justify-content-center flex-row-fluid ms-11 mb-5">
                          <div className="d-flex fs-6 fw-bold align-items-center mb-3">
                            <div
                              className="bullet me-3"
                              style={{ background: color.review }}
                            ></div>
                            <div className="text-gray-400">
                              Pengajuan Review
                            </div>
                            <div className="ms-auto fw-bolder text-gray-700">
                              {API_Dashboard.total_pengajuan_review}
                            </div>
                          </div>
                          <div className="d-flex fs-6 fw-bold align-items-center mb-3">
                            <div
                              className="bullet me-3"
                              style={{ background: color.revise }}
                            ></div>
                            <div className="text-gray-400">
                              Pengajuan Revisi
                            </div>
                            <div className="ms-auto fw-bolder text-gray-700">
                              {API_Dashboard.total_pengajuan_revisi}
                            </div>
                          </div>
                          <div className="d-flex fs-6 fw-bold align-items-center mb-3">
                            <div
                              className="bullet me-3"
                              style={{ background: color.ditolak }}
                            ></div>
                            <div className="text-gray-400">
                              Pengajuan Ditolak
                            </div>
                            <div className="ms-auto fw-bolder text-gray-700">
                              {API_Dashboard.total_pengajuan_ditolak}
                            </div>
                          </div>
                          <div className="d-flex fs-6 fw-bold align-items-center mb-3">
                            <div
                              className="bullet me-3"
                              style={{ background: color.distejui }}
                            ></div>
                            <div className="text-gray-400">
                              Pengajuan Dietujui
                            </div>
                            <div className="ms-auto fw-bolder text-gray-700">
                              {API_Dashboard.total_pengajuan_disetujui}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
  // return
};
export default Dashboard;
