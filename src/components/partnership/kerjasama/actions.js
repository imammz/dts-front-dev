import axios from "axios";
import Cookies from "js-cookie";
import moment from "moment";
import Swal from "sweetalert2";
import { isJsonString } from "../../../utils/commonutil";

const configs = {
  headers: {
    Authorization: "Bearer " + Cookies.get("token"),
  },
};

const checkToken = () => {
  return new Promise(async (resolve, reject) => {
    if (Cookies.get("token") == null) {
      const { isConfirmed } = await Swal.fire({
        title: "Unauthenticated.",
        text: "Silahkan login ulang",
        icon: "warning",
        confirmButtonText: "Ok",
      });

      if (isConfirmed) return reject();
    }

    resolve();
  });
};

export const fetchAllStatus = async ({ start = 0, length = 50 }) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/kerjasama/status_kerjasama_list",
            {
              start,
              rows: length,
            },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Data);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchMitras = async ({
  start = 0,
  length = 50,
  search = "",
  orderBy = "nama_mitra",
  orderDir = "asc",
  status = 1,
  tahun = moment().year(),
}) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/mitra/list-mitra-s3",
            {
              start,
              length,
              sort: orderBy,
              sort_val: orderDir,
              param: search,
              status,
              tahun,
            },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Data);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchMitraById = async () => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/mitra/cari-mitra",
            { id: Cookies.get("partner_id") },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Data);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchKategoriKerjasama = async ({ start = 0, length = 50 }) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/masterKerjasama/list_catcoorpaktif",
            {
              start,
              rows: length,
            },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Data);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchDetailKategoriKerjasama = async (id) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/get_catcoorp",
            { id },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};
            [Data] = Data || [];
            let { data_form } = Data || {};
            data_form = (data_form || []).map((f) => ({
              ...f,
              cooperation_form: isJsonString(f.cooperation_form)
                ? JSON.parse(f.cooperation_form)["Name"]
                : f.cooperation_form,
            }));

            if (Status) resolve(data_form);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchDetailKerjasama = async (id) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/kerjasama/API_Detail_Kerjasama",
            { id },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};
            [Data] = Data || [];

            if (Status) resolve(Data);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const hasError = (error = {}) => {
  let _hasError = false;
  Object.keys(error).forEach((k) => {
    if (!error[k]) {
    } else if (Array.isArray(error[k]) || typeof error[k] == "object") {
      _hasError = _hasError || hasError(error[k]);
    } else {
      _hasError = _hasError || true;
    }
  });
  return _hasError;
};

export const resetError = () => {
  return {
    lembaga: null,
  };
};

export const validate = ({
  judul,
  nomor_mou_pks,
  kerjasama_ttd,
  periode,
  kategori,
  file,
  dataFormKerjasama,
  status_kerjasama = [],
}) => {
  let error = {};
  if (!judul) error.judul = "Judul harus terisi";
  //if (!periode_mulai) error.periode_mulai = 'Periode mulai kerjasama harus terisi';
  if (!periode) error.periode = "Periode kerjasama harus terisi";
  if (!status_kerjasama)
    error.status_kerjasama = "Status kerjasama harus terisi";
  if (!kategori)
    error.kategori = "Kategori kerjasama harus terpilih salah satu";
  if (!file) error.file = "File kerjasama harus terisi";
  dataFormKerjasama.forEach((f, idx) => {
    if (!f?.value) {
      error.dataFormKerjasama = error.dataFormKerjasama || [];
      error.dataFormKerjasama[idx] = "Form  harus terisi";
    }
  });

  return error;
};

export const insertKerjasama = async (payload) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/kerjasama/API_Insert_Kerjasama",
            payload,
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Message);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const updateKerjasama = async (payload) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/kerjasama/API_Update_Kerjasama",
            payload,
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Message);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};
