import React, { useState, useEffect, Fragment } from "react";
import { useParams, useNavigate } from "react-router-dom";
import axios from "axios";
import Header from "../../../../components/Header";
import SideNav from "../../../../components/SideNav";
import Footer from "../../../../components/Footer";
import Cookies from "js-cookie";
import moment from "moment";
import { isJsonString } from "../../../../utils/commonutil";
import swal from "sweetalert2";
import { capitalizeTheFirstLetterOfEachWord } from "../../../publikasi/helper";

const ReviewKerjasama = (props) => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  const [optSet, setOpt] = useState(0);
  const { id } = useParams();
  const history = useNavigate();
  const [isNotShownFull, setIsNotShownFull] = useState(true);
  const [currentAkademi, setCurrentAkademi] = useState({
    id: 25,
    created_at: "2022-01-28 04:24:23",
    idpartner_name: 24,
    partner_name: "Perum Perumnas",
    email: "l@gmail.com",
    tanggal: "2022-01-28",
    periode_kerjasama: 2,
    periode_unit: "tahun",
    judul_kerjasama: "Kerjasama Pelatihan Ui Ux Designer",
    id_kategori_kerjasama: 15,
    kategori_kerjasama: "Kerjasama Dts",
    status: 1,
    tujuan_kerjasama: "Tujuan Kerjasama",
    form_kerjasama: "Form Kerjasama",
    periode_kerjasama_mulai: "2022-01-28",
    periode_kerjasama_selesai: "2024-01-28",
    nomor_perjanjian_lembaga: "12-DTS-2021",
    nomor_perjanjian_kemkominfo: "KOMINFO-304-2021",
    tanggal_tanda_tangan: "2022-01-28",
    file_kerjasama:
      "/files/document_cooperations/c2ab5518-84c2-443a-8f7d-993021c3eaa3.pdf",
    kategori: [
      {
        cooperation_form_id: 25,
        cooperation_category_form_id: 70,
        cooperation_form_content: "Meningkatkan kemampuan anak bangsa",
      },
      {
        cooperation_form_id: 25,
        cooperation_category_form_id: 71,
        cooperation_form_content: "Winanda Sisilia Sinaga",
      },
    ],
  });

  const [listPertanyaan, setListPertanyaan] = useState({});
  const initialVal = [
    {
      label: "",
      value: "",
    },
  ];
  const [optionList, setOptioList] = useState(initialVal);

  const getAkademi = (id) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/kerjasama/API_Detail_Kerjasama",
        {
          id: id,
        },
        { headers: { Authorization: "Bearer " + Cookies.get("token") } },
      )
      .then(async function (response) {
        let data = response.data.result.Data[0];
        let resp = await axios.post(
          process.env.REACT_APP_BASE_API_URI + "/get_catcoorp",
          { id: data.id_kategori_kerjasama },
          { headers: { Authorization: "Bearer " + Cookies.get("token") } },
        );
        if ([5].includes(data.status)) {
          setIsNotShownFull(false);
        }
        let cats = resp.data.result.Data[0].data_form;
        let objCat = {};

        Object.keys(cats).forEach((k) => {
          objCat[`${cats[k].id}`] = isJsonString(cats[k].cooperation_form)
            ? JSON.parse(cats[k].cooperation_form)["Name"]
            : cats[k].cooperation_form;
        });
        console.log(objCat);
        setCurrentAkademi(data);
        setListPertanyaan(objCat);
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  useEffect(() => {
    getAkademi(id);
  }, []);

  const handleDownloadData = (url, nama) => {
    const configs = {
      responseType: "arraybuffer",
      headers: {
        Authorization: "Bearer " + Cookies.get("token"),
        Accept: "*/*",
        "Content-Length": 0,
        Connection: "keep-alive",
      },
    };
    swal.fire({
      title: "Memproses Download File, Mohon Tunggu..",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios.post(url, configs).then((response) => {
      const byteString = window.atob(response.data);
      const arrayBuffer = new ArrayBuffer(byteString.length);
      const int8Array = new Uint8Array(arrayBuffer);
      for (let i = 0; i < byteString.length; i++) {
        int8Array[i] = byteString.charCodeAt(i);
      }
      const blob = new Blob([int8Array], { type: "application/pdf" });
      var fileURL = window.URL.createObjectURL(blob);
      var fileLink = document.createElement("a");
      fileLink.href = fileURL;
      fileLink.setAttribute("download", nama + ".pdf");
      document.body.appendChild(fileLink);
      fileLink.click();
      swal.close();
    });
  };

  const axConfig = {
    headers: { Authorization: "Bearer " + Cookies.get("token") },
  };

  const deleteMouPks = (id) => {
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          let params = new FormData();
          params.append("id", id);
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI +
                "/kerjasama/API_Hapus_Kerjasama",
              params,
              axConfig,
            )
            .then(function (response) {
              if (response.status === 200) {
                if (response.data.result.Status === true) {
                  swal.fire({
                    title: "<strong>Sukses</strong>",
                    html: "<i>Kerjasama Berhasil Dihapus</i>",
                    icon: "success",
                    allowOutsideClick: false,
                    willClose: () => {
                      //getData()
                      window.location = "/partnership/kerjasama";
                    },
                  });
                } else {
                  swal.fire({
                    title: "<strong>Gagal</strong>",
                    html: "<i>Kerjasama Gagal Dihapus</i>",
                    icon: "warning",
                  });
                }
              } else {
              }
            })
            .catch((error) => {
              swal.fire({
                title: "Terjadi kesalahan",
                icon: "warning",
                text:
                  error.response.data.result.Message || "data tidak terhapus",
              });
            });
        }
      });
  };

  return (
    <div>
      <Header />
      <SideNav />
      <div className="toolbar" id="kt_toolbar">
        <div
          id="kt_toolbar_container"
          className="container-fluid d-flex flex-stack my-2"
        >
          <div className="d-flex align-items-start my-2">
            <div>
              <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                <span className="me-3">
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    className="mh-50px"
                  >
                    <path
                      opacity="0.3"
                      d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z"
                      fill="#FFC700"
                    ></path>
                    <path
                      d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z"
                      fill="#FFC700"
                    ></path>
                  </svg>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Partnership
                  <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                </span>
                Kerjasama
              </h1>
            </div>
          </div>
          <div className="d-flex align-items-end my-2">
            <div>
              <button
                onClick={() => history("/partnership/kerjasama/")}
                type="reset"
                className="btn btn-sm btn-light btn-active-light-primary me-2"
                data-kt-menu-dismiss="true"
              >
                {" "}
                <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                  <i className="fa fa-chevron-left"></i>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Kembali
                </span>
              </button>

              {currentAkademi.status == 1 || currentAkademi.status == 3 ? (
                <button
                  onClick={() => history("/partnership/kerjasama/edit/" + id)}
                  type="reset"
                  className="btn btn-warning fw-bolder btn-sm me-2"
                  data-kt-menu-dismiss="true"
                >
                  <i className="bi bi-gear-fill"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Edit
                  </span>
                </button>
              ) : (
                ""
              )}
              {currentAkademi.status == 1 ||
              currentAkademi.status == 3 ||
              currentAkademi.status == 4 ? (
                <button
                  className="btn btn-sm btn-danger btn-active-light-info me-2"
                  onClick={() => {
                    deleteMouPks(currentAkademi.id);
                  }}
                >
                  <i className="bi bi-trash-fill text-white"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </button>
              ) : (
                ""
              )}
            </div>
          </div>
        </div>
      </div>
      <div
        className="wrapper d-flex flex-column flex-row-fluid pt-0"
        id="kt_wrapper"
      >
        <div
          className="content d-flex flex-column flex-column-fluid pt-0"
          id="kt_content"
        >
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container-xxl">
              <div className="row">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Detail Kerjasama
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="mt-6 mb-6">
                        <div className="d-flex flex-wrap py-3">
                          <div className="symbol symbol-100px symbol-lg-120px">
                            <img
                              src={currentAkademi.agency_logo}
                              alt=""
                              className="img-fluid me-3"
                            />
                          </div>

                          <div className="flex-grow-1 mb-3">
                            <div className="d-flex justify-content-between align-items-start flex-wrap pt-6">
                              <div className="d-flex flex-column">
                                <div className="d-flex flex-wrap fw-bold fs-6 pe-2">
                                  <span className="text-primary fw-semibold fs-6">
                                    {currentAkademi.partner_name}
                                  </span>
                                </div>
                                <div className="d-flex align-items-center mb-2">
                                  <h5 className="fw-bolder mb-n1 fs-5">
                                    {currentAkademi.judul_kerjasama}
                                  </h5>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="row mt-6 pb-5">
                        <div className="col-lg-4 mb-7 fv-row">
                          <label className="form-label fw-bolder">
                            Durasi Kerjasama
                          </label>
                          <div className="d-flex fs-7">
                            {!isNotShownFull &&
                              ` ${moment(currentAkademi.periode_kerjasama_mulai)
                                .locale("ID")
                                .format("D MMM YYYY")} - ${moment(
                                currentAkademi.periode_kerjasama_selesai,
                              )
                                .locale("ID")
                                .format("D MMM YYYY")}`}
                            <span className="ms-1">
                              ({parseFloat(currentAkademi.periode_kerjasama)}{" "}
                              Thn)
                            </span>
                          </div>
                        </div>
                        <div>
                          <div className="mt-5 border-top mx-0"></div>
                        </div>
                        <h6 className="text-muted mt-7 mb-5">
                          Informasi Kerjasama
                        </h6>
                        <div className="col-lg-12 mb-7">
                          <label className="form-label fw-bolder">
                            Kategori
                          </label>
                          <div className="d-flex fs-7">
                            {currentAkademi.kategori_kerjasama}
                          </div>
                        </div>
                        {currentAkademi.kategori.map((value, index) => {
                          return (
                            <div className="col-lg-12 mb-7" key={index}>
                              <label className="form-label fw-bolder">
                                {
                                  listPertanyaan[
                                    value.cooperation_category_form_id
                                  ]
                                }
                              </label>
                              <div className="d-flex fs-7">
                                {value.cooperation_form_content}
                              </div>
                            </div>
                          );
                        })}
                        <div className="col-lg-6 mb-7 fv-row">
                          <label className="form-label fw-bolder">
                            Proposal
                          </label>
                          <a
                            href="#"
                            className="btn btn-light d-block fw-bolder btn-sm me-2"
                            url={
                              process.env.REACT_APP_BASE_API_URI +
                              "/kerjasama/API_Get_File_Kerjasama?path=" +
                              currentAkademi.file_kerjasama
                            }
                            onClick={() =>
                              handleDownloadData(
                                process.env.REACT_APP_BASE_API_URI +
                                  "/kerjasama/API_Get_File_Kerjasama2?path=" +
                                  currentAkademi.file_kerjasama,
                                "Proposal " + currentAkademi.judul_kerjasama,
                              )
                            }
                            title="Download"
                            file={currentAkademi.file_kerjasama}
                          >
                            <i className="bi bi-cloud-download"></i>
                            <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                              Download Proposal
                            </span>
                          </a>
                        </div>
                        <div className="col-md-12 mt-5 mb-7 fv-row">
                          <label className="form-label fw-bolder">
                            Tgl. Input
                          </label>
                          <div className="d-flex fs-7">
                            {moment(currentAkademi.tanggal)
                              .locale("ID")
                              .format("D MMM YYYY")}
                          </div>
                        </div>
                        {currentAkademi.status == 3 ||
                        currentAkademi.status == 4 ? (
                          <div>
                            <div className="col-lg-6 mb-7 fv-row">
                              <label className="form-label fw-bolder">
                                Catatan{" "}
                                {capitalizeTheFirstLetterOfEachWord(
                                  currentAkademi.ket_status,
                                )}
                              </label>
                              <br />
                              <span>
                                {currentAkademi.catatan.length > 0 ? (
                                  <div
                                    className="alert alert-danger"
                                    role="alert"
                                  >
                                    {currentAkademi.catatan.replace(
                                      /(?:\\n)/g,
                                      "<br/>",
                                    )}
                                  </div>
                                ) : (
                                  "-"
                                )}
                              </span>
                            </div>
                          </div>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default ReviewKerjasama;
