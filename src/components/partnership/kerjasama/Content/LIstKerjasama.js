import React, { useState, useEffect, useMemo, useRef } from "react";
import Header from "../../../Header";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";
import Select from "react-select";
import axios from "axios";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import DataTable from "react-data-table-component";
import Cookies from "js-cookie";
import moment from "moment";
import * as XLSX from "xlsx";
import _ from "lodash";
import Moment from "react-moment";
import { cekPermition, logout } from "../../../AksesHelper";
import {
  capitalizeFirstLetter,
  capitalizeTheFirstLetterOfEachWord,
  indonesianDateFormat,
} from "../../../publikasi/helper";

const ListKerjasama = () => {
  const btnModalFilter = useRef(null);
  const swal = withReactContent(Swal);
  const [data, setData] = useState([]);
  const [totalKerjasama, setTotalKerjasama] = useState({
    ttl_kerjasama_aktif: 0,
    ttl_pengajuan_kerjasama: 0,
    ttl_kerjasama_akan_habis: 0,
    ttl_pengajuan: 0,
  });

  const [isLoading, setIsLoading] = useState(true);
  const [totalRows, setTotalRows] = useState(0);
  const [listOptionStatus, setListOptionStatus] = useState([]);
  const [listOptionKategoriKerjasama, setListOptionKategoriKerjasama] =
    useState([]);
  const [listOptionMitra, setListOptionMitra] = useState([]);
  const [filterStatus, setFilterStatus] = useState(null);
  const [filterMitra, setFilterMitra] = useState(null);
  const [filterKategoriKerjasama, setFilterKategoriKerjasama] = useState(null);
  const init_status = [
    { id_status_pengajuan: 1 },
    { id_status_pengajuan: 2 },
    { id_status_pengajuan: 3 },
    { id_status_pengajuan: 4 },
    { id_status_pengajuan: 5 },
  ];
  const [status_pengajuan, setStatusPengajuan] = useState(init_status);
  const searchInputRef = useRef();
  const isMitra = useMemo(
    () => Cookies.get("role_id") == "4" || Cookies.get("role_id") == 86,
    [],
  );
  const [preFilter, setPreFilter] = useState({
    mitra: "",
    kategori_kerjasama: "",
    status: "",
  });
  const [queryParam, setQueryParam] = useState({
    pos: 0,
    perPage: 10,
    sortField: "id",
    sortDirection: "desc",
    mitra: "",
    kategori_kerjasama: "",
    status: "",
    word: "",
  });
  const [isPaginationReset, setIsPaginationReset] = useState(false);
  const [isLoadingDownload, setIsLoadingDownload] = useState(false);
  const history = useMemo(() => {
    return (url) => (window.location.href = url);
  }, []);

  const customLoader = useMemo(
    () => (
      <div style={{ padding: "24px" }}>
        <img src="/assets/media/loader/loader-biru.gif" />
      </div>
    ),
    [],
  );

  const columns = useMemo(
    () => [
      {
        Header: "No",
        accessor: "",
        sortType: "basic",
        name: "No",
        sortable: false,
        center: true,
        width: "70px",
        grow: 1,
        allowOverflow: true,
        cell: (row, i) => {
          return <span>{i + 1 + queryParam.pos * queryParam.perPage}</span>;
        },
        sortField: "id",
        selector: (akademi) => akademi.id,
      },
      {
        Header: "MoU/PKS",
        accessor: "",
        className: "min-w-300px mw-300px",
        width: "300px",
        sortType: "basic",
        sortable: true,
        name: "Kerjasama",
        initSort: "mitra",
        allowOverflow: true,
        grow: 6,
        cell: (akademi) => {
          return (
            <>
              <label className="d-flex flex-stack mb- mt-1">
                <span className="d-flex align-items-center me-2">
                  <span className="d-flex flex-column">
                    <a
                      href={"/partnership/kerjasama/review/" + akademi.id}
                      className="text-dark mb-0"
                    >
                      <h6 className="fw-bolder fs-7 mb-0">
                        {akademi.judul_kerjasama}
                      </h6>
                      <span className="text-muted fs-7 fw-semibold">
                        {" "}
                        {akademi.mitra}
                      </span>
                    </a>
                  </span>
                </span>
              </label>
            </>
          );
        },
        sortField: "mitra",
        selector: (akademi) => akademi.mitra,
      },
      {
        Header: "Kategori",
        accessor: "",
        className: "min-w-200px mw-200px",
        sortType: "basic",
        sortable: true,
        name: "Kategori",
        allowOverflow: true,
        width: "200px",
        grow: 6,
        initSort: "kategori_kerjasama",
        cell: (akademi) => {
          return (
            <div style={{ textAlign: "left" }}>
              <span className="fs-7" style={{ textAlign: "left" }}>
                {akademi.kategori_kerjasama}
              </span>
            </div>
          );
        },
        sortField: "kategori_kerjasama",
        selector: (akademi) => akademi.judul_kerjasama,
      },
      {
        Header: "Durasi",
        accessor: "",
        sortType: "basic",
        sortable: true,
        name: "Durasi",
        center: true,
        allowOverflow: true,
        grow: 2,
        initSort: "periode",
        cell: (akademi) => {
          return (
            <div style={{ textAlign: "center" }}>
              <h4>{akademi.slug}</h4>
              <center>
                <span className="fs-7">{parseFloat(akademi.periode)} Thn</span>
              </center>
            </div>
          );
        },
        sortField: "periode",
        selector: (akademi) => akademi.periode,
      },
      {
        Header: "Tgl.Mulai",
        accessor: "",
        sortType: "basic",
        className: "min-w-150px mw-150px",
        center: "true",
        width: "160px",
        sortable: true,
        allowOverflow: true,
        name: "Tgl.Mulai",
        initSort: "tanggal_awal_kerjasama",
        cell: (akademi) => {
          return (
            <div style={{ textAlign: "center" }}>
              <center>
                <span className="fs-7">
                  {akademi.tanggal_awal_kerjasama != "infinity"
                    ? indonesianDateFormat(akademi.tanggal_awal_kerjasama)
                    : "-"}
                </span>
              </center>
            </div>
          );
        },
        sortField: "tanggal_awal_kerjasama",
        selector: (akademi) => akademi.tanggal_awal_kerjasama,
      },
      {
        Header: "Tgl.Akhir",
        accessor: "",
        className: "min-w-150px mw-150px",
        sortType: "basic",
        name: "Tgl.Akhir",
        width: "160px",
        sortable: true,
        center: "ture",
        allowOverflow: true,
        initSort: "tanggal_selesai_kerjasama",
        cell: (akademi) => {
          return (
            <div style={{ textAlign: "center" }}>
              <center>
                <span className="fs-7">
                  {akademi.tanggal_selesai_kerjasama != "infinity"
                    ? indonesianDateFormat(akademi.tanggal_selesai_kerjasama)
                    : "-"}
                </span>
              </center>
            </div>
          );
        },
        sortField: "tanggal_selesai_kerjasama",
        selector: (akademi) => akademi.tanggal_selesai_kerjasama,
      },
      {
        Header: "Status",
        Title: "status",
        accessor: "",
        className: "min-w-200px mw-200px",
        sortable: true,
        name: "Status",
        allowOverflow: true,
        grow: 2,
        initSort: "status",
        cell: (akademi) => {
          let colors = "primary";
          if (akademi.status == "DISETUJUI") {
            colors = "success";
          } else if (akademi.status == "REVISI") {
            colors = "warning";
          } else if (akademi.status == "DITOLAK") {
            colors = "danger";
          }

          return (
            <span className={"badge badge-light-" + colors + " fs-7 m-1"}>
              {capitalizeTheFirstLetterOfEachWord(akademi.status)}
            </span>
          );
        },
        sortField: "status",
        selector: (akademi) => akademi.status,
      },
      {
        Header: "Aksi",
        accessor: "actions",
        sortable: false,
        name: "Aksi",
        width: "180px",
        center: true,
        grow: 3,
        selector: (akademi) => {
          return (
            <div>
              <button
                className="btn btn-icon btn-bg-primary btn-sm me-1"
                onClick={() => {
                  history("/partnership/kerjasama/review/" + akademi.id);
                }}
              >
                <i className="fas fa-eye text-white"></i>
              </button>
              &nbsp;
              {akademi.status == "Draft" || akademi.status == "REVISI" ? (
                <button
                  className="btn btn-icon btn-bg-warning btn-sm me-1"
                  onClick={() => {
                    history("/partnership/kerjasama/edit/" + akademi.id);
                  }}
                >
                  <i className="bi bi-gear-fill text-white"></i>
                </button>
              ) : (
                ""
              )}
              &nbsp;
              {akademi.status == "Draft" ||
              akademi.status == "REVISI" ||
              akademi.status == "DITOLAK" ? (
                <button
                  className="btn btn-icon btn-bg-danger btn-sm me-1"
                  onClick={() => {
                    deleteMouPks(akademi);
                  }}
                >
                  <i className="bi bi-trash-fill text-white"></i>
                </button>
              ) : (
                ""
              )}
              &nbsp;
            </div>
          );
        },
      },
    ],
    [queryParam.perPage, queryParam.pos],
  );

  const customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  const axConfig = {
    headers: { Authorization: "Bearer " + Cookies.get("token") },
  };

  const getData = async () => {
    setData([]);
    setIsLoading(true);
    try {
      let status_pengajuan_id = status_pengajuan;
      if (queryParam.status == 1) {
        status_pengajuan_id = [{ id_status_pengajuan: 1 }];
      }
      if (queryParam.status == 2) {
        status_pengajuan_id = [{ id_status_pengajuan: 2 }];
      } else if (queryParam.status == 3) {
        status_pengajuan_id = [{ id_status_pengajuan: 3 }];
      } else if (queryParam.status == 4) {
        status_pengajuan_id = [{ id_status_pengajuan: 4 }];
      } else if (queryParam.status == 5) {
        status_pengajuan_id = [{ id_status_pengajuan: 5 }];
      }
      const params = {
        start: queryParam.pos * queryParam.perPage,
        rows: queryParam.perPage,
        status_pengajuan: JSON.stringify(status_pengajuan_id),
        status: "-1",
        kategori_kerjsama: queryParam.kategori_kerjasama || "0",
        mitra: isMitra ? Cookies.get("partner_id") : queryParam.mitra || "0",
        sort_by: queryParam.sortField,
        sort_type: queryParam.sortDirection,
        search: queryParam.word,
      };
      let formData = new FormData();
      Object.keys(params).forEach((k) => {
        formData.append(k, params[k]);
      });
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/kerjasama/API_List_Kerjasama",
          params,
          {
            headers: {
              Authorization: "Bearer " + Cookies.get("token"),
            },
          },
        )
        .then((resp) => {
          if (resp.data.result.Status) {
            setData(resp.data.result.Data);
            setTotalRows(resp.data.result.TotalLength[0].jumlah);
            setIsLoading(false);
          } else {
            swal.fire({
              title: "Kesalahan",
              text: resp.data.result.Message || "Error",
              icon: "warning",
            });
            setIsLoading(false);
          }
        })
        .catch((err) => {
          const resp = err.response;
          swal.fire({
            title: "Kesalahan",
            text: resp.data.result.Message || "Error",
            icon: "warning",
          });
          setIsLoading(false);
        });
    } catch (error) {
      console.log(error);
    }
  };

  const downloadData = async () => {
    setIsLoadingDownload(true);
    try {
      let status_pengajuan_id = status_pengajuan;
      if (queryParam.status == 1) {
        status_pengajuan_id = [{ id_status_pengajuan: 1 }];
      }
      if (queryParam.status == 2) {
        status_pengajuan_id = [{ id_status_pengajuan: 2 }];
      } else if (queryParam.status == 3) {
        status_pengajuan_id = [{ id_status_pengajuan: 3 }];
      } else if (queryParam.status == 4) {
        status_pengajuan_id = [{ id_status_pengajuan: 4 }];
      } else if (queryParam.status == 5) {
        status_pengajuan_id = [{ id_status_pengajuan: 5 }];
      }
      const params = {
        start: 0,
        rows: 100000,
        status_pengajuan: JSON.stringify(status_pengajuan_id),
        status: "-1",
        kategori_kerjsama: queryParam.kategori_kerjasama || "0",
        mitra: isMitra ? Cookies.get("partner_id") : queryParam.mitra || "0",
        sort_by: queryParam.sortField,
        sort_type: queryParam.sortDirection,
        search: queryParam.word,
      };

      let formData = new FormData();
      Object.keys(params).forEach((k) => {
        formData.append(k, params[k]);
      });
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/kerjasama/API_List_Kerjasama",
          formData,
          {
            headers: {
              Authorization: "Bearer " + Cookies.get("token"),
            },
          },
        )
        .then((resp) => {
          if (resp.data.result.Status) {
            let data = resp.data.result.Data;
            console.log(data);
            let dataExcel = [];
            let i = 0;
            Object.keys(data).forEach((k) => {
              const {
                row_num,
                kategori_kerjasama,
                id,
                mitra,
                judul_kerjasama,
                periode,
                tanggal_awal_kerjasama,
                tanggal_selesai_kerjasama,
                status,
                countdown,
              } = data[k];

              dataExcel.push({
                No: ++i,
                Mitra: mitra,
                "Judul Kerjasama": judul_kerjasama,
                "Kategori Kerjasama": kategori_kerjasama,
                Periode: `${periode} Tahun`,
                "Tanggal Awal Kerjasama": moment(tanggal_awal_kerjasama)
                  .locale("id")
                  .format("D MMM YYYY"),
                "Tanggal Selesai Kerjasama": moment(tanggal_selesai_kerjasama)
                  .locale("id")
                  .format("D MMM YYYY"),
                "Berakhir Dalam": `${countdown > 0 ? countdown : 0} Hari`,
                Status: status,
              });
            });
            const worksheet = XLSX.utils.json_to_sheet(dataExcel);
            const workbook = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(workbook, worksheet, "Mitra");
            XLSX.writeFile(
              workbook,
              "Daftar Kerjasama Aktif " + moment().format("YYYYMMDD") + ".xlsx",
            );
          } else {
            swal.fire({
              title: "Kesalahan",
              text: resp.data.result.Message || "Error",
              icon: "warning",
            });
          }
        })
        .catch((err) => {
          const resp = err.response;
          swal.fire({
            title: "Kesalahan",
            text: resp.data.result.Message || "Error",
            icon: "warning",
          });
        });
    } catch (error) {
      console.log(error);
    }
    setIsLoadingDownload(false);
  };

  const getListOptionStatus = async () => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/kerjasama/status_kerjasama_list",
        {
          start: 0,
          rows: 100,
        },
        axConfig,
      )
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.result.Status === true) {
            let repo = response.data.result.Data;
            let ui = [
              {
                label: "Semua Status",
                value: -1,
              },
            ];
            const listItem = repo.map((number) => {
              ui.push({
                label: number.name.toUpperCase(),
                value: number.id,
              });
            });
            setListOptionStatus(ui);
          } else {
          }
        }
      });
  };

  const getListOptionKategoriKerjasama = async () => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/masterKerjasama/list_catcoorpaktif",
        {
          start: 0,
          rows: 100,
        },
        axConfig,
      )
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.result.Status === true) {
            let repo = response.data.result.Data;
            let ui = [
              {
                label: "Semua Kategori",
                value: 0,
              },
            ];
            const listItem = repo.map((number) =>
              ui.push({
                label: number.pcooperation_categories,
                value: number.pid,
              }),
            );
            setListOptionKategoriKerjasama(ui);
          } else {
          }
        }
      });
  };

  const getListOptionMitra = async () => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/mitra/list-mitra-s3",
        {
          start: 0,
          length: 2000,
          sort: "nama_mitra",
          sort_val: "asc",
          param: "",
          status: 1,
          tahun: 0, //ganti ke current year
        },
        axConfig,
      )
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.result.Status === true) {
            let repo = response.data.result.Data;
            let ui = [
              {
                label: "Semua Mitra",
                value: 0,
              },
            ];
            const listItem = repo.map((number) =>
              ui.push({
                label: number.nama_mitra,
                value: number.id,
              }),
            );
            setListOptionMitra(ui);
          } else {
          }
        }
      });
  };

  const getTotalKerjasamaAktif = async () => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/kerjasama/API_Total_Kerjasama_Aktif",
        { id: 1 },
        axConfig,
      )
      .then((resp) => {
        if (resp.data.result.Status) {
          setTotalKerjasama(resp.data.result?.Data[0]);
        }
      })
      .catch((err) => {});
  };

  const deleteMouPks = (cell) => {
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          let params = new FormData();
          params.append("id", cell.id);
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI +
                "/kerjasama/API_Hapus_Kerjasama",
              params,
              axConfig,
            )
            .then(function (response) {
              console.log("nidw");
              console.log(response);
              if (response.status === 200) {
                if (response.data.result.Status === true) {
                  swal.fire({
                    title: <strong>Information!</strong>,
                    html: <i>{response.data.result.Message}</i>,
                    icon: "success",
                    willClose: () => {
                      getData();
                    },
                  });
                  getData();
                } else {
                  swal.fire({
                    title: <strong>Information!</strong>,
                    html: <i>{response.data.result.Message}</i>,
                    icon: "info",
                  });
                }
              } else {
              }
            })
            .catch((error) => {
              swal.fire({
                title: "Terjadi kesalahan",
                icon: "warning",
                text:
                  error.response.data.result.Message || "data tidak terhapus",
              });
            });
        }
      });
  };

  useEffect(() => {
    getListOptionKategoriKerjasama();
    getListOptionMitra();
    getListOptionStatus();
    getTotalKerjasamaAktif();
  }, []);

  useEffect(() => {
    if (cekPermition().view !== 1) {
      Swal.fire({
        title: "Akses Tidak Diizinkan",
        html: "<i> Anda akan kembali kehalaman login </i>",
        icon: "warning",
      }).then(() => {
        logout();
      });
    }
    if (Cookies.get("role_id_user") != 86) {
      Swal.fire({
        title: "Akses Tidak Diizinkan",
        html: "<i> Hanya Bisa Diakses Oleh Mitra </i>",
        icon: "warning",
      }).then(() => {
        window.location = "/";
      });
    }
  }, []);

  useEffect(() => {
    getData();
  }, [queryParam]);

  useEffect(() => {
    if (preFilter?.isReset) {
      btnModalFilter.current?.click();
      setPreFilter({ ...preFilter, isReset: false });
    }
  }, [preFilter]);

  const onSortTable = (column, sortDirection) => {
    if (column.sortable) {
      setQueryParam((q) => ({
        ...q,
        sortField: column.sortField,
        sortDirection: sortDirection,
      }));
    }
  };

  const handlePageChange = (page) => {
    setQueryParam((q) => ({ ...q, pos: page - 1 }));
  };

  const handlePerRowsChange = async (newPerPage, page) => {
    setQueryParam((q) => ({ ...q, pos: page - 1, perPage: newPerPage }));
  };

  const handleSearch = () => {
    setIsPaginationReset(!isPaginationReset);
    setQueryParam((q) => ({
      ...q,
      pos: 0,
      word: searchInputRef.current.value,
    }));
  };

  return (
    <div>
      <Header></Header>
      <SideNav></SideNav>
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z"
                        fill="#FFC700"
                      ></path>
                      <path
                        d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z"
                        fill="#FFC700"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Partnership
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Kerjasama
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                  data-bs-toggle="modal"
                  data-bs-target="#filter"
                >
                  <i className="bi bi-sliders"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Filter
                  </span>
                </button>

                <a
                  href="/partnership/tambah-kerjasama"
                  className="btn btn-success fw-bolder btn-sm"
                >
                  <i className="bi bi-plus-circle"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Tambah Kerjasama
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Daftar Kerjasama
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <div className="d-flex align-items-center position-relative my-1 me-2">
                            <span className="svg-icon svg-icon-1 position-absolute ms-6">
                              <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className="mh-50px"
                              >
                                <rect
                                  opacity="0.5"
                                  x="17.0365"
                                  y="15.1223"
                                  width="8.15546"
                                  height="2"
                                  rx="1"
                                  transform="rotate(45 17.0365 15.1223)"
                                  fill="currentColor"
                                ></rect>
                                <path
                                  d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                  fill="currentColor"
                                ></path>
                              </svg>
                            </span>
                            <input
                              ref={searchInputRef}
                              type="text"
                              onChange={_.debounce(handleSearch, 700)}
                              data-kt-user-table-filter="search"
                              className="form-control form-control-sm form-control-solid w-250px ps-14 me-3"
                              placeholder="Cari Pengajuan Kerjasama"
                              name="cari"
                            />
                            <a onClick={downloadData}>
                              <button
                                className={`btn btn-sm btn-flex btn-light fw-bolder me-2 ${
                                  isLoadingDownload ? "disabled" : ""
                                }`}
                                disabled={isLoadingDownload}
                              >
                                {isLoadingDownload && (
                                  <span
                                    className="spinner-border spinner-border-sm"
                                    role="status"
                                    aria-hidden="true"
                                  ></span>
                                )}
                                <i className="bi bi-cloud-download"></i>
                                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                  Export
                                </span>
                              </button>
                            </a>
                          </div>
                          <div className="modal fade" tabIndex="-1" id="filter">
                            <div className="modal-dialog modal-lg">
                              <div className="modal-content">
                                <div className="modal-header">
                                  <h5 className="modal-title">
                                    <span className="svg-icon svg-icon-5 me-1">
                                      <i className="bi bi-sliders text-black"></i>
                                    </span>
                                    Filter Data Pengajuan Kerjasama
                                  </h5>
                                  <div
                                    className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                    data-bs-dismiss="modal"
                                    aria-label="Close"
                                  >
                                    <span className="svg-icon svg-icon-2x">
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                      >
                                        <rect
                                          opacity="0.5"
                                          x="6"
                                          y="17.3137"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(-45 6 17.3137)"
                                          fill="currentColor"
                                        />
                                        <rect
                                          x="7.41422"
                                          y="6"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(45 7.41422 6)"
                                          fill="currentColor"
                                        />
                                      </svg>
                                    </span>
                                  </div>
                                </div>
                                <div className="modal-body">
                                  <div className="row">
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label">
                                        Mitra
                                      </label>
                                      <Select
                                        name="mitra"
                                        placeholder="Silahkan pilih"
                                        noOptionsMessage={({ inputValue }) =>
                                          !inputValue
                                            ? filterMitra.value
                                            : "Data tidak tersedia"
                                        }
                                        value={filterMitra}
                                        className="form-select-sm selectpicker p-0"
                                        options={listOptionMitra}
                                        onChange={(m) => {
                                          setFilterMitra(m);
                                          setPreFilter((c) => ({
                                            ...c,
                                            mitra: m.value,
                                          }));
                                        }}
                                      />
                                    </div>
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label">
                                        Kategori Kerjasama
                                      </label>
                                      <Select
                                        name="kategori_kerjasama"
                                        placeholder="Silahkan pilih"
                                        noOptionsMessage={({ inputValue }) =>
                                          !inputValue
                                            ? filterKategoriKerjasama.value
                                            : "Data tidak tersedia"
                                        }
                                        value={filterKategoriKerjasama}
                                        className="form-select-sm selectpicker p-0"
                                        options={listOptionKategoriKerjasama}
                                        onChange={(k) => {
                                          setFilterKategoriKerjasama(k);
                                          setPreFilter((c) => ({
                                            ...c,
                                            kategori_kerjasama: k.value,
                                          }));
                                        }}
                                      />
                                    </div>
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label">
                                        Status
                                      </label>
                                      <Select
                                        name="status"
                                        placeholder="Silahkan pilih"
                                        noOptionsMessage={({ inputValue }) =>
                                          !inputValue
                                            ? filterStatus.value
                                            : "Data tidak tersedia"
                                        }
                                        value={filterStatus}
                                        className="form-select-sm selectpicker p-0"
                                        options={listOptionStatus}
                                        onChange={(s) => {
                                          setFilterStatus(s);
                                          setPreFilter((c) => ({
                                            ...c,
                                            status: s.value,
                                          }));
                                        }}
                                      />
                                    </div>
                                  </div>
                                </div>
                                <div className="modal-footer">
                                  <div className="d-flex justify-content-between">
                                    <button
                                      type="button"
                                      className="btn btn-sm btn-light me-3"
                                      onClick={() => {
                                        setFilterKategoriKerjasama(null);
                                        setFilterStatus(null);
                                        setFilterMitra(null);
                                        setPreFilter({
                                          mitra: "",
                                          kategori_kerjasama: "",
                                          status: "",
                                          isReset: true,
                                        });
                                      }}
                                    >
                                      Reset
                                    </button>
                                    <button
                                      ref={btnModalFilter}
                                      type="button"
                                      className="btn btn-sm btn-primary"
                                      data-bs-dismiss="modal"
                                      onClick={() => {
                                        setIsPaginationReset(
                                          !isPaginationReset,
                                        );
                                        setQueryParam((q) => ({
                                          ...q,
                                          ...preFilter,
                                          pos: 0,
                                        }));
                                      }}
                                    >
                                      Apply Filter
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <DataTable
                          columns={columns}
                          data={data}
                          progressPending={isLoading}
                          highlightOnHover
                          pointerOnHover
                          paginationTotalRows={totalRows}
                          paginationComponentOptions={{
                            selectAllRowsItem: true,
                            selectAllRowsItemText: "Semua",
                          }}
                          customStyles={customStyles}
                          progressComponent={customLoader}
                          paginationDefaultPage={1}
                          paginationResetDefaultPage={isPaginationReset}
                          persistTableHead={true}
                          pagination
                          paginationServer
                          sortServer
                          onChangeRowsPerPage={handlePerRowsChange}
                          onChangePage={handlePageChange}
                          onSort={onSortTable}
                          noDataComponent={
                            <div className="mt-5">Tidak Ada Data</div>
                          }
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer></Footer>
    </div>
  );
};

export default ListKerjasama;
