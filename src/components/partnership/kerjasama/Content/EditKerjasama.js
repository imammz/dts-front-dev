import moment from "moment";
import React, { useEffect, useState } from "react";
import axios from "axios";
import Cookies from "js-cookie";
import { useParams } from "react-router-dom";
import Select from "react-select";
import Swal from "sweetalert2";
import Footer from "../../../../components/Footer";
import Header from "../../../../components/Header";
import SideNav from "../../../../components/SideNav";
import {
  fetchAllStatus,
  fetchMitraById,
  fetchDetailKategoriKerjasama,
  fetchDetailKerjasama,
  fetchKategoriKerjasama,
  fetchMitras,
  hasError,
  insertKerjasama,
  resetError,
  updateKerjasama,
  validate,
} from "../actions";
import Moment from "react-moment";
import { numericOnly, numericOnlyDirect } from "../../../publikasi/helper";

const EditKerjasama = () => {
  const { id = null } = useParams();
  const [error, setError] = useState({});
  const [dataStatus, setDataStatus] = useState([]);
  const [dataMitra, setDataMitra] = useState([]);
  const [dataKategoriKerjasama, setDataKategoriKerjasama] = useState([]);
  const [dataFormKerjasama, setDataFormKerjasama] = useState([]);
  const [selectedIdMitra, setSelectedIdMitra] = useState(null);
  let [kerjasama, setKerjasama] = useState({
    tanggal: moment().locale("ID").format("DD MMMM YYYY"),
    lembaga: "",
    email: "",
    periode: "",
    periode_mulai: null,
    judul: "",
    kategori: "",
    deskripsi: "",
    deskripsi2: "",
    tujuan: "",
    file: null,
    nama_mitra: "",
    kerjasama_akhir: "",
    nope_lembaga: "",
    nope_kekom: "",
    kerjasama_ttd: "",
    cooperation_category_id: "",
    status_kerjasama: "",
    catatan: "",
  });

  useEffect(() => {
    fetch();
  }, []);

  const fetch = async () => {
    Swal.fire({
      title: "Mohon Tunggu..",
      icon: "info",
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      },
    });
    const [kerjasama, dataMitra, dataKategoriKerjasama, dataStatus] =
      await Promise.all([
        fetchDetailKerjasama(id),
        fetchMitraById(),
        fetchKategoriKerjasama({ start: 0, length: 2000 }),
        fetchAllStatus({}),
      ]);

    if (kerjasama.status != 1 && kerjasama.status != 3) {
      window.location.href = "/partnership/kerjasama";
    }

    let [dataFormKerjasama] = await Promise.all([
      fetchDetailKategoriKerjasama(kerjasama?.id_kategori_kerjasama),
    ]);

    dataFormKerjasama = dataFormKerjasama?.map((k) => ({
      ...k,
      value: kerjasama?.kategori?.find(
        (_k) => _k.cooperation_category_form_id == k.id,
      )?.cooperation_form_content,
    }));

    const dataStatusEdit = [];
    dataStatus.forEach(function (element) {
      if (element.name == "REVIEW" || element.name == "Draft") {
        element.name = element.name.toUpperCase();
        dataStatusEdit.push(element);
      }
    });
    console.log(numericOnlyDirect(kerjasama.periode_kerjasama));
    setDataStatus(dataStatusEdit);
    setDataMitra(dataMitra[0]);
    setDataKategoriKerjasama(dataKategoriKerjasama);
    setDataFormKerjasama(dataFormKerjasama);
    setSelectedIdMitra(kerjasama?.idpartner_name);
    setKerjasama({
      tanggal: moment(kerjasama?.tanggal).locale("ID").format("DD MMMM YYYY"),
      lembaga: "",
      email: "",
      periode: numericOnlyDirect(kerjasama.periode_kerjasama),
      periode_mulai:
        kerjasama.periode_kerjasama_mulai != "infinity"
          ? kerjasama.periode_kerjasama
          : null,
      judul: kerjasama?.judul_kerjasama,
      kategori: `${kerjasama?.id_kategori_kerjasama}`,
      deskripsi: "",
      deskripsi2: "",
      tujuan: "",
      file: { name: kerjasama?.file_kerjasama },
      nama_mitra: "",
      kerjasama_akhir:
        kerjasama.periode_kerjasama_mulai != "infinity"
          ? moment(kerjasama?.periode_kerjasama_mulai)
              .add(kerjasama?.periode_kerjasama, "years")
              .valueOf() / 1000
          : null,
      nope_lembaga: kerjasama?.nomor_perjanjian_lembaga,
      nope_kekom: kerjasama?.nomor_perjanjian_kemkominfo,
      kerjasama_ttd: moment(kerjasama?.tanggal_tanda_tangan).valueOf() / 1000,
      cooperation_category_id: "",
      status_kerjasama: kerjasama.status == 3 ? null : kerjasama.status,
      catatan: kerjasama?.catatan,
    });
    Swal.close();
  };

  let kerjasama_ttd = new Date();
  if (kerjasama.kerjasama_ttd) kerjasama_ttd = new Date(0);
  kerjasama_ttd.setUTCSeconds(kerjasama.kerjasama_ttd);

  let periode_mulai = new Date();
  if (kerjasama.periode_mulai) periode_mulai = new Date(0);
  periode_mulai.setUTCSeconds(kerjasama.periode_mulai);

  const handleInputTanggal = (e) => {
    const value = e;
    console.log(value);
    if (value != "" && value != "Invalid date") {
      console.log(value);
      let [dd, m, y] = value.split(" ");
      const months = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Agt",
        "Sep",
        "Okt",
        "Nov",
        "Des",
      ];
      months.forEach(function (element, i) {
        if (element == m) {
          m = i + 1;
        }
      });
      let monthNum = m;

      /* eslint-disable no-undef */
      if (typeof id?.months?.shorthand != "undefined") {
        const shm = id?.months?.shorthand;
        for (let i = 0; i < shm.length; i++) {
          if (shm[i] == m) {
            monthNum = i + 1;
          }
        }
      }
      if (`${monthNum}`.length == 1) {
        monthNum = `0${monthNum}`;
      } else {
        monthNum = `${monthNum}`;
      }

      const tAwalM = moment(`${dd} ${monthNum} ${y}`, "D MM YYYY");
      let tanggal_return = tAwalM.format("YYYY-MM-DD");
      return tanggal_return;
    }
  };

  const handleWriteTanggal = (tanggal) => {
    if (tanggal != null && tanggal != "") {
      let [y, m, dd] = tanggal.split("-");
      const months = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Agt",
        "Sept",
        "Okt",
        "Nov",
        "Des",
      ];
      months.forEach(function (element, i) {
        if (element == m) {
          m = i + 1;
        }
      });
      let monthNum = m;

      /* eslint-disable no-undef */
      if (typeof id?.months?.shorthand != "undefined") {
        const shm = id?.months?.shorthand;
        for (let i = 0; i < shm.length; i++) {
          if (shm[i] == m) {
            monthNum = i + 1;
          }
        }
      }
      if (`${monthNum}`.length == 1) {
        monthNum = `0${monthNum}`;
      } else {
        monthNum = `${monthNum}`;
      }

      const tAwalM = moment(`${dd} ${monthNum} ${y}`, "D MM YYYY");

      return tAwalM.format("D MMM YYYY");
    }
  };

  const handleDownloadData = (url, nama) => {
    const configs = {
      responseType: "arraybuffer",
      headers: {
        Authorization: "Bearer " + Cookies.get("token"),
        Accept: "*/*",
        "Content-Length": 0,
        Connection: "keep-alive",
      },
    };
    swal.fire({
      title: "Memproses Download File, Mohon Tunggu..",
      icon: "info",
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios.post(url, configs).then((response) => {
      const byteString = window.atob(response.data);
      const arrayBuffer = new ArrayBuffer(byteString.length);
      const int8Array = new Uint8Array(arrayBuffer);
      for (let i = 0; i < byteString.length; i++) {
        int8Array[i] = byteString.charCodeAt(i);
      }
      const blob = new Blob([int8Array], { type: "application/pdf" });
      var fileURL = window.URL.createObjectURL(blob);
      var fileLink = document.createElement("a");
      fileLink.href = fileURL;
      fileLink.setAttribute("download", nama + ".pdf");
      document.body.appendChild(fileLink);
      fileLink.click();
      swal.close();
    });
  };

  return (
    <div>
      <Header />
      <SideNav />
      <div className="toolbar" id="kt_toolbar">
        <div
          id="kt_toolbar_container"
          className="container-fluid d-flex flex-stack my-2"
        >
          <div className="d-flex align-items-start my-2">
            <div>
              <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                <span className="me-3">
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    className="mh-50px"
                  >
                    <path
                      opacity="0.3"
                      d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z"
                      fill="#FFC700"
                    ></path>
                    <path
                      d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z"
                      fill="#FFC700"
                    ></path>
                  </svg>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Partnership
                  <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                </span>
                Kerjasama
              </h1>
            </div>
          </div>
          <div className="d-flex align-items-end my-2">
            <div>
              <button
                onClick={() => {
                  window.location.href = "/partnership/kerjasama";
                }}
                type="reset"
                className="btn btn-sm btn-light btn-active-light-primary"
                data-kt-menu-dismiss="true"
              >
                <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                  <i className="fa fa-chevron-left"></i>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Kembali
                </span>
              </button>

              <button className="btn btn-sm btn-danger btn-active-light-info ms-2">
                <i className="bi bi-trash-fill text-white"></i>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Hapus
                </span>
              </button>
            </div>
          </div>
        </div>
      </div>
      <div
        className="wrapper d-flex flex-column flex-row-fluid pt-0"
        id="kt_wrapper"
      >
        <div
          className="content d-flex flex-column flex-column-fluid pt-0"
          id="kt_content"
        >
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container-xxl">
              <div className="row">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Edit Proposal Kerjasama
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <form>
                        <div className="row">
                          <div className="col-12 col-md-6">
                            <div>
                              <label
                                htmlFor="nama_mitra"
                                className="required form-label"
                              >
                                Nama Mitra
                              </label>
                              <div className="fw-bold">
                                <strong>{dataMitra.nama_mitra}</strong>
                              </div>
                            </div>
                            <div className="mb-7 mt-2 text-danger">
                              {error?.nama_mitra}
                            </div>
                          </div>
                          <div className="col-12 col-md-6">
                            <div>
                              <label
                                htmlFor="email_mitra"
                                className="required form-label"
                              >
                                Email Mitra
                              </label>
                              <div className="fw-bold">
                                <strong>{dataMitra.email}</strong>
                              </div>
                            </div>
                          </div>
                          <div className="col-12">
                            <div>
                              <label
                                htmlFor="judul"
                                className="required form-label"
                              >
                                Judul Kerjasama
                              </label>
                              <input
                                type="text"
                                value={kerjasama.judul}
                                onChange={(e) => {
                                  const value = e?.target?.value;
                                  setKerjasama((state) => ({
                                    ...state,
                                    judul: value,
                                  }));
                                  setError((error) => ({
                                    ...error,
                                    judul: null,
                                  }));
                                }}
                                className="form-control form-control-sm"
                                placeholder="Masukan judul kerjasama"
                              />
                            </div>
                            <div className="mb-7 mt-2 text-danger">
                              {error?.judul}
                            </div>
                          </div>
                          <div className="col-12">
                            <div>
                              <label
                                htmlFor="scop"
                                className="required form-label"
                              >
                                Kategori Kerjasama
                              </label>
                              <Select
                                name="status"
                                placeholder="Silahkan Pilih Kategori Kerjasama"
                                className="form-select-sm selectpicker p-0"
                                options={dataKategoriKerjasama.map((k) => ({
                                  value: k.pid,
                                  label: k.pcooperation_categories,
                                }))}
                                value={dataKategoriKerjasama
                                  .map((k) => ({
                                    value: k.pid,
                                    label: k.pcooperation_categories,
                                  }))
                                  .find((k) => k.value == kerjasama.kategori)}
                                onChange={async (o) => {
                                  const dataFormKerjasama =
                                    await fetchDetailKategoriKerjasama(
                                      o?.value,
                                    );
                                  setKerjasama((k) => ({
                                    ...k,
                                    kategori: o?.value,
                                  }));
                                  setDataFormKerjasama(dataFormKerjasama);
                                  setError((error) => ({
                                    ...error,
                                    kategori: null,
                                  }));
                                }}
                              />
                            </div>
                            <div className="mb-7 mt-2 text-danger">
                              {error?.kategori}
                            </div>
                          </div>
                          {dataFormKerjasama.length != 0 &&
                            dataFormKerjasama.map((item, idx) => (
                              <React.Fragment key={idx}>
                                <div className="col-12">
                                  <label
                                    className="form-label required"
                                    htmlFor="title"
                                  >
                                    {item.cooperation_form}
                                  </label>
                                  <input
                                    type="text"
                                    className="form-control form-control-sm"
                                    name={item.cooperation_form}
                                    index={idx}
                                    placeholder={item.cooperation_form}
                                    defaultValue={item?.value}
                                    onChange={(e) => {
                                      const value = e?.target?.value;
                                      dataFormKerjasama[idx].value = value;
                                      setDataFormKerjasama(dataFormKerjasama);

                                      error.dataFormKerjasama =
                                        error.dataFormKerjasama || [];
                                      error.dataFormKerjasama[idx] = null;
                                      setError((error) => ({
                                        ...error,
                                        dataFormKerjasama:
                                          error.dataFormKerjasama,
                                      }));
                                    }}
                                  ></input>
                                </div>
                                <span className="mb-7" style={{ color: "red" }}>
                                  {(error?.dataFormKerjasama || [])[idx]}
                                </span>
                              </React.Fragment>
                            ))}
                          <div className="col-12">
                            <div>
                              <label
                                htmlFor="periode"
                                className="required form-label"
                              >
                                Durasi
                              </label>
                              <div className="input-group input-group-sm mb-3">
                                <input
                                  value={kerjasama.periode}
                                  onChange={(e) => {
                                    let value = numericOnlyDirect(
                                      e?.target?.value,
                                    );
                                    try {
                                      if (value.match(/\./g).length > 1) {
                                        value = value.slice(0, -1);
                                      }
                                    } catch (err) {}
                                    setKerjasama((state) => ({
                                      ...state,
                                      periode: value,
                                    }));
                                    setError((error) => ({
                                      ...error,
                                      periode: null,
                                    }));
                                  }}
                                  onKeyDown={(evt) =>
                                    evt.key === "," && evt.preventDefault()
                                  }
                                  className="form-control form-control-sm"
                                  placeholder="Periode Kerjasama"
                                />
                                <span
                                  className="input-group-text"
                                  id="basic-addon2"
                                >
                                  Tahun
                                </span>
                              </div>
                            </div>
                            <div className="mb-7 mt-2 text-danger">
                              {error?.periode}
                            </div>
                          </div>
                          <label
                            className="form-label required"
                            htmlFor="title"
                          >
                            Lampiran Dokumen Kerjasama
                          </label>
                          {kerjasama.file ? (
                            <div className="col-lg-12 mb-5">
                              <div>
                                <a
                                  href="#"
                                  url={
                                    process.env.REACT_APP_BASE_API_URI +
                                    "/kerjasama/API_Get_File_Kerjasama?path=" +
                                    kerjasama.file
                                  }
                                  onClick={() =>
                                    handleDownloadData(
                                      process.env.REACT_APP_BASE_API_URI +
                                        "/kerjasama/API_Get_File_Kerjasama2?path=" +
                                        kerjasama.file.name,
                                      kerjasama.judul,
                                    )
                                  }
                                  title="Download"
                                  className="btn btn-light fw-bolder btn-sm me-2 mb-2"
                                  file={kerjasama.file}
                                >
                                  <i className="bi bi-cloud-download"></i>{" "}
                                  Download
                                </a>
                                <a
                                  className="btn btn-danger fw-bolder btn-sm me-2 mb-2"
                                  title="Hapus file"
                                  onClick={(e) =>
                                    setKerjasama((f) => ({ ...f, file: null }))
                                  }
                                >
                                  <i className="bi bi-trash-fill text-white"></i>
                                </a>
                              </div>
                            </div>
                          ) : (
                            ""
                          )}
                          <div className="col-12">
                            <div>
                              <input
                                className="form-control form-control-sm"
                                type="file"
                                accept=".pdf"
                                onChange={(e) => {
                                  const [file] = e?.target?.files;
                                  if (file)
                                    setKerjasama((f) => ({ ...f, file: file }));
                                  else
                                    setKerjasama((f) => ({ ...f, file: null }));
                                  setError((error) => ({
                                    ...error,
                                    file: null,
                                  }));
                                  e.target.value = null;
                                }}
                              />
                              <span className="text-muted font-size-sm">
                                File yang diupload *.pdf dengan ukuran maksimal
                                1 MB
                              </span>
                              <br />
                            </div>
                            <div className="mb-7 mt-2" style={{ color: "red" }}>
                              {error?.file}
                            </div>
                          </div>

                          {kerjasama.catatan != "" ? (
                            <div className="col-12 col-md-12 mb-7">
                              <div>
                                <label
                                  htmlFor="nama_mitra"
                                  className="required form-label"
                                >
                                  Catatan{" "}
                                  {kerjasama.status_kerjasama == 3
                                    ? "REVISI"
                                    : ""}
                                </label>
                                <div
                                  className="alert alert-danger"
                                  role="alert"
                                >
                                  {kerjasama.catatan.replace(
                                    /(?:\\n)/g,
                                    "<br/>",
                                  )}
                                </div>
                              </div>
                            </div>
                          ) : (
                            ""
                          )}

                          <div className="col-12 col-md-12">
                            <div>
                              <label
                                htmlFor="nama_mitra"
                                className="required form-label"
                              >
                                Status
                              </label>
                              <Select
                                name="status"
                                placeholder="Silahkan pilih"
                                // noOptionsMessage={"Data tidak tersedia"}
                                className="form-select-sm selectpicker p-0"
                                options={dataStatus.map((m) => ({
                                  value: m?.id,
                                  label: m?.name,
                                }))}
                                value={dataStatus
                                  .map((m) => ({
                                    value: m?.id,
                                    label: m?.name,
                                  }))
                                  .find(
                                    (m) =>
                                      m.value == kerjasama?.status_kerjasama,
                                  )}
                                onChange={(o) => {
                                  setKerjasama((k) => ({
                                    ...k,
                                    status_kerjasama: o?.value,
                                  }));
                                  setError((error) => ({
                                    ...error,
                                    status_kerjasama: null,
                                  }));
                                }}
                              />
                            </div>
                            <div className="mb-7 mt-2 text-danger">
                              {error?.status_kerjasama}
                            </div>
                          </div>
                          <div className="col-12">
                            <label
                              className="form-label required"
                              htmlFor="title"
                            >
                              Tgl. Input
                            </label>
                            <p className="fs-7">{kerjasama.tanggal}</p>
                          </div>
                        </div>
                        <div className="form-group fv-row pt-7 mb-7">
                          <div className="d-flex justify-content-center mb-7">
                            <div className="d-flex justify-content-between px-5 pb-5">
                              <button
                                className="btn btn-light me-2"
                                onClick={() => {
                                  window.location.href =
                                    "/partnership/kerjasama";
                                }}
                              >
                                Batal
                              </button>
                              <button
                                className="btn btn-primary"
                                onClick={async (e) => {
                                  e.preventDefault();
                                  kerjasama = {
                                    ...kerjasama,
                                    dataFormKerjasama: dataFormKerjasama,
                                  };
                                  let error = validate(kerjasama);
                                  if (!hasError(error)) {
                                    let formPostData = [];
                                    if (dataFormKerjasama.length > 0) {
                                      for (
                                        let i = 0;
                                        i < dataFormKerjasama.length;
                                        i++
                                      ) {
                                        formPostData.push({
                                          cooperation_category_form_id:
                                            dataFormKerjasama[i].id,
                                          cooperation_form_content:
                                            dataFormKerjasama[i].value_text,
                                        });
                                      }
                                    }

                                    const isFile = (input) =>
                                      "File" in window && input instanceof File;
                                    let payload = {
                                      tanggal: moment(kerjasama.tanggal).format(
                                        "YYYY-MM-DD",
                                      ),
                                      Lembaga: Cookies.get("partner_id"),
                                      Judul_Kerjasama: kerjasama.judul,
                                      Tujuan_Kerjasama: "",
                                      Form_Kerjasama: "",
                                      Nomor_Perjanjian_Lembaga: "",
                                      Nomor_Perjanjian_Kekominfo: "",
                                      nomor_mou_pks: "",
                                      Tanggal_Tanda_Tangan: "2000-01-01",
                                      Periode_Kerjasama_mulai:
                                        kerjasama.periode_mulai,
                                      Periode_Kerjasama_selesai:
                                        kerjasama.kerjasama_akhir,
                                      Periode_Kerjasama: kerjasama.periode,
                                      Kategori_kerjasama: kerjasama.kategori,

                                      status_kerjasama:
                                        kerjasama.status_kerjasama,
                                      status_migrates_id:
                                        kerjasama?.status_kerjasama,
                                      id: id,
                                      json_detail: JSON.stringify(
                                        dataFormKerjasama.map((f) => ({
                                          cooperation_category_form_id: f.id,
                                          cooperation_form_content: f.value,
                                        })),
                                      ),
                                    };
                                    if (isFile(kerjasama.file)) {
                                      payload["Unggah_Dokumen_Kerjasama"] =
                                        kerjasama.file;
                                    }

                                    var form = new FormData();
                                    for (var key in payload) {
                                      if (payload)
                                        form.append(key, payload[key]);
                                    }

                                    Swal.fire({
                                      title: "Mohon Tunggu!",
                                      icon: "info",
                                      allowOutsideClick: false,
                                      didOpen: () => Swal.showLoading(),
                                    });

                                    let message = await updateKerjasama(form);

                                    Swal.close();

                                    await Swal.fire({
                                      title: "Kerjasama berhasil disimpan",
                                      icon: "success",
                                      confirmButtonText: "Ok",
                                    });

                                    window.location.href =
                                      "/partnership/kerjasama";
                                  } else {
                                    setError({ ...resetError(), ...error });

                                    Swal.fire({
                                      title:
                                        "Masih terdapat isian yang belum lengkap",
                                      icon: "warning",
                                      confirmButtonText: "Ok",
                                    });
                                  }
                                }}
                              >
                                <i className="fa fa-paper-plane ms-1"></i>Simpan
                              </button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default EditKerjasama;
