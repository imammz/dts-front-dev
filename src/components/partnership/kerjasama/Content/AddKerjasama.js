import moment from "moment";
import React, { useEffect, useState } from "react";
import Cookies from "js-cookie";
import Select from "react-select";
import Swal from "sweetalert2";
import Footer from "../../../../components/Footer";
import Header from "../../../../components/Header";
import SideNav from "../../../../components/SideNav";
import {
  fetchDetailKategoriKerjasama,
  fetchKategoriKerjasama,
  fetchMitraById,
  fetchMitras,
  hasError,
  insertKerjasama,
  resetError,
  validate,
} from "../actions";

const TambahKerjasama = () => {
  const [error, setError] = useState({});
  const [dataMitra, setDataMitra] = useState([]);
  const [dataKategoriKerjasama, setDataKategoriKerjasama] = useState([]);
  const [dataFormKerjasama, setDataFormKerjasama] = useState([]);

  let [kerjasama, setKerjasama] = useState({
    tanggal: moment().locale("ID").format("DD MMMM YYYY"),
    lembaga: "",
    email: "",
    periode: "",
    periode_mulai: null,
    judul: "",
    kategori: "",
    deskripsi: "",
    deskripsi2: "",
    tujuan: "",
    file: null,
    nama_mitra: "",
    kerjasama_akhir: "",
    nope_lembaga: "",
    nope_kekom: "",
    nomor_mou_pks: "",
    kerjasama_ttd: "",
    cooperation_category_id: "",
    status_kerjasama: "",
  });

  useEffect(() => {
    fetch();
  }, []);

  const fetch = async () => {
    const [dataMitra, dataKategoriKerjasama] = await Promise.all([
      fetchMitraById(),
      fetchKategoriKerjasama({ start: 0, length: 2000 }),
    ]);
    setDataMitra(dataMitra[0]);
    setDataKategoriKerjasama(dataKategoriKerjasama);
  };

  let kerjasama_ttd = new Date();
  if (kerjasama.kerjasama_ttd) kerjasama_ttd = new Date(0);
  kerjasama_ttd.setUTCSeconds(kerjasama.kerjasama_ttd);

  const simpan = async (status_pengajuan) => {
    kerjasama = {
      ...kerjasama,
      dataFormKerjasama: dataFormKerjasama,
    };

    kerjasama.status_kerjasama = status_pengajuan;

    let error = validate(kerjasama);

    if (!hasError(error)) {
      let formPostData = [];
      if (dataFormKerjasama.length > 0) {
        for (let i = 0; i < dataFormKerjasama.length; i++) {
          formPostData.push({
            cooperation_category_form_id: dataFormKerjasama[i].id,
            cooperation_form_content: dataFormKerjasama[i].value,
          });
        }
      }
      let payload = {
        tanggal: kerjasama.tanggal,
        Lembaga: Cookies.get("partner_id"),
        Judul_Kerjasama: kerjasama.judul,
        Nomor_Perjanjian_Lembaga: "",
        Nomor_Perjanjian_Kemkominfo: "",
        nomor_mou_pks: "",
        Tanggal_Tanda_Tangan: "2000-01-01",
        Periode_Kerjasama_mulai: null,
        Periode_Kerjasama_selesai: null,
        Periode_Kerjasama: kerjasama.periode,
        Kategori_kerjasama: kerjasama.kategori,
        Unggah_Dokumen_Kerjasama: kerjasama.file,
        status_kerjasama: status_pengajuan,
        json_detail: JSON.stringify(
          dataFormKerjasama.map((f) => ({
            cooperation_category_form_id: f.id,
            cooperation_form_content: f.value,
          })),
        ),
      };

      var form = new FormData();
      for (var key in payload) {
        form.append(key, payload[key]);
      }

      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info",
        allowOutsideClick: false,
        didOpen: () => Swal.showLoading(),
      });

      let message = await insertKerjasama(form);

      Swal.close();

      await Swal.fire({
        title: message || "Kerjasama berhasil disimpan",
        icon: "success",
        confirmButtonText: "Ok",
      });

      window.location.href = "/partnership/kerjasama";
    } else {
      setError({ ...resetError(), ...error });

      Swal.fire({
        title: "Masih terdapat isian yang belum lengkap",
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  return (
    <div>
      <Header />
      <SideNav />
      <div className="toolbar" id="kt_toolbar">
        <div
          id="kt_toolbar_container"
          className="container-fluid d-flex flex-stack my-2"
        >
          <div className="d-flex align-items-start my-2">
            <div>
              <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                <span className="me-3">
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    className="mh-50px"
                  >
                    <path
                      opacity="0.3"
                      d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z"
                      fill="#FFC700"
                    ></path>
                    <path
                      d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z"
                      fill="#FFC700"
                    ></path>
                  </svg>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Partnership
                  <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                </span>
                Kerjasama
              </h1>
            </div>
          </div>
          <div className="d-flex align-items-end my-2">
            <div>
              <button
                onClick={() => {
                  window.history.back();
                }}
                type="reset"
                className="btn btn-sm btn-light btn-active-light-primary"
                data-kt-menu-dismiss="true"
              >
                <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                  <i className="fa fa-chevron-left"></i>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Kembali
                </span>
              </button>
            </div>
          </div>
        </div>
      </div>
      <div
        className="wrapper d-flex flex-column flex-row-fluid pt-0"
        id="kt_wrapper"
      >
        <div
          className="content d-flex flex-column flex-column-fluid pt-0"
          id="kt_content"
        >
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container-xxl">
              <div className="row">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Tambah Kerjasama
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <form>
                        <div className="row">
                          <div className="row mb-3">
                            <div className="col-12 col-md-6 mb-2">
                              <div>
                                <label
                                  htmlFor="nama_mitra"
                                  className="required form-label"
                                >
                                  Nama Mitra
                                </label>
                                <div className="fw-bold">
                                  <strong>{dataMitra.nama_mitra}</strong>
                                </div>
                              </div>
                            </div>
                            <div className="col-12 col-md-6 mb-2">
                              <div>
                                <label
                                  htmlFor="email_mitra"
                                  className="required form-label"
                                >
                                  Email Mitra
                                </label>
                                <div className="fw-bold">
                                  <strong>{dataMitra.email}</strong>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="col-12">
                            <div>
                              <label
                                htmlFor="judul"
                                className="required form-label"
                              >
                                Judul Kerjasama
                              </label>
                              <input
                                type="text"
                                value={kerjasama.judul}
                                onChange={(e) => {
                                  const value = e?.target?.value;
                                  setKerjasama((state) => ({
                                    ...state,
                                    judul: value,
                                  }));
                                  setError((error) => ({
                                    ...error,
                                    judul: null,
                                  }));
                                }}
                                className="form-control form-control-sm"
                                placeholder="Masukan judul kerjasama"
                              />
                            </div>
                            <div className="mb-7 mt-2 text-danger">
                              {error?.judul}
                            </div>
                          </div>
                          <div className="col-12">
                            <div>
                              <label
                                htmlFor="scop"
                                className="required form-label"
                              >
                                Kategori Kerjasama
                              </label>
                              <Select
                                name="status"
                                placeholder="Silahkan Pilih Kategori Kerjasama"
                                className="form-select-sm selectpicker p-0"
                                options={dataKategoriKerjasama.map((k) => ({
                                  value: k.pid,
                                  label: k.pcooperation_categories,
                                }))}
                                value={dataKategoriKerjasama
                                  .map((k) => ({
                                    value: k.pid,
                                    label: k.pcooperation_categories,
                                  }))
                                  .find((k) => k.value == kerjasama.kategori)}
                                onChange={async (o) => {
                                  const dataFormKerjasama =
                                    await fetchDetailKategoriKerjasama(
                                      o?.value,
                                    );
                                  setKerjasama((k) => ({
                                    ...k,
                                    kategori: o?.value,
                                  }));
                                  setDataFormKerjasama(dataFormKerjasama);
                                  setError((error) => ({
                                    ...error,
                                    kategori: null,
                                  }));
                                }}
                              />
                            </div>
                            <div className="mb-7 mt-2 text-danger">
                              {error?.kategori}
                            </div>
                          </div>
                          {dataFormKerjasama.length != 0 &&
                            dataFormKerjasama.map((item, idx) => (
                              <React.Fragment key={idx}>
                                <div className="col-12">
                                  <label
                                    className="form-label required"
                                    htmlFor="title"
                                  >
                                    {item.cooperation_form}
                                  </label>
                                  <input
                                    type="text"
                                    className="form-control form-control-sm"
                                    name={item.cooperation_form}
                                    index={idx}
                                    placeholder={item.cooperation_form}
                                    defaultValue={item?.value}
                                    onChange={(e) => {
                                      const value = e?.target?.value;
                                      dataFormKerjasama[idx].value = value;
                                      setDataFormKerjasama(dataFormKerjasama);

                                      error.dataFormKerjasama =
                                        error.dataFormKerjasama || [];
                                      error.dataFormKerjasama[idx] = null;
                                      setError((error) => ({
                                        ...error,
                                        dataFormKerjasama:
                                          error.dataFormKerjasama,
                                      }));
                                    }}
                                  ></input>
                                </div>
                                <span className="mb-7" style={{ color: "red" }}>
                                  {(error?.dataFormKerjasama || [])[idx]}
                                </span>
                              </React.Fragment>
                            ))}
                          <div className="col-12">
                            <div>
                              <label
                                htmlFor="periode"
                                className="required form-label"
                              >
                                Durasi
                              </label>
                              <div className="input-group input-group-sm mb-3">
                                <input
                                  type="number"
                                  min={1}
                                  value={kerjasama.periode}
                                  onChange={(e) => {
                                    let value = e?.target?.value.replace(
                                      /,/g,
                                      "",
                                    );
                                    setKerjasama((state) => ({
                                      ...state,
                                      periode: value,
                                    }));
                                    setError((error) => ({
                                      ...error,
                                      periode: null,
                                    }));
                                  }}
                                  onKeyDown={(evt) =>
                                    evt.key === "," && evt.preventDefault()
                                  }
                                  className="form-control form-control-sm"
                                  placeholder="Periode Kerjasama"
                                />
                                <span
                                  className="input-group-text"
                                  id="basic-addon2"
                                >
                                  Tahun
                                </span>
                              </div>
                            </div>
                            <div className="mb-7 mt-2 text-danger">
                              {error?.periode}
                            </div>
                          </div>
                          <div className="col-12">
                            <div>
                              <label
                                htmlFor="formFile"
                                className="form-label required"
                              >
                                Lampiran Dokumen Kerjasama
                              </label>
                              <input
                                className="form-control form-control-sm"
                                type="file"
                                accept=".pdf"
                                onChange={(e) => {
                                  const [file] = e?.target?.files;
                                  if (file)
                                    setKerjasama((f) => ({ ...f, file: file }));
                                  else
                                    setKerjasama((f) => ({ ...f, file: null }));
                                  setError((error) => ({
                                    ...error,
                                    file: null,
                                  }));
                                  e.target.value = null;
                                }}
                              />
                              <span className="text-muted font-size-sm">
                                File yang diupload *.pdf dengan ukuran maksimal
                                1 MB
                              </span>
                              <br />
                            </div>
                            <div className="mb-7 mt-2" style={{ color: "red" }}>
                              {error?.file}
                            </div>
                          </div>
                          {kerjasama?.file?.name && (
                            <div className="col-lg-12 mb-7 fv-row">
                              <div className="row">
                                <div className="col-lg-12 mb-7 fv-row">
                                  <span>{kerjasama?.file?.name}</span>
                                  <div
                                    className="btn btn-sm btn-icon btn-light-danger float-end"
                                    title="Hapus file"
                                    onClick={(e) =>
                                      setKerjasama((f) => ({
                                        ...f,
                                        file: null,
                                      }))
                                    }
                                  >
                                    <span className="las la-trash-alt" />
                                  </div>
                                </div>
                              </div>
                            </div>
                          )}
                        </div>
                        <div className="col-12">
                          <label
                            className="form-label required"
                            htmlFor="title"
                          >
                            Tgl. Input
                          </label>
                          <p className="fs-7">{kerjasama.tanggal}</p>
                        </div>
                        <div className="form-group fv-row pt-7 mb-7">
                          <div className="d-flex justify-content-center mb-7">
                            <div className="me-2">
                              <button
                                onClick={(e) => {
                                  e.preventDefault();
                                  simpan(1);
                                }}
                                type="button"
                                className="btn btn-md btn-secondary me-3"
                              >
                                Simpan Draft
                              </button>
                              <button
                                className="btn btn-primary"
                                onClick={(e) => {
                                  e.preventDefault();
                                  simpan(2);
                                }}
                              >
                                <i className="fa fa-paper-plane ms-1"></i>Submit
                              </button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default TambahKerjasama;
