import React, { useState, useRef, useEffect } from "react";

import { useParams, useNavigate } from "react-router-dom";
import axios from "axios";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import Header from "../../../components/Header";
import SideNav from "../../../components/SideNav";
import Footer from "../../../components/Footer";
import { capitalizeFirstLetter } from "../../publikasi/helper";
import SignatureCanvas from "react-signature-canvas";
import Select from "react-select";
import Cookies from "js-cookie";

const AddTtd = (props) => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  let history = useNavigate();
  let canvasRef = "";
  let [cBase64, setBase64] = useState();

  const userId = Cookies.get("user_id");
  const [kategori, setKategori] = useState({ nama: "", jabatan: "" });
  // const canvasRef = useRef(null);

  const { id } = useParams();
  const MySwal = withReactContent(Swal);

  const [selectStatus, setSelectStatus] = useState({
    label: "Tidak Aktif",
    value: "0",
  });
  const [selectOptionStatus, setSelectOptionStatus] = useState([]);
  const [selectedStatusValue, setSelectedStatusValue] = useState();
  const [currentAkademi, setCurrentAkademi] = useState([]);
  const [currentAkademiq, setCurrentAkademiq] = useState({});
  const [error, setError] = useState({
    nama: "",
    jabatan: "",
    status: "",
    signature: "",
  });
  const varAx = useRef();

  const [isEditTtd, setIsEditTtd] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const getAkademi = (id) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/tandatangandigital/API_Get_Tanda_Tangan_Digital",
        {
          id: id,
        },
        {
          Authorization: "Bearer " + Cookies.get("token"),
        },
      )
      .then(function (response) {
        setCurrentAkademi(response.data.result.Data[0]);
        setCurrentAkademiq({
          ...currentAkademiq,
          tanda_tangan: response.data.result.Data[0].tanda_tangan,
        });
        if (response.data.result.Data[0].status == "0") {
          setSelectStatus({ value: "0", label: "Tidak Aktif" });
        } else if (response.data.result.Data[0].status == "1") {
          setSelectStatus({ value: "1", label: "Aktif" });
        }
        // canvasRef.current = supose;
        // contextRef.current = supose;
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const handleDel = () => {
    // let cell = currentAkademi
    MySwal.fire({
      title: "Apakah anda yakin ?",
      text: "Data ini tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, hapus!",
      cancelButtonText: "Tidak",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/tandatangandigital/API_Hapus_Tanda_Tangan_Digital",
            {
              id: id,
              user_id: "1",
            },
            { Authorization: "Bearer " + Cookies.get("token") },
          )
          .then(function (response) {
            console.log("nidw");
            console.log(response);
            if (response.status === 200) {
              if (response.data.result.Status === true) {
                MySwal.fire({
                  title: <strong>Information!</strong>,
                  html: <i>{response.data.result.Message}</i>,
                  icon: "success",
                });
                // setResetPaginationToggle(true);
                // retrieveAkademi();
                // window.location.reload(true);
                // getData();
                history("/partnership/ttd");
              } else {
                MySwal.fire({
                  title: <strong>Information!</strong>,
                  html: <i>{response.data.result.Message}</i>,
                  icon: "info",
                });
                // refreshList();
              }
            } else {
              // getData();
            }
          })
          .catch((error) => {
            // getData();
          });
        // refreshList();
      }
    });
  };

  useEffect(() => {
    varAx.current = "Tidak Aktif";

    getAkademi(id);
    setSelectOptionStatus([
      { value: "1", label: "Aktif" },
      { value: "0", label: "Tidak Aktif" },
    ]);
  }, []);

  const handleChangeIpt = (status_id) => {
    let dataxstatusvalue = status_id;
    setKategori({ ...kategori, ["status"]: dataxstatusvalue });
    setSelectStatus({
      label: dataxstatusvalue.label,
      value: dataxstatusvalue.value,
    });
    setSelectedStatusValue(status_id.value);
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setCurrentAkademi({ ...currentAkademi, [name]: value });
  };

  const saveResult = (e) => {
    let err = { ...error };
    Object.keys(err).forEach((k) => {
      err[k] = "";
    });
    if (currentAkademi.nama == "") {
      err["nama"] = "Tidak boleh kosong";
      // return setError({ 'nama': 'Tidak Boleh Kosong' });
      // e.preventDefault();
    }
    if (currentAkademi.jabatan == "") {
      // return setError({ 'jabatan': 'Tidak Boleh Kosong' });
      // e.preventDefault();
      err["jabatan"] = "Tidak boleh kosong";
    }
    if (selectedStatusValue == "") {
      // return setError({ 'status': 'Tidak Boleh Kosong' });
      err["status"] = "Tidak boleh kosong";
    }
    if (isEditTtd) {
      if (canvasRef.isEmpty()) {
        err["signature"] = "Tidak boleh kosong";
      }
    }
    let isError = false;
    Object.keys(err).forEach((k) => {
      if (err[k] != "") {
        isError = true;
      }
    });
    setError(err);
    // console.log(err)
    if (isError) {
      return MySwal.fire({
        title: <strong>Information!</strong>,
        html: <i>Silahkan Lengkapi Form</i>,
        icon: "info",
      });
    }

    let sumNo = varAx.current;
    if (sumNo === "Tidak Aktif") {
      sumNo = "0";
    } else {
      sumNo = "1";
    }
    if (sumNo === "") {
      return MySwal.fire({
        title: <strong>Information!</strong>,
        html: <i>Silahkan Pilih Status</i>,
        icon: "info",
      });
    }
    // console.log(selectStatus)
    axios.defaults.headers.common["Authorization"] =
      "Bearer " + Cookies.get("token");
    setIsLoading(true);

    const respon = axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/tandatangandigital/API_UbaH_Tanda_Tangan_Digital",
        {
          id: parseInt(id),
          userid: userId,
          Nama: capitalizeFirstLetter(currentAkademi.nama),
          Jabatan: capitalizeFirstLetter(currentAkademi.jabatan),
          Tanda_Tangan: !isEditTtd
            ? currentAkademiq.tanda_tangan
            : canvasRef.toDataURL(),
          Status: selectStatus.value,
        },
        { Authorization: "Bearer " + Cookies.get("token") },
      )
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.result.Status === true) {
            setIsLoading(false);
            MySwal.fire({
              title: <strong>Information!</strong>,
              html: <i>Berhasil Di Tambah</i>,
              icon: "success",
            });
            history("/partnership/ttd");
          } else {
            setIsLoading(false);
            MySwal.fire({
              title: <strong>Information!</strong>,
              html: <i>Gagal Di Tambah</i>,
              icon: "info",
            });
            window.location.reload(true);
          }
        }
      });
  };

  return (
    <div>
      <Header />
      <SideNav />
      <div className="toolbar" id="kt_toolbar">
        <div
          id="kt_toolbar_container"
          className="container-fluid d-flex flex-stack my-2"
        >
          <div className="d-flex align-items-start my-2">
            <div>
              <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                <span className="me-3">
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    className="mh-50px"
                  >
                    <path
                      opacity="0.3"
                      d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z"
                      fill="#FFC700"
                    ></path>
                    <path
                      d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z"
                      fill="#FFC700"
                    ></path>
                  </svg>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Partnership
                  <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                </span>
                Tnada Tangan
              </h1>
            </div>
          </div>
          <div className="d-flex align-items-end my-2">
            <div>
              <button
                onClick={() => history("/partnership/ttd")}
                type="reset"
                className="btn btn-sm btn-light btn-active-light-primary"
                data-kt-menu-dismiss="true"
              >
                <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                  <i className="fa fa-chevron-left"></i>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Kembali
                </span>
              </button>
              <button
                className="btn btn-sm btn-danger btn-active-light-info ms-2"
                onClick={() => {
                  handleDel();
                }}
              >
                <i className="bi bi-trash-fill text-white"></i>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Hapus
                </span>
              </button>
            </div>
          </div>
        </div>
      </div>
      <div
        className="wrapper d-flex flex-column flex-row-fluid pt-0"
        id="kt_wrapper"
      >
        <div
          className="content d-flex flex-column flex-column-fluid pt-0"
          id="kt_content"
        >
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container-xxl">
              <div className="row">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Edit Tanda Tangan
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="submit-form">
                        <div>
                          <div className="col-lg-12 fv-row mb-7">
                            <label
                              className="form-label required"
                              htmlFor="description"
                            >
                              Nama
                            </label>
                            <input
                              type="text"
                              className="form-control form-control-sm"
                              id="nama"
                              required
                              placeholder="Masukan Nama"
                              value={currentAkademi.nama}
                              style={{ textTransform: "capitalize" }}
                              onChange={handleInputChange}
                              name="nama"
                            />
                            <span style={{ color: "red" }}>
                              {error["nama"]}
                            </span>
                          </div>
                          <div className="col-lg-12 fv-row mb-7">
                            <label
                              className="form-label required"
                              htmlFor="description"
                            >
                              Jabatan
                            </label>
                            <input
                              type="text"
                              className="form-control form-control-sm font-size-h4"
                              // id="name"
                              required
                              placeholder="Masukan Jabatan"
                              value={currentAkademi.jabatan}
                              style={{ textTransform: "capitalize" }}
                              onChange={handleInputChange}
                              name="jabatan"
                            />
                            <span style={{ color: "red" }}>
                              {error["jabatan"]}
                            </span>
                          </div>
                          <div className="col-lg-12 fv-row mb-7">
                            <label
                              className="form-label required"
                              htmlFor="description"
                            >
                              Status
                            </label>
                            <Select
                              name="status"
                              placeholder="Silahkan pilih"
                              className="form-select-sm selectpicker p-0"
                              value={selectStatus}
                              defaultValue={selectStatus}
                              isOptionSelected={selectOptionStatus[0]}
                              options={selectOptionStatus}
                              onChange={handleChangeIpt}
                            />
                          </div>
                          <h5 className="mt-10 mb-5">Buat Tanda Tangan</h5>
                          <div className="row ms-2">
                            <div
                              className="col-lg-6"
                              style={{
                                width: "400px",
                                height: "400px",
                                backgroundColor: "whitesmoke",
                                position: "relative",
                                borderRadius: "30px",
                                marginTop: "20px",
                                marginRight: "50px",
                              }}
                            >
                              <button
                                style={{
                                  position: "absolute",
                                  right: 15,
                                  top: 10,
                                }}
                                onClick={() => {
                                  setIsEditTtd(true);
                                }}
                                className="btn btn-sm btn-secondary"
                              >
                                edit
                              </button>
                              <img src={currentAkademiq.tanda_tangan}></img>
                            </div>

                            {/* <canvas style={{'background-color' : 'whitesmoke', 'border-radius': '30px','marginTop' : '20px'}}
                                        onMouseDown={startDrawing}
                                        onMouseUp={finishDrawing}
                                        onMouseMove={draw}
                                        ref={canvasRef}
                                    ></canvas> */}
                            {isEditTtd && (
                              <>
                                {" "}
                                <div
                                  className="col-lg-6"
                                  style={{
                                    width: "400px",
                                    height: "400px",
                                    backgroundColor: "whitesmoke",
                                    position: "relative",
                                    borderRadius: "30px",
                                    marginTop: "20px",
                                  }}
                                >
                                  <div
                                    style={{
                                      position: "absolute",
                                      right: 15,
                                      top: 10,
                                    }}
                                  >
                                    <button
                                      onClick={() => {
                                        canvasRef.clear();
                                        setIsEditTtd(false);
                                      }}
                                      className="btn btn-sm btn-danger me-2"
                                    >
                                      batal edit
                                    </button>
                                    <button
                                      onClick={() => {
                                        canvasRef.clear();
                                      }}
                                      className="btn btn-sm btn-secondary"
                                    >
                                      clear
                                    </button>
                                  </div>

                                  <SignatureCanvas
                                    ref={(ref) => (canvasRef = ref)}
                                    onEnd={(e) => {
                                      // console.log(canvasRef.isEmpty());
                                      setBase64(canvasRef.toDataURL());
                                    }}
                                    canvasProps={{
                                      width: 400,
                                      height: 400,
                                    }}
                                  />
                                </div>
                                <div style={{ color: "red" }}>
                                  {error.signature}
                                </div>
                              </>
                            )}
                          </div>
                        </div>

                        {/* )} */}
                      </div>
                    </div>

                    <div className="form-group fv-row mt-10 pt-10 mb-7">
                      <div className="d-flex justify-content-center mb-7">
                        <button
                          onClick={() => history("/partnership/ttd")}
                          type="reset"
                          className="btn btn-md btn-light me-3"
                          data-kt-menu-dismiss="true"
                        >
                          Batal
                        </button>
                        <button
                          type="submit"
                          className="btn btn-primary btn-md"
                          onClick={(e) => saveResult(e)}
                          disabled={isLoading}
                        >
                          {isLoading ? (
                            <>
                              <span
                                className="spinner-border spinner-border-sm me-2"
                                role="status"
                                aria-hidden="true"
                              ></span>
                              <span className="sr-only">Loading...</span>
                              Loading...
                            </>
                          ) : (
                            <>
                              <i className="fa fa-paper-plane me-1"></i>Simpan
                            </>
                          )}
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default AddTtd;
