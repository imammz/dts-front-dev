import React, { useState, useEffect, useMemo, useRef } from "react";
import AkademiService from "../../../service/AkademiService";
// import Pagination from "@material-ui/lab/Pagination";
// import { useTable, useSortBy } from 'react-table';
// import { GlobalFilter, DefaultFilterForColumn} from "../../Filter";
import Header from "../../../components/Header";
import SideNav from "../../../components/SideNav";
import Footer from "../../../components/Footer";
import Select from "react-select";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import axios from "axios";
import { useHistory, useNavigate, useParams } from "react-router-dom";
// import { useFilters, useGlobalFilter } from 'react-table/dist/react-table.development';
import DataTable from "react-data-table-component";
import Cookies from "js-cookie";
import _ from "lodash";
import { cekPermition, logout } from "../../AksesHelper";
import { capitalizeFirstLetter } from "../../publikasi/helper";

function TtdList() {
  const btnModalFilter = useRef(null);
  const MySwal = withReactContent(Swal);
  const history = (l) => {
    window.location.href = l;
  };

  const [data, setData] = useState([
    {
      id: 96,
      nama_mitra: "Sukseskarir",
      email: "sukseskarir@gmail.com",
      agency_logo:
        "https://s3d.sdmdigital.id:9000/dts-partnership/1656407954_945754bfe4b5507e3f94b0233770871b.png?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=TEST1%2F20220918%2Fus-west-0%2Fs3%2Faws4_request&X-Amz-Date=20220918T044122Z&X-Amz-SignedHeaders=host&X-Amz-Expires=60&X-Amz-Signature=87cbf5fe0b8786aaddbcd8417314b44e1764b71d4673bc00622e9e6292a93ec3",
      website: "www.sukseskarir.com",
      address: "Jl Apel",
      indonesia_provinces_id: 14,
      indonesia_cities_id: 1403,
      postal_code: "16267               ",
      user_id: 156091,
      pic_name: "Anto",
      pic_contact_number: "087717282719",
      pic_email: "darkatnight16@gmail.com",
      status: "Tidak aktif",
      jml_kerjasama: 0,
      visit: 1,
      jml_pelatihan: null,
    },
  ]);

  const [isLoading, setIsLoading] = useState(true);
  const [totalRows, setTotalRows] = useState(0);

  const [filterStatus, setFilterStatus] = useState(null);

  const searchInputRef = useRef();

  const [downloadUrl, setDownloadUrl] = useState("");
  // const [word, setWord] = useState("")

  // const [filter, setFilter] = useState({
  //   tahun: "0",
  //   status: ""
  // })

  const [preFilter, setPreFilter] = useState({
    tahun: "0",
    status: "",
  });

  // const [page, setPage] = useState({ pos: 0, perPage: 10 })
  // const [sort, setSort] = useState({ sortField: "id", sortDirection: "desc" })
  const [queryParam, setQueryParam] = useState({
    word: "",
    pos: 0,
    perPage: 10,
    sortField: "id",
    sortDirection: "desc",
    status: "",
    tahun: "0",
  });

  const [isPaginationReset, setIsPaginationReset] = useState(false);

  const optionstatus = [
    { value: "-1", label: "Semua status" },
    { value: "1", label: "Aktif" },
    { value: "0", label: "Tidak Aktif" },
  ];

  const customLoader = (
    <div style={{ padding: "24px" }}>
      <img src="/assets/media/loader/loader-biru.gif" />
    </div>
  );

  const columns = [
    {
      Header: "No",
      accessor: "created_at",
      sortType: "basic",
      sortable: false,
      name: "No",
      // sortable : true,
      width: "70px",
      grow: 1,
      // cell:(row, index) => index + 1 ,
      cell: (row, index) => {
        return <span>{index + 1 + queryParam.pos * queryParam.perPage}</span>;
      },
      selector: (akademi) => akademi.id,
    },
    {
      Header: "Nama",
      accessor: "",
      className: "min-w-300px mw-300px",
      sortType: "basic",
      name: "Nama",
      grow: 6,
      initSort: "nama",
      sortable: true,
      sortField: "nama",
      cell: (akademi) => {
        return (
          <div>
            <label className="d-flex flex-stack my-2 cursor-pointer">
              <span className="d-flex align-items-center me-2">
                <span className="d-flex flex-column">
                  <a
                    href={"/partnership/ttd/edit/" + akademi.id}
                    className="text-dark mb-0"
                  >
                    <span className="fw-bolder fs-7">{akademi.nama}</span>
                  </a>
                </span>
              </span>
            </label>
          </div>
        );
      },
      selector: (akademi) => akademi.nama,
    },
    {
      Header: "Jabatan",
      accessor: "slug",
      className: "min-w-300px mw-300px",
      sortType: "basic",
      name: "Jabatan",
      grow: 6,
      sortable: true,
      initSort: "jabatan",
      sortField: "jabatan",
      cell: (akademi) => {
        return (
          <div>
            <span>{akademi.jabatan}</span>
          </div>
        );
      },
      selector: (akademi) => akademi.jabatan,
    },
    {
      Header: "Status",
      Title: "status",
      accessor: "status",
      className: "min-w-200px mw-200px",
      name: "Status",
      sortable: true,
      initSort: "status",
      grow: 2,
      sortField: "status",
      cell: (akademi) => {
        return (
          <span
            className={
              "badge badge-light-" +
              (akademi.status == "Aktif"
                ? "success"
                : akademi.status == "Tidak Aktif"
                  ? "danger"
                  : "warning") +
              " fs-7 m-1"
            }
          >
            {akademi.status == "Aktif"
              ? "Aktif"
              : akademi.status == "Tidak Aktif"
                ? "Tidak Aktif"
                : "Tidak Aktif"}
          </span>
        );
      },
      selector: (akademi) => akademi.status,
    },
    {
      Header: "Aksi",
      accessor: "actions",
      name: "Aksi",
      grow: 3,
      center: true,
      selector: (akademi) => {
        return (
          <div>
            <a
              href={"/partnership/ttd/edit/" + akademi.id}
              className="btn btn-icon btn-bg-warning btn-sm me-1"
              title="Edit"
              alt="Edit"
            >
              <i className="bi bi-gear-fill text-white"></i>
            </a>

            <button
              onClick={() => handleDel(akademi)}
              className="btn btn-icon btn-bg-danger btn-sm me-1"
              title="Edit"
              alt="Edit"
            >
              <i className="fas fa-trash text-white"></i>
            </button>
          </div>
        );
      },
    },
  ];

  const customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  useEffect(() => {
    if (cekPermition().view !== 1) {
      Swal.fire({
        title: "Akses Tidak Diizinkan",
        html: "<i> Anda akan kembali kehalaman login </i>",
        icon: "warning",
      }).then(() => {
        logout();
      });
    }
  }, []);

  useEffect(() => {
    if (filterStatus == null) {
      btnModalFilter?.current?.click();
    }
  }, [filterStatus]);

  const handleShow = (cell) => {
    history("/partnership/ttd/edit/" + cell.id);
  };

  const handleDel = (cell) => {
    MySwal.fire({
      title: "Apakah anda yakin ?",
      text: "Data ini tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, hapus!",
      cancelButtonText: "Tidak",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/tandatangandigital/API_Hapus_Tanda_Tangan_Digital",
            {
              id: cell.id,
              user_id: "1",
            },
            { Authorization: "Bearer " + Cookies.get("token") },
          )
          .then(function (response) {
            console.log("nidw");
            console.log(response);
            if (response.status === 200) {
              if (response.data.result.Status === true) {
                MySwal.fire({
                  title: <strong>Information!</strong>,
                  html: <i>{response.data.result.Message}</i>,
                  icon: "success",
                });
                // setResetPaginationToggle(true);
                // retrieveAkademi();
                // window.location.reload(true);
                getData();
              } else {
                MySwal.fire({
                  title: <strong>Information!</strong>,
                  html: <i>{response.data.result.Message}</i>,
                  icon: "info",
                });
                // refreshList();
              }
            } else {
              getData();
            }
          })
          .catch((error) => {
            getData();
          });
        // refreshList();
      }
    });
  };

  const getData = async () => {
    // console.log(sort)
    setTotalRows(0);
    setData([]);
    setIsLoading(true);
    try {
      const params = {
        start: queryParam.pos * queryParam.perPage,
        rows: queryParam.perPage,
        sort_by: queryParam.sortField,
        sort_type: queryParam.sortDirection,
        search: queryParam.word,
        status: queryParam.status || "-1",
      };
      // console.log(params)
      let formData = new FormData();
      Object.keys(params).forEach((k) => {
        formData.append(k, params[k]);
      });
      const resp = await axios.post(
        process.env.REACT_APP_BASE_API_URI +
          "/tandatangandigital/API_Daftar_Tanda_Tangan_Digital",
        formData,
        {
          headers: {
            Authorization: "Bearer " + Cookies.get("token"),
          },
        },
      );
      // console.log(resp)
      if (resp.data.result.Status) {
        setData(resp.data.result.Data);
        setTotalRows(resp.data.result.TotalLength[0].jml);
      }
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  const onSortTable = (column, sortDirection) => {
    // console.log(column, sortDirection)
    if (column.sortable) {
      setQueryParam((q) => ({
        ...q,
        sortField: column.sortField,
        sortDirection: sortDirection,
      }));
    }
  };

  const handlePageChange = (page) => {
    // fetchUsers(page);
    console.log(page);
    setQueryParam((q) => ({ ...q, pos: page - 1 }));
  };

  const handlePerRowsChange = async (newPerPage, page) => {
    // setLoading(true);
    setQueryParam((q) => ({ ...q, pos: page - 1, perPage: newPerPage }));
    // setPage({ pos: page - 1, perPage: newPerPage })
  };

  const handleSearch = () => {
    // console.log(searchInputRef.current.value)
    // setPage(c => ({ ...c, pos: 0 }))
    // setWord(searchInputRef.current.value);
    setIsPaginationReset(!isPaginationReset);
    setQueryParam((q) => ({
      ...q,
      word: searchInputRef.current.value,
      pos: 0,
    }));
  };
  // useEffect(() => {
  //   getData()
  // }, [])

  const debouceSearch = useMemo((e) => {
    console.log(e);
    return _.debounce(() => {
      handleSearch(e);
    }, 300);
  }, []);

  useEffect(
    _.debounce(() => {
      getData();
    }, 1),
    [queryParam],
  );

  return (
    <div>
      <SideNav />
      <Header />
      <div>
        <input
          type="hidden"
          name="csrf-token"
          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
        />
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z"
                        fill="#FFC700"
                      ></path>
                      <path
                        d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z"
                        fill="#FFC700"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Partnership
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Tanda Tangan
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                  data-bs-toggle="modal"
                  data-bs-target="#filter"
                >
                  <i className="bi bi-sliders"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Filter
                  </span>
                </button>

                <a
                  href="/partnership/ttd/add"
                  className="btn btn-success fw-bolder btn-sm"
                >
                  <i className="bi bi-plus-circle"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Tambah Tanda Tagan
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Daftar Tanda Tangan
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <div className="d-flex align-items-center position-relative my-1 me-2">
                            <span className="svg-icon svg-icon-1 position-absolute ms-6">
                              <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className="mh-50px"
                              >
                                <rect
                                  opacity="0.5"
                                  x="17.0365"
                                  y="15.1223"
                                  width="8.15546"
                                  height="2"
                                  rx="1"
                                  transform="rotate(45 17.0365 15.1223)"
                                  fill="currentColor"
                                ></rect>
                                <path
                                  d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                  fill="currentColor"
                                ></path>
                              </svg>
                            </span>
                            <input
                              ref={searchInputRef}
                              type="text"
                              data-kt-user-table-filter="search"
                              className="form-control form-control-sm form-control-solid w-250px ps-14"
                              placeholder="Cari Tanda Tangan"
                              onChange={_.debounce(() => {
                                // setPage(c => ({ ...c, pos: 0 }))
                                // setIsPaginationReset(!isPaginationReset)
                                handleSearch();
                              }, 700)}
                            />
                          </div>
                          <div className="modal fade" tabindex="-1" id="filter">
                            <div className="modal-dialog modal-lg">
                              <div className="modal-content">
                                <div className="modal-header">
                                  <h5 className="modal-title">
                                    <span className="svg-icon svg-icon-5 me-1">
                                      <i className="bi bi-sliders text-black"></i>
                                    </span>
                                    Filter Tanda Tangan
                                  </h5>
                                  <div
                                    className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                    data-bs-dismiss="modal"
                                    aria-label="Close"
                                  >
                                    <span className="svg-icon svg-icon-2x">
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                      >
                                        <rect
                                          opacity="0.5"
                                          x="6"
                                          y="17.3137"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(-45 6 17.3137)"
                                          fill="currentColor"
                                        />
                                        <rect
                                          x="7.41422"
                                          y="6"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(45 7.41422 6)"
                                          fill="currentColor"
                                        />
                                      </svg>
                                    </span>
                                  </div>
                                </div>
                                <form action="#">
                                  <div className="modal-body">
                                    <div className="row">
                                      <div className="col-lg-12 mb-7 fv-row">
                                        <label className="form-label">
                                          Status
                                        </label>
                                        <Select
                                          name="status"
                                          placeholder="Silahkan pilih"
                                          // noOptionsMessage={({ inputValue }) => !inputValue ? this.state.optionstatus : "Data tidak tersedia"}
                                          className="form-select-sm selectpicker p-0"
                                          options={optionstatus}
                                          value={filterStatus}
                                          onChange={(v) => {
                                            setFilterStatus(v);
                                            setPreFilter((pr) => ({
                                              ...pr,
                                              status: v.value,
                                            }));
                                          }}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                  <div className="modal-footer">
                                    <div className="d-flex justify-content-between">
                                      <button
                                        type="button"
                                        className="btn btn-sm btn-light me-3"
                                        onClick={() => {
                                          if (filterStatus != null) {
                                            // setIsPaginationReset(!isPaginationReset)
                                            // setPage(p => ({ ...p, pos: 0 }))
                                            setFilterStatus(null);
                                            setPreFilter({
                                              status: "",
                                              tahun: "0",
                                            });
                                          }
                                        }}
                                      >
                                        Reset
                                      </button>
                                      <button
                                        ref={btnModalFilter}
                                        type="button"
                                        className="btn btn-sm btn-primary"
                                        data-bs-dismiss="modal"
                                        onClick={() => {
                                          // setPage({ pos: 0, perPage: 10 })
                                          // setPage(p => ({ ...p, pos: 0 }))
                                          setQueryParam((q) => ({
                                            ...q,
                                            ...preFilter,
                                          }));
                                          setIsPaginationReset(
                                            !isPaginationReset,
                                          );
                                          // setPreFilter({ status: "", tahun: "0" })
                                        }}
                                      >
                                        Apply Filter
                                      </button>
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="table-responsive">
                          <DataTable
                            columns={columns}
                            data={data}
                            progressPending={isLoading}
                            progressComponent={customLoader}
                            highlightOnHover
                            pointerOnHover
                            pagination
                            paginationServer
                            // paginationTotalRows={this.state.totalRows}
                            // onChangeRowsPerPage={this.handlePerRowsChange}
                            // onChangePage={handlePageChange}
                            paginationPerPage={queryParam.perPage}
                            paginationDefaultPage={1}
                            paginationResetDefaultPage={isPaginationReset}
                            paginationTotalRows={totalRows}
                            paginationComponentOptions={{
                              selectAllRowsItem: true,
                              selectAllRowsItemText: "Semua",
                            }}
                            onChangeRowsPerPage={handlePerRowsChange}
                            onChangePage={handlePageChange}
                            customStyles={customStyles}
                            persistTableHead={true}
                            noDataComponent={
                              <div className="mt-5">Tidak Ada Data</div>
                            }
                            onSort={onSortTable}
                            sortServer
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    </div>
  );
}

export default TtdList;
