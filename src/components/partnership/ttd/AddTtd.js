import React, { useState, useRef, useEffect } from "react";

import { useParams, useNavigate } from "react-router-dom";
import axios from "axios";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import Header from "../../../components/Header";
import SideNav from "../../../components/SideNav";
import Footer from "../../../components/Footer";
import Select from "react-select";
import {
  indonesianDateFormat,
  capitalizeFirstLetter,
  getDateTime,
} from "../../publikasi/helper";
import SignatureCanvas from "react-signature-canvas";
import Cookies from "js-cookie";
// import Kategori from '../../publikasi/kategori/ctrl/Kategori-View';

const AddTtd = (props) => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  let history = useNavigate();
  let canvasRef = "";
  let [cBase64, setBase64] = useState();

  const userId = Cookies.get("user_id");

  const [submitted, setSubmitted] = useState(false);
  const MySwal = withReactContent(Swal);
  const [kategori, setKategori] = useState({
    nama: "",
    jabatan: "",
    status: "",
  });
  // const canvasRef = useRef(null);
  const [error, setError] = useState({
    nama: "",
    jabatan: "",
    status: "",
    signature: "",
  });

  const varAx = useRef();
  const [selectStatus, setSelectStatus] = useState(null);
  const [selectOptionStatus, setSelectOptionStatus] = useState([]);
  const [selectedStatusValue, setSelectedStatusValue] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    varAx.current = "Tidak Aktif";
    setSelectOptionStatus([
      { value: "1", label: "Aktif" },
      { value: "0", label: "Tidak Aktif" },
    ]);
  }, []);

  const handleChangeIpt = (status_id) => {
    let dataxstatusvalue = status_id;
    setKategori({ ...kategori, ["status"]: dataxstatusvalue });
    setSelectStatus({
      label: dataxstatusvalue.label,
      value: dataxstatusvalue.value,
    });
    setSelectedStatusValue(status_id.value);

    // console.log(status_id)
    let err = { ...error };
    if (status_id.value == "") {
      err["status"] = "Tidak boleh kosong";
    } else {
      err["status"] = "";
    }
    setError({ ...err });
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setKategori({ ...kategori, [name]: value });
    // console.log(name, value)
    let err = { ...error };
    if (value == "" || value == null) {
      err[name] = "Tidak boleh kosong";
    } else {
      err[name] = "";
    }
    setError({ ...err });
    // console.log(error)
  };

  const newAkademi = () => {
    // setAkademi(initialAkademiState);
    setSubmitted(false);
  };

  const saveResult = (e) => {
    // console.log(canvasRef.isEmpty())
    let isError = false;
    let err = { ...error };
    Object.keys(kategori).forEach((k) => {
      if (kategori[k] == "") {
        err[k] = "Tidak boleh kosong";
        isError = true;
      }
    });
    if (canvasRef.isEmpty()) {
      err["signature"] = "Tidak boleh kosong";
    }
    // console.log(err)
    setError({ ...err });

    if (isError) {
      return MySwal.fire({
        title: <strong>Information!</strong>,
        html: <i>Periksa kembali isian anda.</i>,
        icon: "info",
      });
    }

    Object.keys(error).forEach((k) => {
      if (error[k] != "") {
        isError = true;
      }
    });

    if (isError) {
      return MySwal.fire({
        title: <strong>Information!</strong>,
        html: <i>Silahkan Pilih Status</i>,
        icon: "info",
      });
    }

    let sumNo = varAx.current;
    if (sumNo === "") {
      return MySwal.fire({
        title: <strong>Information!</strong>,
        html: <i>Silahkan Pilih Status</i>,
        icon: "info",
      });
    }
    if (sumNo === "Tidak Aktif") {
      sumNo = "0";
    } else {
      sumNo = "1";
    }
    console.log(Cookies.get("token"));
    setIsLoading(true);
    // console.log(canvasRef.toDataURL());
    // return
    // axios.defaults.headers.common['Authorization'] = 'Bearer ' + Cookies.get("token");
    const respon = axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/tandatangandigital/API_Insert_Tanda_Tangan_Digital",
        {
          userid: userId,
          Nama: capitalizeFirstLetter(document.querySelector("#nama").value),
          Jabatan: capitalizeFirstLetter(
            document.querySelector("#jabatan").value,
          ),
          Tanda_Tangan: canvasRef.toDataURL(),
          Status: selectedStatusValue,
        },
        { headers: { Authorization: "Bearer " + Cookies.get("token") } },
      )
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.result.Status === true) {
            setIsLoading(false);
            MySwal.fire({
              title: <strong>Information!</strong>,
              html: <i>Berhasil Di Tambah</i>,
              icon: "success",
            });
            history("/partnership/ttd");
          } else {
            setIsLoading(false);
            MySwal.fire({
              title: <strong>Information!</strong>,
              html: <i>Gagal Di Tambah</i>,
              icon: "info",
            });
            window.location.reload(true);
          }
        }
      });
  };

  return (
    <div>
      <Header />
      <SideNav />
      <div className="toolbar" id="kt_toolbar">
        <div
          id="kt_toolbar_container"
          className="container-fluid d-flex flex-stack my-2"
        >
          <div className="d-flex align-items-start my-2">
            <div>
              <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                <span className="me-3">
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    className="mh-50px"
                  >
                    <path
                      opacity="0.3"
                      d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z"
                      fill="#FFC700"
                    ></path>
                    <path
                      d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z"
                      fill="#FFC700"
                    ></path>
                  </svg>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Partnership
                  <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                </span>
                Tnada Tangan
              </h1>
            </div>
          </div>
          <div className="d-flex align-items-end my-2">
            <div>
              <button
                onClick={() => history("/partnership/ttd")}
                type="reset"
                className="btn btn-sm btn-light btn-active-light-primary"
                data-kt-menu-dismiss="true"
              >
                <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                  <i className="fa fa-chevron-left"></i>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Kembali
                </span>
              </button>
            </div>
          </div>
        </div>
      </div>
      <div
        className="wrapper d-flex flex-column flex-row-fluid pt-0"
        id="kt_wrapper"
      >
        <div
          className="content d-flex flex-column flex-column-fluid pt-0"
          id="kt_content"
        >
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container-xxl">
              <div className="row">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Tambah Tanda Tangan
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="submit-form">
                        {submitted ? (
                          <div>
                            <h4>You submitted successfully!</h4>
                            <button
                              className="btn btn-success"
                              onClick={newAkademi}
                            >
                              Add
                            </button>
                          </div>
                        ) : (
                          <div>
                            <div className="col-lg-12 fv-row mb-7">
                              <label
                                className="form-label required"
                                htmlFor="description"
                              >
                                Nama
                              </label>
                              <input
                                type="text"
                                className="form-control form-control-sm"
                                id="nama"
                                required
                                placeholder="Masukan Nama"
                                style={{ textTransform: "capitalize" }}
                                onChange={handleInputChange}
                                name="nama"
                              />
                              <span style={{ color: "red" }}>
                                {error["nama"]}
                              </span>
                            </div>
                            <div className="col-lg-12 fv-row mb-7">
                              <label
                                className="form-label required"
                                htmlFor="description"
                              >
                                Jabatan
                              </label>
                              <input
                                type="text"
                                className="form-control form-control-sm"
                                id="jabatan"
                                required
                                placeholder="Masukan Jabatan"
                                style={{ textTransform: "capitalize" }}
                                onChange={handleInputChange}
                                name="jabatan"
                              />
                              <span style={{ color: "red" }}>
                                {error["jabatan"]}
                              </span>
                            </div>
                            <div className="col-lg-12 fv-row mb-2">
                              <label
                                className="form-label required"
                                htmlFor="description"
                              >
                                Status
                              </label>
                              <Select
                                name="status"
                                placeholder="Silahkan pilih"
                                className="form-select-sm selectpicker p-0"
                                value={selectStatus}
                                defaultValue={selectStatus}
                                isOptionSelected={selectOptionStatus[0]}
                                options={selectOptionStatus}
                                onChange={handleChangeIpt}
                              />
                            </div>
                            <div style={{ color: "red" }} className="mb-5">
                              {error.status}
                            </div>
                            <h5 className="mt-10 mb-5">Buat Tanda Tangan</h5>

                            <div
                              style={{
                                width: "400px",
                                height: "400px",
                                backgroundColor: "whitesmoke",
                                position: "relative",
                                borderRadius: "30px",
                                marginTop: "20px",
                              }}
                            >
                              <button
                                className="btn btn-sm btn-secondary"
                                style={{
                                  position: "absolute",
                                  right: 15,
                                  top: 8,
                                }}
                                onClick={() => {
                                  canvasRef.clear();
                                }}
                              >
                                clear
                              </button>
                              <SignatureCanvas
                                ref={(ref) => (canvasRef = ref)}
                                onEnd={(e) => {
                                  setError({ ...error, signature: "" });
                                  // console.log(canvasRef.isEmpty());
                                  setBase64(canvasRef.toDataURL());
                                }}
                                canvasProps={{
                                  width: 400,
                                  height: 400,
                                }}
                              />
                            </div>
                            <div style={{ color: "red" }}>
                              {error.signature}
                            </div>
                          </div>
                        )}
                      </div>
                      <div className="form-group fv-row mt-10 pt-7 mb-7">
                        <div className="d-flex justify-content-center mb-7">
                          <button
                            onClick={() => history("/partnership/ttd")}
                            type="reset"
                            className="btn btn-md btn-light me-3"
                            data-kt-menu-dismiss="true"
                          >
                            Batal
                          </button>
                          <button
                            type="submit"
                            className="btn btn-primary btn-md"
                            onClick={(e) => saveResult(e)}
                            disabled={isLoading}
                          >
                            {isLoading ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm me-2"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Loading...</span>
                                Loading...
                              </>
                            ) : (
                              <>
                                <i className="fa fa-paper-plane me-1"></i>Simpan
                              </>
                            )}
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </div>
  );
};

export default AddTtd;
