import React from "react";
// import { useTable, useSortBy, usePagination } from 'react-table';
import DataTable from "react-data-table-component";
import Select from "react-select";
import Swal from "sweetalert2";
import Footer from "../../../components/Footer";
import Header from "../../../components/Header";
import SideNav from "../../../components/SideNav";
import { cekPermition, logout } from "../../AksesHelper";
import { capitalizeFirstLetter } from "../../publikasi/helper";
import { deleteMasterKerjasama, fetchMasterKerjasama } from "./actions";
import _ from "lodash";

class Content extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentRowsPerPage: 10,
      currentPage: 1,
      sortField: "id",
      sortDir: "desc",

      statusList: [
        {
          label: "Aktif",
          value: 1,
        },
        {
          label: "Tidak Aktif",
          value: 0,
        },
      ],
      selectedStatus: -1,
      searchKeyword: "",

      total: 0,
      data: [],
      loading: false,
    };
  }

  fetch = async () => {
    this.setState({ loading: true });

    Swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      },
    });

    try {
      const { total, data } = await fetchMasterKerjasama({
        start: (this.state.currentPage - 1) * this.state.currentRowsPerPage,
        length: this.state.currentRowsPerPage,
        param: this.state.searchKeyword,
        status: this.state.selectedStatus,
        sort: this.state.sortField,
        sort_val: this.state.sortDir,
      });

      this.setState({ total, data });

      Swal.close();
    } catch (err) {
      await Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }

    this.setState({ loading: false });
  };

  componentDidMount() {
    this.fetch();

    if (cekPermition().view !== 1) {
      Swal.fire({
        title: "Akses Tidak Diizinkan",
        html: "<i> Anda akan kembali kehalaman login </i>",
        icon: "warning",
      }).then(() => {
        logout();
      });
    }
  }

  componentDidUpdate() {
    // this.loadData();
  }

  onChangeRowsPerPage = (currentRowsPerPage, currentPage) => {
    this.setState({ currentRowsPerPage, currentPage }, () => this.fetch());
  };

  onChangePage = (page, totalRows) => {
    this.setState({ currentPage: page }, () => this.fetch());
  };

  onSort = ({ sortField }, sortDir) => {
    this.setState({ sortField, sortDir }, () => this.fetch());
  };

  onRemove = async (id) => {
    try {
      const { isConfirmed = false } = await Swal.fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      });

      if (isConfirmed) {
        Swal.fire({
          title: "Mohon Tunggu!",
          icon: "info",
          allowOutsideClick: false,
          didOpen: () => Swal.showLoading(),
        });

        const message = await deleteMasterKerjasama({ id });

        Swal.close();

        await Swal.fire({
          title: message || "MasterKerjasama berhasil dihapus",
          icon: "success",
          confirmButtonText: "Ok",
        });

        this.fetch();
      }

      // throw new Error("API delete belum tersedia");
    } catch (err) {
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  onFilterReset = () => {
    this.setState({ selectedStatus: -1 }, () => this.fetch());
  };

  onFilterFilter = () => {
    this.setState({ currentPage: 1 }, () => this.fetch());
  };

  onFilterSearch = (searchKeyword) => {
    this.setState({ searchKeyword }, () => this.fetch());
  };

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z"
                        fill="#FFC700"
                      ></path>
                      <path
                        d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z"
                        fill="#FFC700"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Partnership
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Master Kerjasama
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                  data-bs-toggle="modal"
                  data-bs-target="#filter"
                >
                  <i className="bi bi-sliders"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Filter
                  </span>
                </button>

                <a
                  href="/partnership/masterkerjasama/add"
                  className="btn btn-success fw-bolder btn-sm"
                >
                  <i className="bi bi-plus-circle"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Tambah Master Kategori
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Daftar Master Kategori Kerjasama
                          </h1>
                        </div>

                        <div className="card-toolbar">
                          <div className="d-flex align-items-center position-relative my-1 me-2">
                            <span className="svg-icon svg-icon-1 position-absolute ms-6">
                              <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className="mh-50px"
                              >
                                <rect
                                  opacity="0.5"
                                  x="17.0365"
                                  y="15.1223"
                                  width="8.15546"
                                  height="2"
                                  rx="1"
                                  transform="rotate(45 17.0365 15.1223)"
                                  fill="currentColor"
                                ></rect>
                                <path
                                  d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                  fill="currentColor"
                                ></path>
                              </svg>
                            </span>
                            <input
                              type="text"
                              data-kt-user-table-filter="search"
                              className="form-control form-control-sm form-control-solid w-250px ps-14"
                              placeholder="Cari Master Kategori"
                              onKeyPress={(e) => {
                                if (e.key == "Enter")
                                  this.onFilterSearch(e.target.value);
                              }}
                              onChange={(e) => {
                                if (!e.target.value) this.onFilterSearch("");
                              }}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <DataTable
                          columns={[
                            {
                              name: "No",
                              width: "70px",
                              center: true,
                              sortable: false,
                              cell: (row, index) => {
                                return (
                                  <span>
                                    {(this.state.currentPage - 1) *
                                      this.state.currentRowsPerPage +
                                      index +
                                      1}
                                  </span>
                                );
                              },
                            },
                            {
                              accessor: "",
                              className: "min-w-300px mw-300px",
                              // sortType: 'basic',
                              allowOverflow: true,
                              name: "Master Kategori Kerjasama",
                              sortField: "cooperation_categories",
                              sortable: true,
                              grow: 6,
                              cell: (row) => {
                                return (
                                  <label className="d-flex flex-stack my-2 cursor-pointer">
                                    <span className="d-flex align-items-center me-2">
                                      <span className="d-flex flex-column">
                                        <a
                                          href={
                                            "/partnership/masterkerjasama/edit/" +
                                            row.id
                                          }
                                          className="text-dark mb-0"
                                        >
                                          <span className="fw-bolder fs-7">
                                            {capitalizeFirstLetter(
                                              row.cooperation_categories.toLowerCase(),
                                            )}
                                          </span>
                                        </a>
                                      </span>
                                    </span>
                                  </label>
                                );
                              },
                              style: {
                                textTransform: "capitalize",
                              },
                            },
                            {
                              allowOverflow: true,
                              className: "min-w-200px mw-200px",
                              name: "Status",
                              sortField: "status",
                              sortable: true,
                              sortType: "basic",
                              grow: 2,
                              cell: (row) => {
                                return (
                                  <span
                                    className={
                                      "badge badge-light-" +
                                      (row.status?.toLowerCase() == "aktif" ||
                                      row.status == "1"
                                        ? "success"
                                        : row.status?.toLowerCase() ==
                                              "tidak aktif" || row.status == "0"
                                          ? "danger"
                                          : "warning") +
                                      " fs-7 m-1"
                                    }
                                  >
                                    {row.status?.toLowerCase() == "aktif" ||
                                    row.status == 1
                                      ? "Aktif"
                                      : row.status?.toLowerCase() ==
                                            "tidak aktif" || row.status == "0"
                                        ? "Tidak Aktif"
                                        : "Tidak Aktif"}
                                  </span>
                                );
                              },
                            },
                            {
                              name: "Aksi",
                              accessor: "actions",
                              grow: 3,
                              center: true,
                              allowOverflow: true,
                              sortable: false,
                              selector: (row) => {
                                return (
                                  <div>
                                    {/* <a
                                    href={"/partnership/masterkerjasama/preview/" + row.pid}
                                    className="btn btn-icon btn-bg-primary btn-sm me-1"
                                    title="Lihat"
                                    alt="Lihat"
                                  >
                                    <i className="bi bi-eye-fill text-white"></i>
                                  </a> */}

                                    <a
                                      href={
                                        "/partnership/masterkerjasama/edit/" +
                                        row.id
                                      }
                                      className="btn btn-icon btn-bg-warning btn-sm me-1"
                                      title="Edit"
                                      alt="Edit"
                                    >
                                      <i className="bi bi-gear-fill text-white"></i>
                                    </a>

                                    <button
                                      className={`btn btn-icon btn-bg-danger btn-sm me-1`}
                                      title="Hapus"
                                      onClick={() => this.onRemove(row?.id)}
                                      alt="Hapus"
                                    >
                                      <i className="bi bi-trash-fill text-white"></i>
                                    </button>
                                  </div>
                                );
                              },
                            },
                          ]}
                          data={this.state.data}
                          progressPending={this.state.loading}
                          progressComponent={
                            <div style={{ padding: "24px" }}>
                              <img src="/assets/media/loader/loader-biru.gif" />
                            </div>
                          }
                          highlightOnHover
                          pointerOnHover
                          pagination
                          paginationServer={true}
                          paginationTotalRows={this.state.total}
                          paginationComponentOptions={{
                            selectAllRowsItem: true,
                            selectAllRowsItemText: "Semua",
                          }}
                          onChangeRowsPerPage={this.onChangeRowsPerPage}
                          onChangePage={this.onChangePage}
                          customStyles={{
                            headCells: {
                              style: {
                                background: "rgb(243, 246, 249)",
                                fontWeight: "bold",
                              },
                            },
                          }}
                          persistTableHead={true}
                          noDataComponent={
                            <div className="mt-5">Tidak Ada Data</div>
                          }
                          onSort={this.onSort}
                          sortServer
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {this.renderModalFilter()}
      </div>
    );
  }

  renderModalFilter() {
    return (
      <div className="modal fade" tabindex="-1" id="filter">
        <div className="modal-dialog modal-md">
          <div className="modal-content">
            <div className="modal-header py-3">
              <h5 className="modal-title">
                <span className="svg-icon svg-icon-5 me-1">
                  <i className="bi bi-sliders text-black"></i>
                </span>
                Filter Master Kerjasama
              </h5>
              <div
                className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                data-bs-dismiss="modal"
                aria-label="Close"
              >
                <span className="svg-icon svg-icon-2x">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <rect
                      opacity="0.5"
                      x="6"
                      y="17.3137"
                      width="16"
                      height="2"
                      rx="1"
                      transform="rotate(-45 6 17.3137)"
                      fill="currentColor"
                    />
                    <rect
                      x="7.41422"
                      y="6"
                      width="16"
                      height="2"
                      rx="1"
                      transform="rotate(45 7.41422 6)"
                      fill="currentColor"
                    />
                  </svg>
                </span>
              </div>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-lg-12 mb-7 fv-row">
                  <label className="form-label">Status</label>
                  <Select
                    name="level_pelatihan"
                    placeholder="Silahkan pilih"
                    value={this.state.statusList.find(
                      ({ value }) => value == this.state.selectedStatus,
                    )}
                    onChange={({ value }) =>
                      this.setState({ selectedStatus: value })
                    }
                    className="form-select-sm selectpicker p-0"
                    options={this.state.statusList}
                  />
                </div>
              </div>
            </div>
            <div className="modal-footer py-3">
              <div className="d-flex justify-content-between">
                <button
                  type="button"
                  className="btn btn-sm btn-light me-3"
                  onClick={() => this.onFilterReset()}
                >
                  Reset
                </button>
                <button
                  type="button"
                  className="btn btn-sm btn-primary"
                  onClick={(e) => this.onFilterFilter(e)}
                >
                  Apply Filter
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

class MasterKerjasamaList extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Header />
        <SideNav />
        <Content />
        <Footer />
      </div>
    );
  }
}

export default MasterKerjasamaList;
