import React from "react";
import Select from "react-select";
import Swal from "sweetalert2";
import Footer from "../../../components/Footer";
import Header from "../../../components/Header";
import SideNav from "../../../components/SideNav";
import { withRouter } from "../../RouterUtil";
import {
  fetchMasterKerjasamaById,
  hasError,
  insert,
  resetError,
  update,
  validate,
} from "./actions";

class Content extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      statusList: [
        {
          label: "Aktif",
          value: 1,
        },
        {
          label: "Tidak Aktif",
          value: 0,
        },
      ],

      data: {
        name: null,
        fields: [{}],
        status: null,
      },
      isloading: false,
      loading: false,
    };
  }

  fetch = async () => {
    const { id } = this.props.params || {};

    try {
      this.setState({ loading: true });
      const data = await fetchMasterKerjasamaById({ id });
      this.setState({ data, loading: false });
    } catch (err) {}
  };

  validateAndSave = () => {
    const errors = validate(this.state?.data);
    if (!hasError(errors)) {
      this.save();
    } else {
      this.setState({ ...resetError(), ...errors });

      Swal.fire({
        title: "Masih terdapat isian yang belum lengkap",
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  save = async () => {
    const { id } = this.props.params || {};

    try {
      this.setState({ isLoading: true });
      Swal.fire({
        title: "Mohon Tunggu!",
        icon: "info",
        allowOutsideClick: false,
        didOpen: () => Swal.showLoading(),
      });

      var payload = {
        categoryOptItems: this.state.data?.fields?.map((field, idx) => ({
          idcat: id,
          judul: this.state.data?.name,
          idform: field?.id,
          status: this.state.data?.status,
          show: "1",
          formname: field?.name,
          cek: "1",
        })),
      };

      const message = await update(payload);
      this.setState({ isLoading: false });
      Swal.close();

      await Swal.fire({
        title: message || "Master Kerjasama berhasil disimpan",
        icon: "success",
        confirmButtonText: "Ok",
      });

      window.location.href = "/partnership/masterkerjasama";
    } catch (err) {
      this.setState({ isLoading: false });
      Swal.fire({
        title: err,
        icon: "warning",
        confirmButtonText: "Ok",
      });
    }
  };

  componentDidMount() {
    this.fetch();
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z"
                        fill="#FFC700"
                      ></path>
                      <path
                        d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z"
                        fill="#FFC700"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Partnership
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Master Kerjasama
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <a
                  href="/partnership/masterkerjasama"
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500">
                    <i className="fa fa-chevron-left pe-1"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </a>
                <button className="btn btn-sm btn-danger btn-active-light-info ms-2">
                  <i className="bi bi-trash-fill text-white"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Edit Master Kategori Kerjasama
                          </h1>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">
                            Judul Master Kategori Kerjasama
                          </label>
                          <input
                            className="form-control form-control-sm font-size-h4"
                            onChange={(e) => {
                              let data = this.state.data || {};
                              data.name = e.target.value;
                              this.setState({ data, nameError: null });
                            }}
                            value={this.state.data?.name}
                            placeholder="Masukkan Judul Master Kategori Kerjasama"
                            id="name"
                            name="name"
                          />
                          <span style={{ color: "red" }}>
                            {this.state?.nameError}
                          </span>
                        </div>
                        <div className="mt-10 mb-5">
                          <h5 className="mt-7 mb-0">Form isian Kerjasama</h5>
                          <span className="text-muted">
                            Merupakan isian yang akan dilengkapi oleh mitra saat
                            mengajukan kerjsama
                          </span>
                        </div>
                        {this.state.data?.fields.map((field, idx) => (
                          <div className="col-lg-12 mb-7 fv-row">
                            <label className="form-label required">
                              Judul Form Isian {idx + 1}
                            </label>
                            <div className="row">
                              <div className="col-11">
                                <input
                                  className="form-control form-control-sm"
                                  onChange={(e) => {
                                    let data = this.state.data || {};
                                    let fieldsError =
                                      this.state.fieldsError || [];
                                    data.fields = data.fields || [];
                                    data.fields[idx].name = e.target.value;
                                    fieldsError[idx] = null;
                                    this.setState({ data, fieldsError });
                                  }}
                                  value={
                                    (this.state.data?.fields || [])[idx]?.name
                                  }
                                  id="slug"
                                  placeholder={`Masukkan Judul Form Isian ${
                                    idx + 1
                                  }`}
                                  name="slug"
                                />
                                <span style={{ color: "red" }}>
                                  {(this.state?.fieldsError || [])[idx]}
                                </span>
                              </div>
                              <div className="col-1">
                                <button
                                  title="Hapus"
                                  className={`btn btn-icon btn-bg-danger btn-sm ${
                                    (this.state.data?.fields || []).length <= 1
                                      ? `disabled`
                                      : ``
                                  }`}
                                  onClick={(e) => {
                                    let data = this.state.data || {};
                                    data.fields?.splice(idx, 1);
                                    this.setState({ data });
                                  }}
                                >
                                  <i className="bi bi-trash-fill text-white"></i>
                                </button>
                              </div>
                            </div>
                          </div>
                        ))}
                        <div className="row text-center">
                          <div className="col-lg-12 text-center mb-5">
                            <div className="col-lg-12 fv-row mb-7">
                              <a
                                className="btn btn-light text-success btn-sm d-block fw-semibold me-3 mr-2"
                                onClick={() => {
                                  let data = this.state.data || {};
                                  data.fields = [...(data?.fields || []), {}];
                                  this.setState({ data });
                                }}
                              >
                                {" "}
                                <i className="bi bi-plus-circle text-success me-1"></i>{" "}
                                Tambah Form
                              </a>
                            </div>
                          </div>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">Status</label>
                          <>
                            <Select
                              name="status"
                              placeholder="Silahkan pilih"
                              className="form-select-sm selectpicker p-0"
                              value={this.state.statusList.find(
                                ({ value }) => value == this.state.data?.status,
                              )}
                              // defaultValue={selectStatus}
                              // isOptionSelected={selectStatus}
                              options={this.state.statusList}
                              onChange={({ value }) => {
                                let data = this.state.data || {};
                                data.status = value;
                                this.setState({ data, statusError: null });
                              }}
                            />
                          </>
                          <span style={{ color: "red" }}>
                            {this.state?.statusError}
                          </span>
                        </div>

                        <div className="form-group fv-row pt-7 mb-7">
                          <div className="d-flex justify-content-center mb-7">
                            <a
                              href="/partnership/masterkerjasama"
                              type="reset"
                              className="btn btn-md btn-light me-3"
                              data-kt-menu-dismiss="true"
                            >
                              Batal
                            </a>
                            <button
                              type="button"
                              className="btn btn-primary btn-md"
                              onClick={(e) => this.validateAndSave(e)}
                              disabled={this.state.isLoading}
                            >
                              {this.state.isLoading ? (
                                <>
                                  <span
                                    className="spinner-border spinner-border-sm me-2"
                                    role="status"
                                    aria-hidden="true"
                                  ></span>
                                  <span className="sr-only">Loading...</span>
                                  Loading...
                                </>
                              ) : (
                                <>
                                  <i className="fa fa-paper-plane me-1"></i>
                                  Simpan
                                </>
                              )}
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

class MasterKerjasamaContent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Header />
        <SideNav />
        <Content {...this.props} />
        <Footer />
      </div>
    );
  }
}

export default withRouter(MasterKerjasamaContent);
