import axios from "axios";
import Cookies from "js-cookie";
import Swal from "sweetalert2";

const configs = {
  headers: {
    Authorization: "Bearer " + Cookies.get("token"),
  },
};

const checkToken = () => {
  return new Promise(async (resolve, reject) => {
    if (Cookies.get("token") == null) {
      const { isConfirmed } = await Swal.fire({
        title: "Unauthenticated.",
        text: "Silahkan login ulang",
        icon: "warning",
        confirmButtonText: "Ok",
      });

      if (isConfirmed) return reject();
    }

    resolve();
  });
};

export const fetchMasterKerjasama = async ({
  start = 0,
  length = 10,
  status = -1,
  param = "",
  sort = "id",
  sort_val = "desc",
}) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/list_catcoorp",
            {
              start,
              rows: length,
              status: status,
              sort_by: sort,
              sort_type: sort_val,
              search: param,
            },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let {
              TotalLength,
              Data,
              Status = false,
              Message = "",
            } = result || {};
            [TotalLength] = TotalLength;

            if (Status)
              resolve({
                total: TotalLength?.jml,
                data: Data,
              });
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchMasterKerjasamaById = async ({ id }) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/get_catcoorp",
            { id },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data, Status = false, Message = "" } = result || {};
            [Data] = Data || [];
            Data = {
              name: Data?.pcooperation_categories,
              fields: Data?.data_form?.map(({ id, cooperation_form }) => ({
                id,
                name: cooperation_form,
              })),
              status: Data?.pstatus?.toLowerCase() == "aktif" ? 1 : 0,
            };

            if (Status) resolve(Data);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchTemaByMasterKerjasama = async ({
  id,
  start = 0,
  length = 10,
}) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/detail-MasterKerjasama-tema-paging",
            { id, start, length },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let {
              TotalLength,
              Tema,
              Status = false,
              Message = "",
            } = result || {};

            if (Status)
              resolve({
                total: TotalLength,
                data: Tema,
              });
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchMitraByMasterKerjasama = async ({
  id,
  start = 0,
  length = 10,
}) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/detail-MasterKerjasama-mitra-paging",
            { id, start, length },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let {
              TotalLength,
              Mitra,
              Status = false,
              Message = "",
            } = result || {};

            if (Status)
              resolve({
                total: TotalLength,
                data: Mitra,
              });
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const fetchPelatihanByMasterKerjasama = async ({
  id,
  start = 0,
  length = 10,
}) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(async () => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/detail-MasterKerjasama-pelatihan-paging",
            { id, start, length },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let {
              TotalLength,
              pelatihan,
              Status = false,
              Message = "",
            } = result || {};

            if (Status)
              resolve({
                total: TotalLength,
                data: pelatihan,
              });
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const deleteMasterKerjasama = async ({ id }) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/delete_catcoorp",
            { id },
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Message);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const validate = (data) => {
  let { name, fields = [], status } = data || {};
  let error = {};

  if (!name) error["nameError"] = "Nama Master Kerjasama tidak boleh kosong";
  fields.forEach((field, idx) => {
    error["fieldsError"] = error["fieldsError"] || [];
    if (!field?.name)
      error["fieldsError"][idx] = `Judul Form Isian Kerjasama ${
        idx + 1
      } tidak boleh kosong`;
  });
  if (status == null) error["statusError"] = "Status tidak boleh kosong";

  return error;
};

export const resetError = () => {
  return {};
};

export const hasError = (error = {}) => {
  let _hasError = false;
  Object.keys(error).forEach((k) => {
    if (!error[k]) {
    } else if (Array.isArray(error[k]) || typeof error[k] == "object") {
      _hasError = _hasError || hasError(error[k]);
    } else {
      _hasError = _hasError || true;
    }
  });
  return _hasError;
};

export const update = async (payload) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/update_catcoorp",
            payload,
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Message);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};

export const insert = async (payload) => {
  return new Promise((resolve, reject) => {
    checkToken()
      .then(() => {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/create_catcoorp",
            payload,
            configs,
          )
          .then((res) => {
            const { data } = res || {};
            const { result } = data;
            let { Data = [], Status = false, Message = "" } = result || {};

            if (Status) resolve(Message);
            else reject(Message);
          })
          .catch((err) => {
            const { data } = err.response;
            const { result } = data || {};
            const { Data, Status = false, Message = "" } = result || {};
            reject(Message);
          });
      })
      .catch(reject);
  });
};
