import React, { useEffect, useMemo, useRef, useState } from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import DataTable, {
  PaginationComponentProps,
} from "react-data-table-component";
// import Pagination from 'react-data-table-component';
import { customLoader, setUpUrl } from "../../../utils/commonutil";
import _ from "lodash";

import * as Xlsx from "xlsx";
import moment from "moment";
import { capitalizeFirstLetter } from "../../publikasi/helper";

const isMitraLuarNegeri = ({ indonesia_cities_id, indonesia_provinces_id }) => {
  return indonesia_cities_id == 0 && indonesia_provinces_id == 0 ? true : false;
};

export default function MitraContent() {
  const btnModalFilter = useRef(null);

  const [data, setData] = useState([
    {
      id: 96,
      nama_mitra: "Sukseskarir",
      email: "sukseskarir@gmail.com",
      agency_logo:
        "https://s3d.sdmdigital.id:9000/dts-partnership/1656407954_945754bfe4b5507e3f94b0233770871b.png?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=TEST1%2F20220918%2Fus-west-0%2Fs3%2Faws4_request&X-Amz-Date=20220918T044122Z&X-Amz-SignedHeaders=host&X-Amz-Expires=60&X-Amz-Signature=87cbf5fe0b8786aaddbcd8417314b44e1764b71d4673bc00622e9e6292a93ec3",
      website: "www.sukseskarir.com",
      address: "Jl Apel",
      indonesia_provinces_id: 14,
      indonesia_cities_id: 1403,
      postal_code: "16267               ",
      user_id: 156091,
      pic_name: "Anto",
      pic_contact_number: "087717282719",
      pic_email: "darkatnight16@gmail.com",
      status: "Tidak aktif",
      jml_kerjasama: 0,
      visit: 1,
      jml_pelatihan: null,
    },
  ]);
  const [isLoading, setIsLoading] = useState(true);
  const [totalRows, setTotalRows] = useState(0);

  const [filterStatus, setFilterStatus] = useState(null);

  const searchInputRef = useRef();

  const [downloadUrl, setDownloadUrl] = useState("");
  // const [word, setWord] = useState("")

  // const [filter, setFilter] = useState({
  //   tahun: "0",
  //   status: ""
  // })

  const [preFilter, setPreFilter] = useState({
    tahun: "0",
    status: "",
  });

  // const [page, setPage] = useState({ pos: 0, perPage: 10 })
  // const [sort, setSort] = useState({ sortField: "id", sortDirection: "desc" })

  const [queryParam, setQueryParam] = useState({
    word: "",
    pos: 0,
    perPage: 10,
    sortField: "id",
    sortDirection: "desc",
    status: "",
    tahun: "0",
    status: "",
  });

  const [isLoadingDownload, setIsLoadingDownload] = useState(false);

  const [isPaginationReset, setIsPaginationReset] = useState(false);

  useEffect(() => {
    if (isPaginationReset) {
      // setIsPaginationReset(false)
    }
  }, [isPaginationReset]);

  const optionstatus = [
    { value: "99", label: "Semua status" },
    { value: "1", label: "Aktif" },
    { value: "0", label: "Tidak Aktif" },
  ];

  const customLoader = (
    <div style={{ padding: "24px" }}>
      <img src="/assets/media/loader/loader-biru.gif" />
    </div>
  );

  // setUpUrl

  const columns = [
    {
      name: "No",
      center: true,
      width: "70px",
      cell: (row, index) => (
        <div>
          <span>{index + 1 + queryParam.pos * queryParam.perPage}</span>
        </div>
      ),
    },
    {
      name: "Mitra",
      className: "min-w-300px mw-300px",
      sortable: true,
      sortField: "nama_mitra",
      selector: (row) => row.nama_mitra,
      width: "350px",
      grow: 6,
      cell: (row) => (
        <>
          <label className="d-flex flex-stack my-2 cursor-pointer">
            <span className="d-flex align-items-center me-2">
              <span className="symbol symbol-50px me-6">
                <span className="symbol-label bg-light-primary">
                  <span className="svg-icon svg-icon-1 svg-icon-primary">
                    <img
                      src={row.agency_logo}
                      alt=""
                      className="symbol-label"
                    />
                  </span>
                </span>
              </span>
              <span className="d-flex flex-column">
                <a
                  href={"/partnership/view-mitra/" + row.id}
                  id={row.id}
                  className="text-dark"
                >
                  <span
                    className="fw-bolder fs-7"
                    style={{
                      overflow: "hidden",
                      whiteSpace: "wrap",
                      textOverflow: "ellipses",
                    }}
                  >
                    {row.nama_mitra}
                  </span>
                </a>
              </span>
            </span>
          </label>
        </>
      ),
    },
    {
      name: "Kerjasama",
      sortable: true,
      sortField: "jml_kerjasama",
      selector: (row) => row.jml_kerjasama,
      className: "min-w-150px",
      center: true,
      grow: 2,
      cell: (row) => {
        return (
          <span
            className="badge badge-light-primary fs-7 m-1"
            style={{
              overflow: "hidden",
              whiteSpace: "wrap",
              textOverflow: "ellipses",
            }}
          >
            {row.jml_kerjasama}
          </span>
        );
      },
    },
    {
      name: "Pelatihan",
      sortable: true,
      sortField: "jml_pelatihan",
      center: true,
      className: "min-w-200px",
      grow: 2,
      selector: (row) => (row.jml_pelatihan == null ? 0 : row.jml_pelatihan),
      cell: (row) => {
        return (
          <span
            className="badge badge-light-primary fs-7 m-1"
            style={{
              overflow: "hidden",
              whiteSpace: "wrap",
              textOverflow: "ellipses",
            }}
          >
            {row.jml_pelatihan}
          </span>
        );
      },
    },
    {
      name: "Status",
      sortable: true,
      sortField: "status",
      selector: (row) => row.status,
      className: "min-w-200px",
      grow: 2,
      cell: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.status == "Aktif" ? "success" : "danger") +
              " fs-7 m-1"
            }
          >
            {row.status == "Aktif" ? "Aktif" : "Tidak Aktif"}
          </span>
        </div>
      ),
    },
    {
      name: "Aksi",
      center: true,
      grow: 3,
      cell: (row) => (
        <div>
          <a
            href={"/partnership/view-mitra/" + row.id}
            id={row.id}
            className="btn btn-icon btn-bg-primary btn-sm me-1"
            title="Lihat"
            alt="Lihat"
          >
            <i className="bi bi-eye-fill text-white"></i>
          </a>

          <a
            href={"/partnership/edit-mitra/" + row.id}
            id={row.id}
            className="btn btn-icon btn-bg-warning btn-sm me-1"
            title="Edit"
            alt="Edit"
          >
            <i className="bi bi-gear-fill text-white"></i>
          </a>

          <a
            href="#"
            id={row.id}
            onClick={() => {
              handleDelete(row);
            }}
            className="btn btn-icon btn-bg-danger btn-sm me-1"
            title="Hapus"
            alt="Hapus"
          >
            <i className="bi bi-trash-fill text-white"></i>
          </a>
        </div>
      ),
    },
  ];

  const customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  const getData = async () => {
    // console.log(sort)
    setData([]);
    setIsLoading(true);
    try {
      const params = {
        start: queryParam.pos * queryParam.perPage,
        length: queryParam.perPage,
        sort: queryParam.sortField,
        sort_val: queryParam.sortDirection,
        param: queryParam.word,
        status: queryParam.status || "99",
        tahun: queryParam.tahun,
      };
      // console.log(params)
      let formData = new FormData();
      Object.keys(params).forEach((k) => {
        formData.append(k, params[k]);
      });
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/mitra/list-mitra-s3",
          formData,
          {
            headers: {
              Authorization: "Bearer " + Cookies.get("token"),
            },
          },
        )
        .then((resp) => {
          if (resp.data.result.Status) {
            setData(resp.data.result.Data);
            setTotalRows(resp.data.result.Total);
          } else {
            swal.fire({
              title: "Kesalahan",
              text: resp.data.result.Message || "Error",
              icon: "warning",
            });
          }
        })
        .catch((err) => {
          // console.log(err)
          const resp = err.response;
          swal.fire({
            title: "Kesalahan",
            text: resp.data.result.Message || "Error",
            icon: "warning",
          });
        });
      // console.log(resp)
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  const downloadData = async () => {
    setIsLoadingDownload(true);
    try {
      const params = {
        start: 0,
        length: 100000,
        sort: queryParam.sortField,
        sort_val: queryParam.sortDirection,
        param: queryParam.word,
        status: queryParam.status || "99",
        tahun: queryParam.tahun,
      };
      // console.log(params)
      let formData = new FormData();
      Object.keys(params).forEach((k) => {
        formData.append(k, params[k]);
      });
      const resp = await axios.post(
        process.env.REACT_APP_BASE_API_URI + "/mitra/list-mitra-s3",
        formData,
        {
          headers: {
            Authorization: "Bearer " + Cookies.get("token"),
          },
        },
      );
      // console.log(resp)
      if (resp.data.result.Status) {
        let data = resp.data.result.Data;
        console.log(data);
        let excelData = [];
        for (let i = 0; i < data.length; i++) {
          const {
            nama_mitra,
            status,
            email,
            website,
            jml_kerjasama,
            jml_pelatihan,
          } = data[i];
          excelData.push({
            No: i + 1,
            Mitra: nama_mitra,
            Status: status,
            Email: email,
            Website: website,
            "Jumlah Kerjasama": jml_kerjasama || 0,
            "Jumlah Pelatihan": jml_pelatihan || 0,
          });
        }
        const worksheet = Xlsx.utils.json_to_sheet(excelData);
        const workbook = Xlsx.utils.book_new();
        Xlsx.utils.book_append_sheet(workbook, worksheet, "Mitra");
        Xlsx.writeFile(
          workbook,
          "Mitra " + moment().format("YYYYMMDD") + ".xlsx",
        );
        // setData(resp.data.result.Data)
        // setTotalRows(resp.data.result.Total)
      }
    } catch (error) {
      console.log(error);
    }
    setIsLoadingDownload(false);
  };

  const onSortTable = (column, sortDirection) => {
    // console.log(column, sortDirection)
    if (column.sortable) {
      setQueryParam((q) => ({
        ...q,
        sortField: column.sortField,
        sortDirection: sortDirection,
      }));
      // setSort({ sortField: column.sortField, sortDirection: sortDirection })
    }
  };

  const handlePageChange = (page) => {
    // fetchUsers(page);
    // console.log(page)
    setQueryParam((q) => ({ ...q, pos: page - 1 }));
    // setPage((p) => ({ ...p, pos: page - 1 }))
  };

  const handlePerRowsChange = async (newPerPage, page) => {
    // setLoading(true);
    setQueryParam((q) => ({ ...q, pos: page - 1, perPage: newPerPage }));
    // setPage({ pos: page - 1, perPage: newPerPage })
  };

  const handleSearch = () => {
    // console.log(searchInputRef.current.value)
    // setWord(searchInputRef.current.value);
    setIsPaginationReset(!isPaginationReset);
    setQueryParam((q) => ({
      ...q,
      word: searchInputRef.current.value,
      pos: 0,
    }));
  };

  const handleDelete = (row) => {
    // console.log(row)
    // return
    const idx = row.id;
    swal
      .fire({
        title: `Apakah anda yakin akan menghapus mitra <strong>${
          row.nama_mitra
        }</strong> (${isMitraLuarNegeri(row) ? "luar negeri" : "indonesia"}) ?`,
        text: "*Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          const dt = { id: idx };
          let data = new FormData();
          data.append("id", idx);
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI +
                "/mitra//softdelete-mitra-v2",
              data,
              {
                headers: {
                  Authorization: "Bearer " + Cookies.get("token"),
                },
              },
            )
            .then((res) => {
              console.log(res);

              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      getData();
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      // this.handleReload("home");
                      getData();
                    }
                  });
              }
            })
            .catch((error) => {
              if (error.toJSON().message.includes("404")) {
                axios
                  .post(
                    "http://back.dev.sdmdigital.id/api" +
                      "/mitra//softdelete-mitra-v2",
                    data,
                    {
                      headers: {
                        Authorization: "Bearer " + Cookies.get("token"),
                      },
                    },
                  )
                  .then((res) => {
                    console.log(res);

                    const statux = res.data.result.Status;
                    const messagex = res.data.result.Message;
                    if (statux) {
                      swal
                        .fire({
                          title: messagex,
                          icon: "success",
                          confirmButtonText: "Ok",
                        })
                        .then((result) => {
                          if (result.isConfirmed) {
                            getData();
                          }
                        });
                    } else {
                      swal
                        .fire({
                          title: messagex,
                          icon: "warning",
                          confirmButtonText: "Ok",
                        })
                        .then((result) => {
                          if (result.isConfirmed) {
                            // this.handleReload("home");
                            getData();
                          }
                        });
                    }
                  })
                  .catch((error) => {
                    if (error.toJSON().message.includes("404")) {
                      return;
                    }
                    console.log(error.toJSON());
                    let statux = error.response.data.result.Status;
                    let messagex = error.response.data.result.Message;
                    if (!statux) {
                      swal
                        .fire({
                          title: messagex,
                          icon: "warning",
                          confirmButtonText: "Ok",
                        })
                        .then((result) => {
                          if (result.isConfirmed) {
                          }
                        });
                    }
                  });

                return;
              }
              console.log(error.toJSON());
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            });
        }
      });
  };

  useEffect(() => {
    getData();
  }, [queryParam]);

  useEffect(() => {
    if (filterStatus == null) {
      btnModalFilter?.current?.click();
    }
  }, [filterStatus]);

  return (
    <div>
      <input
        type="hidden"
        name="csrf-token"
        value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
      />
      <div className="toolbar" id="kt_toolbar">
        <div
          id="kt_toolbar_container"
          className="container-fluid d-flex flex-stack my-2"
        >
          <div className="d-flex align-items-start my-2">
            <div>
              <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                <span className="me-3">
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    className="mh-50px"
                  >
                    <path
                      opacity="0.3"
                      d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z"
                      fill="#FFC700"
                    ></path>
                    <path
                      d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z"
                      fill="#FFC700"
                    ></path>
                  </svg>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Partnership
                  <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                </span>
                Mitra
              </h1>
            </div>
          </div>
          <div className="d-flex align-items-end my-2">
            <div>
              <button
                className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                data-bs-toggle="modal"
                data-bs-target="#filter"
              >
                <i className="bi bi-sliders"></i>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Filter
                </span>
              </button>
              <a
                href="/partnership/tambah-mitra"
                className="btn btn-success fw-bolder btn-sm"
              >
                <i className="bi bi-plus-circle"></i>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Tambah Mitra
                </span>
              </a>
            </div>
          </div>
        </div>
      </div>

      <div
        className="wrapper d-flex flex-column flex-row-fluid pt-0"
        id="kt_wrapper"
      >
        <div
          className="content d-flex flex-column flex-column-fluid pt-0"
          id="kt_content"
        >
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container-xxl">
              <div className="row">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Daftar Mitra
                        </h1>
                      </div>
                      <div className="card-toolbar">
                        <div className="d-flex align-items-center position-relative my-1 me-2">
                          <span className="svg-icon svg-icon-1 position-absolute ms-6">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                              className="mh-50px"
                            >
                              <rect
                                opacity="0.5"
                                x="17.0365"
                                y="15.1223"
                                width="8.15546"
                                height="2"
                                rx="1"
                                transform="rotate(45 17.0365 15.1223)"
                                fill="currentColor"
                              ></rect>
                              <path
                                d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                fill="currentColor"
                              ></path>
                            </svg>
                          </span>
                          <input
                            ref={searchInputRef}
                            type="text"
                            onChange={_.debounce(handleSearch, 700)}
                            data-kt-user-table-filter="search"
                            className="form-control form-control-sm form-control-solid w-250px ps-14 me-3"
                            placeholder="Cari MOU/PKS"
                            name="cari"
                          />
                          <a onClick={downloadData}>
                            <button
                              className={`btn btn-sm btn-flex btn-light fw-bolder me-2 ${
                                isLoadingDownload ? "disabled" : ""
                              }`}
                              disabled={isLoadingDownload}
                            >
                              {isLoadingDownload && (
                                <span
                                  className="spinner-border spinner-border-sm"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                              )}
                              <i className="bi bi-cloud-download"></i>
                              <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                                Export
                              </span>
                            </button>
                          </a>
                        </div>

                        <div className="modal fade" tabIndex="-1" id="filter">
                          <div className="modal-dialog modal-lg">
                            <div className="modal-content">
                              <div className="modal-header py-3">
                                <h5 className="modal-title">
                                  <span className="svg-icon svg-icon-5 me-1">
                                    <i className="bi bi-sliders text-black"></i>
                                  </span>
                                  Filter Daftar Mitra
                                </h5>
                                <div
                                  className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                  data-bs-dismiss="modal"
                                  aria-label="Close"
                                >
                                  <span className="svg-icon svg-icon-2x">
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width="24"
                                      height="24"
                                      viewBox="0 0 24 24"
                                      fill="none"
                                    >
                                      <rect
                                        opacity="0.5"
                                        x="6"
                                        y="17.3137"
                                        width="16"
                                        height="2"
                                        rx="1"
                                        transform="rotate(-45 6 17.3137)"
                                        fill="currentColor"
                                      />
                                      <rect
                                        x="7.41422"
                                        y="6"
                                        width="16"
                                        height="2"
                                        rx="1"
                                        transform="rotate(45 7.41422 6)"
                                        fill="currentColor"
                                      />
                                    </svg>
                                  </span>
                                </div>
                              </div>
                              <form action="#">
                                <div className="modal-body">
                                  <div className="row">
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label">
                                        Status
                                      </label>
                                      <Select
                                        name="status"
                                        placeholder="Silahkan pilih"
                                        // noOptionsMessage={({ inputValue }) => !inputValue ? this.state.optionstatus : "Data tidak tersedia"}
                                        className="form-select-sm selectpicker p-0"
                                        options={optionstatus}
                                        value={filterStatus}
                                        onChange={(v) => {
                                          setFilterStatus(v);
                                          setPreFilter((pr) => ({
                                            ...pr,
                                            status: v.value,
                                          }));
                                        }}
                                      />
                                    </div>
                                  </div>
                                </div>
                                <div className="modal-footer">
                                  <div className="d-flex justify-content-between">
                                    <button
                                      type="button"
                                      className="btn btn-sm btn-light me-3"
                                      onClick={() => {
                                        if (filterStatus != null) {
                                          // setPage({ pos: 0, perPage: 10 })
                                          // setIsPaginationReset(!isPaginationReset)
                                          // setPage((p) => ({ ...p, pos: 0 }))
                                          setFilterStatus(null);
                                          setPreFilter({
                                            status: "",
                                            tahun: "0",
                                          });
                                        }
                                      }}
                                    >
                                      Reset
                                    </button>
                                    <button
                                      ref={btnModalFilter}
                                      type="button"
                                      className="btn btn-sm btn-primary"
                                      data-bs-dismiss="modal"
                                      onClick={() => {
                                        // setPage((p) => ({ ...p, pos: 0 }))
                                        // setFilter({ ...preFilter })
                                        setIsPaginationReset(
                                          !isPaginationReset,
                                        );
                                        setQueryParam((q) => ({
                                          ...q,
                                          ...preFilter,
                                          pos: 0,
                                        }));
                                        // setPreFilter({ status: "", tahun: "0" })
                                      }}
                                    >
                                      Apply Filter
                                    </button>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="table-responsive">
                        <DataTable
                          columns={columns}
                          // page={page.pos}
                          paginationPerPage={queryParam.perPage}
                          paginationDefaultPage={1}
                          paginationResetDefaultPage={isPaginationReset}
                          data={data}
                          progressPending={isLoading}
                          progressComponent={customLoader}
                          highlightOnHover
                          pointerOnHover
                          pagination
                          paginationServer
                          // paginationComponentOptions={}
                          // paginationComponent={<Pagination currentPage={1}></Pagination>}
                          // paginationTotalRows={this.state.totalRows}
                          // onChangeRowsPerPage={this.handlePerRowsChange}
                          // onChangePage={handlePageChange}
                          paginationTotalRows={totalRows}
                          paginationComponentOptions={{
                            selectAllRowsItem: true,
                            selectAllRowsItemText: "Semua",
                          }}
                          onChangeRowsPerPage={handlePerRowsChange}
                          onChangePage={handlePageChange}
                          customStyles={customStyles}
                          persistTableHead={true}
                          noDataComponent={
                            <div className="mt-5">Tidak Ada Data</div>
                          }
                          onSort={onSortTable}
                          sortServer
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
