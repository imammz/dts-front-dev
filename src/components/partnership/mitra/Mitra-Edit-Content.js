import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import { isValidEmail, isValidUrl } from "../../../utils/commonutil";

const isMitraLuarNegeri = ({ indonesia_cities_id, indonesia_provinces_id }) => {
  return indonesia_cities_id == 0 && indonesia_provinces_id == 0 ? true : false;
};

export default class TemaContent extends React.Component {
  constructor(props) {
    super(props);
    Cookies.remove("mitra_id");
    Cookies.remove("user_id");
    this.handleClick = this.handleClickAction.bind(this);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleChangeLogo = this.handleChangeLogoAction.bind(this);
    this.handleChangeProv = this.handleChangeProvAction.bind(this);
    this.handleChangeKabupaten = this.handleChangeKabupatenAction.bind(this);
    this.handleChangeCountry = this.handleChangeCountryAction.bind(this);
    this.handleChangeOrigin = this.handleChangeOriginAction.bind(this);
    this.recheckValidation = this.recheckValidation.bind(this);
    this.state = {
      fields: {},
      isLoading: false,
      errors: {},
      datax: [],
      dataxmitra: [],
      dataxprov: [],
      dataxcountry: [],
      selprovinsi: null,
      selkabupaten: null,
      selstatus: [],
      isDisabledKab: true,
      showing: true,
      showingcountry: false,
      valueorigin: "",
      wasSubmitted: false,
    };
    this.formDatax = new FormData();
  }
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  optionstatus = [
    { value: "1", label: "Aktif" },
    { value: "0", label: "Tidak aktif" },
  ];
  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    let segment_url = window.location.pathname.split("/");
    let urlSegmenttOne = segment_url[3];
    Cookies.set("mitra_id", urlSegmenttOne);
    let data = {
      id: urlSegmenttOne,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/mitra/cari-mitra",
        data,
        this.configs,
      )
      .then((res) => {
        const dataxmitra = res.data.result.Data[0];
        this.setState({ dataxmitra });
        Cookies.set("user_id", this.state.dataxmitra.user_id);

        const dataProv = { start: 0, rows: 1000 };
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/provinsi",
            dataProv,
            this.configs,
          )
          .then((res) => {
            const optionx = res.data.result.Data;
            const dataxprov = [];
            for (let i in optionx) {
              if (
                optionx[i].id == this.state.dataxmitra.indonesia_provinces_id
              ) {
                this.setState({
                  selprovinsi: { value: optionx[i].id, label: optionx[i].name },
                });
              }
            }
            optionx.map((data) =>
              dataxprov.push({ value: data.id, label: data.name }),
            );
            this.setState({ dataxprov });
          });

        if (this.state.dataxmitra.indonesia_provinces_id != "0") {
          const dataKab = {
            kdprop: this.state.dataxmitra.indonesia_provinces_id,
          };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/kabupaten",
              dataKab,
              this.configs,
            )
            .then((res) => {
              this.setState({ isDisabledKab: false });
              const options = res.data.result.Data;
              const dataxkab = [];
              for (let i in options) {
                if (
                  options[i].id == this.state.dataxmitra.indonesia_cities_id
                ) {
                  this.setState({
                    selkabupaten: {
                      value: options[i].id,
                      label: options[i].name,
                    },
                  });
                }
              }
              options.map((data) =>
                dataxkab.push({ value: data.id, label: data.name }),
              );
              this.setState({ dataxkab });
            });
        }

        const dataCountry = { start: 0, rows: 1000 };
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/mitra/list-country",
            dataCountry,
            this.configs,
          )
          .then((res) => {
            const optionx = res.data.result.Data;
            const dataxcountry = [];
            for (let i in optionx) {
              if (optionx[i].id == this.state.dataxmitra.country_id) {
                this.setState({
                  selcountry: {
                    value: optionx[i].id,
                    label: optionx[i].country_name,
                  },
                });
              }
            }
            optionx.map((data) =>
              dataxcountry.push({ value: data.id, label: data.country_name }),
            );
            this.setState({ dataxcountry });
          });

        //initial awal
        if (this.state.dataxmitra.origin_country == "1") {
          this.setState({ showing: false });
          this.setState({ showingcountry: true });
        }
        this.setState({
          selstatus: {
            value: this.state.dataxmitra.status == "Aktif" ? "1" : "0",
            label:
              this.state.dataxmitra.status == "Aktif" ? "Aktif" : "Tidak aktif",
          },
        });
      });
  }
  validation(dataForm) {
    const acceptedFiles = ["png", "jpg", "jpeg", "svg"];
    let errors = {};
    let formIsValid = true;

    if (dataForm.get("name") == "") {
      formIsValid = false;
      errors["name"] = "Tidak boleh kosong";
    }
    if (dataForm.get("website") == "") {
      formIsValid = false;
      errors["website"] = "Tidak boleh kosong";
    } else {
      if (!isValidUrl(dataForm.get("website"))) {
        formIsValid = false;
        errors["website"] = "URL tidak valid";
      }
    }
    if (dataForm.get("email") == "") {
      formIsValid = false;
      errors["email"] = "Tidak boleh kosong";
    } else if (!isValidEmail(dataForm.get("email"))) {
      formIsValid = false;
      errors["email"] = "Alamat email tidak valid";
    }
    // console.log(dataForm.get("upload_logo"))
    if (dataForm.get("upload_logo").name == "") {
      // formIsValid = false;
      // errors["upload_logo"] = "Tidak boleh kosong";
    } else {
      let file = dataForm.get("upload_logo");
      const { size, type } = file;
      // console.log(type.split("/")[1])
      if (size / 1024 > 1024) {
        formIsValid = false;
        errors["upload_logo"] = "Ukuran file terlalu besar";
      } else if (!acceptedFiles.includes(type.split("/")[1])) {
        formIsValid = false;
        errors["upload_logo"] = "Ekstensi file tidak valid";
      }
    }
    if (dataForm.get("origin_country") == null) {
      formIsValid = false;
      errors["origin_country"] = "Tidak boleh kosong";
    } else {
      if (dataForm.get("origin_country") == "2") {
        if (dataForm.get("provinsi") == "") {
          formIsValid = false;
          errors["provinsi"] = "Tidak boleh kosong";
        }
        if (dataForm.get("kota_kabupaten") == "") {
          formIsValid = false;
          errors["kota_kabupaten"] = "Tidak boleh kosong";
        }
      } else if (dataForm.get("origin_country") == "1") {
        if (dataForm.get("country") == "") {
          formIsValid = false;
          errors["country"] = "Tidak boleh kosong";
        }
      }
    }
    if (dataForm.get("kodepos") == "") {
      formIsValid = false;
      errors["kodepos"] = "Tidak boleh kosong";
    }
    if (dataForm.get("alamat") == "") {
      formIsValid = false;
      errors["alamat"] = "Tidak boleh kosong";
    }
    if (dataForm.get("person_in_charge") == "") {
      formIsValid = false;
      errors["person_in_charge"] = "Tidak boleh kosong";
    }
    if (dataForm.get("phone_in_charge") == "") {
      formIsValid = false;
      errors["phone_in_charge"] = "Tidak boleh kosong";
    }
    if (dataForm.get("email_in_charge") == "") {
      formIsValid = false;
      errors["email_in_charge"] = "Tidak boleh kosong";
    }
    if (dataForm.get("nik") == "") {
      formIsValid = false;
      errors["nik"] = "Tidak boleh kosong";
    }
    if (dataForm.get("status") === "") {
      formIsValid = false;
      errors["status"] = "Tidak boleh kosong";
    }

    return { errors: errors, formIsValid: formIsValid };
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const { errors, formIsValid } = this.validation(dataForm);

    this.setState({ errors: errors });
    return formIsValid;
  }

  resetError() {
    let errors = {};
    errors[
      ("name",
      "website",
      "email",
      "upload_logo",
      "provinsi",
      "kota_kabupaten",
      "kodepos",
      "alamat",
      "person_in_charge",
      "phone_in_charge",
      "email_in_charge",
      "origin_country",
      "country")
    ] = "";
    this.setState({ errors: errors });
  }

  handleClickAction(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    this.setState({ wasSubmitted: true });
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      let data = {
        id: Cookies.get("tema_id"),
        akademi_id: dataForm.get("akademi_id"),
        name: dataForm.get("name"),
        deskripsi: dataForm.get("deskripsi"),
        status: dataForm.get("status"),
      };
      const dataFormSubmit = new FormData();
      dataFormSubmit.append("id", Cookies.get("mitra_id"));
      dataFormSubmit.append("user_id", Cookies.get("user_id"));
      dataFormSubmit.append("name", dataForm.get("name"));
      dataFormSubmit.append("website", dataForm.get("website"));
      dataFormSubmit.append("email", dataForm.get("email"));
      dataFormSubmit.append("address", dataForm.get("alamat"));
      dataFormSubmit.append("postal_code", dataForm.get("kodepos"));
      dataFormSubmit.append("pic_name", dataForm.get("person_in_charge"));
      dataFormSubmit.append(
        "pic_contact_number",
        dataForm.get("phone_in_charge"),
      );
      dataFormSubmit.append("pic_email", dataForm.get("email_in_charge"));
      dataFormSubmit.append("agency_logo", this.formDatax.get("agency_logo"));
      dataFormSubmit.append("origin_country", dataForm.get("origin_country"));
      if (dataForm.get("origin_country") == "2") {
        dataFormSubmit.append(
          "indonesia_provinces_id",
          dataForm.get("provinsi"),
        );
        dataFormSubmit.append(
          "indonesia_cities_id",
          dataForm.get("kota_kabupaten"),
        );
        dataFormSubmit.append("country_id", "0");
      } else {
        dataFormSubmit.append("indonesia_provinces_id", "0");
        dataFormSubmit.append("indonesia_cities_id", "0");
        dataFormSubmit.append("country_id", dataForm.get("country"));
      }
      dataFormSubmit.append("nik", dataForm.get("nik"));
      dataFormSubmit.append("status", dataForm.get("status"));

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/mitra/update-mitra-s3",
          dataFormSubmit,
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/partnership/mitra";
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      this.setState({ isLoading: false });
      swal.fire({
        title: "Periksa Kembali isian anda",
        icon: "error",
        confirmButtonText: "Ok",
      });
      window.scrollTo(0, 0);
    }
  }
  handleClickBatalAction(e) {
    window.location = "/partnership/mitra";
  }
  handleChangeProvAction = (selectedOption) => {
    this.setState({ selkabupaten: null });
    this.setState({
      selprovinsi: { value: selectedOption.value, label: selectedOption.label },
    });
    const dataKab = { kdprop: selectedOption.value };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/kabupaten",
        dataKab,
        this.configs,
      )
      .then((res) => {
        this.setState({ isDisabledKab: false });
        const options = res.data.result.Data;
        const dataxkab = [];
        for (let i in options) {
          if (options[i].id == this.state.dataxmitra.indonesia_cities_id) {
            this.setState({
              selkabupaten: { value: options[i].id, label: options[i].name },
            });
          }
        }
        options.map((data) =>
          dataxkab.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxkab });
      });
  };
  handleChangeKabupatenAction = (selectedOption) => {
    this.setState({
      selkabupaten: {
        value: selectedOption.value,
        label: selectedOption.label,
      },
    });
  };
  handleChangeCountryAction = (selectedOption) => {
    this.setState({
      selcountry: { value: selectedOption.value, label: selectedOption.label },
    });
  };
  handleChangeLogoAction(e) {
    const logofile = e.target.files[0];
    let fm = new FormData();
    fm.append("agency_logo", logofile);
    this.formDatax = fm;
    // this.formDatax.append("agency_logo", logofile);
  }
  handleChangeOriginAction(e) {
    this.setState({ valueorigin: e.target.value });
    if (e.target.value == "2") {
      this.setState({ showing: true });
      this.setState({ showingcountry: false });
    } else {
      this.setState({ showing: false });
      this.setState({ showingcountry: true });
    }
  }

  recheckValidation() {
    if (!this.state.wasSubmitted) return;
    const el = document.getElementById("form-edit-mitra");
    const form = new FormData(el);
    // console.log(this)
    const { errors, formIsValid } = this.validation(form);
    this.setState({ errors: errors });
    // return formIsValid;
  }

  handleChangeStatusAction = (selectedOption) => {
    this.setState({
      selstatus: { value: selectedOption.value, label: selectedOption.label },
    });
  };

  handleDelete = () => {
    // console.log(row)
    // return
    const row = this.state.dataxmitra;
    const idx = row.id;
    swal
      .fire({
        title: `Apakah anda yakin akan menghapus mitra <strong>${
          row.nama_mitra
        }</strong> (${isMitraLuarNegeri(row) ? "luar negeri" : "indonesia"}) ?`,
        text: "*Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          const dt = { id: idx };
          let data = new FormData();
          data.append("id", idx);
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI +
                "/mitra//softdelete-mitra-v2",
              data,
              {
                headers: {
                  Authorization: "Bearer " + Cookies.get("token"),
                },
              },
            )
            .then((res) => {
              console.log(res);

              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      // getData()
                      // history.back()
                      this.handleClickBatal();
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      // this.handleReload("home");
                      // getData()
                      this.handleClickBatal();
                    }
                  });
              }
            })
            .catch((error) => {
              if (error.toJSON().message.includes("404")) {
                axios
                  .post(
                    "http://back.dev.sdmdigital.id/api" +
                      "/mitra//softdelete-mitra-v2",
                    data,
                    {
                      headers: {
                        Authorization: "Bearer " + Cookies.get("token"),
                      },
                    },
                  )
                  .then((res) => {
                    console.log(res);

                    const statux = res.data.result.Status;
                    const messagex = res.data.result.Message;
                    if (statux) {
                      swal
                        .fire({
                          title: messagex,
                          icon: "success",
                          confirmButtonText: "Ok",
                        })
                        .then((result) => {
                          if (result.isConfirmed) {
                            // getData()
                            this.handleClickBatal();
                          }
                        });
                    } else {
                      swal
                        .fire({
                          title: messagex,
                          icon: "warning",
                          confirmButtonText: "Ok",
                        })
                        .then((result) => {
                          if (result.isConfirmed) {
                            // this.handleReload("home");
                            // getData()
                            this.handleClickBatal();
                          }
                        });
                    }
                  })
                  .catch((error) => {
                    if (error.toJSON().message.includes("404")) {
                      return;
                    }
                    console.log(error.toJSON());
                    let statux = error.response.data.result.Status;
                    let messagex = error.response.data.result.Message;
                    if (!statux) {
                      swal
                        .fire({
                          title: messagex,
                          icon: "warning",
                          confirmButtonText: "Ok",
                        })
                        .then((result) => {
                          if (result.isConfirmed) {
                          }
                        });
                    }
                  });

                return;
              }
              console.log(error.toJSON());
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            });
        }
      });
  };
  render() {
    return (
      <div>
        <input
          type="hidden"
          name="csrf-token"
          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
        />
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z"
                        fill="#FFC700"
                      ></path>
                      <path
                        d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z"
                        fill="#FFC700"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Partnership
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Mitra
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>

                <button
                  className="btn btn-sm btn-danger btn-active-light-info ms-2"
                  onClick={() => {
                    this.handleDelete();
                  }}
                >
                  <i className="bi bi-trash-fill text-white"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Edit Mitra
                          </h1>
                        </div>
                      </div>
                      <div className="card-body ">
                        <form
                          onChange={this.recheckValidation}
                          id="form-edit-mitra"
                          className="form"
                          action="#"
                          onSubmit={this.handleClick}
                        >
                          <div className="row">
                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Nama Mitra
                              </label>
                              <input
                                className="form-control form-control-sm"
                                placeholder="Masukkan nama lembaga"
                                name="name"
                                defaultValue={this.state.dataxmitra.nama_mitra}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["name"]}
                              </span>
                            </div>
                            <div className="col-lg-6 mb-7 fv-row">
                              <label className="form-label required">
                                Website Resmi
                              </label>
                              <input
                                className="form-control form-control-sm"
                                placeholder="Masukkan Website Resmi"
                                name="website"
                                defaultValue={this.state.dataxmitra.website}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["website"]}
                              </span>
                            </div>
                            <div className="col-lg-6 mb-7 fv-row">
                              <label className="form-label required">
                                Email Resmi
                              </label>
                              <input
                                className="form-control form-control-sm"
                                placeholder="Masukkan Email Resmi"
                                name="email"
                                defaultValue={this.state.dataxmitra.email}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["email"]}
                              </span>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Asal Mitra
                              </label>
                              <div className="d-flex">
                                <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                  {this.state.dataxmitra.origin_country ==
                                  "2" ? (
                                    <input
                                      className="form-check-input"
                                      type="radio"
                                      name="origin_country"
                                      value="2"
                                      id="origin_country1"
                                      onClick={this.handleChangeOrigin}
                                      defaultChecked="true"
                                    />
                                  ) : (
                                    <input
                                      className="form-check-input"
                                      type="radio"
                                      name="origin_country"
                                      value="2"
                                      id="origin_country1"
                                      onClick={this.handleChangeOrigin}
                                    />
                                  )}
                                  <label
                                    className="form-check-label"
                                    htmlFor="origin_country1"
                                  >
                                    Dalam Negeri
                                  </label>
                                </div>
                                <div className="form-check form-check-sm form-check-custom form-check-solid">
                                  {this.state.dataxmitra.origin_country ==
                                  "1" ? (
                                    <input
                                      className="form-check-input"
                                      type="radio"
                                      name="origin_country"
                                      value="1"
                                      id="origin_country2"
                                      onClick={this.handleChangeOrigin}
                                      defaultChecked="true"
                                    />
                                  ) : (
                                    <input
                                      className="form-check-input"
                                      type="radio"
                                      name="origin_country"
                                      value="1"
                                      id="origin_country2"
                                      onClick={this.handleChangeOrigin}
                                    />
                                  )}
                                  <label
                                    className="form-check-label"
                                    htmlFor="origin_country2"
                                  >
                                    Luar Negeri
                                  </label>
                                </div>
                              </div>
                              <span style={{ color: "red" }}>
                                {this.state.errors["origin_country"]}
                              </span>
                            </div>
                            <div
                              style={{
                                display: this.state.showing ? "block" : "none",
                              }}
                            >
                              <div className="col-lg-12 mb-7 fv-row">
                                <label className="form-label required">
                                  Provinsi
                                </label>
                                <Select
                                  name="provinsi"
                                  placeholder="Silahkan pilih"
                                  noOptionsMessage={({ inputValue }) =>
                                    !inputValue
                                      ? this.state.dataxprov
                                      : "Data tidak tersedia"
                                  }
                                  className="form-select-sm selectpicker p-0"
                                  options={this.state.dataxprov}
                                  value={this.state.selprovinsi}
                                  onChange={this.handleChangeProv}
                                />
                                <span style={{ color: "red" }}>
                                  {this.state.errors["provinsi"]}
                                </span>
                              </div>
                              <div className="col-lg-12 mb-7 fv-row">
                                <label className="form-label required">
                                  Kota / Kabupaten
                                </label>
                                <Select
                                  name="kota_kabupaten"
                                  placeholder="Silahkan pilih"
                                  noOptionsMessage={({ inputValue }) =>
                                    !inputValue
                                      ? this.state.dataxkab
                                      : "Data tidak tersedia"
                                  }
                                  className="form-select-sm selectpicker p-0"
                                  options={this.state.dataxkab}
                                  isDisabled={this.state.selprovinsi == null}
                                  value={this.state.selkabupaten}
                                  onChange={this.handleChangeKabupaten}
                                />
                                <span style={{ color: "red" }}>
                                  {this.state.errors["kota_kabupaten"]}
                                </span>
                              </div>
                            </div>
                            <div
                              style={{
                                display: this.state.showingcountry
                                  ? "block"
                                  : "none",
                              }}
                            >
                              <div className="col-lg-12 mb-7 fv-row">
                                <label className="form-label required">
                                  Negara
                                </label>
                                <Select
                                  name="country"
                                  placeholder="Silahkan pilih"
                                  noOptionsMessage={({ inputValue }) =>
                                    !inputValue
                                      ? this.state.dataxcountry
                                      : "Data tidak tersedia"
                                  }
                                  className="form-select-sm selectpicker p-0"
                                  options={this.state.dataxcountry}
                                  value={this.state.selcountry}
                                  onChange={this.handleChangeCountry}
                                />
                                <span style={{ color: "red" }}>
                                  {this.state.errors["country"]}
                                </span>
                              </div>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Alamat Mitra
                              </label>
                              <textarea
                                className="form-control form-control-sm"
                                placeholder="Masukkan Alamat Mitra"
                                name="alamat"
                                defaultValue={this.state.dataxmitra.address}
                              ></textarea>
                              <span style={{ color: "red" }}>
                                {this.state.errors["alamat"]}
                              </span>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Kode Pos
                              </label>
                              <input
                                className="form-control form-control-sm"
                                placeholder="Masukkan Kode Pos"
                                name="kodepos"
                                defaultValue={this.state.dataxmitra.postal_code}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["kodepos"]}
                              </span>
                            </div>
                            <div className="col-lg-12">
                              <div className="mb-7 fv-row">
                                <label className="form-label required">
                                  Logo Mitra
                                </label>
                                <br />
                                <img
                                  style={{ height: "80px", width: "80px" }}
                                  src={this.state.dataxmitra.agency_logo}
                                />
                                <br />
                                <input
                                  type="hidden"
                                  name="upload_logo_hidden"
                                  defaultValue={
                                    this.state.dataxmitra.agency_logo
                                  }
                                />
                                <label className="form-label required">
                                  Logo Mitra
                                </label>
                                <input
                                  type="file"
                                  className="form-control form-control-sm mb-2"
                                  name="upload_logo"
                                  accept=".png,.jpg,.jpeg,.svg"
                                  onChange={this.handleChangeLogo}
                                />
                                <small className="text-muted">
                                  Format File (.png/.jpg/.jpeg/.svg ), Max 10240
                                </small>
                                <br />
                                <span style={{ color: "red" }}>
                                  {this.state.errors["upload_logo"]}
                                </span>
                              </div>
                            </div>
                            <h5 className="mt-7 mb-5">
                              Kontak Person In-Charge (PIC) Mitra
                            </h5>
                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Nama Lengkap PIC
                              </label>
                              <input
                                className="form-control form-control-sm"
                                placeholder="Masukkan Lengkap PIC"
                                name="person_in_charge"
                                defaultValue={this.state.dataxmitra.pic_name}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["person_in_charge"]}
                              </span>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">NIK</label>
                              <input
                                className="form-control form-control-sm"
                                placeholder="Masukkan NIK"
                                name="nik"
                                defaultValue={this.state.dataxmitra.nik}
                                maxLength="16"
                                type="text"
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["nik"]}
                              </span>
                            </div>
                            <div className="col-lg-6 mb-7 fv-row">
                              <label className="form-label required">
                                No. Handphone PIC
                              </label>
                              <input
                                className="form-control form-control-sm"
                                placeholder="Masukkan No.Handphone PIC)"
                                name="phone_in_charge"
                                defaultValue={
                                  this.state.dataxmitra.pic_contact_number
                                }
                                maxLength="13"
                                type="text"
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["phone_in_charge"]}
                              </span>
                            </div>
                            <div className="col-lg-6 mb-7 fv-row">
                              <label className="form-label required">
                                E-mail PIC
                              </label>
                              <input
                                className="form-control form-control-sm"
                                placeholder="Masukkan Email PIC"
                                name="email_in_charge"
                                defaultValue={this.state.dataxmitra.pic_email}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["email_in_charge"]}
                              </span>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Status Mitra
                              </label>
                              <Select
                                name="status"
                                placeholder="Silahkan Pilih Status Mitra"
                                noOptionsMessage={({ inputValue }) =>
                                  !inputValue
                                    ? this.state.datax
                                    : "Data tidak tersedia"
                                }
                                className="form-select-sm selectpicker p-0"
                                options={this.optionstatus}
                                value={this.state.selstatus}
                                onChange={this.handleChangeStatusAction}
                              />
                            </div>
                          </div>

                          <div className="form-group fv-row pt-7 mb-7">
                            <div className="d-flex justify-content-center mb-7">
                              <button
                                onClick={this.handleClickBatal}
                                type="reset"
                                className="btn btn-md btn-light me-3"
                                data-kt-menu-dismiss="true"
                              >
                                Batal
                              </button>
                              <button
                                type="submit"
                                className="btn btn-primary btn-md"
                                id="submitQuestion1"
                                disabled={this.state.isLoading}
                              >
                                {this.state.isLoading ? (
                                  <>
                                    <span
                                      className="spinner-border spinner-border-sm me-2"
                                      role="status"
                                      aria-hidden="true"
                                    ></span>
                                    <span className="sr-only">Loading...</span>
                                    Loading...
                                  </>
                                ) : (
                                  <>
                                    <i className="fa fa-paper-plane me-1"></i>
                                    Simpan
                                  </>
                                )}
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
