import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import Select from "react-select";
import { isValidEmail, isValidUrl } from "../../../utils/commonutil";

export default class TambahContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClickAction.bind(this);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleChangeLogo = this.handleChangeLogoAction.bind(this);
    this.handleChangeProv = this.handleChangeProvAction.bind(this);
    this.handleChangeOrigin = this.handleChangeOriginAction.bind(this);
    this.recheckValidation = this.recheckValidation.bind(this);
    this.handleChangeKab = this.handleChangeKabAction.bind(this);
    this.state = {
      fields: {},
      isLoading: false,
      errors: {},
      dataxprov: [],
      dataxcountry: [],
      isDisabledKab: true,
      showing: true,
      showingcountry: false,
      valueorigin: "",
      wasSubmitted: false,
      selectedProv: null,
      selectedKab: null,
    };
    this.formDatax = new FormData();
  }
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  optionstatus = [
    { value: "1", label: "Aktif" },
    // { value: '0', label: 'Non Aktif' }
  ];

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    const dataProv = { start: 0, rows: 1000 };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/provinsi",
        dataProv,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxprov = [];
        optionx.map((data) =>
          dataxprov.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxprov });
      });
    const dataCountry = { start: 0, rows: 1000 };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/mitra/list-country",
        dataCountry,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxcountry = [];
        optionx.map((data) =>
          dataxcountry.push({ value: data.id, label: data.country_name }),
        );
        this.setState({ dataxcountry });
      });
  }

  validation(dataForm) {
    const acceptedFiles = ["png", "jpg", "jpeg", "svg"];
    let errors = {};
    let formIsValid = true;

    if (dataForm.get("name") == "") {
      formIsValid = false;
      errors["name"] = "Tidak boleh kosong";
    }
    if (dataForm.get("website") == "") {
      formIsValid = false;
      errors["website"] = "Tidak boleh kosong";
    } else {
      if (!isValidUrl(dataForm.get("website"))) {
        formIsValid = false;
        errors["website"] = "URL tidak valid";
      }
    }
    if (dataForm.get("email") == "") {
      formIsValid = false;
      errors["email"] = "Tidak boleh kosong";
    } else if (!isValidEmail(dataForm.get("email"))) {
      formIsValid = false;
      errors["email"] = "Alamat email tidak valid";
    }
    // console.log(dataForm.get("upload_logo"))
    if (dataForm.get("upload_logo").name == "") {
      formIsValid = false;
      errors["upload_logo"] = "Tidak boleh kosong";
    } else {
      let file = dataForm.get("upload_logo");
      const { size, type } = file;
      // console.log(type.split("/")[1])
      if (size / 1024 > 1024) {
        formIsValid = false;
        errors["upload_logo"] = "Ukuran file terlalu besar";
      } else if (!acceptedFiles.includes(type.split("/")[1])) {
        formIsValid = false;
        errors["upload_logo"] = "Ekstensi file tidak valid";
      }
    }
    if (dataForm.get("origin_country") == null) {
      formIsValid = false;
      errors["origin_country"] = "Tidak boleh kosong";
    } else {
      if (dataForm.get("origin_country") == "2") {
        if (dataForm.get("provinsi") == "") {
          formIsValid = false;
          errors["provinsi"] = "Tidak boleh kosong";
        }
        if (dataForm.get("kota_kabupaten") == "") {
          formIsValid = false;
          errors["kota_kabupaten"] = "Tidak boleh kosong";
        }
      } else if (dataForm.get("origin_country") == "1") {
        if (dataForm.get("country") == "") {
          formIsValid = false;
          errors["country"] = "Tidak boleh kosong";
        }
      }
    }
    if (dataForm.get("kodepos") == "") {
      formIsValid = false;
      errors["kodepos"] = "Tidak boleh kosong";
    }
    if (dataForm.get("alamat") == "") {
      formIsValid = false;
      errors["alamat"] = "Tidak boleh kosong";
    }
    if (dataForm.get("person_in_charge") == "") {
      formIsValid = false;
      errors["person_in_charge"] = "Tidak boleh kosong";
    }
    if (dataForm.get("phone_in_charge") == "") {
      formIsValid = false;
      errors["phone_in_charge"] = "Tidak boleh kosong";
    }
    if (dataForm.get("email_in_charge") == "") {
      formIsValid = false;
      errors["email_in_charge"] = "Tidak boleh kosong";
    }
    if (dataForm.get("nik") == "") {
      formIsValid = false;
      errors["nik"] = "Tidak boleh kosong";
    }
    if (dataForm.get("status") === "") {
      formIsValid = false;
      errors["status"] = "Tidak boleh kosong";
    }

    return { errors: errors, formIsValid: formIsValid };
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const { errors, formIsValid } = this.validation(dataForm);

    this.setState({ errors: errors });
    return formIsValid;
  }

  resetError() {
    let errors = {};
    errors[
      ("name",
      "website",
      "email",
      "upload_logo",
      "provinsi",
      "kota_kabupaten",
      "kodepos",
      "alamat",
      "person_in_charge",
      "phone_in_charge",
      "email_in_charge",
      "origin_country",
      "country")
    ] = "";
    this.setState({ errors: errors });
  }

  handleClickAction(e) {
    const dataForm = new FormData(e.currentTarget);
    this.setState({ wasSubmitted: true });
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      const dataFormSubmit = new FormData();
      dataFormSubmit.append("name", dataForm.get("name"));
      dataFormSubmit.append("website", dataForm.get("website"));
      dataFormSubmit.append("email", dataForm.get("email"));
      dataFormSubmit.append("address", dataForm.get("alamat"));
      dataFormSubmit.append("postal_code", dataForm.get("kodepos"));
      dataFormSubmit.append("pic_name", dataForm.get("person_in_charge"));
      dataFormSubmit.append(
        "pic_contact_number",
        dataForm.get("phone_in_charge"),
      );
      dataFormSubmit.append("pic_email", dataForm.get("email_in_charge"));
      dataFormSubmit.append("agency_logo", this.formDatax.get("agency_logo"));
      dataFormSubmit.append("origin_country", dataForm.get("origin_country"));
      if (dataForm.get("origin_country") == "2") {
        dataFormSubmit.append(
          "indonesia_provinces_id",
          dataForm.get("provinsi"),
        );
        dataFormSubmit.append(
          "indonesia_cities_id",
          dataForm.get("kota_kabupaten"),
        );
        dataFormSubmit.append("country_id", "0");
      } else {
        dataFormSubmit.append("indonesia_provinces_id", "0");
        dataFormSubmit.append("indonesia_cities_id", "0");
        dataFormSubmit.append("country_id", dataForm.get("country"));
      }
      dataFormSubmit.append("nik", dataForm.get("nik"));
      dataFormSubmit.append("status", dataForm.get("status"));

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/mitra/tambah-mitra-s3",
          dataFormSubmit,
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/partnership/mitra";
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      this.setState({ isLoading: false });
      swal.fire({
        title: "Periksa Kembali isian anda",
        icon: "error",
        confirmButtonText: "Ok",
      });
      window.scrollTo(0, 0);
    }
  }

  handleClickBatalAction(e) {
    window.location = "/partnership/mitra";
  }

  handleChangeLogoAction(e) {
    const logofile = e.target.files[0];
    let fm = new FormData();
    fm.append("agency_logo", logofile);
    this.formDatax = fm;
  }

  handleChangeProvAction = (provinsi_id) => {
    this.setState({ selectedProv: provinsi_id });
    this.setState({ selectedKab: null });
    this.setState({ errors: { ...this.state.errors, provinsi: "" } });
    console.log(this.state.errors);

    const dataKab = { kdprop: provinsi_id.value };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/kabupaten",
        dataKab,
        this.configs,
      )
      .then((res) => {
        this.setState({ isDisabledKab: false });
        const options = res.data.result.Data;
        const dataxkab = [];
        options.map((data) =>
          dataxkab.push({ value: data.id, label: data.name }),
        );
        this.setState({ dataxkab });
      });
    if (this.state.wasSubmitted) this.recheckValidation();
  };

  handleChangeKabAction = (provinsi_id) => {
    this.setState({ errors: { ...this.state.errors, kota_kabupaten: "" } });
    this.setState({ selectedKab: provinsi_id });
    if (this.state.wasSubmitted) this.recheckValidation();
  };

  handleChangeOriginAction(e) {
    this.setState({ valueorigin: e.target.value });
    if (e.target.value == "2") {
      this.setState({ showing: true });
      this.setState({ showingcountry: false });
    } else {
      this.setState({ showing: false });
      this.setState({ showingcountry: true });
    }
  }

  recheckValidation() {
    if (!this.state.wasSubmitted) return;
    const el = document.getElementById("form-tambah-mitra");
    const form = new FormData(el);
    // console.log(this)
    const { errors, formIsValid } = this.validation(form);
    this.setState({ errors: errors });
    // return formIsValid;
  }

  render() {
    return (
      <div>
        <input
          type="hidden"
          name="csrf-token"
          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
        />
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z"
                        fill="#FFC700"
                      ></path>
                      <path
                        d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z"
                        fill="#FFC700"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Partnership
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Mitra
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Tambah Mitra
                          </h1>
                        </div>
                      </div>
                      <div className="card-body ">
                        <form
                          id="form-tambah-mitra"
                          onChange={this.recheckValidation}
                          className="form"
                          action="#"
                          onSubmit={this.handleClick}
                        >
                          <div className="row">
                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Nama Mitra
                              </label>
                              <input
                                className="form-control form-control-sm"
                                placeholder="Masukan Nama Mitra"
                                name="name"
                                value={this.state.fields["name"]}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["name"]}
                              </span>
                            </div>
                            <div className="col-lg-6 mb-7 fv-row">
                              <label className="form-label required">
                                Website Resmi
                              </label>
                              <input
                                className="form-control form-control-sm"
                                placeholder="Masukkan website"
                                name="website"
                                value={this.state.fields["website"]}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["website"]}
                              </span>
                            </div>
                            <div className="col-lg-6 mb-7 fv-row">
                              <label className="form-label required">
                                Email Resmi
                              </label>
                              <input
                                className="form-control form-control-sm"
                                placeholder="Masukkan Email Resmi"
                                name="email"
                                value={this.state.fields["email"]}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["email"]}
                              </span>
                            </div>

                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Asal Mitra
                              </label>
                              <div className="d-flex">
                                <div className="form-check form-check-sm form-check-custom form-check-solid me-5">
                                  <input
                                    className="form-check-input"
                                    type="radio"
                                    name="origin_country"
                                    value="2"
                                    id="origin_country1"
                                    onClick={this.handleChangeOrigin}
                                  />
                                  <label
                                    className="form-check-label"
                                    for="origin_country1"
                                  >
                                    Dalam Negeri
                                  </label>
                                </div>
                                <div className="form-check form-check-sm form-check-custom form-check-solid">
                                  <input
                                    className="form-check-input"
                                    type="radio"
                                    name="origin_country"
                                    value="1"
                                    id="origin_country2"
                                    onClick={this.handleChangeOrigin}
                                  />
                                  <label
                                    className="form-check-label"
                                    for="origin_country2"
                                  >
                                    Luar Negeri
                                  </label>
                                </div>
                              </div>
                              <span style={{ color: "red" }}>
                                {this.state.errors["origin_country"]}
                              </span>
                            </div>
                            {this.state.valueorigin == 2 && (
                              <div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Provinsi
                                  </label>
                                  <Select
                                    name="provinsi"
                                    placeholder="Silahkan pilih"
                                    noOptionsMessage={({ inputValue }) =>
                                      !inputValue
                                        ? this.state.dataxprov
                                        : "Data tidak tersedia"
                                    }
                                    className="form-select-sm selectpicker p-0"
                                    value={this.state.selectedProv}
                                    options={this.state.dataxprov}
                                    onChange={this.handleChangeProv}
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["provinsi"]}
                                  </span>
                                </div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Kota / Kabupaten
                                  </label>
                                  <Select
                                    name="kota_kabupaten"
                                    placeholder="Silahkan pilih"
                                    noOptionsMessage={({ inputValue }) =>
                                      !inputValue
                                        ? this.state.dataxkab
                                        : "Data tidak tersedia"
                                    }
                                    className="form-select-sm selectpicker p-0"
                                    options={this.state.dataxkab}
                                    onChange={this.handleChangeKab}
                                    value={this.state.selectedKab}
                                    isDisabled={this.state.selectedProv == null}
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["kota_kabupaten"]}
                                  </span>
                                </div>
                              </div>
                            )}

                            {this.state.valueorigin == 1 && (
                              <div>
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label required">
                                    Negara
                                  </label>
                                  <Select
                                    name="country"
                                    placeholder="Silahkan pilih"
                                    noOptionsMessage={({ inputValue }) =>
                                      !inputValue
                                        ? this.state.dataxcountry
                                        : "Data tidak tersedia"
                                    }
                                    className="form-select-sm selectpicker p-0"
                                    options={this.state.dataxcountry}
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors["country"]}
                                  </span>
                                </div>
                              </div>
                            )}
                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Alamat
                              </label>
                              <textarea
                                className="form-control form-control-sm"
                                placeholder="Masukkan Alamat Mitra"
                                name="alamat"
                                value={this.state.fields["deskripsi"]}
                              ></textarea>
                              <span style={{ color: "red" }}>
                                {this.state.errors["alamat"]}
                              </span>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Kode Pos
                              </label>
                              <input
                                className="form-control form-control-sm"
                                placeholder="Masukkan Kode Pos"
                                name="kodepos"
                                value={this.state.fields["kodepos"]}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["kodepos"]}
                              </span>
                            </div>
                            <div className="col-lg-12">
                              <div className="mb-7 fv-row">
                                <label className="form-label required">
                                  Logo Mitra
                                </label>
                                <input
                                  type="file"
                                  className="form-control form-control-sm mb-2"
                                  name="upload_logo"
                                  accept=".png,.jpg,.jpeg,.svg"
                                  onChange={this.handleChangeLogo}
                                />
                                <small className="text-muted">
                                  Format File (.png/.jpg/.jpeg/.svg ), Max
                                  1024KB
                                </small>
                                <br />
                                <span style={{ color: "red" }}>
                                  {this.state.errors["upload_logo"]}
                                </span>
                              </div>
                            </div>
                            <h5 className="mt-7 mb-5">
                              Kontak Person In-Charge (PIC) Mitra
                            </h5>
                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">
                                Nama Lengkap PIC
                              </label>
                              <input
                                className="form-control form-control-sm"
                                placeholder="Masukkan Nama Lengkap PIC"
                                name="person_in_charge"
                                value={this.state.fields["person_in_charge"]}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["person_in_charge"]}
                              </span>
                            </div>
                            <div className="col-lg-12 mb-7 fv-row">
                              <label className="form-label required">NIK</label>
                              <input
                                className="form-control form-control-sm"
                                placeholder="Masukkan NIK"
                                name="nik"
                                value={this.state.fields["nik"]}
                                maxlength="16"
                                type="text"
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["nik"]}
                              </span>
                            </div>
                            <div className="col-lg-6 mb-7 fv-row">
                              <label className="form-label required">
                                No. Handphone PIC
                              </label>
                              <input
                                className="form-control form-control-sm"
                                placeholder="Masukkan No.Handphone PIC"
                                name="phone_in_charge"
                                value={this.state.fields["phone_in_charge"]}
                                maxlength="13"
                                type="text"
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["phone_in_charge"]}
                              </span>
                            </div>
                            <div className="col-lg-6 mb-7 fv-row">
                              <label className="form-label required">
                                E-mail PIC
                              </label>
                              <input
                                className="form-control form-control-sm"
                                placeholder="Masukkan Email PIC"
                                name="email_in_charge"
                                value={this.state.fields["email_in_charge"]}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["email_in_charge"]}
                              </span>
                            </div>
                            <div className="col-lg-12 fv-row">
                              <label className="form-label"></label>
                              <input
                                type={"hidden"}
                                name="status"
                                value={1}
                              ></input>
                            </div>
                            <div className="form-group fv-row pt-7 mb-7">
                              <div className="d-flex justify-content-center mb-7">
                                <button
                                  onClick={this.handleClickBatal}
                                  type="reset"
                                  className="btn btn-md btn-light me-3"
                                  data-kt-menu-dismiss="true"
                                >
                                  Batal
                                </button>
                                <button
                                  type="submit"
                                  className="btn btn-primary btn-md"
                                  id="submitQuestion1"
                                  disabled={this.state.isLoading}
                                >
                                  {this.state.isLoading ? (
                                    <>
                                      <span
                                        className="spinner-border spinner-border-sm me-2"
                                        role="status"
                                        aria-hidden="true"
                                      ></span>
                                      <span className="sr-only">
                                        Loading...
                                      </span>
                                      Loading...
                                    </>
                                  ) : (
                                    <>
                                      <i className="fa fa-paper-plane me-1"></i>
                                      Simpan
                                    </>
                                  )}
                                </button>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
