import React, { useEffect, useMemo, useState } from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";

function CustomDataTable({
  idmitra,
  type = "",
  coulumnsData = [],
  urlApi = "",
  axiosConf,
}) {
  const [data, setData] = useState([]);
  const [detailPage, setDetailPage] = useState({ page: 0, perPage: 10000 });
  const [totalData, setTotalData] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [idMitra, setIdMitra] = useState();

  const [activePaging, setActivePaging] = useState({ page: 1, perPage: 10 });

  const customLoader = useMemo(
    () => (
      <div style={{ padding: "24px" }}>
        <img src="/assets/media/loader/loader-biru.gif" />
      </div>
    ),
    [],
  );

  const coulumns = [
    {
      name: "No",
      center: true,
      width: "70px",
      cell: (row, index) => (
        <div>
          <span>
            {index + 1 + (activePaging.page - 1) * activePaging.perPage}
          </span>
        </div>
      ),
    },
    ...coulumnsData,
  ];
  // console.log(col)
  useEffect(() => {
    if (idmitra) {
      if (idmitra != idMitra) {
        // console.log(idmitra, idMitra)
        setIdMitra(idmitra);
      }
    }
  }, [idmitra]);

  useEffect(() => {
    if (idmitra && urlApi && coulumns && type) {
      getData();
    }
  }, [detailPage, idMitra]);

  const getData = () => {
    setIsLoading(true);
    const params = {
      id: idmitra,
      start: detailPage.page * detailPage.perPage,
      length: detailPage.perPage,
    };

    // let df = new FormData()
    // Object.keys(params).forEach((k) => { df.append(k, params[k]) })

    axios
      .post(process.env.REACT_APP_BASE_API_URI + urlApi, params, axiosConf)
      .then((res) => {
        // console.log(res)
        if (type == "kerjasama") {
          if (res.data.result.Status) {
            // console.log("data", res.data.result.Kerjasama)
            setData(res.data.result.Kerjasama);
          }
        } else if (type == "tema") {
          if (res.data.result.Status) {
            // console.log("data", res.data.result.Kerjasama)
            setData(res.data.result.Tema);
          }
        } else if (type == "pelatihan") {
          if (res.data.result.Status) {
            // console.log("data", res.data.result.Kerjasama)
            setData(res.data.result.pelatihan);
          }
        }
      })
      .catch((er) => {
        console.log(er);
      })
      .finally(function () {
        setIsLoading(false);
      });
  };

  const handlePerRowChange = (newPerPage, page) => {};

  const handlePageChange = (page) => {
    console.log(page);
    setActivePaging((p) => ({ ...p, page: page }));
  };

  return (
    <DataTable
      columns={coulumns}
      data={data}
      progressPending={isLoading}
      progressComponent={customLoader}
      highlightOnHover
      pointerOnHover
      pagination
      // onChangeRowsPerPage={handlePerRowChange}
      onChangePage={handlePageChange}
      defaultSortAsc={true}
      persistTableHead={true}
      paginationPerPage={activePaging.perPage}
      // sortServer
      // paginationServer
      // paginationTotalRows={totalData}
      noDataComponent={<div className="mt-5">Tidak Ada Data</div>}
    ></DataTable>
  );
}

export default class MitraViewContent extends React.Component {
  constructor(props) {
    super(props);
    // Cookies.remove("mitra_id");
    // Cookies.remove("user_id");
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.state = {
      fields: {},
      errors: {},
      datax: [],
      dataxtema: [],
      dataxkerjasama: [],
      dataxpelatihan: [],
      totalRowsTema: 0,
      totalRowsPelatihan: 0,
      totalRowsKerjasama: 0,
      numberrowtema: 1,
      numberrowpelatihan: 1,
      newPerPageTema: 10,
      newPerPagePelatihan: 10,
      newPerPageKerjasama: 10,
      dataxmitra: [],
      dataxprov: [],
      dataxcountry: [],
      selprovinsi: [],
      selkabupaten: [],
      selcountry: [],
      isDisabledKab: true,
      showing: true,
      showingcountry: false,
      valueorigin: "",
      idMitra: "",
    };
    this.formDatax = new FormData();
  }
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  customLoader = (
    <div style={{ padding: "24px" }}>
      <img src="/assets/media/loader/loader-biru.gif" />
    </div>
  );
  columnstema = [
    {
      name: "Tema",
      sortable: true,
      selector: (row) => row.name,
      cell: (row) => (
        <div>
          <span
            style={{
              overflow: "hidden",
              whiteSpace: "wrap",
              textOverflow: "ellipses",
            }}
          >
            {row.name}
          </span>
        </div>
      ),
    },
    {
      name: "Status",
      center: true,
      sortable: true,
      selector: (row) => row.status,
      cell: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.status == "publish" ? "success" : "danger") +
              " fs-7 m-1"
            }
          >
            {row.status == "publish" ? "Publish" : "Unpublish"}
          </span>
        </div>
      ),
    },
    {
      name: "Aksi",
      center: true,
      cell: (row) => (
        <div>
          <a
            href={"/pelatihan/view-tema/" + row.tema_id}
            id={row.tema_id}
            title="View"
            className="btn btn-icon btn-active-light-primary w-30px h-30px me-3"
          >
            <span className="svg-icon svg-icon-3">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width={24}
                height={24}
                viewBox="0 0 24 24"
                fill="none"
              >
                <path
                  d="M21.7 18.9L18.6 15.8C17.9 16.9 16.9 17.9 15.8 18.6L18.9 21.7C19.3 22.1 19.9 22.1 20.3 21.7L21.7 20.3C22.1 19.9 22.1 19.3 21.7 18.9Z"
                  fill="currentColor"
                />
                <path
                  opacity="0.3"
                  d="M11 20C6 20 2 16 2 11C2 6 6 2 11 2C16 2 20 6 20 11C20 16 16 20 11 20ZM11 4C7.1 4 4 7.1 4 11C4 14.9 7.1 18 11 18C14.9 18 18 14.9 18 11C18 7.1 14.9 4 11 4ZM8 11C8 9.3 9.3 8 11 8C11.6 8 12 7.6 12 7C12 6.4 11.6 6 11 6C8.2 6 6 8.2 6 11C6 11.6 6.4 12 7 12C7.6 12 8 11.6 8 11Z"
                  fill="currentColor"
                />
              </svg>
            </span>
          </a>
        </div>
      ),
    },
  ];
  columnskerjasama = [
    {
      name: "MoU/PKS",
      sortable: true,
      selector: (row) => row.title,
      cell: (row) => (
        <div>
          <span
            style={{
              overflow: "hidden",
              whiteSpace: "wrap",
              textOverflow: "ellipses",
            }}
          >
            {row.title}
          </span>
        </div>
      ),
    },
    {
      name: "Status",
      center: true,
      sortable: true,
      selector: (row) => row.status,
      cell: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.status == "Aktif" ? "success" : "danger") +
              " fs-7 m-1"
            }
          >
            {row.status}
          </span>
        </div>
      ),
    },
    {
      name: "Aksi",
      center: true,
      cell: (row) => (
        <div>
          <a
            href={"/partnership/kerjasama/review/" + row.id}
            id={row.id}
            title="View"
            className="btn btn-icon btn-active-light-primary w-30px h-30px me-3"
          >
            <span className="svg-icon svg-icon-3">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width={24}
                height={24}
                viewBox="0 0 24 24"
                fill="none"
              >
                <path
                  d="M21.7 18.9L18.6 15.8C17.9 16.9 16.9 17.9 15.8 18.6L18.9 21.7C19.3 22.1 19.9 22.1 20.3 21.7L21.7 20.3C22.1 19.9 22.1 19.3 21.7 18.9Z"
                  fill="currentColor"
                />
                <path
                  opacity="0.3"
                  d="M11 20C6 20 2 16 2 11C2 6 6 2 11 2C16 2 20 6 20 11C20 16 16 20 11 20ZM11 4C7.1 4 4 7.1 4 11C4 14.9 7.1 18 11 18C14.9 18 18 14.9 18 11C18 7.1 14.9 4 11 4ZM8 11C8 9.3 9.3 8 11 8C11.6 8 12 7.6 12 7C12 6.4 11.6 6 11 6C8.2 6 6 8.2 6 11C6 11.6 6.4 12 7 12C7.6 12 8 11.6 8 11Z"
                  fill="currentColor"
                />
              </svg>
            </span>
          </a>
        </div>
      ),
    },
  ];
  columnspelatihan = [
    {
      name: "Pelatihan",
      sortable: true,
      selector: (row) => row.pelatihan,
      cell: (row) => (
        <div>
          <span
            style={{
              overflow: "hidden",
              whiteSpace: "wrap",
              textOverflow: "ellipses",
            }}
          >
            {row.pelatihan}
          </span>
        </div>
      ),
    },
    {
      name: "Status Substansi",
      center: true,
      sortable: true,
      selector: (row) => row.status_substansi,
      cell: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.status_substansi == "Disetujui" ? "success" : "danger") +
              " fs-7 m-1"
            }
          >
            {row.status_substansi}
          </span>
        </div>
      ),
    },
    {
      name: "Status Pelatihan",
      center: true,
      sortable: true,
      selector: (row) => row.status_pelatihan,
      cell: (row) => (
        <div>
          <span className={"badge badge-light-primary fs-7 m-1"}>
            {row.status_pelatihan}
          </span>
        </div>
      ),
    },
    {
      name: "Aksi",
      center: true,
      cell: (row) => (
        <div>
          <a
            href={"/pelatihan/view-pelatihan/" + row.id_pelatihan}
            id={row.id_pelatihan}
            title="View"
            className="btn btn-icon btn-active-light-primary w-30px h-30px me-3"
          >
            <span className="svg-icon svg-icon-3">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width={24}
                height={24}
                viewBox="0 0 24 24"
                fill="none"
              >
                <path
                  d="M21.7 18.9L18.6 15.8C17.9 16.9 16.9 17.9 15.8 18.6L18.9 21.7C19.3 22.1 19.9 22.1 20.3 21.7L21.7 20.3C22.1 19.9 22.1 19.3 21.7 18.9Z"
                  fill="currentColor"
                />
                <path
                  opacity="0.3"
                  d="M11 20C6 20 2 16 2 11C2 6 6 2 11 2C16 2 20 6 20 11C20 16 16 20 11 20ZM11 4C7.1 4 4 7.1 4 11C4 14.9 7.1 18 11 18C14.9 18 18 14.9 18 11C18 7.1 14.9 4 11 4ZM8 11C8 9.3 9.3 8 11 8C11.6 8 12 7.6 12 7C12 6.4 11.6 6 11 6C8.2 6 6 8.2 6 11C6 11.6 6.4 12 7 12C7.6 12 8 11.6 8 11Z"
                  fill="currentColor"
                />
              </svg>
            </span>
          </a>
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };
  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    let segment_url = window.location.pathname.split("/");
    let urlSegmenttOne = segment_url[3];
    Cookies.set("mitra_id", urlSegmenttOne);
    this.setState({ idMitra: urlSegmenttOne });
    let data = {
      id: urlSegmenttOne,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/mitra/cari-mitra",
        data,
        this.configs,
      )
      .then((res) => {
        const dataxmitra = res.data.result.Data[0];
        this.setState({ dataxmitra });
        Cookies.set("user_id", this.state.dataxmitra.user_id);

        const dataProv = { start: 0, rows: 1000 };
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/provinsi",
            dataProv,
            this.configs,
          )
          .then((res) => {
            const optionx = res.data.result.Data;
            const dataxprov = [];
            for (let i in optionx) {
              if (
                optionx[i].id == this.state.dataxmitra.indonesia_provinces_id
              ) {
                this.setState({
                  selprovinsi: { value: optionx[i].id, label: optionx[i].name },
                });
              }
            }
            optionx.map((data) =>
              dataxprov.push({ value: data.id, label: data.name }),
            );
            this.setState({ dataxprov });
          });

        if (this.state.dataxmitra.indonesia_provinces_id != "0") {
          const dataKab = {
            kdprop: this.state.dataxmitra.indonesia_provinces_id,
          };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI + "/kabupaten",
              dataKab,
              this.configs,
            )
            .then((res) => {
              this.setState({ isDisabledKab: false });
              const options = res.data.result.Data;
              const dataxkab = [];
              for (let i in options) {
                if (
                  options[i].id == this.state.dataxmitra.indonesia_cities_id
                ) {
                  this.setState({
                    selkabupaten: {
                      value: options[i].id,
                      label: options[i].name,
                    },
                  });
                }
              }
              options.map((data) =>
                dataxkab.push({ value: data.id, label: data.name }),
              );
              this.setState({ dataxkab });
            });
        }

        const dataCountry = { start: 0, rows: 1000 };
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/mitra/list-country",
            dataCountry,
            this.configs,
          )
          .then((res) => {
            const optionx = res.data.result.Data;
            const dataxcountry = [];
            for (let i in optionx) {
              if (optionx[i].id == this.state.dataxmitra.country_id) {
                this.setState({
                  selcountry: {
                    value: optionx[i].id,
                    label: optionx[i].country_name,
                  },
                });
              }
            }
            optionx.map((data) =>
              dataxcountry.push({ value: data.id, label: data.country_name }),
            );
            this.setState({ dataxcountry });
          });

        //initial awal
        if (this.state.dataxmitra.origin_country == "1") {
          this.setState({ showing: false });
          this.setState({ showingcountry: true });
        }

        // this.handleReload("tema", 1, 10);
        // this.handleReload("kerjasama", 1, 10);
        // this.handleReload("pelatihan", 1, 10);
      });
  }
  handleClickBatalAction(e) {
    window.location = "/partnership/mitra";
  }
  // handleReload(param, page, newPerPage) {
  //   let segment_url = window.location.pathname.split('/');
  //   let urlSegmenttOne = segment_url[3];
  //   this.setState({ loading: true });
  //   let start_tmp = 0;
  //   let length_tmp = (newPerPage != undefined ? newPerPage : 10);
  //   if (page != 1 && page != undefined) {
  //     start_tmp = newPerPage * page;
  //     start_tmp = (start_tmp - length_tmp);
  //   }
  //   let dataReqDetailView = {
  //     id: urlSegmenttOne,
  //     start: start_tmp,
  //     length: length_tmp
  //   }
  //   if ("tema" == param) {
  //     axios.post(process.env.REACT_APP_BASE_API_URI + '/mitra/detail-mitra-tema-paging', dataReqDetailView, this.configs)
  //       .then(res => {
  //         const dataxtema = res.data.result.Tema;
  //         this.setState({ dataxtema });
  //         this.setState({ loading: false });
  //         this.setState({ totalRowsTema: res.data.result.Total });
  //       });
  //   } else if ("kerjasama" == param) {
  //     axios.post(process.env.REACT_APP_BASE_API_URI + '/mitra/detail-mitra-kerjasama-paging', dataReqDetailView, this.configs)
  //       .then(res => {
  //         const dataxkerjasama = res.data.result.Kerjasama;
  //         this.setState({ dataxkerjasama });
  //         this.setState({ loading: false });
  //         this.setState({ totalRowsKerjasama: res.data.result.Total });
  //       });
  //   } else if ("pelatihan" == param) {
  //     axios.post(process.env.REACT_APP_BASE_API_URI + '/mitra/detail-mitra-pelatihan-paging', dataReqDetailView, this.configs)
  //       .then(res => {
  //         const dataxpelatihan = res.data.result.pelatihan;
  //         this.setState({ dataxpelatihan });
  //         this.setState({ loading: false });
  //         this.setState({ totalRowsPelatihan: res.data.result.Total });
  //       });
  //   }
  // }
  // handlePageChangeTema = page => {
  //   console.log("handlePageChange Tema");
  //   this.handleReload("tema", page, this.state.newPerPageTema);
  // };
  // handlePerRowsChangeTema = async (newPerPage, page) => {
  //   console.log("handlePerRowsChange Tema");
  //   this.setState({ newPerPageTema: newPerPage });
  //   this.handleReload("tema", page, newPerPage);
  // }
  // handlePageChangeKerjasama = page => {
  //   console.log("handlePageChange Kerjasama");
  //   this.handleReload("kerjasama", page, this.state.newPerPageKerjasama);
  // };
  // handlePerRowsChangeKerjasama = async (newPerPage, page) => {
  //   console.log("handlePerRowsChange Kerjasama");
  //   this.setState({ newPerPageKerjasama: newPerPage });
  //   this.handleReload("kerjasama", page, newPerPage);
  // }
  // handlePageChangePelatihan = page => {
  //   console.log("handlePageChange Pelatihan");
  //   this.handleReload("pelatihan", page, this.state.newPerPagePelatihan);
  // };
  // handlePerRowsChangePelatihan = async (newPerPage, page) => {
  //   console.log("handlePerRowsChange Pelatihan");
  //   this.setState({ newPerPagePelatihan: newPerPage });
  //   this.handleReload("pelatihan", page, newPerPage);
  // }
  render() {
    return (
      <div>
        <input
          type="hidden"
          name="csrf-token"
          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
        />
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z"
                        fill="#FFC700"
                      ></path>
                      <path
                        d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z"
                        fill="#FFC700"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Partnership
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Mitra
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500">
                    <i className="fa fa-chevron-left pe-1"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>

                <a
                  href={"/partnership/edit-mitra/" + this.state.dataxmitra.id}
                  className="btn btn-warning fw-bolder btn-sm me-2"
                >
                  <i className="bi bi-gear-fill"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Edit
                  </span>
                </a>

                <button className="btn btn-sm btn-danger btn-active-light-info">
                  <i className="bi bi-trash-fill text-white pe-1"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Detail Mitra
                          </h1>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="mb-7">
                          <div className="d-flex flex-wrap py-3">
                            <div className="symbol symbol-100px symbol-lg-120px">
                              <img
                                src={this.state.dataxmitra.agency_logo}
                                alt=""
                                className="img-fluid me-4"
                              />
                            </div>
                            <div className="flex-grow-1 mb-3">
                              <div className="d-flex justify-content-between align-items-start flex-wrap pt-6">
                                <div className="d-flex flex-column">
                                  <div className="d-flex align-items-center mb-2">
                                    <h5 className="fw-bolder mb-0 fs-5">
                                      {this.state.dataxmitra.nama_mitra}
                                    </h5>
                                  </div>
                                  <div className="d-flex flex-wrap fw-bold fs-6 pe-2">
                                    <span className="text-muted fs-7">
                                      <i className="fa fa-link me-1"></i>
                                      {this.state.dataxmitra.website}
                                    </span>
                                  </div>
                                  <div className="d-flex flex-wrap fw-bold fs-6 pe-2">
                                    <span className="text-muted fs-7">
                                      <i className="fa fa-envelope me-1"></i>
                                      {this.state.dataxmitra.email}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="d-flex border-bottom p-0">
                          <ul className="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap">
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary active"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_1"
                              >
                                <span className="fs-6">Informasi</span>
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary false"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_2"
                              >
                                <span className="fs-6">Kerjasama</span>
                              </a>
                            </li>
                            <li className="nav-item">
                              <a
                                className="nav-link text-dark text-active-primary false"
                                data-bs-toggle="tab"
                                href="#kt_tab_pane_3"
                              >
                                <span className="fs-6">Pelatihan</span>
                              </a>
                            </li>
                          </ul>
                        </div>
                        <div className="tab-content" id="detail-mitra">
                          <div
                            className="tab-pane fade show active"
                            id="kt_tab_pane_1"
                            role="tabpanel"
                          >
                            <div className="row mt-7">
                              <div className="col-lg-12 mb-7">
                                <label className="form-label">Asal Mitra</label>
                                <div className="d-flex">
                                  {this.state.dataxmitra.origin_country ==
                                  "2" ? (
                                    <b>Dalam Negeri</b>
                                  ) : (
                                    <b>Luar Negeri</b>
                                  )}
                                </div>
                              </div>
                              <div className="col=lg-12 mb-7">
                                <label className="form-label">
                                  Alamat Mitra
                                </label>
                                <div className="d-flex">
                                  <b>{this.state.dataxmitra.address}</b>
                                </div>
                              </div>
                              {this.state.dataxmitra.origin_country == "2" ? (
                                <div className="col-lg-4 mb-7">
                                  <label className="form-label">Provinsi</label>
                                  <div className="d-flex">
                                    <b>{this.state.selprovinsi.label}</b>
                                  </div>
                                </div>
                              ) : (
                                <div className="col-6 col-md-6 mb-7">
                                  <label className="form-label">Negara</label>
                                  <div className="d-flex">
                                    <b>{this.state.selcountry.label}</b>
                                  </div>
                                </div>
                              )}
                              {this.state.dataxmitra.origin_country == "2" ? (
                                <div className="col-lg-4 mb-7">
                                  <label className="form-label">
                                    Kota / Kabupaten
                                  </label>
                                  <div className="d-flex">
                                    <b>{this.state.selkabupaten.label}</b>
                                  </div>
                                </div>
                              ) : (
                                ""
                              )}
                              <div className="col-lg-4 mb-7">
                                <label className="form-label">Kodepos</label>
                                <div className="d-flex">
                                  <b>{this.state.dataxmitra.postal_code}</b>
                                </div>
                              </div>
                              <div>
                                <div className="mt-5 border-top mx-0"></div>
                              </div>
                              <h6 className="text-muted mt-7 mb-5">
                                Kontak Person In-Charge (PIC) Mitra
                              </h6>
                              <div className="col-12 col-md-6 mb-7">
                                <label className="form-label">
                                  Nama Lengkap PIC
                                </label>
                                <div className="d-flex">
                                  <b>{this.state.dataxmitra.pic_name}</b>
                                </div>
                              </div>
                              <div className="col-12 col-md-6 mb-7">
                                <label className="form-label">NIK</label>
                                <div className="d-flex">
                                  <b>{this.state.dataxmitra.nik}</b>
                                </div>
                              </div>
                              <div className="col-12 col-md-6 mb-7">
                                <label className="form-label">
                                  No. Handphone PIC
                                </label>
                                <div className="d-flex">
                                  <b>
                                    {this.state.dataxmitra.pic_contact_number}
                                  </b>
                                </div>
                              </div>
                              <div className="col-12 col-md-6 mb-7">
                                <label className="form-label">E-mail PIC</label>
                                <div className="d-flex">
                                  <b>{this.state.dataxmitra.pic_email}</b>
                                </div>
                              </div>
                              <div className="col-12 col-lg-12 mb-7">
                                <label className="form-label">
                                  Status Mitra
                                </label>
                                <div className="d-flex">
                                  <b
                                    className={`badge badge-light-${
                                      this.state.dataxmitra.status
                                        ? "success"
                                        : "danger"
                                    } fs-7 `}
                                  >
                                    {this.state.dataxmitra.status}
                                  </b>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div
                            className="tab-pane fade show"
                            id="kt_tab_pane_2"
                            role="tabpanel"
                          >
                            <div className="mt-7">
                              <h5 className="mb-5 d-md-none d-lg-none d-xl-none d-block">
                                Riwayat Kerjasama
                              </h5>
                              <div className="col-12 mb-7">
                                <CustomDataTable
                                  idmitra={this.state.idMitra}
                                  axiosConf={this.configs}
                                  coulumnsData={this.columnskerjasama}
                                  urlApi="/mitra/detail-mitra-kerjasama-paging"
                                  type="kerjasama"
                                ></CustomDataTable>
                                {/* <DataTable
                        columns={this.columnskerjasama}
                        data={this.state.dataxkerjasama}
                        progressPending={this.state.loading}
                        progressComponent={this.customLoader}
                        highlightOnHover
                        pointerOnHover
                        pagination
                        paginationServer
                        paginationTotalRows={this.state.totalRowsKerjasama}
                        onChangeRowsPerPage={this.handlePerRowsChangeKerjasama}
                        onChangePage={this.handlePageChangeKerjasama}
                        defaultSortAsc={true}
                        persistTableHead={true}
                        sortServer
                        noDataComponent={<div className="mt-5">Tidak Ada Data</div>}
                  />*/}
                              </div>
                            </div>
                          </div>
                          <div
                            className="tab-pane fade show"
                            id="kt_tab_pane_3"
                            role="tabpanel"
                          >
                            <div className="mt-7">
                              <h5 className="mb-5 d-md-none d-lg-none d-xl-none d-block">
                                Pelatihan
                              </h5>
                              <div className="col-12 mb-7">
                                <CustomDataTable
                                  idmitra={this.state.idMitra}
                                  axiosConf={this.configs}
                                  coulumnsData={this.columnspelatihan}
                                  urlApi="/mitra/detail-mitra-pelatihan-paging"
                                  type="pelatihan"
                                ></CustomDataTable>
                                {/* <DataTable
                        columns={this.columnspelatihan}
                        data={this.state.dataxpelatihan}
                        progressPending={this.state.loading}
                        progressComponent={this.customLoader}
                        highlightOnHover
                        pointerOnHover
                        pagination
                        paginationServer
                        paginationTotalRows={this.state.totalRowsPelatihan}
                        onChangeRowsPerPage={this.handlePerRowsChangePelatihan}
                        onChangePage={this.handlePageChangePelatihan}
                        defaultSortAsc={true}
                        persistTableHead={true}
                        sortServer
                        noDataComponent={<div className="mt-5">Tidak Ada Data</div>}
                      /> */}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
