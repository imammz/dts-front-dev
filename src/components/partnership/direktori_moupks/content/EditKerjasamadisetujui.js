import React, { useState, useEffect, useRef } from "react";
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import Header from "../../../../components/Header";
import SideNav from "../../../../components/SideNav";
import Footer from "../../../../components/Footer";
import Select from "react-select";
import Cookies from "js-cookie";
import moment from "moment";
import Moment from "react-moment";
import {
  convertPeriodeKerjasama,
  isJsonString,
} from "../../../../utils/commonutil";

const EditKerjasamadisetujui = (props) => {
  Cookies.remove("pelatian_id");
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  const [optSet, setOpt] = useState(0);
  const [dataForm, setDataForm] = useState([]);
  const [dataFormPost, setDataFormPost] = useState([]);
  const [lembaga_error, setLembaga_error] = useState("");
  const [periode_error, setPeriode_error] = useState("");
  const [judul_error, setJudul_error] = useState("");
  const [cooperation_category_id_error, setCooperation_category_id_error] =
    useState("");
  const [status_error, setStatus_error] = useState("");
  const [stepperFunc, setStepperFunc] = useState(null);
  const [wizPos, setWizPos] = useState(0);
  const [isDisabledNext, setIsDisabledNext] = useState(false);
  const [error_tgl_awal, setErrorTglAwal] = useState("");
  const [error_tgl_akhir, setErrorTglAkhir] = useState("");
  const [error_nomor_mou_pks, setErrorNomorMouPks] = useState("");
  const [error_tgl_penandatanganan, setErrorTanggalPendandatanganan] =
    useState("");
  const [error_file, setErrorFile] = useState("");
  const [cooperation_category_val, setCooperation_category_val] = useState([]);
  const [kategori, setKategori] = useState("");
  const [status_val, setStatusVal] = useState("");
  const [status_id, setStatusId] = useState("");
  const [is_update_pic, setUpdatePic] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const history = useNavigate();
  const params = useParams();

  const initialAkademiState = {
    tanggal: moment().locale("ID").format("DD MMMM YYYY, HH:mm"),
    lembaga: null,
    email: null,
    periode: null,
    judul: null,
    kategori: null,
    deskripsi: null,
    deskripsi2: null,
    tujuan: null,
    selectedFileIcon: null,
    kerjasama_awal: "",
    kerjasama_akhir: null,
    nope_lembaga: null,
    nope_kekom: null,
    nomor_mou_pks: null,
    kerjasama_ttd: null,
    cooperation_category_id: null,
    unggah_file_final: null,
  };

  const handleChangeIpt = (status_id) => {
    let dataxstatusvalue = status_id;
    setStatusVal({
      label: dataxstatusvalue.label,
      value: dataxstatusvalue.value,
    });
    setStatusId(dataxstatusvalue.value);
  };

  const handleChangecooperation_category_id = (e) => {
    if (e.value) {
      setIsDisabledNext(true);
      setDataForm([]);
      setDataFormPost([]);
      let dataxstatusvalue = e.value;
      akademi.cooperation_category_id = e.value;
      setCooperation_category_val({ label: e.label, value: e.value });
      //axios.defaults.headers.common['Authorization'] = 'Bearer 19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp';
      axios.defaults.headers.common["Authorization"] = Cookies.get("token");
      axios
        .post(process.env.REACT_APP_BASE_API_URI + "/get_catcoorp", {
          id: e.value,
        })
        .then((response) => {
          if (response.status === 200) {
            if (response.data.result.Status === true) {
              let suppsosed_todo = response.data.result.Data[0].data_form;
              let ui = [];

              suppsosed_todo.map((number) => {
                let is_filled = false;
                kategori.forEach(function (element, i) {
                  if (element.cooperation_category_form_id == number.id) {
                    ui.push({
                      id: number.id,
                      cooperation_form: isJsonString(number.cooperation_form)
                        ? JSON.parse(number.cooperation_form)["Name"]
                        : number.cooperation_form,
                      value_text: element.cooperation_form_content,
                      error: "",
                    });
                    is_filled = true;
                  }
                });
                if (is_filled == false) {
                  ui.push({
                    id: number.id,
                    cooperation_form: isJsonString(number.cooperation_form)
                      ? JSON.parse(number.cooperation_form)["Name"]
                      : number.cooperation_form,
                    value_text: "",
                    error: "",
                  });
                }
              });
              setDataForm(ui);
            } else {
              setDataForm([]);
              setDataFormPost([]);
            }
          }
        })
        .catch((error) => {
          setDataForm([]);
          setDataFormPost([]);
        })
        .finally(() => {
          setIsDisabledNext(false);
        });
    }
  };
  const [filesICon, setFilesIcon] = useState({});
  const [akademi, setAkademi] = useState({
    tanggal: moment().locale("ID").format("DD MMMM YYYY, HH:mm"),
    lembaga: "",
    email: "",
    periode: "",
    judul: "",
    kategori: "",
    deskripsi: "",
    deskripsi2: "",
    tujuan: "",
    selectedFileIcon: "",
    kerjasama_awal: "",
    kerjasama_akhir: "",
    nope_lembaga: "",
    nope_kekom: "",
    kerjasama_ttd: "",
    cooperation_category_id: "",
    nomor_mou_pks: "",
    idpartner_name: "",
    unggah_file_final: "",
  });

  const [submitted, setSubmitted] = useState(false);
  const MySwal = withReactContent(Swal);
  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setAkademi({ ...akademi, [name]: value });
  };
  const initialVal = [
    {
      label: "",
      value: "",
    },
  ];
  const awal = useRef();
  const [optionList, setOptioList] = useState(initialVal);
  const [optionListSelected, setOptionListSelected] = useState({});
  const [optionListlist_catcoorp, setOptioListlist_catcoorp] =
    useState(initialVal);
  const [optionstatus, setOptioList_status_kerjsama] = useState(initialVal);
  const [init, setInit] = useState(true);
  const [mitra, setMitra] = useState({});

  useEffect(() => {
    MySwal.fire({
      title: "Mohon Tunggu!",
      icon: "info",
      allowOutsideClick: false,
      didOpen: () => {
        MySwal.showLoading();
      },
    });
    retrivelist_catcoorp().then(getDetailKerjasama());
    // retriveOptionStatus_kerjsama().then();
  }, []);

  const getDetailKerjasama = async () => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/kerjasama/API_Detail_Kerjasama",
        {
          id: params.id,
        },
      )
      .then(async function (response) {
        const {
          created_at,
          email,
          idpartner_name,
          partner_name,
          tanggal,
          periode_kerjasama,
          periode_unit,
          judul_kerjasama,
          id_kategori_kerjasama,
          kategori_kerjasama,
          status,
          tujuan_kerjasama,
          form_kerjasama,
          periode_kerjasama_mulai,
          periode_kerjasama_selesai,
          nomor_perjanjian_lembaga,
          nomor_perjanjian_kemkominfo,
          nomor_mou_pks,
          tanggal_tanda_tangan,
          file_kerjasama,
          ket_status,
          unggah_file_final,
        } = response.data.result.Data[0];
        const kategori = response.data.result.Data[0].kategori;

        const [y, m, dd] = periode_kerjasama_mulai.split("-");
        let monthNum = 1;
        if (`${m}`.length == 1) {
          monthNum = `0${m}`;
        } else {
          monthNum = `${m}`;
        }

        const tAwalM = moment(`${dd} ${monthNum} ${y}`, "D MM YYYY");
        awal.current = tAwalM.format("YYYY-MM-DD");

        const [y2, m2, dd2] = tanggal_tanda_tangan.split("-");
        let monthNum2 = 1;

        if (`${m2}`.length == 1) {
          monthNum2 = `0${m2}`;
        } else {
          monthNum2 = `${m2}`;
        }
        let f_tanggal_tanda_tangan = moment(
          `${dd2} ${monthNum2} ${y2}`,
          "D MM YYYY",
        );

        setAkademi({
          tanggal: tanggal,
          lembaga: idpartner_name,
          email: email,
          periode: convertPeriodeKerjasama(periode_kerjasama),
          judul: judul_kerjasama,
          kategori: null,
          deskripsi: null,
          status: status,
          deskripsi2: null,
          tujuan: null,
          selectedFileIcon: null,
          kerjasama_awal: tAwalM.format("D MMM YYYY"),
          kerjasama_akhir: tAwalM
            .add(periode_kerjasama, "years")
            .format("D MMM YYYY"),
          nope_lembaga: nomor_perjanjian_lembaga,
          nope_kekom: nomor_perjanjian_kemkominfo,
          nomor_mou_pks: nomor_mou_pks,
          kerjasama_ttd: f_tanggal_tanda_tangan.format("D MMM YYYY"),
          cooperation_category_id: id_kategori_kerjasama,
          kategori_kerjasama: kategori_kerjasama,
          unggah_file_final: unggah_file_final,
        });

        retriveOption(response.data.result.Data[0].idpartner_name);
        setKategori(kategori);
        const status_val = { label: ket_status, value: status };
        handleChangeIpt(status_val);
        MySwal.close();
      })
      .catch(function (error) {
        MySwal.close();
      });
  };
  useEffect(() => {
    const cooperation_category_val = {
      label: akademi.kategori_kerjasama,
      value: akademi.cooperation_category_id,
    };
    setCooperation_category_val(cooperation_category_val);
    if (init) {
      handleChangecooperation_category_id(cooperation_category_val);
      setInit(false);
    }
  }, [akademi]);

  useEffect(() => {
    // console.log(akademi.periode)
    const { periode, kerjasama_awal, kerjasama_akhir } = akademi;
    // console.log(kerjasama_awal)
    const ta = moment(kerjasama_awal, "DD MMM YYYY", "id");
    // console.log(ta.isValid(), !isNaN(periode))
    if (ta.isValid() && !isNaN(periode)) {
      console.log("masuk");
      setAkademi((a) => ({
        ...a,
        kerjasama_akhir: ta
          .add("years", periode)
          .locale("id")
          .format("DD MMM YYYY"),
      }));
    }
  }, [akademi.periode]);

  useEffect(() => {
    handleChangecooperation_category_id(cooperation_category_val);
  }, [kategori]);

  useEffect(() => {
    initStepper();
  }, [window.KTStepper]);

  const initStepper = () => {
    if (window?.KTStepper) {
      let element = document.querySelector("#kt_stepper_example_basic");
      const s = new window.KTStepper(element);
      setStepperFunc(s);
    }
  };

  const prevStep = () => {
    if (!stepperFunc.uid) {
      initStepper();
    }
    stepperFunc.goPrevious();
    setWizPos(stepperFunc.getCurrentStepIndex());
  };

  const nextStep = () => {
    if (!stepperFunc.uid) {
      initStepper();
    }
    if (handleValidation(stepperFunc.getCurrentStepIndex())) {
      stepperFunc.goNext();
      setWizPos(stepperFunc.getCurrentStepIndex());
    }
  };

  const retriveOption = async (id_selected_partnaer) => {
    const respon = axios
      .post(process.env.REACT_APP_BASE_API_URI + "/mitra/list_mitra_aktif", {
        start: "0",
        length: "1000",
        status: "1",
        orderby: "nama_mitra",
        ordertype: "asc",
      })
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.result.Status === true) {
            let repo = response.data.result.Data;
            let ui = [
              {
                label: "Pilih Lembaga",
                value: 0,
              },
            ];
            let selected = {};
            repo.map((number) => {
              if (number.id == id_selected_partnaer) {
                selected = {
                  label: number.nama_mitra,
                  value: number.id,
                };
              }
              ui.push({
                label: number.nama_mitra,
                value: number.id,
              });
            });
            setOptioList(ui);
            setOptionListSelected(selected);
          } else {
            setOptioList();
          }
        }
      });
  };

  const retrivelist_catcoorp = async () => {
    //axios.defaults.headers.common['Authorization'] = 'Bearer 12|qEyaQQuG5xPcXujeqknAv9nhe1wV7l7Rn8xLqK31';
    axios.defaults.headers.common["Authorization"] = Cookies.get("token");
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/masterKerjasama/list_catcoorpaktif",
        {
          start: "0",
          rows: "50",
        },
      )
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.result.Status === true) {
            let repo = response.data.result.Data;
            let ui = [
              {
                label: "Pilih Kategori Kerjasama",
                value: 0,
              },
            ];
            const listItem = repo.map((number) =>
              ui.push({
                label: number.pcooperation_categories,
                value: number.pid,
              }),
            );
            setOptioListlist_catcoorp(ui);
          } else {
            setOptioListlist_catcoorp();
          }
        }
      });
  };

  const retriveOptionStatus_kerjsama = async () => {
    const respon = axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/kerjasama/status_kerjasama_list",
        {
          start: "0",
          rows: "50",
        },
      )
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.result.Status === true) {
            let repo = response.data.result.Data;
            let ui = [];
            repo.map((number) =>
              ui.push({
                label: number.name,
                value: number.id,
              }),
            );
            setOptioList_status_kerjsama(ui);
          } else {
            setOptioList();
          }
        }
      });
  };

  const onFileChange_icon = (event) => {
    if (event.target.files[0].size >= 2000000) {
      MySwal.fire({
        title: <strong>Information!</strong>,
        html: <i>File Melebihi Ukuran</i>,
        icon: "warning",
      });
      document.querySelector("#icon").value = "";
    } else {
      setUpdatePic(true);
      setFilesIcon({ selectedFileIcon: event.target.files[0] });
    }
  };

  const handleFilter = (value) => {
    //axios.defaults.headers.common['Authorization'] = 'Bearer 12|qEyaQQuG5xPcXujeqknAv9nhe1wV7l7Rn8xLqK31';
    axios.defaults.headers.common["Authorization"] = Cookies.get("token");
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/mitra/cari-mitra",
        {
          id: value.value,
        },
        { Authorization: Cookies.get("token") },
      )
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.result.Status === true) {
            let repo = response.data.result.Data[0];
            setAkademi({ ...akademi, lembaga: value.value, email: repo.email });
          } else {
            setAkademi({ ...akademi, email: "-" });
          }
        }
      });
  };

  const handleInputTanggalAwal = (e) => {
    const { name, value } = e.currentTarget;
    if (value != "") {
      let [dd, m, y] = value.split(" ");
      const months = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "June",
        "July",
        "Aug",
        "Sept",
        "Oct",
        "Nov",
        "Dec",
      ];
      months.forEach(function (element, i) {
        if (element == m) {
          m = i + 1;
        }
      });
      let monthNum = m;

      /* eslint-disable no-undef */
      if (typeof id?.months?.shorthand != "undefined") {
        const shm = id?.months?.shorthand;
        for (let i = 0; i < shm.length; i++) {
          if (shm[i] == m) {
            monthNum = i + 1;
          }
        }
      }
      if (`${monthNum}`.length == 1) {
        monthNum = `0${monthNum}`;
      } else {
        monthNum = `${monthNum}`;
      }

      const tAwalM = moment(`${dd} ${monthNum} ${y}`, "D MM YYYY");
      awal.current = tAwalM.format("YYYY-MM-DD");

      setAkademi({
        ...akademi,
        kerjasama_awal: tAwalM.format("D MMM YYYY"),
        kerjasama_akhir: tAwalM
          .add(akademi.periode, "years")
          .format("D MMM YYYY"),
      });
    }
  };

  const handleInputTanggalTtd = (e) => {
    const { name, value } = e.currentTarget;
    if (value != "") {
      let [dd, m, y] = value.split(" ");
      const months = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "June",
        "July",
        "Aug",
        "Sept",
        "Oct",
        "Nov",
        "Dec",
      ];
      months.forEach(function (element, i) {
        if (element == m) {
          m = i + 1;
        }
      });
      let monthNum = m;

      /* eslint-disable no-undef */
      if (typeof id?.months?.shorthand != "undefined") {
        const shm = id?.months?.shorthand;
        for (let i = 0; i < shm.length; i++) {
          if (shm[i] == m) {
            monthNum = i + 1;
          }
        }
      }
      if (`${monthNum}`.length == 1) {
        monthNum = `0${monthNum}`;
      } else {
        monthNum = `${monthNum}`;
      }
      const tAwalM = moment(`${dd} ${monthNum} ${y}`, "D MM YYYY");
      setAkademi({ ...akademi, [name]: tAwalM.format("D MMM YYYY") });
    }
  };

  const handleValidation = (page = 1) => {
    let validation_result = true;
    setLembaga_error("");
    setPeriode_error("");
    setJudul_error("");
    setCooperation_category_id_error("");
    setStatus_error("");
    setErrorTglAwal("");
    setErrorTglAkhir("");
    setErrorTanggalPendandatanganan("");
    setErrorNomorMouPks("");
    setErrorFile("");

    if (page == 1) {
      if (akademi.lembaga == "" || akademi.lembaga == null) {
        setLembaga_error("Tidak Boleh Kosong");
        validation_result = false;
      }
      if (akademi.periode == "" || akademi.periode == null) {
        setPeriode_error("Tidak Boleh Kosong");
        validation_result = false;
      }
      if (akademi.judul == "" || akademi.judul == null) {
        setJudul_error("Tidak Boleh Kosong");
        validation_result = false;
      }
      if (
        akademi.cooperation_category_id == "" ||
        akademi.cooperation_category_id == null
      ) {
        setCooperation_category_id_error("Tidak Boleh Kosong");
        validation_result = false;
      }
      if (status_id == "" || status_id == null) {
        setStatus_error("Tidak Boleh Kosong");
        validation_result = false;
      }
      if (dataForm.length > 0) {
        let f = [...dataForm];
        let newForm = f.map((isian) =>
          isian?.value_text == ""
            ? { ...isian, error: "Tidak boleh kosong" }
            : { ...isian, error: "" },
        );
        setDataForm(newForm);
      }
    } else if (page == 2) {
      if (akademi.kerjasama_awal == "" || akademi.kerjasama_awal == null) {
        setErrorTglAwal("Tidak boleh kosong");
        validation_result = false;
      }
      if (akademi.kerjasama_akhir == "" || akademi.kerjasama_akhir == null) {
        setErrorTglAkhir("Tidak boleh kosong");
        validation_result = false;
      }
      if (akademi.nomor_mou_pks == "" || akademi.nomor_mou_pks == null) {
        setErrorNomorMouPks("Tidak boleh kosong");
        validation_result = false;
      }
      if (akademi.kerjasama_ttd == "" || akademi.kerjasama_ttd == null) {
        setErrorTanggalPendandatanganan("Tidak boleh kosong");
        validation_result = false;
      }
      if (filesICon.selectedFileIcon == undefined) {
      } else {
        if (filesICon.selectedFileIcon.size >= 2000000) {
          setErrorFile("Ukuran file terlalu besar");
          validation_result = false;
        }
      }
    }
    if (validation_result == false) {
      MySwal.fire({
        title: <strong>Information!</strong>,
        html: <i>Maaf, Masih ada yang belum diisi</i>,
        icon: "warning",
      });
    }
    return validation_result;
  };

  const handleDelete = () => {
    const configs = {
      responseType: "arraybuffer",
      headers: {
        Authorization: "Bearer " + Cookies.get("token"),
        Accept: "*/*",
        "Content-Length": 0,
        Connection: "keep-alive",
      },
    };
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          let param_delete = new FormData();
          param_delete.append("id", params.id);
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI +
                "/kerjasama/API_Hapus_Kerjasama",
              param_delete,
              configs,
            )
            .then(function (response) {
              if (response.status === 200) {
                if (response.data.result.Status === true) {
                  swal
                    .fire({
                      title: messagex,
                      icon: "success",
                      confirmButtonText: "Ok",
                      allowOutsideClick: false,
                    })
                    .then((result) => {
                      if (result.isConfirmed) {
                        //window.location = '/publikasi/video';
                        window.history.back();
                      }
                    });
                } else {
                  swal.fire({
                    title: <strong>Information!</strong>,
                    html: <i>{response.data.result.Message}</i>,
                    icon: "info",
                  });
                }
              } else {
              }
            })
            .catch((error) => {
              console.log(error);
              swal.fire({
                title: "Terjadi kesalahan",
                icon: "warning",
                text:
                  error.response.data.result.Message || "data tidak terhapus",
              });
            });
        }
      });
  };

  const saveAkademi = () => {
    if (handleValidation(stepperFunc.getCurrentStepIndex())) {
      setIsLoading(true);
      MySwal.fire({
        title: "Mohon Tunggu!",
        icon: "info",
        allowOutsideClick: false,
        didOpen: () => {
          MySwal.showLoading();
        },
      });

      let formPostData = [];
      if (dataForm.length > 0) {
        for (let i = 0; i < dataForm.length; i++) {
          formPostData.push({
            cooperation_category_form_id: dataForm[i].id,
            cooperation_form_content: dataForm[i].value_text,
          });
        }
      }
      axios.defaults.headers.post["Content-Type"] = "multipart/form-data";
      const data = new FormData();
      data.append("id", params.id);
      data.append(
        "tanggal",
        momentToDoStandardDate(akademi.tanggal, "D MMM YYYY, HH:mm"),
      );
      data.append("Lembaga", akademi.lembaga);
      data.append("Periode_Kerjasama", akademi.periode);
      data.append("Judul_Kerjasama", akademi.judul);
      data.append("Kategori_kerjasama", akademi.cooperation_category_id);
      data.append("Tujuan_Kerjasama", "TUJUAN KERJASAMA");
      data.append("Form_Kerjasama", akademi.deskripsi);
      if (is_update_pic) {
        data.append("Unggah_File_Final", filesICon.selectedFileIcon);
      }
      data.append(
        "Tanggal_Tanda_Tangan",
        momentToDoStandardDate(akademi.kerjasama_ttd, "D MMM YYYY"),
      );
      data.append("Nomor_Perjanjian_Kemkominfo", akademi.nope_kekom);
      data.append("Nomor_Perjanjian_Lembaga", akademi.nope_lembaga);
      data.append(
        "Periode_Kerjasama_mulai",
        momentToDoStandardDate(akademi.kerjasama_awal, "D MMM YYYY"),
      );
      data.append(
        "Periode_Kerjasama_selesai",
        momentToDoStandardDate(akademi.kerjasama_akhir, "D MMM YYYY"),
      );
      data.append("status_migrates_id", 5);
      data.append("nomor_mou_pks", akademi.nomor_mou_pks);
      data.append("json_detail", JSON.stringify(formPostData));

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI +
            "/kerjasama/API_Update_Kerjasama",
          data,
          {
            Authorization: Cookies.get("token"),
            "Content-Type": "multipart/form-data",
          },
        )
        .then(function (response) {
          if (response.status === 200) {
            setIsLoading(true);
            if (response.data.result.Status === true) {
              MySwal.fire({
                title: <strong>Information!</strong>,
                html: <i>Berhasil Edit MoU/PKS</i>,
                icon: "success",
              });
              history("/partnership/direktori_moupks");
            } else {
              MySwal.fire({
                title: <strong>Information!</strong>,
                html: <i>Gagal Edit MoU/PKS</i>,
                icon: "info",
              });
              window.location.reload(true);
            }
          }
        })
        .catch(function (error) {
          setIsLoading(true);
          MySwal.fire({
            title: <strong>Information!</strong>,
            html: <i>{error.response.data.result.Message}</i>,
            icon: "warning",
          });
        });
    }
  };

  const handleDataForm = (e) => {
    let index = e.target.attributes.index.value;
    let fieldValue = e.target.value;
    dataForm.splice(index, 0);
    const tempRows = [...dataForm];
    const tempObj = dataForm[index];
    tempObj["value_text"] = fieldValue;
    tempRows[index] = tempObj;
    setDataForm(tempRows);
  };

  const momentToDoStandardDate = (date, format) => {
    return moment(date, format).locale("id").format("YYYY-MM-DD");
  };

  const handleDownloadData = (url, nama) => {
    const configs = {
      responseType: "arraybuffer",
      headers: {
        Authorization: "Bearer " + Cookies.get("token"),
        Accept: "*/*",
        "Content-Length": 0,
        Connection: "keep-alive",
      },
    };
    swal.fire({
      title: "Memproses Download File, Mohon Tunggu..",
      icon: "info",
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios.post(url, configs).then((response) => {
      const byteString = window.atob(response.data);
      const arrayBuffer = new ArrayBuffer(byteString.length);
      const int8Array = new Uint8Array(arrayBuffer);
      for (let i = 0; i < byteString.length; i++) {
        int8Array[i] = byteString.charCodeAt(i);
      }
      const blob = new Blob([int8Array], { type: "application/pdf" });
      var fileURL = window.URL.createObjectURL(blob);
      var fileLink = document.createElement("a");
      fileLink.href = fileURL;
      fileLink.setAttribute("download", nama + ".pdf");
      document.body.appendChild(fileLink);
      fileLink.click();
      swal.close();
    });
  };

  return (
    <div>
      <Header />
      <SideNav />
      <div className="toolbar" id="kt_toolbar">
        <div
          id="kt_toolbar_container"
          className="container-fluid d-flex flex-stack my-2"
        >
          <div className="d-flex align-items-start my-2">
            <div>
              <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                <span className="me-3">
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    className="mh-50px"
                  >
                    <path
                      opacity="0.3"
                      d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z"
                      fill="#FFC700"
                    ></path>
                    <path
                      d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z"
                      fill="#FFC700"
                    ></path>
                  </svg>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Partnership
                  <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                </span>
                Direktori Mou/PKS
              </h1>
            </div>
          </div>
          <div className="d-flex align-items-end my-2">
            <div>
              <button
                onClick={() => window.history.back()}
                type="reset"
                className="btn btn-sm btn-light btn-active-light-primary"
                data-kt-menu-dismiss="true"
              >
                <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                  <i className="fa fa-chevron-left"></i>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Kembali
                </span>
              </button>

              <a
                href="#"
                onClick={handleDelete}
                className="btn btn-sm btn-danger btn-active-light-info ms-2"
              >
                <i className="bi bi-trash-fill text-white"></i>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Hapus
                </span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div
        className="wrapper d-flex flex-column flex-row-fluid pt-0"
        id="kt_wrapper"
      >
        <div
          className="content d-flex flex-column flex-column-fluid pt-0"
          id="kt_content"
        >
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container-xxl">
              <div className="row">
                <div
                  className="stepper stepper-links mb-n3"
                  id="kt_stepper_example_basic"
                >
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <div className="stepper-nav flex-wrap mb-n4">
                            <div
                              className="stepper-item ms-0 my-2 current"
                              data-kt-stepper-element="nav"
                              data-kt-stepper-action="step"
                            >
                              <div className="stepper-wrapper d-flex align-items-center">
                                <div className="stepper-label ms-0">
                                  <h3 className="stepper-title fs-6">
                                    Informasi MoU/PKS
                                  </h3>
                                </div>
                              </div>
                            </div>
                            <div
                              className="stepper-item mx-3 my-2"
                              data-kt-stepper-element="nav"
                              data-kt-stepper-action="step"
                            >
                              <div className="stepper-wrapper d-flex align-items-center">
                                <div className="stepper-label">
                                  <h3 className="stepper-title fs-6">
                                    Lampiran MoU/PKS
                                  </h3>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <form
                          className="form"
                          noValidate="novalidate"
                          id="kt_stepper_example_basic_form"
                        >
                          <div className="mb-5">
                            <div
                              className="current"
                              data-kt-stepper-element="content"
                            >
                              <div className="row mt-7">
                                <div className="form-group fv-row mb-7">
                                  <label
                                    className="form-label required"
                                    htmlFor="description"
                                  >
                                    Judul Kerjasama
                                  </label>
                                  <div className="input-group">
                                    <input
                                      type="text"
                                      name="judul"
                                      className="form-control form-control-sm"
                                      placeholder="Masukan Judul Kerjasama"
                                      aria-describedby="basic-addon2"
                                      value={akademi.judul}
                                      onChange={handleInputChange}
                                    />
                                  </div>
                                  <span style={{ color: "red" }}>
                                    {judul_error}
                                  </span>
                                </div>
                                <div className="form-group fv-row mb-7">
                                  <label
                                    className="form-label required"
                                    htmlFor="title"
                                  >
                                    Kategori Kerjasama
                                  </label>
                                  <Select
                                    name="status"
                                    placeholder="Silahkan pilih"
                                    className="form-select-sm selectpicker p-0"
                                    value={cooperation_category_val}
                                    isOptionSelected={
                                      akademi.cooperation_category_id ===
                                      optionListlist_catcoorp
                                    }
                                    options={optionListlist_catcoorp}
                                    onChange={(e) =>
                                      handleChangecooperation_category_id(e)
                                    }
                                  />
                                  <span style={{ color: "red" }}>
                                    {cooperation_category_id_error}
                                  </span>
                                </div>
                                {dataForm.length != 0 &&
                                  dataForm.map((item, idx) => (
                                    <React.Fragment key={idx}>
                                      <div className="form-group fv-row">
                                        <label
                                          className="form-label required"
                                          htmlFor="title"
                                        >
                                          {item.cooperation_form}
                                        </label>
                                        <input
                                          type="text"
                                          className="form-control form-control-sm"
                                          name={item.cooperation_form}
                                          index={idx}
                                          placeholder={item.cooperation_form}
                                          value={dataForm[idx]["value_text"]}
                                          onChange={(e) => handleDataForm(e)}
                                        ></input>
                                      </div>
                                      <span
                                        className="mb-7"
                                        style={{ color: "red" }}
                                      >
                                        {item.error}
                                      </span>
                                    </React.Fragment>
                                  ))}
                                <div className="form-group fv-row mb-3">
                                  <label
                                    className="form-label required"
                                    htmlFor="title"
                                  >
                                    Mitra
                                  </label>
                                  <Select
                                    name="level_pelatihan"
                                    placeholder="Silahkan Pilih Mitra"
                                    noOptionsMessage={({ inputValue }) =>
                                      !inputValue
                                        ? optionList.value
                                        : "Data tidak tersedia"
                                    }
                                    value={optionListSelected}
                                    className="form-select-sm selectpicker p-0 mb-0"
                                    options={optionList}
                                    onChange={(value) => handleFilter(value)}
                                    isDisabled={true}
                                  />
                                  <span
                                    className="ms-3"
                                    style={{ color: "red" }}
                                  >
                                    {lembaga_error}
                                  </span>
                                </div>
                                <div className="form-group fv-row mb-7">
                                  <label
                                    className="form-label required"
                                    htmlFor="title"
                                  >
                                    Email
                                  </label>
                                  <br />
                                  <label className="form-label" htmlFor="title">
                                    {akademi.email}
                                  </label>
                                </div>
                                <div className="form-group fv-row mb-7">
                                  <div className="row">
                                    <h5 className="mb-5">Periode Kerjasama</h5>
                                    <div className="col-12 mb-5">
                                      <label
                                        className="form-label required"
                                        htmlFor="description"
                                      >
                                        Durasi MoU/PKS (Dalam Tahun)
                                      </label>
                                      <div className="row col-lg-12">
                                        <div className="input-group">
                                          <input
                                            type="number"
                                            min={1}
                                            className="form-control form-control-sm"
                                            placeholder="Masukan Lama Kerjasama"
                                            aria-describedby="basic-addon2"
                                            name="periode"
                                            value={akademi.periode}
                                            onChange={handleInputChange}
                                          />
                                          <div className="input-group-append">
                                            <span className="input-group-text">
                                              Tahun
                                            </span>
                                          </div>
                                        </div>
                                        <span style={{ color: "red" }}>
                                          {periode_error}
                                        </span>
                                      </div>
                                    </div>
                                    <div className="col-6">
                                      <label
                                        className="form-label required"
                                        htmlFor="title"
                                      >
                                        Tgl. Mulai
                                      </label>
                                      <br />
                                      <div className="input-group">
                                        <input
                                          id="kerjasama_awal"
                                          className="form-control form-control-sm"
                                          placeholder="Masukkan tanggal mulai kerjasama"
                                          name="kerjasama_awal"
                                          defaultValue={akademi.kerjasama_awal}
                                          onFocus={(e) => {
                                            handleInputTanggalAwal(e);
                                          }}
                                        />
                                      </div>
                                      <span
                                        className="mb-7"
                                        style={{ color: "red" }}
                                      >
                                        {error_tgl_awal}
                                      </span>
                                    </div>
                                    <div className="col-6">
                                      <label
                                        className="form-label required"
                                        htmlFor="title"
                                      >
                                        Tgl. Akhir
                                      </label>
                                      <br />
                                      <div className="input-group date">
                                        <input
                                          id="kerjasama_akhir"
                                          className="form-control form-control-sm"
                                          placeholder="Masukkan tanggal selesai kerjasama"
                                          name="kerjasama_akhir"
                                          value={akademi.kerjasama_akhir}
                                          disabled
                                        />
                                      </div>
                                      <span
                                        className="mb-7"
                                        style={{ color: "red" }}
                                      >
                                        {error_tgl_akhir}
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div data-kt-stepper-element="content">
                              <div className="row mt-7">
                                <div className="form-group fv-row mb-7">
                                  <label
                                    className="form-label required"
                                    htmlFor="title"
                                  >
                                    Nomor MoU/PKS
                                  </label>
                                  <input
                                    type="text"
                                    className="form-control form-control-sm"
                                    placeholder="Masukan Nomor MoU/PKS"
                                    aria-describedby="basic-addon2"
                                    name="nomor_mou_pks"
                                    value={akademi.nomor_mou_pks}
                                    onChange={handleInputChange}
                                  />
                                  <span
                                    className="mb-7"
                                    style={{ color: "red" }}
                                  >
                                    {error_nomor_mou_pks}
                                  </span>
                                </div>

                                <div className="form-group fv-row mb-7">
                                  <label
                                    className="form-label required"
                                    htmlFor="title"
                                  >
                                    Tgl. Penandatanganan
                                  </label>
                                  <input
                                    id="kerjasama_ttd"
                                    className="form-control form-control-sm"
                                    placeholder="Masukkan tanggal penandatanganan"
                                    name="kerjasama_ttd"
                                    defaultValue={akademi.kerjasama_ttd}
                                    onFocus={(e) => {
                                      handleInputTanggalTtd(e);
                                    }}
                                  />
                                  <span
                                    className="mb-7"
                                    style={{ color: "red" }}
                                  >
                                    {error_tgl_penandatanganan}
                                  </span>
                                </div>
                                {akademi.unggah_file_final ? (
                                  <div className="form-group fv-row">
                                    <label
                                      className="form-label"
                                      htmlFor="title"
                                    >
                                      Dokumen MoU/PKS
                                    </label>
                                    <div>
                                      <a
                                        href="#"
                                        url={
                                          process.env.REACT_APP_BASE_API_URI +
                                          "/kerjasama/API_Get_File_Kerjasama?path=" +
                                          akademi.unggah_file_final
                                        }
                                        onClick={() =>
                                          handleDownloadData(
                                            process.env.REACT_APP_BASE_API_URI +
                                              "/kerjasama/API_Get_File_Kerjasama2?path=" +
                                              akademi.unggah_file_final,
                                            akademi.judul,
                                          )
                                        }
                                        title="Download"
                                        className="btn btn-light fw-bolder btn-sm me-2 mb-2"
                                        file={akademi.unggah_file_final}
                                      >
                                        <i className="bi bi-cloud-download"></i>{" "}
                                        Download
                                      </a>
                                    </div>
                                  </div>
                                ) : (
                                  ""
                                )}
                                <div className="form-group fv-row mb-7">
                                  <div className="input-group">
                                    <input
                                      type="file"
                                      className="form-control form-control-sm font-size-h4"
                                      name="icon"
                                      onChange={onFileChange_icon}
                                      id="icon"
                                      accept="application/pdf"
                                    />
                                  </div>
                                  <span className="text-muted font-size-sm">
                                    File yang diupload *.pdf dengan ukuran
                                    maksimal 2 MB
                                  </span>
                                  <br />
                                  <span
                                    className="mb-7"
                                    style={{ color: "red" }}
                                  >
                                    {error_file}
                                  </span>
                                </div>

                                <div className="form-group fv-row mb-7">
                                  <label
                                    className="form-label required"
                                    htmlFor="title"
                                  >
                                    Tgl. Input
                                  </label>
                                  <p className="fs-7">
                                    <Moment locale="ID" format="D MMM YYYY">
                                      {akademi.tanggal}
                                    </Moment>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="form-group fv-row pt-7 mb-7">
                            <div className="d-flex justify-content-center mb-7">
                              <div className="me-2">
                                <button
                                  type="button"
                                  className="btn btn-md btn-secondary me-3"
                                  data-kt-stepper-action="previous"
                                  onClick={prevStep}
                                >
                                  <i className="fa fa-chevron-left me-1"></i>
                                  Sebelumnya
                                </button>
                              </div>
                              <div>
                                <button
                                  onClick={saveAkademi}
                                  type="button"
                                  className="btn btn-primary btn-md"
                                  data-kt-stepper-action="submit"
                                  disabled={isLoading}
                                >
                                  {isLoading ? (
                                    <>
                                      <span
                                        className="spinner-border spinner-border-sm me-2"
                                        role="status"
                                        aria-hidden="true"
                                      ></span>
                                      <span className="sr-only">
                                        Loading...
                                      </span>
                                      Loading...
                                    </>
                                  ) : (
                                    <>
                                      <i className="fa fa-paper-plane me-1"></i>
                                      Simpan
                                    </>
                                  )}
                                </button>

                                {wizPos != 2 && (
                                  <button
                                    type="button"
                                    className={`btn btn-primary ${
                                      isDisabledNext ? "disabled" : ""
                                    }`}
                                    disabled={isDisabledNext}
                                    onClick={nextStep}
                                  >
                                    Selanjutnya{" "}
                                    <i className="fa fa-chevron-right ms-1"></i>
                                  </button>
                                )}
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default EditKerjasamadisetujui;
