import React, { useState, useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import Header from "../../../../components/Header";
import SideNav from "../../../../components/SideNav";
import Footer from "../../../../components/Footer";
import Select from "react-select";
import Cookies from "js-cookie";
import moment from "moment";
import { isJsonString } from "../../../../utils/commonutil";
import { cekPermition } from "../../../AksesHelper";
import Moment from "react-moment";

const useUnload = (fn) => {
  const cb = useRef(fn); // init with fn, so that type checkers won't assume that current might be undefined

  useEffect(() => {
    cb.current = fn;
  }, [fn]);

  useEffect(() => {
    const onUnload = (...args) => cb.current?.(...args);

    window.addEventListener("beforeunload", onUnload);

    return () => window.removeEventListener("beforeunload", onUnload);
  }, []);
};

const AddKerjasamadisetujui = (props) => {
  Cookies.remove("pelatian_id");

  const [optSet, setOpt] = useState(0);
  const [errorDataForm, setErrorDataForm] = useState([]);
  const [dataForm, setDataForm] = useState([]);
  const [dataFormPost, setDataFormPost] = useState([]);
  const [lembaga_error, setLembaga_error] = useState("");
  const [periode_error, setPeriode_error] = useState("");
  const [judul_error, setJudul_error] = useState("");
  const [cooperation_category_id_error, setCooperation_category_id_error] =
    useState("");
  const [status_error, setStatus_error] = useState("");
  const [stepperFunc, setStepperFunc] = useState(null);
  const [isDisabledNext, setIsDisabledNext] = useState(false);
  const [error_tgl_awal, setErrorTglAwal] = useState("");
  const [error_tgl_akhir, setErrorTglAkhir] = useState("");
  const [error_nomor_mou_pks, setErrorNomorMouPks] = useState("");
  const [error_tgl_penandatanganan, setErrorTanggalPendandatanganan] =
    useState("");
  const [error_file, setErrorFile] = useState("");
  const [nextClicked, setNextClicked] = useState(false);
  const [submitClicked, setSubmitClicked] = useState(false);
  const [page, setPage] = useState(1);

  const [isLoading, setIsLoading] = useState(false);

  let history = useNavigate();

  const handleChangeIpt = (status_id) => {
    let dataxstatusvalue = status_id;
    setAkademi({ ...akademi, ["status"]: dataxstatusvalue.value });
  };

  const handleChangecooperation_category_id = (e) => {
    setIsDisabledNext(true);
    setDataForm([]);
    setDataFormPost([]);
    let dataxstatusvalue = e.value;

    akademi.cooperation_category_id = e.value;

    axios.defaults.headers.common["Authorization"] =
      "Bearer " + Cookies.get("token");
    axios
      .post(process.env.REACT_APP_BASE_API_URI + "/get_catcoorp", {
        id: e.value,
      })
      .then((response) => {
        if (response.status === 200) {
          if (response.data.result.Status === true) {
            let suppsosed_todo = response.data.result.Data[0].data_form;

            let ui = [];
            let arrError = [];
            // isJsonString
            const listItem = suppsosed_todo.map((number) => {
              arrError.push("");
              ui.push({
                id: number.id,
                cooperation_form: isJsonString(number.cooperation_form)
                  ? JSON.parse(number.cooperation_form)["Name"]
                  : number.cooperation_form,
                value_text: "",
                error: "",
              });
            });
            setDataForm(ui);
            setErrorDataForm(arrError);
          } else {
            setDataForm([]);
            setDataFormPost([]);
          }
        }
      })
      .catch((error) => {
        setDataForm([]);
        setDataFormPost([]);
      })
      .finally(() => {
        setIsDisabledNext(false);
      });
  };

  const [filesICon, setFilesIcon] = useState({});
  const [akademi, setAkademi] = useState({
    tanggal: moment().locale("ID").format("DD MMMM YYYY, HH:mm"),
    lembaga: "",
    email: "",
    periode: "",
    judul: "",
    kategori: "",
    deskripsi: "",
    deskripsi2: "",
    tujuan: "",
    selectedFileIcon: "",
    kerjasama_akhir: "",
    nomor_mou_pks: "",
    kerjasama_ttd: "",
    cooperation_category_id: "",
  });

  const MySwal = withReactContent(Swal);
  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setAkademi({ ...akademi, [name]: value });
  };
  const initialVal = [
    {
      label: "",
      value: "",
    },
  ];
  const awal = useRef();
  const [optionList, setOptioList] = useState(initialVal);
  const [optionListlist_catcoorp, setOptioListlist_catcoorp] =
    useState(initialVal);
  const [optionstatus, setOptioList_status_kerjsama] = useState(initialVal);

  useUnload((e) => {
    e.preventDefault();
    e.returnValue =
      "Apakah anda yakin akan meninggalkan halaman ? isian anda akan terhapus.";
  });

  useEffect(() => {
    MySwal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        MySwal.showLoading();
      },
    });
    retriveOption();
    // retriveOptionStatus_kerjsama();
    retrivelist_catcoorp();
  }, []);

  useEffect(() => {
    initStepper();
  }, [window.KTStepper]);

  const initStepper = () => {
    if (window?.KTStepper) {
      let element = document.querySelector("#kt_stepper_example_basic");
      const s = new window.KTStepper(element);
      setStepperFunc(s);
    }
  };

  useEffect(() => {
    recheckValidation();
  }, [dataForm]);

  const prevStep = () => {
    if (!stepperFunc.uid) {
      initStepper();
    }
    stepperFunc.goPrevious();
    setPage(1);
    // console.log(stepperFunc.getCurrentStepIndex());
  };

  const nextStep = () => {
    if (!stepperFunc.uid) {
      initStepper();
    }
    const pos = stepperFunc.getCurrentStepIndex();

    if (pos == 1) {
      setNextClicked(true);
    } else if (pos == 2) {
      setSubmitClicked(true);
    }

    const isValid = handleValidation(pos);

    if (isValid) {
      stepperFunc.goNext();
    } else {
      return;
    }

    setPage(2);
    console.log(stepperFunc.getCurrentStepIndex());
  };

  const retriveOption = async () => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/mitra/list-mitra-s3",
        {
          start: 0,
          length: 2000,
          sort: "nama_mitra",
          sort_val: "asc",
          param: "",
          status: 1,
          tahun: 0, //ganti ke current year
        },
        {
          headers: {
            Authorization: "Bearer " + Cookies.get("token"),
          },
        },
      )
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.result.Status === true) {
            let repo = response.data.result.Data;
            let ui = [
              {
                label: "Pilih Lembaga",
                value: 0,
              },
            ];
            const listItem = repo.map((number) =>
              ui.push({
                label: number.nama_mitra,
                value: number.id,
              }),
            );
            setOptioList(ui);
          } else {
            setOptioList();
          }
        }
      });
  };

  const retrivelist_catcoorp = async () => {
    axios.defaults.headers.common["Authorization"] =
      "Bearer " + Cookies.get("token");

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/masterKerjasama/list_catcoorpaktif",
        {
          start: "0",
          rows: "50",
        },
      )
      .then(function (response) {
        MySwal.close();
        if (response.status === 200) {
          if (response.data.result.Status === true) {
            let repo = response.data.result.Data;
            let ui = [
              {
                label: "Pilih Kategori Kerjasama",
                value: 0,
              },
            ];
            const listItem = repo.map((number) =>
              ui.push({
                label: number.pcooperation_categories,
                value: number.pid,
              }),
            );
            setOptioListlist_catcoorp(ui);
          } else {
            setOptioListlist_catcoorp();
          }
        }
      });
  };

  const retriveOptionStatus_kerjsama = async () => {
    const respon = axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/kerjasama/status_kerjasama_list",
        {
          start: "0",
          rows: "50",
        },
      )
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.result.Status === true) {
            let repo = response.data.result.Data;

            let ui = [];
            const listItem = repo.map((number) => {
              if (`${number.name}`.toLocaleLowerCase() == "disetujui") {
                ui.push({
                  label: number.name,
                  value: number.id,
                });
              }
            });
            setAkademi({ ...akademi, status: ui[0].value });
            setOptioList_status_kerjsama(ui);
          } else {
            setOptioList();
          }
        }
      });
  };

  const onFileChange_icon = (event) => {
    if (event.target.files[0].size >= 2000000) {
      MySwal.fire({
        title: <strong>Information!</strong>,
        html: <i>File Melebihi Ukuran</i>,
        icon: "warning",
      });
      document.querySelector("#icon").value = "";
    } else {
      setFilesIcon({ selectedFileIcon: event.target.files[0] });
    }
  };

  const handleFilter = (value) => {
    axios.defaults.headers.common["Authorization"] =
      "Bearer " + Cookies.get("token");
    const respon = axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/mitra/cari-mitra",
        {
          id: value.value,
        },
        { Authorization: "Bearer " + Cookies.get("token") },
      )
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.result.Status === true) {
            let repo = response.data.result.Data[0];
            setAkademi({ ...akademi, lembaga: value.value, email: repo.email });
          } else {
            setAkademi({ ...akademi, email: "-" });
          }
        }
      });
  };

  const handleInputTanggalAwal = (e) => {
    const { name, value } = e.currentTarget;
    if (value != "") {
      setAkademi({ ...akademi, [name]: value });
      const [dd, m, y] = value.split(" ");
      let monthNum = 1;
      /* eslint-disable no-undef */
      if (typeof id?.months?.shorthand != "undefined") {
        const shm = id?.months?.shorthand;
        for (let i = 0; i < shm.length; i++) {
          if (shm[i] == m) {
            monthNum = i + 1;
          }
        }
        if (`${monthNum}`.length == 1) {
          monthNum = `0${monthNum}`;
        } else {
          monthNum = `${monthNum}`;
        }
      }

      const tAwalM = moment(`${dd} ${monthNum} ${y}`, "D MM YYYY").locale("id");
      awal.current = tAwalM.format("YYYY-MM-DD");

      setAkademi({
        ...akademi,
        kerjasama_awal: tAwalM.format("D MMM YYYY"),
        kerjasama_akhir: tAwalM
          .add("years", akademi.periode)
          .format("D MMM YYYY"),
      });
    }
  };

  const handleInputTanggalTtd = (e) => {
    const { name, value } = e.currentTarget;
    setAkademi({ ...akademi, [name]: value });
  };

  const handleValidation = (page = 1, withNotif = true) => {
    let validation_result = true;

    setLembaga_error("");
    setPeriode_error("");
    setJudul_error("");
    setCooperation_category_id_error("");
    setStatus_error("");
    setErrorTglAwal("");
    setErrorTglAkhir("");
    setErrorTanggalPendandatanganan("");
    setErrorNomorMouPks("");
    setErrorFile("");

    if (page == 1) {
      if (akademi.lembaga == "" || akademi.lembaga == null) {
        setLembaga_error("Tidak Boleh Kosong");
        validation_result = false;
      }
      if (akademi.periode == "" || akademi.periode == null) {
        setPeriode_error("Tidak Boleh Kosong");
        validation_result = false;
      } else if (akademi.periode < 1) {
        setPeriode_error("Minimal 1 tahun");
        validation_result = false;
      }
      if (akademi.judul == "" || akademi.judul == null) {
        setJudul_error("Tidak Boleh Kosong");
        validation_result = false;
      }
      if (
        akademi.cooperation_category_id == "" ||
        akademi.cooperation_category_id == null
      ) {
        setCooperation_category_id_error("Tidak Boleh Kosong");
        validation_result = false;
      }
      if (akademi.status == "" || akademi.status == null) {
        setStatus_error("Tidak Boleh Kosong");
        validation_result = false;
      }

      if (akademi.kerjasama_awal == "" || akademi.kerjasama_awal == null) {
        setErrorTglAwal("Tidak boleh kosong");
        validation_result = false;
      }
      if (akademi.kerjasama_akhir == "" || akademi.kerjasama_akhir == null) {
        setErrorTglAkhir("Tidak boleh kosong");
        validation_result = false;
      }

      if (dataForm.length > 0) {
        let f = [...dataForm];
        let errs = [];
        let newForm = f.map((isian) => {
          if (isian?.value_text == "") {
            validation_result = false;
          }
          errs.push(isian?.value_text == "" ? "Tidak boleh kosong" : "");
          return isian?.value_text == ""
            ? { ...isian, error: "Tidak boleh kosong" }
            : { ...isian, error: "" };
        });
        setErrorDataForm(errs);
      }
    } else if (page == 2) {
      if (akademi.nomor_mou_pks == "" || akademi.nomor_mou_pks == null) {
        setErrorNomorMouPks("Tidak Boleh Kosong");
        validation_result = false;
      }
      if (akademi.kerjasama_ttd == "" || akademi.kerjasama_ttd == null) {
        setErrorTanggalPendandatanganan("Tidak boleh kosong");
        validation_result = false;
      }
      if (filesICon.selectedFileIcon == undefined) {
        setErrorFile("Tidak boleh kosong");
        validation_result = false;
      } else {
        if (filesICon.selectedFileIcon.size >= 2000000) {
          setErrorFile("Ukuran file terlalu besar");
          validation_result = false;
        }
      }
    }
    // if (validation_result == false && withNotif) {
    // 	MySwal.fire({
    // 		title: <strong>Information!</strong>,
    // 		html: <i>Maaf, Masih ada yang belum diisi</i>,
    // 		icon: 'warning'
    // 	});
    // }
    // return validation_result;
    return true;
  };

  const saveAkademi = () => {
    setSubmitClicked(true);
    if (handleValidation(stepperFunc.getCurrentStepIndex())) {
      setIsLoading(true);
      MySwal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          MySwal.showLoading();
        },
      });

      let formPostData = [];
      if (dataForm.length > 0) {
        for (let i = 0; i < dataForm.length; i++) {
          formPostData.push({
            cooperation_category_form_id: dataForm[i].id,
            cooperation_form_content: dataForm[i].value_text,
          });
        }
      }
      axios.defaults.headers.post["Content-Type"] = "multipart/form-data";
      const data = new FormData();
      data.append("tanggal", moment().locale("id").format("YYYY-MM-DD"));
      data.append("Lembaga", akademi.lembaga);
      data.append("Periode_Kerjasama", akademi.periode);
      data.append("Judul_Kerjasama", akademi.judul);
      data.append("Kategori_kerjasama", akademi.cooperation_category_id);
      data.append("Tujuan_Kerjasama", "TUJUAN KERJASAMA");
      data.append("Form_Kerjasama", akademi.deskripsi);
      data.append("Unggah_File_Final", filesICon.selectedFileIcon);
      data.append(
        "Tanggal_Tanda_Tangan",
        momentToDoStandardDate(akademi.kerjasama_ttd, "D MMM YYYY", "id"),
      );
      data.append("Nomor_Perjanjian_Kemkominfo", "");
      data.append("Nomor_Perjanjian_Lembaga", "");
      data.append("nomor_mou_pks", akademi.nomor_mou_pks);
      data.append(
        "Periode_Kerjasama_mulai",
        momentToDoStandardDate(akademi.kerjasama_awal, "D MMM YYYY", "id"),
      );
      data.append(
        "Periode_Kerjasama_selesai",
        momentToDoStandardDate(akademi.kerjasama_akhir, "D MMM YYYY", "id"),
      );
      data.append("status_kerjasama", 5);
      data.append("json_detail", JSON.stringify(formPostData));

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI +
            "/kerjasama/API_Insert_Kerjasama",
          data,
          {
            Authorization: "Bearer " + Cookies.get("token"),
            "Content-Type": "multipart/form-data",
          },
        )
        .then(function (response) {
          if (response.status === 200) {
            setIsLoading(false);
            if (response.data.result.Status === true) {
              MySwal.fire({
                title: <strong>Information!</strong>,
                html: <i>Berhasil Di Tambah</i>,
                icon: "success",
                onClose: history("/partnership/direktori_moupks"),
              });
            } else {
              MySwal.fire({
                title: <strong>Information!</strong>,
                html: <i>Gagal Di Tambah</i>,
                icon: "info",
              });
            }
          }
        })
        .catch(function (error) {
          setIsLoading(false);
          MySwal.fire({
            title: <strong>Information!</strong>,
            html: <i>{error.response.data.result.Message}</i>,
            icon: "warning",
          });
        });
    }
  };

  const handleDataForm = (e) => {
    let index = e.target.attributes.index.value;
    let fieldValue = e.target.value;
    dataForm.splice(index, 0);
    const tempRows = [...dataForm];
    const tempObj = dataForm[index];
    tempObj["value_text"] = fieldValue;
    tempRows[index] = tempObj;

    setDataForm(tempRows);
  };

  const momentToDoStandardDate = (date, format, locale) => {
    if (locale) {
      return moment(date, format, locale).locale("id").format("YYYY-MM-DD");
    } else {
      return moment(date, format).locale("id").format("YYYY-MM-DD");
    }
  };

  useEffect(() => {
    recheckValidation();
  }, [akademi, filesICon]);

  useEffect(() => {
    // console.log(akademi.periode)
    const { periode, kerjasama_awal, kerjasama_akhir } = akademi;
    // console.log(kerjasama_awal)
    const ta = moment(kerjasama_awal, "DD MMM YYYY", "id");
    // console.log(ta.isValid(), !isNaN(periode))
    if (ta.isValid() && !isNaN(periode)) {
      console.log("masuk");
      setAkademi((a) => ({
        ...a,
        kerjasama_akhir: ta
          .add("years", periode)
          .locale("id")
          .format("DD MMM YYYY"),
      }));
    }
  }, [akademi.periode]);

  const recheckValidation = () => {
    if (stepperFunc) {
      const pos = stepperFunc.getCurrentStepIndex();
      if (nextClicked && pos == 1) {
        handleValidation(pos, false);
      } else if (submitClicked && pos == 2) {
        handleValidation(pos, false);
      }
    }
  };

  return (
    <div>
      <Header />
      <SideNav />
      <div className="toolbar" id="kt_toolbar">
        <div
          id="kt_toolbar_container"
          className="container-fluid d-flex flex-stack my-2"
        >
          <div className="d-flex align-items-start my-2">
            <div>
              <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                <span className="me-3">
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                    className="mh-50px"
                  >
                    <path
                      opacity="0.3"
                      d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z"
                      fill="#FFC700"
                    ></path>
                    <path
                      d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z"
                      fill="#FFC700"
                    ></path>
                  </svg>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Partnership
                  <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                </span>
                Direktori Mou/PKS
              </h1>
            </div>
          </div>
          <div className="d-flex align-items-end my-2">
            <div>
              <button
                onClick={() => window.history.back()}
                type="reset"
                className="btn btn-sm btn-light btn-active-light-primary"
                data-kt-menu-dismiss="true"
              >
                <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                  <i className="fa fa-chevron-left"></i>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Kembali
                </span>
              </button>
            </div>
          </div>
        </div>
      </div>
      <div
        className="wrapper d-flex flex-column flex-row-fluid pt-0"
        id="kt_wrapper"
      >
        <div
          className="content d-flex flex-column flex-column-fluid pt-0"
          id="kt_content"
        >
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container-xxl">
              <div className="row">
                <div
                  className="stepper stepper-links mb-n3"
                  id="kt_stepper_example_basic"
                >
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <div className="stepper-nav flex-wrap mb-n4">
                            <div
                              className="stepper-item ms-0 me-3 my-2 current"
                              data-kt-stepper-element="nav"
                              data-kt-stepper-action="step"
                            >
                              <div className="stepper-wrapper d-flex align-items-center">
                                <div className="stepper-label ms-0">
                                  <h3 className="stepper-title fs-6">
                                    Informasi MoU/PKS
                                  </h3>
                                </div>
                              </div>
                            </div>
                            <div
                              className="stepper-item mx-3 my-2"
                              data-kt-stepper-element="nav"
                              data-kt-stepper-action="step"
                            >
                              <div className="stepper-wrapper d-flex align-items-center">
                                <div className="stepper-label">
                                  <h3 className="stepper-title fs-6">
                                    Lampiran MoU/PKS
                                  </h3>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <form
                          className="form"
                          noValidate="novalidate"
                          id="kt_stepper_example_basic_form"
                        >
                          <div className="mb-5">
                            <div
                              className="flex-column current"
                              data-kt-stepper-element="content"
                            >
                              <div className="form-group fv-row mb-7">
                                <label
                                  className="form-label required"
                                  htmlFor="description"
                                >
                                  Judul Kerjasama
                                </label>
                                <div className="input-group">
                                  <input
                                    type="text"
                                    name="judul"
                                    className="form-control form-control-sm"
                                    placeholder="Masukan Judul Kerjasama"
                                    aria-describedby="basic-addon2"
                                    value={akademi.judul}
                                    onChange={handleInputChange}
                                  />
                                </div>
                                <span style={{ color: "red" }}>
                                  {judul_error}
                                </span>
                              </div>
                              <div className="form-group fv-row mb-7">
                                <label
                                  className="form-label required"
                                  htmlFor="title"
                                >
                                  Kategori Kerjasama
                                </label>
                                <Select
                                  name="status"
                                  placeholder="Silahkan pilih"
                                  className="form-select-sm selectpicker p-0"
                                  defaultValue={akademi.cooperation_category_id}
                                  isOptionSelected={
                                    akademi.cooperation_category_id ===
                                    optionListlist_catcoorp
                                  }
                                  options={optionListlist_catcoorp}
                                  onChange={(e) =>
                                    handleChangecooperation_category_id(e)
                                  }
                                />
                                <span style={{ color: "red" }}>
                                  {cooperation_category_id_error}
                                </span>
                              </div>
                              {dataForm.length != 0 &&
                                dataForm.map((item, idx) => (
                                  <React.Fragment key={idx}>
                                    <div className="form-group fv-row ">
                                      <label
                                        className="form-label required"
                                        htmlFor="title"
                                      >
                                        {item.cooperation_form}
                                      </label>
                                      <input
                                        type="text"
                                        className="form-control form-control-sm"
                                        name={item.cooperation_form}
                                        index={idx}
                                        placeholder={item.cooperation_form}
                                        value={dataForm[idx][item]}
                                        onChange={(e) => handleDataForm(e)}
                                      ></input>
                                    </div>

                                    <span
                                      className="mb-7"
                                      style={{ color: "red" }}
                                    >
                                      {errorDataForm[idx]}
                                    </span>
                                  </React.Fragment>
                                ))}
                              <div className="form-group fv-row mb-3">
                                <label
                                  className="form-label required"
                                  htmlFor="title"
                                >
                                  Mitra
                                </label>
                                <Select
                                  name="level_pelatihan"
                                  placeholder="Silahkan Pilih Mitra"
                                  noOptionsMessage={({ inputValue }) =>
                                    !inputValue
                                      ? optionList.value
                                      : "Data tidak tersedia"
                                  }
                                  value={optionList.lembaga}
                                  className="form-select-sm selectpicker p-0 mb-0"
                                  options={optionList}
                                  onChange={(value) => handleFilter(value)}
                                />
                                <span className="ms-3" style={{ color: "red" }}>
                                  {lembaga_error}
                                </span>
                              </div>
                              <div className="form-group fv-row mb-7">
                                <label
                                  className="form-label required"
                                  htmlFor="title"
                                >
                                  Email
                                </label>
                                <br />
                                <label className="form-label" htmlFor="title">
                                  {akademi.email}
                                </label>
                              </div>
                              <div className="form-group fv-row mb-7">
                                <div className="row">
                                  <h5 className="mb-5">Periode Kerjasama</h5>
                                  <div className="col-12 mb-5">
                                    <label
                                      className="form-label required"
                                      htmlFor="description"
                                    >
                                      Durasi MoU/PKS (Dalam Tahun)
                                    </label>
                                    <div className="row col-lg-12">
                                      <div className="input-group">
                                        <input
                                          type="number"
                                          min={1}
                                          className="form-control form-control-sm"
                                          placeholder="Masukan Lama Kerjasama"
                                          aria-describedby="basic-addon2"
                                          name="periode"
                                          value={akademi.periode}
                                          onChange={handleInputChange}
                                        />
                                        <div className="input-group-append">
                                          <span className="input-group-text">
                                            Tahun
                                          </span>
                                        </div>
                                      </div>
                                      <span style={{ color: "red" }}>
                                        {periode_error}
                                      </span>
                                    </div>
                                  </div>
                                  <div className="col-6">
                                    <label
                                      className="form-label required"
                                      htmlFor="title"
                                    >
                                      Tgl. Mulai
                                    </label>
                                    <br />
                                    <div className="input-group">
                                      <input
                                        id="kerjasama_awal"
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan tanggal mulai MoU/PKS"
                                        name="kerjasama_awal"
                                        onFocus={(e) => {
                                          handleInputTanggalAwal(e);
                                        }}
                                      />
                                    </div>
                                    <span
                                      className="mb-7"
                                      style={{ color: "red" }}
                                    >
                                      {error_tgl_awal}
                                    </span>
                                  </div>
                                  <div className="col-6">
                                    <label
                                      className="form-label required"
                                      htmlFor="title"
                                    >
                                      Tgl. Akhir
                                    </label>
                                    <br />
                                    <div className="input-group  date">
                                      <input
                                        id="kerjasama_akhir"
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan tanggal selesai MoU/PKS"
                                        name="kerjasama_akhir"
                                        value={akademi.kerjasama_akhir}
                                        disabled
                                      />
                                    </div>
                                    <span
                                      className="mb-7"
                                      style={{ color: "red" }}
                                    >
                                      {error_tgl_akhir}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div
                              className="flex-column"
                              data-kt-stepper-element="content"
                            >
                              <div className="row mb-5">
                                <div className="form-group fv-row mb-7">
                                  <label
                                    className="form-label required"
                                    htmlFor="title"
                                  >
                                    Nomor MOU/PKS
                                  </label>
                                  <br />
                                  <input
                                    type="text"
                                    className="form-control form-control-sm"
                                    placeholder="Masukan Nomor MOU/PKS"
                                    aria-describedby="basic-addon2"
                                    name="nomor_mou_pks"
                                    value={akademi.nomor_mou_pks}
                                    onChange={handleInputChange}
                                  />
                                  <span
                                    className="mb-7"
                                    style={{ color: "red" }}
                                  >
                                    {error_nomor_mou_pks}
                                  </span>
                                </div>
                                <div className="form-group fv-row mb-7">
                                  <label
                                    className="form-label required"
                                    htmlFor="title"
                                  >
                                    Tgl. Penandatanganan
                                  </label>
                                  <br />
                                  <div className="input-group date">
                                    <input
                                      id="kerjasama_ttd"
                                      className="form-control form-control-sm"
                                      placeholder="Masukkan tanggal penandatanganan"
                                      name="kerjasama_ttd"
                                      onFocus={(e) => {
                                        handleInputTanggalTtd(e);
                                      }}
                                    />
                                  </div>
                                  <span
                                    className="mb-7"
                                    style={{ color: "red" }}
                                  >
                                    {error_tgl_penandatanganan}
                                  </span>
                                </div>
                                <div className="form-group fv-row mb-7">
                                  <label
                                    className="form-label required"
                                    htmlFor="title"
                                  >
                                    Dokumen MoU/PKS
                                  </label>
                                  <br />
                                  <div className="input-group">
                                    <input
                                      type="file"
                                      className="form-control form-control-sm font-size-h4"
                                      name="icon"
                                      onChange={onFileChange_icon}
                                      id="icon"
                                      accept="application/pdf"
                                    />
                                  </div>
                                  <span className="text-muted font-size-sm">
                                    File yang diupload *.pdf dengan ukuran
                                    maksimal 2 MB
                                  </span>
                                  <br />
                                  <span
                                    className="mb-7"
                                    style={{ color: "red" }}
                                  >
                                    {error_file}
                                  </span>
                                </div>
                                <div className="form-group fv-row mb-7">
                                  <label
                                    className="form-label required"
                                    htmlFor="title"
                                  >
                                    Tgl. Input
                                  </label>
                                  <p className="fs-7">{akademi.tanggal + ""}</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="form-group fv-row pt-7 mb-7">
                            <div className="d-flex justify-content-center mb-7">
                              <div className="me-2">
                                <button
                                  //onClick={() => history("/partnership/direktori_moupks/")}
                                  type="button"
                                  className="btn btn-md btn-secondary me-3"
                                  data-kt-stepper-action="previous"
                                  onClick={prevStep}
                                >
                                  <i className="fa fa-chevron-left me-1"></i>
                                  Sebelumnya
                                </button>
                              </div>
                              <div>
                                <button
                                  onClick={saveAkademi}
                                  type="button"
                                  className="btn btn-primary btn-md"
                                  data-kt-stepper-action="submit"
                                  disabled={isLoading}
                                >
                                  {isLoading ? (
                                    <>
                                      <span
                                        className="spinner-border spinner-border-sm me-2"
                                        role="status"
                                        aria-hidden="true"
                                      ></span>
                                      <span className="sr-only">
                                        Loading...
                                      </span>
                                      Loading...
                                    </>
                                  ) : (
                                    <>
                                      <i className="fa fa-paper-plane me-1"></i>
                                      Simpan
                                    </>
                                  )}
                                </button>

                                {page == 1 ? (
                                  <button
                                    // onClick={() => history("/partnership/direktori_moupks/")}
                                    type="button"
                                    className={`btn btn-primary ${
                                      isDisabledNext ? "disabled" : ""
                                    }`}
                                    disabled={isDisabledNext}
                                    onClick={nextStep}
                                  >
                                    Selanjutnya{" "}
                                    <i className="fa fa-chevron-right ms-1"></i>
                                  </button>
                                ) : (
                                  ""
                                )}
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </div>
  );
};

export default AddKerjasamadisetujui;
