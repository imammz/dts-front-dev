import React from "react";

const Breadcumb = () => {
  let segment_url = window.location.pathname.split("/");
  let urlElelementOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  console.log(segment_url);
  return (
    <div>
      <div
        className="wrapper d-flex flex-column flex-row-fluid"
        id="kt_wrapper"
        style={{ paddingTop: 8 }}
      >
        <div
          className="content d-flex flex-column flex-column-fluid"
          id="kt_content"
        >
          <div className="toolbar" id="kt_toolbar">
            <div
              id="kt_toolbar_container"
              className="container-fluid d-flex flex-stack"
            >
              <div
                data-kt-swapper="true"
                data-kt-swapper-mode="prepend"
                data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                className="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0"
              >
                {/*begin::Title*/}
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-3 my-1">
                  {urlElelementOne}
                </h1>
                {/*end::Title*/}
                {/*begin::Separator*/}
                <span className="h-20px border-gray-200 border-start mx-4" />
                {/*end::Separator*/}
                {/*begin::Breadcrumb*/}
                <ul className="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                  {/*begin::Item*/}
                  <li className="breadcrumb-item text-muted">
                    <a
                      href="/dashboard/digitalent"
                      className="text-muted text-hover-primary"
                    >
                      {urlSegmentZero}
                    </a>
                  </li>
                  {/*end::Item*/}
                  {/*begin::Item*/}
                  <li className="breadcrumb-item">
                    <span className="bullet bg-gray-200 w-5px h-2px" />
                  </li>
                  {/*end::Item*/}
                  {/*begin::Item*/}
                  <li className="breadcrumb-item text-muted">
                    {urlElelementOne}
                  </li>
                  {/*end::Item*/}
                  {/*begin::Item*/}
                  <li className="breadcrumb-item">
                    <span className="bullet bg-gray-200 w-5px h-2px" />
                  </li>
                  {/*end::Item*/}
                </ul>
                {/*end::Breadcrumb*/}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Breadcumb;
