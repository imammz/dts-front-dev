import React from "react";

const DashboardSKMContent = () => {
  return (
    <div>
      <div className="toolbar" id="kt_toolbar">
        <div
          id="kt_toolbar_container"
          className="container-fluid d-flex flex-stack my-2"
        >
          <div className="d-flex align-items-start my-2">
            <div>
              <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                <span className="me-3">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="#50cd89"
                  >
                    <rect
                      x="2"
                      y="2"
                      width="9"
                      height="9"
                      rx="2"
                      fill="50cd89"
                    ></rect>
                    <rect
                      opacity="0.3"
                      x="13"
                      y="2"
                      width="9"
                      height="9"
                      rx="2"
                      fill="50cd89"
                    ></rect>
                    <rect
                      opacity="0.3"
                      x="13"
                      y="13"
                      width="9"
                      height="9"
                      rx="2"
                      fill="50cd89"
                    ></rect>
                    <rect
                      opacity="0.3"
                      x="2"
                      y="13"
                      width="9"
                      height="9"
                      rx="2"
                      fill="50cd89"
                    ></rect>
                  </svg>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Dashboard
                  <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                </span>
                Kepuasan Layanan
              </h1>
            </div>
          </div>

          <div className="d-flex align-items-end my-2"></div>
        </div>
      </div>
      <div
        className="wrapper d-flex flex-column flex-row-fluid pt-5"
        id="kt_wrapper"
      >
        <iframe
          src="https://data.sdmdigital.id/t/Publish/views/SurveyLayananWeb/DashbaordUtamasurvey?%3Aembed=y&:isGuestRedirectFromVizportal=y&:display_count=n&:showAppBanner=false&:origin=viz_share_link&:showVizHome=n"
          style={{ width: "100%", height: "1600px", padding: "0", margin: "0" }}
          allowFullScreen
        ></iframe>
      </div>
    </div>
  );
};

export default DashboardSKMContent;
