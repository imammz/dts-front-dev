import React, { useEffect } from "react";
import Header from "../Header";
import Content from "./diploy-content";
import SideNav from "../SideNav";
import Footer from "../Footer";
import { cekPermition, logout } from "../AksesHelper";
import Swal from "sweetalert2";

const Diploy = () => {
  useEffect(() => {
    if (cekPermition().view !== 1) {
      // Swal.fire({
      //     title: "Akses Tidak Diizinkan",
      //     html: "<i> Anda akan kembali kehalaman login </i>",
      //     icon: "warning",
      //   }).then(()=>{
      //     logout();
      //   });
    }
  }, []);

  return (
    <div>
      <SideNav />
      <Header />
      <Content />
      <Footer />
    </div>
  );
};

export default Diploy;
