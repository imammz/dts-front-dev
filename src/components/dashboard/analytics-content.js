import React from "react";
import { dateRange, handleFormatDate } from "../pelatihan/Pelatihan/helper";
import RenderFormElement from "../pelatihan/Pelatihan/RenderFormElement";

const AnalyticsContent = () => {
  return (
    <div>
      <div className="toolbar" id="kt_toolbar">
        <div
          id="kt_toolbar_container"
          className="container-fluid d-flex flex-stack my-2"
        >
          <div className="d-flex align-items-start my-2">
            <div>
              <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                <span className="me-3">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="#50cd89"
                  >
                    <rect
                      x="2"
                      y="2"
                      width="9"
                      height="9"
                      rx="2"
                      fill="50cd89"
                    ></rect>
                    <rect
                      opacity="0.3"
                      x="13"
                      y="2"
                      width="9"
                      height="9"
                      rx="2"
                      fill="50cd89"
                    ></rect>
                    <rect
                      opacity="0.3"
                      x="13"
                      y="13"
                      width="9"
                      height="9"
                      rx="2"
                      fill="50cd89"
                    ></rect>
                    <rect
                      opacity="0.3"
                      x="2"
                      y="13"
                      width="9"
                      height="9"
                      rx="2"
                      fill="50cd89"
                    ></rect>
                  </svg>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Dashboard
                  <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                </span>
                Analytics
              </h1>
            </div>
          </div>

          <div className="d-flex align-items-end my-2"></div>
        </div>
      </div>
      <div
        className="wrapper d-flex flex-column flex-row-fluid pt-0"
        id="kt_wrapper"
      >
        <div
          className="content d-flex flex-column flex-column-fluid pt-0"
          id="kt_content"
        >
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container-xxl">
              <div className="col-lg-12 mt-5">
                <div className="card border">
                  <div className="card-header">
                    <div className="card-title">
                      <div className="d-flex p-0 mb-n4">
                        <ul
                          className="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap mb-n4"
                          style={{
                            overflowX: "auto",
                            overflowY: "hidden",
                            display: "flex",
                            whiteSpace: "nowrap",
                            marginBottom: 0,
                          }}
                        >
                          <li className="nav-item">
                            <a
                              className="nav-link text-dark text-active-primary active"
                              data-bs-toggle="tab"
                              href="#kt_tab_pane_1"
                            >
                              <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                Digitalent
                              </span>
                            </a>
                          </li>
                          <li className="nav-item">
                            <a
                              className="nav-link text-dark text-active-primary false"
                              data-bs-toggle="tab"
                              href="#kt_tab_pane_2"
                            >
                              <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                Diploy
                              </span>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div className="card-body pt-0">
                    <div className="tab-content" id="detail-account-tab">
                      {/* tab 1 */}
                      <div
                        className="tab-pane fade show active"
                        id="kt_tab_pane_1"
                        role="tabpanel"
                      >
                        <div className="row mt-7 pb-7">
                          <iframe
                            src="https://datastudio.google.com/embed/reporting/d855d7e1-d297-4807-b0ed-90f500844104/page/e6n7C"
                            style={{
                              height: "900px",
                              padding: "0",
                              margin: "0",
                              width: "100%",
                            }}
                            allowFullScreen
                          ></iframe>
                        </div>
                      </div>

                      <div
                        className="tab-pane fade"
                        id="kt_tab_pane_2"
                        role="tabpanel"
                      >
                        <div className="row mt-7 pt-5 pb-7">
                          <iframe
                            src="https://datastudio.google.com/embed/reporting/00eb1b46-fca3-4cb3-870b-6c95054c6e50/page/e6n7C"
                            style={{
                              height: "900px",
                              padding: "0",
                              margin: "0",
                              width: "100%",
                            }}
                            allowFullScreen
                          ></iframe>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/*end::Card body*/}
              </div>
            </div>
            {/*end::Container*/}
          </div>
          {/*end::Post*/}
        </div>
      </div>
    </div>
  );
};

export default AnalyticsContent;
