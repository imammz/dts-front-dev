import React from "react";

const Footer = () => {
  return (
    <div>
      <div
        className="wrapper d-flex flex-column flex-row-fluid"
        id="kt_wrapper"
      >
        <div
          className="footer py-4 d-flex flex-lg-column"
          id="kt_footer"
          style={{
            position: "fixed",
            width: "100%",
            bottom: "0",
            paddingTop: 8,
          }}
        >
          {/*begin::Container*/}
          <div className="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
            {/*begin::Copyright*/}
            <div className="text-dark order-2 order-md-1">
              <a href="#" className="text-muted text-hover-primary">
                Badan Pengembangan SDM Kominfo{" "}
                <span className="text-muted fw-bold me-1">
                  © {new Date().getFullYear()}
                </span>{" "}
              </a>
            </div>
            {/*end::Copyright*/}
            {/*begin::Menu*/}
            {/*end::Menu*/}
          </div>
          {/*end::Container*/}
        </div>
      </div>
    </div>
  );
};

export default Footer;
