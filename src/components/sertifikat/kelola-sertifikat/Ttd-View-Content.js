import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import { handleFormatDate } from "../../pelatihan/Pelatihan/helper";
import _ from "lodash";
import { numericOnly } from "../../publikasi/helper";
import Select from "react-select";

export default class TTEContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleUpdateCert = this.handleUpdateCertAction.bind(this);
  }
  state = {
    tteData: {},
    sendData: {},
    view: true,
    errors: [],
    isLoading: false,
  };
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    this.handleReload();
  }
  emptyValidation(value, key, isFile = false) {
    const errors = this.state.errors;
    if (value == "" || value == null) {
      errors[key] = "Tidak boleh kosong";
      this.setState({ errors });
      return false;
    } else {
      delete errors[key];
      this.setState({ errors });
      return true;
    }
  }

  handleReload() {
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/sertifikat/get-tte",
        {},
        this.configs,
      )
      .then((res) => {
        swal.close();
        if (res.data.result.Status) {
          this.setState(
            {
              tteData: res.data.result.Data,
              sendData: res.data.result.Data,
              // fileName: res.data.result.Data.p12?.split("/")[1],
            },
            () => {
              delete this.state.sendData["updated_at"];
              // delete this.state.sendData["password"];
            },
          );
        } else {
          throw Error(res.data.result.Message);
        }
      })
      .catch((err) => {
        const message =
          typeof err == "string" ? err : err.response.data.message;
        swal.fire({
          icon: "warning",
          title: message ?? "Terjadi kesalahan",
          confirmButtonText: "Ok",
        });
      });
  }

  validate(formElement) {
    const inputs = formElement.querySelectorAll("input");
    inputs.forEach((input) => {
      input.focus();
      input.blur();
    });
    return Object.keys(this.state.errors).length == 0;
  }

  handleUpdateCertAction(e) {
    e.preventDefault();
    const dataForm = new FormData(e.currentTarget);
    this.setState({ isLoading: true });
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    const formData = new FormData();
    formData.append("name", dataForm.get("name"));
    formData.append("position", dataForm.get("position"));
    formData.append("nik", dataForm.get("nik"));
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/sertifikat/update-tte",
        formData,
        this.configs,
      )
      .then((res) => {
        if (res.data.result.Status) {
          this.setState({ isLoading: false });
          this.setState({ tteData: res.data.result.Data });
          swal
            .fire({
              icon: "success",
              title: res.data.result.Message ?? "Berhasil update TTE.",
              confirmButtonText: "Ok",
            })
            .then((act) => {
              this.handleReload();
              this.setState({ view: true });
            });
        } else {
          this.setState({ isLoading: false });
          swal.fire({
            icon: "warning",
            title: res.data.result.Message,
            confirmButtonText: "Ok",
          });
        }
      })
      .catch((err) => {
        this.setState({ isLoading: false });
        const message =
          typeof err == "string" ? err : err.response?.data.message;
        swal.fire({
          icon: "warning",
          title: message ?? "Terjadi kesalahan",
          confirmButtonText: "Ok",
        });
      });
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24px"
                      height="24px"
                      viewBox="0 0 24 24"
                      className="mh-50px"
                    >
                      <path
                        d="M10.0813 3.7242C10.8849 2.16438 13.1151 2.16438 13.9187 3.7242V3.7242C14.4016 4.66147 15.4909 5.1127 16.4951 4.79139V4.79139C18.1663 4.25668 19.7433 5.83365 19.2086 7.50485V7.50485C18.8873 8.50905 19.3385 9.59842 20.2758 10.0813V10.0813C21.8356 10.8849 21.8356 13.1151 20.2758 13.9187V13.9187C19.3385 14.4016 18.8873 15.491 19.2086 16.4951V16.4951C19.7433 18.1663 18.1663 19.7433 16.4951 19.2086V19.2086C15.491 18.8873 14.4016 19.3385 13.9187 20.2758V20.2758C13.1151 21.8356 10.8849 21.8356 10.0813 20.2758V20.2758C9.59842 19.3385 8.50905 18.8873 7.50485 19.2086V19.2086C5.83365 19.7433 4.25668 18.1663 4.79139 16.4951V16.4951C5.1127 15.491 4.66147 14.4016 3.7242 13.9187V13.9187C2.16438 13.1151 2.16438 10.8849 3.7242 10.0813V10.0813C4.66147 9.59842 5.1127 8.50905 4.79139 7.50485V7.50485C4.25668 5.83365 5.83365 4.25668 7.50485 4.79139V4.79139C8.50905 5.1127 9.59842 4.66147 10.0813 3.7242V3.7242Z"
                        fill="#50cd89"
                      ></path>
                      <path
                        className="permanent"
                        d="M14.8563 9.1903C15.0606 8.94984 15.3771 8.9385 15.6175 9.14289C15.858 9.34728 15.8229 9.66433 15.6185 9.9048L11.863 14.6558C11.6554 14.9001 11.2876 14.9258 11.048 14.7128L8.47656 12.4271C8.24068 12.2174 8.21944 11.8563 8.42911 11.6204C8.63877 11.3845 8.99996 11.3633 9.23583 11.5729L11.3706 13.4705L14.8563 9.1903Z"
                        fill="white"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Sertifikat
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Kelola Sertifikat
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <a
                  href="/sertifikat/upload-background"
                  className="btn btn-primary fw-bolder btn-sm"
                >
                  <i className="bi bi-gear"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Template Sertifikat
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Kelola Sertifikat
                          </h1>
                        </div>
                      </div>
                      <div className="card-body">
                        <form action="" onSubmit={this.handleUpdateCert}>
                          <div className="row align-items-center justify-content-center">
                            <div className="col-lg-6">
                              <label
                                className="btn btn-outline btn-outline-dashed btn-outline-default p-7 d-flex align-items-center mb-10"
                                for="kt_create_account_form_account_type_personal"
                              >
                                <span className="svg-icon svg-icon-primary svg-icon-3x me-5">
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24px"
                                    height="24px"
                                    viewBox="0 0 24 24"
                                    version="1.1"
                                  >
                                    <g
                                      stroke="none"
                                      stroke-width="1"
                                      fill="none"
                                      fill-rule="evenodd"
                                    >
                                      <rect
                                        x="0"
                                        y="0"
                                        width="24"
                                        height="24"
                                      />
                                      <path
                                        d="M4,4 L11.6314229,2.5691082 C11.8750185,2.52343403 12.1249815,2.52343403 12.3685771,2.5691082 L20,4 L20,13.2830094 C20,16.2173861 18.4883464,18.9447835 16,20.5 L12.5299989,22.6687507 C12.2057287,22.8714196 11.7942713,22.8714196 11.4700011,22.6687507 L8,20.5 C5.51165358,18.9447835 4,16.2173861 4,13.2830094 L4,4 Z"
                                        fill="#000000"
                                        opacity="0.3"
                                      />
                                      <path
                                        d="M12,11 C10.8954305,11 10,10.1045695 10,9 C10,7.8954305 10.8954305,7 12,7 C13.1045695,7 14,7.8954305 14,9 C14,10.1045695 13.1045695,11 12,11 Z"
                                        fill="#000000"
                                        opacity="0.3"
                                      />
                                      <path
                                        d="M7.00036205,16.4995035 C7.21569918,13.5165724 9.36772908,12 11.9907452,12 C14.6506758,12 16.8360465,13.4332455 16.9988413,16.5 C17.0053266,16.6221713 16.9988413,17 16.5815,17 C14.5228466,17 11.463736,17 7.4041679,17 C7.26484009,17 6.98863236,16.6619875 7.00036205,16.4995035 Z"
                                        fill="#000000"
                                        opacity="0.3"
                                      />
                                    </g>
                                  </svg>
                                </span>
                                <div className="d-block fw-bold text-start w-100">
                                  <span className="text-dark fw-bolder d-block fs-7">
                                    Nama
                                  </span>
                                  {this.state.view ? (
                                    <span className="text-muted fw-bold fs-7">
                                      {this.state.tteData.name}
                                    </span>
                                  ) : (
                                    <div className="row">
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan nama disini"
                                        name="name"
                                        defaultValue={this.state.tteData.name}
                                        onBlur={(e) => {
                                          this.emptyValidation(
                                            e.target.value,
                                            "name",
                                          );
                                        }}
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["name"]}
                                      </span>
                                    </div>
                                  )}
                                </div>
                              </label>
                            </div>
                            <div className="col-lg-6">
                              <label
                                className="btn btn-outline btn-outline-dashed btn-outline-default p-7 d-flex align-items-center mb-10"
                                for="kt_create_account_form_account_type_personal"
                              >
                                <span className="svg-icon svg-icon-primary svg-icon-3x me-5">
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24px"
                                    height="24px"
                                    viewBox="0 0 24 24"
                                    version="1.1"
                                  >
                                    <g
                                      stroke="none"
                                      stroke-width="1"
                                      fill="none"
                                      fill-rule="evenodd"
                                    >
                                      <rect
                                        x="0"
                                        y="0"
                                        width="24"
                                        height="24"
                                      />
                                      <path
                                        d="M14.1124454,7.00625159 C14.0755336,7.00212117 14.0380145,7 14,7 L10,7 C9.96198549,7 9.92446641,7.00212117 9.88755465,7.00625159 L7.34761705,4.55799196 C6.95060373,4.17530866 6.9382927,3.54346816 7.32009765,3.14561006 L8.41948359,2 L15.5805164,2 L16.6799023,3.14561006 C17.0617073,3.54346816 17.0493963,4.17530866 16.6523829,4.55799196 L14.1124454,7.00625159 Z"
                                        fill="#000000"
                                      />
                                      <path
                                        d="M13.7640285,9 L15.4853424,18.1494183 C15.5450675,18.4668794 15.4477627,18.7936387 15.2240963,19.0267093 L12.7215131,21.6345146 C12.7120098,21.6444174 12.7023037,21.6541236 12.6924008,21.6636269 C12.2939201,22.0460293 11.6608893,22.0329953 11.2784869,21.6345146 L8.77590372,19.0267093 C8.55223728,18.7936387 8.45493249,18.4668794 8.5146576,18.1494183 L10.2359715,9 L13.7640285,9 Z"
                                        fill="#000000"
                                        opacity="0.3"
                                      />
                                    </g>
                                  </svg>
                                </span>
                                <div className="d-block fw-bold text-start w-100">
                                  <span className="text-dark fw-bolder d-block fs-7">
                                    Jabatan
                                  </span>
                                  {this.state.view ? (
                                    <span className="text-muted fw-bold fs-7">
                                      {this.state.tteData.position}
                                    </span>
                                  ) : (
                                    <div className="row">
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan Jabatan"
                                        name="position"
                                        defaultValue={
                                          this.state.tteData.position
                                        }
                                        onBlur={(e) => {
                                          this.emptyValidation(
                                            e.target.value,
                                            "position",
                                          );
                                        }}
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["position"]}
                                      </span>
                                    </div>
                                  )}
                                </div>
                              </label>
                            </div>
                            <div className="col-lg-6">
                              <label
                                className="btn btn-outline btn-outline-dashed btn-outline-default p-7 d-flex align-items-center mb-10"
                                for="kt_create_account_form_account_type_personal"
                              >
                                <span className="svg-icon svg-icon-primary svg-icon-3x me-5">
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24px"
                                    height="24px"
                                    viewBox="0 0 24 24"
                                    version="1.1"
                                  >
                                    <g
                                      stroke="none"
                                      stroke-width="1"
                                      fill="none"
                                      fill-rule="evenodd"
                                    >
                                      <rect
                                        x="0"
                                        y="0"
                                        width="24"
                                        height="24"
                                      />
                                      <path
                                        d="M3.5,21 L20.5,21 C21.3284271,21 22,20.3284271 22,19.5 L22,8.5 C22,7.67157288 21.3284271,7 20.5,7 L10,7 L7.43933983,4.43933983 C7.15803526,4.15803526 6.77650439,4 6.37867966,4 L3.5,4 C2.67157288,4 2,4.67157288 2,5.5 L2,19.5 C2,20.3284271 2.67157288,21 3.5,21 Z"
                                        fill="#000000"
                                        opacity="0.3"
                                      />
                                      <path
                                        d="M14.5,13 C15.0522847,13 15.5,13.4477153 15.5,14 L15.5,17 C15.5,17.5522847 15.0522847,18 14.5,18 L9.5,18 C8.94771525,18 8.5,17.5522847 8.5,17 L8.5,14 C8.5,13.4477153 8.94771525,13 9.5,13 L9.5,12.5 C9.5,11.1192881 10.6192881,10 12,10 C13.3807119,10 14.5,11.1192881 14.5,12.5 L14.5,13 Z M12,11 C11.1715729,11 10.5,11.6715729 10.5,12.5 L10.5,13 L13.5,13 L13.5,12.5 C13.5,11.6715729 12.8284271,11 12,11 Z"
                                        fill="#000000"
                                      />
                                    </g>
                                  </svg>
                                </span>
                                <div className="d-block fw-bold text-start w-100">
                                  <span className="text-dark fw-bolder d-block fs-7">
                                    NIK
                                  </span>
                                  {this.state.view ? (
                                    <span className="text-muted fw-bold fs-7">
                                      <div>
                                        {this.state.tteData.nik
                                          ? this.state.tteData.nik
                                          : "Nomor Induk Kependudukan"}{" "}
                                      </div>
                                    </span>
                                  ) : (
                                    <div className="row">
                                      <input
                                        className="form-control form-control-sm"
                                        type="text"
                                        id="nik"
                                        name="nik"
                                        defaultValue={this.state.tteData.nik}
                                        onChange={(e) => {
                                          if (
                                            this.emptyValidation(
                                              e.target.value,
                                              "nik",
                                            )
                                          ) {
                                            numericOnly(e.target);
                                            this.setState({
                                              sendData: {
                                                ...this.state.sendData,
                                                nik: e.target.value,
                                              },
                                            });
                                          }
                                        }}
                                      />
                                      <span style={{ color: "red" }}>
                                        {this.state.errors["nik"]}
                                      </span>
                                    </div>
                                  )}
                                </div>
                              </label>
                            </div>
                            {/* <div className="col-lg-6">
                              <label
                                className="btn btn-outline btn-outline-dashed btn-outline-default p-7 d-flex align-items-center mb-10"
                                for="kt_create_account_form_account_type_personal"
                              >
                                {this.state.view ? (
                                  <>
                                    <span className="svg-icon svg-icon-primary svg-icon-3x me-5">
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24px"
                                        height="24px"
                                        viewBox="0 0 24 24"
                                        version="1.1"
                                      >
                                        <g
                                          stroke="none"
                                          stroke-width="1"
                                          fill="none"
                                          fill-rule="evenodd"
                                        >
                                          <rect
                                            x="0"
                                            y="0"
                                            width="24"
                                            height="24"
                                          />
                                          <path
                                            d="M12,22 C7.02943725,22 3,17.9705627 3,13 C3,8.02943725 7.02943725,4 12,4 C16.9705627,4 21,8.02943725 21,13 C21,17.9705627 16.9705627,22 12,22 Z"
                                            fill="#000000"
                                            opacity="0.3"
                                          />
                                          <path
                                            d="M11.9630156,7.5 L12.0475062,7.5 C12.3043819,7.5 12.5194647,7.69464724 12.5450248,7.95024814 L13,12.5 L16.2480695,14.3560397 C16.403857,14.4450611 16.5,14.6107328 16.5,14.7901613 L16.5,15 C16.5,15.2109164 16.3290185,15.3818979 16.1181021,15.3818979 C16.0841582,15.3818979 16.0503659,15.3773725 16.0176181,15.3684413 L11.3986612,14.1087258 C11.1672824,14.0456225 11.0132986,13.8271186 11.0316926,13.5879956 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z"
                                            fill="#000000"
                                          />
                                        </g>
                                      </svg>
                                    </span>
                                    <span className="d-block fw-bold text-start">
                                      <span className="text-dark fw-bolder d-block fs-7">
                                        Log Perubahan
                                      </span>
                                      <span className="text-muted fw-bold fs-6">
                                        {handleFormatDate(
                                          this.state.tteData.updated_at,
                                          "DD MMMM YYYY, HH:mm:ss",
                                          "DD-MM-YYYY HH:mm:ss"
                                        )}
                                      </span>
                                    </span>
                                  </>
                                ) : (
                                  <>
                                    <span className="svg-icon svg-icon-primary svg-icon-3x me-5">
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24px"
                                        height="24px"
                                        viewBox="0 0 24 24"
                                        version="1.1"
                                      >
                                        <g
                                          stroke="none"
                                          stroke-width="1"
                                          fill="none"
                                          fill-rule="evenodd"
                                        >
                                          <rect
                                            x="0"
                                            y="0"
                                            width="24"
                                            height="24"
                                          />
                                          <circle
                                            fill="#000000"
                                            opacity="0.3"
                                            cx="12"
                                            cy="12"
                                            r="10"
                                          />
                                          <path
                                            d="M14.5,11 C15.0522847,11 15.5,11.4477153 15.5,12 L15.5,15 C15.5,15.5522847 15.0522847,16 14.5,16 L9.5,16 C8.94771525,16 8.5,15.5522847 8.5,15 L8.5,12 C8.5,11.4477153 8.94771525,11 9.5,11 L9.5,10.5 C9.5,9.11928813 10.6192881,8 12,8 C13.3807119,8 14.5,9.11928813 14.5,10.5 L14.5,11 Z M12,9 C11.1715729,9 10.5,9.67157288 10.5,10.5 L10.5,11 L13.5,11 L13.5,10.5 C13.5,9.67157288 12.8284271,9 12,9 Z"
                                            fill="#000000"
                                          />
                                        </g>
                                      </svg>
                                    </span>
                                    <div className="d-block fw-bold text-start w-100">
                                      <span className="text-dark fw-bolder d-block fs-7">
                                        Password
                                      </span>
                                      <div className="row">
                                        <input
                                          className="form-control form-control-sm"
                                          type="password"
                                          name="password"
                                          placeholder="Masukkan password"
                                          onBlur={(e) => {
                                            this.emptyValidation(
                                              e.target.value,
                                              "password"
                                            );
                                          }}
                                        />
                                        <span style={{ color: "red" }}>
                                          {this.state.errors["password"]}
                                        </span>
                                      </div>
                                    </div>
                                  </>
                                )}
                              </label>
                            </div> */}
                          </div>
                          <div className="row">
                            <div className="form-group fv-row pt-7 mb-7">
                              <div className="d-flex justify-content-center mb-7">
                                <div className="me-2">
                                  {this.state.view ? (
                                    <a
                                      href="#"
                                      className="btn btn-light"
                                      onClick={() =>
                                        this.setState({ view: false })
                                      }
                                    >
                                      <i className="bi bi-gear-fill"></i> Edit
                                      TTE
                                    </a>
                                  ) : (
                                    <>
                                      <button
                                        type="reset"
                                        className="btn btn-light me-5"
                                        onClick={() =>
                                          this.setState({
                                            view: true,
                                          })
                                        }
                                      >
                                        Batal
                                      </button>
                                      <button
                                        type="submit"
                                        className="btn btn-primary"
                                        disabled={this.state.isLoading}
                                      >
                                        {this.state.isLoading ? (
                                          <>
                                            <span
                                              className="spinner-border spinner-border-sm me-2"
                                              role="status"
                                              aria-hidden="true"
                                            ></span>
                                            <span className="sr-only">
                                              Loading...
                                            </span>
                                            Loading...
                                          </>
                                        ) : (
                                          <>
                                            <i className="fa fa-paper-plane me-1"></i>
                                            Simpan
                                          </>
                                        )}
                                      </button>
                                    </>
                                  )}
                                </div>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
