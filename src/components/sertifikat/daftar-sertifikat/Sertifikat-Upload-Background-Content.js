import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import Select from "react-select";
import { capitalizeFirstLetter } from "../../publikasi/helper";
import moment from "moment";

export default class SertifikatUploadBackgroundContent extends React.Component {
  constructor(props) {
    super(props);
    this.fileRef = React.createRef(null);
    this.submit = this.handleSubmit.bind(this);
  }
  state = {
    dataxsertifikat: [],
    errors: {},
    mFile: null,
    isLoading: false,
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/sertifikat/get-background",
        {},
        this.configs,
      )
      .then(async (result) => {
        const response = await axios.get(
          `${process.env.REACT_APP_BASE_API_URI}/sertifikat/get-file?path=${result.data.result.Data.file}`,
          { ...this.configs, responseType: "blob" },
        );
        this.fileRef.current.src = URL.createObjectURL(response.data);
        return axios.post(
          process.env.REACT_APP_BASE_API_URI + "/sertifikat/get-tte",
          {},
          this.configs,
        );
      })
      .then((res) => {
        const dataxsertifikat = res.data.result.Data;
        this.setState({ dataxsertifikat });
      })
      .catch((err) => {
        console.log(err);
        const messagex = err.data?.result?.Message;
        swal.fire({
          title: messagex ?? "Terjadi Kesalahan!",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      });
  }

  handleSubmit(e) {
    e.preventDefault();
    if (this.state.mFile == null) {
      const errors = this.state.errors;
      this.setState({ errors });
      return;
    }
    this.setState({ isLoading: true });
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    const formData = new FormData();
    formData.append("background", this.state.mFile);
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/sertifikat/upload-background",
        formData,
        this.configs,
      )
      .then((result) => {
        this.setState({ isLoading: false });
        const statux = result.data.result.Status;
        const messagex = result.data.result.Message;
        if (statux) {
          swal
            .fire({
              title: messagex,
              icon: "success",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                window.location = "/sertifikat/kelola-sertifikat";
              }
            });
        } else {
          swal.fire({
            title: messagex,
            icon: "warning",
            confirmButtonText: "Ok",
          });
        }
      })
      .catch((err) => {
        this.setState({ isLoading: false });
        const messagex = err.data?.result?.Message;
        swal.fire({
          title: messagex ?? "Terjadi Kesalahan!",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      });
  }
  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24px"
                      height="24px"
                      viewBox="0 0 24 24"
                      className="mh-50px"
                    >
                      <path
                        d="M10.0813 3.7242C10.8849 2.16438 13.1151 2.16438 13.9187 3.7242V3.7242C14.4016 4.66147 15.4909 5.1127 16.4951 4.79139V4.79139C18.1663 4.25668 19.7433 5.83365 19.2086 7.50485V7.50485C18.8873 8.50905 19.3385 9.59842 20.2758 10.0813V10.0813C21.8356 10.8849 21.8356 13.1151 20.2758 13.9187V13.9187C19.3385 14.4016 18.8873 15.491 19.2086 16.4951V16.4951C19.7433 18.1663 18.1663 19.7433 16.4951 19.2086V19.2086C15.491 18.8873 14.4016 19.3385 13.9187 20.2758V20.2758C13.1151 21.8356 10.8849 21.8356 10.0813 20.2758V20.2758C9.59842 19.3385 8.50905 18.8873 7.50485 19.2086V19.2086C5.83365 19.7433 4.25668 18.1663 4.79139 16.4951V16.4951C5.1127 15.491 4.66147 14.4016 3.7242 13.9187V13.9187C2.16438 13.1151 2.16438 10.8849 3.7242 10.0813V10.0813C4.66147 9.59842 5.1127 8.50905 4.79139 7.50485V7.50485C4.25668 5.83365 5.83365 4.25668 7.50485 4.79139V4.79139C8.50905 5.1127 9.59842 4.66147 10.0813 3.7242V3.7242Z"
                        fill="#50cd89"
                      ></path>
                      <path
                        className="permanent"
                        d="M14.8563 9.1903C15.0606 8.94984 15.3771 8.9385 15.6175 9.14289C15.858 9.34728 15.8229 9.66433 15.6185 9.9048L11.863 14.6558C11.6554 14.9001 11.2876 14.9258 11.048 14.7128L8.47656 12.4271C8.24068 12.2174 8.21944 11.8563 8.42911 11.6204C8.63877 11.3845 8.99996 11.3633 9.23583 11.5729L11.3706 13.4705L14.8563 9.1903Z"
                        fill="white"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Sertifikat
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Kelola Sertifikat
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <a
                  type="button"
                  onClick={() => {
                    swal
                      .fire({
                        title: "Apakah Anda Yakin?",
                        icon: "warning",
                        confirmButtonText: "Ya",
                        showCancelButton: true,
                        cancelButtonText: "Tidak",
                        confirmButtonColor: "#3085d6",
                        cancelButtonColor: "#d33",
                      })
                      .then((result) => {
                        if (result.isConfirmed) {
                          window.location = "/sertifikat/kelola-sertifikat";
                        }
                      });
                  }}
                  className="btn btn-sm btn-light btn-active-light-primary"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <form onSubmit={this.submit}>
              <div className="post d-flex flex-column-fluid" id="kt_post">
                <div id="kt_content_container" className="container-xxl">
                  <div className="row">
                    <div className="col-lg-12 mt-7">
                      <div className="card border">
                        <div className="card-header">
                          <div className="card-title">
                            <h1
                              className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                              style={{ textTransform: "capitalize" }}
                            >
                              Template Sertifikat
                            </h1>
                          </div>
                        </div>
                        <div className="card-body">
                          <div className="row mb-5">
                            <div className="col-12">
                              <label className="form-label">
                                Upload Background
                              </label>
                              <input
                                type="file"
                                className="form-control form-control-sm mb-2"
                                name="upload_background"
                                accept=".png, .jpg, .jpeg"
                                onChange={(e) => {
                                  if (
                                    e.target.files.length > 0 &&
                                    e.target.files[0].size / 1024 > 4096
                                  ) {
                                    const errors = this.state.errors;
                                    errors["background"] =
                                      "Ukuran File Tidak Beloeh Melebihi 4096Mb";
                                    this.setState({ errors });
                                  } else {
                                    this.setState({ mFile: e.target.files[0] });
                                    const [file] = e.target.files;
                                    this.fileRef.current.src =
                                      URL.createObjectURL(file);
                                  }
                                }}
                              />
                              <small className="text-muted d-block">
                                Format Image (.png/.jpg/.jpeg/.svg), Max 4096Mb
                              </small>
                              <span style={{ color: "red" }}>
                                {this.state.errors["background"]}
                              </span>
                            </div>
                          </div>
                          <div className="row">
                            <div style={{ height: "595px" }} className="">
                              <div
                                className="row align-items-center m-0 h-100 border-primary border position-relative"
                                style={{
                                  backgroundSize: "fit",
                                  // backgroundRepeat: "no-repeat",
                                }}
                              >
                                <img
                                  src=""
                                  ref={this.fileRef}
                                  alt="Background"
                                  className="position-absolute w-100 h-100 p-0"
                                />
                                <div className="col-12 text-center fw-normal p-0 justify-content-center z-index-1">
                                  <label className="mb-0 w-100">
                                    <h1 className="fw-bolder">
                                      SERTIFIKAT PELATIHAN
                                    </h1>
                                  </label>
                                  <div
                                    className="mb-3"
                                    style={{
                                      height: "18px",
                                      minHeight: "18px",
                                    }}
                                  >
                                    <label className="fs-12">
                                      Nomor Sertifikat
                                    </label>
                                  </div>
                                  <div className="w-100 fs-11">
                                    Diberikan kepada
                                  </div>
                                  <div className="my-2">
                                    <h1>Nama Peserta</h1>
                                  </div>
                                  <span className="w-100 fs-11 mb-5 d-block">
                                    telah menyelesaikan pelatihan
                                  </span>
                                  <div className="justify-content-center px-10">
                                    <div className="text-center fw-bolder">
                                      Pengenalan Data Science untuk Anak Sekolah
                                      Dasar (Batch 2)
                                    </div>
                                  </div>
                                  <div className=" w-100">
                                    <span className="w-100">pada Program </span>
                                    <span className="fw-bold w-100">
                                      Thematic Academy
                                    </span>
                                  </div>
                                  <div className="w-100">
                                    <span>Digital Talent Scholarship</span>
                                    <span className="px-2 border-2 fw-bolder">
                                      YYYY
                                    </span>
                                  </div>
                                  <div>
                                    <span className="mx-2">pada tanggal</span>
                                    <span className="fw-bolder">
                                      DD {moment().format("MMMM")} YYYY - DD{" "}
                                      {moment().format("MMMM")} YYYY
                                    </span>
                                    <span className="mx-2">selama </span>
                                    <span>
                                      <span className="fw-bolder mr-2">
                                        XX Jam Pelatihan
                                      </span>
                                    </span>
                                  </div>
                                  <div className="w-100 mb-3 text-center mt-10">
                                    <span className="mx-2 px-2 border-2">
                                      Jakarta, DD {moment().format("MMMM")} YYYY
                                    </span>
                                  </div>
                                  <div
                                    className=" justify-content-center m-0 p-0 d-flex w-100"
                                    style={{ width: "100%", height: "100%" }}
                                  >
                                    <div className="col-3 p-0 ">
                                      <div className="col p-0 ">
                                        <div
                                          className="col border border-dashed border-gray-800 align-items-center justify-content-center d-flex position-relative"
                                          style={{ height: "80px" }}
                                        ></div>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="row justify-content-center mt-1">
                                    {this.state.dataxsertifikat.name}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="form-group fv-row pt-7 mb-7">
                            <div className="d-flex justify-content-center mb-7">
                              <div className="me-2">
                                <div>
                                  <a
                                    type="button"
                                    className="btn btn-light me-3"
                                    id="batal"
                                    onClick={() => {
                                      swal
                                        .fire({
                                          title: "Apakah Anda Yakin?",
                                          icon: "warning",
                                          confirmButtonText: "Ya",
                                          showCancelButton: true,
                                          cancelButtonText: "Tidak",
                                          confirmButtonColor: "#3085d6",
                                          cancelButtonColor: "#d33",
                                        })
                                        .then((result) => {
                                          if (result.isConfirmed) {
                                            window.location =
                                              "/sertifikat/kelola-sertifikat";
                                          }
                                        });
                                    }}
                                  >
                                    Batal
                                  </a>
                                  <button
                                    type="submit"
                                    className="btn btn-primary"
                                    id="submit"
                                    disabled={this.state.isLoading}
                                  >
                                    {this.state.isLoading ? (
                                      <>
                                        <span
                                          className="spinner-border spinner-border-sm me-2"
                                          role="status"
                                          aria-hidden="true"
                                        ></span>
                                        <span className="sr-only">
                                          Loading...
                                        </span>
                                        Loading...
                                      </>
                                    ) : (
                                      <>
                                        <i className="fa fa-paper-plane me-1"></i>
                                        Simpan
                                      </>
                                    )}
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
