import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import Select from "react-select";
import {
  capitalizeFirstLetter,
  colorStatusPelaksanaan2,
  statusPelaksanaanWithLabel2,
} from "../../publikasi/helper";

export default class SertifikatPesertaContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleClickPratinjau = this.handleClickPratinjauAction.bind(this);
    this.handleClickFilter = this.handleClickFilterAction.bind(this);
    this.handleClickReset = this.handleClickResetAction.bind(this);
    // this.handleChangeAkademi = this.handleChangeAkademiAction.bind(this);
    this.handleChangeStatusSertifikat =
      this.handleChangeStatusSertifikatAction.bind(this);
    this.handleSort = this.handleSortAction.bind(this);

    this.state = {
      pelatihanName: "",
      sertifikatTersedia: 0,
      sertifikatBelumTersedia: 0,
      datax: [],
      datax_pelatihan: [],
      loading: true,
      totalRows: 0,
      newPerPage: 10,
      totalEventOffline: 0,
      totalEventOnlineOffline: 0,
      totalEvent: 0,
      totalEventOnline: 0,
      tempLastNumber: 0,
      currentPage: 0,
      pelatihanId: "",
      valKategoriStatus: [],
      pratinjau: [],
      column: "id",
      sortDirection: "asc",
      searchText: "",
      dataxstatus: [
        {
          label: "Semua",
          value: null,
        },
        {
          label: "Tersedia",
          value: 1,
        },
        {
          label: "Belum Tersedia",
          value: 0,
        },
      ],
      valStatus: null,
      isRowChangeRef: false,
      statusPelaksanaan: "",
      colorStatusPelaksanaan: "",
    };
  }

  handleClickPratinjauAction(e) {
    const kategori = e.currentTarget.getAttribute("kategori");
    const judul = e.currentTarget.getAttribute("judul");
    const deskripsi = e.currentTarget.getAttribute("deskripsi");
    const tanggal_event = e.currentTarget.getAttribute("tanggal_event");
    const lokasi = e.currentTarget.getAttribute("lokasi");
    const pembicara =
      typeof e.currentTarget.getAttribute("pembicara") == "string"
        ? e.currentTarget.getAttribute("pembicara").split(";")
        : "";
    const link = e.currentTarget.getAttribute("link");
    const jenis = e.currentTarget.getAttribute("jenis");
    const id = e.currentTarget.getAttribute("id");

    this.setState(
      {
        pratinjau: {
          kategori: kategori,
          judul: judul,
          deskripsi: deskripsi,
          tanggal_event: tanggal_event,
          lokasi: lokasi,
          pembicara: pembicara,
          link: link,
          jenis: jenis,
          id: id,
        },
      },
      () => {},
    );
  }

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  columns = [
    {
      name: "No",
      cell: (row, index) => {
        return (
          <>
            <div style={{ textAlign: "left" }}>
              <span className="fs-7" style={{ textAlign: "left" }}>
                {this.state.tempLastNumber + index + 1}
              </span>
            </div>
          </>
        );
      },
      width: "70px",
      grow: 1,
    },
    {
      Header: "Nama Peserta",
      accessor: "",
      className: "min-w-300px mw-300px",
      width: "300px",
      name: "Nama Peserta",
      sortable: true,
      grow: 6,
      selector: (row, index) => capitalizeFirstLetter(row.user_name),
      cell: (row, index) => {
        return (
          <label className="d-flex flex-stack my-2 cursor-pointer">
            <span className="d-flex align-items-center me-2">
              <span className="symbol symbol-50px me-6">
                <span className="symbol-label bg-light-primary">
                  <span className="svg-icon svg-icon-1 svg-icon-primary">
                    <img
                      src={
                        process.env.REACT_APP_BASE_API_URI +
                        "/download/get-file?path=" +
                        row.foto
                      }
                      width="100%"
                      className="rounded"
                      alt="symbol-label"
                    />
                  </span>
                </span>
              </span>
              <span className="d-flex flex-column">
                <span className="fs-7 fw-semibold text-muted">
                  {row.nomor_pendaftaran}
                </span>
                <span
                  className="fw-bolder fs-7"
                  style={{
                    overflow: "hidden",
                    whiteSpace: "wrap",
                    textOverflow: "ellipses",
                  }}
                >
                  {row.user_name}
                </span>
                <span className="fs-7 fw-semibold text-muted">{row.email}</span>
              </span>
            </span>
          </label>
        );
      },
    },
    {
      name: "Tema",
      className: "min-w-200px mw-200px",
      sortable: true,
      grow: 6,
      width: "200px",
      selector: (row) => capitalizeFirstLetter(row.tema_name),
      cell: (row, index) => {
        return (
          <>
            <div style={{ textAlign: "left" }}>
              <span className="fs-7" style={{ textAlign: "left" }}>
                {" "}
                {row.tema_name}
              </span>
            </div>
          </>
        );
      },
    },
    {
      name: "Akademi",
      sortable: true,
      className: "min-w-200px mw-200px",
      grow: 6,
      width: "200px",
      selector: (row) => capitalizeFirstLetter(row.akademi_name),
      cell: (row, index) => {
        return (
          <>
            <div style={{ textAlign: "left" }}>
              <span className="fs-7" style={{ textAlign: "left" }}>
                {" "}
                {row.akademi_name}
              </span>
            </div>
          </>
        );
      },
    },
    {
      name: "Sertifikat",
      sortable: true,
      selector: (row) => row.status_sertifikat,
      width: "200px",

      cell: (row, index) => (
        <div>
          <span
            className={
              "badge fs- badge-light-" +
              (row.status_sertifikat.toLowerCase() == "belum tersedia"
                ? "danger"
                : row.status_sertifikat.toLowerCase() == "tersedia"
                  ? "success"
                  : "primary") +
              " fs-7"
            }
          >
            {row.status_sertifikat}
          </span>
        </div>
      ),
    },
    {
      name: "Status Peserta",
      sortable: true,
      selector: (row) => (row.file == null ? "Belum Tersedia" : "Tersedia"),
      width: "250px",

      cell: (row, index) => (
        <div>
          <span className={"badge badge-light-primary fs-7"}>
            {row.status_peserta}
          </span>
        </div>
      ),
    },
    {
      name: "Aksi",
      center: true,
      width: "150px",
      cell: (row) => (
        <div>
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              this.downloadSertifikat(
                row.file,
                `${row.user_name}_${row.nomor_registrasi}_Sertifikat`,
              );
            }}
            id={row.id}
            title="Download Sertifikat"
            className={`btn btn-icon btn-success btn-sm me-1 ${
              row.status_sertifikat.toLowerCase() == "belum tersedia"
                ? "disabled"
                : ""
            }`}
          >
            <i class="fa fa-download text-white"></i>
          </a>
          {/* <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              swal.fire({
                title: "Mohon Tunggu!",
                icon: "info", // add html attribute if you want or remove
                allowOutsideClick: false,
                didOpen: () => {
                  swal.showLoading();
                },
              });
              axios
                .post(
                  process.env.REACT_APP_BASE_API_URI + "/sertifikat/generate",
                  {
                    id_user: row.user_id,
                    id_pelatihan: row.pelatihan_id,
                  },
                  this.configs
                )
                .then((res) => {
                  let status = res.data.result.Status;
                  let message = res.data.result.Message;
                  if (status) {
                    swal.fire({
                      title: "Informasi",
                      text: message,
                      icon: "success",
                    });
                    this.handleReload(
                      this.state.currentPage,
                      this.state.newPerPage
                    );
                  } else {
                    throw Error(message);
                  }
                })
                .catch((error) => {
                  let message = error.response?.data?.result
                    ? error.response?.data?.result.Message
                    : error.message;
                  swal.fire({
                    title: "Error",
                    text: message ?? "Tidak dapat men-generate sertifikat.",
                    icon: "error",
                  });
                });
            }}
            id={row.id}
            title="Generate Ulang Sertifikat"
            className={`btn btn-icon btn-primary btn-sm m-1 ${
              row.status_peserta == "Lulus Pelatihan - Kehadiran" ||
              row.status_peserta == "Lulus Pelatihan - Nilai" ||
              row.status_peserta == "Berhak Sertifikasi" ||
              row.status_peserta == "Ikut Sertifikasi" ||
              row.status_peserta == "Lulus Sertifikasi"
                ? ""
                : "disabled"
            }`}
          >
            <i class="bi bi-recycle text-white"></i>
          </a> */}
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };
  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    const payload = {
      start: 0,
      length: 100,
      status: "publish",
    };
    // axios
    //   .post(
    //     process.env.REACT_APP_BASE_API_URI + "/list-akademi",
    //     payload,
    //     this.configs
    //   )
    //   .then((res) => {
    //     const optionx =
    //       res.data.result.Data[0] == null ? [] : res.data.result.Data;
    //     const dataxakademi = [{ value: "", label: "Pilih Semua" }];
    //     optionx.map((data) =>
    //       dataxakademi.push({ value: data.id, label: data.name })
    //     );
    //     this.setState({ dataxakademi });
    //   });
    this.loadPelatihan();
    this.handleReload();
  }
  downloadSertifikat(file, nama) {
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .get(
        `${process.env.REACT_APP_BASE_API_URI}/sertifikat/get-file?path=${file}`,
        this.configs,
      )
      .then((result) => {
        const linkSource = `data:application/pdf;base64,${result.data}`;
        const downloadLink = document.createElement("a");
        const fileName = nama + ".pdf";

        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        swal.close();
      })
      .catch((err) => {
        const message = err?.response?.data
          ? err.response.data.result?.Message
          : err.message;
        swal.fire({
          icon: "warning",
          title: "Peringatan!",
          text:
            message ?? "Terjadi kesalahan saat akan terhubung dengan server.",
        });
      });
  }
  // handleChangeAkademiAction = (selectedOption) => {
  //   console.log("selected options:", selectedOption);
  //   const { value, label } = selectedOption;
  //   this.setState({
  //     valAkademi: {
  //       label: label,
  //       value: value,
  //     },
  //   });
  //   // Cookies.set("akademi_id", value, 1);
  //   const dataBody = { start: 1, rows: 100, id: value };
  //   axios
  //     .post(
  //       process.env.REACT_APP_BASE_API_URI + "/cari_tema_byakademi",
  //       dataBody,
  //       this.configs
  //     )
  //     .then((res) => {
  //       this.setState({ isDisabled: false });
  //       const optionx = res.data.result.Data;
  //       const dataxstatus = [];
  //       optionx.map((data) =>
  //         dataxstatus.push({ value: data.id, label: data.name })
  //       );
  //       this.setState({ dataxstatus }, () => {
  //         console.log(this.state.dataxstatus);
  //       });
  //       this.setState({ seltema: [] });
  //     })
  //     .catch((error) => {
  //       const dataxstatus = [];
  //       this.setState({ dataxstatus });
  //       this.setState({ seltema: [] });
  //       let messagex = error.response.data.result.Message;
  //       swal
  //         .fire({
  //           title: messagex,
  //           icon: "warning",
  //           confirmButtonText: "Ok",
  //         })
  //         .then((result) => {
  //           if (result.isConfirmed) {
  //           }
  //         });
  //     });
  // };

  handleClickResetAction() {
    this.setState(
      {
        valStatus: null,
      },
      () => {
        this.handleReload();
      },
    );
  }

  handleChangeStatusSertifikatAction = (selectedOption) => {
    this.setState({
      valStatus: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  };

  handleSortAction(column, sortDirection) {
    let server_name = "";
    if (column.name == "Nama Peserta") {
      server_name = "user_name";
    } else if (column.name == "Akademi") {
      server_name = "akademi_name";
    } else if (column.name == "Sertifikat") {
      server_name = "status_sertifikat";
    } else if (column.name == "Pelatihan") {
      server_name = "pelatihan_name";
    } else if (column.name == "Status") {
      server_name = "file";
    } else if (column.name == "Status Peserta") {
      server_name = "status_peserta";
    }

    this.setState(
      {
        column: server_name,
        sortDirection: sortDirection,
      },
      () => {
        this.handleReload(1, this.state.newPerPage);
      },
    );
  }

  handleClickFilterAction(e) {
    e.preventDefault();
    this.setState({ loading: true }, () => {
      this.handleReload(1, this.state.newPerPage);
    });
  }

  handleReload(page, newPerPage) {
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    let segment_url = window.location.pathname.split("/");
    this.setState({ pelatihanId: segment_url[3], tempLastNumber: start_tmp });
    const dataBody = {
      start: start_tmp,
      length: length_tmp,
      pelatihanId: segment_url[3],
      search: this.state.searchText,
      sort_by: this.state.column,
      sort_val: this.state.sortDirection.toUpperCase(),
      status_sertifikat: this.state.valStatus?.value,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/sertifikat/peserta-filter",
        dataBody,
        this.configs,
      )
      .then((res) => {
        const status = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (status) {
          const datax =
            res.data.result.Data[0] == null ? [] : res.data.result.Data;
          this.setState({ datax });
          this.setState({
            loading: false,
            totalRows: res.data.result.TotalLength,
            currentPage: page,
            pelatihanName: datax[0].pelatihan_name,
            sertifikatTersedia: res.data.result.Tersedia,
            sertifikatBelumTersedia: res.data.result.BelumTersedia,
          });
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                // this.handleClickResetAction();
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      })
      .catch((error) => {
        let messagex = error.response?.data?.result?.Message;
        swal.fire({
          title: messagex ?? "Terjadi Kesalahan!",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      });
  }
  handlePerRowsChange = async (arg1, arg2, srcEvent) => {
    if (srcEvent == "page-change") {
      this.setState({ loading: true }, () => {
        if (!this.state.isRowChangeRef) {
          this.handleReload(arg1, this.state.newPerPage);
        }
      });
    } else if (srcEvent == "row-change") {
      this.setState({ isRowChangeRef: true }, () => {
        this.handleReload(arg2, arg1);
      });
      this.setState({ loading: true, newPerPage: arg1 }, () => {
        this.setState({ isRowChangeRef: false });
      });
    }
  };

  handleKeyPressAction(e) {
    console.log("from here!");
    const searchText = e.currentTarget.value;
    if (e.key == "Enter") {
      if (searchText == "") {
        this.handleReload();
      } else {
        this.setState({ loading: true, searchText: searchText }, () => {
          this.handleReload(1, this.state.newPerPage);
        });
      }
    }
  }

  loadPelatihan() {
    let segment_url = window.location.pathname.split("/");
    let data = {
      id: segment_url[3],
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/pelatihanz/findpelatihan2",
        data,
        this.configs,
      )
      .then((res) => {
        const statusx = res.data.result.Status;
        if (statusx) {
          const datax_pelatihan = res.data.result.Data[0];
          const statusPelaksanaan = statusPelaksanaanWithLabel2(
            datax_pelatihan.pelatihan_start,
            datax_pelatihan.pelatihan_end,
          );
          const colorStatusPelaksanaan = colorStatusPelaksanaan2(
            datax_pelatihan.pelatihan_start,
            datax_pelatihan.pelatihan_end,
          );
          this.setState({
            datax_pelatihan,
            statusPelaksanaan,
            colorStatusPelaksanaan,
          });
        }
      })
      .catch((error) => {
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  }

  render() {
    let rowCounter = 1;
    const styleImg = {
      width: "40px",
      height: "30px",
    };
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24px"
                      height="24px"
                      viewBox="0 0 24 24"
                      className="mh-50px"
                    >
                      <path
                        d="M10.0813 3.7242C10.8849 2.16438 13.1151 2.16438 13.9187 3.7242V3.7242C14.4016 4.66147 15.4909 5.1127 16.4951 4.79139V4.79139C18.1663 4.25668 19.7433 5.83365 19.2086 7.50485V7.50485C18.8873 8.50905 19.3385 9.59842 20.2758 10.0813V10.0813C21.8356 10.8849 21.8356 13.1151 20.2758 13.9187V13.9187C19.3385 14.4016 18.8873 15.491 19.2086 16.4951V16.4951C19.7433 18.1663 18.1663 19.7433 16.4951 19.2086V19.2086C15.491 18.8873 14.4016 19.3385 13.9187 20.2758V20.2758C13.1151 21.8356 10.8849 21.8356 10.0813 20.2758V20.2758C9.59842 19.3385 8.50905 18.8873 7.50485 19.2086V19.2086C5.83365 19.7433 4.25668 18.1663 4.79139 16.4951V16.4951C5.1127 15.491 4.66147 14.4016 3.7242 13.9187V13.9187C2.16438 13.1151 2.16438 10.8849 3.7242 10.0813V10.0813C4.66147 9.59842 5.1127 8.50905 4.79139 7.50485V7.50485C4.25668 5.83365 5.83365 4.25668 7.50485 4.79139V4.79139C8.50905 5.1127 9.59842 4.66147 10.0813 3.7242V3.7242Z"
                        fill="#50cd89"
                      ></path>
                      <path
                        className="permanent"
                        d="M14.8563 9.1903C15.0606 8.94984 15.3771 8.9385 15.6175 9.14289C15.858 9.34728 15.8229 9.66433 15.6185 9.9048L11.863 14.6558C11.6554 14.9001 11.2876 14.9258 11.048 14.7128L8.47656 12.4271C8.24068 12.2174 8.21944 11.8563 8.42911 11.6204C8.63877 11.3845 8.99996 11.3633 9.23583 11.5729L11.3706 13.4705L14.8563 9.1903Z"
                        fill="white"
                      ></path>
                    </svg>
                  </span>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Sertifikat
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Daftar Sertifikat
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <a
                  href={"/sertifikat/daftar-sertifikat/"}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  {" "}
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </a>

                <button
                  className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                  data-bs-toggle="modal"
                  data-bs-target="#filter"
                >
                  <i className="bi bi-sliders"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Filter
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Rekap Sertifikat
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <div className="d-flex align-items-center position-relative my-1 me-2">
                            <span className="svg-icon svg-icon-1 position-absolute ms-6">
                              <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className="mh-50px"
                              >
                                <rect
                                  opacity="0.5"
                                  x="17.0365"
                                  y="15.1223"
                                  width="8.15546"
                                  height="2"
                                  rx="1"
                                  transform="rotate(45 17.0365 15.1223)"
                                  fill="currentColor"
                                ></rect>
                                <path
                                  d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                  fill="currentColor"
                                ></path>
                              </svg>
                            </span>
                            <input
                              type="text"
                              data-kt-user-table-filter="search"
                              className="form-control form-control-sm form-control-solid w-250px ps-14"
                              placeholder="Cari Penerima Sertifikat"
                              onKeyPress={this.handleKeyPress}
                              onChange={(e) => {
                                if (e.target.value == "") {
                                  this.setState({ searchText: "" }, () => {
                                    this.handleReload();
                                  });
                                }
                              }}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="mb-6">
                          <div className="d-flex flex-wrap">
                            <div className="d-flex justify-content-between align-items-start flex-wrap">
                              <div className="d-flex flex-column">
                                <div className="col-12 mt-5 mb-3">
                                  <span
                                    className={
                                      "badge badge-" +
                                      this.state.colorStatusPelaksanaan
                                    }
                                  >
                                    {this.state.statusPelaksanaan}
                                  </span>
                                  <h1 className="align-items-center text-dark fw-bolder my-1 fs-4">
                                    {
                                      this.state.datax_pelatihan
                                        .slug_pelatian_id
                                    }{" "}
                                    -{this.state.pelatihanName}
                                  </h1>
                                  <span className="text-muted fw-semibold fs-7 mb-0">
                                    {this.state.datax_pelatihan.akademi} -{" "}
                                    {this.state.datax_pelatihan.tema}
                                  </span>
                                </div>

                                <div className="d-flex flex-wrap fw-bold fs-6">
                                  <span className="text-muted fw-semibold fs-8">
                                    <span className="text-dark">
                                      Total:{" "}
                                      {this.state.sertifikatTersedia +
                                        this.state.sertifikatBelumTersedia}
                                    </span>{" "}
                                    |{" "}
                                    <span className="text-success">
                                      Tersedia: {this.state.sertifikatTersedia}
                                    </span>{" "}
                                    |{" "}
                                    <span className="text-danger">
                                      Belum Tersedia:{" "}
                                      {this.state.sertifikatBelumTersedia}
                                    </span>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="table-responsive">
                          <DataTable
                            columns={this.columns}
                            data={this.state.datax}
                            progressPending={this.state.loading}
                            highlightOnHover
                            pointerOnHover
                            pagination
                            paginationServer
                            paginationTotalRows={this.state.totalRows}
                            paginationComponentOptions={{
                              selectAllRowsItem: true,
                              selectAllRowsItemText: "Semua",
                            }}
                            paginationDefaultPage={this.state.currentPage}
                            onChangeRowsPerPage={(
                              currentRowsPerPage,
                              currentPage,
                            ) => {
                              this.handlePerRowsChange(
                                currentRowsPerPage,
                                currentPage,
                                "row-change",
                              );
                            }}
                            onChangePage={(page, totalRows) => {
                              this.handlePerRowsChange(
                                page,
                                totalRows,
                                "page-change",
                              );
                            }}
                            customStyles={this.customStyles}
                            persistTableHead={true}
                            onSort={this.handleSort}
                            noDataComponent={
                              <div className="mt-5">Tidak Ada Data</div>
                            }
                            sortServer
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* Filter */}
        <div className="modal fade" tabindex="-1" id="filter">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">
                  <span className="svg-icon svg-icon-5 me-1">
                    <i className="bi bi-sliders text-black"></i>
                  </span>
                  Filter Rekap Sertifikat
                </h5>
                <div
                  className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <span className="svg-icon svg-icon-2x">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <rect
                        opacity="0.5"
                        x="6"
                        y="17.3137"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(-45 6 17.3137)"
                        fill="currentColor"
                      />
                      <rect
                        x="7.41422"
                        y="6"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(45 7.41422 6)"
                        fill="currentColor"
                      />
                    </svg>
                  </span>
                </div>
              </div>
              <form action="#" onSubmit={this.handleClickFilter}>
                <input
                  type="hidden"
                  name="csrf-token"
                  value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                />
                <div className="modal-body">
                  <div className="fv-row form-group mb-7">
                    <label className="form-label">Status Sertifikat</label>
                    <Select
                      id="status"
                      name="status"
                      value={this.state.valStatus}
                      placeholder="Silahkan pilih Status"
                      noOptionsMessage={() => "Data tidak tersedia"}
                      className="form-select-sm selectpicker p-0"
                      options={this.state.dataxstatus}
                      isDisabled={this.state.dataxstatus.length == 0}
                      onChange={this.handleChangeStatusSertifikat}
                    />
                  </div>
                </div>
                <div className="modal-footer">
                  <div className="d-flex justify-content-between">
                    <button
                      type="reset"
                      className="btn btn-sm btn-light me-3"
                      onClick={this.handleClickReset}
                    >
                      Reset
                    </button>
                    <button
                      type="submit"
                      className="btn btn-sm btn-primary"
                      data-bs-dismiss="modal"
                    >
                      Apply Filter
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
