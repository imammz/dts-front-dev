import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../Sertifikat-Daftar-Peserta";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const SertifikatDartarPeserta = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default SertifikatDartarPeserta;
