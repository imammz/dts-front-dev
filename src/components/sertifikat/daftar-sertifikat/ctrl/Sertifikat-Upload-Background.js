import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../Sertifikat-Upload-Background-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const SertifikatUploadBackground = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default SertifikatUploadBackground;
