import React, { useState, useEffect, useRef } from "react";
import Header from "../../components/Header";
import SideNav from "../../components/SideNav";
import Footer from "../../components/Footer";
import axios from "axios";
import Swal from "sweetalert2";
import Board, { moveCard } from "@asseinfo/react-kanban";
import withReactContent from "sweetalert2-react-content";
import { useParams, useNavigate } from "react-router-dom";
import "@asseinfo/react-kanban/dist/styles.css";
const CopyForm = (props) => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  const [RepoSize, setRepoSize] = useState({});
  const [FilterRepoSize, setFilterRepoSize] = useState({});
  const [countSize, setCountSize] = useState();
  let history = useNavigate();
  const { id } = useParams();

  let saur = "0";
  let imsak = "0";
  const initialPendaftaranState = {
    id: id,
    judul: "",
  };
  const initialOldPendaftaran = {
    id: "0",
    judul: "",
  };

  const MySwal = withReactContent(Swal);
  const [Pendaftaran, setPendaftaran] = useState(initialPendaftaranState);
  const [oldPendaftaran, setOldPendaftaran] = useState(initialOldPendaftaran);
  const arr_push = [];
  // const [board, setBoard] = useState({});
  var nameField = "";
  const retriveRepositoryFind = async () => {
    const respon = await axios
      .post(process.env.REACT_APP_BASE_API_URI + "/cari-formbuilder", {
        id: id,
      })
      .then(function (response) {
        console.log(response);
        if (response.status === 200) {
          let repo = response.data.result;
          console.log(repo.utama[0].judul_form);
          if (repo.Status === true) {
            saur = repo.utama[0].id;
            setOldPendaftaran({
              ...oldPendaftaran,
              judul: repo.utama[0].judul_form,
              id: repo.utama[0].id,
            });
            nameField = repo.utama[0].judul_form;
          } else {
            nameField = "";
          }
        } else {
          nameField = "";
        }
      });
  };
  const retriveRepository = async () => {
    const respon = await axios
      .post(process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/list-repo", {
        start: "0",
        length: "50",
      })
      .then(function (response) {
        if (response.status === 200) {
          let repo = response.data.result;
          if (repo.Status === true) {
            let columns = [];
            const listItem = repo.Data.map((number) =>
              columns.push({
                id: number.id,
                title: number.name,
                element: number.element,
                maxlength: number.maxlength,
                placeholder: number.placeholder,
                min: number.min,
                max: number.max,
                required: "",
                size: number.size,
                option: number.option,
                data_option: number.data_option,
                className: number.className,
                html: number.html,
              }),
            );
            setRepoSize(columns);
            console.log(columns.length);
          } else {
            setRepoSize();
          }
        }
      })
      .catch(function (error) {
        setRepoSize();
      });
  };
  const handleSubmit = async () => {
    // console.log(countSize);
    // console.log(boardRepo.columns[1].cards.length);
    // return false;
    if (Pendaftaran.judul === "" || Pendaftaran.judul === undefined) {
      MySwal.fire({
        title: <strong>Information!</strong>,
        html: <i>Silahkan Masukan Judul Terlebih Dahulu</i>,
        icon: "warning",
      });
      return false;
    }
    if (Pendaftaran.judul === oldPendaftaran.judul) {
      MySwal.fire({
        title: <strong>Information!</strong>,
        html: <i>Silahkan Ganti Judul Form Terlebih Dahulu</i>,
        icon: "warning",
      });
      return false;
    }
    if (countSize === boardRepo.columns[1].cards.length) {
      const formData = new FormData();
      formData.append("judul_form", Pendaftaran.judul);
      formData.append("id_judul_form", 0);
      formData.append("id_repository", 0);
      formData.append("id_lama", id);
      const respon = await axios
        .post(
          process.env.REACT_APP_BASE_API_URI +
            "/daftarpeserta/createjson-formbuildernew",
          formData,
        )
        .then(function (response) {
          history("/pelatihan/pendaftaran");
        })
        .catch(function (error) {});
    } else {
    }
  };
  const retriveRepository_filter = async () => {
    const respon = await axios
      .post(process.env.REACT_APP_BASE_API_URI + "/cari-formbuilder", {
        id: id,
      })
      .then(function (response) {
        console.log(response.data.result.detail.length);
        setCountSize(response.data.result.detail.length);
        if (response.status === 200) {
          let repo = response.data.result;

          if (repo.Status === true) {
            let columns = [];
            const listItem = repo.detail.map((number) =>
              columns.push({
                id: number.id,
                title: number.name,
                element: number.element,
                maxlength: number.maxlength,
                placeholder: number.placeholder,
                min: number.min,
                max: number.max,
                required: "",
                size: number.size,
                option: number.option,
                data_option: number.data_option,
                className: number.className,
                html: number.html,
              }),
            );
            setPendaftaran({
              ...Pendaftaran,
              judul: repo.utama[0].judul_form,
              id: repo.utama[0].id,
            });
            setFilterRepoSize(columns);
            // console.log(columns.length);
          } else {
            setFilterRepoSize();
          }
        }
      })
      .catch(function (error) {
        setFilterRepoSize();
      });
  };
  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setPendaftaran({ ...Pendaftaran, [name]: value, id: 0 });
  };
  const boardRepo = {
    columns: [
      {
        id: 1,
        title: "Repositories",
        cards: RepoSize,
      },
      {
        id: 2,
        title: "Form Element",
        cards: FilterRepoSize,
      },
    ],
  };
  const board = {
    columns: [
      {
        id: 2,
        title: "Repositories",
        cards: [
          {
            id: 298,
            title: "TEST GROUP PENDIDIKAN",
          },
          {
            id: 297,
            title: "TESJKJK",
            // description: "Card content"
          },
          {
            id: 296,
            title: "Mencari Kesunyian Anda",
            // description: "Card content"
          },
          {
            id: 295,
            title: "asjdlkasd",
            // description: "Card content"
          },
          {
            id: 294,
            title: "uioqwueoi",
            // description: "Card content"
          },
          {
            id: 293,
            title: "Pendidikan",
            // description: "Card content"
          },
        ],
      },

      {
        id: 3,
        title: "Element",
        cards: [
          {
            id: 10,
            title: "tanggal_lahir",
            // description: "Card content"
          },
          {
            id: 11,
            title: "status_pendidikan",
            // description: "Card content"
          },
          {
            id: 12,
            title: "nama_ibu",
            // description: "Card content"
          },
          {
            id: 13,
            title: "tempat_tinggal",
            // description: "Card content"
          },
          {
            id: 14,
            title: "status_pegawai",
            // description: "Card content"
          },
          {
            id: 15,
            title: "agama",
            // description: "Card content"
          },
          {
            id: 16,
            title: "ijazah",
            // description: "Card content"
          },
        ],
      },
    ],
  };
  useEffect(() => {
    retriveRepository();
    retriveRepository_filter();
    // retriveRepositoryFind();
  }, []);
  function ControlledBoard_style() {
    const initialValue = {
      id: id,
    };
    const [initial, setInitial] = useState(initialValue);
    // You need to control the state yourself.
    const [controlledBoard, setBoard] = useState(boardRepo);
    const [statePos, setStatePos] = useState({ startX: 0, startScrollX: 0 });
    const sliderL = document.querySelector(".react-kanban-board");
    function handleCheck(e) {
      console.log("mko");
      console.log(e.target.value);
    }
    function handleCardMove(_card, source, destination) {
      if (Pendaftaran.judul === "" || Pendaftaran.judul === undefined) {
        MySwal.fire({
          title: <strong>Information!</strong>,
          html: <i>Silahkan Masukan Judul Terlebih Dahulu</i>,
          icon: "warning",
        });
        return false;
      }
      if (Pendaftaran.judul === oldPendaftaran.judul) {
        MySwal.fire({
          title: <strong>Information!</strong>,
          html: <i>Silahkan Ganti Judul Form Terlebih Dahulu</i>,
          icon: "warning",
        });
        return false;
      }
      console.log(id);
      console.log(saur);
      let categoryOptItems = [];
      let simonas = "";
      console.log(initial);
      // if(saur == '0' || saur == 0){
      //    simonas = saur;
      // }else{
      //     simonas = id;
      // }
      // console.log(saur);
      if (source.fromColumnId === 1) {
        // const listItems = _card.map((number) =>
        categoryOptItems.push({
          judul: Pendaftaran.judul,
          id_judul_form: saur,
          id_repository: _card.id,
          id_lama: initial.id,
        });
        var formData = new FormData();
        formData.append("judul_form", Pendaftaran.judul);
        formData.append("id_judul_form", saur);
        formData.append("id_repository", _card.id);
        // formData.append("id_lama", Pendaftaran.id);
        formData.append("id_lama", initial.id);

        axios.defaults.headers.common["Content-Type"] = "multipart/form-data;";
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/daftarpeserta/createjson-formbuildernew",
            formData,
          )
          .then(function (response) {
            console.log(response);
            if (response.status === 200) {
              let state_final = response.data.result;
              console.log(state_final);
              if (state_final.Status === true) {
                // setPendaftaran({ ...Pendaftaran, 'id' : state_final.Data});
                // MySwal.fire({
                //   title: <strong>Information!</strong>,
                //   html: <i>{response.data.result.Message}</i>,
                //   icon: 'success'
                // });
                const updatedBoard = moveCard(
                  controlledBoard,
                  source,
                  destination,
                );
                saur = state_final.Data;
                const intt = {
                  id: 0,
                };
                setInitial(intt);
                setBoard(updatedBoard);
                window.removeEventListener("mousemove", handleMouseMove);
                window.removeEventListener("mouseup", handleMouseUp);
                window.removeEventListener("mousemove", handleMouseMove);
                window.removeEventListener("mouseup", handleMouseUp);
                window.removeEventListener("mousemove", handleMouseMove);
                window.removeEventListener("mouseup", handleMouseUp);
                window.removeEventListener("mousemove", handleMouseMove);
                window.removeEventListener("mouseup", handleMouseUp);
                window.removeEventListener("mousemove", handleMouseMove);
                window.removeEventListener("mouseup", handleMouseUp);
                return true;
              } else {
                // MySwal.fire({
                //   title: <strong>Information!</strong>,
                //   html: <i>{response.data.result.Message}</i>,
                //   icon: 'info'
                // });
                saur = state_final.Data;
                // setInitial(state_final.Data);
                // setPendaftaran({ ...Pendaftaran, 'id' : state_final.Data});
                return false;
                // console.log(response);
              }
            } else {
              // MySwal.fire({
              //   title: <strong>Information!</strong>,
              //   html: <i>{response.statusText}</i>,
              //   icon: 'warning'
              // });
              return false;
            }
          })
          .catch(function (error) {
            // MySwal.fire({
            //     title: <strong>Information!</strong>,
            //     html: <i>{error.response.data.result.Message}</i>,
            //     icon: 'warning'
            //   });
            return false;
          });
      } else {
        const updatedBoard = moveCard(controlledBoard, source, destination);
        setBoard(updatedBoard);
        window.removeEventListener("mousemove", handleMouseMove);
        window.removeEventListener("mouseup", handleMouseUp);
        window.removeEventListener("mousemove", handleMouseMove);
        window.removeEventListener("mouseup", handleMouseUp);
        window.removeEventListener("mousemove", handleMouseMove);
        window.removeEventListener("mouseup", handleMouseUp);
        window.removeEventListener("mousemove", handleMouseMove);
        window.removeEventListener("mouseup", handleMouseUp);
        window.removeEventListener("mousemove", handleMouseMove);
        window.removeEventListener("mouseup", handleMouseUp);
        return true;
      }
    }
    function handleCardStart(e) {}

    function hasSomeParentTheClass(element, classname) {
      if (element.className.split(" ").indexOf(classname) >= 0) return true;
      return (
        element.parentNode &&
        hasSomeParentTheClass(element.parentNode, classname)
      );
    }

    var handleMouseDown = (e) => {
      const sliderL = document.querySelector(".react-kanban-board");
      const { target, clientX } = e;
      if (
        target.className !== "react-kanban-card" &&
        hasSomeParentTheClass(target.parentNode, "react-kanban-card") === false
      ) {
        return;
      }
      window.addEventListener("mousemove", handleMouseMove);
      window.addEventListener("mouseup", handleMouseUp);
      setStatePos({
        startX: clientX,
        startScrollX: window.scrollX,
      });
    };
    var handleMouseUp = () => {
      const window = document.querySelector(".react-kanban-board");
      window.removeEventListener("mousemove", handleMouseMove);
      window.removeEventListener("mouseup", handleMouseUp);
      if (statePos.startX) {
        setStatePos({ startX: null, startScrollX: null });
      }
    };

    var handleMouseMove = ({ clientX }) => {
      const sliderL = document.querySelector(".react-kanban-board");
      const { startX, startScrollX } = statePos;
      const scrollX = startScrollX - clientX + startX;
      window.scrollTo(scrollX, 0);
      const windowScrollX = window.scrollX;
      if (scrollX !== windowScrollX) {
        setStatePos({
          startX: clientX + windowScrollX - startScrollX,
          startScrollX: startScrollX,
        });
      }
    };

    return (
      <Board
        allowRenameColumn
        allowRemoveCard
        onLaneRemove={console.log}
        onCardRemove={console.log}
        onCardDragEnd={handleCardMove}
        disableColumnDrag
        direction="horizontal"
        onClickCapture={handleCardStart.bind(this)}
        renderCard={(args) => {
          return (
            <div className="react-kanban-card">
              <div className="row">
                <div className="col-md-8" style={{ "margin-left": 10 }}>
                  {/* <strong>{args.title}</strong> */}
                  <div style={{ "margin-top": 5, "font-size": 14 }}>
                    {(() => {
                      if (args.element === "text") {
                        return (
                          <>
                            <div className="form-group">
                              {/* <label>{args.title}</label> */}
                              {/* <div className="input-group" >
                                  
                                  <input type="text" className="form-control form-control-sm" aria-label="Text input with checkbox" placeholder={args.placeholder} style={{'border': '2px solid #009ef7'}} />
                                  
                                  </div> */}
                              <div
                                dangerouslySetInnerHTML={{ __html: args.html }}
                              ></div>
                              {/* <div className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end" style={{'margin-top':'6px'}}>
                                      <input className="form-check-input h-20px w-30px" type="checkbox" value={args.id} id="flexSwitch20x30" onClick={handleCheck}/>
                                      <label className="form-check-label" for="flexSwitch20x30">
                                          required
                                      </label>
                                    </div> */}
                            </div>
                            {/* <div className="form-group fv-row mb-7">
                                    <input className="form-control form-control-sm" value='' placeholder={args.placeholder} name="name" style={{'font-size' : 'small', 'border': '2px solid #009ef7','appearance': 'initial', 'border-radius': '21.474999999999994rem'}}/>
                                   
                                  </div> */}
                          </>
                        );
                      } else if (args.element === "select") {
                        return (
                          <>
                            {/* <label>{args.title}</label> */}
                            {/* <select className='form-control form-control-sm selectpicker' placeholder={args.placeholder} style={{'font-size' : 'small', 'border': '2px solid #009ef7','appearance': 'initial'}}>
                                  {args.data_option.split(';').map((line) => (
                                      <option>{line}</option>
                                    )
                                  )}
                                </select> */}
                            <div
                              dangerouslySetInnerHTML={{ __html: args.html }}
                            ></div>
                            {/* <div className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end" style={{'margin-top':'6px'}}>
                                <input className="form-check-input h-20px w-30px" type="checkbox" value={args.id}  onClick={handleCheck} id="flexSwitch20x30"/>
                                <label className="form-check-label" for="flexSwitch20x30">
                                    required
                                </label>
                              </div> */}
                          </>
                        );
                      } else if (args.element === "checkbox") {
                        return (
                          <div className="form-group row">
                            <span>{args.element}</span>
                            <div
                              dangerouslySetInnerHTML={{ __html: args.html }}
                            ></div>
                            {/* <label className='checkbox checkbox-rounded'>{args.title}</label>
                                        {args.data_option.split(';').map((line) => (
                                          <>
                                              <div className="checkbox checkbox-outline checkbox-success">
                                              <label className="">
                                                <input type="checkbox" id="id[]" value={line} name="Checkboxes15_1" />
                                              <span>{line}</span>
                                            </label>
                                          </div>
                                            </>
                                            )
                                          )} */}
                            {/* <div className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end" style={{'margin-top':'6px'}}>
                                        <input className="form-check-input h-20px w-30px" type="checkbox"  value={args.id}  onClick={handleCheck} id="flexSwitch20x30"/>
                                        <label className="form-check-label" for="flexSwitch20x30">
                                            required
                                        </label>
                                      </div> */}
                          </div>
                        );
                      } else if (args.element === "radiogroup") {
                        return (
                          <div className="form-check form-check-custom form-check-solid form-check-sm">
                            {/* {args.data_option.split(';').map((line) => (
                                          <>
                                              <input className='form-check-input'
                                                  type="radio"
                                                  value={line}
                                              />
                                              <label className="form-check-label" for="flexRadioSm">{line}</label>
                                            </>
                                          
                                      ))} */}
                            <div
                              dangerouslySetInnerHTML={{ __html: args.html }}
                            ></div>

                            {/* <div className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end" style={{'margin-top':'6px'}}>
                                      <input className="form-check-input h-20px w-30px" type="checkbox"  value={args.id}  onClick={handleCheck} id="flexSwitch20x30"/>
                                      <label className="form-check-label" for="flexSwitch20x30">
                                          required
                                      </label>
                                    </div> */}
                          </div>
                        );
                      } else if (args.element === "datepicker") {
                        return (
                          <>
                            <div
                              dangerouslySetInnerHTML={{ __html: args.html }}
                            ></div>
                            {/* <div className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end" style={{'margin-top':'6px'}}>
                                      <input className="form-check-input h-20px w-30px" type="checkbox"  value={args.id}  onClick={handleCheck} id="flexSwitch20x30"/>
                                      <label className="form-check-label" for="flexSwitch20x30">
                                          required
                                      </label>
                                    </div> */}
                          </>
                        );
                      } else if (args.element === "file-doc") {
                        return (
                          <>
                            <div
                              dangerouslySetInnerHTML={{ __html: args.html }}
                            ></div>
                            {/* <div className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end" style={{'margin-top':'6px'}}>
                                      <input className="form-check-input h-20px w-30px" type="checkbox" value={args.id}  onClick={handleCheck} id="flexSwitch20x30"/>
                                      <label className="form-check-label" for="flexSwitch20x30">
                                          required
                                      </label>
                                    </div> */}
                          </>
                        );
                      } else if (args.element === "file") {
                        return (
                          <>
                            <div
                              dangerouslySetInnerHTML={{ __html: args.html }}
                            ></div>
                            {/* <div className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end" style={{'margin-top':'6px'}}>
                                      <input className="form-check-input h-20px w-30px" type="checkbox"  value={args.id}  onClick={handleCheck} id="flexSwitch20x30"/>
                                      <label className="form-check-label" for="flexSwitch20x30">
                                          required
                                      </label>
                                    </div> */}
                          </>
                        );
                      } else if (args.element === "fileimage") {
                        return (
                          <>
                            <div
                              dangerouslySetInnerHTML={{ __html: args.html }}
                            ></div>
                            {/* <div className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end" style={{'margin-top':'6px'}}>
                                      <input className="form-check-input h-20px w-30px" type="checkbox"  value={args.id}  onClick={handleCheck} id="flexSwitch20x30"/>
                                      <label className="form-check-label" for="flexSwitch20x30">
                                          required
                                      </label>
                                    </div> */}
                          </>
                        );
                      } else if (args.element === "uploadfiles") {
                        return (
                          <>
                            <div
                              dangerouslySetInnerHTML={{ __html: args.html }}
                            ></div>
                            {args.required === "required" ? (
                              <div
                                className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                                style={{ marginTop: "6px" }}
                              >
                                <input
                                  className="form-check-input h-20px w-30px"
                                  type="checkbox"
                                  value={args.id}
                                  id="flexSwitch20x30"
                                  defaultChecked={true}
                                  onClick={handleCheck}
                                />
                                <label
                                  className="form-check-label"
                                  for="flexSwitch20x30"
                                >
                                  required
                                </label>
                              </div>
                            ) : (
                              <div
                                className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                                style={{ marginTop: "6px" }}
                              >
                                <input
                                  className="form-check-input h-20px w-30px"
                                  type="checkbox"
                                  defaultChecked={false}
                                  value={args.id}
                                  id="flexSwitch20x30"
                                  onClick={handleCheck}
                                />
                                <label
                                  className="form-check-label"
                                  for="flexSwitch20x30"
                                >
                                  required
                                </label>
                              </div>
                            )}
                          </>
                        );
                      } else {
                        return <div>catch all</div>;
                      }
                    })()}
                  </div>
                </div>
              </div>
            </div>
          );
        }}
      >
        {controlledBoard}
      </Board>
    );
  }

  const saveResult = (e) => {
    console.log(arr_push);
    console.log(Pendaftaran);
  };
  function ControlledBoard() {
    const [controlledBoard, setBoard] = useState(boardRepo);
    function handleCardMove(_card, source, destination) {
      const updatedBoard = moveCard(controlledBoard, source, destination);
      console.log(destination);
      console.log(updatedBoard);
      setBoard(...updatedBoard);
    }

    return (
      <Board
        allowRemoveLane
        allowRenameColumn
        allowRemoveCard
        onLaneRemove={console.log}
        onCardRemove={console.log}
        onLaneRename={console.log}
        allowAddCard={{ on: "top" }}
        onNewCardConfirm={(draftCard) => ({
          id: new Date().getTime(),
          ...draftCard,
        })}
        onCardDragEnd={console.log}
        // initialBoard={boardRepo}

        renderCard={(args) => {
          // console.log(args);
          return (
            <div
              className="react-kanban-card"

              // onClick={(e) => handleMouseDown(e)}
            >
              <div className="row">
                <div className="col-md-1">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    enableBackground="new 0 0 24 24"
                    viewBox="0 0 24 24"
                    fill="black"
                    width="30px"
                    height="35px"
                  >
                    <g>
                      <rect fill="none" height="24" width="24" />
                    </g>
                    <g>
                      <g>
                        <g>
                          <path d="M2.5,19h19v2h-19V19z M19.34,15.85c0.8,0.21,1.62-0.26,1.84-1.06c0.21-0.8-0.26-1.62-1.06-1.84l-5.31-1.42l-2.76-9.02 L10.12,2v8.28L5.15,8.95L4.22,6.63L2.77,6.24v5.17L19.34,15.85z" />
                        </g>
                      </g>
                    </g>
                  </svg>
                </div>
                <div className="col-md-6" style={{ "margin-left": 4 }}>
                  <strong>BA14 - T2</strong>
                  <div style={{ marginTop: -5, fontSize: 14 }}>
                    Expected 21:30
                  </div>
                </div>
                <div className="col-md-4">
                  <strong style={{ float: "right" }}>22:00</strong>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <span>Tesla Model 3</span>
                </div>
                <div className="col-md-6">
                  <strong className="yellow-text">LM20 STR</strong>
                </div>
                <div className="col-md-6">
                  <p
                    style={{
                      color: "purple",
                      marginBottom: 0,
                      marginTop: 4,
                      fontSize: 14,
                    }}
                  >
                    CONFIRMED
                  </p>
                </div>
              </div>
            </div>
          );
        }}
      >
        {boardRepo}
      </Board>

      //   <Board onCardDragEnd={handleCardMove} >
    );
  }
  function ControllerRepo() {
    // console.log(json);
    const [ctrlRepoBoard, setRepoBoard] = useState(RepoSize);
    function handleCardMove(_card, source, destination) {
      const updatedBoardx = moveCard(ctrlRepoBoard, source, destination);
      setRepoBoard(updatedBoardx);
      console.log(destination);
      console.log(updatedBoardx);
    }
    return (
      <Board onCardDragEnd={handleCardMove} disableColumnDrag>
        {ctrlRepoBoard}
      </Board>
    );
  }

  function back() {
    window.location = "/pelatihan/pendaftaran";
  }
  return (
    <div>
      <Header />
      <SideNav />
      <div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h4 style={{ textTransform: "uppercase" }}>
                            Daftar {segment_url[2]}
                          </h4>
                        </div>
                        <div className="card-toolbar">
                          <a
                            href="#"
                            className="btn btn-secondary btn-sm"
                            onClick={back}
                          >
                            <i className="fas fa-arrow-left"></i>
                            KEMBALI
                          </a>
                          &nbsp;
                          <button
                            className="btn btn-primary btn-sm"
                            onClick={handleSubmit}
                          >
                            <i className="las la-paper-plane"></i>
                            Simpan
                          </button>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="mb-10">
                          <label
                            for="exampleFormControlInput1"
                            className="required form-label"
                          >
                            Judul Form
                          </label>
                          <input
                            type="text"
                            id="judul"
                            name="judul"
                            value={Pendaftaran.judul}
                            className="form-control form-control-sm"
                            placeholder="Masukan Judul Form ...."
                            onChange={handleInputChange}
                          />
                        </div>
                        <div className="table-responsive">
                          <ControlledBoard_style />
                          <div className="d-flex justify-content-between align-items-right mt-7 pull-right">
                            {/* <button className='btn btn-primary btn-text-light btn-hover-text-success font-weight-bold btn-sm'>
                        <i className="fas fa-paper-plane action"></i>
                      </button> */}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </div>
  );
};
export default CopyForm;
