import React, { useState, useEffect } from "react";
import Header from "../../components/Header";
import SideNav from "../../components/SideNav";
import Footer from "../../components/Footer";
import Swal from "sweetalert2";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import withReactContent from "sweetalert2-react-content";
import { ReactDOM } from "react-dom";
import Modal from "react-modal";
import { capitalizeFirstLetter } from "../publikasi/helper";
import { cekPermition } from "../AksesHelper";

// import { useForm } from "react-hook-form";
const PendaftaranAdd = () => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  const [uploadFiles, setUploadFiles] = useState(true);
  const [files, setFiles] = useState([]);
  const [KategoriForm, setKategoriForm] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  // let history = useNavigate();

  const groupBy = (array, key) => {
    return array.reduce((result, currentValue) => {
      (result[currentValue[key]] = result[currentValue[key]] || []).push(
        currentValue,
      );
      return result;
    }, {});
  };

  const retriveKategoriForm = () => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/daftarpeserta/list-kategori-form-all",
      )
      .then(function (response) {
        if (response.status == 200) {
          setKategoriForm(response.data.result.Data);
          console.log(KategoriForm);
        }

        console.log(response.data.result.Data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const defrow = [
    {
      name: "",
      element: "",
      size: "",
      option: "",
      data_option: "",
      req: "",
      min: "",
      max: "",
      sts_false: true,
      triger: false,
      span: "",
      upload: "",
      id_parent: "",
      key_parent: "",
      join_parent: "",
      id_form_kategori: "",
    },
  ];
  // const [rows, setRows] = useState([{}]);
  const [rows, setRows] = useState(defrow);
  const history = useNavigate();
  const columnsArray = [
    "Nama Element",
    "Kategori Element",
    "Pilih Element",
    "Min",
    "Max",
    "Option",
    "Data Option",
    "Caption",
    "Trigger",
    "",
  ];
  const initialPendaftaranState = {
    id: null,
    judul: "",
  };
  const MySwal = withReactContent(Swal);
  const columnsName = ["name"];
  const columnElement = ["element"];
  const columnKategori = ["id_form_kategori"];
  const columnSize = ["size"];
  const columnOption = ["option"];
  const columnDataOption = ["data_option"];
  const columnReq = ["req"];

  const columnMin = ["min"];
  const columnMax = ["max"];
  const columnTriger = ["triger"];
  const columnSpan = ["span"];
  const columnUpload = ["upload"];
  const columnIdParent = ["parent"];
  const columnKeyParent = ["key_parent"];
  const columnJoinParent = ["join_parent"];
  const [RepoSize, setRepoSize] = useState([]);
  const [ReferenceSize, setReference] = useState();
  const [Pendaftaran, setPendaftaran] = useState(initialPendaftaranState);
  // const [isDisabled, setDisabled] = useState([]);
  const [isDisabled, setDisabled] = useState([]);
  const [isText, setText] = useState([{}]);
  const [isUpload, setUpload] = useState([]);
  const [modalIsOpen, setIsOpen] = React.useState(false);

  const customStyles = {
    content: {
      top: "55%",
      left: "60%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
      width: "70%",
      height: "88%",
    },
  };

  let subtitle;

  const isRequired = (value) => {
    return value != null && value.trim().length > 0;
  };
  useEffect(() => {
    // retriveRepository();
    retriveReferences();
    retriveKategoriForm();
    // setText([{...isText,index:'1'}]);
  }, []);
  const handleAddRow = () => {
    console.log(rows);
    // rows.map((number) =>
    //   console.log(number.name)
    // )
    const item = {
      name: "",
      element: "",
      size: "",
      option: "",
      data_option: "",
      req: "",

      min: "",
      max: "",
      sts_false: true,
      triger: "",
      span: "",
      upload: [],
      parent: "",
      key_parent: "",
      join_parent: "",
      triggered: "",
      id_form_kategori: "",
    };
    setRows([...rows, item]);
  };
  const handleInputChange = (event) => {
    console.log(event.target);
    const { name, value } = event.target;
    setPendaftaran({ ...Pendaftaran, [name]: value });
  };
  const onFileChange = (e) => {
    let prope = e.target.attributes.column.value; //upload
    let index = e.target.attributes.index.value; //0
    let fieldValue = e.target.files[0];
    const tempRows = [...rows];
    const tempObj = rows[index];
    tempObj[prope] = fieldValue;
    tempRows[index] = tempObj;
    setRows(tempRows);
  };
  const handleChange_Repo = (e) => {
    let index = e.nativeEvent.target.selectedIndex;
    let label = e.nativeEvent.target[index].text;
    // console.log(label);
    const { name, value } = e.target;
    setPendaftaran({ ...Pendaftaran, [name]: value, judul_nama: label });
  };
  const postResults = () => {
    console.log(rows);
  };
  const retriveRepository = () => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/list-repo-judul",
        {
          start: "0",
          length: "100",
        },
      )
      .then(function (response) {
        if (response.status === 200) {
          console.log(response);
          let repo = response.data.result;
          if (repo.Status === true) {
            // cnso
            setRepoSize(repo.Data);
            console.log(RepoSize);
          } else {
            setRepoSize();
          }
        }
      })
      .catch(function (error) {
        setRepoSize();
      });
  };
  const retriveReferences = () => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/list-reference",
        {
          start: "0",
          length: "50",
        },
      )
      .then(function (response) {
        if (response.status === 200) {
          console.log(response);
          let repo = response.data.result;
          if (repo.Status === true) {
            // cnso
            setReference(repo.Data);
          }
        }
      })
      .catch(function (error) {});
  };
  const postResults_Save = () => {
    let categoryOptItems = [];
    let summerAt = "";
    let sumNot = "";
    let sumVar = "";

    const listItems = rows.map((number) => {
      console.log(number);

      if (number.name === undefined) {
        MySwal.fire({
          title: <strong>Perhatian!</strong>,
          html: <i>Silahkan Lengkapi Nama Field!</i>,
          icon: "warning",
        });
        return false;
      }
      if (number.name == "") {
        MySwal.fire({
          title: <strong>Perhatian!</strong>,
          html: <i>Silahkan Lengkapi Nama Field!</i>,
          icon: "warning",
        });
        return false;
      }
      if (number.element === undefined) {
        MySwal.fire({
          title: <strong>Perhatian!</strong>,
          html: <i>Silahkan Lengkapi Data Tipe Element!</i>,
          icon: "warning",
        });
        return false;
      }
      if (number.element === "") {
        MySwal.fire({
          title: <strong>Perhatian!</strong>,
          html: <i>Silahkan Lengkapi Data Tipe Element!</i>,
          icon: "warning",
        });
        return false;
      }

      if (parseInt(number.min) < 0 || parseInt(number.max) < 0) {
        MySwal.fire({
          title: <strong>Perhatian!</strong>,
          html: <i>Kolom Min dan Max tidak bisa kurang dari 0</i>,
          icon: "warning",
        });
        return false;
      }

      if (parseInt(number.min) > 512 || parseInt(number.max) > 512) {
        MySwal.fire({
          title: <strong>Perhatian!</strong>,
          html: <i>Kolom Min dan Max tidak bisa lebih dari 512</i>,
          icon: "warning",
        });
        return false;
      }

      if (parseInt(number.min) > parseInt(number.max)) {
        MySwal.fire({
          title: <strong>Perhatian!</strong>,
          html: <i>Kolom Min tidak bisa lebih dari kolom Max</i>,
          icon: "warning",
        });
        return false;
      }

      if (
        number.element === "select" ||
        number.element === "checkbox" ||
        number.element === "radiogroup" ||
        number.element === "trigered"
      ) {
        if (number.data_option === undefined || number.data_option === "") {
          MySwal.fire({
            title: <strong>Perhatian!</strong>,
            html: <i>Silahkan Isi Data Option!</i>,
            icon: "warning",
          });
          return false;
        }
      }

      if (number.option === undefined) {
        sumVar = "";
      } else {
        sumVar = number.option;
      }
      if (number.data_option === undefined) {
        summerAt = "";
      } else {
        summerAt = number.data_option.trim();
      }
      if (number.req === "") {
        sumNot = "";
      } else {
        sumNot = "";
        // sumNot = 'required'
      }
      let kor = "0";
      if (number.triger === true) {
        kor = "0";
      } else {
        kor = "1";
      }
      if (
        number.element !== "uploadfiles" &&
        number.element !== "fileimage" &&
        number.element !== "file-doc"
      ) {
        setIsLoading(true);
        const formData = new FormData();
        formData.append("judul", capitalizeFirstLetter(Pendaftaran.judul));
        formData.append("name", capitalizeFirstLetter(number.name));
        formData.append("element", number.element);
        formData.append("size", "col-md-12");
        formData.append("option", sumVar);
        formData.append("data_option", summerAt);
        formData.append("required", "");
        formData.append("min", number.min);
        formData.append("max", number.max);
        formData.append("maxlength", number.max);
        formData.append("className", "form-solid");

        formData.append("span", capitalizeFirstLetter(number.span));
        formData.append("upload", number.upload);

        formData.append("file_name", number.name.replace(/ /g, "_"));
        formData.append("triggered", number.triggered); //1
        formData.append("triggered_parent", number.triggered_parent); //1
        formData.append("triggered_name", number.triggered_name); //dataoption trivger
        formData.append("key_triggered_name", number.key_triggered_name);
        formData.append("id_form_kategori", number.id_form_kategori);

        axios
          .post(process.env.REACT_APP_BASE_API_URI + "/create-repo", formData)
          .then(function (response) {
            setIsLoading(false);
            MySwal.fire({
              title: <strong>Perhatian!</strong>,
              html: <i>{response.data.result.Message}</i>,
              icon: "success",
            });
            history("/pelatihan/element");
          })
          .catch(function (error) {
            setIsLoading(false);
            MySwal.fire({
              title: <strong>Perhatian!</strong>,
              html: <i>{error.response.data.result.Message}</i>,
              icon: "warning",
            });
          });
      } else {
        setIsLoading(true);
        const formData = new FormData();
        formData.append("judul", capitalizeFirstLetter(Pendaftaran.judul));
        formData.append("name", capitalizeFirstLetter(number.name));
        formData.append("element", number.element);
        formData.append("size", "col-md-12");
        formData.append("option", "");
        formData.append("data_option", number.upload);
        formData.append("required", "");
        formData.append("min", number.min);
        formData.append("max", number.max);
        formData.append("maxlength", number.max);
        formData.append("className", "form-solid");

        formData.append("span", capitalizeFirstLetter(number.span));
        formData.append("upload", number.upload);
        formData.append("file_name", number.name.replace(/ /g, "_"));
        formData.append("triggered", number.triggered); //1
        formData.append("triggered_parent", number.triggered_parent); //1
        formData.append("triggered_name", number.triggered_name); //dataoption trivger
        formData.append("key_triggered_name", number.triggered_name);
        formData.append("id_form_kategori", number.id_form_kategori);

        axios
          .post(process.env.REACT_APP_BASE_API_URI + "/create-repo", formData)
          .then(function (response) {
            setIsLoading(false);
            MySwal.fire({
              title: <strong>Perhatian!</strong>,
              html: <i>{response.data.result.Message}</i>,
              icon: "success",
            });
            console.log(response);
            history("/pelatihan/element");
          })
          .catch(function (error) {
            setIsLoading(false);
            MySwal.fire({
              title: <strong>Perhatian!</strong>,
              html: <i>{error.response.data.result.Message}</i>,
              icon: "warning",
            });
          });
      }
    });

    const formData = new FormData();
    formData.append("judul", Pendaftaran.judul);
  };

  const handleRemoveSpecificRow = (idx) => {
    const tempRows = [...rows];
    tempRows.splice(idx, 1);
    setRows(tempRows);
  };
  const checkTriger = (idx, e) => {
    console.log(idx);
    console.log(rows[0]);
    console.log(e.target.checked);
    let prope = e.target.attributes.column.value;
    let index_up = e.target.attributes.index.value;
    // console.log(index_);
    let fieldValue = e.target.checked;
    const tempRows = [...rows];
    const tempObj = rows[idx];
    tempObj[prope] = fieldValue;
    tempRows[idx] = tempObj;
    setRows(tempRows);
    if (e.target.checked === true) {
      let furo = [];
      let item = [...rows];
      rows[idx].data_option.split(";").map((number) =>
        item.push({
          name: "",
          element: "",
          size: "",
          option: "",
          data_option: "",
          req: "",
          min: "",
          max: "",
          sts_false: true,
          triger: "",
          span: "",
          upload: [],
          triggered: 1,
          triggered_parent: 0,
          triggered_name: capitalizeFirstLetter(rows[idx].name),
          key_triggered_name: number,
          parent: number,
          key_parent: capitalizeFirstLetter(rows[idx].name),
          join_parent: capitalizeFirstLetter(rows[idx].name) + ":" + number,
          id_form_kategori: rows[idx].id_form_kategori,
        }),
      );
      console.log(item);
      setRows(item);
    } else {
      var index = rows.indexOf(rows[idx].parent);
      if (index > -1) {
        //Make sure item is present in the array, without if condition, -n indexes will be considered from the end of the array.
        rows.splice(index, 1);
      }
      // handleRemoveSpecificRow(idx);
    }
  };
  const handleAddRowTriger = (idx) => {
    // if(e.target.checked === true){
    let furo = [];
    let item = [...rows];
    // rows[idx].data_option.split(';').map((number) =>
    item.push({
      name: "",
      element: "",
      size: "",
      option: "",
      data_option: "",
      req: "",

      min: "",
      max: "",
      sts_false: true,
      triger: "",
      span: "",
      upload: [],
      triggered: 1,
      triggered_parent: 0,
      triggered_name: capitalizeFirstLetter(rows[idx].key_parent),
      key_triggered_name: rows[idx].parent,
      parent: rows[idx].parent,
      key_parent: capitalizeFirstLetter(rows[idx].key_parent),
      join_parent:
        capitalizeFirstLetter(rows[idx].key_parent) + ":" + rows[idx].parent,
      id_form_kategori: rows[idx].id_form_kategori,
    });
    // )

    console.log(item);
    setRows(item.sort((a, b) => (a.join_parent > b.join_parent ? 1 : -1)));
    console.log(rows);
    console.log(groupArrayOfObjects(rows, "join_parent"));
  };

  function groupArrayOfObjects(list, key) {
    return list.reduce(function (rv, x) {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  }
  const updateState = (e) => {
    let prope = e.target.attributes.column.value;
    let index = e.target.attributes.index.value;

    console.log(e.target.value);
    console.log(prope);
    console.log(index);

    console.log(rows[index]);

    let item = rows;
    item.map((column, idx) => {
      console.log(column);
      console.log(idx);
      if (item[idx].key_parent === item[index].name) {
        if (item[idx].key_parent !== "") {
          item[idx].id_form_kategori = e.target.value;
        }

        setRows(item);
      }
    });

    let fieldValue = e.target.value;

    if (prope == "min" || prope == "max") {
      if (e.target.value < 0) {
        MySwal.fire({
          title: <strong>Perhatian</strong>,
          html: (
            <i>
              Tidak bisa kurang dari <b>0</b>
            </i>
          ),
          icon: "warning",
        });

        e.target.attributes.value = 0;
      }

      if (e.target.value > 512) {
        MySwal.fire({
          title: <strong>Perhatian</strong>,
          html: (
            <i>
              Tidak bisa lebih dari <b>512</b>{" "}
            </i>
          ),
          icon: "warning",
        });

        e.target.attributes.value = 0;
      }
    }

    const tempRows = [...rows];
    const tempObj = rows[index];
    tempObj[prope] = fieldValue;
    tempRows[index] = tempObj;
    const tempObjx = columnOption[index];
    let checkCol = [prope] + [index];
    let opt = "option";
    if (e.target.attributes.index.value === index) {
      if (prope === "element") {
        tempRows[index]["min"] = "";
        tempRows[index]["max"] = "";
        tempRows[index]["option"] = "";
        tempRows[index]["data_option"] = "";
        tempRows[index]["span"] = "";
        if (
          tempObj[prope] === "select" ||
          tempObj[prope] === "checkbox" ||
          tempObj[prope] === "radiogroup"
        ) {
          setDisabled({ isDisabled, [index]: false });
        } else {
          setDisabled({ isDisabled, [index]: true });
        }
      }
      setRows(tempRows);
      if (prope === "option") {
        if (tempObj[prope] === "manual") {
          setText([[index], { index: "1" }]);
        } else if (tempObj[prope] === "referensi") {
          setText([[index], { [index]: "2" }]);
        } else {
          setText([[index], { index: "3" }]);
        }
        console.log(isText);
      }
    }
  };

  function outOfBox(props) {
    if (props === "referensi") {
      return (
        <select
          className="form-control-sm form-solid"
          data-kt-select2="true"
          data-placeholder="Select option"
          data-dropdown-parent="#kt_menu_61484c5a8da38"
          data-allow-clear="true"
        >
          {/* <option /> */}
          <option value="1">Publish</option>
          <option value="0">Unpublish</option>
        </select>
      );
    } else {
    }
  }

  function openModal() {
    setIsOpen(true);
  }

  function afterOpenModal() {
    // references are now sync'd and can be accessed.
  }

  function closeModal() {
    setIsOpen(false);
  }

  function back() {
    window.location = "/pelatihan/element";
  }

  return (
    <div>
      <Header />
      <SideNav />
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Form Element
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={back}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Tambah Element
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <button
                            className="btn btn-flex btn-info btn-sm px-6"
                            data-bs-toggle="modal"
                            data-bs-target="#harap-baca"
                          >
                            <i className="fa fa-info-circle"></i>
                            Harap Baca
                          </button>

                          {/* Harap Baca */}
                          <div
                            className="modal fade"
                            tabIndex="-1"
                            id="harap-baca"
                          >
                            <div className="modal-dialog modal-lg">
                              <div className="modal-content">
                                <div className="modal-header">
                                  <h5 className="modal-title">
                                    Infromasi Penting
                                  </h5>
                                  <div
                                    className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                    data-bs-dismiss="modal"
                                    aria-label="Close"
                                  >
                                    <span className="svg-icon svg-icon-2x">
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                      >
                                        <rect
                                          opacity="0.5"
                                          x="6"
                                          y="17.3137"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(-45 6 17.3137)"
                                          fill="currentColor"
                                        />
                                        <rect
                                          x="7.41422"
                                          y="6"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(45 7.41422 6)"
                                          fill="currentColor"
                                        />
                                      </svg>
                                    </span>
                                  </div>
                                </div>
                                <div className="modal-body">
                                  {/* Preview widget */}
                                  <div className="m-5">
                                    <p>
                                      Berikut adalah Data Pokok yang telah diisi
                                      oleh Peserta saat melakukan registrasi
                                      akun :
                                    </p>
                                    <div className="content-profile mb-5">
                                      <h6 className="mb-2 fz-16 text-dark fw-600">
                                        Data Diri
                                      </h6>
                                      <p className="fz-16">
                                        Photo Profil, Nama Lengkap, NIK, Email,
                                        Jenis Kelamin, Nomor Handphone, Tanggal
                                        Lahir
                                      </p>
                                    </div>
                                    <div className="content-profile mb-5">
                                      <h6 className="mb-2 fz-16 text-dark fw-600">
                                        Alamat Domisili
                                      </h6>
                                      <p className="fz-16">
                                        Provinsi, Kota/Kabupaten, Kecamatan,
                                        Desa/Kelurahan
                                      </p>
                                    </div>
                                    <div className="content-profile">
                                      <h6 className="mb-2 fz-16 text-dark fw-600">
                                        Pekerjaan
                                      </h6>
                                      <p className="fz-16 mb-2">
                                        Status Pekerjaan :
                                      </p>
                                      <ul className="fz-16">
                                        <li>
                                          Bekerja : Pekerjaan,
                                          Perusahaan/Institusi Tempat Bekerja
                                        </li>
                                        <li>Pelajar/Mahasiswa</li>
                                        <li>Tidak Bekerja</li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>

                                <div className="modal-footer">
                                  <button
                                    className="btn btn-danger btn-sm"
                                    data-bs-dismiss="modal"
                                  >
                                    Close
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="table-responsive">
                          <table
                            className="table table-bordered table-responsive"
                            id="tabx_logic"
                            style={{ width: "max-content" }}
                          >
                            <thead>
                              <tr>
                                {columnsArray.map((column, index) => (
                                  <th
                                    className="fw-semibold font-size-sm"
                                    key={index}
                                  >
                                    {column}
                                  </th>
                                ))}
                              </tr>
                            </thead>
                            <tbody>
                              {rows.map((item, idx) => (
                                <>
                                  {columnJoinParent.map((column, index) =>
                                    rows[idx][column] === "" ? (
                                      <>
                                        <tr key={idx}>
                                          {columnsName.map((column, index) => (
                                            <td
                                              key={index}
                                              style={{ width: "200px" }}
                                            >
                                              <div className="form-group">
                                                <input
                                                  type="text"
                                                  column={column}
                                                  value={rows[idx][column]}
                                                  index={idx}
                                                  placeholder="Masukkan Nama Element"
                                                  className="form-control form-control-sm"
                                                  required
                                                  onChange={(e) =>
                                                    updateState(e)
                                                  }
                                                />
                                              </div>
                                            </td>
                                          ))}
                                          {columnKategori.map(
                                            (column, index) => (
                                              <td
                                                key={index}
                                                style={{ width: "180px" }}
                                              >
                                                <select
                                                  value={rows[idx][column]}
                                                  className="form-control  form-control-sm"
                                                  data-placeholder="Select option"
                                                  index={idx}
                                                  column={column}
                                                  data-allow-clear="true"
                                                  id="id_kategori_form[]"
                                                  name="id_kategori_form"
                                                  required
                                                  onChange={(e) =>
                                                    updateState(e)
                                                  }
                                                >
                                                  <option value={0}>
                                                    -- Pilih Kategori --
                                                  </option>
                                                  {KategoriForm.map((val) => (
                                                    <>
                                                      <option value={val.value}>
                                                        {" "}
                                                        {val.label}{" "}
                                                      </option>
                                                    </>
                                                  ))}
                                                </select>
                                              </td>
                                            ),
                                          )}
                                          {columnElement.map(
                                            (column, index) => (
                                              <td
                                                key={index}
                                                style={{ width: "180px" }}
                                              >
                                                <select
                                                  value={rows[idx][column]}
                                                  className="form-control  form-control-sm"
                                                  data-placeholder="Select option"
                                                  index={idx}
                                                  column={column}
                                                  data-allow-clear="true"
                                                  id="pilih_element[]"
                                                  name="pilih_element"
                                                  required
                                                  onChange={(e) =>
                                                    updateState(e)
                                                  }
                                                >
                                                  <option>
                                                    --- Pilih Element --
                                                  </option>
                                                  <option value="select">
                                                    Select
                                                  </option>
                                                  <option value="checkbox">
                                                    Checkbox
                                                  </option>
                                                  <option value="text">
                                                    Text
                                                  </option>
                                                  <option value="text-numeric">
                                                    Text Format Numeric
                                                  </option>
                                                  <option value="text-email">
                                                    Text Format Email
                                                  </option>
                                                  <option value="text-URL">
                                                    Text Format URL
                                                  </option>
                                                  <option value="textarea">
                                                    Text Area
                                                  </option>
                                                  <option value="radiogroup">
                                                    Radio
                                                  </option>
                                                  <option value="datepicker">
                                                    Input Date
                                                  </option>
                                                  <option value="file-doc">
                                                    File Upload Dokumen
                                                  </option>
                                                  <option value="fileimage">
                                                    File Upload Gambar
                                                  </option>
                                                  <option value="trigered">
                                                    Triger
                                                  </option>
                                                </select>
                                              </td>
                                            ),
                                          )}

                                          {columnMin.map((column, index) => (
                                            <>
                                              {item["element"] === "select" ||
                                              item["element"] ===
                                                "radiogroup" ||
                                              item["element"] === "checkbox" ||
                                              item["element"] === "file-doc" ||
                                              item["element"] === "fileimage" ||
                                              item["element"] ===
                                                "uploadfiles" ||
                                              item["element"] ===
                                                "datepicker" ||
                                              item["element"] === "textarea" ||
                                              item["element"] === "trigered" ? (
                                                <td
                                                  key={index}
                                                  style={{ width: "90px" }}
                                                >
                                                  <div className="form-group">
                                                    <input
                                                      type="number"
                                                      column={column}
                                                      value={rows[idx][column]}
                                                      index={idx}
                                                      max="225"
                                                      min="1"
                                                      placeholder="Min"
                                                      className="form-control form-control-sm"
                                                      disabled
                                                      onChange={(e) =>
                                                        updateState(e)
                                                      }
                                                    />
                                                  </div>
                                                </td>
                                              ) : (
                                                <td
                                                  key={index}
                                                  style={{ width: "90px" }}
                                                >
                                                  <div className="form-group">
                                                    <input
                                                      type="number"
                                                      column={column}
                                                      value={rows[idx][column]}
                                                      index={idx}
                                                      max="225"
                                                      min="1"
                                                      placeholder="Min"
                                                      className="form-control form-control-sm"
                                                      onChange={(e) =>
                                                        updateState(e)
                                                      }
                                                    />
                                                  </div>
                                                </td>
                                              )}
                                            </>
                                          ))}
                                          {columnMax.map((column, index) => (
                                            <>
                                              {item["element"] === "select" ||
                                              item["element"] ===
                                                "radiogroup" ||
                                              item["element"] === "checkbox" ||
                                              item["element"] === "file-doc" ||
                                              item["element"] === "fileimage" ||
                                              item["element"] ===
                                                "uploadfiles" ||
                                              item["element"] ===
                                                "datepicker" ||
                                              item["element"] === "textarea" ||
                                              item["element"] === "trigered" ? (
                                                <td
                                                  key={index}
                                                  style={{ width: "90px" }}
                                                >
                                                  <div className="form-group">
                                                    <input
                                                      type="number"
                                                      column={column}
                                                      value={rows[idx][column]}
                                                      index={idx}
                                                      max="225"
                                                      min="1"
                                                      placeholder="Max"
                                                      className="form-control form-control-sm"
                                                      onChange={(e) =>
                                                        updateState(e)
                                                      }
                                                      disabled
                                                    />
                                                  </div>
                                                </td>
                                              ) : (
                                                <td
                                                  key={index}
                                                  style={{ width: "90px" }}
                                                >
                                                  <div className="form-group">
                                                    <input
                                                      type="number"
                                                      column={column}
                                                      value={rows[idx][column]}
                                                      index={idx}
                                                      max="225"
                                                      min="1"
                                                      placeholder="Max"
                                                      className="form-control form-control-sm"
                                                      onChange={(e) =>
                                                        updateState(e, idx)
                                                      }
                                                    />
                                                  </div>
                                                </td>
                                              )}
                                            </>
                                          ))}
                                          <>
                                            {item["element"] === "select" ||
                                            item["element"] === "checkbox" ||
                                            item["element"] === "radiogroup" ||
                                            item["element"] === "trigered" ? (
                                              <>
                                                {columnOption.map(
                                                  (column, index) => (
                                                    <td key={index}>
                                                      <>
                                                        <select
                                                          value={
                                                            rows[idx][column]
                                                          }
                                                          className="form-control form-control-sm selectpicker"
                                                          data-placeholder="Select option"
                                                          index={idx}
                                                          column={column}
                                                          data-allow-clear="true"
                                                          id="pilih_element[]"
                                                          name="pilih_element"
                                                          onChange={(e) =>
                                                            updateState(e)
                                                          }
                                                        >
                                                          <option>
                                                            -- Pilih Option --
                                                          </option>
                                                          <option value="manual">
                                                            Manual
                                                          </option>
                                                          <option value="referensi">
                                                            Select Reference
                                                          </option>
                                                        </select>
                                                      </>
                                                    </td>
                                                  ),
                                                )}
                                                {columnDataOption.map(
                                                  (column, index) => (
                                                    <td key={index}>
                                                      {item["option"] ===
                                                        "manual" ||
                                                      item["option"] === "" ? (
                                                        <>
                                                          {/* <span>{columnElement}</span> */}
                                                          {/* <span>{item['element']}</span> */}

                                                          <div className="form-group">
                                                            <input
                                                              type="text"
                                                              column={column}
                                                              value={
                                                                rows[idx][
                                                                  column
                                                                ]
                                                              }
                                                              index={idx}
                                                              id="data_option[]"
                                                              placeholder="Data1;Data2"
                                                              className="form-control form-control-sm"
                                                              onChange={(e) =>
                                                                updateState(e)
                                                              }
                                                            />
                                                          </div>
                                                        </>
                                                      ) : (
                                                        <>
                                                          <select
                                                            value={
                                                              rows[idx][column]
                                                            }
                                                            className="form-control  form-control-sm selectpicker"
                                                            data-placeholder="Select option"
                                                            index={idx}
                                                            column={column}
                                                            data-allow-clear="true"
                                                            id="data_option[]"
                                                            name="pilih_element"
                                                            onChange={(e) =>
                                                              updateState(e)
                                                            }
                                                          >
                                                            <option value="">
                                                              Pilih Referensi
                                                            </option>
                                                            {ReferenceSize.map(
                                                              (size) => (
                                                                <option
                                                                  value={
                                                                    size.id
                                                                  }
                                                                >
                                                                  {size.name}
                                                                </option>
                                                              ),
                                                            )}
                                                          </select>
                                                        </>
                                                      )}
                                                    </td>
                                                  ),
                                                )}
                                              </>
                                            ) : item["element"] ===
                                              "file-doc" ? (
                                              <>
                                                {columnUpload.map(
                                                  (column, index) => (
                                                    <td colSpan="2">
                                                      {item["element"] ===
                                                      "file-doc" ? (
                                                        <>
                                                          <input
                                                            type="file"
                                                            className="form-control form-control-sm font-size-h4"
                                                            name="data_option"
                                                            index={idx}
                                                            column={column}
                                                            accept="application/pdf"
                                                            id="data_option[]"
                                                            onChange={(e) =>
                                                              onFileChange(e)
                                                            }
                                                          ></input>

                                                          <div
                                                            style={{
                                                              color: "#7a7a7a",
                                                            }}
                                                          >
                                                            {" "}
                                                            )* Upload template
                                                            file jika ada{" "}
                                                          </div>
                                                        </>
                                                      ) : (
                                                        <></>
                                                      )}
                                                    </td>
                                                  ),
                                                )}
                                              </>
                                            ) : (
                                              <>
                                                {columnOption.map(
                                                  (column, index) => (
                                                    <td
                                                      key={index}
                                                      style={{
                                                        pointerEvents: "none",
                                                      }}
                                                    >
                                                      <>
                                                        <select
                                                          value={
                                                            rows[idx][column]
                                                          }
                                                          className="form-control form-control-sm selectpicker"
                                                          data-placeholder="Select option"
                                                          index={idx}
                                                          column={column}
                                                          data-allow-clear="true"
                                                          id="pilih_element[]"
                                                          name="pilih_element"
                                                          onChange={(e) =>
                                                            updateState(e)
                                                          }
                                                          disabled
                                                        >
                                                          <option>
                                                            -- Pilih Option --
                                                          </option>
                                                          <option value="manual">
                                                            Manual
                                                          </option>
                                                          <option value="referensi">
                                                            Select Reference
                                                          </option>
                                                        </select>
                                                      </>
                                                    </td>
                                                  ),
                                                )}
                                                {columnDataOption.map(
                                                  (column, index) => (
                                                    <td
                                                      key={index}
                                                      style={{
                                                        pointerEvents: "none",
                                                      }}
                                                    >
                                                      {item["option"] ===
                                                        "manual" ||
                                                      item["option"] === "" ? (
                                                        <>
                                                          {/* <span>{columnElement}</span> */}
                                                          {/* <span>{item['element']}</span> */}

                                                          <input
                                                            type="text"
                                                            column={column}
                                                            value={
                                                              rows[idx][column]
                                                            }
                                                            index={idx}
                                                            id="data_option[]"
                                                            placeholder="Data1;Data2"
                                                            className="form-control form-control-sm"
                                                            onChange={(e) =>
                                                              updateState(e)
                                                            }
                                                            disabled
                                                          />
                                                        </>
                                                      ) : (
                                                        <>
                                                          {/* {isText[idx]} */}
                                                          {/* {isDisabled[idx]} */}
                                                          <select
                                                            value={
                                                              rows[idx][column]
                                                            }
                                                            className="form-control  form-control-sm selectpicker"
                                                            data-placeholder="Select option"
                                                            index={idx}
                                                            column={column}
                                                            data-allow-clear="true"
                                                            id="data_option[]"
                                                            name="pilih_element"
                                                            onChange={(e) =>
                                                              updateState(e)
                                                            }
                                                            disabled
                                                          >
                                                            <option value="">
                                                              Pilh Referensi
                                                            </option>
                                                            {ReferenceSize.map(
                                                              (size) => (
                                                                <option
                                                                  value={
                                                                    size.id
                                                                  }
                                                                >
                                                                  {size.name}
                                                                </option>
                                                              ),
                                                            )}
                                                          </select>
                                                        </>
                                                      )}
                                                    </td>
                                                  ),
                                                )}
                                              </>
                                            )}
                                          </>
                                          {columnSpan.map((column, index) => (
                                            <td key={index}>
                                              <input
                                                type="text"
                                                column={column}
                                                value={rows[idx][column]}
                                                index={idx}
                                                placeholder="Caption"
                                                className="form-control form-control-sm"
                                                onChange={(e) => updateState(e)}
                                              />
                                            </td>
                                          ))}
                                          {/* {columnReq.map((column, index) => (
                                          <td key={index}>
                                              <input type="checkbox" 
                                                value={rows[idx][column]}  index={idx}
                                              column={column} name="Checkboxes" onChange={(e) => updateState(e)}/>
                                              <span></span>
                                              
                                          </td>
                                          ))} */}
                                          {item["element"] === "trigered" ? (
                                            columnTriger.map(
                                              (column, index) => (
                                                <td key={index}>
                                                  <input
                                                    className="form-check-input align-self-center"
                                                    type="checkbox"
                                                    id="flexSwitch20x30"
                                                    index={idx}
                                                    value={rows[idx][column]}
                                                    checked={rows[idx][column]}
                                                    column={column}
                                                    name="Checkboxes"
                                                    onChange={(e) =>
                                                      checkTriger(idx, e)
                                                    }
                                                  />

                                                  <span></span>
                                                </td>
                                              ),
                                            )
                                          ) : (
                                            <td key={index}>
                                              <span> - </span>
                                            </td>
                                          )}
                                          {/* {columnIdParent.map((column, index) => (
                                          <td key={index}>
                                              <input
                                                      type="text"
                                                      column={column}
                                                      value={rows[idx][column]}
                                                      index={idx}
                                                      id="parent[]"
                                                      placeholder="Span"
                                                      
                                                      className="form-control form-control-sm"
                                                      onChange={(e) => updateState(e)}
                                                      />
                                              <span></span>
                                              
                                          </td>
                                          ))}
                                            {columnKeyParent.map((column, index) => (
                                          <td key={index}>
                                              <input
                                                      type="text"
                                                      column={column}
                                                      value={rows[idx][column]}
                                                      index={idx}
                                                      id="parent[]"
                                                      placeholder="Span"
                                                      
                                                      className="form-control form-control-sm"
                                                      onChange={(e) => updateState(e)}
                                                      />
                                              <span></span>
                                              
                                          </td>
                                          ))} */}
                                          <td>
                                            <button
                                              className="btn btn-icon btn-danger btn-sm me-1"
                                              onClick={() =>
                                                handleRemoveSpecificRow(idx)
                                              }
                                            >
                                              <i className="fas fa-trash fs-5">
                                                {item["triger"]}
                                              </i>
                                            </button>
                                          </td>
                                        </tr>
                                      </>
                                    ) : (
                                      <>
                                        <div className="d-flex flex-stack cursor-pointer p-0 m-0">
                                          <span className="d-flex align-items-center me-2 mb-0">
                                            <span className="symbol me-4">
                                              <h6 className="text-primary ms-3 me-2 align-self-center mb-0">
                                                {rows[idx][column]}
                                              </h6>
                                            </span>
                                            <span className="d-flex flex-column">
                                              <a
                                                href="#"
                                                onClick={() =>
                                                  handleAddRowTriger(idx)
                                                }
                                                className="badge bg-light text-success fs-7 m-1 align-self-center"
                                              >
                                                <i className="fa fa-plus-circle me-1 text-success"></i>
                                                Tambah Child Element
                                              </a>
                                            </span>
                                          </span>
                                        </div>

                                        <tr key={idx}>
                                          {columnsName.map((column, index) => (
                                            <td
                                              key={index}
                                              style={{ width: "200px" }}
                                            >
                                              <div
                                                className="form-group"
                                                style={{ paddingLeft: "20px" }}
                                              >
                                                <input
                                                  type="text"
                                                  column={column}
                                                  value={rows[idx][column]}
                                                  index={idx}
                                                  placeholder="Field"
                                                  className="form-control form-control-sm"
                                                  required
                                                  onChange={(e) =>
                                                    updateState(e)
                                                  }
                                                />
                                              </div>
                                            </td>
                                          ))}
                                          {columnKategori.map(
                                            (column, index) => (
                                              <td
                                                key={index}
                                                style={{ width: "180px" }}
                                              >
                                                <select
                                                  value={rows[idx][column]}
                                                  className="form-control  form-control-sm"
                                                  data-placeholder="Select option"
                                                  index={idx}
                                                  column={column}
                                                  data-allow-clear="true"
                                                  id="id_kategori_form[]"
                                                  name="id_kategori_form"
                                                  disabled
                                                  required
                                                  onChange={(e) =>
                                                    updateState(e)
                                                  }
                                                >
                                                  <option value={0}>
                                                    -- Pilih Kategori --
                                                  </option>
                                                  {KategoriForm.map((val) => (
                                                    <>
                                                      <option value={val.value}>
                                                        {" "}
                                                        {val.label}{" "}
                                                      </option>
                                                    </>
                                                  ))}
                                                </select>
                                              </td>
                                            ),
                                          )}
                                          {columnElement.map(
                                            (column, index) => (
                                              <td
                                                key={index}
                                                style={{ width: "140px" }}
                                              >
                                                <select
                                                  value={rows[idx][column]}
                                                  className="form-control  form-control-sm"
                                                  data-placeholder="Select option"
                                                  index={idx}
                                                  column={column}
                                                  data-allow-clear="true"
                                                  id="pilih_element[]"
                                                  name="pilih_element"
                                                  required
                                                  onChange={(e) =>
                                                    updateState(e)
                                                  }
                                                >
                                                  <option>
                                                    -- Pilih Element --
                                                  </option>
                                                  <option value="select">
                                                    Select
                                                  </option>
                                                  <option value="checkbox">
                                                    Checkbox
                                                  </option>
                                                  <option value="text">
                                                    Text
                                                  </option>
                                                  <option value="text-numeric">
                                                    Text Format Numeric
                                                  </option>
                                                  <option value="text-email">
                                                    Text Format Email
                                                  </option>
                                                  <option value="text-URL">
                                                    Text Format URL
                                                  </option>
                                                  <option value="textarea">
                                                    Text Area
                                                  </option>
                                                  <option value="radiogroup">
                                                    Radio
                                                  </option>
                                                  <option value="datepicker">
                                                    Input Date
                                                  </option>
                                                  <option value="file-doc">
                                                    File Upload Dokumen
                                                  </option>
                                                  <option value="fileimage">
                                                    File Upload Gambar
                                                  </option>
                                                  <option value="trigered">
                                                    Triger
                                                  </option>
                                                </select>
                                              </td>
                                            ),
                                          )}

                                          {columnMin.map((column, index) => (
                                            <>
                                              {item["element"] === "select" ||
                                              item["element"] ===
                                                "radiogroup" ||
                                              item["element"] === "checkbox" ||
                                              item["element"] === "file-doc" ||
                                              item["element"] ===
                                                "uploadfiles" ||
                                              item["element"] ===
                                                "datepicker" ||
                                              item["element"] === "trigered" ? (
                                                <td key={index}>
                                                  <div className="form-group">
                                                    <input
                                                      type="number"
                                                      column={column}
                                                      value={rows[idx][column]}
                                                      index={idx}
                                                      max="225"
                                                      min="1"
                                                      placeholder="Min"
                                                      className="form-control form-control-sm"
                                                      disabled
                                                      onChange={(e) =>
                                                        updateState(e)
                                                      }
                                                    />
                                                  </div>
                                                </td>
                                              ) : (
                                                <td key={index}>
                                                  <div className="form-group">
                                                    <input
                                                      type="number"
                                                      column={column}
                                                      value={rows[idx][column]}
                                                      index={idx}
                                                      placeholder="Min"
                                                      className="form-control form-control-sm"
                                                      onChange={(e) =>
                                                        updateState(e)
                                                      }
                                                    />
                                                  </div>
                                                </td>
                                              )}
                                            </>
                                          ))}
                                          {columnMax.map((column, index) => (
                                            <>
                                              {item["element"] === "select" ||
                                              item["element"] ===
                                                "radiogroup" ||
                                              item["element"] === "checkbox" ||
                                              item["element"] === "file-doc" ||
                                              item["element"] ===
                                                "uploadfiles" ||
                                              item["element"] ===
                                                "datepicker" ||
                                              item["element"] === "trigered" ? (
                                                <td key={index}>
                                                  <div className="form-group">
                                                    <input
                                                      type="number"
                                                      column={column}
                                                      value={rows[idx][column]}
                                                      index={idx}
                                                      placeholder="Max"
                                                      className="form-control form-control-sm"
                                                      onChange={(e) =>
                                                        updateState(e)
                                                      }
                                                      disabled
                                                    />
                                                  </div>
                                                </td>
                                              ) : (
                                                <td key={index}>
                                                  <div className="form-group">
                                                    <input
                                                      type="number"
                                                      column={column}
                                                      value={rows[idx][column]}
                                                      index={idx}
                                                      placeholder="Max"
                                                      className="form-control form-control-sm"
                                                      onChange={(e) =>
                                                        updateState(e)
                                                      }
                                                    />
                                                  </div>
                                                </td>
                                              )}
                                            </>
                                          ))}
                                          <>
                                            {item["element"] === "select" ||
                                            item["element"] === "checkbox" ||
                                            item["element"] === "radiogroup" ||
                                            item["element"] === "trigered" ? (
                                              <>
                                                {columnOption.map(
                                                  (column, index) => (
                                                    <td key={index}>
                                                      <>
                                                        <select
                                                          value={
                                                            rows[idx][column]
                                                          }
                                                          className="form-control form-control-sm selectpicker"
                                                          data-placeholder="Select option"
                                                          index={idx}
                                                          column={column}
                                                          data-allow-clear="true"
                                                          id="pilih_element[]"
                                                          name="pilih_element"
                                                          onChange={(e) =>
                                                            updateState(e)
                                                          }
                                                        >
                                                          <option>
                                                            -- Pilih Option --
                                                          </option>
                                                          <option value="manual">
                                                            Manual
                                                          </option>
                                                          <option value="referensi">
                                                            Select Reference
                                                          </option>
                                                        </select>
                                                      </>
                                                    </td>
                                                  ),
                                                )}
                                                {columnDataOption.map(
                                                  (column, index) => (
                                                    <td key={index}>
                                                      {item["option"] ===
                                                        "manual" ||
                                                      item["option"] === "" ? (
                                                        <>
                                                          {/* <span>{columnElement}</span> */}
                                                          {/* <span>{item['element']}</span> */}

                                                          <input
                                                            type="text"
                                                            column={column}
                                                            value={
                                                              rows[idx][column]
                                                            }
                                                            index={idx}
                                                            id="data_option[]"
                                                            placeholder="Data1;Data2"
                                                            className="form-control form-control-sm"
                                                            onChange={(e) =>
                                                              updateState(e)
                                                            }
                                                          />
                                                        </>
                                                      ) : (
                                                        <>
                                                          <select
                                                            value={
                                                              rows[idx][column]
                                                            }
                                                            className="form-control  form-control-sm selectpicker"
                                                            data-placeholder="Select option"
                                                            index={idx}
                                                            column={column}
                                                            data-allow-clear="true"
                                                            id="data_option[]"
                                                            name="pilih_element"
                                                            onChange={(e) =>
                                                              updateState(e)
                                                            }
                                                          >
                                                            <option value="">
                                                              Pilh Referensi
                                                            </option>
                                                            {ReferenceSize.map(
                                                              (size) => (
                                                                <option
                                                                  value={
                                                                    size.id
                                                                  }
                                                                >
                                                                  {size.name}
                                                                </option>
                                                              ),
                                                            )}
                                                          </select>
                                                        </>
                                                      )}
                                                    </td>
                                                  ),
                                                )}
                                              </>
                                            ) : item["element"] ===
                                              "file-doc" ? (
                                              <>
                                                {columnUpload.map(
                                                  (column, index) => (
                                                    <td colSpan="2">
                                                      {item["element"] ===
                                                      "file-doc" ? (
                                                        <>
                                                          <input
                                                            type="file"
                                                            className="form-control form-control-sm font-size-h4"
                                                            name="data_option"
                                                            index={idx}
                                                            column={column}
                                                            accept="application/pdf"
                                                            id="data_option[]"
                                                            onChange={(e) =>
                                                              onFileChange(e)
                                                            }
                                                          ></input>
                                                        </>
                                                      ) : (
                                                        <>
                                                          <input
                                                            type="file"
                                                            className="form-control form-control-sm font-size-h4"
                                                            name="data_option"
                                                            index={idx}
                                                            column={column}
                                                            accept="image/*"
                                                            id="data_option[]"
                                                            onChange={(e) =>
                                                              onFileChange(e)
                                                            }
                                                          ></input>
                                                        </>
                                                      )}

                                                      <div
                                                        style={{
                                                          color: "#7a7a7a",
                                                        }}
                                                      >
                                                        {" "}
                                                        )* Upload template file
                                                        jika ada{" "}
                                                      </div>
                                                    </td>
                                                  ),
                                                )}
                                              </>
                                            ) : (
                                              <>
                                                {columnOption.map(
                                                  (column, index) => (
                                                    <td
                                                      key={index}
                                                      style={{
                                                        pointerEvents: "none",
                                                      }}
                                                    >
                                                      <>
                                                        <select
                                                          disabled
                                                          value={
                                                            rows[idx][column]
                                                          }
                                                          className="form-control form-control-sm selectpicker"
                                                          data-placeholder="Select option"
                                                          index={idx}
                                                          column={column}
                                                          data-allow-clear="true"
                                                          id="pilih_element[]"
                                                          name="pilih_element"
                                                          onChange={(e) =>
                                                            updateState(e)
                                                          }
                                                        >
                                                          <option>
                                                            -- Pilih Option --
                                                          </option>
                                                          <option value="manual">
                                                            Manual
                                                          </option>
                                                          <option value="referensi">
                                                            Select Reference
                                                          </option>
                                                        </select>
                                                      </>
                                                    </td>
                                                  ),
                                                )}
                                                {columnDataOption.map(
                                                  (column, index) => (
                                                    <td
                                                      key={index}
                                                      style={{
                                                        pointerEvents: "none",
                                                      }}
                                                    >
                                                      {item["option"] ===
                                                        "manual" ||
                                                      item["option"] === "" ? (
                                                        <>
                                                          {/* <span>{columnElement}</span> */}
                                                          {/* <span>{item['element']}</span> */}

                                                          <input
                                                            type="text"
                                                            column={column}
                                                            value={
                                                              rows[idx][column]
                                                            }
                                                            index={idx}
                                                            id="data_option[]"
                                                            placeholder="Data1;Data2"
                                                            className="form-control form-control-sm"
                                                            onChange={(e) =>
                                                              updateState(e)
                                                            }
                                                          />
                                                        </>
                                                      ) : (
                                                        <>
                                                          {/* {isText[idx]} */}
                                                          {/* {isDisabled[idx]} */}
                                                          <select
                                                            value={
                                                              rows[idx][column]
                                                            }
                                                            className="form-control  form-control-sm selectpicker"
                                                            data-placeholder="Select option"
                                                            index={idx}
                                                            column={column}
                                                            data-allow-clear="true"
                                                            id="data_option[]"
                                                            name="pilih_element"
                                                            onChange={(e) =>
                                                              updateState(e)
                                                            }
                                                          >
                                                            <option value="">
                                                              Pilh Referensi
                                                            </option>
                                                            {ReferenceSize.map(
                                                              (size) => (
                                                                <option
                                                                  value={
                                                                    size.id
                                                                  }
                                                                >
                                                                  {size.name}
                                                                </option>
                                                              ),
                                                            )}
                                                          </select>
                                                        </>
                                                      )}
                                                    </td>
                                                  ),
                                                )}
                                              </>
                                            )}
                                          </>
                                          {columnSpan.map((column, index) => (
                                            <td key={index}>
                                              <input
                                                type="text"
                                                column={column}
                                                value={rows[idx][column]}
                                                index={idx}
                                                id="data_option[]"
                                                placeholder="Caption"
                                                className="form-control form-control-sm"
                                                onChange={(e) => updateState(e)}
                                              />
                                              <span></span>
                                            </td>
                                          ))}
                                          {/* {columnReq.map((column, index) => (
                                          <td key={index}>
                                              <input type="checkbox" 
                                                value={rows[idx][column]}  index={idx}
                                              column={column} name="Checkboxes" onChange={(e) => updateState(e)}/>
                                              <span></span>
                                              
                                          </td>
                                          ))} */}

                                          {item["element"] === "trigered" ? (
                                            columnTriger.map(
                                              (column, index) => (
                                                <td key={index}>
                                                  <input
                                                    type="checkbox"
                                                    index={idx}
                                                    value={rows[idx][column]}
                                                    checked={rows[idx][column]}
                                                    className={
                                                      "form-check-input align-self-center"
                                                    }
                                                    column={column}
                                                    name="Checkboxes"
                                                    onChange={(e) =>
                                                      checkTriger(idx, e)
                                                    }
                                                  />
                                                  <span></span>
                                                </td>
                                              ),
                                            )
                                          ) : (
                                            <> </>
                                          )}

                                          {columnIdParent.map(
                                            (column, index) => (
                                              <td key={index}>
                                                <input
                                                  type="hidden"
                                                  column={column}
                                                  value={rows[idx][column]}
                                                  index={idx}
                                                  id="parent[]"
                                                  placeholder="Span"
                                                  className="form-control form-control-sm"
                                                  onChange={(e) =>
                                                    updateState(e)
                                                  }
                                                />
                                                <span></span>
                                              </td>
                                            ),
                                          )}
                                          {columnKeyParent.map(
                                            (column, index) => (
                                              <td key={index}>
                                                <input
                                                  type="hidden"
                                                  column={column}
                                                  value={rows[idx][column]}
                                                  index={idx}
                                                  id="parent[]"
                                                  placeholder="Caption"
                                                  className="form-control form-control-sm"
                                                  onChange={(e) =>
                                                    updateState(e)
                                                  }
                                                />
                                                <span></span>
                                              </td>
                                            ),
                                          )}
                                          <td>
                                            <button
                                              className="btn btn-icon btn-danger btn-sm me-1"
                                              onClick={() =>
                                                handleRemoveSpecificRow(idx)
                                              }
                                            >
                                              <i className="fas fa-trash fs-5">
                                                {item["triger"]}
                                              </i>
                                            </button>
                                          </td>
                                        </tr>
                                      </>
                                    ),
                                  )}
                                </>
                              ))}
                            </tbody>
                          </table>
                        </div>
                        <div className="col-lg-12 text-center mb-5">
                          <a
                            onClick={handleAddRow}
                            className="btn btn-light text-success btn-sm d-block fw-semibold me-3 mr-2"
                          >
                            <i className="bi bi-plus-circle text-success me-1"></i>{" "}
                            Tambah Element
                          </a>
                        </div>

                        <div className="form-group fv-row pt-7 mb-7">
                          <div className="d-flex justify-content-center mb-7">
                            <button
                              onClick={back}
                              type="reset"
                              className="btn btn-md btn-light me-3"
                              data-kt-menu-dismiss="true"
                            >
                              Batal
                            </button>
                            <button
                              onClick={postResults_Save}
                              className="btn btn-primary btn-text-light btn-hover-text-success font-weight-bold"
                              disabled={isLoading}
                            >
                              {isLoading ? (
                                <>
                                  <span
                                    className="spinner-border spinner-border-sm me-2"
                                    role="status"
                                    aria-hidden="true"
                                  ></span>
                                  <span className="sr-only">Loading...</span>
                                  Loading...
                                </>
                              ) : (
                                <>
                                  <i className="fa fa-paper-plane me-1"></i>
                                  Simpan
                                </>
                              )}
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </div>
  );
};

export default PendaftaranAdd;
