import React, { useState, useEffect } from "react";
import Header from "../Header";
import SideNav from "../SideNav";
import Footer from "../Footer";
import Swal from "sweetalert2";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";
import withReactContent from "sweetalert2-react-content";
import { ReactDOM } from "react-dom";
import Modal from "react-modal";
import Cookies from "js-cookie";
import {
  renderToStaticMarkup,
  renderToNodeStream,
  renderToString,
  renderToReadableStream,
  renderToStaticNodeStream,
} from "react-dom/server";
import { loadTrigred_rekursive } from "../pelatihan/FormHelper";
import { cekPermition } from "../AksesHelper";

// import { useForm } from "react-hook-form";
const PendaftaranPreview = () => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  const [uploadFiles, setUploadFiles] = useState(true);
  const [files, setFiles] = useState([]);
  // let history = useNavigate();

  const groupBy = (array, key) => {
    return array.reduce((result, currentValue) => {
      (result[currentValue[key]] = result[currentValue[key]] || []).push(
        currentValue,
      );
      return result;
    }, {});
  };
  const defrow = [
    {
      name: "",
      element: "",
      size: "",
      option: "",
      data_option: "",
      req: "",
      min: "",
      max: "",
      sts_false: true,
      triger: false,
      span: "",
      upload: "",
      id_parent: "",
      key_parent: "",
      join_parent: "",
    },
  ];
  // const [rows, setRows] = useState([{}]);
  const [rows, setRows] = useState([]);
  const history = useNavigate();
  const columnsArray = [
    "Nama Field",
    "Pilih Element",
    "Min",
    "Max Length",
    "Option",
    "Data Option",
    "Caption",
    "Triger",
    "",
  ];
  const initialPendaftaranState = {
    id: null,
    judul: "",
  };
  const MySwal = withReactContent(Swal);
  const columnsName = ["name"];
  const columnElement = ["element"];
  const columnSize = ["size"];
  const columnOption = ["option"];
  const columnDataOption = ["data_option"];
  const columnReq = ["req"];

  const columnMin = ["min"];
  const columnMax = ["max"];
  const columnTriger = ["triger"];
  const columnSpan = ["span"];
  const columnUpload = ["upload"];
  const columnIdParent = ["parent"];
  const columnKeyParent = ["key_parent"];
  const columnJoinParent = ["join_parent"];
  const [RepoSize, setRepoSize] = useState();
  const [ReferenceSize, setReference] = useState();
  const [Pendaftaran, setPendaftaran] = useState(initialPendaftaranState);
  // const [isDisabled, setDisabled] = useState([]);
  const [isDisabled, setDisabled] = useState([]);
  const [isText, setText] = useState([{}]);
  const [isUpload, setUpload] = useState([]);
  const [modalIsOpen, setIsOpen] = React.useState(false);

  const customStyles = {
    content: {
      top: "55%",
      left: "60%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
      width: "70%",
      height: "88%",
    },
  };

  let subtitle;

  const { id } = useParams();

  const isRequired = (value) => {
    return value != null && value.trim().length > 0;
  };
  useEffect(() => {
    retriveRepository_filter();
  }, []);
  const handleAddRow = () => {
    console.log(rows);
    // rows.map((number) =>
    //   console.log(number.name)
    // )
    const item = {
      name: "",
      element: "",
      size: "",
      option: "",
      data_option: "",
      req: "",

      min: "",
      max: "",
      sts_false: true,
      triger: "",
      span: "",
      upload: [],
      parent: "",
      key_parent: "",
      html: "",
      join_parent: "",
    };
    setRows([...rows, item]);
  };
  const handleInputChange = (event) => {
    console.log(event.target);
    const { name, value } = event.target;
    setPendaftaran({ ...Pendaftaran, [name]: value });
  };
  const onFileChange = (e) => {
    let prope = e.target.attributes.column.value; //upload
    let index = e.target.attributes.index.value; //0
    let fieldValue = e.target.files[0];
    const tempRows = [...rows];
    const tempObj = rows[index];
    tempObj[prope] = fieldValue;
    tempRows[index] = tempObj;
    setRows(tempRows);
  };
  const handleChange_Repo = (e) => {
    let index = e.nativeEvent.target.selectedIndex;
    let label = e.nativeEvent.target[index].text;
    // console.log(label);
    const { name, value } = e.target;
    setPendaftaran({ ...Pendaftaran, [name]: value, judul_nama: label });
  };
  const postResults = () => {
    console.log(rows);
  };

  const checkTriger = (idx, e) => {
    console.log(rows);
    console.log(e.target.checked);
    let prope = e.target.attributes.column.value;
    let index_up = e.target.attributes.index.value;
    // console.log(index_);
    let fieldValue = e.target.checked;
    const tempRows = [...rows];
    const tempObj = rows[idx];
    tempObj[prope] = fieldValue;
    tempRows[idx] = tempObj;
    setRows(tempRows);
    if (e.target.checked === true) {
      let furo = [];
      let item = [...rows];
      rows[idx].data_option.split(";").map((number) =>
        item.push({
          name: "",
          element: "",
          size: "",
          option: "",
          data_option: "",
          req: "",
          min: "",
          max: "",
          sts_false: true,
          triger: "",
          html: "",
          span: "",
          upload: [],
          parent: number,
          key_parent: rows[idx].name,
          join_parent: rows[idx].name + " : " + number,
        }),
      );
      setRows(item);
    } else {
      var index = rows.indexOf(rows[idx].parent);
      if (index > -1) {
        //Make sure item is present in the array, without if condition, -n indexes will be considered from the end of the array.
        rows.splice(index, 1);
      }
      // handleRemoveSpecificRow(idx);
    }
  };
  const handleAddRowTriger = (idx, e) => {
    // if(e.target.checked === true){
    let furo = [];
    let item = [...rows];
    // rows[idx].data_option.split(';').map((number) =>
    item.push({
      name: "",
      element: "",
      size: "",
      option: "",
      data_option: "",
      req: "",

      min: "",
      max: "",
      sts_false: true,
      triger: "",
      html: "",
      span: "",
      upload: [],
      parent: rows[idx].parent,
      key_parent: rows[idx].key_parent,
      join_parent: rows[idx].join_parent,
    });
    // )

    setRows(item.sort((a, b) => (a.join_parent > b.join_parent ? 1 : -1)));
    console.log(rows);
    console.log(groupArrayOfObjects(rows, "join_parent"));
  };

  function groupArrayOfObjects(list, key) {
    return list.reduce(function (rv, x) {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  }
  const updateState = (e) => {
    let prope = e.target.attributes.column.value;
    let index = e.target.attributes.index.value;
    console.log(prope);
    let fieldValue = e.target.value;
    const tempRows = [...rows];
    const tempObj = rows[index];
    tempObj[prope] = fieldValue;
    tempRows[index] = tempObj;
    const tempObjx = columnOption[index];
    let checkCol = [prope] + [index];
    let opt = "option";
    if (e.target.attributes.index.value === index) {
      if (prope === "element") {
        tempRows[index]["min"] = "";
        tempRows[index]["max"] = "";
        tempRows[index]["option"] = "";
        tempRows[index]["data_option"] = "";
        tempRows[index]["span"] = "";
        if (
          tempObj[prope] === "select" ||
          tempObj[prope] === "checkbox" ||
          tempObj[prope] === "radiogroup"
        ) {
          setDisabled({ isDisabled, [index]: false });
        } else {
          setDisabled({ isDisabled, [index]: true });
        }
      }
      setRows(tempRows);
      if (prope === "option") {
        if (tempObj[prope] === "manual") {
          setText([[index], { index: "1" }]);
        } else if (tempObj[prope] === "referensi") {
          setText([[index], { [index]: "2" }]);
        } else {
          setText([[index], { index: "3" }]);
        }
        console.log(isText);
      }
    }
  };

  const handleRemoveSpecificRow = (idx) => {
    const tempRows = [...rows];
    tempRows.splice(idx, 1);
    setRows(tempRows);
  };

  const retriveRepository_filter = () => {
    axios
      .post(process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/cari-repo", {
        id: id,
      })
      .then(function (response) {
        console.log(response);
        if (response.status === 200) {
          let repo = response.data.result;

          if (repo.Status === true) {
            setRepoSize(repo.Data);
            console.log(RepoSize);

            let item = [...rows];

            repo.Data.map((ress, index) =>
              item.push({
                name: ress.name,
                element: ress.element,
                size: ress.size,
                option: ress.option,
                data_option: ress.data_option,
                req: ress.req,
                placeholder: ress.placeholder,
                min: ress.min,
                max: ress.max,
                sts_false: true,
                triger: ress.triggered,
                triggered: ress.triggered,
                triggered_parent: ress.triggered_parent,
                triggered_name: ress.triggered_name,
                key_triggered_name: ress.key_triggered_name,

                span: ress.span,
                html: ress.html,
                file_name: ress.file_name,
                ref_values: ress.ref_values,
                upload: [],
                parent: "",
                key_parent: "",
                child: ress.child === undefined ? [] : ress.child,
                join_parent: "",
                ref_values: ress.ref_values,
                ref_values: ress.ref_values,
                kategori: ress.kategori,
              }),
            );

            setRows(
              item.sort((a, b) => (a.join_parent > b.join_parent ? 1 : -1)),
            );
            console.log(rows);
            console.log(groupArrayOfObjects(rows, "join_parent"));

            console.log(item);
          } else {
          }
        }
      })
      .catch(function (error) {});
  };

  function outOfBox(props) {
    if (props === "referensi") {
      return (
        <select
          className="form-control-sm form-solid"
          data-kt-select2="true"
          data-placeholder="Select option"
          data-dropdown-parent="#kt_menu_61484c5a8da38"
          data-allow-clear="true"
        >
          {/* <option /> */}
          <option value="1">Publish</option>
          <option value="0">Unpublish</option>
        </select>
      );
    } else {
    }
  }

  function loadTrigered(idEle, valEle, parent) {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/cari-repo-triger",
        {
          triggered_name: parent,
          key_triggered_name: valEle,
        },
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + Cookies.get("token"),
          },
        },
      )
      .then(function (response) {
        console.log(response);
        if (response.status === 200) {
          console.log(response);
          if (response.data.result.Status === true) {
            let ress = response.data.result.Data[0];

            if (ress.element == "trigered") {
              let element_html = (
                <div>
                  <div className="form-group mb-5">
                    <span className="badge badge-light-primay fs-7 m-1">
                      {" "}
                      {ress.kategori}{" "}
                    </span>
                    <div
                      className="row align-items-end aos-init aos-animate"
                      data-aos="fade-up"
                    >
                      <div className={"mb-4 mb-md-0 "}>
                        <label className={ress.className}> {ress.name}</label>
                      </div>
                    </div>
                    <div>
                      <select
                        className="form-control form-control-sm form-select-sm selectpicker"
                        data-control="select2"
                        data-placeholder="Select an option"
                        name={ress.file_name}
                      >
                        <option value=""> -- Pilih -- </option>
                        {ress.data_option
                          .split(";")
                          .map((row_opsi, idx_opsi) => (
                            <option value={row_opsi}>{row_opsi}</option>
                          ))}
                      </select>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-lg-12">
                      <span className="form-caption font-size-sm text-italic my-2 text-muted">
                        {" "}
                        {ress.caption}{" "}
                      </span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-1"> </div>
                    <div className="col-lg-10" id={ress.file_name}></div>
                  </div>
                </div>
              );

              console.log(renderToString(element_html));
              document.getElementById(idEle).innerHTML =
                renderToString(element_html);
            } else {
              document.getElementById(idEle).innerHTML = ress.html;
            }
          }
        }
      })
      .catch(function (error) {
        console.log(error);
      });

    if (valEle == "") {
      document.getElementById(idEle).innerHTML = "";
    }
  }

  function back() {
    window.location = "/pelatihan/element";
  }

  return (
    <div>
      <Header />
      <SideNav />
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Form Element
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={back}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>

                {cekPermition().manage === 1 ? (
                  <>
                    <a
                      href="/pelatihan/element/add/old"
                      className="btn btn-success fw-bolder btn-sm"
                    >
                      <i className="bi bi-plus-circle"></i>
                      <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                        Tambah Element
                      </span>
                    </a>
                  </>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Preview Form Element
                        </h1>
                      </div>
                      <div className="card-toolbar"></div>
                    </div>
                    <div className="card-body">
                      {rows.map((item, idx) => (
                        <div>
                          <span className="badge badge-light-primary fs-7 m-1">
                            {" "}
                            {item.kategori}{" "}
                          </span>
                          {loadTrigred_rekursive(item)}
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </div>
  );
};

export default PendaftaranPreview;
