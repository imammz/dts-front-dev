import React, { useState, useEffect, useMemo, useRef } from "react";
import Pagination from "@material-ui/lab/Pagination";
import PendaftaranService from "../../service/PendaftaranService";
import { useTable } from "react-table";
import { GlobalFilter, DefaultFilterForColumn } from "../Filter";
import Header from "../../components/Header";
import SideNav from "../../components/SideNav";
import Footer from "../../components/Footer";
import axios from "axios";
import { useHistory, useNavigate, useParams } from "react-router-dom";
import {
  useFilters,
  useGlobalFilter,
} from "react-table/dist/react-table.development";
// import Parser from 'html-react-parser';
import { loadTrigred_rekursive } from "../pelatihan/FormHelper";
import Cookies from "js-cookie";
import Swal from "sweetalert2";
import { cekPermition } from "../AksesHelper";

const PreviewPendaftaranForm = (props) => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];

  const [Pendaftaran, setPendaftaran] = useState([]);
  const [HeaderForm, setHeaderForm] = useState([]);
  // const PendaftaranRef = useRef();
  // PendaftaranRef.current = Pendaftaran;
  const history = useNavigate();
  const { id } = useParams();
  useEffect(() => {
    retrievePendaftaran();
  }, []);
  const retrievePendaftaran = () => {
    axios
      .post(process.env.REACT_APP_BASE_API_URI + "/cari-formbuilder", {
        id: id,
      })
      .then(function (response) {
        console.log(response);
        if (response.status === 200) {
          console.log(response);
          if (response.data.result.Status === true) {
            setPendaftaran(response.data.result.detail);
            setHeaderForm(response.data.result.utama[0]);
          } else {
            setPendaftaran([]);
          }
        } else {
          setPendaftaran();
          //   setCount();
        }
      })
      .catch(function (error) {
        setPendaftaran();
        //   setCount();
        console.log(error);
      });
  };
  const refreshList = () => {
    retrievePendaftaran();
  };
  const removeAllPendaftaran = () => {
    PendaftaranService.removeAll()
      .then((response) => {
        console.log(response);
        refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const handleShow = () => {
    // console.log(cell?.row?.original.id);
    // return false;

    history("/pelatihan/pendaftaran/edit/" + id);
  };

  const handleShowDel = () => {
    // console.log(cell?.row?.original.id);
    // return false;

    history("/pelatihan/pendaftaran/edit/" + id);
  };

  const handleShowCopy = (cell) => {
    history("/pendaftaran/copy/" + id);
  };
  const handleShowPreview = (cell) => {
    history("/pelatihan/pendaftaran/akademi/edit/" + id);
  };

  const handleShowDelete = () => {
    Swal.fire({
      title: "Apakah anda yakin ?",
      text: "Akan menghapus Form " + HeaderForm.judul_form + " ? ",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, hapus!",
      cancelButtonText: "Tidak",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/daftarpeserta/hapus-formbuilder",
            {
              id: id,
            },
          )
          .then(function (response) {
            console.log(response);
            if (response.status === 200) {
              Swal.fire({
                title: "<strong>Berhasil dihapus</strong>",
                icon: "success",
              });
              back();
            }
          });
      }
    });
  };

  function loadTrigered(idEle, valEle, parent) {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/cari-repo-triger",
        {
          triggered_name: parent,
          key_triggered_name: valEle,
        },
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + Cookies.get("token"),
          },
        },
      )
      .then(function (response) {
        console.log(response);
        if (response.status === 200) {
          console.log(response);
          if (response.data.result.Status === true) {
            let ress = response.data.result.Data[0];

            document.getElementById(idEle).innerHTML = ress.html;
            console.log(idEle);
            console.log(ress.html);
          }
        }
      })
      .catch(function (error) {
        console.log(error);
      });

    if (valEle == "") {
      document.getElementById(idEle).innerHTML = "";
    }
  }

  function back() {
    window.location = "/pelatihan/pendaftaran";
  }

  //   c _renderPreview() {
  //     if (this._text) {
  //       let htmlText = () => ({ __html: this._text.value });
  //       return (
  //         <div>
  //           <div className="preview-box">
  //             <span className="preview-title">Preview</span>
  //             <div dangerouslySetInnerHTML={htmlText()}></div>
  //           </div>
  //           <button onClick={this._handleEditClick}>
  //             Edit
  //           </button>
  //         </div>
  //       );
  //     }
  //     return null;
  //   }
  let rowCounter = 1;
  return (
    <div>
      <Header />
      <SideNav />
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Master Form Pendaftaran
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={back}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>

                {HeaderForm.sdh_digunakan == 1 ? (
                  <></>
                ) : (
                  <>
                    <a
                      onClick={() => handleShow()}
                      className="btn btn-sm btn-warning btn-active-light-info me-2"
                    >
                      <i className="bi bi-gear-fill text-white"></i>
                      <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                        Edit
                      </span>
                    </a>
                    <a
                      onClick={() => handleShowDelete()}
                      className="btn btn-sm btn-danger btn-active-light-info me-2"
                    >
                      <i className="bi bi-trash text-white"></i>
                      <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                        Hapus
                      </span>
                    </a>
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Detail {HeaderForm.judul_form}
                          </h1>
                        </div>
                      </div>
                      <div className="card-body">
                        {Pendaftaran != undefined ? (
                          Pendaftaran.map((column, index) => (
                            <div style={{ paddingBottom: "5px" }}>
                              <span className="badge badge-light-primary">
                                {" "}
                                {column.kategori}{" "}
                              </span>
                              {loadTrigred_rekursive(column)}
                            </div>
                          ))
                        ) : (
                          <span></span>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </div>
  );
};

export default PreviewPendaftaranForm;
