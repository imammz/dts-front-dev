import React, { useState, useEffect, useMemo, useRef } from "react";
import Pagination from "@material-ui/lab/Pagination";
import PendaftaranService from "../../service/PendaftaranService";
import { useTable, useSortBy } from "react-table";
import { GlobalFilter, DefaultFilterForColumn } from "../Filter";
import Select from "react-select";
import Header from "../../components/Header";
import SideNav from "../../components/SideNav";
import Footer from "../../components/Footer";
import axios from "axios";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import DataTable from "react-data-table-component";
import { customLoader } from "../publikasi/helper";
import Cookies from "js-cookie";

import {
  indonesianTimeFormat,
  indonesianDateFormat,
  capitalizeFirstLetter,
  timeDifference,
  dateDifference,
  indonesianDateFormatWithoutTime,
} from "../publikasi/helper";

import { useHistory, useNavigate, useParams } from "react-router-dom";
import {
  useFilters,
  useGlobalFilter,
} from "react-table/dist/react-table.development";
import { left } from "@popperjs/core";
import { cekPermition, logout } from "../AksesHelper";
import { capitalWord } from "../pelatihan/Pelatihan/helper";

const PendaftaranListAkademi = (props) => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];

  const [Pendaftaran, setPendaftaran] = useState([]);
  const [searchTitle, setSearchTitle] = useState("");
  const [getJudulForm, setJudulForm] = useState("");
  const PendaftaranRef = useRef();
  PendaftaranRef.current = Pendaftaran;
  const [page, setPage] = useState(0);
  const [pageq, setPageq] = useState(1);
  const [count, setCount] = useState(0);
  const [pageSize, setPageSize] = useState(10);
  const [loader, setLoader] = useState();
  const pageSizes = [10];
  const [fRep, setfRep] = useState();
  const [status, setStatus] = useState(null);
  const [search, setSearch] = useState("");
  const [sortBy, setSortBy] = useState("id");
  const [sortVal, setSortVal] = useState("desc");
  const numberrow = useRef();

  const configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  const cariJudul = useRef();

  const initialVal = [
    {
      label: "",
      value: "",
    },
  ];
  const [optionList, setOptioList] = useState(initialVal);

  let [Num, setNum] = useState(1);

  var startNum = 1;

  const MySwal = withReactContent(Swal);

  const history = useNavigate();

  useEffect(() => {
    if (cekPermition().view !== 1) {
      Swal.fire({
        title: "Akses Tidak Diizinkan",
        html: "<i> Anda akan kembali kehalaman login </i>",
        icon: "warning",
      }).then(() => {
        logout();
      });
    }
  }, []);

  useEffect(() => {
    retrievePendaftaran();
    retriveOption();
    numberrow.current = 1;

    cariJudul.current = "";
  }, [page, pageSize, sortBy, sortVal, search, status]);
  const getRequestParams = (searchTitle, page, pageSize) => {
    let params = {};

    if (searchTitle) {
      params["title"] = searchTitle;
    }

    if (page) {
      params["page"] = page - 1;
    }

    if (pageSize) {
      params["size"] = pageSize;
    }

    return params;
  };
  const onChangeSearchTitle = (e) => {
    console.log(e);
    const searchTitle = e.target.value;
    setSearchTitle(searchTitle);
  };
  const customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
        width: "90%",
      },
    },
  };
  const retrievePendaftaran = (judul_awal = cariJudul.current) => {
    setLoader(true);

    //alert(cariJudul.current);
    let start_tmp = 0;
    let length_tmp = pageSize != undefined ? pageSize : 10;

    if (page != 0 && page != undefined) {
      start_tmp = length_tmp * page;
      start_tmp = start_tmp - pageSize;
    }

    const params = getRequestParams(searchTitle, page, pageSize);

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/list-formbuilder",
        {
          start: start_tmp,
          length: length_tmp,
          status: status,
          search: search,
          sort_by: sortBy,
          sort_val: sortVal,
        },
        configs,
      )
      .then(function (response) {
        if (page != 0 && page != undefined) {
          numberrow.current = start_tmp + 1;
        } else {
          numberrow.current = 1;
        }

        cariJudul.current = getJudulForm;

        if (response.status === 200) {
          if (response.data.result.Status === true) {
            setPendaftaran(response.data.result.Data);
            setCount(response.data.result.Total);
            setLoader(false);
          } else {
            setPendaftaran([]);
            setCount([]);
            setLoader(false);
            MySwal.fire({
              title:
                search == null || search == "" ? (
                  <strong> Tidak Ada Data </strong>
                ) : (
                  <strong> Tidak Ditemukan </strong>
                ),
              html:
                "<span> " +
                search +
                " tidak ditemukan pada form repositori </span>",
              icon: "warning",
            });
          }
        }
        setLoader(false);
      })
      .catch(function (error) {
        setLoader(false);
      });
  };
  const refreshList = () => {
    setJudulForm("");
    cariJudul.current = "";
    retrievePendaftaran("");
  };

  const searchPendaftaran = (judul_form) => {
    setLoader(true);

    cariJudul.current = judul_form;

    setPage(1);
    setPageSize(pageSize);

    let start_tmp = 0;
    let length_tmp = pageSize != undefined ? pageSize : 10;

    if (page != 0 && page != 1 && page != undefined) {
      start_tmp = length_tmp * page;
      start_tmp = start_tmp - pageSize;
    }
    const params = getRequestParams(searchTitle, page, pageSize);
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/list-formbuilder",
        {
          start: start_tmp,
          length: length_tmp,
          status: status,
          search: search,
          sort_by: sortBy,
          sort_val: sortVal,
        },
        configs,
      )
      .then(function (response) {
        setPageq(page);

        if (page != 0 && page != 1 && page != undefined) {
          start_tmp = length_tmp * page;
          start_tmp = start_tmp - pageSize;
          numberrow.current = start_tmp + 1;
        } else {
          numberrow.current = 1;
        }

        //alert(response.data.result.TotalLength);

        if (response.status === 200) {
          if (response.data.result.Status === true) {
            setPendaftaran(response.data.result.Data);
            setCount(response.data.result.Total);
            setLoader(false);

            numberrow.current = 1;
          } else {
            setPendaftaran([]);
            setCount([]);
            setPageq(1);
            setLoader(false);

            MySwal.fire({
              title: <strong> Tidak Ditemukan </strong>,
              html:
                "<span> " +
                judul_form +
                " tidak ditemukan pada form repositori </span>",
              icon: "warning",
            });
          }
        } else {
          setPendaftaran();
          setCount();
          setPageq(1);
          setLoader(false);
        }
      })
      .catch(function (error) {
        setPendaftaran();
        setCount();
        setPageq(1);
        setLoader(false);
      });
  };

  const removeAllPendaftaran = () => {
    PendaftaranService.removeAll()
      .then((response) => {
        console.log(response);
        refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const deletePendaftaran = (rowIndex) => {
    const id = PendaftaranRef.current[rowIndex].id;
    PendaftaranService.remove(id)
      .then((response) => {
        window.location.href = "";
        let newPendaftaran = [...PendaftaranRef.current];
        newPendaftaran.splice(rowIndex, 1);
        setPendaftaran(newPendaftaran);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const handlePageChange = (page) => {
    setPage(page);
    setPageSize(pageSize);

    // setLoader(true);
    // refreshList();
    //alert(getJudulForm);
    // handleReload(page, pageSize,getJudulForm);
  };
  const handleReload = async (
    page,
    newPerPage,
    judul_search = cariJudul.current,
  ) => {
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;

    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - pageSize;
      numberrow.current = start_tmp + 1;
      //  start_tmp = start_tmp + 1;
    } else {
      numberrow.current = 1;
    }

    //  alert(getJudulForm);

    console.log("newPerPage :" + length_tmp);
    console.log("page :" + start_tmp);
    const respo = await axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/list-formbuilder",
        {
          start: start_tmp,
          length: length_tmp,
          status: status,
          search: search,
          sort_by: sortBy,
          sort_val: sortVal,
        },
        configs,
      )
      .then(function (response) {
        //  const { tutorials, totalPages } = response.data.result.Data;
        setPendaftaran(response.data.result.Data);
        setCount(response.data.result.Total);
        //   setCount(response.data.result.Total);
        setLoader(false);
        console.log(response);

        setPage(page);
        setPageSize(newPerPage);
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  const handlePerRowsChange = async (newPerPage, page) => {
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;

    if (page != 0 && page != undefined) {
      start_tmp = length_tmp * page;
      start_tmp = start_tmp - pageSize;
      numberrow.current = start_tmp + 1;
      //  start_tmp = start_tmp + 1;
    } else {
      numberrow.current = 1;
    }

    //  alert(numberrow.current);

    // setLoader(true);
    setPage(page);
    setPageSize(newPerPage);

    setPageq(1);

    //  handleReload(page, newPerPage,getJudulForm);
  };
  const handleShow = (cell) => {
    window.location = "/pelatihan/pendaftaran/edit/" + cell.id;
  };
  const handleShowCopy = (cell) => {
    window.location = "/pendaftaran/copy/" + cell.id;
  };
  const handleShowPreview = (cell) => {
    window.location = "/pendaftaran/preview/" + cell.id;
  };
  const handleShowDelete = (cell) => {
    console.log(cell);

    MySwal.fire({
      title: "Apakah anda yakin ?",
      text: "Akan menghapus Form " + cell.judul_form + " ? ",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, hapus!",
      cancelButtonText: "Tidak",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/daftarpeserta/hapus-formbuilder",
            {
              id: cell.id,
            },
            configs,
          )
          .then(function (response) {
            console.log(response);
            if (response.status === 200) {
              MySwal.fire({
                title: <strong>Berhasil dihapus</strong>,
                html: "<i>" + cell.judul_form + " berhasil dihapus </i>",
                icon: "success",
              });
              refreshList();
            }
          });
      }
    });
  };

  const handleSort = async (column, sortDirection, newPerPage) => {
    let col = "id";
    if (column.name == "Judul Form") {
      col = "judul_form";
    } else if (column.name == "Tgl Pembuatan") {
      col = "created_at";
    } else if (column.name == "Jml Pelatihan") {
      col = "jml_pelatihan";
    } else if (column.name == "Status Publish") {
      col = "status";
    }
    setSortBy(col);
    setSortVal(sortDirection);
  };

  const retriveOption = async () => {};
  const retrieveFind = async () => {
    const responsi = await axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/cari-formbuilder",
        {
          id: fRep,
        },
        configs,
      )
      .then(function (response) {
        if (response.status === 200) {
          console.log(response);
          if (response.data.result.Status === true) {
            setPendaftaran(response.data.result.utama);
            setCount(1);
            retriveOption();
          } else {
            refreshList();
            retriveOption();
            // setCount([]);
          }
        } else {
          refreshList();
          retriveOption();
          // setCount([]);
        }
      })
      .catch((error) => {
        refreshList();
        retriveOption();
        // setCount([]);
      });
  };

  const handleFilter = (value) => {
    setOptioList({ value: value });
    console.log(value.value);
    setfRep(value.value);
  };
  const handleSearch = () => {
    console.log(fRep);
    retrieveFind();
  };

  const cariForm = (e) => {
    const searchText = e.currentTarget.value;

    if (e.key === "Enter") {
      if (searchText == "") {
        //        setPage(1);
        //        setPageSize(pageSize);
        setJudulForm("");
        setSearch("");
        cariJudul.current = "";
      } else {
        //     setPage(1);
        //     setPageSize(pageSize);
        setJudulForm(searchText);
        cariJudul.current = searchText;
        setSearch(searchText);

        //    alert(searchText);
      }
    }
  };

  const handleChangeSearchAction = (e) => {
    const searchText = e.currentTarget.value;

    if (searchText == "") {
      setJudulForm("");
      cariJudul.current = "";
      setPage(1);
      setPageSize(pageSize);
      // refreshList();
      setSearch("");
    }
  };

  const handlePageSizeChange = (event) => {
    setPageSize(event.target.value);
    setPage(0);
  };

  const columns = useMemo(
    () => [
      {
        Header: "ID",
        accessor: "id",
        sortable: false,
        sortType: "basic",
        width: "70px",
        center: true,
        name: "No",
        cell: (row, index) => {
          return <span>{numberrow.current++}</span>;
        },
        grow: 1,
        selector: (Pendaftaran) => Pendaftaran.id,
      },
      {
        Header: "Judul form",
        accessor: "judul_form",
        className:
          "text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0",
        sortType: "basic",
        name: "Judul Form",
        sortable: true,
        width: "250px",
        grow: 2,
        cell: (Pendaftaran) => {
          return (
            <div className="my-2">
              <a
                onClick={() => handleShowPreview(Pendaftaran)}
                title="Detail"
                className="text-dark"
              >
                <span
                  className="fw-bolder fs-7"
                  style={{
                    overflow: "hidden",
                    whiteSpace: "wrap",
                    textOverflow: "ellipses",
                  }}
                >
                  {Pendaftaran.judul_form}
                </span>
              </a>
            </div>
          );
        },
        selector: (Pendaftaran) => Pendaftaran.judul_form,
      },
      {
        Header: "Tanggal",
        accessor: "created_at",
        className:
          "text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0",
        sortType: "basic",
        name: "Tgl Pembuatan",
        sortable: true,
        grow: 2,
        width: "150px",
        cell: (Pendaftaran) => {
          return (
            <div style={{ textAlign: "center", maxWidth: "10px!important" }}>
              <center>
                <span className="fs-7">
                  {indonesianDateFormatWithoutTime(Pendaftaran.created_at)}
                </span>
              </center>
            </div>
          );
        },
        selector: (Pendaftaran) => Pendaftaran.created_at,
      },
      {
        Header: "Jumlah Pelatihan",
        accessor: "jml_pelatihan",
        className: "text-start",
        sortType: "basic",
        name: "Jml Pelatihan",
        sortable: true,
        center: true,
        grow: 2,
        cell: (Pendaftaran) => {
          return (
            <div style={{ maxWidth: "20px!important" }}>
              <center>
                <span className="badge badge-sm badge-light-primary fs-7">
                  {" "}
                  {Pendaftaran.jml_pelatihan == null
                    ? 0
                    : Pendaftaran.jml_pelatihan}{" "}
                </span>
              </center>
            </div>
          );
        },
        selector: (Pendaftaran) => Pendaftaran.jml_pelatihan,
      },

      {
        Header: "Status",
        accessor: "status",
        sortType: "basic",
        name: "Status Publish",
        sortable: true,
        grow: 2,
        cell: (Pendaftaran) => {
          return (
            <div>
              <span
                className={
                  "badge badge-light-" +
                  (Pendaftaran.status == 1
                    ? "success"
                    : Pendaftaran.status == 0
                      ? "danger"
                      : "warning") +
                  " fs-7 m-1"
                }
              >
                {Pendaftaran.status == 1
                  ? "Publish"
                  : Pendaftaran.status == 0
                    ? "Unpublish"
                    : "Unlisted"}
              </span>
            </div>
          );
        },
        selector: (Pendaftaran) => Pendaftaran.status,
      },

      {
        Header: "Aksi",
        accessor: "actions",
        name: "Aksi",
        sortable: false,
        width: "200px",

        cell: (Pendaftaran) => {
          return (
            <div className="row" style={{ width: "100%", paddingLeft: "10px" }}>
              {cekPermition().manage === 1 ? (
                <>
                  <a
                    title="Preview Form Pendaftaran"
                    className="btn btn-icon btn-primary btn-sm col-3"
                    style={{ marginLeft: "3px", marginTop: "3px" }}
                    onClick={() => handleShowPreview(Pendaftaran)}
                  >
                    <i className="bi bi-eye-fill text-white"></i>
                  </a>

                  <a
                    title="Copy Form Pendaftaran"
                    className="btn btn-icon btn-info btn-sm"
                    style={{ marginLeft: "3px", marginTop: "3px" }}
                    onClick={() => handleShowCopy(Pendaftaran)}
                  >
                    <i className="fa fa-copy text-white"></i>
                  </a>
                </>
              ) : (
                <></>
              )}
              {Pendaftaran.jml_pelatihan != 0 &&
              Pendaftaran.jml_pelatihan != null ? (
                <>
                  <a
                    href="#"
                    title="Edit Form Pendaftaran"
                    className="btn btn-icon disabled btn-warning btn-sm"
                    style={{ marginLeft: "3px", marginTop: "3px" }}
                  >
                    <i className="bi bi-gear-fill text-white"></i>
                  </a>
                  <a
                    href="#"
                    title="Hapus Form Pendaftaran"
                    className="btn btn-icon disabled btn-danger btn-sm"
                    style={{ marginLeft: "3px", marginTop: "3px" }}
                  >
                    <i className="bi bi-trash text-white"></i>
                  </a>
                </>
              ) : cekPermition().manage === 1 ? (
                <>
                  {/*<a
                title="Edit Form Pendaftaran"
                className="btn btn-icon btn-warning btn-sm"
                style={{ marginLeft: "3px", marginTop: "3px" }}
                onClick={() => handleShow(Pendaftaran)}
              >
                <i className="bi bi-gear-fill text-white"></i>
              </a>*/}
                  <a
                    title="Edit Form Pendaftaran"
                    className="btn btn-icon btn-warning btn-sm"
                    style={{ marginLeft: "3px", marginTop: "3px" }}
                    onClick={() => handleShow(Pendaftaran)}
                  >
                    <i className="bi bi-gear-fill text-white"></i>
                  </a>
                  <a
                    title="Hapus Form Pendaftaran"
                    className="btn btn-icon btn-danger btn-sm"
                    style={{ marginLeft: "3px", marginTop: "3px" }}
                    onClick={() => handleShowDelete(Pendaftaran)}
                  >
                    <i className="bi bi-trash text-white"></i>
                  </a>
                </>
              ) : (
                <></>
              )}
            </div>
          );
        },
      },
    ],
    [],
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    state,
    prepareRow,
    setGlobalFilter,
    preGlobalFilteredRows,
  } = useTable(
    {
      columns,
      data: Pendaftaran,
      defaultColumn: { Filter: DefaultFilterForColumn },
    },
    useFilters,
    useGlobalFilter,
    useSortBy,
  );
  let rowCounter = 1;
  return (
    <div>
      <Header />
      <SideNav />
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Master Form Pendaftaran
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                {cekPermition().manage === 1 ? (
                  <>
                    <a
                      href={
                        "/" + urlSegmentZero + "/" + urlSegmenttOne + "/add"
                      }
                      className="btn btn-success fw-bolder btn-sm"
                    >
                      <i className="bi bi-plus-circle"></i>
                      <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                        Tambah Form Pendaftaran
                      </span>
                    </a>
                  </>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Daftar Form Pendaftaran
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <div className="d-flex align-items-center position-relative my-1 me-2">
                            <span className="svg-icon svg-icon-1 position-absolute ms-6">
                              <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className="mh-50px"
                              >
                                <rect
                                  opacity="0.5"
                                  x="17.0365"
                                  y="15.1223"
                                  width="8.15546"
                                  height="2"
                                  rx="1"
                                  transform="rotate(45 17.0365 15.1223)"
                                  fill="currentColor"
                                ></rect>
                                <path
                                  d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                  fill="currentColor"
                                ></path>
                              </svg>
                            </span>
                            <input
                              type="text"
                              data-kt-user-table-filter="search"
                              className="form-control form-control-sm form-control-solid w-250px ps-14"
                              placeholder="Cari Form Pendaftaran"
                              onKeyPress={cariForm}
                              onChange={handleChangeSearchAction}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="table-responsive">
                          <DataTable
                            columns={columns}
                            data={Pendaftaran}
                            progressPending={loader}
                            progressComponent={customLoader()}
                            highlightOnHover
                            pointerOnHover
                            pagination
                            paginationServer
                            paginationTotalRows={count}
                            paginationComponentOptions={{
                              selectAllRowsItem: true,
                              selectAllRowsItemText: "Semua",
                            }}
                            onChangeRowsPerPage={handlePerRowsChange}
                            onChangePage={handlePageChange}
                            customStyles={customStyles}
                            persistTableHead={true}
                            onSort={handleSort}
                            // defaultSortAsc={true}
                            noDataComponent={
                              <div className="mt-5">Tidak Ada Data</div>
                            }
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </div>
  );
};

export default PendaftaranListAkademi;
