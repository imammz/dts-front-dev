import React, { useState, useEffect, useRef } from "react";
import Header from "../../components/Header";
import SideNav from "../../components/SideNav";
import Footer from "../../components/Footer";
import axios from "axios";
import Swal from "sweetalert2";
import Board, { moveCard, addCard } from "@asseinfo/react-kanban";
import withReactContent from "sweetalert2-react-content";
import { useParams, useNavigate, withRouter } from "react-router-dom";
import "../Pendaftaran-Content/sytle_kaban_una.css";
import "@asseinfo/react-kanban/dist/styles.css";
import { capitalizeFirstLetter } from "../publikasi/helper";
import { loadTrigred_rekursive } from "../pelatihan/FormHelper";
import Cookies from "js-cookie";
import Select from "react-select";
import swal from "sweetalert2";

const PendaftaranDataLKanban = (props) => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  let dataForm = [];
  let judulForm = [];
  let statusPublish = 0;

  var alertHapus = 0;

  const initialPendaftaranState = {
    id: "0",
    judul: "",
  };

  const judulRef = useRef("FORM_" + Cookies.get("user_email") + "_[ID]");

  const [RepoSize, setRepoSize] = useState([]);
  const [FormElement, setFormElement] = useState([]);
  const [JudulFormPendaftaran, setJudul] = useState("");
  const [StatusFormPendaftaran, setStatus] = useState("");
  const [KategoriForm, setKategoriForm] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const [CekRequired, setCekRequired] = useState(0);

  const [KategoriFormPilih, setKategoriFormPilih] = useState([]);

  var eleRequired = useRef([]);

  const [ReqId, setReqId] = useState([]);

  const MySwal = withReactContent(Swal);

  var checkElement = [];

  function handleCheck(e) {}

  const retriveKategoriForm = () => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/daftarpeserta/list-kategori-form",
      )
      .then(function (response) {
        if (response.status == 200) {
          setKategoriForm(response.data.result.Data);
          console.log(KategoriForm);
        }

        console.log(response.data.result.Data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const retriveRepository = (filter = null) => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/list-repo-v2",
        {
          start: "0",
          length: "100",
          cari: filter == null ? "" : filter,
          sort: "name",
          sort_val: "ASC",
        },
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + Cookies.get("token"),
          },
        },
      )
      .then(function (response) {
        if (response.status === 200) {
          let repo = response.data.result;
          if (repo.Status === true) {
            let columns = [];
            repo.Data.map((number) =>
              columns.push({
                id: number.id,
                title: number.name,
                element: number.element,
                maxlength: number.maxlength,
                placeholder: number.placeholder,
                min: number.min,
                max: number.max,
                required: "",
                size: number.size,
                option: number.option,
                data_option: number.data_option,
                className: number.className,
                html: number.html,
                description: number.name,
                name: number.name,
                file_name: number.file_name,
                child: number.child,
                span: number.span,
                hapus: false,
                kategori: number.kategori,
                ref_values: number.ref_values,
                id_form_kategori: number.id_form_kategori,
              }),
            );
            setRepoSize(columns);
          } else {
            setRepoSize([]);
          }
        }
      })
      .catch((err) => {
        const message = err.response?.data?.result?.Message;
        Swal.fire({
          title: "Perhatian!",
          text: message ?? "Form element tidak ditemukan",
          icon: "warning",
        }).then((result) => {
          if (result.isConfirmed) {
            retriveRepository();
            setKategoriFormPilih(0);
          }
        });
        setRepoSize([]);
      });
  };

  const board = {
    columns: [
      {
        id: 1,
        title: "Daftar Form Element",
        cards: RepoSize,
      },
      {
        id: 2,
        title: "Canvas Form Pendaftaran ",
        cards: FormElement,
      },
    ],
  };

  useEffect(() => {
    retriveRepository();
    retriveKategoriForm();
  }, []);

  function ControlledBoard() {
    // You need to control the state yourself.
    const [controlledBoard, setBoard] = useState(board);

    function handleCardMove(_card, source, destination) {
      const updatedBoard = moveCard(controlledBoard, source, destination);
      setBoard(updatedBoard);
    }

    return (
      <Board onCardDragEnd={handleCardMove} disableColumnDrag>
        {controlledBoard}
      </Board>
    );
  }

  function safeRequired(value, id) {
    if (value) {
      checkElement[id] = "required";
    } else {
      checkElement[id] = "";
    }

    console.log(checkElement);

    let send = [];

    FormElement.map((ress, index) =>
      send.push({
        judul: judulRef.current.value,
        id: ress.id,
        name: ress.name,
        description: ress.description,
        title: ress.title,
        element: ress.element,
        status: StatusFormPendaftaran,
        className: ress.className,
        maxlength: ress.maxlength,
        placeholder: ress.placeholder,
        required:
          id == ress.id ? (value == true ? "required" : "") : ress.required,
        option: ress.option,
        data_option: ress.data_option,
        size: ress.size,
        min: ress.min,
        max: ress.max,
        html: ress.html,
        id_repository: ress.id,
        span: ress.span,
        hapus: true,
        file_name: ress.file_name,
        kategori: ress.kategori,
        ref_values: ress.ref_values,
        id_form_kategori: ress.id_form_kategori,
      }),
    );

    eleRequired.current.value = send;

    console.log(eleRequired.current.value);
  }

  function loadTrigered(idEle, valEle, parent) {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/cari-repo-triger",
        {
          triggered_name: parent,
          key_triggered_name: valEle,
        },
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + Cookies.get("token"),
          },
        },
      )
      .then(function (response) {
        console.log(response);
        if (response.status === 200) {
          console.log(response);
          if (response.data.result.Status === true) {
            let ress = response.data.result.Data[0];

            document.getElementById(idEle).innerHTML = ress.html;
            console.log(idEle);
            console.log(ress.html);
          }
        }
      })
      .catch(function (error) {
        console.log(error);
      });

    if (valEle == "") {
      document.getElementById(idEle).innerHTML = "";
    }
  }

  function UncontrolledBoard() {
    let cek = 0;
    return (
      <Board
        allowRemoveLane
        disableColumnDrag
        allowAddCard
        allowRemoveCard
        onLaneRemove={console.log}
        onCardRemove={(board, column, card) => {
          console.log(card);
          alertHapus = 1;

          if (
            window.confirm(
              "Apakah anda yakin akan menghapus Elemen " + card.title + " ? ",
            )
          ) {
            MySwal.fire({
              title: <strong>Berhasil dihapus!</strong>,
              html: <i>Elemen {card.title} dihapus</i>,
              icon: "success",
            });

            card.hapus = false;
            console.log(board.columns[0].cards.unshift(card));
          } else {
            console.log(board.columns[1].cards.unshift(card));
          }

          //  addCard(board,1,card)
        }}
        onLaneRename={console.log}
        onCardDragEnd={(board, card, source, destination) => {
          console.log(card);
          if (destination.toColumnId == 2) {
            card.hapus = true;
            console.log(card.hapus);
          } else {
            card.hapus = false;
          }
          //setFormSave(board.columns[1].cards);

          console.log(board.columns[1].cards);

          dataForm = board.columns[1].cards;

          setRepoSize(board.columns[0].cards);
          setFormElement(board.columns[1].cards);

          // setFormData(board.columns[1].cards);
        }}
        initialBoard={board}
        onCardNew={console.log}
        renderCard={(card, { removeCard, dragging }) => (
          <div>
            <div
              dragging={dragging}
              className="col-12 rounded-5 react-kanban-card"
            >
              <div className="row">
                <div className="col-lg-12 rounded-5">
                  <div className="card">
                    {card.hapus ? (
                      <div className="d-flex justify-content-end">
                        <a
                          onClick={removeCard}
                          className="btn btn-danger btn-hover-danger btn-sm btn-icon"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                          style={{ marginBottom: "5px" }}
                        >
                          <i className="fa la-trash text-white mr-5"></i>
                        </a>
                      </div>
                    ) : (
                      <span> </span>
                    )}

                    <span className="badge badge-light-primary">
                      {" "}
                      {card.kategori}{" "}
                    </span>
                    {loadTrigred_rekursive(card)}

                    {card.hapus ? (
                      <div
                        className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                        style={{ marginTop: "6px" }}
                      >
                        <input
                          className="form-check-input h-20px w-30px"
                          type="checkbox"
                          defaultCheck={
                            card.required == "required" ? true : false
                          }
                          onChange={(event) =>
                            safeRequired(event.target.checked, card.id)
                          }
                        />
                        <label className="form-check-label">required</label>
                      </div>
                    ) : (
                      <div></div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      />
    );
  }

  function safeForm() {
    let send = { categoryOptItems: [] };

    console.log("State : " + FormElement);
    console.log("Ref : " + eleRequired.current.value);

    if (eleRequired.current.value == undefined) {
      eleRequired.current.value = FormElement;
    }

    console.log(judulRef.current.value);

    let msg = "";
    if (
      judulRef.current.value == "" ||
      judulRef.current.value.length < 4 ||
      StatusFormPendaftaran == "" ||
      FormElement.length == 0
    ) {
      if (judulRef.current.value == "") {
        msg += "<span> Judul Form Tidak Boleh Kosong </span> <br/>";
      }
      if (judulRef.current.value.length == "") {
        msg += "<span> Judul Form Harus lebih dari 4 karakter </span> <br/>";
      }
      if (StatusFormPendaftaran == "") {
        msg += "<span> Status Publish Harus Dipilih </span> <br/>";
      }
      if (FormElement.length == 0) {
        msg += "<span> Element Belum diisi </span> <br/>";
      }

      MySwal.fire({
        title: <strong> Form Pendaftaran Gagal Disimpan </strong>,
        html: msg,
        icon: "warning",
      });
    } else {
      let cekRequired = false;

      eleRequired.current.value.map((ress, index) => {
        if (ress.required === "required") {
          cekRequired = true;
        }

        send.categoryOptItems.push({
          judul: capitalizeFirstLetter(judulRef.current.value),
          id: ress.id,
          name: ress.name,
          description: ress.description,
          title: ress.title,
          element: ress.element,
          status: StatusFormPendaftaran,
          className: ress.className,
          maxlength: ress.maxlength,
          placeholder: ress.placeholder,
          required: ress.required,
          option: ress.option,
          data_option: ress.data_option,
          size: ress.size,
          min: ress.min,
          max: ress.max,
          html: ress.html,
          file_name: ress.file_name,
          ref_values: ress.ref_values,
          child: ress.child,
          span: ress.span,
          id_repository: ress.id,
          kategori: ress.kategori,
          id_form_kategori: ress.id_form_kategori,
        });
      });

      console.log(send);

      //    let url ='http://localhost/dts-back-dev/public/api/daftarpeserta/createjson-formbuilder';
      //    let url ='http://localhost:8000/api/daftarpeserta/createjson-formbuilder';
      let url =
        process.env.REACT_APP_BASE_API_URI +
        "/daftarpeserta/createjson-formbuilder";

      if (cekRequired === true) {
        swal
          .fire({
            title: "Apakah anda yakin?",
            text: "Pastikan Form Pendaftaran telah benar, Form Pendaftaran tidak dapat diubah jika nantinya telah digunakan pada pelatihan",
            icon: "info",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Ya, Simpan",
            cancelButtonText: "Tidak",
          })
          .then((result) => {
            if (result.isConfirmed) {
              setIsLoading(true);
              axios
                .post(url, send, {
                  headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + Cookies.get("token"),
                  },
                })
                .then(function (response) {
                  setIsLoading(false);
                  console.log(response);

                  if (response.data.result.Status) {
                    MySwal.fire({
                      title: <strong>Berhasil simpan!</strong>,
                      html: <i> Berhasil membuat {judulRef.current.value} </i>,
                      icon: "success",
                    }).then(() => {
                      window.location = "/pelatihan/pendaftaran";
                    });
                  } else {
                    MySwal.fire({
                      title: <sstrong>Gagal disimpan!</sstrong>,
                      html: <i> {response.data.result.Message} </i>,
                      icon: "warning",
                    });
                  }
                })
                .catch(function (error) {
                  setIsLoading(false);
                  MySwal.fire({
                    title: <strong>Information!</strong>,
                    html: <i> Gagal Simpan </i>,
                    icon: "warning",
                  });
                  console.log(error);
                });
            }
          });
      } else {
        setIsLoading(false);
        MySwal.fire({
          title: <sstrong>Gagal disimpan!</sstrong>,
          html: <i> Minimal ada 1 element form yg Required/Wajib Diisi </i>,
          icon: "warning",
        });
      }
    }
  }

  function safeJudul(args) {
    judulForm = args;

    setJudul(args);
  }

  function back() {
    window.location = "/pelatihan/pendaftaran";
  }

  function changeStatus(args) {
    statusPublish = args;
    setStatus(args);
    console.log("status publish " + statusPublish);
  }

  function changeKategori(args) {
    setKategoriFormPilih(args);
    retriveRepository(args);
  }

  const [Pendaftaran, setPendaftaran] = useState(initialPendaftaranState);

  return (
    <div>
      <Header />
      <SideNav />
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={24}
                      height={25}
                      viewBox="0 0 24 25"
                      fill="#009ef7"
                    >
                      <path
                        opacity="0.3"
                        d="M8.9 21L7.19999 22.6999C6.79999 23.0999 6.2 23.0999 5.8 22.6999L4.1 21H8.9ZM4 16.0999L2.3 17.8C1.9 18.2 1.9 18.7999 2.3 19.1999L4 20.9V16.0999ZM19.3 9.1999L15.8 5.6999C15.4 5.2999 14.8 5.2999 14.4 5.6999L9 11.0999V21L19.3 10.6999C19.7 10.2999 19.7 9.5999 19.3 9.1999Z"
                        fill="#009ef7"
                      />
                      <path
                        d="M21 15V20C21 20.6 20.6 21 20 21H11.8L18.8 14H20C20.6 14 21 14.4 21 15ZM10 21V4C10 3.4 9.6 3 9 3H4C3.4 3 3 3.4 3 4V21C3 21.6 3.4 22 4 22H9C9.6 22 10 21.6 10 21ZM7.5 18.5C7.5 19.1 7.1 19.5 6.5 19.5C5.9 19.5 5.5 19.1 5.5 18.5C5.5 17.9 5.9 17.5 6.5 17.5C7.1 17.5 7.5 17.9 7.5 18.5Z"
                        fill="#009ef7"
                      />
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Pelatihan
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Master Form Pendaftaran
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={back}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Tambah Form Pendaftaran
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="row">
                        <div className="col-lg-12">
                          <div className="col-lg-12 mb-7 fv-row">
                            <label className="required form-label">
                              Judul Form
                            </label>
                            <input
                              type="text"
                              id="judul"
                              ref={judulRef}
                              name="judul"
                              className="form-control form-control-sm"
                              placeholder="Masukan Judul Form ...."
                              disabled
                              value={
                                "FORM_" +
                                Cookies.get("user_email").split("@")[0] +
                                "_[ID]"
                              }
                              required
                            />
                          </div>
                          <div className="col-lg-12 mb-7 fv-row">
                            <label className="required form-label">
                              Status Publish
                            </label>
                            <select
                              onChange={(event) =>
                                changeStatus(event.target.value)
                              }
                              value={StatusFormPendaftaran}
                              className="form-select form-select-sm"
                              data-placeholder="Pilih Status Publish"
                              placeholder="Pilih Status Publish"
                              data-allow-clear="true"
                            >
                              <option value="">Pilih Status</option>
                              <option value={0}>Unpublish</option>
                              <option value={1}>Publish</option>
                            </select>
                          </div>
                        </div>
                        <div className="col-lg-12">
                          <h2 className="fs-5 mt-3 text-muted mb-3">
                            Drag atau geser Element ke dalam Canvas
                          </h2>
                          <div className="col-lg-12 mb-7 fv-row">
                            <label className="form-label">
                              Kategori Form Element
                            </label>
                            <select
                              id="KategoriFormPilih"
                              name="KategoriFormPilih"
                              className="form-select form-select-sm"
                              onChange={(event) =>
                                changeKategori(event.target.value)
                              }
                              value={KategoriFormPilih}
                              placeholder="Pilih Form Kategori"
                            >
                              <option value={0}>Semua Kategori</option>
                              {KategoriForm.map((val) => (
                                <>
                                  <option value={val.value}>
                                    {" "}
                                    {val.label}{" "}
                                  </option>
                                </>
                              ))}
                            </select>
                          </div>
                          <UncontrolledBoard />
                        </div>
                      </div>

                      <div className="mb-10">
                        {alertHapus == 1 ? (
                          <div className="alert alert-danger" role="alert">
                            <div className="alert-text">
                              {" "}
                              Element {alertHapus} berhasil dihapus{" "}
                            </div>
                          </div>
                        ) : (
                          <div> </div>
                        )}
                      </div>

                      <div className="form-group fv-row border-top pt-7 mb-7">
                        <div className="d-flex justify-content-center mb-7">
                          <button
                            onClick={back}
                            type="reset"
                            className="btn btn-md btn-light me-3"
                            data-kt-menu-dismiss="true"
                          >
                            Batal
                          </button>
                          <button
                            type="submit"
                            className="btn btn-md btn-primary"
                            onClick={safeForm}
                          >
                            {isLoading ? (
                              <>
                                <span
                                  className="spinner-border spinner-border-sm me-2"
                                  role="status"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Loading...</span>
                                Loading...
                              </>
                            ) : (
                              <>
                                <i className="fa fa-paper-plane me-1"></i>Simpan
                              </>
                            )}
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PendaftaranDataLKanban;
