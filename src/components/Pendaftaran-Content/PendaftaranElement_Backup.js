import React, { useState, useEffect } from "react";
import Header from "../../components/Header";
import SideNav from "../../components/SideNav";
import Footer from "../../components/Footer";
import Swal from "sweetalert2";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import withReactContent from "sweetalert2-react-content";
// import { useForm } from "react-hook-form";
const PendaftaranAdd = () => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  const [uploadFiles, setUploadFiles] = useState(true);
  const [files, setFiles] = useState([]);
  // let history = useNavigate();
  const defrow = [
    {
      name: "",
      element: "",
      size: "",
      option: "",
      data_option: "",
      req: "",
      placeholder: "",
      min: "",
      max: "",
      sts_false: true,
      triger: false,
      span: "",
      upload: "",
      id_parent: "",
      key_parent: "",
    },
  ];
  // const [rows, setRows] = useState([{}]);
  const [rows, setRows] = useState(defrow);
  const history = useNavigate();
  const columnsArray = [
    "Nama Field",
    "Pilih Element",
    "Ukuran Font",
    "Placeholder",
    "Min",
    "Max Length",
    "Option",
    "Data Option",
    "Span",
    "Req",
    "Triger",
    "Parent Trigger",
    "Key Parent Trigger",
    "",
  ];
  const initialPendaftaranState = {
    id: null,
    judul: "",
  };
  const MySwal = withReactContent(Swal);
  const columnsName = ["name"];
  const columnElement = ["element"];
  const columnSize = ["size"];
  const columnOption = ["option"];
  const columnDataOption = ["data_option"];
  const columnReq = ["req"];
  const columnPlaceholder = ["placeholder"];
  const columnMin = ["min"];
  const columnMax = ["max"];
  const columnTriger = ["triger"];
  const columnSpan = ["span"];
  const columnUpload = ["upload"];
  const columnIdParent = ["parent"];
  const columnKeyParent = ["key_parent"];
  const [RepoSize, setRepoSize] = useState();
  const [ReferenceSize, setReference] = useState();
  const [Pendaftaran, setPendaftaran] = useState(initialPendaftaranState);
  // const [isDisabled, setDisabled] = useState([]);
  const [isDisabled, setDisabled] = useState([]);
  const [isText, setText] = useState([{}]);
  const [isUpload, setUpload] = useState([]);
  const isRequired = (value) => {
    return value != null && value.trim().length > 0;
  };
  useEffect(() => {
    retriveRepository();
    retriveReferences();
    // setText([{...isText,index:'1'}]);
  }, []);
  const handleAddRow = () => {
    console.log(rows);
    // rows.map((number) =>
    //   console.log(number.name)
    // )
    const item = {
      name: "",
      element: "",
      size: "",
      option: "",
      data_option: "",
      req: "",
      placeholder: "",
      min: "",
      max: "",
      sts_false: true,
      triger: false,
      span: "",
      upload: [],
      parent: "",
      key_parent: "",
    };
    setRows([...rows, item]);
  };
  const handleInputChange = (event) => {
    console.log(event.target);
    const { name, value } = event.target;
    setPendaftaran({ ...Pendaftaran, [name]: value });
  };
  const onFileChange = (e) => {
    let prope = e.target.attributes.column.value; //upload
    let index = e.target.attributes.index.value; //0
    let fieldValue = e.target.files[0];
    const tempRows = [...rows];
    const tempObj = rows[index];
    tempObj[prope] = fieldValue;
    tempRows[index] = tempObj;
    setRows(tempRows);
  };
  const handleChange_Repo = (e) => {
    let index = e.nativeEvent.target.selectedIndex;
    let label = e.nativeEvent.target[index].text;
    // console.log(label);
    const { name, value } = e.target;
    setPendaftaran({ ...Pendaftaran, [name]: value, judul_nama: label });
  };
  const postResults = () => {
    console.log(rows);
  };
  const retriveRepository = () => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/list-repo-judul",
        {
          start: "0",
          length: "100",
        },
      )
      .then(function (response) {
        if (response.status === 200) {
          console.log(response);
          let repo = response.data.result;
          if (repo.Status === true) {
            // cnso
            setRepoSize(repo.Data);
            console.log(RepoSize);
          } else {
            setRepoSize();
          }
        }
      })
      .catch(function (error) {
        setRepoSize();
      });
  };
  const retriveReferences = () => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/list-reference",
        {
          start: "0",
          length: "50",
        },
      )
      .then(function (response) {
        if (response.status === 200) {
          console.log(response);
          let repo = response.data.result;
          if (repo.Status === true) {
            // cnso
            setReference(repo.Data);
            console.log(RepoSize);
          }
        }
      })
      .catch(function (error) {});
  };
  const postResults_Save = () => {
    let categoryOptItems = [];
    let summerAt = "";
    let sumNot = "";
    let sumVar = "";

    const listItems = rows.map((number) => {
      if (number.name === undefined) {
        MySwal.fire({
          title: <strong>Information!</strong>,
          html: <i>Silahkan Lengkapi Data Nama!</i>,
          icon: "warning",
        });
        return false;
      }
      if (number.element === undefined) {
        MySwal.fire({
          title: <strong>Information!</strong>,
          html: <i>Silahkan Lengkapi Data Tipe Element!</i>,
          icon: "warning",
        });
        return false;
      }
      if (number.size === undefined) {
        MySwal.fire({
          title: <strong>Information!</strong>,
          html: <i>Silahkan Lengkapi Data Ukuran Element!</i>,
          icon: "warning",
        });
        return false;
      }

      if (number.option === undefined) {
        sumVar = "";
      } else {
        sumVar = number.option;
      }
      if (number.data_option === undefined) {
        summerAt = "";
      } else {
        summerAt = number.data_option;
      }
      if (number.req === "") {
        sumNot = "";
      } else {
        sumNot = "required";
      }
      if (number.element !== "uploadfiles") {
        const formData = new FormData();
        formData.append("judul", Pendaftaran.judul);
        formData.append("name", number.name);
        formData.append("element", number.element);
        formData.append("size", number.size);
        formData.append("option", sumVar);
        formData.append("data_option", summerAt);
        formData.append("required", sumNot);
        formData.append("min", number.min);
        formData.append("max", number.max);
        formData.append("maxlength", number.max);
        formData.append("className", "form-solid");
        formData.append("placeholder", number.placeholder);
        formData.append("span", number.span);
        formData.append("upload", number.upload);
        formData.append("file_name", number.name.replace(/ /g, "_"));
        formData.append("triggered", number.triger); //1
        formData.append("triggered_name", number); //dataoption trivger
        formData.append("name_optio"); //trigered name
        axios
          .post(process.env.REACT_APP_BASE_API_URI + "/create-repo", formData)
          .then(function (response) {
            MySwal.fire({
              title: <strong>Information!</strong>,
              html: <i>{response.data.result.Message}</i>,
              icon: "success",
            });
          })
          .catch(function (error) {
            MySwal.fire({
              title: <strong>Information!</strong>,
              html: <i>{error.response.data.result.Message}</i>,
              icon: "warning",
            });
          });
      } else {
        const formData = new FormData();
        formData.append("judul", Pendaftaran.judul);
        formData.append("name", number.name);
        formData.append("element", number.element);
        formData.append("size", number.size);
        formData.append("option", "");
        formData.append("data_option", number.upload);
        formData.append("required", sumNot);
        formData.append("min", number.min);
        formData.append("max", number.max);
        formData.append("maxlength", number.max);
        formData.append("className", "form-solid");
        formData.append("placeholder", number.placeholder);
        formData.append("span", number.span);
        formData.append("upload", number.upload);
        formData.append("file_name", number.name.replace(/ /g, "_"));
        axios
          .post(process.env.REACT_APP_BASE_API_URI + "/create-repo", formData)
          .then(function (response) {
            MySwal.fire({
              title: <strong>Information!</strong>,
              html: <i>{response.data.result.Message}</i>,
              icon: "success",
            });
            console.log(response);
          })
          .catch(function (error) {
            MySwal.fire({
              title: <strong>Information!</strong>,
              html: <i>{error.response.data.result.Message}</i>,
              icon: "warning",
            });
          });
      }

      // categoryOptItems.push({
      //   'judul' : '123',
      //   'name' : number.name,
      //   'file_name' : number.name.replace(" ", "_"),
      //   'element' : number.element,
      //   'size' : number.size,
      //   'option' : sumVar,
      //   'data_option' : summerAt,
      //   'required' : sumNot,
      //   'min': number.min,
      //   'max':number.max,
      //   'maxlength' : number.max,
      //   'className' : 'form-solid',
      //   'placeholder' : number.placeholder,
      //   'span' : number.span,
      //   'upload' : number.upload
      // })
    });

    const formData = new FormData();
    formData.append("judul", Pendaftaran.judul);
    // axios.post(process.env.REACT_APP_BASE_API_URI+'/daftarpeserta/create-repo-loop', {
    //    categoryOptItems
    // })
    // .then(function (response) {
    //   console.log(response);
    //   // return false;
    //     MySwal.fire({
    //         title: <strong>Information!</strong>,
    //         html: <i>{response.data.result.Message}</i>,
    //         icon: 'success'
    //       });
    //     console.log(response);
    //     history('/repository/element');
    //     // history('/' + urlSegmentZero + '/' + urlSegmenttOne );
    //     // setCurrentAkademi(response.data.result.Data[0]);
    // })
    // .catch(function (error) {
    //     MySwal.fire({
    //         title: <strong>Information!</strong>,
    //         html: <i>{error.response.data.result.Message}</i>,
    //         icon: 'warning'
    //       });
    // });
  };
  const handleRemoveSpecificRow = (idx) => {
    const tempRows = [...rows];
    tempRows.splice(idx, 1);
    setRows(tempRows);
  };
  const checkTriger = (idx, e) => {
    console.log(rows);
    console.log(idx);
    // console.log(idx);
    if (e.target.checked === true) {
      let furo = [];
      let item = [...rows];
      rows[idx].data_option.split(";").map(
        (number) =>
          // console.log(number)
          item.push({
            name: "",
            element: "",
            size: "",
            option: "",
            data_option: "",
            req: "",
            placeholder: "",
            min: "",
            max: "",
            sts_false: true,
            triger: false,
            span: "",
            upload: [],
            parent: number,
            key_parent: rows[idx].name,
          }),
        // setRows([...rows, item])
        // setRows([...rows, item])
      );
      setRows(item);
      // setRows(...rows, furo);
    } else {
      handleRemoveSpecificRow(idx);
    }

    // let prope = e.target.attributes.column.value;
    // let index = e.target.attributes.index.value;
    // console.log(e.target.checked);
  };
  const updateState = (e) => {
    let prope = e.target.attributes.column.value;
    let index = e.target.attributes.index.value;
    console.log(prope);
    let fieldValue = e.target.value;
    const tempRows = [...rows];
    const tempObj = rows[index];
    tempObj[prope] = fieldValue;
    tempRows[index] = tempObj;
    console.log(index);
    const tempObjx = columnOption[index];
    let checkCol = [prope] + [index];
    let opt = "option";
    if (e.target.attributes.index.value === index) {
      if (prope === "element") {
        if (
          tempObj[prope] === "select" ||
          tempObj[prope] === "checkbox" ||
          tempObj[prope] === "radiogroup"
        ) {
          setDisabled({ isDisabled, [index]: false });
        } else {
          setDisabled({ isDisabled, [index]: true });
        }
      }
      setRows(tempRows);
      if (prope === "option") {
        if (tempObj[prope] === "manual") {
          setText([[index], { index: "1" }]);
        } else if (tempObj[prope] === "referensi") {
          setText([[index], { [index]: "2" }]);
        } else {
          setText([[index], { index: "3" }]);
        }
        console.log(isText);
      }
    }
  };

  function outOfBox(props) {
    if (props === "referensi") {
      return (
        <select
          className="form-control-sm form-solid"
          data-kt-select2="true"
          data-placeholder="Select option"
          data-dropdown-parent="#kt_menu_61484c5a8da38"
          data-allow-clear="true"
        >
          {/* <option /> */}
          <option value="1">Publish</option>
          <option value="0">Unpublish</option>
        </select>
      );
    } else {
    }
  }
  return (
    <div>
      <Header />
      <SideNav />
      <div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h2 style={{ textTransform: "capitalize" }}>
                            List {segment_url[1]}
                          </h2>
                        </div>
                        <div className="card-toolbar">
                          <button
                            className="btn btn-flex btn-warning btn-sm px-6"
                            data-bs-toggle="modal"
                            data-bs-target="#kt_modal_add_permission"
                          >
                            <i className="las la-quote-left"></i>
                            Harap Baca
                          </button>
                          &nbsp;&nbsp;
                          <button
                            onClick={handleAddRow}
                            className="btn btn-primary btn-text-light btn-hover-text-success font-weight-bold btn-sm"
                          >
                            <i className="las la-plus"></i>
                            Tambah Field
                          </button>
                          &nbsp;&nbsp;
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="table-responsive">
                          <table
                            className="table table-bordered table-responsive"
                            id="tabx_logic"
                            style={{ width: "max-content" }}
                          >
                            <thead>
                              <tr>
                                {columnsArray.map((column, index) => (
                                  <th
                                    className="fw-bold fs-6 text-gray-800"
                                    key={index}
                                  >
                                    {column}
                                  </th>
                                ))}
                                <th />
                              </tr>
                            </thead>
                            <tbody>
                              {rows.map((item, idx) => (
                                <tr key={idx}>
                                  {/* <td>{idx + 1}</td> */}
                                  {columnsName.map((column, index) => (
                                    <td key={index} style={{ width: "200px" }}>
                                      <div className="form-group">
                                        <input
                                          type="text"
                                          column={column}
                                          value={rows[idx][column]}
                                          index={idx}
                                          placeholder="Field"
                                          className="form-control form-control-sm"
                                          required
                                          onChange={(e) => updateState(e)}
                                        />
                                      </div>
                                    </td>
                                  ))}
                                  {columnElement.map((column, index) => (
                                    <td key={index} style={{ width: "140px" }}>
                                      <select
                                        value={rows[idx][column]}
                                        className="form-control  form-control-sm"
                                        data-placeholder="Select option"
                                        index={idx}
                                        column={column}
                                        data-allow-clear="true"
                                        id="pilih_element[]"
                                        name="pilih_element"
                                        required
                                        onChange={(e) => updateState(e)}
                                      >
                                        <option>Pilih Element</option>
                                        <option value="select">Select</option>
                                        <option value="checkbox">
                                          Checkbox
                                        </option>
                                        <option value="text">Text</option>
                                        <option value="textarea">
                                          Text Area
                                        </option>
                                        <option value="radiogroup">
                                          Radio
                                        </option>
                                        {/* <option value='file'>File Image</option> */}
                                        <option value="datepicker">
                                          Input Date
                                        </option>
                                        <option value="file-doc">
                                          File Document
                                        </option>
                                        <option value="uploadfiles">
                                          Upload Document
                                        </option>
                                        <option value="fileimage">
                                          File Image
                                        </option>
                                      </select>
                                    </td>
                                  ))}
                                  {columnSize.map((column, index) => (
                                    <td key={index} style={{ width: "100px" }}>
                                      <select
                                        value={rows[idx][column]}
                                        className="form-control  form-control-sm"
                                        data-placeholder="Select option"
                                        index={idx}
                                        column={column}
                                        data-allow-clear="true"
                                        id="pilih_element[]"
                                        name="pilih_element"
                                        onChange={(e) => updateState(e)}
                                      >
                                        <option value="">Pilih Size</option>
                                        <option value="col-md-6">
                                          Half-Width
                                        </option>
                                        <option value="col-md-12">
                                          Full-Width
                                        </option>
                                      </select>
                                    </td>
                                  ))}
                                  {columnPlaceholder.map((column, index) => (
                                    <>
                                      {item["element"] === "select" ||
                                      item["element"] === "radiogroup" ||
                                      item["element"] === "checkbox" ||
                                      item["element"] === "file-doc" ||
                                      item["element"] === "uploadfiles" ||
                                      item["element"] === "datepicker" ? (
                                        <td
                                          key={index}
                                          style={{ width: "200px" }}
                                        >
                                          <div className="form-group">
                                            <input
                                              type="text"
                                              column={column}
                                              value={rows[idx][column]}
                                              index={idx}
                                              placeholder="Field"
                                              className="form-control form-control-sm"
                                              disabled
                                              onChange={(e) => updateState(e)}
                                            />
                                          </div>
                                        </td>
                                      ) : (
                                        <td
                                          key={index}
                                          style={{ width: "200px" }}
                                        >
                                          <div className="form-group">
                                            <input
                                              type="text"
                                              column={column}
                                              value={rows[idx][column]}
                                              index={idx}
                                              placeholder="Field"
                                              className="form-control form-control-sm"
                                              onChange={(e) => updateState(e)}
                                            />
                                          </div>
                                        </td>
                                      )}
                                    </>
                                  ))}
                                  {columnMin.map((column, index) => (
                                    <>
                                      {item["element"] === "select" ||
                                      item["element"] === "radiogroup" ||
                                      item["element"] === "checkbox" ||
                                      item["element"] === "file-doc" ||
                                      item["element"] === "uploadfiles" ||
                                      item["element"] === "datepicker" ? (
                                        <td key={index}>
                                          <div className="form-group">
                                            <input
                                              type="number"
                                              column={column}
                                              value={rows[idx][column]}
                                              index={idx}
                                              max="225"
                                              min="1"
                                              placeholder="Field"
                                              className="form-control form-control-sm"
                                              disabled
                                              onChange={(e) => updateState(e)}
                                            />
                                          </div>
                                        </td>
                                      ) : (
                                        <td key={index}>
                                          <div className="form-group">
                                            <input
                                              type="number"
                                              column={column}
                                              value={rows[idx][column]}
                                              index={idx}
                                              placeholder="Field"
                                              className="form-control form-control-sm"
                                              onChange={(e) => updateState(e)}
                                            />
                                          </div>
                                        </td>
                                      )}
                                    </>
                                  ))}
                                  {columnMax.map((column, index) => (
                                    <>
                                      {item["element"] === "select" ||
                                      item["element"] === "radiogroup" ||
                                      item["element"] === "checkbox" ||
                                      item["element"] === "file-doc" ||
                                      item["element"] === "uploadfiles" ||
                                      item["element"] === "datepicker" ? (
                                        <td key={index}>
                                          <div className="form-group">
                                            <input
                                              type="number"
                                              column={column}
                                              value={rows[idx][column]}
                                              index={idx}
                                              placeholder="Field"
                                              className="form-control form-control-sm"
                                              onChange={(e) => updateState(e)}
                                              disabled
                                            />
                                          </div>
                                        </td>
                                      ) : (
                                        <td key={index}>
                                          <div className="form-group">
                                            <input
                                              type="number"
                                              column={column}
                                              value={rows[idx][column]}
                                              index={idx}
                                              placeholder="Field"
                                              className="form-control form-control-sm"
                                              onChange={(e) => updateState(e)}
                                            />
                                          </div>
                                        </td>
                                      )}
                                    </>
                                  ))}
                                  {/* {(isUpload[idx] === false) ? ( */}
                                  <>
                                    {item["element"] === "select" ||
                                    item["element"] === "checkbox" ||
                                    item["element"] === "radiogroup" ? (
                                      <>
                                        {columnOption.map((column, index) => (
                                          <td key={index}>
                                            <>
                                              <select
                                                value={rows[idx][column]}
                                                className="form-control form-control-sm selectpicker"
                                                data-placeholder="Select option"
                                                index={idx}
                                                column={column}
                                                data-allow-clear="true"
                                                id="pilih_element[]"
                                                name="pilih_element"
                                                onChange={(e) => updateState(e)}
                                              >
                                                <option>Pilih Option</option>
                                                <option value="manual">
                                                  Manual
                                                </option>
                                                <option value="referensi">
                                                  Select Reference
                                                </option>
                                              </select>
                                            </>
                                          </td>
                                        ))}
                                        {columnDataOption.map(
                                          (column, index) => (
                                            <td key={index}>
                                              {item["option"] === "manual" ||
                                              item["option"] === "" ? (
                                                <>
                                                  {/* <span>{columnElement}</span> */}
                                                  {/* <span>{item['element']}</span> */}

                                                  <input
                                                    type="text"
                                                    column={column}
                                                    value={rows[idx][column]}
                                                    index={idx}
                                                    id="data_option[]"
                                                    placeholder="Data1;Data2"
                                                    className="form-control form-control-sm"
                                                    onChange={(e) =>
                                                      updateState(e)
                                                    }
                                                  />
                                                </>
                                              ) : (
                                                <>
                                                  <select
                                                    value={rows[idx][column]}
                                                    className="form-control  form-control-sm selectpicker"
                                                    data-placeholder="Select option"
                                                    index={idx}
                                                    column={column}
                                                    data-allow-clear="true"
                                                    id="data_option[]"
                                                    name="pilih_element"
                                                    onChange={(e) =>
                                                      updateState(e)
                                                    }
                                                  >
                                                    <option value="">
                                                      Pilh Referensi
                                                    </option>
                                                    {ReferenceSize.map(
                                                      (size) => (
                                                        <option value={size.id}>
                                                          {size.name}
                                                        </option>
                                                      ),
                                                    )}
                                                  </select>
                                                </>
                                              )}
                                            </td>
                                          ),
                                        )}
                                      </>
                                    ) : item["element"] === "uploadfiles" ? (
                                      <>
                                        {columnUpload.map((column, index) => (
                                          <td colSpan="2">
                                            <input
                                              type="file"
                                              className="form-control form-control-sm font-size-h4"
                                              name="uploadfiles"
                                              index={idx}
                                              column={column}
                                              accept="application/pdf"
                                              id="uploadfiles[]"
                                              onChange={(e) => onFileChange(e)}
                                            ></input>
                                          </td>
                                        ))}
                                      </>
                                    ) : (
                                      <>
                                        {columnOption.map((column, index) => (
                                          <td
                                            key={index}
                                            style={{ pointerEvents: "none" }}
                                          >
                                            <>
                                              <select
                                                value={rows[idx][column]}
                                                className="form-control form-control-sm selectpicker"
                                                data-placeholder="Select option"
                                                index={idx}
                                                column={column}
                                                data-allow-clear="true"
                                                id="pilih_element[]"
                                                name="pilih_element"
                                                onChange={(e) => updateState(e)}
                                              >
                                                <option>Pilih Option</option>
                                                <option value="manual">
                                                  Manual
                                                </option>
                                                <option value="referensi">
                                                  Select Reference
                                                </option>
                                              </select>
                                            </>
                                          </td>
                                        ))}
                                        {columnDataOption.map(
                                          (column, index) => (
                                            <td
                                              key={index}
                                              style={{ pointerEvents: "none" }}
                                            >
                                              {item["option"] === "manual" ||
                                              item["option"] === "" ? (
                                                <>
                                                  {/* <span>{columnElement}</span> */}
                                                  {/* <span>{item['element']}</span> */}

                                                  <input
                                                    type="text"
                                                    column={column}
                                                    value={rows[idx][column]}
                                                    index={idx}
                                                    id="data_option[]"
                                                    placeholder="Data1;Data2"
                                                    className="form-control form-control-sm"
                                                    onChange={(e) =>
                                                      updateState(e)
                                                    }
                                                  />
                                                </>
                                              ) : (
                                                <>
                                                  {/* {isText[idx]} */}
                                                  {/* {isDisabled[idx]} */}
                                                  <select
                                                    value={rows[idx][column]}
                                                    className="form-control  form-control-sm selectpicker"
                                                    data-placeholder="Select option"
                                                    index={idx}
                                                    column={column}
                                                    data-allow-clear="true"
                                                    id="data_option[]"
                                                    name="pilih_element"
                                                    onChange={(e) =>
                                                      updateState(e)
                                                    }
                                                  >
                                                    <option value="">
                                                      Pilh Referensi
                                                    </option>
                                                    {ReferenceSize.map(
                                                      (size) => (
                                                        <option value={size.id}>
                                                          {size.name}
                                                        </option>
                                                      ),
                                                    )}
                                                  </select>
                                                </>
                                              )}
                                            </td>
                                          ),
                                        )}
                                      </>
                                    )}
                                  </>
                                  {columnSpan.map((column, index) => (
                                    <td key={index}>
                                      <input
                                        type="text"
                                        column={column}
                                        value={rows[idx][column]}
                                        index={idx}
                                        id="data_option[]"
                                        placeholder="Span"
                                        className="form-control form-control-sm"
                                        onChange={(e) => updateState(e)}
                                      />
                                      <span></span>
                                    </td>
                                  ))}
                                  {columnReq.map((column, index) => (
                                    <td key={index}>
                                      <input
                                        type="checkbox"
                                        value={rows[idx][column]}
                                        index={idx}
                                        column={column}
                                        name="Checkboxes"
                                        onChange={(e) => updateState(e)}
                                      />
                                      <span></span>
                                    </td>
                                  ))}
                                  {columnTriger.map((column, index) => (
                                    <td key={index}>
                                      <input
                                        type="checkbox"
                                        index={idx}
                                        value={rows[idx][column]}
                                        column={column}
                                        name="Checkboxes"
                                        onChange={(e) => checkTriger(idx, e)}
                                      />
                                      <span></span>
                                    </td>
                                  ))}

                                  {columnIdParent.map((column, index) => (
                                    <td key={index}>
                                      <input
                                        type="text"
                                        column={column}
                                        value={rows[idx][column]}
                                        index={idx}
                                        id="parent[]"
                                        placeholder="Span"
                                        className="form-control form-control-sm"
                                        onChange={(e) => updateState(e)}
                                      />
                                      <span></span>
                                    </td>
                                  ))}
                                  {columnKeyParent.map((column, index) => (
                                    <td key={index}>
                                      <input
                                        type="text"
                                        column={column}
                                        value={rows[idx][column]}
                                        index={idx}
                                        id="parent[]"
                                        placeholder="Span"
                                        className="form-control form-control-sm"
                                        onChange={(e) => updateState(e)}
                                      />
                                      <span></span>
                                    </td>
                                  ))}
                                  <td>
                                    <button
                                      className="btn btn-icon btn-danger"
                                      onClick={() =>
                                        handleRemoveSpecificRow(idx)
                                      }
                                    >
                                      <i className="fas fa-trash fs-5">
                                        {item["triger"]}
                                      </i>
                                    </button>
                                  </td>
                                </tr>
                              ))}
                            </tbody>
                          </table>
                        </div>
                        <div className="d-flex justify-content-between align-items-right mt-7 pull-right">
                          <button
                            onClick={postResults}
                            className="btn btn-light btn-text-success btn-hover-text-success font-weight-bold btn-sm"
                          >
                            Save Results
                          </button>
                          &nbsp;&nbsp;
                          <button
                            onClick={postResults_Save}
                            className="btn btn-primary btn-text-light btn-hover-text-success font-weight-bold btn-sm"
                          >
                            {" "}
                            <i className="fas fa-paper-plane action"></i>
                            Simpan
                          </button>
                        </div>
                        <div
                          className="modal fade"
                          id="kt_modal_add_permission"
                          tabIndex={-1}
                          aria-hidden="true"
                        >
                          <div className="modal-dialog modal-dialog-centered mw-650px">
                            <div className="modal-content">
                              <div className="modal-header">
                                <h2 className="fw-bolder">
                                  {" "}
                                  Data yang sudah diisi peserta!
                                </h2>
                                <div
                                  className="btn btn-icon btn-sm btn-active-icon-primary"
                                  data-kt-permissions-modal-action="close"
                                >
                                  <span className="svg-icon svg-icon-1">
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width={24}
                                      height={24}
                                      viewBox="0 0 24 24"
                                      fill="none"
                                    >
                                      <rect
                                        opacity="0.5"
                                        x={6}
                                        y="17.3137"
                                        width={16}
                                        height={2}
                                        rx={1}
                                        transform="rotate(-45 6 17.3137)"
                                        fill="black"
                                      />
                                      <rect
                                        x="7.41422"
                                        y={6}
                                        width={16}
                                        height={2}
                                        rx={1}
                                        transform="rotate(45 7.41422 6)"
                                        fill="black"
                                      />
                                    </svg>
                                  </span>
                                </div>
                              </div>
                              <div className="modal-body scroll-y mx-5 mx-xl-15 my-7">
                                <form
                                  id="kt_modal_add_permission_form"
                                  className="form"
                                  action="#"
                                >
                                  Data Diri Foto Profil, Nama Lengkap, Email,
                                  NIK, Jenis Kelamin, Nomor Handphone, Agama,
                                  Tempat dan Tanggal Lahir, Kontak Darurat (Nama
                                  Lengkap, Nomor Handphone, Hubungan), File KTP
                                  Alamat KTP Alamat Lengkap, Provinsi,
                                  Kota/Kabupaten, Kecamatan, Desa/Kelurahan,
                                  Kode Pos Alamat Domisili Alamat Lengkap,
                                  Provinsi, Kota/Kabupaten, Kecamatan,
                                  Desa/Kelurahan, Kode Pos Pendidikan Terakhir
                                  Jenjang Pendidikan : TK, SD, SMP, SMA : Asal
                                  Sekolah, Tahun Masuk, File Ijazah D3, S1, S2,
                                  S3 : Asal Perguruan Tinggi, Program Studi,
                                  IPK, Tahun Masuk, File Ijazah Pekerjaan Status
                                  Pekerjaan : Bekerja : Pekerjaan,
                                  Perusahaan/Institut Tempat Bekerja,
                                  Penghasilan Tidak Bekerja Pelajar/Mahasiswa:
                                  Sekolah/Perguruan Tinggi, Tahun Masuk
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </div>
  );
};

export default PendaftaranAdd;
