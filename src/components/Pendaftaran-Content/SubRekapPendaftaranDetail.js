import React, { useState, useEffect, useMemo, useRef } from "react";
import AkademiService from "../../service/AkademiService";
import Pagination from "@material-ui/lab/Pagination";
import { useTable, useSortBy } from "react-table";
import { GlobalFilter, DefaultFilterForColumn } from "../Filter";
import Header from "../Header";
import SideNav from "../SideNav";
import Footer from "../Footer";
import Select from "react-select";
import axios from "axios";
import { useHistory, useNavigate, useParams } from "react-router-dom";
import {
  useFilters,
  useGlobalFilter,
} from "react-table/dist/react-table.development";

const SubRekapPendaftaranDetail = (props) => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  const [akademi, setAkademi] = useState([]);
  const [infoA, setInfoA] = useState([]);
  const [searchTitle, setSearchTitle] = useState("");
  const [page, setPage] = useState(0);
  const [count, setCount] = useState(0);
  const [pageSize, setPageSize] = useState(10);
  const pageSizes = [10];
  const [fRep, setfRep] = useState();
  const uppercase = {
    "text-transforms": "capitalize",
  };
  const initialVal = [
    {
      label: "",
      value: "",
    },
  ];
  const { id } = useParams();
  const [optionList, setOptioList] = useState(initialVal);
  const AkademiRef = useRef();
  AkademiRef.current = akademi;

  const history = useNavigate();
  useEffect(() => {
    retrieveAkademi();
    retriveOption();
  }, [page, pageSize]);

  const onChangeSearchTitle = (e) => {
    console.log(e);
    const searchTitle = e.target.value;
    setSearchTitle(searchTitle);
  };
  const retrieveAkademi = async () => {
    const params = getRequestParams(searchTitle, page, pageSize);
    axios.defaults.headers.common["Authorization"] =
      "Bearer 19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp";
    const respo = await axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/r-detail-rekap-pendaftaran-byid",
        {
          mulai: page,
          limit: pageSize,
          id: id,
        },
      )
      .then(function (response) {
        const { tutorials, totalPages } = response.data.result.list_peserta;
        setAkademi(response.data.result.list_peserta);
        setInfoA(response.data.result.jadwal_detail[0]);
        setCount(response.data.result.length);
        console.log("sdniw");
        console.log(response.data.result.jadwal_detail[0]);
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  const refreshList = () => {
    retrieveAkademi();
  };
  const handlePageChange = (event, value) => {
    if (value === 1) {
      setPage(0);
    } else {
      setPage(pageSize * value - 10);
    }
    console.log(pageSize);
    retrieveAkademi();
  };
  const handlePageSizeChange = (event) => {
    setPageSize(event.target.value);
    setPage(0);
  };
  const removeAllAkademi = () => {
    AkademiService.removeAll()
      .then((response) => {
        console.log(response);
        refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const deleteAkademi = (rowIndex) => {
    const id = AkademiRef.current[rowIndex].id;
    AkademiService.remove(id)
      .then((response) => {
        window.location.href = "";
        let newAkademi = [...AkademiRef.current];
        newAkademi.splice(rowIndex, 1);
        setAkademi(newAkademi);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const getRequestParams = (searchTitle, page, pageSize) => {
    let params = {};

    if (searchTitle) {
      params["title"] = searchTitle;
    }

    if (page) {
      params["page"] = page - 1;
    }

    if (pageSize) {
      params["size"] = pageSize;
    }

    return params;
  };
  const retriveOption = async () => {
    const respon = axios
      .post(process.env.REACT_APP_BASE_API_URI + "/akademi/list-akademi", {
        start: "0",
        length: "50",
      })
      .then(function (response) {
        console.log("do");
        console.log(response);
        if (response.status === 200) {
          if (response.data.result.Status === true) {
            let repo = response.data.result.Data;
            let ui = [{}];
            const listItem = repo.map((number) =>
              ui.push({
                label: number.name,
                value: number.id,
              }),
            );
            setOptioList(ui);
            console.log(optionList);
          } else {
            setOptioList();
          }
        }
      });
  };
  const retrieveFind = async () => {
    const responsi = await axios
      .post(process.env.REACT_APP_BASE_API_URI + "/akademi/cari-akademi", {
        id: fRep,
      })
      .then(function (response) {
        if (response.status === 200) {
          console.log(response);
          if (response.data.result.Status === true) {
            console.log(response.data.result.Data);
            // console.log(response.Data.result.Data);
            setAkademi(response.data.result.Data);
            setCount(response.data.result.Data.length);
            retriveOption();
          } else {
            refreshList();
            retriveOption();
          }
        } else {
          refreshList();
          retriveOption();
        }
      })
      .catch((error) => {
        refreshList();
        retriveOption();
      });
  };

  const handleFilter = (value) => {
    setOptioList({ value: value });
    console.log(value.value);
    setfRep(value.value);
  };
  const handleSearch = () => {
    console.log(fRep);
    //   console.log(event.target);
    retrieveFind();
  };
  const handleShow = (cell) => {
    console.log(cell?.row?.original);
    console.log(cell?.row?.original.name);
    history(
      "/rekap/pendaftaran/view/detail/" + cell?.row?.original.id_pelatihan,
    );
  };
  const handleShowPrev = (cell) => {
    console.log(cell?.row?.original);
    console.log(cell?.row?.original.name);
    history("/pelatihan/akademi/preview/" + cell?.row?.original.id);
  };
  const columns = useMemo(
    () => [
      {
        Header: "No",
        accessor: "",
        className: "",
        sortType: "basic",

        Cell: (props) => {
          return (
            <div
              className="min-w-40px min-h-40px mw-50px mh-40px"
              style={{ textAlign: "center" }}
            >
              <center>
                <span>{props.row.original.id_pelatihan}</span>
              </center>
            </div>
          );
        },
      },
      {
        Header: "Pelatihan",
        accessor: "",
        className: "",
        sortType: "basic",
        Cell: (props) => {
          console.log(props);
          return (
            <div
              className="min-w-200px min-h-200px mw-200px mh-200px"
              style={{ textAlign: "center" }}
            >
              <center>
                <span>{props.row.original.id_slug}</span>
              </center>
            </div>
          );
        },
      },
      {
        Header: "Pelaksanaan",
        accessor: "slug",
        className: "",
        sortType: "basic",
        Cell: (props) => {
          return (
            <div style={{ textAlign: "justify" }}>
              <h4>{props.row.original.pelatihan}</h4>
              <span>{props.row.original.penyelenggara}</span>
              <br />
              <span>{props.row.original.metode_pelatihan}</span>
            </div>
          );
        },
      },
      {
        Header: "Aksi",
        accessor: "actions",
        Cell: (props) => {
          const rowIdx = props.row.nama;
          return (
            <div>
              <button className="btn btn-icon btn-active-light-primary w-30px h-30px me-3">
                <span
                  onClick={() => handleShow(props)}
                  className="svg-icon svg-icon-3"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={24}
                    height={24}
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <path
                      d="M17.5 11H6.5C4 11 2 9 2 6.5C2 4 4 2 6.5 2H17.5C20 2 22 4 22 6.5C22 9 20 11 17.5 11ZM15 6.5C15 7.9 16.1 9 17.5 9C18.9 9 20 7.9 20 6.5C20 5.1 18.9 4 17.5 4C16.1 4 15 5.1 15 6.5Z"
                      fill="black"
                    />
                    <path
                      opacity="0.3"
                      d="M17.5 22H6.5C4 22 2 20 2 17.5C2 15 4 13 6.5 13H17.5C20 13 22 15 22 17.5C22 20 20 22 17.5 22ZM4 17.5C4 18.9 5.1 20 6.5 20C7.9 20 9 18.9 9 17.5C9 16.1 7.9 15 6.5 15C5.1 15 4 16.1 4 17.5Z"
                      fill="black"
                    />
                  </svg>
                </span>
              </button>
              <button
                className="btn btn-icon btn-active-light-primary w-30px h-30px me-3"
                onClick={() => handleShowPrev(props)}
              >
                <span className="svg-icon svg-icon-3">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <path
                      d="M16.0173 9H15.3945C14.2833 9 13.263 9.61425 12.7431 10.5963L12.154 11.7091C12.0645 11.8781 12.1072 12.0868 12.2559 12.2071L12.6402 12.5183C13.2631 13.0225 13.7556 13.6691 14.0764 14.4035L14.2321 14.7601C14.2957 14.9058 14.4396 15 14.5987 15H18.6747C19.7297 15 20.4057 13.8774 19.912 12.945L18.6686 10.5963C18.1487 9.61425 17.1285 9 16.0173 9Z"
                      fill="black"
                    />
                    <rect
                      opacity="0.3"
                      x="14"
                      y="4"
                      width="4"
                      height="4"
                      rx="2"
                      fill="black"
                    />
                    <path
                      d="M4.65486 14.8559C5.40389 13.1224 7.11161 12 9 12C10.8884 12 12.5961 13.1224 13.3451 14.8559L14.793 18.2067C15.3636 19.5271 14.3955 21 12.9571 21H5.04292C3.60453 21 2.63644 19.5271 3.20698 18.2067L4.65486 14.8559Z"
                      fill="black"
                    />
                    <rect
                      opacity="0.3"
                      x="6"
                      y="5"
                      width="6"
                      height="6"
                      rx="3"
                      fill="black"
                    />
                  </svg>
                </span>
              </button>
            </div>
          );
        },
      },
    ],
    [],
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    state,
    prepareRow,
    setGlobalFilter,
    preGlobalFilteredRows,
  } = useTable(
    {
      columns,
      data: akademi,
      defaultColumn: { Filter: DefaultFilterForColumn },
    },
    useFilters,
    useGlobalFilter,
    useSortBy,
  );
  let rowCounter = 1;
  return (
    <div>
      <Header />
      <SideNav />
      <div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1 style={{ textTransform: "capitalize" }}>
                            Loyo Sapto - 3524051004970001
                          </h1>
                        </div>
                      </div>

                      <div className="card-body">
                        <div className="row">
                          <div className="col-lg-6 mb-7 fv-row">
                            <label className="form-label">Test Subtansi</label>
                            <div className="d-flex">
                              <b className="text-success font-green-meadow">
                                Belum Mengerjakan
                              </b>
                            </div>
                          </div>
                          <div className="col-lg-6 mb-7 fv-row">
                            <label className="form-label">Survey</label>
                            <div className="d-flex">
                              <b className="text-primary font-green-meadow">
                                Survey Belum Tersedia
                              </b>
                            </div>
                          </div>
                          <div className="col-lg-6 mb-7 fv-row">
                            <label className="form-label">Administrasi</label>
                            <div className="d-flex">
                              <select
                                className="form-select form-select-sm selectpicker"
                                data-control="select2"
                                data-placeholder="Select an option"
                                disabled
                              >
                                <option selected>Verified</option>
                              </select>
                            </div>
                          </div>
                          <div className="col-lg-6 mb-7 fv-row">
                            <label className="form-label">Status Peserta</label>
                            <div className="d-flex">
                              <select
                                className="form-select form-select-sm selectpicker"
                                data-control="select2"
                                data-placeholder="Status Peserta"
                              >
                                <option selected>Verified</option>
                              </select>
                            </div>
                          </div>
                          <div className="col-lg-12 mb-7 card-toolbar">
                            <button
                              className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                              data-bs-toggle="modal"
                              data-bs-target="#filter"
                            >
                              <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  width="24"
                                  height="24"
                                  viewBox="0 0 24 24"
                                  fill="none"
                                >
                                  <path
                                    d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                                    fill="currentColor"
                                  />
                                </svg>
                              </span>
                              Batal
                            </button>
                            <button
                              className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                              data-bs-toggle="modal"
                              data-bs-target="#filter"
                            >
                              <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  width="24"
                                  height="24"
                                  viewBox="0 0 24 24"
                                  fill="none"
                                >
                                  <path
                                    d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                                    fill="currentColor"
                                  />
                                </svg>
                              </span>
                              Simpan
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1 style={{ textTransform: "capitalize" }}>
                            Pengingat Kelengkapan Berkas
                          </h1>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="row">
                          <div className="checkbox-inline">
                            <label className="checkbox">
                              <input
                                type="checkbox"
                                checked="checked"
                                name="Checkboxes3"
                              />
                              <span></span>
                              Profile
                            </label>
                            &nbsp;&nbsp;
                            <label className="checkbox">
                              <input type="checkbox" name="Checkboxes3" />
                              <span></span>
                              Riwayat Pelatihan
                            </label>
                            &nbsp;&nbsp;
                            <label className="checkbox">
                              <input type="checkbox" name="Checkboxes3" />
                              <span></span>
                              Berkas Pendaftaran
                            </label>
                            &nbsp;&nbsp;
                            <label className="checkbox">
                              <input type="checkbox" name="Checkboxes3" />
                              <span></span>
                              Dokumen Pendukung
                            </label>
                          </div>
                        </div>

                        <div className="card">
                          <div
                            className="stepper stepper-links d-flex flex-column"
                            id="kt_sub_rekap_pendaftaran_detail"
                          >
                            <div className="stepper-nav mb-5">
                              <div
                                className="stepper-item current"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <h2 className="stepper-title">Profile</h2>
                              </div>
                              <div
                                className="stepper-item"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <h3 className="stepper-title">
                                  Riwayat Pelatihan
                                </h3>
                              </div>
                              <div
                                className="stepper-item"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <h3 className="stepper-title">
                                  Berkas Pendaftaran
                                </h3>
                              </div>
                              <div
                                className="stepper-item"
                                data-kt-stepper-element="nav"
                                data-kt-stepper-action="step"
                              >
                                <h3 className="stepper-title">
                                  Dokumen Pendukung
                                </h3>
                              </div>
                            </div>
                            <div className="modal-body scroll-y">
                              <form
                                action="#"
                                id="kt_sub_rekap_pendaftaran_detail_form"
                              >
                                <div
                                  className="current"
                                  data-kt-stepper-element="content"
                                >
                                  <div className="card-body">
                                    <div className="row">
                                      <div className="card-title">
                                        <div className="card mb-12 mb-xl-8">
                                          <h2 className="me-3 mr-2">
                                            Data Pribadi
                                          </h2>
                                        </div>
                                      </div>
                                      <div className="row">
                                        <div className="col-lg-6 mb-7 fv-row">
                                          <label className="form-label">
                                            Nama Lengkap
                                          </label>
                                          <div className="d-flex">
                                            <b>{akademi.name}</b>
                                          </div>
                                        </div>
                                        <div className="col-lg-6 mb-7 fv-row">
                                          <label className="form-label">
                                            Email
                                          </label>
                                          <div className="d-flex">
                                            <b>{akademi.level_pelatihan}</b>
                                          </div>
                                        </div>

                                        <div className="col-lg-6 mb-7 fv-row">
                                          <label className="form-label">
                                            Nomor Identitas (KTP)
                                          </label>
                                          <div className="d-flex">
                                            <b>{akademi.akademi_id}</b>
                                          </div>
                                        </div>
                                        <div className="col-lg-6 mb-7 fv-row">
                                          <label className="form-label">
                                            Jenis Kelamin
                                          </label>
                                          <div className="d-flex">
                                            <b>{akademi.tema_id}</b>
                                          </div>
                                        </div>
                                        <div className="col-lg-6 mb-7 fv-row">
                                          <label className="form-label">
                                            No Handphone
                                          </label>
                                          <div className="d-flex">
                                            <b></b>
                                          </div>
                                        </div>
                                        <div className="col-lg-6 mb-7 fv-row">
                                          <label className="form-label">
                                            Pendidikan
                                          </label>
                                          <div className="d-flex">
                                            <b></b>
                                          </div>
                                        </div>
                                        <div className="col-lg-6 mb-7 fv-row">
                                          <label className="form-label">
                                            Nama Kontak Darurat
                                          </label>
                                          <div className="d-flex">
                                            <b>{akademi.silabus}</b>
                                          </div>
                                        </div>
                                        <div className="col-lg-6 mb-7 fv-row">
                                          <label className="form-label">
                                            Nomor Kontak Darurat
                                          </label>
                                          <div className="d-flex">
                                            <b>{akademi.metode_pelatihan}</b>
                                          </div>
                                        </div>
                                        <div className="col-lg-6 mb-7 fv-row">
                                          <label className="form-label">
                                            Tempat Lahir
                                          </label>
                                          <div className="d-flex">
                                            <b>{akademi.penyelenggara}</b>
                                          </div>
                                        </div>
                                        <div className="col-lg-6 mb-7 fv-row">
                                          <label className="form-label">
                                            Tanggal Lahir
                                          </label>
                                          <div className="d-flex">
                                            <b>{akademi.mitra}</b>
                                          </div>
                                        </div>

                                        <h4 className="my-5 d-flex align-items-center">
                                          Alamat
                                        </h4>
                                        <div className="col-lg-12 mb-7 fv-row">
                                          <label className="form-label">
                                            Alamat (Sesuai KTP)
                                          </label>
                                          <div className="d-flex">
                                            <b>{akademi.kuota_pendaftar}</b>
                                          </div>
                                        </div>
                                        <div className="col-lg-6 mb-7 fv-row">
                                          <label className="form-label">
                                            Provinsi
                                          </label>
                                          <div className="d-flex">
                                            <b>{akademi.kuota_peserta}</b>
                                          </div>
                                        </div>
                                        <div className="col-lg-6 mb-7 fv-row">
                                          <label className="form-label">
                                            Kota
                                          </label>
                                          <div className="d-flex">
                                            <b>{akademi.komitmen}</b>
                                          </div>
                                        </div>
                                        <div className="col-lg-6 mb-7 fv-row">
                                          <label className="form-label">
                                            Kecamatan
                                          </label>
                                          <div className="d-flex">
                                            <b>{akademi.lpj_peserta}</b>
                                          </div>
                                        </div>
                                        <div className="col-lg-12 mb-7 fv-row">
                                          <label className="form-label">
                                            Kode Pos
                                          </label>
                                          <div className="d-flex">
                                            <b>{akademi.sertifikasi}</b>
                                          </div>
                                        </div>
                                        <div className="col-lg-12 mb-7 fv-row">
                                          <label className="form-label">
                                            Alamat Domisili
                                          </label>
                                          <div className="d-flex">
                                            <b>{akademi.metode_pelatihan}</b>
                                          </div>
                                        </div>
                                        <div className="col-lg-6 mb-7 fv-row">
                                          <label className="form-label">
                                            Provinsi
                                          </label>
                                          <div className="d-flex">
                                            <b>{akademi.status_kuota}</b>
                                          </div>
                                        </div>
                                        <div className="col-lg-6 mb-7 fv-row">
                                          <label className="form-label">
                                            Kota
                                          </label>
                                          <div className="d-flex">
                                            <b></b>
                                          </div>
                                        </div>
                                        <div className="col-lg-6 mb-7 fv-row">
                                          <label className="form-label">
                                            Kecamatan
                                          </label>
                                          <div className="d-flex">
                                            <b>{akademi.zonasi}</b>
                                          </div>
                                        </div>
                                        <div className="col-lg-6 mb-7 fv-row">
                                          <label className="form-label">
                                            Kode Pos
                                          </label>
                                          <div className="d-flex">
                                            <b>{akademi.batch}</b>
                                          </div>
                                        </div>
                                        <h4 className="my-5 d-flex align-items-center">
                                          Pendidikan Terakhir
                                        </h4>
                                        <div className="col-lg-6 mb-7 fv-row">
                                          <label className="form-label">
                                            Jenjang Pendidikan
                                          </label>
                                          <div className="d-flex">
                                            <b>{akademi.alamat}</b>
                                          </div>
                                        </div>
                                        <h4 className="my-5 d-flex align-items-center">
                                          Pekerjaan
                                        </h4>
                                        <div className="col-lg-12 mb-7 fv-row">
                                          <label className="form-label required">
                                            Status Pekerjaan
                                          </label>
                                          <div className="d-flex">
                                            <b></b>
                                          </div>
                                        </div>
                                        <h4 className="my-5 d-flex align-items-center">
                                          Berkas
                                        </h4>
                                        <div className="col-lg-6 mb-7 fv-row">
                                          <label className="form-label required">
                                            Scan KTP
                                          </label>
                                          <div className="d-flex">
                                            <b></b>
                                          </div>
                                        </div>
                                        <div className="col-lg-6 mb-7 fv-row">
                                          <label className="form-label required">
                                            Scan Ijazah
                                          </label>
                                          <div className="d-flex">
                                            <b></b>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div data-kt-stepper-element="content">
                                  <div
                                    id="kt_content_container"
                                    className="container-xxl"
                                  >
                                    <div className="card-body">
                                      <div className="card-title">
                                        <div className="card mb-12 mb-xl-8">
                                          <h2 className="me-3 mr-2">
                                            Pelatihan
                                          </h2>
                                        </div>
                                      </div>
                                      <div className="card-body">
                                        <div className="row"></div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div data-kt-stepper-element="content">
                                  <div
                                    id="kt_content_container"
                                    className="container-xxl"
                                  >
                                    <div className="card-body">
                                      <div className="card-title">
                                        <div className="card mb-12 mb-xl-8">
                                          <h2 className="me-3 mr-2">
                                            Berkas Pendaftaran
                                          </h2>
                                        </div>
                                      </div>
                                      <div className="card-body">
                                        <div className="row">
                                          <div className="col-lg-12 mb-7 fv-row">
                                            <label className="form-label">
                                              Berkas Pendaftaran
                                            </label>
                                            <div className="d-flex">
                                              <b>OK</b>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div data-kt-stepper-element="content">
                                  <div
                                    id="kt_content_container"
                                    className="container-xxl"
                                  >
                                    <div className="card-body">
                                      <div className="card-title">
                                        <div className="card mb-12 mb-xl-8">
                                          <h2 className="me-3 mr-2">
                                            Form Komitmen
                                          </h2>
                                        </div>
                                      </div>
                                      <div className="card-body">
                                        <div className="row">
                                          <div className="col-lg-12 mb-7 fv-row">
                                            <label className="form-label">
                                              Komitmen Peserta
                                            </label>
                                            <div className="d-flex">
                                              <b>Ok</b>
                                            </div>
                                          </div>
                                          <h4 className="my-5 d-flex align-items-center">
                                            Telah Menyatakan Menyetujui dengan
                                            sebenarnya secara sadar dan tanpa
                                            paksaan
                                          </h4>
                                          <div className="col-lg-6 mb-7 fv-row">
                                            <label className="form-label">
                                              Tanggal Menyatakan
                                            </label>
                                            <div className="d-flex">
                                              <b>12 Mei 2022</b>
                                            </div>
                                          </div>
                                          <div className="col-lg-6 mb-7 fv-row">
                                            <label className="form-label">
                                              Waktu
                                            </label>
                                            <div className="d-flex">
                                              <b>14:13:5</b>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="text-center">
                                  <button
                                    className="btn btn-secondary btn-sm me-3 mr-2"
                                    data-kt-stepper-action="previous"
                                  >
                                    Sebelumnya
                                  </button>
                                  <button
                                    type="submit"
                                    className="btn btn-primary btn-sm"
                                    data-kt-stepper-action="submit"
                                  >
                                    Simpan
                                  </button>
                                  <button
                                    type="button"
                                    className="btn btn-warning btn-sm"
                                    data-kt-stepper-action="next"
                                  >
                                    Lanjutkan
                                  </button>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </div>

    // <Footer/>
  );
};

export default SubRekapPendaftaranDetail;
