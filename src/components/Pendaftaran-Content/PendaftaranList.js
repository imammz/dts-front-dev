import React, { useState, useEffect, useMemo, useRef } from "react";
import Pagination from "@material-ui/lab/Pagination";
import PendaftaranService from "../../service/PendaftaranService";
// import PendaftaranService from '../../service/PendaftaranService';
import { useTable, useSortBy } from "react-table";
import { GlobalFilter, DefaultFilterForColumn } from "../Filter";
import Header from "../Header";
import SideNav from "../SideNav";
import Footer from "../Footer";
import Select from "react-select";
import axios from "axios";
import { useHistory, useNavigate, useParams } from "react-router-dom";
import {
  useFilters,
  useGlobalFilter,
} from "react-table/dist/react-table.development";
import ReactPaginate from "react-paginate";
import DataTable from "react-data-table-component";

import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { capitalizeFirstLetter } from "../publikasi/helper";
import Cookies from "js-cookie";

const PendaftaranList = (props) => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  const [fRep, setfRep] = useState();
  const [Pendaftaran, setPendaftaran] = useState([]);
  const [searchTitle, setSearchTitle] = useState("");
  const [currentItems, setCurrentItems] = useState(null);
  const [loader, setLoader] = useState();
  const itemsPerPage = 10;
  const PendaftaranRef = useRef();
  PendaftaranRef.current = Pendaftaran;
  const [page, setPage] = useState(1);
  const [start, setStart] = useState(0);
  const [count, setCount] = useState(0);
  const [pageSize, setPageSize] = useState(10);
  // const inselect = ''
  const initialVal = [
    {
      label: "",
      value: "",
    },
  ];
  const [optionList, setOptioList] = useState(initialVal);
  const pageSizes = [10];
  const MySwal = withReactContent(Swal);
  const [searchQuery, setSearchQuery] = useState("");
  const [sort, setSort] = useState("id");
  const [sortVal, setSortVal] = useState("desc");
  const numberrow = useRef();

  const history = useNavigate();
  useEffect(() => {
    retrievePendaftaran();
    numberrow.current = page;
  }, [page, pageSize, sort, sortVal, searchQuery]);
  const getRequestParams = (searchTitle, page, pageSize) => {
    let params = {};

    if (searchTitle) {
      params["title"] = searchTitle;
    }

    if (page) {
      params["page"] = page - 1;
    }

    if (pageSize) {
      params["size"] = pageSize;
    }

    return params;
  };
  const onChangeSearchTitle = (e) => {
    console.log(e);
    const searchTitle = e.target.value;
    setSearchTitle(searchTitle);
  };
  const customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };
  const retrievePendaftaran = async () => {
    setLoader(true);
    //  alert(page); alert(pageSize);
    numberrow.current = page;
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/list-repo-v2",
        {
          start: start,
          length: pageSize,
          sort: sort,
          sort_val: sortVal.toUpperCase(),
          cari: searchQuery,
        },
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + Cookies.get("token"),
          },
        },
      )
      .then(function (response) {
        if (response.status === 200) {
          console.log(response);
          if (response.data.result.Status === true) {
            setPendaftaran(response.data.result.Data);
            setCount(response.data.result.Total);
            setLoader(false);
          } else {
            setPendaftaran([]);
            setCount([]);
            setLoader(false);
          }
        } else {
          setPendaftaran([]);
          setCount();
          setLoader(false);
        }
      })
      .catch((error) => {
        //   console.log(error);
        // console.log(error.result.Message);
        MySwal.fire({
          title: error.response.data.result.Message,
          icon: "warning",
          confirmButtonText: "Ok",
        }).then((result) => {
          if (result.isConfirmed) {
            // this.handleClickResetAction();
            setPendaftaran([]);
            setCount();
            setLoader(false);
          }
        });
      });
  };
  const retriveOption = async () => {
    const respon = axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/list-repo-v2",
        {
          start: "0",
          length: "50",
        },
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + Cookies.get("token"),
          },
        },
      )
      .then(function (response) {
        console.log("do");
        console.log(response);
        if (response.status === 200) {
          if (response.data.result.Status === true) {
            let repo = response.data.result.Data;
            let ui = [{}];
            const listItem = repo.map((number) =>
              ui.push({
                label: number.name,
                value: number.id,
              }),
            );
            setOptioList(ui);
            console.log(optionList);
          } else {
            setOptioList();
          }
        }
      });
  };
  const retrieveFind = async () => {
    const responsi = await axios
      .post(process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/cari-repo", {
        id: fRep,
      })
      .then(function (response) {
        if (response.status === 200) {
          console.log(response);
          if (response.data.result.Status === true) {
            console.log(response.data.result.Data);
            // console.log(response.Data.result.Data);
            setPendaftaran(response.data.result.Data);
            setCount(response.data.result.Data.length);
            retriveOption();
          } else {
            refreshList();
            retriveOption();
          }
        } else {
          refreshList();
          retriveOption();
        }
      })
      .catch((error) => {
        refreshList();
        retriveOption();
      });
  };
  const refreshList = () => {
    retrievePendaftaran();
  };
  const removeAllPendaftaran = () => {
    PendaftaranService.removeAll()
      .then((response) => {
        console.log(response);
        refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const deletePendaftaran = (rowIndex) => {
    const id = PendaftaranRef.current[rowIndex].id;
    PendaftaranService.remove(id)
      .then((response) => {
        window.location.href = "";
        let newPendaftaran = [...PendaftaranRef.current];
        newPendaftaran.splice(rowIndex, 1);
        setPendaftaran(newPendaftaran);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const filterChange = (value) => {
    setOptioList({ value: value });
  };
  const handleShow = (cell) => {
    window.location = "/pelatihan/element/edit/" + cell.id;
  };

  const handleShowPreview = (cell) => {
    window.location = "/pelatihan/element/preview/" + cell.id;
  };

  // const handlePageChange = (event, value) => {
  //     if(value===1){
  //         setPage(0);
  //       }else{
  //         setPage((pageSize * value) - 10);
  //       }
  //       retrievePendaftaran();
  //   };
  const handlePageChange = (page) => {
    handleReload(page, pageSize);
  };
  const handleSort = (column, sortDirection) => {
    let server_name = "";
    if (column.name == "Name Field") {
      server_name = "name";
    } else if (column.name == "Element") {
      server_name = "element";
    }
    setSort(server_name);
    setSortVal(sortDirection);
  };
  const handleReload = async (page, newPerPage) => {
    //          setPageSize(newPerPage);
    //          setPage(page);
    numberrow.current = page;

    //          alert(page);

    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - 10;
    }
    const respo = await axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/list-repo-v2",
        {
          start: start_tmp,
          length: length_tmp,
        },
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + Cookies.get("token"),
          },
        },
      )
      .then(function (response) {
        const { tutorials, totalPages } = response.data.result.Data;
        setPendaftaran(response.data.result.Data);
        setCount(response.data.result.Total);
        //   setCount(response.data.result.Total);
        setLoader(false);
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  const handlePerRowsChange = async (newPerPage, page) => {
    setLoader(true);
    setPageSize(newPerPage);
    setPage(page);
    numberrow.current = page;
  };

  const handlePageSizeChange = (event) => {
    setPageSize(event.target.value);
    setPage(0);
    numberrow.current = page;
  };

  const handleFilter = (value) => {
    setOptioList({ value: value });
    console.log(value.value);
    setfRep(value.value);
  };
  const handleTriger = (value, e) => {
    alert(value);
  };
  const handleSearch = () => {
    console.log(fRep);
    //   console.log(event.target);
    retrieveFind();
  };

  const handleShowDelete = (cell) => {
    console.log(cell);

    MySwal.fire({
      title: "Apakah anda yakin ?",
      text: "Akan menghapus Element " + cell.file_name + " ? ",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, hapus!",
      cancelButtonText: "Tidak",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/hapus-repo",
            {
              id: cell.id,
            },
          )
          .then(function (response) {
            console.log(response);
            if (response.status === 200) {
              MySwal.fire({
                title: <strong>Berhasil dihapus</strong>,
                html: "<i>" + cell.file_name + " berhasil dihapus </i>",
                icon: "success",
              });
              refreshList();
            }
          });
      }
    });
  };

  const cariForm = (e) => {
    const searchText = e.currentTarget.value;

    if (e.key === "Enter") {
      setSearchQuery(searchText);
    }
  };

  const handleChangeSearchAction = (e) => {
    const searchQuery = e.currentTarget.value;

    if (searchQuery == "") {
      setSearchQuery(searchQuery);
    }
  };

  const searchElement = (judul_element) => {
    setLoader(true);

    const params = getRequestParams(searchTitle, page, pageSize);
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/cari-repo-judul",
        {
          cari: judul_element,
        },
      )
      .then(function (response) {
        //  alert(page);
        //   alert(pageSize);

        if (response.status === 200) {
          if (response.data.result.Status === true) {
            setPendaftaran(response.data.result.Data);
            setCount(response.data.result.Total);
            setLoader(false);
          } else {
            setPendaftaran([]);
            setCount([]);
            setLoader(false);

            MySwal.fire({
              title: <strong> Tidak Ditemukan </strong>,
              html:
                "<span> " +
                judul_element +
                " tidak ditemukan pada form repositori </span>",
              icon: "warning",
            });
          }
        } else {
          setPendaftaran([]);
          setCount();
          setLoader(false);

          MySwal.fire({
            title: <strong> Tidak Ditemukan </strong>,
            html:
              "<span> " +
              judul_element +
              " tidak ditemukan pada form repositori </span>",
            icon: "warning",
          });
        }
      })
      .catch(function (error) {
        setPendaftaran([]);
        setCount();
        setLoader(false);

        MySwal.fire({
          title: <strong> Tidak Ditemukan </strong>,
          html:
            "<span> " +
            judul_element +
            " tidak ditemukan pada form repositori </span>",
          icon: "warning",
        });
      });
  };

  const columns = useMemo(
    () => [
      {
        Header: "ID",
        accessor: "id",
        className:
          "text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0",
        sortable: false,
        sortType: "basic",
        name: "No",
        width: "5%",
        style: {
          width: "8px !important",
          maxWidth: "8px !important",
        },
        cell: (row, index) => {
          let start_tmp = 0;
          if (
            numberrow.current != 0 &&
            numberrow.current != 1 &&
            numberrow.current != undefined
          ) {
            start_tmp = pageSize * numberrow.current;
            start_tmp = start_tmp - 10;
          }
          let number = start_tmp + index + 1;
          return <span> {number} </span>;
        },
        grow: 1,
        selector: (Pendaftaran) => Pendaftaran.id,
      },
      {
        Header: "Nama field",
        accessor: "name",
        className:
          "text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0",
        sortType: "basic",
        name: "Name Field",
        sortable: true,
        width: "33%",
        grow: 4,
        cell: (Pendaftaran) => {
          return (
            <div className="min-w-200px min-h-180px mw-200px mh-180px">
              <span
                className="badge badge-light-primary fs-7 m-1"
                title={Pendaftaran.name}
              >
                {Pendaftaran.name.substring(0, 50)}
                {Pendaftaran.name.length > 50 ? "..." : ""}
              </span>
            </div>
          );
        },
        selector: (Pendaftaran) => Pendaftaran.name,
      },

      {
        Header: "Keterangan",
        accessor: "Keterangan Element",
        sortType: "basic",
        style: { width: "400px !important" },
        name: "keterangan",
        sortable: false,
        width: "33%",
        grow: 10,
        cell: (Pendaftaran) => {
          return (
            <div className="min-w-250px min-h-180px mw-250px mh-180px mt-5">
              <p>
                <table style={{ width: "400px" }}>
                  <tr>
                    <td width={100}>Element</td>
                    <td>:</td>
                    <td>
                      <span className="badge badge-light-primary fs-7 m-1">
                        {" "}
                        {Pendaftaran.element}{" "}
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td width={100}>Caption</td>
                    <td>:</td>
                    <td>{capitalizeFirstLetter(Pendaftaran.span)}</td>
                  </tr>
                  <tr>
                    <td width={100}>Option</td>
                    <td>:</td>
                    <td>{Pendaftaran.option}</td>
                  </tr>
                  <tr>
                    <td width={100}>Data Option</td>
                    <td>:</td>
                    <td></td>
                  </tr>
                  <tr>
                    <td colspan="3">
                      {" "}
                      {Pendaftaran.element === "checkbox" ||
                      Pendaftaran.element === "select" ||
                      Pendaftaran.element === "radio" ||
                      Pendaftaran.element === "radiogroup" ||
                      Pendaftaran.element === "trigered" ? (
                        Pendaftaran.data_option.split(";").map((schema) => (
                          <span
                            className="badge badge-light-info fs-7 m-1"
                            title={schema}
                          >
                            {capitalizeFirstLetter(schema.substring(0, 46))}
                            {schema.length > 46 ? "..." : ""}
                          </span>
                        ))
                      ) : Pendaftaran.element == "uploadfiles" ||
                        Pendaftaran.element == "file-doc" ||
                        Pendaftaran.element == "fileimage" ? (
                        Pendaftaran.data_option.length > 1 ? (
                          <span>
                            <a
                              href={Pendaftaran.data_option}
                              target={"_blank"}
                              className="btn btn-outline-primary btn-sm mr-3"
                            >
                              <i className="icon-sm fas fa-download"></i> Unduh
                              Template{" "}
                            </a>
                          </span>
                        ) : (
                          <span></span>
                        )
                      ) : (
                        <span>
                          {Pendaftaran.data_option.substring(0, 50)}
                          {Pendaftaran.name.length > 50 ? "..." : ""}
                        </span>
                      )}
                    </td>
                  </tr>
                </table>
              </p>
            </div>
          );
        },
        selector: (Pendaftaran) => Pendaftaran.placeholder,
      },

      {
        Header: "Digunakan",
        accessor: "jml_pendaftaran",
        className:
          "text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0",
        sortType: "basic",
        name: "Digunakan",
        sortable: false,
        grow: 4,
        width: "17%",
        style: { width: "40px !important" },
        cell: (Pendaftaran) => {
          return (
            <div>
              <span className="badge badge-light-info fs-7 m-1">
                {Pendaftaran.jml_form}
              </span>{" "}
              Form Pendaftaran
              <br />
              <span className="badge badge-light-info fs-7 m-1">
                {Pendaftaran.jml_pelatihan}
              </span>{" "}
              Pelatihan
            </div>
          );
        },
        selector: (Pendaftaran) => Pendaftaran.jml_pendaftaran,
      },

      {
        Header: "Aksi",
        accessor: "actions",
        name: "Aksi",
        sortable: false,
        style: { width: "300px !important" },
        grow: 4,
        selector: (Pendaftaran) => {
          return (
            <div
              className="min-w-250px min-h-180px mw-250px mh-180px mt-5 row"
              style={{ width: "100%", paddingLeft: "10px" }}
            >
              {Pendaftaran.jml_pelatihan < 1 ? (
                <>
                  <a
                    title="Edit Form Repositoy"
                    className="btn btn-icon btn-warning btn-sm col-3"
                    style={{ marginLeft: "3px", marginTop: "3px" }}
                    onClick={() => handleShow(Pendaftaran)}
                  >
                    <i className="fas fa-edit action fs-5 mr-2"></i>
                  </a>
                  <span
                    title="Hapus Form Repositoy"
                    className="btn btn-icon btn-danger btn-sm col-3"
                    style={{ marginLeft: "3px", marginTop: "3px" }}
                    onClick={() => handleShowDelete(Pendaftaran)}
                  >
                    <i className="fas fa-trash-alt fs-5 mr-2"></i>
                  </span>
                </>
              ) : (
                <span></span>
              )}

              <a
                title="Preview Form Pendaftaran"
                className="btn btn-icon btn-primary btn-sm col-3"
                style={{ marginLeft: "3px", marginTop: "3px" }}
                onClick={() => handleShowPreview(Pendaftaran)}
              >
                <i className="fas fa-search action fs-5 mr-2"></i>
              </a>
            </div>
          );
        },
      },
    ],
    [],
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    state,
    prepareRow,
    setGlobalFilter,
    preGlobalFilteredRows,
  } = useTable(
    {
      columns,
      data: Pendaftaran,
      defaultColumn: { Filter: DefaultFilterForColumn },
    },
    useFilters,
    useGlobalFilter,
    useSortBy,
  );
  let rowCounter = 1;
  return (
    <div>
      <Header />
      <SideNav />
      <div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1 style={{ textTransform: "capitalize" }}>
                            List Repository
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <input
                            type="text"
                            className="form-control form-control-sm form-control-solid w-250px ps-14"
                            placeholder="Cari Element Repos"
                            onKeyPress={cariForm}
                            onChange={handleChangeSearchAction}
                          />
                          &nbsp;&nbsp;
                          {/* {let sta} */}
                          <a
                            href={
                              "/" +
                              urlSegmentZero +
                              "/" +
                              urlSegmenttOne +
                              "/add/old"
                            }
                            className="btn btn-light-primary"
                          >
                            <span className="svg-icon svg-icon-3">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width={24}
                                height={24}
                                viewBox="0 0 24 24"
                                fill="none"
                              >
                                <rect
                                  opacity="0.3"
                                  x={2}
                                  y={2}
                                  width={20}
                                  height={20}
                                  rx={5}
                                  fill="black"
                                />
                                <rect
                                  x="10.8891"
                                  y="17.8033"
                                  width={12}
                                  height={2}
                                  rx={1}
                                  transform="rotate(-90 10.8891 17.8033)"
                                  fill="black"
                                />
                                <rect
                                  x="6.01041"
                                  y="10.9247"
                                  width={12}
                                  height={2}
                                  rx={1}
                                  fill="black"
                                />
                              </svg>
                            </span>
                            Tambah {urlSegmenttOne}
                          </a>
                        </div>
                      </div>
                      <div className="card-body">
                        {/* <div className="table-responsive">
                                    <table className="table align-middle table-row-dashed fs-6" {...getTableProps()}>
                                        <thead  style={{ background:"rgb(243, 246, 249) none repeat scroll 0% 0%"}}>
                                        
                                        {headerGroups.map((headerGroup) => (
                                        <tr className="fw-bold fs-6 text-gray-800" {...headerGroup.getHeaderGroupProps()}>
                                            {headerGroup.headers.map((column) => (
                                                <th {...column.getHeaderProps(column.getSortByToggleProps())} style={{padding:"15px", fontSize:"small"}}>
                                                {column.render('Header')}
                                                {column.isSorted
                                                    ? column.isSortedDesc
                                                    ? ' 🔽'
                                                    : ' 🔼'
                                                    : ''}
                                            </th>
                                            ))}
                                        </tr>
                                        ))}
                                        </thead>
                                    <tbody {...getTableBodyProps()}>
                                        {rows.map((row, i) => {
                                        prepareRow(row);
                                        return (
                                            <tr {...row.getRowProps()} style={{ borderBottom: "1px solid #f3f6f9"}}>
                                            {row.cells.map((cell) => {
                                                return (
                                                <td {...cell.getCellProps()} style={{ textAlign: "center!important", padding:"10px"  }}>{cell.render("Cell")}</td>
                                                );
                                            })}
                                            </tr>
                                        );
                                        })}
                                    </tbody>
                                    </table>
                                </div>
                                <div className="d-flex justify-content-between align-items-center mt-7">
                                    <div className='pagination'>
                                    <Pagination
                                        className="page-item"
                                        color="primary"
                                        count={rows.length}
                                        page={page}
                                        siblingCount={0}
                                        rowsPerPageOptions={pageSize}
                                        rowsPerPage={pageSize}
                                        boundaryCount={2}
                                        variant="outlined"
                                        shape="rounded"
                                        onChange={handlePageChange}
                                        showFirstButton showLastButton
                                        
                                    />
                                    </div>
                                
                                    <p></p>
                                
                                </div> */}
                        <DataTable
                          columns={columns}
                          data={Pendaftaran}
                          progressPending={loader}
                          highlightOnHover
                          pointerOnHover
                          pagination
                          paginationServer
                          paginationTotalRows={count}
                          onChangeRowsPerPage={handlePerRowsChange}
                          onChangePage={handlePageChange}
                          customStyles={customStyles}
                          persistTableHead={true}
                          noDataComponent={
                            <div className="mt-5">Tidak Ada Data</div>
                          }
                          sortServer
                          onSort={handleSort}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </div>
  );
};

export default PendaftaranList;
