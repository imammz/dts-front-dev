import React from "react";

const PendaftaranContent = () => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  return (
    <div>
      <div
        className="wrapper d-flex flex-column flex-row-fluid"
        id="kt_wrapper"
        style={{ paddingTop: 8 }}
      >
        <div
          className="content d-flex flex-column flex-column-fluid"
          id="kt_content"
        >
          <div className="toolbar" id="kt_toolbar">
            <div
              id="kt_toolbar_container"
              className="container-fluid d-flex flex-stack"
            >
              <div
                data-kt-swapper="true"
                data-kt-swapper-mode="prepend"
                data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                className="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0"
              >
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-3 my-1">
                  {urlSegmentZero}
                </h1>
                <span className="h-20px border-gray-200 border-start mx-4" />
                <ul className="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                  <li className="breadcrumb-item text-muted">
                    <a
                      href="/dashboard/digitalent"
                      className="text-muted text-hover-primary"
                    >
                      {urlSegmentZero}
                    </a>
                  </li>
                  <li className="breadcrumb-item">
                    <span className="bullet bg-gray-200 w-5px h-2px" />
                  </li>
                  <li className="breadcrumb-item text-muted">
                    {urlSegmenttOne}
                  </li>
                  <li className="breadcrumb-item">
                    <span className="bullet bg-gray-200 w-5px h-2px" />
                  </li>
                </ul>
              </div>
              {/* <div className="d-flex align-items-center py-1">
                        
                        <a href="#" className="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_create_app" id="kt_toolbar_primary_button">Create</a>
                    </div> */}
            </div>
          </div>
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container-xxl">
              <div className="card card-flush">
                <div className="card-header mt-6">
                  <div className="card-title">
                    <div className="card mb-12 mb-xl-8">
                      <h3 className="card-title align-items-start flex-column">
                        <span className="card-label fw-bolder fs-3 mb-1">
                          Daftar Kandidat {urlSegmenttOne}
                        </span>
                        <span className="text-muted mt-6 fw-bold fs-7">
                          <div className="d-flex align-items-center position-relative my-1 me-6">
                            <span className="svg-icon svg-icon-1 position-absolute ms-6">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width={24}
                                height={24}
                                viewBox="0 0 24 24"
                                fill="none"
                              >
                                <rect
                                  opacity="0.5"
                                  x="17.0365"
                                  y="15.1223"
                                  width="8.15546"
                                  height={2}
                                  rx={1}
                                  transform="rotate(45 17.0365 15.1223)"
                                  fill="black"
                                />
                                <path
                                  d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                  fill="black"
                                />
                              </svg>
                            </span>
                            <input
                              type="text"
                              data-kt-permissions-table-filter="search"
                              className="form-control form-control-solid w-500px ps-15"
                              placeholder="Klik disini untuk pencarian ..."
                            />
                          </div>
                        </span>
                      </h3>
                    </div>
                    {/* <div className="card mb-5 mb-xl-8">
                            <div className="d-flex align-items-center position-relative my-1 me-6">
                            <span className="svg-icon svg-icon-1 position-absolute ms-6">
                                <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height={2} rx={1} transform="rotate(45 17.0365 15.1223)" fill="black" />
                                <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
                                </svg>
                            </span>
                            <input type="text" data-kt-permissions-table-filter="search" className="form-control form-control-solid w-250px ps-15" placeholder="Klik disini untuk pencarian ..." />
                            </div>
                        </div> */}
                  </div>
                  <div className="card-toolbar">
                    {/* <button type="button" className="btn btn-light-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_add_permission">
                          <span className="svg-icon svg-icon-3">
                            <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" fill="none">
                              <rect opacity="0.3" x={2} y={2} width={20} height={20} rx={5} fill="black" />
                              <rect x="10.8891" y="17.8033" width={12} height={2} rx={1} transform="rotate(-90 10.8891 17.8033)" fill="black" />
                              <rect x="6.01041" y="10.9247" width={12} height={2} rx={1} fill="black" />
                            </svg>
                          </span>
                          Add Permission</button> */}
                  </div>
                </div>
                <div className="card-body pt-0">
                  <table
                    className="table align-middle table-row-dashed fs-6 gy-5 mb-0"
                    id="kt_permissions_table"
                  >
                    <thead>
                      <tr className="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                        <th className="min-w-20px">No</th>
                        <th className="min-w-200px">ID Pendaftaran</th>
                        <th className="min-w-125px">Nama Form Pendaftaran</th>
                        <th className="min-w-250px">Status</th>
                        <th className="mi-w-20px">Aksi</th>
                      </tr>
                    </thead>
                    <tbody className="fw-bold text-gray-600">
                      <tr>
                        <td>1</td>
                        <td>235</td>
                        <td>Form TA Pelatihan TI</td>
                        <td>
                          <a
                            href="javascript::void(0);"
                            className="badge badge-light-danger fs-7 m-1"
                          >
                            Listed
                          </a>
                        </td>
                        <td>
                          <button
                            className="btn btn-icon btn-active-light-primary w-30px h-30px me-3"
                            data-bs-toggle="modal"
                            data-bs-target="#kt_modal_update_permission"
                          >
                            <span className="svg-icon svg-icon-3">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width={24}
                                height={24}
                                viewBox="0 0 24 24"
                                fill="none"
                              >
                                <path
                                  d="M17.5 11H6.5C4 11 2 9 2 6.5C2 4 4 2 6.5 2H17.5C20 2 22 4 22 6.5C22 9 20 11 17.5 11ZM15 6.5C15 7.9 16.1 9 17.5 9C18.9 9 20 7.9 20 6.5C20 5.1 18.9 4 17.5 4C16.1 4 15 5.1 15 6.5Z"
                                  fill="black"
                                />
                                <path
                                  opacity="0.3"
                                  d="M17.5 22H6.5C4 22 2 20 2 17.5C2 15 4 13 6.5 13H17.5C20 13 22 15 22 17.5C22 20 20 22 17.5 22ZM4 17.5C4 18.9 5.1 20 6.5 20C7.9 20 9 18.9 9 17.5C9 16.1 7.9 15 6.5 15C5.1 15 4 16.1 4 17.5Z"
                                  fill="black"
                                />
                              </svg>
                            </span>
                          </button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PendaftaranContent;
