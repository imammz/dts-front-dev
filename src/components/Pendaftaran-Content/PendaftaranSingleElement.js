import React, { useState, useEffect } from "react";
import Header from "../../components/Header";
import SideNav from "../../components/SideNav";
import Footer from "../../components/Footer";
import Swal from "sweetalert2";
import axios from "axios";
import withReactContent from "sweetalert2-react-content";
import { useParams, useNavigate } from "react-router-dom";
// import { useParams, useNavigate } from 'react-router-dom';

const PendaftaranSingleElement = (props) => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  let history = useNavigate();
  const MySwal = withReactContent(Swal);
  const initialPendataran = {
    name: "",
    element: "",
    size: "",
    option: "",
    data_option: "",
    placeholder: "-",
    min: "",
    max: "",
    uploadfiles: null,
    span: "",
  };
  const [Pendaftaran, setPendaftaran] = useState(initialPendataran);
  const [isDisabled, setDisabled] = useState(true);
  const [revDisabled, setRevDisabled] = useState(false);
  const [Referenced, setReference] = useState([]);
  const [visibleText, setVisible] = useState(false);
  const [uploadFiles, setUploadFiles] = useState(true);
  const [files, setFiles] = useState([]);
  // var blind = '';
  useEffect(() => {
    retrieveReferences();
  }, []);
  const retrieveReferences = () => {
    console.log("sats");
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/list-reference",
      )
      .then(function (response) {
        console.log(response);
        if (response.status === 200) {
          let repo = response.data.result;
          if (repo.Status === true) {
            setReference(repo.Data);
            console.log(Referenced);
          } else {
            setReference();
          }
        }
      })
      .catch(function (error) {
        setReference();
      });
  };
  const postSave = () => {};
  const onFileChange = (event) => {
    // console.log(event.target.file)
    setFiles({ uploadfiles: event.target.files[0] });
    setPendaftaran({ ...Pendaftaran, uploadfiles: event.target.files[0] });
  };
  const handleBack = () => {
    history("/" + urlSegmentZero + "/" + urlSegmenttOne);
  };
  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setPendaftaran({ ...Pendaftaran, [name]: value });
    if (event.target.name === "element") {
      if (
        event.target.value === "select" ||
        event.target.value === "checkbox" ||
        event.target.value === "radiogroup"
      ) {
        setDisabled(false);
        setRevDisabled(true);
      } else {
        setDisabled(true);

        if (
          event.target.value === "file" ||
          event.target.value === "datepicker" ||
          (event.target.value === "file-doc") |
            (event.target.value === "fileimage")
        ) {
          setRevDisabled(true);
        } else if (event.target.value === "uploadfiles") {
          setUploadFiles(false);
          setRevDisabled(true);
        } else {
          setUploadFiles(true);
          setRevDisabled(false);
        }
      }
    }
    if (event.target.name === "option") {
      if (event.target.value === "manual") {
        setVisible(false);
      } else {
        setVisible(true);
      }
    }
  };
  const handleInputSliderMin = (event) => {
    console.log(event.target.value);
    setPendaftaran({ ...Pendaftaran, min: event.target.value });
  };
  const handleInputSliderMax = (event) => {
    console.log(event.target.value);
    setPendaftaran({ ...Pendaftaran, max: event.target.value });
  };
  function uploadImage(e) {
    var file = e.target.files[0];
    let reader = new FileReader();
    reader.onload = (e) => {
      let image = e.target.result;
      console.log(image);
    };
    reader.readAsDataURL(file);
  }
  const savePendaftaran = () => {
    const categoryOptItems = [];
    // var myBlob = new Blob();
    // console.log(files.uploadfiles);
    // return false;
    if (Pendaftaran.uploadfiles === null || Pendaftaran.uploadfiles === "") {
      categoryOptItems.push({
        name: Pendaftaran.name,
        file_name: Pendaftaran.name.replace(" ", "_"),
        element: Pendaftaran.element,
        size: Pendaftaran.size,
        option: Pendaftaran.option,
        data_option: Pendaftaran.data_option,
        required: "",
        min: Pendaftaran.min,
        max: Pendaftaran.max,
        maxlength: Pendaftaran.max,
        className: "form-solid",
        placeholder: Pendaftaran.placeholder,
        span: Pendaftaran.span,
      });
    } else {
      categoryOptItems.push({
        name: Pendaftaran.name,
        file_name: Pendaftaran.name.replace(" ", "_"),
        element: Pendaftaran.element,
        size: Pendaftaran.size,
        option: Pendaftaran.option,
        data_option: Pendaftaran.uploadfiles,
        required: "",
        min: Pendaftaran.min,
        max: Pendaftaran.max,
        maxlength: Pendaftaran.max,
        className: "form-solid",
        placeholder: Pendaftaran.placeholder,
        span: Pendaftaran.span,
      });
    }
    // console.log(categoryOptItems);
    // return false;
    // var FormData = new FormData();
    axios.defaults.headers.post["Content-Type"] = "multipart/form-data";
    // const data = new FormData();
    // data.append("name", Pendaftaran.name);
    // data.append("file_name", Pendaftaran.name.replace(" ", "_"));
    // data.append("element", Pendaftaran.element);
    // data.append("size", Pendaftaran.size);
    // data.append("option", Pendaftaran.option);
    // data.append("data_option", files.uploadFiles);
    // data.append("required", '');
    // data.append("min", Pendaftaran.min);
    // data.append("max", Pendaftaran.max);
    // data.append("maxlength", Pendaftaran.max);
    // data.append("className", "form-solid");
    // data.append("placeholder" , Pendaftaran.placeholder);
    // data.append("span", Pendaftaran.span);
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/create-repo-loop",
        { categoryOptItems },
      )
      .then(function (response) {
        console.log(response);
        MySwal.fire({
          title: <strong>Information!</strong>,
          html: <i>{response.data.result.Message}</i>,
          icon: "success",
        });
        history("/repository/element");
      })
      .catch(function (error) {
        MySwal.fire({
          title: <strong>Information!</strong>,
          html: <i>{error.response.data.result.Message}</i>,
          icon: "warning",
        });
      });
  };
  function input_text(props) {
    return (
      <div>
        <div className="col-lg-12 form-group">
          <label className="fs-6 fw-bold form-label mb-5">
            <span className="required">Option</span>
          </label>
          <select
            className="form-control form-control-sm"
            placeholder="Pilih element"
            value={Pendaftaran.option}
            disabled={isDisabled}
          >
            <option>Select Reference</option>
            <option value="manual">Manual</option>
            <option value="referensi">Select Reference</option>
          </select>
        </div>
        <div className="col-lg-12 form-group">
          <label className="fs-6 fw-bold form-label mb-5">
            <span className="required">Data Option</span>
          </label>
          <select
            className="form-control form-control-sm"
            placeholder="Pilih element"
            value={Pendaftaran.data_option}
            disabled={isDisabled}
          >
            {/* <option>-- PILIH --</option>
                        {Reference.map((size) => (
                            <option value={size.id}>{size.name}</option>   
                            ))
                        }    */}
          </select>
        </div>
      </div>
    );
  }
  return (
    <div>
      <Header />
      <SideNav />
      <div
        className="wrapper d-flex flex-column flex-row-fluid pt-0"
        id="kt_wrapper"
      >
        <div
          className="content d-flex flex-column flex-column-fluid pt-0"
          id="kt_content"
        >
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container-xxl">
              <div className="row">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h2 style={{ textTransform: "uppercase" }}>
                          Buat {segment_url[1]}
                        </h2>
                      </div>
                      <div className="card-toolbar">
                        <button
                          className="btn btn-flex btn-warning btn-sm px-6"
                          data-bs-toggle="modal"
                          data-bs-target="#kt_modal_add_permission"
                        >
                          <i className="las la-quote-left"></i>
                          Harap Baca x
                        </button>
                        &nbsp;&nbsp;
                        {/* <button className="btn btn-primary btn-text-light btn-hover-text-success font-weight-bold btn-sm">
                                    <i className="las la-plus"></i> 
                                        Tambah Field
                                    </button>&nbsp;&nbsp; */}
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="col-lg-12 mb-7 fv-row">
                        <label className="fs-6 fw-bold form-label mb-5">
                          <span className="required">Nama Field</span>
                        </label>
                        <input
                          type="text"
                          className="form-control form-control-sm"
                          value={Pendaftaran.name}
                          onChange={handleInputChange}
                          placeholder="Masukan nama field "
                          name="name"
                          id="name"
                        ></input>
                      </div>
                      <div className="col-lg-12 mb-7 fv-row">
                        <label className="fs-6 fw-bold form-label mb-5">
                          <span className="required">Pilih Element</span>
                        </label>
                        <select
                          className="form-control form-control-sm selectpicker"
                          placeholder="Pilih element"
                          name="element"
                          id="element"
                          onChange={handleInputChange}
                          value={Pendaftaran.element}
                        >
                          <option>Pilih Element</option>
                          {/* <option value='select'>Select</option>
                                            <option value='checkbox'>Checkbox</option>
                                            <option value='text'>Text</option>
                                            <option value='textarea'>Text Area</option>
                                            <option value='radiogroup'>Radio</option>
                                            <option value='file'>File Image</option>
                                            <option value='datepicker'>Input Date</option>
                                            <option value='uploaddok'>File Document</option>
                                            <option value='uploadfiles'>Upload Document</option>
                                            <option value='uploadimage'>File Image</option> */}
                          <option value="select">Select</option>
                          <option value="checkbox">Checkbox</option>
                          <option value="text">Text</option>
                          <option value="textarea">Text Area</option>
                          <option value="radiogroup">Radio</option>
                          <option value="file">File Image</option>
                          <option value="datepicker">Input Date</option>
                          <option value="file-doc">File Document</option>
                          <option value="uploadfiles">Upload Document</option>
                          <option value="fileimage">File Image</option>
                        </select>
                      </div>
                      <div className="col-lg-12 mb-7 fv-row">
                        <label className="required">
                          <span>Ukuran form</span>
                        </label>
                        <select
                          className="form-control form-control-sm selectpicker"
                          name="size"
                          id="size"
                          value={Pendaftaran.size}
                          onChange={handleInputChange}
                          placeholder="--PILIH--"
                        >
                          <option value="">Pilih</option>
                          <option value="col-md-6">Half-Width</option>
                          <option value="col-md-12">Full-Width</option>
                        </select>
                      </div>
                      <div className="col-lg-12 mb-7 fv-row">
                        <label className="fs-6 fw-bold form-label mb-5">
                          <span className="required">Minimal Karakter</span>
                        </label>
                        <input
                          type="range"
                          min="1"
                          max="255"
                          onInput={handleInputSliderMin}
                          disabled={revDisabled}
                        ></input>
                        &nbsp;&nbsp;
                        <span>{Pendaftaran.min}</span>
                        {/* <input type="text" className="form-control-sm" value={Pendaftaran.min} onChange={handleInputChange} placeholder="Masukan minimal karakter" disabled name="min" id="min"></input> */}
                      </div>
                      <div className="col-lg-12 mb-7 fv-row">
                        <label className="fs-6 fw-bold form-label mb-5">
                          <span className="required">
                            Maksimal Panjang Karakter
                          </span>
                        </label>
                        <input
                          type="range"
                          min="1"
                          max="255"
                          onInput={handleInputSliderMax}
                          disabled={revDisabled}
                        ></input>
                        &nbsp;&nbsp;
                        <span>{Pendaftaran.max}</span>
                        {/* <input type="number" className="form-control-sm" value={Pendaftaran.max} onChange={handleInputChange} placeholder="Masukan panjang maksimal karakter" disabled name="max" id="max"></input> */}
                      </div>
                      <div className="col-lg-12 mb-7 fv-row">
                        <label className="fs-6 fw-bold form-label mb-5">
                          <span className="required">Placeholder</span>
                        </label>
                        <input
                          type="text"
                          className="form-control form-control-sm"
                          value={Pendaftaran.placeholder}
                          onChange={handleInputChange}
                          placeholder="Masukan placeholder"
                          disabled={revDisabled}
                          name="placeholder"
                          id="placeholder"
                        ></input>
                      </div>
                      <div className="col-lg-12 mb-7 fv-row">
                        <label className="fs-6 fw-bold form-label mb-5">
                          <span className="required">Keterangan Infromasi</span>
                        </label>
                        <input
                          type="text"
                          className="form-control form-control-sm"
                          value={Pendaftaran.span}
                          onChange={handleInputChange}
                          placeholder="Masukan keterangan infromasi element"
                          name="span"
                          id="span"
                        ></input>
                      </div>
                      <input_text />
                      <div className="col-lg-12 mb-7 fv-row">
                        <label className="fs-6 fw-bold form-label mb-5">
                          <span className="required">Option</span>
                        </label>
                        <select
                          className="form-control form-control-sm selectpicker"
                          placeholder="Pilih element"
                          name="option"
                          id="option"
                          value={Pendaftaran.option}
                          onChange={handleInputChange}
                          disabled={isDisabled}
                        >
                          <option>Pilih referensi</option>
                          <option value="manual">Manual</option>
                          <option value="referensi">Select Reference</option>
                        </select>
                      </div>
                      {visibleText === true ? (
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="fs-6 fw-bold form-label mb-5">
                            <span className="required">Data Option</span>
                          </label>
                          <select
                            className="form-control form-control-sm selectpicker"
                            placeholder="Pilih element"
                            name="data_option"
                            id="data_option"
                            value={Pendaftaran.data_option}
                            disabled={isDisabled}
                          >
                            <option>-- PILIH --</option>
                            {Referenced.map((size) => (
                              <option value={size.id}>{size.name}</option>
                            ))}
                          </select>
                        </div>
                      ) : (
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="fs-6 fw-bold form-label mb-5">
                            <span className="required">Data Option</span>
                          </label>
                          <input
                            type="text"
                            className="form-control form-control-sm"
                            value={Pendaftaran.data_option}
                            onChange={handleInputChange}
                            placeholder="data1;data2"
                            disabled={isDisabled}
                            name="data_option"
                            id="data_option"
                          ></input>
                        </div>
                      )}
                      <div className="col-lg-12 mb-7 fv-row">
                        <label className="fs-6 fw-bold form-label mb-5">
                          <span className="required">Upload File</span>
                        </label>
                        <input
                          type="file"
                          className="form-control form-control-sm font-size-h4"
                          name="uploadfiles"
                          accept="application/pdf"
                          id="uploadfiles"
                          onChange={onFileChange}
                          disabled={uploadFiles}
                        />
                      </div>
                      <div className="d-flex justify-content-end mb-7">
                        <button
                          type="reset"
                          className="btn btn-sm btn- btn-active-light-primary me-2"
                          onClick={handleBack}
                          data-kt-menu-dismiss="true"
                        >
                          Kembali
                        </button>
                        <button
                          onClick={savePendaftaran}
                          className="btn btn-primary btn-text-light btn-hover-text-success font-weight-bold btn-sm"
                        >
                          {" "}
                          <i className="fas fa-paper-plane action"></i>
                          Submit
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};
export default PendaftaranSingleElement;
