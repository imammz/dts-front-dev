import $ from "jquery";
import React, { createRef, useState, useEffect, useRef } from "react";
import ReactDOM from "react-dom";
import Header from "../../Header";
import SideNav from "../../SideNav";
// import "./styles.css";

const PendaftaranDataList = (props) => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  const formData = [
    {
      type: "header",
      subtype: "h1",
      label: "formBuilder in React",
    },
    {
      type: "paragraph",
      subtype: "p",
      label:
        "This is a demonstration of formBuilder running in a React project.",
    },
    {
      type: "header",
      subtype: "h1",
      label: "Header",
    },
    {
      type: "text",
      label: "Text Field",
      className: "form-control",
      name: "text-1649678768868",
      subtype: "text",
    },
    {
      type: "file",
      label: "File Upload",
      className: "form-control",
      name: "file-1649686416083",
      subtype: "file",
    },
    {
      type: "paragraph",
      subtype: "p",
      label: "Paragraph",
    },
  ];
  window.jQuery = $;
  window.$ = $;
  require("jquery-ui-sortable");
  require("formBuilder");

  const fb = useRef();
  useEffect(() => {
    $(fb.current).formBuilder({ formData });
  });

  // document.getElementById("saveData").addEventListener("click", () => {
  //     console.log("external save clicked");
  //     const result = $(fb.current).actions.save();
  //     console.log("result:", result);
  //   });
  // jQuery($ => {
  //     const fbEditor = document.getElementById("build-wrap");
  //     const formBuilder = $(fbEditor).formBuilder();

  //     document
  //         .getElementById("saveData")
  //         .addEventListener("click", () => formBuilder.actions.save());
  //     });

  return (
    <div>
      <Header />
      <SideNav />
      <div
        className="wrapper d-flex flex-column flex-row-fluid"
        id="kt_wrapper"
        style={{ paddingTop: 8 }}
      >
        <div
          className="content d-flex flex-column flex-column-fluid"
          id="kt_content"
        >
          <div className="toolbar" id="kt_toolbar">
            <div
              id="kt_toolbar_container"
              className="container-fluid d-flex flex-stack"
            >
              <div
                data-kt-swapper="true"
                data-kt-swapper-mode="prepend"
                data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                className="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0"
              >
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-3 my-1">
                  {urlSegmentZero}
                </h1>
                <span className="h-20px border-gray-200 border-start mx-4" />
                <ul className="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                  <li className="breadcrumb-item text-muted">
                    <a
                      href="../../demo1/dist/index.html"
                      className="text-muted text-hover-primary"
                    >
                      {urlSegmentZero}
                    </a>
                  </li>
                  <li className="breadcrumb-item">
                    <span className="bullet bg-gray-200 w-5px h-2px" />
                  </li>
                  <li className="breadcrumb-item text-muted">
                    {urlSegmenttOne}
                  </li>
                  <li className="breadcrumb-item">
                    <span className="bullet bg-gray-200 w-5px h-2px" />
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container">
              <div className="card card-custom card-stretch">
                <div className="card-header mt-12">
                  <div className="card-title">
                    <div className="card card-custom gutter-b">
                      <div className="card-header">
                        <div className="card-title">
                          <h3 className="card-label">
                            List Form Master Pendaftaran
                            <small></small>
                          </h3>
                        </div>
                      </div>
                      <div className="card-body">
                        <div id="fb-editor" ref={fb} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
  //   return <div id="fb-editor" ref={fb} />;
};

export default PendaftaranDataList;

// ReactDOM.render(<FormBuilder />, document.getElementById("root"));
