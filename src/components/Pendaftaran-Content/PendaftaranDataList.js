import React, { useState, useEffect, useRef } from "react";
import Header from "../../components/Header";
import SideNav from "../../components/SideNav";
import Footer from "../../components/Footer";
import axios from "axios";
import Swal from "sweetalert2";
import Board, { moveCard } from "@asseinfo/react-kanban";
import withReactContent from "sweetalert2-react-content";
import { useParams, useNavigate } from "react-router-dom";
import "../Pendaftaran-Content/sytle_kaban_una.css";
import "@asseinfo/react-kanban/dist/styles.css";
const PendaftaranDataList = (props) => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  const [RepoSize, setRepoSize] = useState({});
  const label = { inputProps: { "aria-label": "Switch demo" } };
  const [FilterRepoSize, setFilterRepoSize] = useState({});
  let history = useNavigate();
  let saur = "0";

  const initialPendaftaranState = {
    id: "0",
    judul: "",
  };
  const MySwal = withReactContent(Swal);
  const [Pendaftaran, setPendaftaran] = useState(initialPendaftaranState);
  const arr_push = [];
  // const [board, setBoard] = useState({});
  const retriveRepository = () => {
    axios
      .post(process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/list-repo", {
        start: "0",
        length: "50",
      })
      .then(function (response) {
        if (response.status === 200) {
          let repo = response.data.result;
          if (repo.Status === true) {
            let columns = [];
            const listItem = repo.Data.map((number) =>
              columns.push({
                id: number.id,
                title: number.name,
                element: number.element,
                maxlength: number.maxlength,
                placeholder: number.placeholder,
                min: number.min,
                max: number.max,
                required: "",
                size: number.size,
                option: number.option,
                data_option: number.data_option,
                className: number.className,
                html: number.html,
              }),
            );
            setRepoSize(columns);
            console.log(columns.length);
          } else {
            setRepoSize();
          }
        }
      })
      .catch(function (error) {
        setRepoSize();
      });
  };
  const retriveRepository_filter = () => {
    axios
      .post(process.env.REACT_APP_BASE_API_URI + "/cari-formbuilder", {
        id: Pendaftaran.id,
      })
      .then(function (response) {
        if (response.status === 200) {
          let repo = response.data.result;

          if (repo.Status === true) {
            let columns = [];
            const listItem = repo.Data.map((number) =>
              columns.push({
                id: number.id,
                title: number.name,
                element: number.element,
                maxlength: number.maxlength,
                placeholder: number.placeholder,
                min: number.min,
                max: number.max,
                required: "",
                size: number.size,
                option: number.option,
                data_option: number.data_option,
                className: number.className,
              }),
            );
            setFilterRepoSize(columns);
          } else {
            setFilterRepoSize({});
          }
        }
      })
      .catch(function (error) {
        setFilterRepoSize({});
      });
  };
  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setPendaftaran({ ...Pendaftaran, [name]: value, id: 0 });
  };
  const boardRepo = {
    columns: [
      {
        id: 1,
        title: "Repositories",
        cards: RepoSize,
      },
      {
        id: 2,
        title: "Form Element",
        cards: FilterRepoSize,
      },
    ],
  };
  useEffect(() => {
    retriveRepository();
    retriveRepository_filter();
  }, []);

  function ControlledBoard_style() {
    const initialValue = {
      id: "0",
    };
    const [initial, setInitial] = useState(initialValue);
    // You need to control the state yourself.
    const [controlledBoard, setBoard] = useState(boardRepo);
    const [statePos, setStatePos] = useState({ startX: 0, startScrollX: 0 });
    const sliderL = document.querySelector(".react-kanban-board");
    function handleCheck(e) {
      if (e.target.checked === true) {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/daftarpeserta/update-publish-required",
            {
              id_form: Pendaftaran.id,
              id_data_form: e.target.value,
              required: "required",
            },
          )
          .then(function (response) {
            axios
              .post(process.env.REACT_APP_BASE_API_URI + "/cari-formbuilder", {
                id: Pendaftaran.id,
              })
              .then(function (response) {
                console.log(response);
                if (response.status === 200) {
                  let repo = response.data.result;

                  if (repo.Status === true) {
                    let columns = [];
                    const listItem = repo.detail.map((number) =>
                      columns.push({
                        id: number.id,
                        title: number.name,
                        element: number.element,
                        maxlength: number.maxlength,
                        placeholder: number.placeholder,
                        min: number.min,
                        max: number.max,
                        required: number.required,
                        size: number.size,
                        option: number.option,
                        data_option: number.data_option,
                        className: number.className,
                        html: number.html,
                      }),
                    );
                    setPendaftaran({
                      ...Pendaftaran,
                      judul: repo.utama[0].judul_form,
                      id: repo.utama[0].id,
                    });
                    setFilterRepoSize(columns);
                  } else {
                    setFilterRepoSize();
                  }
                }
              })
              .catch(function (error) {
                setFilterRepoSize();
              });
          })
          .catch((error) => {
            alert(error.response.result.Message);
          });
      } else {
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/daftarpeserta/update-publish-required",
            {
              id_form: Pendaftaran.id,
              id_data_form: e.target.value,
              required: "",
            },
          )
          .then(function (response) {
            axios
              .post(process.env.REACT_APP_BASE_API_URI + "/cari-formbuilder", {
                id: Pendaftaran.id,
              })
              .then(function (response) {
                console.log(response);
                if (response.status === 200) {
                  let repo = response.data.result;

                  if (repo.Status === true) {
                    let columns = [];
                    const listItem = repo.detail.map((number) =>
                      columns.push({
                        id: number.id,
                        title: number.name,
                        element: number.element,
                        maxlength: number.maxlength,
                        placeholder: number.placeholder,
                        min: number.min,
                        max: number.max,
                        required: number.required,
                        size: number.size,
                        option: number.option,
                        data_option: number.data_option,
                        className: number.className,
                        html: number.html,
                      }),
                    );
                    setPendaftaran({
                      ...Pendaftaran,
                      judul: repo.utama[0].judul_form,
                      id: repo.utama[0].id,
                    });
                    setFilterRepoSize(columns);
                  } else {
                    setFilterRepoSize();
                  }
                }
              })
              .catch(function (error) {
                setFilterRepoSize();
              });
          })
          .catch((error) => {
            alert(error.response.result.Message);
          });
      }
    }
    function handleCardMove(_card, source, destination) {
      if (Pendaftaran.judul === "" || Pendaftaran.judul === undefined) {
        MySwal.fire({
          title: <strong>Information!</strong>,
          html: <i>Silahkan Masukan Judul Terlebih Dahulu</i>,
          icon: "warning",
        });
        return false;
      }
      let categoryOptItems = [];
      if (source.fromColumnId === 1) {
        var formData = new FormData();
        formData.append("judul_form", Pendaftaran.judul);
        formData.append("id_judul_form", Pendaftaran.id);
        formData.append("id_repository", _card.id);
        formData.append("id_lama", "0");
        axios.defaults.headers.common["Content-Type"] = "multipart/form-data;";
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/daftarpeserta/createjson-formbuildernew",
            formData,
          )
          .then(function (response) {
            console.log(response);
            if (response.status === 200) {
              saur = response.data.result.Data;
              let state_final = response.data.result;
              console.log(state_final);
              if (state_final.Status === true) {
                axios
                  .post(
                    process.env.REACT_APP_BASE_API_URI + "/cari-formbuilder",
                    {
                      id: state_final.Data,
                    },
                  )
                  .then(function (response) {
                    console.log(response);
                    if (response.status === 200) {
                      let repo = response.data.result;

                      if (repo.Status === true) {
                        let columns = [];
                        const listItem = repo.detail.map((number) =>
                          columns.push({
                            id: number.id,
                            title: number.name,
                            element: number.element,
                            maxlength: number.maxlength,
                            placeholder: number.placeholder,
                            min: number.min,
                            max: number.max,
                            required: number.required,
                            size: number.size,
                            option: number.option,
                            data_option: number.data_option,
                            className: number.className,
                            html: number.html,
                          }),
                        );
                        setPendaftaran({
                          ...Pendaftaran,
                          judul: repo.utama[0].judul_form,
                          id: repo.utama[0].id,
                        });
                        setFilterRepoSize(columns);
                        // console.log(columns.length);
                      } else {
                        setFilterRepoSize();
                      }
                    }
                  })
                  .catch(function (error) {
                    setFilterRepoSize();
                  });
                const updatedBoard = moveCard(
                  controlledBoard,
                  source,
                  destination,
                );
                saur = state_final.Data;
                setInitial(state_final.Data);
                setBoard(updatedBoard);
                window.removeEventListener("mousemove", handleMouseMove);
                window.removeEventListener("mouseup", handleMouseUp);
                window.removeEventListener("mousemove", handleMouseMove);
                window.removeEventListener("mouseup", handleMouseUp);
                window.removeEventListener("mousemove", handleMouseMove);
                window.removeEventListener("mouseup", handleMouseUp);
                window.removeEventListener("mousemove", handleMouseMove);
                window.removeEventListener("mouseup", handleMouseUp);
                window.removeEventListener("mousemove", handleMouseMove);
                window.removeEventListener("mouseup", handleMouseUp);
                return true;
              } else {
                MySwal.fire({
                  title: <strong>Information!</strong>,
                  html: <i>{response.data.result.Message}</i>,
                  icon: "info",
                });
                saur = state_final.Data;
                return false;
              }
            } else {
              MySwal.fire({
                title: <strong>Information!</strong>,
                html: <i>{response.statusText}</i>,
                icon: "warning",
              });
              return false;
            }
          })
          .catch(function (error) {
            MySwal.fire({
              title: <strong>Information!</strong>,
              html: <i>{error.response.data.result.Message}</i>,
              icon: "warning",
            });
            return false;
          });
      } else {
        var formData = new FormData();
        formData.append("id_form", Pendaftaran.id);
        formData.append("id_data_form", _card.id);
        axios.defaults.headers.common["Content-Type"] = "multipart/form-data;";
        axios.defaults.headers.common["Content-Type"] = "multipart/form-data;";
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI +
              "/daftarpeserta/delete-element-form",
            formData,
          )
          .then(function (response) {})
          .catch(function (error) {
            MySwal.fire({
              title: <strong>Information!</strong>,
              html: <i>{error.response.data.result.Message}</i>,
              icon: "warning",
            });
            return false;
          });
        console.log("bapus");
        const updatedBoard = moveCard(controlledBoard, source, destination);
        setBoard(updatedBoard);
        window.removeEventListener("mousemove", handleMouseMove);
        window.removeEventListener("mouseup", handleMouseUp);
        window.removeEventListener("mousemove", handleMouseMove);
        window.removeEventListener("mouseup", handleMouseUp);
        window.removeEventListener("mousemove", handleMouseMove);
        window.removeEventListener("mouseup", handleMouseUp);
        window.removeEventListener("mousemove", handleMouseMove);
        window.removeEventListener("mouseup", handleMouseUp);
        window.removeEventListener("mousemove", handleMouseMove);
        window.removeEventListener("mouseup", handleMouseUp);
        return true;
      }
    }
    function handleCardStart(e) {}

    function hasSomeParentTheClass(element, classname) {
      if (element.className.split(" ").indexOf(classname) >= 0) return true;
      return (
        element.parentNode &&
        hasSomeParentTheClass(element.parentNode, classname)
      );
    }

    var handleMouseDown = (e) => {
      const sliderL = document.querySelector(".react-kanban-board");
      const { target, clientX } = e;
      if (
        target.className !== "react-kanban-card" &&
        hasSomeParentTheClass(target.parentNode, "react-kanban-card") === false
      ) {
        return;
      }
      window.addEventListener("mousemove", handleMouseMove);
      window.addEventListener("mouseup", handleMouseUp);
      setStatePos({
        startX: clientX,
        startScrollX: window.scrollX,
      });
    };
    var handleMouseUp = () => {
      const window = document.querySelector(".react-kanban-board");
      window.removeEventListener("mousemove", handleMouseMove);
      window.removeEventListener("mouseup", handleMouseUp);
      if (statePos.startX) {
        setStatePos({ startX: null, startScrollX: null });
      }
    };

    var handleMouseMove = ({ clientX }) => {
      const sliderL = document.querySelector(".react-kanban-board");
      const { startX, startScrollX } = statePos;
      const scrollX = startScrollX - clientX + startX;
      window.scrollTo(scrollX, 0);
      const windowScrollX = window.scrollX;
      if (scrollX !== windowScrollX) {
        setStatePos({
          startX: clientX + windowScrollX - startScrollX,
          startScrollX: startScrollX,
        });
      }
    };

    return (
      <Board
        allowRenameColumn
        allowRemoveCard
        onLaneRemove={console.log}
        onCardRemove={console.log}
        onCardDragEnd={handleCardMove}
        disableColumnDrag
        direction="horizontal"
        removeItem
        onClickCapture={handleCardStart.bind(this)}
        renderCard={(args) => {
          return (
            <div className="react-kanban-card">
              <div className="row">
                <div className="col-md-12">
                  <div style={{ fontSize: 14 }}>
                    {(() => {
                      if (args.element === "text") {
                        return (
                          <>
                            <div
                              dangerouslySetInnerHTML={{ __html: args.html }}
                            ></div>
                            {args.required === "required" ? (
                              <div
                                className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                                style={{ marginTop: "6px" }}
                              >
                                <input
                                  className="form-check-input h-20px w-30px"
                                  type="checkbox"
                                  value={args.id}
                                  id="flexSwitch20x30"
                                  defaultChecked={true}
                                  onClick={handleCheck}
                                />
                                <label
                                  className="form-check-label"
                                  for="flexSwitch20x30"
                                >
                                  required {{ args }}
                                </label>
                              </div>
                            ) : (
                              <div
                                className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                                style={{ marginTop: "6px" }}
                              >
                                <input
                                  className="form-check-input h-20px w-30px"
                                  type="checkbox"
                                  defaultChecked={false}
                                  value={args.id}
                                  id="flexSwitch20x30"
                                  onClick={handleCheck}
                                />
                                <label
                                  className="form-check-label"
                                  for="flexSwitch20x30"
                                >
                                  required
                                </label>
                              </div>
                            )}
                          </>
                        );
                      } else if (args.element === "select") {
                        return (
                          <div>
                            <div
                              id="satus"
                              dangerouslySetInnerHTML={{ __html: args.html }}
                            ></div>
                            {args.required === "required" ? (
                              <div
                                className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                                style={{ marginTop: "6px" }}
                              >
                                <input
                                  className="form-check-input h-20px w-30px"
                                  type="checkbox"
                                  value={args.id}
                                  id="flexSwitch20x30"
                                  defaultChecked={true}
                                  onClick={handleCheck}
                                />
                                <label
                                  className="form-check-label"
                                  for="flexSwitch20x30"
                                >
                                  required
                                </label>
                              </div>
                            ) : (
                              <div
                                className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                                style={{ marginTop: "6px" }}
                              >
                                <input
                                  className="form-check-input h-20px w-30px"
                                  type="checkbox"
                                  defaultChecked={false}
                                  value={args.id}
                                  id="flexSwitch20x30"
                                  onClick={handleCheck}
                                />
                                <label
                                  className="form-check-label"
                                  for="flexSwitch20x30"
                                >
                                  required
                                </label>
                              </div>
                            )}
                          </div>
                        );
                      } else if (args.element === "checkbox") {
                        return (
                          <div className="form-group row">
                            <div
                              dangerouslySetInnerHTML={{ __html: args.html }}
                            ></div>
                            {args.required === "required" ? (
                              <div
                                className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                                style={{ marginTop: "6px" }}
                              >
                                <input
                                  className="form-check-input h-20px w-30px"
                                  type="checkbox"
                                  value={args.id}
                                  id="flexSwitch20x30"
                                  defaultChecked={true}
                                  onClick={handleCheck}
                                />
                                <label
                                  className="form-check-label"
                                  for="flexSwitch20x30"
                                >
                                  required
                                </label>
                              </div>
                            ) : (
                              <div
                                className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                                style={{ marginTop: "6px" }}
                              >
                                <input
                                  className="form-check-input h-20px w-30px"
                                  type="checkbox"
                                  defaultChecked={false}
                                  value={args.id}
                                  id="flexSwitch20x30"
                                  onClick={handleCheck}
                                />
                                <label
                                  className="form-check-label"
                                  for="flexSwitch20x30"
                                >
                                  required
                                </label>
                              </div>
                            )}
                          </div>
                        );
                      } else if (args.element === "radiogroup") {
                        return (
                          <div className="form-check form-check-custom form-check-solid form-check-sm">
                            <div
                              dangerouslySetInnerHTML={{ __html: args.html }}
                            ></div>

                            {args.required === "required" ? (
                              <div
                                className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                                style={{ marginTop: "6px" }}
                              >
                                <input
                                  className="form-check-input h-20px w-30px"
                                  type="checkbox"
                                  value={args.id}
                                  id="flexSwitch20x30"
                                  defaultChecked={true}
                                  onClick={handleCheck}
                                />
                                <label
                                  className="form-check-label"
                                  for="flexSwitch20x30"
                                >
                                  required
                                </label>
                              </div>
                            ) : (
                              <div
                                className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                                style={{ marginTop: "6px" }}
                              >
                                <input
                                  className="form-check-input h-20px w-30px"
                                  type="checkbox"
                                  defaultChecked={false}
                                  value={args.id}
                                  id="flexSwitch20x30"
                                  onClick={handleCheck}
                                />
                                <label
                                  className="form-check-label"
                                  for="flexSwitch20x30"
                                >
                                  required
                                </label>
                              </div>
                            )}
                          </div>
                        );
                      } else if (args.element === "datepicker") {
                        return (
                          <>
                            <div className="col-lg-6 mb-7 fv-row">
                              <div
                                dangerouslySetInnerHTML={{ __html: args.html }}
                              ></div>
                            </div>
                            {args.required === "required" ? (
                              <div
                                className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                                style={{ marginTop: "6px" }}
                              >
                                <input
                                  className="form-check-input h-20px w-30px"
                                  type="checkbox"
                                  value={args.id}
                                  id="flexSwitch20x30"
                                  defaultChecked={true}
                                  onClick={handleCheck}
                                />
                                <label
                                  className="form-check-label"
                                  for="flexSwitch20x30"
                                >
                                  required
                                </label>
                              </div>
                            ) : (
                              <div
                                className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                                style={{ marginTop: "6px" }}
                              >
                                <input
                                  className="form-check-input h-20px w-30px"
                                  type="checkbox"
                                  defaultChecked={false}
                                  value={args.id}
                                  id="flexSwitch20x30"
                                  onClick={handleCheck}
                                />
                                <label
                                  className="form-check-label"
                                  for="flexSwitch20x30"
                                >
                                  required
                                </label>
                              </div>
                            )}
                          </>
                        );
                      } else if (args.element === "file-doc") {
                        return (
                          <>
                            <div
                              dangerouslySetInnerHTML={{ __html: args.html }}
                            ></div>
                            {args.required === "required" ? (
                              <div
                                className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                                style={{ marginTop: "6px" }}
                              >
                                <input
                                  className="form-check-input h-20px w-30px"
                                  type="checkbox"
                                  value={args.id}
                                  id="flexSwitch20x30"
                                  defaultChecked={true}
                                  onClick={handleCheck}
                                />
                                <label
                                  className="form-check-label"
                                  for="flexSwitch20x30"
                                >
                                  required
                                </label>
                              </div>
                            ) : (
                              <div
                                className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                                style={{ marginTop: "6px" }}
                              >
                                <input
                                  className="form-check-input h-20px w-30px"
                                  type="checkbox"
                                  defaultChecked={false}
                                  value={args.id}
                                  id="flexSwitch20x30"
                                  onClick={handleCheck}
                                />
                                <label
                                  className="form-check-label"
                                  for="flexSwitch20x30"
                                >
                                  required
                                </label>
                              </div>
                            )}
                          </>
                        );
                      } else if (args.element === "file") {
                        return (
                          <>
                            <div
                              dangerouslySetInnerHTML={{ __html: args.html }}
                            ></div>
                            {args.required === "required" ? (
                              <div
                                className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                                style={{ marginTop: "6px" }}
                              >
                                <input
                                  className="form-check-input h-20px w-30px"
                                  type="checkbox"
                                  value={args.id}
                                  id="flexSwitch20x30"
                                  defaultChecked={true}
                                  onClick={handleCheck}
                                />
                                <label
                                  className="form-check-label"
                                  for="flexSwitch20x30"
                                >
                                  required
                                </label>
                              </div>
                            ) : (
                              <div
                                className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                                style={{ marginTop: "6px" }}
                              >
                                <input
                                  className="form-check-input h-20px w-30px"
                                  type="checkbox"
                                  defaultChecked={false}
                                  value={args.id}
                                  id="flexSwitch20x30"
                                  onClick={handleCheck}
                                />
                                <label
                                  className="form-check-label"
                                  for="flexSwitch20x30"
                                >
                                  required
                                </label>
                              </div>
                            )}
                          </>
                        );
                      } else if (args.element === "filemage") {
                        return (
                          <>
                            <div
                              dangerouslySetInnerHTML={{ __html: args.html }}
                            ></div>
                            {args.required === "required" ? (
                              <div
                                className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                                style={{ marginTop: "6px" }}
                              >
                                <input
                                  className="form-check-input h-20px w-30px"
                                  type="checkbox"
                                  value={args.id}
                                  id="flexSwitch20x30"
                                  defaultChecked={true}
                                  onClick={handleCheck}
                                />
                                <label
                                  className="form-check-label"
                                  for="flexSwitch20x30"
                                >
                                  required
                                </label>
                              </div>
                            ) : (
                              <div
                                className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                                style={{ marginTop: "6px" }}
                              >
                                <input
                                  className="form-check-input h-20px w-30px"
                                  type="checkbox"
                                  defaultChecked={false}
                                  value={args.id}
                                  id="flexSwitch20x30"
                                  onClick={handleCheck}
                                />
                                <label
                                  className="form-check-label"
                                  for="flexSwitch20x30"
                                >
                                  required
                                </label>
                              </div>
                            )}
                          </>
                        );
                      } else if (args.element === "uploadfiles") {
                        return (
                          <>
                            <div
                              dangerouslySetInnerHTML={{ __html: args.html }}
                            ></div>
                            {args.required === "required" ? (
                              <div
                                className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                                style={{ marginTop: "6px" }}
                              >
                                <input
                                  className="form-check-input h-20px w-30px"
                                  type="checkbox"
                                  value={args.id}
                                  id="flexSwitch20x30"
                                  defaultChecked={true}
                                  onClick={handleCheck}
                                />
                                <label
                                  className="form-check-label"
                                  for="flexSwitch20x30"
                                >
                                  required
                                </label>
                              </div>
                            ) : (
                              <div
                                className="form-check form-switch form-check-custom form-check-solid d-flex justify-content-end"
                                style={{ marginTop: "6px" }}
                              >
                                <input
                                  className="form-check-input h-20px w-30px"
                                  type="checkbox"
                                  defaultChecked={false}
                                  value={args.id}
                                  id="flexSwitch20x30"
                                  onClick={handleCheck}
                                />
                                <label
                                  className="form-check-label"
                                  for="flexSwitch20x30"
                                >
                                  required
                                </label>
                              </div>
                            )}
                          </>
                        );
                      }
                    })()}
                  </div>
                </div>
              </div>
            </div>
          );
        }}
      >
        {controlledBoard}
      </Board>
    );
  }

  const saveResult = (e) => {
    console.log(arr_push);
    console.log(Pendaftaran);
  };
  function ControlledBoard() {
    const [controlledBoard, setBoard] = useState(boardRepo);
    function handleCardMove(_card, source, destination) {
      const updatedBoard = moveCard(controlledBoard, source, destination);
      console.log(destination);
      console.log(updatedBoard);
      setBoard(...updatedBoard);
    }

    return (
      <Board
        allowRemoveLane
        allowRenameColumn
        allowRemoveCard
        onLaneRemove={console.log}
        onCardRemove={console.log}
        onLaneRename={console.log}
        allowAddCard={{ on: "top" }}
        onNewCardConfirm={(draftCard) => ({
          id: new Date().getTime(),
          ...draftCard,
        })}
        onCardDragEnd={console.log}
        // initialBoard={boardRepo}

        renderCard={(args) => {
          // console.log(args);
          return (
            <div
              className="react-kanban-card"

              // onClick={(e) => handleMouseDown(e)}
            >
              <div className="row">
                <div className="col-md-1">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    enableBackground="new 0 0 24 24"
                    viewBox="0 0 24 24"
                    fill="black"
                    width="30px"
                    height="35px"
                  >
                    <g>
                      <rect fill="none" height="24" width="24" />
                    </g>
                    <g>
                      <g>
                        <g>
                          <path d="M2.5,19h19v2h-19V19z M19.34,15.85c0.8,0.21,1.62-0.26,1.84-1.06c0.21-0.8-0.26-1.62-1.06-1.84l-5.31-1.42l-2.76-9.02 L10.12,2v8.28L5.15,8.95L4.22,6.63L2.77,6.24v5.17L19.34,15.85z" />
                        </g>
                      </g>
                    </g>
                  </svg>
                </div>
                <div className="col-md-6" style={{ marginLeft: 4 }}>
                  <strong>BA14 - T2</strong>
                  <div style={{ marginTop: -5, fontSize: 14 }}>
                    Expected 21:30
                  </div>
                </div>
                <div className="col-md-4">
                  <strong style={{ float: "right" }}>22:00</strong>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <span>Tesla Model 3</span>
                </div>
                <div className="col-md-6">
                  <strong className="yellow-text">LM20 STR</strong>
                </div>
                <div className="col-md-6">
                  <p
                    style={{
                      color: "purple",
                      marginBottom: 0,
                      marginTop: 4,
                      fontSize: 14,
                    }}
                  >
                    CONFIRMED
                  </p>
                </div>
              </div>
            </div>
          );
        }}
      >
        {boardRepo}
      </Board>

      //   <Board onCardDragEnd={handleCardMove} >
    );
  }
  function ControllerRepo() {
    // console.log(json);
    const [ctrlRepoBoard, setRepoBoard] = useState(RepoSize);
    function handleCardMove(_card, source, destination) {
      const updatedBoardx = moveCard(ctrlRepoBoard, source, destination);
      setRepoBoard(updatedBoardx);
      console.log(destination);
      console.log(updatedBoardx);
    }
    return (
      <Board onCardDragEnd={handleCardMove} disableColumnDrag>
        {ctrlRepoBoard}
      </Board>
    );
  }
  return (
    <div>
      <Header />
      <SideNav />
      <div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h4 style={{ textTransform: "uppercase" }}>
                            Daftar {segment_url[2]}
                          </h4>
                        </div>
                        <div className="card-toolbar">
                          <button
                            className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                            data-bs-toggle="modal"
                            data-bs-target="#filter"
                          >
                            <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                              >
                                <path
                                  d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                                  fill="currentColor"
                                />
                              </svg>
                            </span>
                            Filter
                          </button>
                          <div className="modal fade" tabindex="-1" id="filter">
                            <div className="modal-dialog modal-lg">
                              <div className="modal-content">
                                <div className="modal-header">
                                  <h5 className="modal-title">
                                    <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                      >
                                        <path
                                          d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                                          fill="currentColor"
                                        />
                                      </svg>
                                    </span>
                                    Filter
                                  </h5>
                                  <div
                                    className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                    data-bs-dismiss="modal"
                                    aria-label="Close"
                                  >
                                    <span className="svg-icon svg-icon-2x">
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                      >
                                        <rect
                                          opacity="0.5"
                                          x="6"
                                          y="17.3137"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(-45 6 17.3137)"
                                          fill="currentColor"
                                        />
                                        <rect
                                          x="7.41422"
                                          y="6"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(45 7.41422 6)"
                                          fill="currentColor"
                                        />
                                      </svg>
                                    </span>
                                  </div>
                                </div>
                                <div className="modal-body">
                                  <div className="row">
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label required">
                                        Nama Pelatihan
                                      </label>
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan nama pelatihan"
                                        name="name"
                                      />
                                    </div>
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label required">
                                        Pilih Akademi
                                      </label>
                                      <select
                                        className="form-select form-select-sm selectpicker"
                                        data-control="select2"
                                        data-placeholder="Select an option"
                                      >
                                        <option>Test</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                                <div className="modal-footer">
                                  <div className="d-flex justify-content-between">
                                    <button
                                      type="button"
                                      className="btn btn-sm btn-danger me-3"
                                    >
                                      Reset
                                    </button>
                                    <button
                                      type="button"
                                      className="btn btn-sm btn-primary"
                                    >
                                      Apply Filter
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <a
                            href="/pelatihan/pendaftaran"
                            className="btn btn-primary btn-sm"
                          >
                            <i className="las la-paper-plane"></i>
                            Simpan
                          </a>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="mb-10">
                          <label
                            for="exampleFormControlInput1"
                            className="required form-label"
                          >
                            Judul Form
                          </label>
                          <input
                            type="text"
                            id="judul"
                            name="judul"
                            value={Pendaftaran.judul}
                            className="form-control form-control-sm"
                            placeholder="Masukan Judul Form ...."
                            onChange={handleInputChange}
                          />
                        </div>
                        <div className="table-responsive">
                          <ControlledBoard_style />
                          <div className="d-flex justify-content-between align-items-right mt-7 pull-right">
                            {/* <button className='btn btn-primary btn-text-light btn-hover-text-success font-weight-bold btn-sm'>
                                  <i className="fas fa-paper-plane action"></i>
                                </button> */}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </div>
  );
};
export default PendaftaranDataList;
