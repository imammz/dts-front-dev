import React, { useState, useCallback, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import axios from "axios";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import Header from "./../../components/Header";
import Footer from "./../../components/Footer";
import SideNav from "./../../components/SideNav";

// import Dropzone, {useDropZone} from 'react-dropzone';
const DetailRekapPendaftaran = (props) => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  let history = useNavigate();
  const [submitted, setSubmitted] = useState(false);
  const MySwal = withReactContent(Swal);
  const { id } = useParams();
  const [akademi, setAkademi] = useState({});
  const [fp, setFp] = useState([{}]);
  const [fb, setFb] = useState([{}]);
  const retriveOptionAkademi = async () => {
    const respon = axios
      .post(
        process.env.REACT_APP_BASE_API_URI +
          "/pelatihan/cari-rekappelatihan-id",
        {
          id: id,
        },
      )
      .then(function (response) {
        console.log(response);
        if (response.status === 200) {
          if (response.data.result.Status === true) {
            console.log(response.data.result);
            setAkademi(response.data.result.data_pelatihan[0]);
            setFp(response.data.result.parameter[0]);
            setFb(response.data.result.form_pendaftaran);
          } else {
            // setAkademi();
            // setFb();
          }
        }
      })
      .catch((error) => {
        // setAkademi();
        // setFb();
      });
  };
  useEffect(() => {
    retriveOptionAkademi();
  }, []);

  return (
    <>
      <Header />
      <SideNav />
      <div
        className="wrapper d-flex flex-column flex-row-fluid pt-0"
        id="kt_wrapper"
      >
        <div
          className="d-flex flex-column flex-column-fluid pt-0"
          id="kt_content"
        >
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container-xxl">
              <div className="row">
                <div className="col-lg-12">
                  <div className="card">
                    <div
                      className="stepper stepper-links d-flex flex-column"
                      id="kt_rekap_pendaftaran"
                    >
                      <div className="stepper-nav mb-5">
                        <div
                          className="stepper-item current"
                          data-kt-stepper-element="nav"
                          data-kt-stepper-action="step"
                        >
                          <h2 className="stepper-title">Data Pelatihan</h2>
                        </div>
                        <div
                          className="stepper-item"
                          data-kt-stepper-element="nav"
                          data-kt-stepper-action="step"
                        >
                          <h3 className="stepper-title">Form Pendaftaran</h3>
                        </div>
                        <div
                          className="stepper-item"
                          data-kt-stepper-element="nav"
                          data-kt-stepper-action="step"
                        >
                          <h3 className="stepper-title">Form Komitmen</h3>
                        </div>
                        <div
                          className="stepper-item"
                          data-kt-stepper-element="nav"
                          data-kt-stepper-action="step"
                        >
                          <h3 className="stepper-title">Parameter</h3>
                        </div>
                      </div>
                      <div className="modal-body scroll-y">
                        <form action="#" id="kt_rekap_pendaftaran_form">
                          <div
                            className="current"
                            data-kt-stepper-element="content"
                          >
                            <div className="card-body">
                              <div className="row">
                                <div className="card-title">
                                  <div className="card mb-12 mb-xl-8">
                                    <h2 className="me-3 mr-2">
                                      Data Pelatihan
                                    </h2>
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label">
                                      Program Digital Talent Scholarship (DTS)
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.program_dts}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Nama Pelatihan
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.name}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Level Pelatihan
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.level_pelatihan}</b>
                                    </div>
                                  </div>

                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Akademi
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.akademi_id}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">Tema</label>
                                    <div className="d-flex">
                                      <b>{akademi.tema_id}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Logo Reference
                                    </label>
                                    <div className="d-flex">
                                      <b></b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Thumbnail
                                    </label>
                                    <div className="d-flex">
                                      <b></b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Silabus
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.silabus}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Metode Pelatihan
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.metode_pelatihan}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Penyelenggara
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.penyelenggara}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">Mitra</label>
                                    <div className="d-flex">
                                      <b>{akademi.mitra}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Tanggal Pendaftaran
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.pendaftaran_start} </b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Tanggal Pelatihan
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.pelatihan_start} </b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Deskripsi
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.deskripsi}</b>
                                    </div>
                                  </div>
                                  <h4 className="my-5 d-flex align-items-center">
                                    Kuota Pelatihan
                                  </h4>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Kuota Target Pendaftaran
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.kuota_pendaftar}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Kuota Target Peserta
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.kuota_peserta}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Komitmen Peserta
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.komitmen}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      LPJ Peserta
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.lpj_peserta}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label">
                                      Info Sertifikasi
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.sertifikasi}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Metode Pelatihan
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.metode_pelatihan}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Status Kuota
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.status_kuota}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Alur Pendaftaran
                                    </label>
                                    <div className="d-flex">
                                      <b></b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">Zonasi</label>
                                    <div className="d-flex">
                                      <b>{akademi.zonasi}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">Batch</label>
                                    <div className="d-flex">
                                      <b>{akademi.batch}</b>
                                    </div>
                                  </div>
                                  <h4 className="my-5 d-flex align-items-center">
                                    Alamat
                                  </h4>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">Alamat</label>
                                    <div className="d-flex">
                                      <b>{akademi.alamat}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Target Kuota Peserta
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.kuota_peserta}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Status Kuota
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.status_kuota}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Sertifikasi
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.sertifikasi}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      LPJ Peserta
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.lpj_peserta}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">Zonasi</label>
                                    <div className="d-flex">
                                      <b></b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">Batch</label>
                                    <div className="d-flex">
                                      <b>{akademi.batch}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Metode Pelatihan
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.metode_pelatihan}</b>
                                    </div>
                                  </div>
                                  <h4 className="my-5 d-flex align-items-center">
                                    Lokasi Pelatihan
                                  </h4>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Alamat
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.alamat}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label">
                                      Provinsi
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.provinsi}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label required">
                                      Kota / Kabupaten
                                    </label>
                                    <div className="d-flex">
                                      <b>{akademi.kabupaten}</b>
                                    </div>
                                  </div>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Disabilitas
                                    </label>
                                    <div className="d-flex">
                                      <b>
                                        {akademi.disabilitas == "1"
                                          ? "Umum"
                                          : akademi.tuna_netra == "1"
                                            ? ", Tuna Netra"
                                            : akademi.tuna_rungu == "1"
                                              ? ", Tuna Rungu"
                                              : akademi.tuna_daksa == "1"
                                                ? ", Tuna Daksa"
                                                : ""}
                                      </b>
                                    </div>
                                  </div>
                                  <h4 className="my-5 d-flex align-items-center">
                                    Status Pelatihan
                                  </h4>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label required">
                                      Status Publish
                                    </label>
                                    <div className="d-flex">
                                      <b></b>
                                    </div>
                                  </div>
                                  <div className="col-lg-6 mb-7 fv-row">
                                    <label className="form-label required">
                                      Link / Detail Pelatihan
                                    </label>
                                    <div className="d-flex">
                                      <b></b>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div data-kt-stepper-element="content">
                            <div
                              id="kt_content_container"
                              className="container-xxl"
                            >
                              <div className="card-body">
                                <div className="card-title">
                                  <div className="card mb-12 mb-xl-8">
                                    <h2 className="me-3 mr-2">
                                      Form Pendaftaran
                                    </h2>
                                  </div>
                                </div>
                                <div className="card-body">
                                  <div className="row">
                                    {/* { JSON.stringify(fb)} */}
                                    {fb.map((item, number) => (
                                      <>
                                        <div
                                          dangerouslySetInnerHTML={{
                                            __html: item.html,
                                          }}
                                        ></div>
                                      </>
                                    ))}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div data-kt-stepper-element="content">
                            <div
                              id="kt_content_container"
                              className="container-xxl"
                            >
                              <div className="card-body">
                                <div className="card-title">
                                  <div className="card mb-12 mb-xl-8">
                                    <h2 className="me-3 mr-2">Form Komitmen</h2>
                                  </div>
                                </div>
                                <div className="card-body">
                                  <div className="row">
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label">
                                        Komitmen Peserta
                                      </label>
                                      <div className="d-flex">
                                        <b>{akademi.komitmen_deskripsi}</b>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div data-kt-stepper-element="content">
                            <div
                              id="kt_content_container"
                              className="container-xxl"
                            >
                              <div className="card-body">
                                <div className="card-title">
                                  <div className="card mb-12 mb-xl-8">
                                    <h2 className="me-3 mr-2">Parameter</h2>
                                  </div>
                                </div>
                                <div className="card-body">
                                  <div className="row">
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Test Subtansi
                                      </label>
                                      <div className="d-flex">
                                        <b>{fp.test_substansi}</b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Tanggal
                                      </label>
                                      <div className="d-flex">
                                        <b>{fp.tgl_test_substansi}</b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Mid Test
                                      </label>
                                      <div className="d-flex">
                                        <b>{fp.mid_test}</b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Tanggal
                                      </label>
                                      <div className="d-flex">
                                        <b>{fp.tgl_mid_test}</b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Survey
                                      </label>
                                      <div className="d-flex">
                                        <b>{fp.survery}</b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Tanggal
                                      </label>
                                      <div className="d-flex">
                                        <b>{fp.tgl_survey}</b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Sertifikat
                                      </label>
                                      <div className="d-flex">
                                        <b>{fp.sertifikat}</b>
                                      </div>
                                    </div>
                                    <div className="col-lg-6 mb-7 fv-row">
                                      <label className="form-label">
                                        Tanggal
                                      </label>
                                      <div className="d-flex">
                                        <b>{fp.tgl_sertifikat}</b>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="text-center">
                            <button
                              className="btn btn-secondary btn-sm me-3 mr-2"
                              data-kt-stepper-action="previous"
                            >
                              Sebelumnya
                            </button>
                            <button
                              type="submit"
                              className="btn btn-primary btn-sm"
                              data-kt-stepper-action="submit"
                            >
                              Simpan
                            </button>
                            <button
                              type="button"
                              className="btn btn-warning btn-sm"
                              data-kt-stepper-action="next"
                            >
                              Lanjutkan
                            </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};
export default DetailRekapPendaftaran;
