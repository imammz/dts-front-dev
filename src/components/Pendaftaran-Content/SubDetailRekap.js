import React, { useState, useEffect, useMemo, useRef } from "react";
import AkademiService from "../../service/AkademiService";
import Pagination from "@material-ui/lab/Pagination";
import { useTable, useSortBy } from "react-table";
import { GlobalFilter, DefaultFilterForColumn } from "../Filter";
import Header from "../../components/Header";
import SideNav from "../../components/SideNav";
import Footer from "../../components/Footer";
import Select from "react-select";
import axios from "axios";
import { useHistory, useNavigate, useParams } from "react-router-dom";
import DataTable from "react-data-table-component";
import {
  useFilters,
  useGlobalFilter,
} from "react-table/dist/react-table.development";

const ListRekapPendaftaran = (props) => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  const [akademi, setAkademi] = useState([]);
  const [infoA, setInfoA] = useState([]);
  const [searchTitle, setSearchTitle] = useState("");
  const [page, setPage] = useState(0);
  const [loader, setLoader] = useState();
  const [count, setCount] = useState(0);
  const [pageSize, setPageSize] = useState(10);
  const pageSizes = [10];
  const [fRep, setfRep] = useState();
  const uppercase = {
    "text-transforms": "capitalize",
  };
  const initialVal = [
    {
      label: "",
      value: "",
    },
  ];
  const { id } = useParams();
  const [optionList, setOptioList] = useState(initialVal);
  const AkademiRef = useRef();
  AkademiRef.current = akademi;

  const history = useNavigate();
  useEffect(() => {
    retrieveAkademi();
    retriveOption();
  }, [page, pageSize]);

  const onChangeSearchTitle = (e) => {
    console.log(e);
    const searchTitle = e.target.value;
    setSearchTitle(searchTitle);
  };
  const retrieveAkademi = async () => {
    setLoader(true);
    const params = getRequestParams(searchTitle, page, pageSize);
    axios.defaults.headers.common["Authorization"] =
      "Bearer 19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp";
    const respo = await axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/r-detail-rekap-pendaftaran-byid",
        {
          mulai: page,
          limit: pageSize,
          id: id,
        },
      )
      .then(function (response) {
        const { tutorials, totalPages } = response.data.result.list_peserta;
        setAkademi(response.data.result.list_peserta);
        setInfoA(response.data.result.jadwal_detail[0]);
        setCount(response.data.result.length);
        setLoader(false);
      })
      .catch(function (error) {
        console.log(error);
        setLoader(false);
      });
  };
  const refreshList = () => {
    retrieveAkademi();
  };
  const handlePageChange = (event, value) => {
    if (value === 1) {
      setPage(0);
    } else {
      setPage(pageSize * value - 10);
    }
    console.log(pageSize);
    retrieveAkademi();
  };
  const handlePageSizeChange = (event) => {
    setPageSize(event.target.value);
    setPage(0);
  };
  const removeAllAkademi = () => {
    AkademiService.removeAll()
      .then((response) => {
        console.log(response);
        refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const deleteAkademi = (rowIndex) => {
    const id = AkademiRef.current[rowIndex].id;
    AkademiService.remove(id)
      .then((response) => {
        window.location.href = "";
        let newAkademi = [...AkademiRef.current];
        newAkademi.splice(rowIndex, 1);
        setAkademi(newAkademi);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const getRequestParams = (searchTitle, page, pageSize) => {
    let params = {};

    if (searchTitle) {
      params["title"] = searchTitle;
    }

    if (page) {
      params["page"] = page - 1;
    }

    if (pageSize) {
      params["size"] = pageSize;
    }

    return params;
  };
  const retriveOption = async () => {
    const respon = axios
      .post(process.env.REACT_APP_BASE_API_URI + "/akademi/list-akademi", {
        start: "0",
        length: "50",
      })
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.result.Status === true) {
            let repo = response.data.result.Data;
            let ui = [{}];
            const listItem = repo.map((number) =>
              ui.push({
                label: number.name,
                value: number.id,
              }),
            );
            setOptioList(ui);
            console.log(optionList);
          } else {
            setOptioList();
          }
        }
      });
  };
  const retrieveFind = async () => {
    const responsi = await axios
      .post(process.env.REACT_APP_BASE_API_URI + "/akademi/cari-akademi", {
        id: fRep,
      })
      .then(function (response) {
        if (response.status === 200) {
          console.log(response);
          if (response.data.result.Status === true) {
            setAkademi(response.data.result.Data);
            setCount(response.data.result.Data.length);
            retriveOption();
          } else {
            refreshList();
            retriveOption();
          }
        } else {
          refreshList();
          retriveOption();
        }
      })
      .catch((error) => {
        refreshList();
        retriveOption();
      });
  };
  const handleFilter = (value) => {
    setOptioList({ value: value });
    console.log(value.value);
    setfRep(value.value);
  };
  const handleSearch = () => {
    console.log(fRep);
    //   console.log(event.target);
    retrieveFind();
  };
  const handleShow = (cell) => {
    console.log(cell?.row?.original);
    console.log(cell?.row?.original.name);
    // history("/rekap/pendaftaran/view/detail/" + cell?.row?.original.id_pelatihan);
    history("/rekap/pendaftaran/view/detail/" + "1");
  };
  const handleShowPrev = (cell) => {
    console.log(cell?.row?.original);
    console.log(cell?.row?.original.name);
    history("/pelatihan/akademi/preview/" + cell?.row?.original.id);
  };
  const columns = useMemo(
    () => [
      {
        Header: "No",
        accessor: "",
        className: "min-w-30px min-h-30px mw-30px mh-30px",
        sortType: "basic",
        style: { width: "20px !important" },
        selector: (akademi) => {
          return (
            <span className="d-flex flex-column">
              <span className="fw-bolder">{akademi.id_pelatihan}</span>
            </span>
          );
        },
        name: "No",
        sortable: true,
        Cell: (props) => {
          return (
            <div
              className="min-w-40px min-h-40px mw-50px mh-40px"
              style={{ textAlign: "center" }}
            >
              <center>
                <span>{props.row.original.id_pelatihan}</span>
              </center>
            </div>
          );
        },
      },
      {
        Header: "Peserta",
        accessor: "",
        className: "min-w-80px min-h-80px mw-80px mh-80px",
        sortType: "basic",
        selector: (akademi) => {
          return (
            <span className="d-flex flex-column">
              <span className="fw-bolder">{akademi.id_slug}</span>
            </span>
          );
        },
        name: "No",
        sortable: true,
        Cell: (props) => {
          console.log(props);
          return (
            <div
              className="min-w-80px min-h-80px mw-80px mh-80px"
              style={{ textAlign: "center" }}
            >
              <center>
                <span>{props.row.original.id_slug}</span>
              </center>
            </div>
          );
        },
      },
      {
        Header: "Pelatihan Sebelumnya",
        accessor: "slug",
        className:
          "text-start text-gray-200 fw-bolder fs-7 text-uppercase gs-0",
        sortType: "basic",
        selector: (akademi) => {
          return (
            <div style={{ textAlign: "justify" }}>
              <h4>{akademi.pelatihan}</h4>
              <span>{akademi.penyelenggara}</span>
              <br />
              <span>{akademi.metode_pelatihan}</span>
            </div>
          );
        },
        name: "No",
        sortable: true,
        Cell: (props) => {
          return (
            <div style={{ textAlign: "justify" }}>
              <h4>{props.row.original.pelatihan}</h4>
              <span>{props.row.original.penyelenggara}</span>
              <br />
              <span>{props.row.original.metode_pelatihan}</span>
            </div>
          );
        },
      },
      {
        Header: "Test Subtansi",
        accessor: "pelatihan_start",
        className:
          "text-start text-gray-200 fw-bolder fs-7 text-uppercase gs-0",
        sortType: "basic",
        selector: (akademi) => {
          return (
            <div style={{ textAlign: "justify" }}>
              <span>
                {akademi.pendaftaran_start} - {akademi.pendaftaran_end}
              </span>
              <br />
              <span>
                {akademi.pelatihan_start} - {akademi.pelatihan_end}
              </span>
            </div>
          );
        },
        name: "No",
        sortable: true,
        Cell: (props) => {
          return (
            <div style={{ textAlign: "justify" }}>
              <span>
                {props.row.original.pendaftaran_start} -{" "}
                {props.row.original.pendaftaran_end}
              </span>
              <br />
              <span>
                {props.row.original.pelatihan_start} -{" "}
                {props.row.original.pelatihan_end}
              </span>
            </div>
          );
        },
      },
      {
        Header: "Berkas",
        accessor: "",
        className:
          "text-start text-gray-200 fw-bolder fs-7 text-uppercase gs-0",
        sortType: "basic",
        selector: (akademi) => {
          return (
            <div style={{ textAlign: "justify" }}>
              <span>
                {akademi.pendaftaran_start} - {akademi.pendaftaran_end}
              </span>
              <br />
              <span>
                {akademi.pelatihan_start} - {akademi.pelatihan_end}
              </span>
            </div>
          );
        },
        name: "No",
        sortable: true,
        Cell: (props) => {
          return (
            <div style={{ textAlign: "justify" }}>
              <span>
                {props.row.original.pendaftaran_start} -{" "}
                {props.row.original.pendaftaran_end}
              </span>
              <br />
              <span>
                {props.row.original.pelatihan_start} -{" "}
                {props.row.original.pelatihan_end}
              </span>
            </div>
          );
        },
      },
      {
        Header: "Status Peserta",
        accessor: "",
        className:
          "text-start text-gray-200 fw-bolder fs-7 text-uppercase gs-0",
        sortType: "basic",
        Cell: (props) => {
          return (
            <div style={{ textAlign: "justify" }}>
              <span>
                {props.row.original.pendaftaran_start} -{" "}
                {props.row.original.pendaftaran_end}
              </span>
              <br />
              <span>
                {props.row.original.pelatihan_start} -{" "}
                {props.row.original.pelatihan_end}
              </span>
            </div>
          );
        },
      },
      {
        Header: "Updated",
        accessor: "",
        className:
          "text-start text-gray-200 fw-bolder fs-7 text-uppercase gs-0",
        sortType: "basic",
        Cell: (props) => {
          return (
            <div style={{ textAlign: "justify" }}>
              <span>
                {props.row.original.pendaftaran_start} -{" "}
                {props.row.original.pendaftaran_end}
              </span>
              <br />
              <span>
                {props.row.original.pelatihan_start} -{" "}
                {props.row.original.pelatihan_end}
              </span>
            </div>
          );
        },
      },
      {
        Header: "Aksi",
        accessor: "actions",
        Cell: (props) => {
          const rowIdx = props.row.nama;
          return (
            <div>
              <button className="btn btn-icon btn-active-light-primary w-30px h-30px me-3">
                <span
                  onClick={() => handleShow(props)}
                  className="svg-icon svg-icon-3"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={24}
                    height={24}
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <path
                      d="M17.5 11H6.5C4 11 2 9 2 6.5C2 4 4 2 6.5 2H17.5C20 2 22 4 22 6.5C22 9 20 11 17.5 11ZM15 6.5C15 7.9 16.1 9 17.5 9C18.9 9 20 7.9 20 6.5C20 5.1 18.9 4 17.5 4C16.1 4 15 5.1 15 6.5Z"
                      fill="black"
                    />
                    <path
                      opacity="0.3"
                      d="M17.5 22H6.5C4 22 2 20 2 17.5C2 15 4 13 6.5 13H17.5C20 13 22 15 22 17.5C22 20 20 22 17.5 22ZM4 17.5C4 18.9 5.1 20 6.5 20C7.9 20 9 18.9 9 17.5C9 16.1 7.9 15 6.5 15C5.1 15 4 16.1 4 17.5Z"
                      fill="black"
                    />
                  </svg>
                </span>
              </button>
              <button
                className="btn btn-icon btn-active-light-primary w-30px h-30px me-3"
                onClick={() => handleShowPrev(props)}
              >
                <span className="svg-icon svg-icon-3">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <path
                      d="M16.0173 9H15.3945C14.2833 9 13.263 9.61425 12.7431 10.5963L12.154 11.7091C12.0645 11.8781 12.1072 12.0868 12.2559 12.2071L12.6402 12.5183C13.2631 13.0225 13.7556 13.6691 14.0764 14.4035L14.2321 14.7601C14.2957 14.9058 14.4396 15 14.5987 15H18.6747C19.7297 15 20.4057 13.8774 19.912 12.945L18.6686 10.5963C18.1487 9.61425 17.1285 9 16.0173 9Z"
                      fill="black"
                    />
                    <rect
                      opacity="0.3"
                      x="14"
                      y="4"
                      width="4"
                      height="4"
                      rx="2"
                      fill="black"
                    />
                    <path
                      d="M4.65486 14.8559C5.40389 13.1224 7.11161 12 9 12C10.8884 12 12.5961 13.1224 13.3451 14.8559L14.793 18.2067C15.3636 19.5271 14.3955 21 12.9571 21H5.04292C3.60453 21 2.63644 19.5271 3.20698 18.2067L4.65486 14.8559Z"
                      fill="black"
                    />
                    <rect
                      opacity="0.3"
                      x="6"
                      y="5"
                      width="6"
                      height="6"
                      rx="3"
                      fill="black"
                    />
                  </svg>
                </span>
              </button>
            </div>
          );
        },
      },
    ],
    [],
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    state,
    prepareRow,
    setGlobalFilter,
    preGlobalFilteredRows,
  } = useTable(
    {
      columns,
      data: akademi,
      defaultColumn: { Filter: DefaultFilterForColumn },
    },
    useFilters,
    useGlobalFilter,
    useSortBy,
  );
  let rowCounter = 1;
  return (
    <div>
      <Header />
      <SideNav />
      <div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-12 rounded-xl mb-7 ml-md-3 col-md">
                    <a href="#" className="card bg-primary hoverable">
                      <div className="card-body">
                        <span className="svg-icon svg-icon-muted svg-icon-2hx text-inverse-warning fs-2 mb-2 mt-5">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                          >
                            <path
                              d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z"
                              fill="black"
                            />
                            <rect
                              opacity="1.9"
                              x="8"
                              y="3"
                              width="8"
                              height="8"
                              rx="4"
                              fill="black"
                            />
                          </svg>
                          0
                        </span>
                        <div className="text-inverse-warning fs-5 mb-14 mt-5">
                          Pendaftar
                        </div>
                        {/* <div className="fw-bold text-inverse-warning fs-7">Pendaftar</div> */}
                      </div>
                    </a>
                  </div>
                  <div className="col-12 rounded-xl mb-7 ml-md-3 col-md">
                    <a href="#" className="card bg-success hoverable">
                      <div className="card-body">
                        <span className="svg-icon svg-icon-muted svg-icon-2hx text-inverse-warning fw-bolder">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                          >
                            <path
                              d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z"
                              fill="black"
                            />
                            <rect
                              opacity="1.9"
                              x="8"
                              y="3"
                              width="8"
                              height="8"
                              rx="4"
                              fill="black"
                            />
                          </svg>
                          0
                        </span>
                        <div className="text-inverse-warning fs-5 mb-14 mt-5">
                          Verified administrasi
                        </div>
                        {/* <div className="fw-bold text-inverse-warning fs-7">Pendaftar</div> */}
                      </div>
                    </a>
                  </div>
                  <div className="col-12 rounded-xl mb-7 ml-md-3 col-md">
                    <a href="#" className="card bg-info hoverable">
                      <div className="card-body">
                        <span className="svg-icon svg-icon-muted svg-icon-2hx text-inverse-warning fw-bolder">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                          >
                            <path
                              d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z"
                              fill="black"
                            />
                            <rect
                              opacity="1.9"
                              x="8"
                              y="3"
                              width="8"
                              height="8"
                              rx="4"
                              fill="black"
                            />
                          </svg>
                          0
                        </span>
                        <div className="text-inverse-warning fs-5 mb-14 mt-5">
                          Lulus Tes Subtansi
                        </div>
                        {/* <div className="fw-bold text-inverse-warning fs-7">Pendaftar</div> */}
                      </div>
                    </a>
                  </div>
                  <div className="col-12 rounded-xl mb-7 ml-md-3 col-md">
                    <a href="#" className="card bg-info hoverable">
                      <div className="card-body">
                        <span className="svg-icon svg-icon-muted svg-icon-2hx text-inverse-warning fw-bolder">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                          >
                            <path
                              d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z"
                              fill="black"
                            />
                            <rect
                              opacity="1.9"
                              x="8"
                              y="3"
                              width="8"
                              height="8"
                              rx="4"
                              fill="black"
                            />
                          </svg>
                          0
                        </span>
                        <div className="text-inverse-warning fs-5 mb-7 mt-5">
                          Verified Administrasi Lulus Tes Substansi
                        </div>
                        {/* <div className="fw-bold text-inverse-warning fs-7">Pendaftar</div> */}
                      </div>
                    </a>
                  </div>
                  <div className="col-12 rounded-xl mb-7 ml-md-3 col-md">
                    <a href="#" className="card bg-success hoverable">
                      <div className="card-body">
                        <span className="svg-icon svg-icon-muted svg-icon-2hx text-inverse-warning fw-bolder">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="24"
                            height="24"
                            viewBox="0 0 24 24"
                            fill="none"
                          >
                            <path
                              d="M6.28548 15.0861C7.34369 13.1814 9.35142 12 11.5304 12H12.4696C14.6486 12 16.6563 13.1814 17.7145 15.0861L19.3493 18.0287C20.0899 19.3618 19.1259 21 17.601 21H6.39903C4.87406 21 3.91012 19.3618 4.65071 18.0287L6.28548 15.0861Z"
                              fill="black"
                            />
                            <rect
                              opacity="1.9"
                              x="8"
                              y="3"
                              width="8"
                              height="8"
                              rx="4"
                              fill="black"
                            />
                          </svg>
                          0
                        </span>
                        <div className="text-inverse-warning fs-5 mb-14 mt-5">
                          Diterima
                        </div>
                        {/* <div className="fw-bold text-invers0-warning fs-7">Pendaftar</div> */}
                      </div>
                    </a>
                  </div>
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1 style={{ textTransform: "capitalize" }}>
                            List Rekap PendaftaranAkademi Majapahit / Tema
                            Majapahit / kerja batch 8
                          </h1>
                        </div>
                      </div>

                      <div className="card-body">
                        <div className="row">
                          <div className="col-lg-6 mb-7 fv-row">
                            <label className="form-label">
                              Kouta Pendaftar
                            </label>
                            <div className="d-flex">
                              <b>{infoA.kuota_pendaftar}</b>
                            </div>
                          </div>
                          <div className="col-lg-6 mb-7 fv-row">
                            <label className="form-label">Kuota Peserta</label>
                            <div className="d-flex">
                              <b>{infoA.kuota_peserta}</b>
                            </div>
                          </div>
                          <div className="col-lg-6 mb-7 fv-row">
                            <label className="form-label">
                              Jadwal Pendaftaran
                            </label>
                            <div className="d-flex">
                              <b>-</b>
                            </div>
                          </div>
                          <div className="col-lg-6 mb-7 fv-row">
                            <label className="form-label">
                              Jadwal Pelatihan
                            </label>
                            <div className="d-flex">
                              <b>-</b>
                            </div>
                          </div>
                          <div className="col-lg-6 mb-7 fv-row">
                            <label className="form-label">
                              Alur Pendaftaran
                            </label>
                            <div className="d-flex">
                              <b>{infoA.alur_pendaftaran}</b>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  {/* </div> */}
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1 style={{ textTransform: "capitalize" }}>
                            List Peserta
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          {/* <div> */}
                          <GlobalFilter
                            preGlobalFilteredRows={preGlobalFilteredRows}
                            globalFilter={state.globalFilter}
                            setGlobalFilter={setGlobalFilter}
                          />{" "}
                          &nbsp;&nbsp;&nbsp;
                          {/* </div>&nbsp;&nbsp;&nbsp; */}
                          <button
                            className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                            data-bs-toggle="modal"
                            data-bs-target="#filter"
                          >
                            <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                              >
                                <path
                                  d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                                  fill="currentColor"
                                />
                              </svg>
                            </span>
                            Filter
                          </button>
                          <a
                            href={
                              process.env.REACT_APP_BASE_API_URI +
                              "/unduh-rekap-pelatihan"
                            }
                          >
                            <button className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2">
                              {" "}
                              Export
                              <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  width="24"
                                  height="24"
                                  viewBox="0 0 24 24"
                                  fill="none"
                                >
                                  <rect
                                    opacity="0.5"
                                    x="11"
                                    y="18"
                                    width="13"
                                    height="2"
                                    rx="1"
                                    transform="rotate(-90 11 18)"
                                    fill="black"
                                  />
                                  <path
                                    d="M11.4343 15.4343L7.25 11.25C6.83579 10.8358 6.16421 10.8358 5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75L11.2929 18.2929C11.6834 18.6834 12.3166 18.6834 12.7071 18.2929L18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25C17.8358 10.8358 17.1642 10.8358 16.75 11.25L12.5657 15.4343C12.2533 15.7467 11.7467 15.7467 11.4343 15.4343Z"
                                    fill="black"
                                  />
                                </svg>
                              </span>
                            </button>
                          </a>
                          <div className="modal fade" tabindex="-1" id="filter">
                            <div className="modal-dialog modal-lg">
                              <div className="modal-content">
                                <div className="modal-header">
                                  <h5 className="modal-title">
                                    <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                      >
                                        <path
                                          d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                                          fill="currentColor"
                                        />
                                      </svg>
                                    </span>
                                    Filter
                                  </h5>
                                  <div
                                    className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                    data-bs-dismiss="modal"
                                    aria-label="Close"
                                  >
                                    <span className="svg-icon svg-icon-2x">
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                      >
                                        <rect
                                          opacity="0.5"
                                          x="6"
                                          y="17.3137"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(-45 6 17.3137)"
                                          fill="currentColor"
                                        />
                                        <rect
                                          x="7.41422"
                                          y="6"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(45 7.41422 6)"
                                          fill="currentColor"
                                        />
                                      </svg>
                                    </span>
                                  </div>
                                </div>
                                <div className="modal-body">
                                  <div className="row">
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label required">
                                        Nama Akademi
                                      </label>
                                      <Select
                                        name="level_pelatihan"
                                        placeholder="Silahkan pilih"
                                        noOptionsMessage={({ inputValue }) =>
                                          !inputValue
                                            ? optionList.value
                                            : "Data tidak tersedia"
                                        }
                                        value={optionList.value}
                                        className="form-select-sm selectpicker p-0"
                                        options={optionList}
                                        onChange={(value) =>
                                          handleFilter(value)
                                        }
                                      />
                                    </div>
                                  </div>
                                </div>
                                <div className="modal-footer">
                                  <div className="d-flex justify-content-between">
                                    <button
                                      type="button"
                                      className="btn btn-sm btn-danger me-3"
                                    >
                                      Reset
                                    </button>
                                    <button
                                      type="button"
                                      className="btn btn-sm btn-primary"
                                      onClick={handleSearch}
                                    >
                                      Apply Filter
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="form-control-sm">
                          {"Items per Page: "}
                          <select
                            onChange={handlePageSizeChange}
                            value={pageSize}
                            className="form-control-sm"
                          >
                            {pageSizes.map((size) => (
                              <option key={size} value={size}>
                                {size}
                              </option>
                            ))}
                          </select>
                        </div>
                        <div className="table-responsive">
                          <table
                            className="table align-middle table-row-dashed fs-6"
                            {...getTableProps()}
                          >
                            <thead
                              style={{
                                background:
                                  "rgb(243, 246, 249) none repeat scroll 0% 0%",
                              }}
                            >
                              {headerGroups.map((headerGroup) => (
                                <tr
                                  className="fw-bold fs-6 text-gray-800"
                                  {...headerGroup.getHeaderGroupProps()}
                                >
                                  {headerGroup.headers.map((column) => (
                                    <th
                                      {...column.getHeaderProps(
                                        column.getSortByToggleProps(),
                                      )}
                                      style={{ padding: "15px" }}
                                    >
                                      {column.render("Header")}
                                      {/* Add a sort direction indicator */}
                                      {/* <span style={{ textAlign: "center" }}> */}
                                      {column.isSorted
                                        ? column.isSortedDesc
                                          ? " 🔽"
                                          : " 🔼"
                                        : ""}
                                      {/* </span> */}
                                    </th>
                                  ))}
                                </tr>
                              ))}
                            </thead>
                            <tbody {...getTableBodyProps()}>
                              <tr>
                                <td>1</td>
                                <td>Loyo Saputra</td>
                                <td>Pelatihan Sebelumnya</td>
                                <td>Belum Mengerjakan</td>
                                <td>
                                  <span className="badge badge-light-danger fs-7 m-1">
                                    Verified
                                  </span>
                                </td>
                                <td>
                                  <span className="badge badge-light-success fs-7 m-1">
                                    Diterima
                                  </span>
                                </td>
                                <td>
                                  <p>
                                    <h4>Super Majapahit</h4>
                                    <span>12 Mei 2021</span>
                                    <br />
                                    <span>14.03.08</span>
                                  </p>
                                </td>
                                <td>
                                  <button className="btn btn-icon btn-active-light-primary w-30px h-30px me-3">
                                    <span
                                      onClick={() => handleShow(props)}
                                      className="svg-icon svg-icon-3"
                                    >
                                      {/* <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" fill="none">
                                      <path d="M17.5 11H6.5C4 11 2 9 2 6.5C2 4 4 2 6.5 2H17.5C20 2 22 4 22 6.5C22 9 20 11 17.5 11ZM15 6.5C15 7.9 16.1 9 17.5 9C18.9 9 20 7.9 20 6.5C20 5.1 18.9 4 17.5 4C16.1 4 15 5.1 15 6.5Z" fill="black" />
                                      <path opacity="0.3" d="M17.5 22H6.5C4 22 2 20 2 17.5C2 15 4 13 6.5 13H17.5C20 13 22 15 22 17.5C22 20 20 22 17.5 22ZM4 17.5C4 18.9 5.1 20 6.5 20C7.9 20 9 18.9 9 17.5C9 16.1 7.9 15 6.5 15C5.1 15 4 16.1 4 17.5Z" fill="black" />
                                    </svg> */}
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="16"
                                        height="16"
                                        fill="currentColor"
                                        className="bi bi-eye"
                                        viewBox="0 0 16 16"
                                      >
                                        <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z" />
                                        <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z" />
                                      </svg>
                                    </span>
                                  </button>
                                </td>
                              </tr>
                              {rows.map((row, i) => {
                                prepareRow(row);
                                return (
                                  <tr
                                    {...row.getRowProps()}
                                    style={{
                                      borderBottom: "1px solid #f3f6f9",
                                    }}
                                  >
                                    {row.cells.map((cell) => {
                                      return (
                                        <td
                                          {...cell.getCellProps()}
                                          style={{
                                            textAlign: "center!important",
                                            padding: "15px",
                                          }}
                                        >
                                          {cell.render("Cell")}
                                        </td>
                                      );
                                    })}
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                        </div>
                        <div className="d-flex justify-content-between align-items-center mt-7">
                          <div className="pagination">
                            <Pagination
                              className="page-item"
                              color="primary"
                              count={rows.length}
                              page={page}
                              siblingCount={0}
                              rowsPerPageOptions={pageSize}
                              rowsPerPage={pageSize}
                              boundaryCount={1}
                              variant="outlined"
                              shape="rounded"
                              onChange={handlePageChange}
                              showFirstButton
                              showLastButton
                            />
                          </div>

                          <p>{pageSize} data dari ....</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </div>

    // <Footer/>
  );
};

export default ListRekapPendaftaran;
