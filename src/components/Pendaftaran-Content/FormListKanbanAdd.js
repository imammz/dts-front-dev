import React, { useState, useEffect, useRef } from "react";
import Header from "../Header";
import SideNav from "../SideNav";
import Footer from "../Footer";
import axios from "axios";
import Swal from "sweetalert2";
import Board, { moveCard, addCard } from "@asseinfo/react-kanban";
import withReactContent from "sweetalert2-react-content";
import { useParams, useNavigate, withRouter } from "react-router-dom";
import "../Pendaftaran-Content/sytle_kaban_una.css";
import "@asseinfo/react-kanban/dist/styles.css";
import { capitalizeFirstLetter } from "../publikasi/helper";
import { loadTrigred_rekursive } from "../pelatihan/FormHelper";
import Cookies from "js-cookie";
import Select from "react-select";

import PelatihanPendaftaranContentAdd from "../pelatihan/Pelatihan/PendaftaranDataListKanban";

export default class FormListKanbanAdd extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      valuejudulform: "",
    };
  }

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  render() {
    return (
      <div>
        <Header />
        <SideNav />
        <div>
          <div
            className="wrapper d-flex flex-column flex-row-fluid pt-0"
            id="kt_wrapper"
          >
            <div
              className="content d-flex flex-column flex-column-fluid pt-0"
              id="kt_content"
            >
              <div className="post d-flex flex-column-fluid" id="kt_post">
                <div id="kt_content_container" className="container-xxl">
                  <div class="row">
                    <div className="col-lg-12 card-title">
                      <h4 style={{ textTransform: "uppercase" }}>
                        Tambah Form Pendaftaran
                      </h4>
                    </div>
                  </div>
                  <PelatihanPendaftaranContentAdd
                    props={null}
                    onSave={(data) => {
                      if (data.length) {
                        this.setState({
                          valuejudulform: {
                            label: null,
                            value: data[0].pid ?? "",
                          },
                          showBtnNextW2: true,
                        });
                      }
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
