import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import Header from "../../Header";
import SideNav from "../../SideNav";
import Footer from "../../Footer";
import axios from "axios";

const PendaftaranAkademiContent = (props) => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  let history = useNavigate();
  const initialPendaftaranAkademiContent = {};
  const [currentAkademi, setCurrentAkademi] = useState({});
  const [message, setMessage] = useState("");
  const { id } = useParams();
  const getPendaftaranAkademi = (id) => {
    axios
      .post(process.env.REACT_APP_BASE_API_URI + "/cari-formbuilder", {
        id: id,
      })
      .then(function (response) {
        console.log(response.data.result.detail);
        setCurrentAkademi(response.data.result);
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  useEffect(() => {
    getPendaftaranAkademi(id);
  }, []);

  return (
    <>
      <Header />
      <SideNav />
      <div
        className="wrapper d-flex flex-column flex-row-fluid"
        id="kt_wrapper"
        style={{ paddingTop: 8 }}
      >
        <div
          className="content d-flex flex-column flex-column-fluid"
          id="kt_content"
        >
          <div className="toolbar" id="kt_toolbar">
            <div
              id="kt_toolbar_container"
              className="container-fluid d-flex flex-stack"
            >
              <div
                data-kt-swapper="true"
                data-kt-swapper-mode="prepend"
                data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                className="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0"
              >
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-3 my-1">
                  {urlSegmentZero}
                </h1>
                <span className="h-20px border-gray-200 border-start mx-4" />
                <ul className="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                  <li className="breadcrumb-item text-muted">
                    <a
                      href="../../demo1/dist/index.html"
                      className="text-muted text-hover-primary"
                    >
                      {urlSegmentZero}
                    </a>
                  </li>
                  <li className="breadcrumb-item">
                    <span className="bullet bg-gray-200 w-5px h-2px" />
                  </li>
                  <li className="breadcrumb-item text-muted">
                    {urlSegmenttOne}
                  </li>
                  <li className="breadcrumb-item">
                    <span className="bullet bg-gray-200 w-5px h-2px" />
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container-xxl">
              <div className="card card-flush">
                <div className=" mt-6">
                  <div className="card-title">
                    <div className="card-body pt-0">
                      {JSON.stringify(currentAkademi)}
                      {/* {currentAkademi.detail.map((column) => {
                                      <>
                                        {JSON.stringify(column)}
                                      </>
                                    })} */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};
export default PendaftaranAkademiContent;
