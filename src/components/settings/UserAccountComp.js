import React, { useMemo, useState } from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import { handleFormatDate, capitalWord } from "./../pelatihan/Pelatihan/helper";
import moment from "moment";
import Select from "react-select";

const configTableAkademi = {
  columns: [
    {
      name: "Nama Akademi",
      // center: false,
      sortable: true,
      selector: (row) => row.nama_akademi,
    },
    {
      name: "Dibuat",
      sortable: true,
      selector: (row) => {
        const d = moment(row.created_at).locale("id");
        return d.isValid() ? d.format("D MMM YYYY, HH:mm:ss") : "-";
      },
    },
  ],
  data: [],
};

const configTablePelatihan = {
  columns: [
    {
      name: "Nama Pelatihan",
      // center: false,
      sortable: true,
      selector: (row) => row.nama_pelatihan,
    },
    {
      name: "Nama Akademi",
      // center: false,
      sortable: true,
      selector: (row) => row.nama_akademi,
    },
    {
      name: "Nama Tema",
      // center: false,
      sortable: true,
      selector: (row) => row.nama_tema,
    },
    {
      name: "Dibuat",
      // center: true,
      sortable: true,
      selector: (row) => {
        const d = moment(row.created_at).locale("id");
        return d.isValid() ? d.format("D MMM YYYY, HH:mm:ss") : "-";
      },
    },
  ],
  data: [],
};

const customLoader = () => (
  <div style={{ padding: "24px" }}>
    <img src="/assets/media/loader/loader-biru.gif" />
  </div>
);

const CustomList = ({ columns, data, mode = "table", chipPropName = "" }) => {
  // const [activeColumns,setActiveColumns] = useState()
  const [activePaging, setActivePaging] = useState({ page: 1, perPage: 10 });

  const activeColumns = useMemo(
    () => [
      {
        name: "No",
        center: true,
        width: "70px",
        cell: (row, index) => (
          <div>
            <span>
              {index + 1 + (activePaging.page - 1) * activePaging.perPage}
            </span>
          </div>
        ),
      },
      ...columns,
    ],
    [columns, activePaging],
  );

  const handlePageChange = (page) => {
    // console.log(page)
    setActivePaging((p) => ({ ...p, page: page }));
  };

  const handlePerRowChange = (perPage) => {
    // console.log(page)
    setActivePaging((p) => ({ ...p, perPage: perPage }));
  };

  return (
    <>
      {mode == "table" ? (
        <DataTable
          columns={activeColumns}
          data={data}
          progressComponent={customLoader}
          highlightOnHover
          pointerOnHover
          pagination
          onChangeRowsPerPage={handlePerRowChange}
          onChangePage={handlePageChange}
          defaultSortAsc={true}
          // persistTableHead={true}
          paginationPerPage={activePaging.perPage}
          // sortServer
          // paginationServer
          // paginationTotalRows={totalData}
          noDataComponent={<div className="mt-5">Tidak Ada Data</div>}
        ></DataTable>
      ) : mode == "chip" ? (
        <>
          {data?.length !== 0 ? (
            data.map((element, i) => (
              <span key={i} className="badge badge-secondary me-1 mb-1 ms-1">
                {element[chipPropName]}
              </span>
            ))
          ) : (
            <></>
          )}
        </>
      ) : (
        <></>
      )}
    </>
  );
};

export default class UserAccountComp extends React.Component {
  constructor(props) {
    super(props);

    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleChangeName = this.handleChangeNameAction.bind(this);
    this.handleChangeNIK = this.handleChangeNIKAction.bind(this);
    this.handleChangePassword = this.handleChangePasswordAction.bind(this);
    this.handleChangeConfPassword =
      this.handleChangeConfPasswordAction.bind(this);

    this.handleSubmit = this.handleSubmitAction.bind(this);
    this.back = this.backAction.bind(this);

    this.showConfirmPassword = this.showConfirmPassword.bind(this);
    this.handleSort = this.handleSortAction.bind(this);
  }

  state = {
    datax: [],
    dataxpdp: [],
    error_name: "",
    error_email: "",
    error_nik: "",
    error_password: "",
    error_confPassword: "",
    name: Cookies.get("user_name"),
    email: Cookies.get("user_email"),
    nik: Cookies.get("user_nik"),
    role_id: Cookies.get("role_id"),
    flag_nik: Cookies.get("user_flag_nik"),
    current_flag_nik: "",

    current_password: "",
    password: "",
    confPassword: "",

    submit: true,
    isRevealPwd: false,
    classConfirmEye: "fa fa-eye-slash",
    typeConfirmPassword: "password",
    typeCurrentPassword: "password",
    loading: false,
    totalRows: 0,
    newPerPage: 10,
    tempLastNumber: 0,
    currentPage: 0,
    column: "id",
    sortDirection: "asc",
    searchText: "",
    isRowChangeRef: false,

    data_user: [],
    data_role: [],
    data_satker: [],
    data_akademi: [],
    data_pelatihan: [],
    roles: [],
    satkers: [],
    akademis: [],
    pelatihans: [],
    last_login: "-",
    valSatker: [],
    valLevel: [],
    valRole: [],
    akademis: [],
    pelatihans: [],
  };

  option_satker = [];

  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  columns = [
    {
      name: "No",
      cell: (row, index) => this.state.tempLastNumber + index + 1,
      width: "70px",
      center: true,
    },
    {
      name: "Nama File",
      width: "300px",
      sortable: true,
      selector: (row) => row.filename,
    },
    {
      name: "Deskripsi",
      sortable: false,
      selector: (row) => capitalWord(row.jenis),
      width: "300px",
    },
    {
      name: "Created At",
      center: true,
      sortable: true,
      selector: (row) => (
        <span className="">
          {handleFormatDate(
            row.created_at,
            "DD MMMM YYYY, HH:mm:ss",
            "YYYY-MM-DD, HH:mm:ss",
          )}
        </span>
      ),
    },
    {
      name: "Aksi",
      center: true,
      width: "150px",
      cell: (row) => (
        <div>
          <a
            href="#"
            title="Unduh File"
            className="btn btn-icon btn-bg-success btn-sm me-1"
            onClick={() => {
              this.downloadFilePdp(row.filename);
            }}
          >
            <i class="bi bi-file-earmark-check-fill text-white"></i>
          </a>
        </div>
      ),
    },
  ];

  handleSortAction(column, sortDirection) {
    let server_name = "";
    if (column.name == "Nama File") {
      server_name = "filename";
    } else if (column.name == "Created At") {
      server_name = "created_at";
    }

    this.setState(
      {
        column: server_name,
        sortDirection: sortDirection,
      },
      () => {
        this.handleReload(1, this.state.newPerPage);
      },
    );
  }

  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    if (e.key == "Enter") {
      if (searchText == "") {
        this.handleReload();
      } else {
        this.setState({ loading: true, searchText: searchText }, () => {
          this.handleReload(1, this.state.newPerPage);
        });
      }
    }
  }
  downloadFilePdp(filename) {
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .get(process.env.REACT_APP_BASE_API_URI + "/rekappendaftaran/unduh-pdp", {
        params: {
          filename: filename,
        },
        responseType: "blob",
      })
      .then((resp) => {
        swal.close();
        // create file link in browser's memory
        const href = URL.createObjectURL(resp.data);

        // create "a" HTML element with href to file & click
        const link = document.createElement("a");
        link.href = href;
        link.setAttribute("download", filename); //or any other extension
        document.body.appendChild(link);
        link.click();

        // clean up "a" element & remove ObjectURL
        document.body.removeChild(link);
        URL.revokeObjectURL(href);
      })
      .catch((err) => {
        console.log(err);
        const messagex = err.response?.data?.Message ?? err.message;
        swal.fire({
          title: messagex ?? "Terjadi Kesalahan!",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      });
  }
  handleReload(page, newPerPage) {
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });
    const dataBody = {
      user_id: Cookies.get("user_id"),
      mulai: start_tmp,
      limit: length_tmp,
      param: this.state.searchText,
      sort: this.state.column,
      sort_val: this.state.sortDirection.toUpperCase(),
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/rekappendaftaran/list-unduh-ttd",
        dataBody,
        this.configs,
      )
      .then((res) => {
        const status = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (status) {
          const dataxpdp =
            res.data.result.Data && res.data.result.Data[0]
              ? []
              : res.data.result.result;
          this.setState({ dataxpdp });
          this.setState({ loading: false });
          this.setState({ totalRows: res.data.result.TotalLength });
          this.setState({ currentPage: page });
        } else {
          this.setState({ dataxpdp: [], loading: false });
          // swal
          //   .fire({
          //     title: messagex,
          //     icon: "warning",
          //     confirmButtonText: "Ok",
          //   })
          //   .then((result) => {
          //     if (result.isConfirmed) {
          //       // this.handleClickResetAction();
          //     }
          //   });
        }
      })
      .catch((error) => {
        console.log(error);
        let messagex = error.response?.data?.result?.Message;
        swal.fire({
          title: messagex ?? "Terjadi Kesalahan!",
          icon: "warning",
          confirmButtonText: "Ok",
        });
      });
  }

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    } else {
      this.handleReload();
      const data = { id: Cookies.get("user_id") };
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/get-user-admin",
          data,
          this.configs,
        )
        .then((res) => {
          swal.close();
          const statusx = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statusx) {
            const data_user = res.data.result.DataUser[0];
            const data_role = res.data.result.DataRelasiUser;
            const data_satker = res.data.result.DataSatker;
            const data_akademi = res.data.result.DataAkademi;
            const data_pelatihan = res.data.result.DataPelatihan;
            const d = moment(data_user.last_login).locale("id");
            const last_login = d.isValid()
              ? d.format("D MMM YYYY, HH:mm:ss")
              : "-";
            const roles = [...data_role];
            const satkers = [...data_satker];
            const akademis = [...data_akademi];
            const pelatihans = [...data_pelatihan];
            const valSatker = [];
            data_satker.forEach(function (element, i) {
              const satker = {
                value: element.unit_work_id,
                label: element.unit_work_name,
              };
              valSatker.push(satker);
            });
            const valAkademi = [];
            data_akademi.forEach(function (element, i) {
              const akademi = {
                value: element.academy_id,
                label: element.nama_akademi,
              };
              valAkademi.push(akademi);
            });
            if (data_akademi.length > 0) {
              this.setState({
                valLevel: { label: "Akademi", value: 1 },
                level: 1,
              });
            }
            const valPelatihan = [];
            if (data_pelatihan.length > 0) {
              data_pelatihan.forEach(function (element, i) {
                const pelatihan = {
                  value: element.training_id,
                  label: element.nama_pelatihan,
                };
                valPelatihan.push(pelatihan);
                const akademi = {
                  value: element.id_akademi,
                  label: element.nama_akademi,
                };
                let is_exist = false;
                valAkademi.forEach(function (elementAkademi, i) {
                  if (elementAkademi.value == element.id_akademi) {
                    is_exist = true;
                  }
                });
                if (!is_exist) {
                  valAkademi.push(akademi);
                }
              });

              this.setState({
                valLevel: { label: "Pelatihan", value: 2 },
                level: 2,
              });
            }
            const valRole = [];
            const context = this;
            data_role.forEach(function (element, i) {
              const role = {
                value: element.roles_id,
                label: element.role_name,
              };
              valRole.push(role);

              let akses_akademi = false;
              let arr_request = [];

              const id_role = element.roles_id;
              const dataBody = {
                mulai: 0,
                limit: 100,
                sort: "role_id asc",
                id_role: id_role,
              };
              const request = axios.post(
                process.env.REACT_APP_BASE_API_URI + "/detail-role",
                dataBody,
                context.configs,
              );
              arr_request.push(request);

              axios.all(arr_request).then(
                axios.spread((...responses) => {
                  responses.forEach(function (element) {
                    const detail = element.data.result.Detail;
                    detail.forEach(function (elementDetail) {
                      if (
                        elementDetail.permissions_id == 200 &&
                        (elementDetail.manage_role == 1 ||
                          elementDetail.publish_role == 1 ||
                          elementDetail.view_role == 1)
                      ) {
                        akses_akademi = true;
                      }
                    });
                  });
                  context.setState({
                    show_akademi: akses_akademi,
                  });
                }),
              );
            });

            this.setState({
              data_user,
              data_role,
              data_satker,
              data_akademi,
              data_pelatihan,
              roles,
              satkers,
              akademis,
              pelatihans,
              last_login,
              valSatker,
              valAkademi,
              valPelatihan,
              valRole,
            });
          }
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    }
  }

  customLoader = (
    <div style={{ padding: "24px" }}>
      <img src="/assets/media/loader/loader-biru.gif" />
    </div>
  );
  showConfirmPassword() {
    if (this.state.typeConfirmPassword == "password") {
      this.setState({
        typeConfirmPassword: "text",
        classConfirmEye: "fa fa-eye",
      });
    } else {
      this.setState({
        typeConfirmPassword: "password",
        classConfirmEye: "fa fa-eye-slash",
      });
    }
  }

  backAction = () => {
    // Cookies.remove("partner_id");
    // Cookies.remove("token");
    // Cookies.remove("user_id");
    // Cookies.remove("user_email");
    // Cookies.remove("user_name");
    // Cookies.remove("user_role");
    // Cookies.remove("menus");
    // Cookies.remove("role_id");
    window.location.href = localStorage.getItem("back_url");
  };

  handleChangeNameAction = (e) => {
    this.setState({ name: e.target.value, submit: true });

    if (e.target.value.length > 2) {
      this.setState({ error_name: "" });
    } else {
      this.setState({
        error_name: "*tidak boleh kosong dan terlalu singkat",
        submit: false,
      });
    }
  };

  handleChangeNIKAction = (e) => {
    this.setState({ nik: e.target.value, current_flag_nik: 0 });

    if (e.target.value.length === 16) {
      this.setState({ error_nik: "", submit: true });
    } else {
      this.setState({ error_nik: "*NIK Harus 16 digit" });
    }
  };

  handlePerRowsChange = async (arg1, arg2, srcEvent) => {
    if (srcEvent == "page-change") {
      this.setState({ loading: true }, () => {
        if (!this.state.isRowChangeRef) {
          this.handleReload(arg1, this.state.newPerPage);
        }
      });
    } else if (srcEvent == "row-change") {
      this.setState({ isRowChangeRef: true }, () => {
        this.handleReload(arg2, arg1);
      });
      this.setState({ loading: true, newPerPage: arg1 }, () => {
        this.setState({ isRowChangeRef: false });
      });
    }
  };

  handleChangePasswordAction = (e) => {
    this.setState({ password: e.target.value });

    if (e.target.value.length > 7 || e.target.value.length == 0) {
      this.setState({ error_password: "", submit: true });
    } else {
      this.setState({
        error_password: "*Password minimal 8 digit",
        submit: false,
      });
    }
  };

  handleChangeConfPasswordAction = (e) => {
    this.setState({ confPassword: e.target.value });

    if (e.target.value === this.state.password) {
      this.setState({ error_confPassword: "", submit: true });
    } else {
      this.setState({
        error_confPassword: "*Konfirmasi password tidak sama",
        submit: false,
      });
    }
  };

  handleSubmitAction = () => {
    let url = "/ubah-user-admin_profile";
    let formData = {
      name: Cookies.get("user_name"),
      nik: Cookies.get("user_nik"),
      email: Cookies.get("user_email"),
      current_password: this.state.current_password,
      password: this.state.password,
    };

    if (this.state.password.length > 0 && this.state.password.length < 8) {
      swal
        .fire({
          title: "Password  tidak boleh kosong atau terlalu singkat",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          this.setState({ datax: [] });
          this.setState({ loading: false });
          if (result.isConfirmed) {
          }
        });

      return;
    }

    if (this.state.password !== this.state.confPassword) {
      swal
        .fire({
          title: "Konfirmasi Password  tidak Sama ",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          this.setState({ datax: [] });
          this.setState({ loading: false });
          if (result.isConfirmed) {
          }
        });

      return;
    }

    console.log(formData);

    if (this.state.name.length < 3) {
      swal
        .fire({
          title: "Nama  tidak boleh kosong atau terlalu singkat",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          this.setState({ datax: [] });
          this.setState({ loading: false });
          if (result.isConfirmed) {
          }
        });

      this.setState({
        error_kategori: "*tidak boleh kosong dan terlalu singkat",
      });
    } else {
      axios
        .post(process.env.REACT_APP_BASE_API_URI + url, formData, this.configs)
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          const dtresp = res.data.result.Data;
          // console.log(res)
          // return
          if (statux && dtresp) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  this.backAction();
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                this.setState({ datax: [] });
                this.setState({ loading: false });
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    }
  };

  handleChangeCurrentPassword = (e) => {
    this.setState({ current_password: e.target.value });
  };

  // gak jadi cek nik
  handleCheckNikDucapil = () => {
    let url = "/hitnik";
    let formData = {
      NIK: this.state.nik,
      NAMA_LGKP: this.state.name,
      NO_KK: "",
      JENIS_KLMIN: "",
      TMPT_LHR: "",
      TGL_LHR: "",
      STATUS_KAWIN: "",
      JENIS_PKRJN: "",
      NAMA_LGKP_IBU: "",
      ALAMAT: "",
      NO_PROP: "",
      NO_KAB: "",
      NO_KEC: "",
      NO_KEL: "",
      PROP_NAME: "",
      KAB_NAME: "",
      KEC_NAME: "",
      KEL_NAME: "",
      NO_RT: "",
      NO_RW: "",
      TRESHOLD: "100",
    };
  };

  flagNik = () => {
    if (this.state.nik == Cookies.get("user_nik")) {
      return this.state.flag_nik;
    } else {
      return this.state.current_flag_nik;
    }
  };

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        opacity="0.3"
                        d="M22.0318 8.59998C22.0318 10.4 21.4318 12.2 20.0318 13.5C18.4318 15.1 16.3318 15.7 14.2318 15.4C13.3318 15.3 12.3318 15.6 11.7318 16.3L6.93177 21.1C5.73177 22.3 3.83179 22.2 2.73179 21C1.63179 19.8 1.83177 18 2.93177 16.9L7.53178 12.3C8.23178 11.6 8.53177 10.7 8.43177 9.80005C8.13177 7.80005 8.73176 5.6 10.3318 4C11.7318 2.6 13.5318 2 15.2318 2C16.1318 2 16.6318 3.20005 15.9318 3.80005L13.0318 6.70007C12.5318 7.20007 12.4318 7.9 12.7318 8.5C13.3318 9.7 14.2318 10.6001 15.4318 11.2001C16.0318 11.5001 16.7318 11.3 17.2318 10.9L20.1318 8C20.8318 7.2 22.0318 7.59998 22.0318 8.59998Z"
                        fill="#000"
                      ></path>
                      <path
                        d="M4.23179 19.7C3.83179 19.3 3.83179 18.7 4.23179 18.3L9.73179 12.8C10.1318 12.4 10.7318 12.4 11.1318 12.8C11.5318 13.2 11.5318 13.8 11.1318 14.2L5.63179 19.7C5.23179 20.1 4.53179 20.1 4.23179 19.7Z"
                        fill="#333"
                      ></path>
                    </svg>
                  </span>
                  Pengaturan Akun
                </h1>
              </div>
            </div>
            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.back}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <div className="d-flex p-0">
                            <ul
                              className="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap"
                              style={{
                                overflowX: "auto",
                                overflowY: "hidden",
                                display: "flex",
                                whiteSpace: "nowrap",
                                marginBottom: 0,
                              }}
                            >
                              <li className="nav-item">
                                <a
                                  className="nav-link text-dark text-active-primary active"
                                  data-bs-toggle="tab"
                                  href="#kt_tab_pane_1"
                                >
                                  <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                    Informasi Akun
                                  </span>
                                </a>
                              </li>
                              <li className="nav-item">
                                <a
                                  className="nav-link text-dark text-active-primary false"
                                  data-bs-toggle="tab"
                                  href="#kt_tab_pane_2"
                                >
                                  <span className="fs-6 d-md-inline d-lg-inline d-xl-inline">
                                    Log Export Data
                                  </span>
                                </a>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="tab-content" id="detail-account-tab">
                          {/* tab 1 */}
                          <div
                            className="tab-pane fade show active"
                            id="kt_tab_pane_1"
                            role="tabpanel"
                          >
                            <div className="row mt-7">
                              <div className="col-lg-12 mb-7 fv-row">
                                <label className="form-label required">
                                  Nama
                                </label>
                                <input
                                  className="form-control form-control-sm"
                                  style={{
                                    backgroundColor: "#eef",
                                    fontWeight: "bold",
                                  }}
                                  placeholder="Ubah Nama Lengkap"
                                  name="name"
                                  value={this.state.name}
                                  onChange={this.handleChangeName}
                                  disabled
                                />
                                <span style={{ color: "red" }}>
                                  {this.state.error_name}{" "}
                                </span>
                              </div>

                              <div className="col-lg-12 mb-7 fv-row">
                                <label className="form-label required">
                                  Email
                                </label>
                                <input
                                  disabled
                                  className="form-control form-control-sm"
                                  style={{ fontWeight: "bold" }}
                                  placeholder="Email"
                                  name="email"
                                  value={this.state.email}
                                  onChange={this.handleChangeEmail}
                                />
                                <span style={{ color: "red" }}>
                                  {this.state.error_email}{" "}
                                </span>
                              </div>

                              {/*<div className="col-lg-12 mb-7 fv-row">
																<label className="form-label required">
																	NIK
																</label>
																<div style={{ position: "relative" }}>
																	<i
																		className="fa fa-check-circle text-success"
																		style={{
																			position: "absolute",
																			right: 0,
																			top: 10,
																			marginRight: -20,
																			color:
																				this.flagNik() == 1 ? "green" : "",
																		}}
																	></i>

																	<input
																		disabled
																		{...(this.state.nik == ""
																			? ""
																			: "disabled")}
																		className="form-control form-control-sm"
																		style={{ fontWeight: "bold" }}
																		placeholder="Ubah NIK"
																		type="number"
																		name="nik"
																		value={this.state.nik}
																		onChange={this.handleChangeNIK}
																	/>

																	<span style={{ color: "#f24455" }}>
																		{" "}
																		{this.state.error_nik}{" "}
																	</span>
																</div>
															</div>*/}

                              <div className="col-lg-12 mb-7 fv-row">
                                <label className="form-label required">
                                  Satuan Kerja
                                </label>
                                <Select
                                  name="satker"
                                  placeholder="Silahkan pilih"
                                  noOptionsMessage={({ inputValue }) =>
                                    !inputValue
                                      ? this.state.datax
                                      : "Data tidak tersedia"
                                  }
                                  className="form-select-sm selectpicker p-0"
                                  isDisabled={true}
                                  options={this.option_satker}
                                  value={this.state.valSatker}
                                  onChange={this.handleChangeSatker}
                                  isMulti={false}
                                />
                              </div>

                              {this.state.akademis.length != 0 ? (
                                <>
                                  <div className="col-lg-12 mb-7 fv-row">
                                    <label className="form-label">
                                      Akses Akademi
                                    </label>
                                    <div>
                                      <CustomList
                                        {...configTableAkademi}
                                        data={this.state.akademis}
                                        mode="table"
                                        chipPropName="nama_akademi"
                                      ></CustomList>
                                    </div>
                                  </div>
                                </>
                              ) : (
                                <></>
                              )}

                              {this.state.pelatihans.length != 0 ? (
                                <div className="col-lg-12 mb-7 fv-row">
                                  <label className="form-label">
                                    Akses Pelatihan
                                  </label>
                                  <CustomList
                                    {...configTablePelatihan}
                                    data={this.state.pelatihans}
                                    mode="table"
                                    chipPropName="nama_pelatihan"
                                  ></CustomList>
                                </div>
                              ) : (
                                ""
                              )}

                              <div className="col-lg-12 mb-7 fv-row">
                                <div className="form-group">
                                  <label className="form-label required">
                                    Role
                                  </label>
                                  <Select
                                    name="role"
                                    placeholder="Silahkan pilih"
                                    noOptionsMessage={({ inputValue }) =>
                                      !inputValue
                                        ? this.state.datax
                                        : "Data tidak tersedia"
                                    }
                                    className="form-select-sm selectpicker p-0"
                                    options={this.state.option_role}
                                    value={this.state.valRole}
                                    onChange={this.handleChangeRole}
                                    isMulti={true}
                                    isDisabled={true}
                                  />
                                </div>
                              </div>

                              <div className="col-lg-12 mb-7 fv-row">
                                <label className="form-label required">
                                  Password Aktif
                                </label>
                                <div className="input-group mb-3">
                                  <input
                                    className="form-control form-control-sm"
                                    type={this.state.typeCurrentPassword}
                                    value={this.state.current_password}
                                    name="current_password"
                                    placeholder="Password aktif"
                                    onChange={this.handleChangeCurrentPassword}
                                    autoComplete="off"
                                  />
                                  <span
                                    title="show/hide password"
                                    style={{ cursor: "pointer" }}
                                    className="input-group-text"
                                    id="basic-addon2"
                                    onClick={(e) => {
                                      if (
                                        this.state.typeCurrentPassword ==
                                        "password"
                                      ) {
                                        this.setState({
                                          typeCurrentPassword: "text",
                                        });
                                      } else {
                                        this.setState({
                                          typeCurrentPassword: "password",
                                        });
                                      }
                                    }}
                                  >
                                    <i
                                      className={`${
                                        this.state.typeCurrentPassword ==
                                        "password"
                                          ? "fa fa-eye-slash"
                                          : "fa fa-eye"
                                      }`}
                                    ></i>
                                  </span>
                                </div>
                              </div>

                              <div className="col-lg-12 mb-7 fv-row">
                                <label className="form-label required">
                                  Password Baru
                                </label>
                                <div className="input-group mb-3">
                                  <input
                                    className="form-control form-control-sm"
                                    type={this.state.typeConfirmPassword}
                                    value={this.state.password}
                                    name="password"
                                    placeholder="Ubah Password"
                                    onChange={this.handleChangePassword}
                                    autoComplete="off"
                                  />

                                  <span
                                    title="show/hide password"
                                    style={{ cursor: "pointer" }}
                                    className="input-group-text"
                                    id="basic-addon2"
                                    onClick={this.showConfirmPassword}
                                  >
                                    <i
                                      className={this.state.classConfirmEye}
                                    ></i>
                                  </span>
                                </div>
                                <span style={{ color: "red" }}>
                                  {this.state.error_password}{" "}
                                </span>
                              </div>

                              <div className="col-lg-12 mb-7 fv-row">
                                <label className="form-label required">
                                  Konfirmasi Password Baru
                                </label>
                                <div className="input-group mb-3">
                                  <input
                                    className="form-control form-control-sm"
                                    type={this.state.typeConfirmPassword}
                                    name="confPassword"
                                    value={this.state.confPassword}
                                    onChange={this.handleChangeConfPassword}
                                    placeholder="Konfirmasi Password"
                                    autoComplete="off"
                                  />

                                  <span
                                    title="show/hide password"
                                    style={{ cursor: "pointer" }}
                                    className="input-group-text"
                                    id="basic-addon2"
                                    onClick={this.showConfirmPassword}
                                  >
                                    <i
                                      className={this.state.classConfirmEye}
                                    ></i>
                                  </span>
                                </div>
                                <span style={{ color: "red" }}>
                                  {this.state.error_password}{" "}
                                </span>
                              </div>

                              <div className="form-group fv-row pt-7 mb-7">
                                <div className="d-flex justify-content-center mb-7">
                                  <button
                                    onClick={this.back}
                                    type="reset"
                                    className="btn btn-md btn-light me-3"
                                    data-kt-menu-dismiss="true"
                                  >
                                    Batal
                                  </button>
                                  <button
                                    disabled={!this.state.submit}
                                    onClick={this.handleSubmit}
                                    type="submit"
                                    className="btn btn-primary btn-md"
                                    id="submitQuestion1"
                                  >
                                    <i className="fa fa-paper-plane me-1"></i>
                                    Simpan
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div
                            className="tab-pane fade show"
                            id="kt_tab_pane_2"
                            role="tabpanel"
                          >
                            <div className="row">
                              <div className="card-toolbar">
                                <div className="d-flex align-items-center position-relative float-end mt-7 mb-5 me-2">
                                  <span className="svg-icon svg-icon-1 position-absolute ms-6">
                                    <svg
                                      width="24"
                                      height="24"
                                      viewBox="0 0 24 24"
                                      fill="none"
                                      xmlns="http://www.w3.org/2000/svg"
                                      className="mh-50px"
                                    >
                                      <rect
                                        opacity="0.5"
                                        x="17.0365"
                                        y="15.1223"
                                        width="8.15546"
                                        height="2"
                                        rx="1"
                                        transform="rotate(45 17.0365 15.1223)"
                                        fill="currentColor"
                                      ></rect>
                                      <path
                                        d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                        fill="currentColor"
                                      ></path>
                                    </svg>
                                  </span>
                                  <input
                                    type="text"
                                    data-kt-user-table-filter="search"
                                    className="form-control form-control-sm form-control-solid w-250px ps-14"
                                    placeholder="Cari Log"
                                    onKeyPress={this.handleKeyPress}
                                    onChange={(e) => {
                                      if (e.target.value == "") {
                                        this.setState(
                                          { searchText: "" },
                                          () => {
                                            this.handleReload();
                                          },
                                        );
                                      }
                                    }}
                                  />
                                </div>
                              </div>
                              <DataTable
                                columns={this.columns}
                                data={this.state.dataxpdp}
                                progressPending={this.state.loading}
                                progressComponent={this.customLoader}
                                highlightOnHover
                                pointerOnHover
                                pagination
                                paginationServer
                                paginationTotalRows={this.state.totalRows}
                                paginationDefaultPage={this.state.currentPage}
                                onChangeRowsPerPage={(
                                  currentRowsPerPage,
                                  currentPage,
                                ) => {
                                  this.handlePerRowsChange(
                                    currentRowsPerPage,
                                    currentPage,
                                    "row-change",
                                  );
                                }}
                                onChangePage={(page, totalRows) => {
                                  this.handlePerRowsChange(
                                    page,
                                    totalRows,
                                    "page-change",
                                  );
                                }}
                                customStyles={this.customStyles}
                                persistTableHead={true}
                                sortServer
                                onSort={this.handleSort}
                                noDataComponent={
                                  <div className="mt-5">Tidak Ada Data</div>
                                }
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
