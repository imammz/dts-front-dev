import React from "react";
import Footer from "../Footer";
import Header from "../Header";
import SideNav from "../SideNav";
import Content from "./UserAccountComp";

const UserAccountView = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default UserAccountView;
