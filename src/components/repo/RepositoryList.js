import React, { useState, useEffect, useMemo, useRef } from "react";
import Pagination from "@material-ui/lab/Pagination";
import TutorialDataService from "../../service/UserService";
import { GlobalFilter, DefaultFilterForColumn } from "../Filter";
import { useTable } from "react-table";
import axios from "axios";
import Header from "../Header";
import Footer from "../Footer";
import SideNav from "../SideNav";
import {
  useFilters,
  useGlobalFilter,
} from "react-table/dist/react-table.development";

const RepositoryList = () => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];

  const [repository, setRepository] = useState([]);
  const [searchTitle, setSearchTitle] = useState("");
  const repositoryRef = useRef();
  repositoryRef.current = repository;

  const [page, setPage] = useState(0);
  const [count, setCount] = useState(0);
  const [pageSize, setPageSize] = useState(10);
  const pageSizes = [10];
  // repositoryRef.current = repository;

  const onChangeSearchTitle = (e) => {
    const searchTitle = e.target.value;
    setSearchTitle(searchTitle);
  };
  useEffect(() => {
    retrieveRepository();
  }, [page, pageSize]);
  const getRequestParams = (searchTitle, page, pageSize) => {
    let params = {};

    if (searchTitle) {
      params["title"] = searchTitle;
    }

    if (page) {
      params["page"] = page - 1;
    }

    if (pageSize) {
      params["size"] = pageSize;
    }

    return params;
  };
  const retrieveRepository = async () => {
    const params = getRequestParams(searchTitle, page, pageSize);
    const respon = await axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/list-repo-judul",
        {
          start: page,
          length: pageSize,
        },
      )
      .then(function (response) {
        const { repository, totalPages } = response.data.result.Data;
        setRepository(response.data.result.Data);
        setCount(response.data.result.Data.length);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(retrieveRepository, [page, pageSize]);

  const refreshList = () => {
    retrieveRepository();
  };
  const removeAllTutorials = () => {
    TutorialDataService.removeAll()
      .then((response) => {
        console.log(response.data);
        refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const findByTitle = () => {
    setPage(0);
    retrieveRepository();
  };
  const handlePageChange = (event, value) => {
    if (value === 1) {
      setPage(0);
    } else {
      setPage(pageSize * value - 10);
    }
    retrieveRepository();
  };
  const handlePageSizeChange = (event) => {
    setPageSize(event.target.value);
    setPage(0);
  };
  const columns = useMemo(
    () => [
      {
        Header: "No",
        accessor: "id",
        className:
          "text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0",
        sortType: "basic",
      },
      {
        Header: "Judul Form",
        accessor: "judul_form",
        className:
          "text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0",
        sortable: true,
        sortType: "basic",
      },

      {
        Title: "status",
        accessor: "status",
        className:
          "text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0",
        Cell: (props) => {
          const rowUpdx = props.row.status;
          return (
            <select
              className="form-control-sm form-solid"
              data-kt-select2="true"
              data-placeholder="Select option"
              data-dropdown-parent="#kt_menu_61484c5a8da38"
              data-allow-clear="true"
              value={rowUpdx}
            >
              {/* <option /> */}
              <option value="1">Publish</option>
              <option value="0">Unpublish</option>
            </select>
          );
        },
      },
      {
        Header: "Aksi",
        accessor: "actions",
        Cell: (props) => {
          const rowIdx = props.row.nama;
          return (
            <div>
              {/* <span onClick={() => openPendaftaran(rowIdx)}>
                                <i className="far fa-edit action mr-2"></i>
                            </span> */}
              <span>
                <i className="far fa-edit action mr-2"></i>
              </span>
              <span>
                <i className="fas fa-trash action"></i>
              </span>
            </div>
          );
        },
      },
    ],
    [],
  );
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    state,
    rows,
    prepareRow,
    setGlobalFilter,
    preGlobalFilteredRows,
  } = useTable(
    {
      columns,
      data: repository,
      defaultColumn: { Filter: DefaultFilterForColumn },
    },
    useFilters,
    useGlobalFilter,
  );
  return (
    <div>
      <Header />
      <SideNav />
      <div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h4 style={{ textTransform: "uppercase" }}>
                            Daftar {segment_url[2]}
                          </h4>
                        </div>
                        <div className="card-toolbar">
                          {/* <div> */}
                          <GlobalFilter
                            preGlobalFilteredRows={preGlobalFilteredRows}
                            globalFilter={state.globalFilter}
                            setGlobalFilter={setGlobalFilter}
                          />{" "}
                          &nbsp;&nbsp;&nbsp;
                          {/* </div>&nbsp;&nbsp;&nbsp; */}
                          {/* <button className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2" data-bs-toggle="modal" data-bs-target="#filter">
                                <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z" fill="currentColor" />
                                </svg>
                                </span>
                                Filter
                            </button> */}
                          <div className="modal fade" tabindex="-1" id="filter">
                            <div className="modal-dialog modal-lg">
                              <div className="modal-content">
                                <div className="modal-header">
                                  <h5 className="modal-title">
                                    <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                      >
                                        <path
                                          d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                                          fill="currentColor"
                                        />
                                      </svg>
                                    </span>
                                    Filter
                                  </h5>
                                  <div
                                    className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                                    data-bs-dismiss="modal"
                                    aria-label="Close"
                                  >
                                    <span className="svg-icon svg-icon-2x">
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                      >
                                        <rect
                                          opacity="0.5"
                                          x="6"
                                          y="17.3137"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(-45 6 17.3137)"
                                          fill="currentColor"
                                        />
                                        <rect
                                          x="7.41422"
                                          y="6"
                                          width="16"
                                          height="2"
                                          rx="1"
                                          transform="rotate(45 7.41422 6)"
                                          fill="currentColor"
                                        />
                                      </svg>
                                    </span>
                                  </div>
                                </div>
                                <div className="modal-body">
                                  <div className="row">
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label required">
                                        Nama Pelatihan
                                      </label>
                                      <input
                                        className="form-control form-control-sm"
                                        placeholder="Masukkan nama pelatihan"
                                        name="name"
                                      />
                                    </div>
                                    <div className="col-lg-12 mb-7 fv-row">
                                      <label className="form-label required">
                                        Pilih Akademi
                                      </label>
                                      <select
                                        className="form-select form-select-sm selectpicker"
                                        data-control="select2"
                                        data-placeholder="Select an option"
                                      >
                                        <option>Test</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                                <div className="modal-footer">
                                  <div className="d-flex justify-content-between">
                                    <button
                                      type="button"
                                      className="btn btn-sm btn-danger me-3"
                                    >
                                      Reset
                                    </button>
                                    <button
                                      type="button"
                                      className="btn btn-sm btn-primary"
                                    >
                                      Apply Filter
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <a
                            href={
                              "/" +
                              urlSegmentZero +
                              "/" +
                              urlSegmenttOne +
                              "/add"
                            }
                            className="btn btn-primary btn-sm"
                          >
                            <i className="las la-plus"></i>
                            Tambah {urlSegmenttOne}
                          </a>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="table-responsive">
                          <table
                            className="table align-middle table-row-dashed fs-6"
                            {...getTableProps()}
                          >
                            <thead>
                              {headerGroups.map((headerGroup) => (
                                <tr
                                  className="fw-bold fs-6 text-gray-800"
                                  {...headerGroup.getHeaderGroupProps()}
                                >
                                  {headerGroup.headers.map((column) => (
                                    <th
                                      {...column.getHeaderProps()}
                                      align="center"
                                    >
                                      {column.render("Header")}
                                    </th>
                                  ))}
                                </tr>
                              ))}
                            </thead>
                            <tbody {...getTableBodyProps()}>
                              {rows.map((row, i) => {
                                prepareRow(row);
                                return (
                                  <tr {...row.getRowProps()}>
                                    {row.cells.map((cell) => {
                                      return (
                                        <td {...cell.getCellProps()}>
                                          {cell.render("Cell")}
                                        </td>
                                      );
                                    })}
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                        </div>
                        <div className="d-flex justify-content-between align-items-center mt-7">
                          <div className="pagination">
                            <Pagination
                              className="page-item"
                              color="primary"
                              count={count}
                              page={page}
                              siblingCount={3}
                              boundaryCount={3}
                              variant="outlined"
                              shape="rounded"
                              onChange={handlePageChange}
                              showFirstButton
                              showLastButton
                            />
                          </div>

                          <p>{pageSize} data dari ....</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </div>
  );
};

export default RepositoryList;
