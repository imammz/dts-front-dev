import React, { useState, useCallback, useEffect, Fragment } from "react";
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
// import Dropzone, {u} from "react-dropzone";
import Header from "../../components/Header";
import SideNav from "../../components/SideNav";
import Footer from "../../components/Footer";

const RepositoryAdd = (props) => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  let history = useNavigate();
  const initialRepositoryState = {
    judul_form: null,
    status: null,
  };
  const [repository, setRepository] = useState(initialRepositoryState);
  const [submitted, setSubmitted] = useState(false);
  const MySwal = withReactContent(Swal);
  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setRepository({ ...repository, [name]: value });
  };
  const newRepository = () => {
    setRepository(initialRepositoryState);
    setSubmitted(false);
  };
  const saveRepository = (event) => {
    event.preventDefault();
    const dataAray = new FormData();
    dataAray.append("judul_form", repository.judul_form);
    dataAray.append("status", "1");

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/daftarpeserta/create-repo-judul",
        dataAray,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        },
      )
      .then((response) => {
        MySwal.fire({
          title: <strong>Information!</strong>,
          html: <i>{response}</i>,
          icon: "success",
        });
      })
      .catch((error) => {
        MySwal.fire({
          title: <strong>Information!</strong>,
          html: <i>{error.response.data.result.Message}</i>,
          icon: "warning",
        });
      });
  };
  return (
    <>
      <Header />
      <SideNav />
      <div
        className="wrapper d-flex flex-column flex-row-fluid"
        id="kt_wrapper"
        style={{ paddingTop: 8 }}
      >
        <div
          className="content d-flex flex-column flex-column-fluid"
          id="kt_content"
        >
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container-xxl">
              <div className="card card-flush">
                <div className=" mt-6">
                  <div className="card-title">
                    <div className="card-body pt-0">
                      <div className="submit-form">
                        {submitted ? (
                          <div>
                            <h4>You submitted successfully!</h4>
                            <button
                              className="btn btn-success"
                              onClick={newRepository}
                            >
                              Add
                            </button>
                          </div>
                        ) : (
                          <div>
                            <div className="form-group fv-row mb-7">
                              <label
                                className="fs-6 fw-bold form-label mb-2"
                                htmlFor="title"
                              >
                                Group Repository
                              </label>
                              <input
                                type="text"
                                className="form-control form-control-sm font-size-h4"
                                id="slug"
                                required
                                value={repository.judul_form}
                                onChange={handleInputChange}
                                name="judul_form"
                              />
                            </div>
                            <div className="form-group fv-row mb-7">
                              <div className="d-flex justify-content-end mb-7">
                                <button
                                  onClick={() =>
                                    history(
                                      "/" +
                                        urlSegmentZero +
                                        "/" +
                                        urlSegmenttOne,
                                    )
                                  }
                                  type="reset"
                                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                                  data-kt-menu-dismiss="true"
                                >
                                  Batal
                                </button>
                                <button
                                  onClick={saveRepository}
                                  className="btn btn-primary btn-text-light btn-hover-text-success font-weight-bold btn-sm"
                                >
                                  {" "}
                                  <i className="fas fa-paper-plane action"></i>
                                  Simpan
                                </button>
                                {/* <button onClick={saveRepository} className="btn btn-sm btn-light btn-active-light-info me-2">
                                                Submit
                                                </button> */}
                              </div>
                            </div>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};
export default RepositoryAdd;
