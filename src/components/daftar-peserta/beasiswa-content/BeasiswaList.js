import React, { useState, useEffect, useMemo, useRef } from "react";
import BeasiswaService from "../../../service/BeasiswaService";
import { useTable } from "react-table";
import {
  useHistory,
  userNavigate,
  useParams,
  useNavigate,
} from "react-router-dom";
import Beasiswa from "../beasiswa";

const BeasiswaList = (props) => {
  const [beasiswa, setBeasiswa] = useState([]);
  const [searchTitle, setSearchTitle] = useState("");
  const BeasiswaRef = useRef();
  let segement_uri = window.location.pathname.split("/");
  let urlSegmenttOne = segement_uri[2];
  let urlSegmentZero = segement_uri[1];
  BeasiswaRef.current = beasiswa;
  const history = useNavigate();

  useEffect(() => {
    // retrieveBeasiswa();
  }, []);

  const onChangeSearchTitle = (e) => {
    const searchTitle = e.target.value;
    setSearchTitle(searchTitle);
  };

  const refreshList = () => {
    // retrieveBeasiswa();
  };
  const openBeasiswa = (rowIndex) => {
    const id = Beasiswa.current[rowIndex].id;
    const nama = Beasiswa.current[rowIndex].nama;
    const kategori = Beasiswa.current[rowIndex].kategori;
    const fakultas = Beasiswa.current[rowIndex].fakultas;
    const beasiswa = Beasiswa.current[rowIndex].beasiswa;
    const tahap_seleksi = Beasiswa.current[rowIndex].tahap_seleksi;
    const tahap_beasiswa = Beasiswa.current[rowIndex.tahap_beasiswa];
    history("/beasiswa/edit/", id);
  };
  const removeAllBeasiswa = () => {
    BeasiswaService.removeAll()
      .then((response) => {
        console.log(response);
        refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const findByTitle = () => {
    BeasiswaService.findByTitle(searchTitle)
      .then((response) => {
        setBeasiswa(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const relaoad = () => {
    BeasiswaService.getAll()
      .then((response) => {
        console.log("scai");
        console.log(response);
        setBeasiswa(response.data.result.Data);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const deleteBeasiswa = (rowIndex) => {
    const id = BeasiswaRef.current[rowIndex].id;
    BeasiswaService.remove(id)
      .then((response) => {
        window.location.href = "";
        let newBeasiswa = [...BeasiswaRef.current];
        newBeasiswa.splice(rowIndex, 1);
        setBeasiswa(newBeasiswa);
      })
      .catch((e) => {
        console.log(e);
      });
  };
};
