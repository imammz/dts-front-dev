import React, { useState, useEffect, useMemo, useRef } from "react";
import BeasiswaService from "../../../service/BeasiswaService";
import { useTable } from "react-table";
import { GlobalFilter, DefaultFilterForColumn } from "../../Filter";
import { useHistory, useNavigate, useParams } from "react-router-dom";
import {
  useFilters,
  useGlobalFilter,
} from "react-table/dist/react-table.development";
const BeasiswaContent = (props) => {
  let segment_url = window.location.pathname.split("/");
  let urlSegmenttOne = segment_url[2];
  let urlSegmentZero = segment_url[1];
  const [Beasiswa, setBeasiswa] = useState([]);
  const [searchTitle, setSearchTitle] = useState("");
  const BeasiswaRef = useRef();
  BeasiswaRef.current = Beasiswa;
  const history = useNavigate();
  useEffect(() => {
    retrieveBeasiswa();
  }, []);
  const retrieveBeasiswa = () => {
    BeasiswaService.getAll()
      .then((response) => {
        console.log(response);
        setBeasiswa(response.data);
        console.log(Beasiswa);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const refreshList = () => {
    retrieveBeasiswa();
  };

  const handleShow = (cell) => {
    console.log(cell?.row?.original);
    console.log(cell?.row?.original.name);
    history("/pelatihan/pendaftaran/content/" + cell?.row?.original.name);
  };
  const deleteBeasiswa = (rowIndex) => {
    const id = BeasiswaRef.current[rowIndex].id;
    BeasiswaService.remove(id)
      .then((response) => {
        window.location.href = "";
        let newBeasiswa = [...BeasiswaRef.current];
        newBeasiswa.splice(rowIndex, 1);
        setBeasiswa(newBeasiswa);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const columns = useMemo(
    () => [
      {
        Header: "No",
        accessor: "id",
        className:
          "text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0",
        // sortable :true,
      },
      {
        Header: "Nama Lengkap",
        accessor: "nama",
        className: "fs-5 form-label mb-2",
        // sortable : true
      },
      {
        Header: "Kategori Beasiswa",
        accessor: "kategori",
        className: "fs-5 form-label mb-2",
        // sortable: true
      },
      {
        Header: "Beasiswa Tujuan",
        accessor: "beasiswa",
        className:
          "text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0",
        sortable: true,
      },
      {
        Header: "Tahap Seleksi",
        accessor: "tahap_seleksi",
        className:
          "text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0",
        sortable: true,
      },
      {
        Header: "Tahap Beasiswa",
        accessor: "tahap_beasiswa",
        className:
          "text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0",
        sortable: true,
      },
      {
        Header: "Aksi",
        accessor: "",
        Cell: (props) => {
          const rowIdx = props.row.id;
          return (
            <div>
              {/* <span onClick={() => openPendaftaran(rowIdx)}>
                          <i className="far fa-edit action mr-2"></i>
                      </span> */}
              <span onClick={() => handleShow(props)}>
                <i className="far fa-edit action mr-2"></i>
              </span>
              <span onClick={() => deleteBeasiswa(rowIdx)}>
                <i className="fas fa-trash action"></i>
              </span>
            </div>
          );
        },
      },
    ],
    [],
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    state,
    prepareRow,
    setGlobalFilter,
    preGlobalFilteredRows,
  } = useTable(
    {
      columns,
      data: Beasiswa,
      defaultColumn: { Filter: DefaultFilterForColumn },
    },
    useFilters,
    useGlobalFilter,
  );

  return (
    <div>
      <div
        className="wrapper d-flex flex-column flex-row-fluid"
        id="kt_wrapper"
        style={{ paddingTop: 8 }}
      >
        <div
          className="content d-flex flex-column flex-column-fluid"
          id="kt_content"
        >
          <div className="toolbar" id="kt_toolbar">
            <div
              id="kt_toolbar_container"
              className="container-fluid d-flex flex-stack"
            >
              <div
                data-kt-swapper="true"
                data-kt-swapper-mode="prepend"
                data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                className="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0"
              >
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-3 my-1">
                  {urlSegmentZero}
                </h1>
                <span className="h-20px border-gray-200 border-start mx-4" />
                <ul className="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                  <li className="breadcrumb-item text-muted">
                    <a
                      href="/dashboard/digitalent"
                      className="text-muted text-hover-primary"
                    >
                      {urlSegmentZero}
                    </a>
                  </li>
                  <li className="breadcrumb-item">
                    <span className="bullet bg-gray-200 w-5px h-2px" />
                  </li>
                  <li className="breadcrumb-item text-muted">
                    {urlSegmenttOne}
                  </li>
                  <li className="breadcrumb-item">
                    <span className="bullet bg-gray-200 w-5px h-2px" />
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" className="container-xxl">
              <div className="card card-flush">
                <div className="card-header mt-6">
                  <div className="card-title">
                    <div className="card mb-12 mb-xl-8">
                      <h3 className="card-title align-items-start flex-column">
                        <span className="card-label fw-bolder fs-3 mb-1">
                          Daftar Kandidat {urlSegmenttOne}
                        </span>
                        <span className="text-muted mt-6 fw-bold fs-7">
                          {/* <div className="d-flex align-items-center position-relative my-1 me-6">
                                <span className="svg-icon svg-icon-1 position-absolute ms-6">
                                    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} viewBox="0 0 24 24" fill="none">
                                    <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height={2} rx={1} transform="rotate(45 17.0365 15.1223)" fill="black" />
                                    <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
                                    </svg>
                                </span>
                                <input type="text" data-kt-permissions-table-filter="search" className="form-control form-control-solid w-500px ps-15" placeholder="Klik disini untuk pencarian ..." />
                            </div> */}
                        </span>
                      </h3>
                    </div>
                  </div>
                  <div className="card-toolbar"></div>
                </div>
                <div className="card-body pt-0">
                  <div className="col-md-4">
                    <div className="input-group mb-3">
                      <GlobalFilter
                        preGlobalFilteredRows={preGlobalFilteredRows}
                        globalFilter={state.globalFilter}
                        setGlobalFilter={setGlobalFilter}
                      />
                    </div>
                  </div>
                  <table
                    className="table table-bordered table-responsive"
                    {...getTableProps()}
                  >
                    <thead>
                      {headerGroups.map((headerGroup) => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                          {headerGroup.headers.map((column) => (
                            <th
                              className="fs-5 form-label mb-2 "
                              {...column.getHeaderProps()}
                            >
                              {column.render("Header")}
                            </th>
                          ))}
                        </tr>
                      ))}
                    </thead>
                    <tbody
                      className="fs-5 form-label mb-2"
                      {...getTableBodyProps()}
                    >
                      {rows.map((row, i) => {
                        prepareRow(row);
                        return (
                          <tr {...row.getRowProps()}>
                            {row.cells.map((cell) => {
                              return (
                                <td {...cell.getCellProps()}>
                                  {cell.render("Cell")}
                                </td>
                              );
                            })}
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BeasiswaContent;
