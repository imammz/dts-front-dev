import React from "react";
import Header from "../../components/Header";
import Breadcumb from "../Breadcumb";
import Content from "./digitalent-content/digitalent-content";
import SideNav from "../../components/SideNav";
import Footer from "../../components/Footer";

const Digitalent = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default Digitalent;
