import React from "react";
import Header from "../../components/Header";
import Breadcumb from "../Breadcumb";
import Content from "./simonas-content/simonas-content";
import SideNav from "../../components/SideNav";
import Footer from "../../components/Footer";

const Simonas = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default Simonas;
