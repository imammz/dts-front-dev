import React, { useEffect } from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../../informasi/Informasi-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";
import { cekPermition, logout } from "../../../AksesHelper";
import Swal from "sweetalert2";

const Informasi = () => {
  useEffect(() => {
    if (cekPermition().view !== 1) {
      Swal.fire({
        title: "Akses Tidak Diizinkan",
        html: "<i> Anda akan kembali kehalaman login </i>",
        icon: "warning",
      }).then(() => {
        logout();
      });
    }
  }, []);

  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default Informasi;
