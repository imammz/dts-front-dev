import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../Informasi-Edit-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const InformasiEdit = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default InformasiEdit;
