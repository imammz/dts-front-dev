import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Select from "react-select";
import { TagsInput } from "react-tag-input-component";
import Editor from "@arbor-dev/ckeditor5-arbor-custom";

export default class InformasiEditContent extends React.Component {
  constructor(props) {
    let tags =
      window.location.pathname.split("/")[4] !== "null"
        ? window.location.pathname.split("/")[4].split(",")
        : [];
    super(props);
    this.formDatax = new FormData();
    this.onTagsChanged = this.onTagsChanged.bind(this);
    this.handleClick = this.handleClickAction.bind(this);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleChangeThumbnail = this.handleChangeThumbnailAction.bind(this);
    this.handleChangeIsi = this.handleChangeIsiAction.bind(this);
    this.handleChangeAkademi = this.handleChangeAkademiAction.bind(this);
    this.handleChangeKategori = this.handleChangeKategoriAction.bind(this);
    this.handleChangeStatus = this.handleChangeStatusAction.bind(this);
    this.handleChangeJudul = this.handleChangeJudulAction.bind(this);
    this.handleChangePublish = this.handleChangePublishAction.bind(this);
    this.handleDelete = this.handleDeleteAction.bind(this);
    this.state = {
      loading: true,
      dataxakademi: [],
      isLoading: false,
      dataxkategori: [],
      tags: tags,
      fields: {},
      errors: {},
      isi_informasi: "",
      thumbnail: "",
      id_informasi: "",
      dataxinformasi: [],
      valAkademi: [],
      valKategori: [],
      valPublish: [],
      kategori_akademi: "",
    };
    this.formDatax = new FormData();
  }

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  optionstatus = [
    { value: "1", label: "Publish" },
    { value: "0", label: "Unpublish" },
  ];

  handleClickBatalAction(e) {
    swal
      .fire({
        title: "Apakah Anda Yakin?",
        icon: "warning",
        confirmButtonText: "Ya",
        showCancelButton: true,
        cancelButtonText: "Tidak",
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
      })
      .then((result) => {
        if (result.isConfirmed) {
          //window.location = '/publikasi/informasi';
          window.history.back();
        }
      });
  }

  handleChangeThumbnailAction(e) {
    const logofile = e.target.files[0];
    this.formDatax.append("thumbnail", logofile);
  }

  handleChangeIsiAction(value) {
    const errors = this.state.errors;
    errors["isi"] = "";
    this.setState({
      errors,
    });

    this.state.isi_informasi = value;
  }

  handleChangeJudulAction(e) {
    const errors = this.state.errors;
    errors["judul"] = "";
    this.setState({
      errors,
    });
  }

  handleClickAction(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });
      let slug = dataForm.get("judul").replace(" ", "-");
      let today = new Date();
      let tanggal_publish =
        today.getFullYear() +
        "-" +
        (today.getMonth() + 1) +
        "-" +
        today.getDate();

      const dataFormSubmit = new FormData();
      dataFormSubmit.append("id", Cookies.get("id_informasi"));
      dataFormSubmit.append("users_id", Cookies.get("user_id"));
      dataFormSubmit.append("judul_informasi", dataForm.get("judul"));
      dataFormSubmit.append("slug", slug);
      dataFormSubmit.append("isi_informasi", this.state.isi_informasi);
      dataFormSubmit.append("kategori_akademi", dataForm.get("id_akademi"));
      dataFormSubmit.append("kategori_id", dataForm.get("id_kategori"));
      dataFormSubmit.append("tag", this.state.tags.join(","));
      dataFormSubmit.append("gambar", this.formDatax.get("thumbnail"));
      dataFormSubmit.append("tanggal_publish", tanggal_publish);
      dataFormSubmit.append("publish", dataForm.get("status"));
      dataFormSubmit.append("release", 1);

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/publikasi/update-informasi",
          dataFormSubmit,
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                allowOutsideClick: false,
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/publikasi/informasi";
                }
              });
          } else {
            this.setState({ isLoading: false });
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          console.log(error);
          swal.hideLoading();

          swal
            .fire({
              title: error,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        });
    } else {
      this.setState({ isLoading: false });
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const check = this.checkEmpty(
      dataForm,
      ["judul", "id_kategori", "status"],
      [],
    );
    return check;
  }

  resetError() {
    let errors = {};
    errors[("judul", "id_kategori", "status", "isi")] = "";
    this.setState({ errors: errors });
  }

  checkEmpty(dataForm, fieldName, fileFieldName) {
    console.log("RUN");
    let errors = {};
    let formIsValid = true;
    for (const field in fieldName) {
      const nameAttribute = fieldName[field];
      if (dataForm.get(nameAttribute) == "") {
        errors[nameAttribute] = "Tidak Boleh Kosong";
        formIsValid = false;
      }
    }
    if (fileFieldName) {
      for (const field in fileFieldName) {
        const nameAttribute = fileFieldName[field];
        if (dataForm.get(nameAttribute).name == "") {
          errors[nameAttribute] = "Tidak Boleh Kosong";
          formIsValid = false;
        }
      }
    }
    if (this.state.isi_informasi == "") {
      errors["isi"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    console.log(errors);
    this.setState({ errors: errors });
    return formIsValid;
  }

  onTagsChanged(tags) {
    console.log(tags);
    this.setState({ tags });
  }

  handleChangeKategoriAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["id_kategori"] = "";
    this.setState({
      errors,
    });

    this.setState({
      valKategori: { label: selectedOption.label, value: selectedOption.value },
    });
  };

  handleChangeAkademiAction = (selectedOption) => {
    this.setState({
      valAkademi: { label: selectedOption.label, value: selectedOption.value },
    });
  };

  handleChangeStatusAction = (selectedOption) => {
    this.setState({
      valPublish: { value: selectedOption.value, label: selectedOption.label },
    });
  };

  handleChangePublishAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["status"] = "";
    this.setState({
      errors,
    });
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    let segment_url = window.location.pathname.split("/");
    let id_informasi = segment_url[3];
    this.setState({ id_informasi: id_informasi });
    Cookies.set("id_informasi", id_informasi);
    let data = {
      id: id_informasi,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/cari-informasi",
        data,
        this.configs,
      )
      .then((res) => {
        console.log(res.data.result);
        const dataxinformasi = res.data.result.Data[0];
        this.setState({
          valPublish: {
            value: dataxinformasi.publish,
            label: dataxinformasi.publish == 1 ? "Publish" : "Unpublish",
          },
        });
        this.setState({ dataxinformasi }, function () {
          this.setState({
            kategori_akademi: this.state.dataxinformasi.kategori_akademi,
          });
          this.handleReload();
        });
      });
  }

  handleReload(page, newPerPage) {
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    const dataAkademik = { start: 0, length: 100, status: "publish" };
    const dataKategorik = {
      start: 0,
      length: 100,
      jenis_kategori: "Informasi",
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/list-akademi",
        dataAkademik,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxakademi = [];
        optionx.map((data) =>
          dataxakademi.push({
            value: data.slug,
            label: data.name + " (" + data.slug + ")",
          }),
        );
        this.setState({ dataxakademi }, function () {
          for (let i in optionx) {
            if (optionx[i].slug === this.state.kategori_akademi) {
              this.setState({
                valAkademi: {
                  value: this.state.kategori_akademi,
                  label: optionx[i].name + " (" + optionx[i].slug + ")",
                },
              });
            }
          }
        });
      });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/list-kategori",
        dataKategorik,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxkategori = [];
        optionx.map((data) =>
          dataxkategori.push({ value: data.id, label: data.nama }),
        );
        this.setState({ dataxkategori }, function () {
          for (let i in optionx) {
            if (optionx[i].id == this.state.dataxinformasi.kategori_id) {
              this.setState({
                valKategori: { value: optionx[i].id, label: optionx[i].nama },
              });
            }
          }
        });
      });
    console.log(this.state.valPublish);
  }

  handleDeleteAction(e) {
    e.preventDefault();
    const idx = this.state.id_informasi;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          const data = { id: idx };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI +
                "/publikasi/softdelete-informasi",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                    allowOutsideClick: false,
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      //window.location = '/publikasi/informasi';
                      window.history.back();
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmationBUttonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                    }
                  });
              }
            });
        }
      });
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="32"
                      height="32"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        opacity="0.3"
                        d="M12.9 10.7L3 5V19L12.9 13.3C13.9 12.7 13.9 11.3 12.9 10.7Z"
                        fill="currentColor"
                      ></path>
                      <path
                        d="M21 10.7L11.1 5V19L21 13.3C22 12.7 22 11.3 21 10.7Z"
                        fill="#f1416c"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Publikasi
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Informasi
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>

                <a
                  href="#"
                  onClick={this.handleDelete}
                  className="btn btn-sm btn-danger btn-active-light-info"
                >
                  <i className="bi bi-trash-fill text-white pe-1"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Edit Informasi
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <form
                        className="form"
                        action="#"
                        onSubmit={this.handleClick}
                      >
                        <input
                          type="hidden"
                          name="csrf-token"
                          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                        />
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">Judul</label>
                          <input
                            className="form-control form-control-sm"
                            placeholder="Masukkan Judul Disini"
                            name="judul"
                            defaultValue={
                              this.state.dataxinformasi.judul_informasi
                            }
                            onChange={this.handleChangeJudul}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["judul"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">
                            Isi Informasi
                          </label>
                          <CKEditor
                            editor={Editor}
                            name="isi"
                            data={this.state.dataxinformasi.isi_informasi}
                            onReady={(editor) => {
                              console.log("Editor is ready to use!", editor);
                            }}
                            config={{
                              ckfinder: {
                                // Upload the images to the server using the CKFinder QuickUpload command.
                                uploadUrl:
                                  process.env.REACT_APP_BASE_API_URI +
                                  "/publikasi/ckeditor-upload-image",
                              },
                              mediaEmbed: {
                                previewsInData: true,
                              },
                            }}
                            onChange={(event, editor) => {
                              const data = editor.getData();
                              this.handleChangeIsi(data);
                            }}
                            onBlur={(event, editor) => {
                              console.log("Blur.", editor);
                            }}
                            onFocus={(event, editor) => {
                              console.log("Focus.", editor);
                            }}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["isi"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          {this.state.dataxinformasi.gambar != "" ? (
                            <div>
                              <label className="form-label">
                                Gambar Thumbnail
                              </label>
                              <br />
                              <img
                                style={{ height: "80px", width: "80px" }}
                                src={
                                  process.env.REACT_APP_BASE_API_URI +
                                  "/publikasi/get-file?path=" +
                                  this.state.dataxinformasi.gambar
                                }
                              />
                            </div>
                          ) : (
                            ""
                          )}
                          <br />
                          <input
                            type="hidden"
                            name="upload_logo_hidden"
                            defaultValue={this.state.dataxinformasi.gambar}
                          />
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label">Ganti Thumbnail</label>
                          <input
                            type="file"
                            className="form-control form-control-sm font-size-h4"
                            name="thumbnail"
                            id="thumbnail"
                            accept=".png,.jpg,.jpeg,.svg"
                            onChange={this.handleChangeThumbnail}
                          />
                          <small className="text-muted">
                            Resolusi yang direkomendasikan adalah 837 * 640.
                            Fokus visual pada bagian tengah gambar.
                          </small>
                          <div>
                            <span style={{ color: "red" }}>
                              {this.state.errors["thumbnail"]}
                            </span>
                          </div>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label">Akademi</label>
                          <Select
                            id="id_akademi"
                            name="id_akademi"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.dataxakademi
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            value={this.state.valAkademi}
                            options={this.state.dataxakademi}
                            onChange={this.handleChangeAkademi}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["id_akademi"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">
                            Kategori
                          </label>
                          <Select
                            id="id_kategori"
                            name="id_kategori"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.dataxkategori
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            value={this.state.valKategori}
                            options={this.state.dataxkategori}
                            onChange={this.handleChangeKategori}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["id_kategori"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label">Tag</label>
                          <TagsInput
                            value={this.state.tags}
                            onChange={this.onTagsChanged}
                            name="tags"
                            placeHolder="Isi Tag Disini dan Enter"
                            className="form-control form-control-sm"
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["tags"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">Status</label>
                          <Select
                            name="status"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            options={this.optionstatus}
                            value={this.state.valPublish}
                            onChange={this.handleChangeStatus}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["status"]}
                          </span>
                        </div>

                        <div className="form-group fv-row pt-7 mb-7">
                          <div className="d-flex justify-content-center mb-7">
                            <button
                              onClick={this.handleClickBatal}
                              type="reset"
                              className="btn btn-md btn-light me-3"
                              data-kt-menu-dismiss="true"
                            >
                              Batal
                            </button>
                            <button
                              disabled={this.state.isLoading}
                              type="submit"
                              className="btn btn-primary btn-md"
                              id="submitQuestion1"
                            >
                              {this.state.isLoading ? (
                                <>
                                  <span
                                    className="spinner-border spinner-border-sm me-2"
                                    role="status"
                                    aria-hidden="true"
                                  ></span>
                                  <span className="sr-only">Loading...</span>
                                  Loading...
                                </>
                              ) : (
                                <>
                                  <i className="fa fa-paper-plane me-1"></i>
                                  Simpan
                                </>
                              )}
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
