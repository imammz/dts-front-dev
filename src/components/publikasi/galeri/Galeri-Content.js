import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";

export default class GaleriContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleEditToken = this.handleEditTokenAction.bind(this);
    this.handleSimpanToken = this.handleSimpanTokenAction.bind(this);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
  }
  state = {
    datax: [],
    isLoading: false,
    loading: true,
    token: "",
    err: "",
  };
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  handleEditTokenAction(e) {
    this.setState({ token: e.target.value });
    if (e.target.value == "") {
      this.state.err = "Tidak Boleh Kosong";
    } else {
      this.state.err = "";
    }
  }

  handleSimpanTokenAction() {
    const body = {
      key: "token_instagram",
      value: this.state.token,
    };
    if (!this.state.err) {
      this.simpan(body);
    }
  }

  simpan(body) {
    this.setState({ isLoading: true });
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/update-setting-galeri",
        body,
        this.configs,
      )
      .then((res) => {
        this.setState({ isLoading: false });
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          swal
            .fire({
              title: messagex,
              icon: "success",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.handleReload();
              }
            });
        } else {
          this.setState({ isLoading: false });
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      })
      .catch((error) => {
        this.setState({ isLoading: false });
        swal.hideLoading();
        swal
          .fire({
            title: error,
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
            }
          });
      });
  }

  handleClickBatalAction(e) {
    swal
      .fire({
        title: "Apakah Anda Yakin?",
        icon: "warning",
        confirmButtonText: "Ya",
        showCancelButton: true,
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          this.handleReload();
        }
      });
  }

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    this.handleReload();
  }

  handleReload() {
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/list-setting-galeri",
        null,
        this.configs,
      )
      .then((res) => {
        swal.close();
        const datax = res.data.result.Data;

        this.setState({ token: datax.value });
        this.getInstagramFeeds();
      });
  }

  getInstagramFeeds() {
    //https://graph.instagram.com/me/media?access_token=IGQVJVdm9RVTRZAUDlHWnpmTUdJcUpTQnl0MnhVUENvY2lDdGxDSWhTODFTeEJuTHlmRVVzSlRXaHY3Si11M3ZAqcW9NNkFuZAk50dkVLM0dfUV9LM01GRGlvYXdJN3hzSDlON2N1b3RUUHMzVUlrUGtwMgZDZD&fields=id, media_type, media_url, username, timestamp
    const bodyForm = {
      access_token: this.state.token,
      fields: "id, media_type, media_url, username, timestamp",
    };
    axios
      .get(
        "https://graph.instagram.com/me/media?access_token=" +
          this.state.token +
          "&" +
          "fields=id, thumbnail_url, media_type, media_url, username, timestamp",
      )
      .then((res) => {
        this.setState({ datax: res.data.data });
      });
  }

  render() {
    const { datax } = this.state;

    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="32"
                      height="32"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        opacity="0.3"
                        d="M12.9 10.7L3 5V19L12.9 13.3C13.9 12.7 13.9 11.3 12.9 10.7Z"
                        fill="currentColor"
                      ></path>
                      <path
                        d="M21 10.7L11.1 5V19L21 13.3C22 12.7 22 11.3 21 10.7Z"
                        fill="#f1416c"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Publikasi
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Galeri
                </h1>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Kelola Galeri
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="form-group row">
                        <label className="col-lg-2 col-form-label text-lg-right">
                          Instagram Token
                        </label>
                        <div className="col-lg-1 col-xl-6 mb-3">
                          <input
                            className="form-control"
                            placeholder="@"
                            name="token"
                            type="text"
                            onChange={this.handleEditToken}
                            value={this.state.token}
                          />
                          <div>
                            <span style={{ color: "red" }}>
                              {this.state.err}
                            </span>
                          </div>
                        </div>
                        <button
                          disabled={this.state.isLoading}
                          className="btn btn-primary btn-sm col-lg-1 mb-3"
                          title="Simpan"
                          onClick={this.handleSimpanToken}
                        >
                          {this.state.isLoading ? (
                            <>
                              <span
                                className="spinner-border spinner-border-sm me-2"
                                role="status"
                                aria-hidden="true"
                              ></span>
                              <span className="sr-only">Loading...</span>
                              Loading...
                            </>
                          ) : (
                            <>Simpan</>
                          )}
                        </button>
                      </div>
                      <div className="mt-10 border-top row">
                        <h5 className="my-6">Preview Gallery</h5>
                        {datax.map((item, index) => {
                          let url = item["media_url"];
                          if (item["media_type"] === "VIDEO") {
                            url = item["thumbnail_url"];
                          }
                          return (
                            <div
                              className="col-lg-2 mt-1 mb-1 mr-1 ml-1"
                              key={index}
                            >
                              <img src={url} className="w-100 rounded"></img>
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
