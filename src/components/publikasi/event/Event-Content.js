import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import Select from "react-select";
import { capitalizeFirstLetter } from "../helper";
import { dateRange } from "../../pelatihan/Pelatihan/helper";
import Swal from "sweetalert2";

export default class EventContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickDelete = this.handleClickDeleteAction.bind(this);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleClickNoFilter = this.handleClickNoFilterAction.bind(this);
    this.handleClickFilterOnline =
      this.handleClickFilterOnlineAction.bind(this);
    this.handleClickFilterOffline =
      this.handleClickFilterOfflineAction.bind(this);
    this.handleClickFilterOnlineOffline =
      this.handleClickFilterOnlineOfflineAction.bind(this);
    this.handleClickPratinjau = this.handleClickPratinjauAction.bind(this);
    this.handleClickFilter = this.handleClickFilterAction.bind(this);
    this.handleClickReset = this.handleClickResetAction.bind(this);
    this.handleChangeKategoriFilter =
      this.handleChangeKategoriFilterAction.bind(this);
    this.handleChangeStatusFilter =
      this.handleChangeStatusFilterAction.bind(this);
    this.handleChangeStatusEventFilter =
      this.handleChangeStatusEventFilterAction.bind(this);
    this.handleSort = this.handleSortAction.bind(this);
    this.handleChangeSearch = this.handleChangeSearchAction.bind(this);
  }
  state = {
    datax: [],
    loading: true,
    totalRows: 0,
    newPerPage: 10,
    totalEventOffline: 0,
    totalEventOnlineOffline: 0,
    totalEvent: 0,
    totalEventOnline: 0,
    tempLastNumber: 0,
    currentPage: 0,
    statusPublish: "",
    valKategoriFilter: [],
    valKategoriStatus: [],
    pratinjau: [],
    column: "id",
    sortDirection: "desc",
    searchText: "",
    isRowChangeRef: false,
    valStatusEventFilter: "",
  };

  handleClickPratinjauAction(e) {
    const kategori = e.currentTarget.getAttribute("kategori");
    const judul = e.currentTarget.getAttribute("judul");
    const deskripsi = e.currentTarget.getAttribute("deskripsi");
    const tanggal_event = e.currentTarget.getAttribute("tanggal_event");
    const gambar = e.currentTarget.getAttribute("gambar");
    const lokasi = e.currentTarget.getAttribute("lokasi");
    const pembicara =
      typeof e.currentTarget.getAttribute("pembicara") == "string"
        ? e.currentTarget.getAttribute("pembicara").split(";")
        : "";
    const link = e.currentTarget.getAttribute("link");
    const jenis = e.currentTarget.getAttribute("jenis");
    const id = e.currentTarget.getAttribute("id");

    this.setState(
      {
        pratinjau: {
          kategori: kategori,
          gambar: gambar,
          judul: judul,
          deskripsi: deskripsi,
          tanggal_event: tanggal_event,
          lokasi: lokasi,
          pembicara: pembicara,
          link: link,
          jenis: jenis,
          id: id,
        },
      },
      () => {},
    );
  }

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  option_jenis_event = [
    { value: "", label: "Pilih Semua" },
    { value: 1, label: "Online" },
    { value: 0, label: "Offline" },
    { value: 2, label: "Hybrid" },
  ];
  status = [
    { value: "", label: "Pilih Semua" },
    { value: 1, label: "Publish" },
    { value: 0, label: "Unpublish" },
  ];
  columns = [
    {
      name: "No",
      center: true,
      cell: (row, index) => this.state.tempLastNumber + index + 1,
      width: "70px",
    },
    {
      // name: 'Judul Event',
      // sortable: true,
      // selector: row => capitalizeFirstLetter(row.judul_event),
      // width: "200px"

      Header: "Judul Event",
      accessor: "",
      // sortType: 'basic',
      // allowOverflow: true,
      name: "Judul Event",
      sortable: true,
      className: "min-w-300px mw-300px",
      width: "320px",
      grow: 6,
      selector: (row, index) => capitalizeFirstLetter(row.judul_event),
      cell: (row, index) => {
        return (
          <span className="d-flex flex-column my-2">
            <a
              href="#"
              data-bs-toggle="modal"
              data-bs-target="#pratinjau"
              onClick={this.handleClickPratinjau}
              kategori={row.kategori}
              judul={row.judul_event}
              deskripsi={row.deskripsi_event}
              tanggal_event={dateRange(
                row.tanggal_mulai,
                row.tanggal_selesai,
                "YYYY-MM-DD HH:mm:ss",
                "DD MMM YYYY HH:mm:ss",
              )}
              gambar={row.gambar}
              pembicara={row.pembicara}
              lokasi={row.lokasi}
              link={row.link}
              jenis={row.jenis}
              id={row.id}
              title="Pratinjau"
              className="text-dark"
            >
              <span className="d-flex align-items-center">
                <span className="symbol symbol-75px">
                  <span
                    className="symbol-label"
                    style={{ backgroundColor: "transparent" }}
                  >
                    <span className="svg-icon">
                      <img
                        src={
                          process.env.REACT_APP_BASE_API_URI +
                          "/publikasi/get-file?path=" +
                          row.gambar
                        }
                        alt="thumbnail event"
                        className="w-75 rounded mt-3 mb-3"
                      />
                    </span>
                  </span>
                </span>
                <span className="d-flex flex-column">
                  <span
                    className="fw-bolder fs-7"
                    style={{
                      overflow: "hidden",
                      whiteSpace: "wrap",
                      textOverflow: "ellipses",
                    }}
                  >
                    {capitalizeFirstLetter(row.judul_event)}
                  </span>
                  <span className="fs-7 fw-semibold mt-0 text-muted">
                    {row.kategori}
                  </span>
                </span>
              </span>
              {/* <p
                className="fw-bolder fs-7 mb-0"
                style={{
                  overflow: "hidden",
                  whiteSpace: "wrap",
                  textOverflow: "ellipses",
                }}
              >
                {capitalizeFirstLetter(row.judul_event)}
              </p> */}
            </a>
          </span>
        );
      },
    },
    {
      name: "Tgl. Event",
      sortable: true,
      className: "min-w-100px",
      grow: 2,
      selector: (row) => row.tanggal_mulai,
      cell: (row, index) => {
        return (
          <label class="d-flex flex-stack">
            <span class="d-flex align-items-center me-2">
              <span class="d-flex flex-column">
                <span class="mb-0 fs-7">
                  <p className="mb-0"></p>
                  {dateRange(
                    row.tanggal_mulai,
                    row.tanggal_selesai,
                    "YYYY-MM-DD HH:mm:ss",
                    "DD MMM YYYY HH:mm:ss",
                  )}
                </span>
              </span>
            </span>
          </label>
        );
      },
    },
    {
      name: "Author",
      sortable: true,
      className: "min-w-100px",
      grow: 2,
      selector: (row) => capitalizeFirstLetter(row.user_name),
      cell: (row) => {
        return (
          <span className="d-flex flex-column">
            <span
              className="fs-7"
              style={{
                overflow: "hidden",
                whiteSpace: "wrap",
                textOverflow: "ellipses",
              }}
            >
              {capitalizeFirstLetter(row.user_name)}
            </span>
          </span>
        );
      },
    },
    {
      name: "Status",
      sortable: true,
      center: true,
      className: "min-w-100px",
      grow: 2,
      selector: (row, index) => row.publish,
      cell: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.publish == 1 ? "success" : "danger") +
              " fs-7 m-1"
            }
          >
            {row.publish == 1 ? "Publish" : "Unpublish"}
          </span>
        </div>
      ),
    },
    {
      name: "Jenis Event",
      sortable: true,
      center: true,
      className: "min-w-100px",
      grow: 2,
      selector: (row, index) => row.jenis,
      cell: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.jenis == 1
                ? "success"
                : row.jenis == 2
                  ? "primary"
                  : "info") +
              " fs-7 m-1"
            }
          >
            {row.jenis == 1 ? "Online" : row.jenis == 2 ? "Hybrid" : "Offline"}
          </span>
        </div>
      ),
    },
    {
      name: "Aksi",
      center: true,
      width: "150px",
      cell: (row) => (
        <div>
          <a
            href="#"
            data-bs-toggle="modal"
            data-bs-target="#pratinjau"
            onClick={this.handleClickPratinjau}
            className="btn btn-icon btn-bg-primary btn-sm me-1"
            alt="Lihat"
            kategori={row.kategori}
            judul={row.judul_event}
            deskripsi={row.deskripsi_event}
            tanggal_event={dateRange(
              row.tanggal_mulai,
              row.tanggal_selesai,
              "YYYY-MM-DD HH:mm:ss",
              "DD MMM YYYY HH:mm:ss",
            )}
            gambar={row.gambar}
            pembicara={row.pembicara}
            lokasi={row.lokasi}
            link={row.link}
            jenis={row.jenis}
            id={row.id}
            title="Pratinjau"
          >
            <i className="bi bi-eye-fill text-white"></i>
          </a>
          <a
            href={"/publikasi/edit-event/" + row.id}
            id={row.id}
            className="btn btn-icon btn-bg-warning btn-sm me-1"
            title="Edit"
            alt="Edit"
          >
            <i className="bi bi-gear-fill text-white"></i>
          </a>
          <a
            href="#"
            id={row.id}
            onClick={this.handleClickDelete}
            title="Hapus"
            className="btn btn-icon btn-bg-danger btn-sm me-1"
          >
            <i className="bi bi-trash-fill text-white"></i>
          </a>
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };
  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    const bodyKategori = { start: 0, length: 100, jenis_kategori: "Event" };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/list-kategori",
        bodyKategori,
        this.configs,
      )
      .then((res) => {
        const optionx =
          res.data.result.Data[0] == null ? [] : res.data.result.Data;
        const dataxkategori = [{ value: "", label: "Pilih Semua" }];
        optionx.map((data) =>
          dataxkategori.push({ value: data.id, label: data.nama }),
        );
        this.setState({ dataxkategori });
      });
    this.handleReload();
  }

  handleClickNoFilterAction(e) {
    this.handleClickHeaderFilterAction({
      jenis: "",
      status: "",
      sorting: "",
    });
  }

  handleClickFilterOnlineAction(e) {
    this.handleClickHeaderFilterAction({
      jenis: 1,
      status: "",
      sorting: "",
    });
  }

  handleClickFilterOfflineAction(e) {
    this.handleClickHeaderFilterAction({
      jenis: 0,
      status: "",
      sorting: "",
    });
  }

  handleClickFilterOnlineOfflineAction(e) {
    this.handleClickHeaderFilterAction({
      jenis: 2,
      status: "",
      sorting: "",
    });
  }

  handleClickResetAction() {
    this.setState(
      {
        valKategoriFilter: [],
        valStatusFilter: [],
        valStatusEventFilter: [],
      },
      () => {
        this.handleReload();
      },
    );
  }

  handleChangeKategoriFilterAction = (selectedOption) => {
    this.setState({
      valKategoriFilter: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  };

  handleChangeStatusFilterAction = (selectedOption) => {
    this.setState({
      valStatusFilter: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  };

  handleChangeStatusEventFilterAction = (selectedOption) => {
    this.setState({
      valStatusEventFilter: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  };

  handleSortAction(column, sortDirection) {
    let server_name = "";
    if (column.name == "Judul Event") {
      server_name = "judul_event";
    } else if (column.name == "Tgl. Event") {
      server_name = "tanggal_event";
    } else if (column.name == "Author") {
      server_name = "user_name";
    } else if (column.name == "Status") {
      server_name = "publish";
    } else if (column.name == "Jenis Event") {
      server_name = "jenis";
    }

    this.setState(
      {
        column: server_name,
        sortDirection: sortDirection,
      },
      () => {
        this.handleReload(1, this.state.newPerPage);
      },
    );
  }

  handleClickHeaderFilterAction = (payload) => {
    this.setState({ loading: true });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/filter-event",
        {
          status: payload.status,
          jenis: payload.jenis,
          start: 0,
          length: this.state.newPerPage,
          kategori: "",
          search: this.state.searchText,
          sort_by: this.state.column,
          sort_val: this.state.sortDirection,
        },
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          const datax =
            res.data.result.Data[0] == null ? [] : res.data.result.Data;
          this.setState({ currentPage: 1 });
          this.setState({ tempLastNumber: 0 });
          this.setState({ datax });
          this.setState({ totalRows: res.data.result.TotalLength });
          this.setState({ loading: false });
          this.setState({ totalViews: res.data.result.TotalViews });
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                // this.handleClickResetAction();
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      })
      .catch((error) => {
        let messagex = error.response?.data?.result?.Message;
        swal
          .fire({
            title: messagex ?? "Terjadi Kesalahan!",
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
            }
          });
      });
  };

  handleClickFilterAction(e) {
    e.preventDefault();
    this.setState({ loading: true }, () => {
      this.handleReload(1, this.state.newPerPage);
    });
  }

  handleClickDeleteAction(e) {
    const idx = e.currentTarget.id;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          swal.fire({
            title: "Mohon Tunggu!",
            icon: "info", // add html attribute if you want or remove
            allowOutsideClick: false,
            didOpen: () => {
              swal.showLoading();
            },
          });
          const data = { id: idx };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI +
                "/publikasi/softdelete-event",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload(
                        this.state.currentPage,
                        this.state.newPerPage,
                      );
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmationBUttonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload();
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload();
                    }
                  });
              }
            });
        }
      });
  }

  handleChangeSearchAction(e) {
    const searchText = e.currentTarget.value;
    this.setState({ searchText }, () => {
      if (searchText == "") {
        this.handleReload();
      }
    });
  }
  handleReload(page, newPerPage) {
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });
    const dataBody = {
      start: start_tmp,
      length: length_tmp,
      status: this.state.statusPublish,
      search: this.state.searchText,
      sort_by: this.state.column,
      jenis:
        this.state.valStatusEventFilter &&
        this.state.valKategoriFilter.value != null
          ? this.state.valStatusEventFilter.value
          : "",
      kategori:
        this.state.valKategoriFilter &&
        this.state.valKategoriFilter.value != null
          ? this.state.valKategoriFilter.value
          : "",
      sort_val: this.state.sortDirection.toUpperCase(),
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/filter-event",
        dataBody,
        this.configs,
      )
      .then((res) => {
        const datax =
          res.data.result.Data[0] == null ? [] : res.data.result.Data;
        this.setState({ datax });
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          this.setState({ loading: false });
          this.setState({ totalRows: res.data.result.TotalLength });
          this.setState({
            totalEventOnlineOffline: res.data.result.TotalEventOnlineOffline,
          });
          this.setState({
            totalEventOffline: res.data.result.TotalEventOffline,
          });
          this.setState({ totalEvent: res.data.result.TotalEvent });
          this.setState({ totalEventOnline: res.data.result.TotalEventOnline });
          this.setState({ currentPage: page });
        } else {
          throw Error(messagex);
        }
      })
      .catch((err) => {
        Swal.fire({
          title: err.message,
          icon: "warning",
          confirmButtonText: "Ok",
        }).then(() => {
          this.setState({ loading: false });
        });
      });
  }

  handlePerRowsChange = async (arg1, arg2, srcEvent) => {
    if (srcEvent == "page-change") {
      this.setState({ loading: true }, () => {
        if (!this.state.isRowChangeRef) {
          this.handleReload(arg1, this.state.newPerPage);
        }
      });
    } else if (srcEvent == "row-change") {
      this.setState({ isRowChangeRef: true }, () => {
        this.handleReload(arg2, arg1);
      });
      this.setState({ loading: true, newPerPage: arg1 }, () => {
        this.setState({ isRowChangeRef: false });
      });
    }
  };

  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    if (e.key == "Enter") {
      if (searchText == "") {
        this.handleReload();
      } else {
        this.setState({ loading: true });
        let data = {
          cari: searchText,
          start: 0,
          length: this.state.newPerPage,
        };
        axios
          .post(
            process.env.REACT_APP_BASE_API_URI + "/publikasi/carifull-event",
            data,
            this.configs,
          )
          .then((res) => {
            const statux = res.data.result.Status;
            const messagex = res.data.result.Message;
            if (statux) {
              const datax =
                res.data.result.Data[0] == null ? [] : res.data.result.Data;
              this.setState({ currentPage: 1 });
              this.setState({ tempLastNumber: 0 });
              this.setState({ datax });
              this.setState({ totalRows: res.data.result.TotalEvent });
              this.setState({ loading: false });
            } else {
              swal
                .fire({
                  title: messagex,
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  this.setState({ datax: [] });
                  this.setState({ loading: false });
                });
            }
          })
          .catch((error) => {
            let statux = error.response.data.result.Status;
            let messagex = error.response.data.result.Message;
            if (!statux) {
              swal
                .fire({
                  title: messagex,
                  icon: "warning",
                  confirmButtonText: "Ok",
                })
                .then((result) => {
                  if (result.isConfirmed) {
                  }
                });
            }
          });
      }
    }
  }

  render() {
    let rowCounter = 1;
    const styleImg = {
      width: "40px",
      height: "30px",
    };
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="32"
                      height="32"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        opacity="0.3"
                        d="M12.9 10.7L3 5V19L12.9 13.3C13.9 12.7 13.9 11.3 12.9 10.7Z"
                        fill="currentColor"
                      ></path>
                      <path
                        d="M21 10.7L11.1 5V19L21 13.3C22 12.7 22 11.3 21 10.7Z"
                        fill="#f1416c"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Publikasi
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Event
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                  data-bs-toggle="modal"
                  data-bs-target="#filter"
                >
                  <i className="bi bi-sliders"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Filter
                  </span>
                </button>

                <a
                  href="/publikasi/tambah-event"
                  className="btn btn-success fw-bolder btn-sm"
                >
                  <i className="bi bi-plus-circle"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Tambah Event
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-7"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 col-md-12 col-sm-12">
                    <div className="row">
                      <div className="col-6 col-lg-3 mb-3">
                        <a
                          href="#"
                          onClick={this.handleClickNoFilter}
                          className="card hoverable"
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-primary mb-3 mt-1">
                                <span className="svg-icon svg-icon-primary svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <path
                                      opacity="0.3"
                                      d="M20.9 12.9C20.3 12.9 19.9 12.5 19.9 11.9C19.9 11.3 20.3 10.9 20.9 10.9H21.8C21.3 6.2 17.6 2.4 12.9 2V2.9C12.9 3.5 12.5 3.9 11.9 3.9C11.3 3.9 10.9 3.5 10.9 2.9V2C6.19999 2.5 2.4 6.2 2 10.9H2.89999C3.49999 10.9 3.89999 11.3 3.89999 11.9C3.89999 12.5 3.49999 12.9 2.89999 12.9H2C2.5 17.6 6.19999 21.4 10.9 21.8V20.9C10.9 20.3 11.3 19.9 11.9 19.9C12.5 19.9 12.9 20.3 12.9 20.9V21.8C17.6 21.3 21.4 17.6 21.8 12.9H20.9Z"
                                      fill="#009ef7"
                                    ></path>
                                    <path
                                      d="M16.9 10.9H13.6C13.4 10.6 13.2 10.4 12.9 10.2V5.90002C12.9 5.30002 12.5 4.90002 11.9 4.90002C11.3 4.90002 10.9 5.30002 10.9 5.90002V10.2C10.6 10.4 10.4 10.6 10.2 10.9H9.89999C9.29999 10.9 8.89999 11.3 8.89999 11.9C8.89999 12.5 9.29999 12.9 9.89999 12.9H10.2C10.4 13.2 10.6 13.4 10.9 13.6V13.9C10.9 14.5 11.3 14.9 11.9 14.9C12.5 14.9 12.9 14.5 12.9 13.9V13.6C13.2 13.4 13.4 13.2 13.6 12.9H16.9C17.5 12.9 17.9 12.5 17.9 11.9C17.9 11.3 17.5 10.9 16.9 10.9Z"
                                      fill="#009ef7"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.totalEvent}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Total Event
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-6 col-lg-3 mb-3">
                        <a
                          href="#"
                          onClick={this.handleClickFilterOnline}
                          className="card hoverable"
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-success mb-3 mt-1">
                                <span className="svg-icon svg-icon-success svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <path
                                      d="M11.2166 8.50002L10.5166 7.80007C10.1166 7.40007 10.1166 6.80005 10.5166 6.40005L13.4166 3.50002C15.5166 1.40002 18.9166 1.50005 20.8166 3.90005C22.5166 5.90005 22.2166 8.90007 20.3166 10.8001L17.5166 13.6C17.1166 14 16.5166 14 16.1166 13.6L15.4166 12.9C15.0166 12.5 15.0166 11.9 15.4166 11.5L18.3166 8.6C19.2166 7.7 19.1166 6.30002 18.0166 5.50002C17.2166 4.90002 16.0166 5.10007 15.3166 5.80007L12.4166 8.69997C12.2166 8.89997 11.6166 8.90002 11.2166 8.50002ZM11.2166 15.6L8.51659 18.3001C7.81659 19.0001 6.71658 19.2 5.81658 18.6C4.81658 17.9 4.71659 16.4 5.51659 15.5L8.31658 12.7C8.71658 12.3 8.71658 11.7001 8.31658 11.3001L7.6166 10.6C7.2166 10.2 6.6166 10.2 6.2166 10.6L3.6166 13.2C1.7166 15.1 1.4166 18.1 3.1166 20.1C5.0166 22.4 8.51659 22.5 10.5166 20.5L13.3166 17.7C13.7166 17.3 13.7166 16.7001 13.3166 16.3001L12.6166 15.6C12.3166 15.2 11.6166 15.2 11.2166 15.6Z"
                                      fill="#50cd89"
                                    ></path>
                                    <path
                                      opacity="0.3"
                                      d="M5.0166 9L2.81659 8.40002C2.31659 8.30002 2.0166 7.79995 2.1166 7.19995L2.31659 5.90002C2.41659 5.20002 3.21659 4.89995 3.81659 5.19995L6.0166 6.40002C6.4166 6.60002 6.6166 7.09998 6.5166 7.59998L6.31659 8.30005C6.11659 8.80005 5.5166 9.1 5.0166 9ZM8.41659 5.69995H8.6166C9.1166 5.69995 9.5166 5.30005 9.5166 4.80005L9.6166 3.09998C9.6166 2.49998 9.2166 2 8.5166 2H7.81659C7.21659 2 6.71659 2.59995 6.91659 3.19995L7.31659 4.90002C7.41659 5.40002 7.91659 5.69995 8.41659 5.69995ZM14.6166 18.2L15.1166 21.3C15.2166 21.8 15.7166 22.2 16.2166 22L17.6166 21.6C18.1166 21.4 18.4166 20.8 18.1166 20.3L16.7166 17.5C16.5166 17.1 16.1166 16.9 15.7166 17L15.2166 17.1C14.8166 17.3 14.5166 17.7 14.6166 18.2ZM18.4166 16.3L19.8166 17.2C20.2166 17.5 20.8166 17.3 21.0166 16.8L21.3166 15.9C21.5166 15.4 21.1166 14.8 20.5166 14.8H18.8166C18.0166 14.8 17.7166 15.9 18.4166 16.3Z"
                                      fill="#50cd89"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.totalEventOnline}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Event Online
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-6 col-lg-3 mb-3">
                        <a
                          href="#"
                          className="card hoverable"
                          onClick={this.handleClickFilterOffline}
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-info mb-3 mt-1">
                                <span className="svg-icon svg-icon-info svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <path
                                      opacity="0.3"
                                      d="M18.0624 15.3453L13.1624 20.7453C12.5624 21.4453 11.5624 21.4453 10.9624 20.7453L6.06242 15.3453C4.56242 13.6453 3.76242 11.4453 4.06242 8.94534C4.56242 5.34534 7.46242 2.44534 11.0624 2.04534C15.8624 1.54534 19.9624 5.24534 19.9624 9.94534C20.0624 12.0453 19.2624 13.9453 18.0624 15.3453Z"
                                      fill="#7239EA"
                                    ></path>
                                    <path
                                      d="M12.0624 13.0453C13.7193 13.0453 15.0624 11.7022 15.0624 10.0453C15.0624 8.38849 13.7193 7.04535 12.0624 7.04535C10.4056 7.04535 9.06241 8.38849 9.06241 10.0453C9.06241 11.7022 10.4056 13.0453 12.0624 13.0453Z"
                                      fill="#7239EA"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.totalEventOffline}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Event Offline
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-6 col-lg-3 mb-3">
                        <a
                          href="#"
                          className="card hoverable"
                          onClick={this.handleClickFilterOnlineOffline}
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-primary mb-3 mt-1">
                                <span className="svg-icon svg-icon-primary svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <path
                                      d="M18 21.6C16.6 20.4 9.1 20.3 6.3 21.2C5.7 21.4 5.1 21.2 4.7 20.8L2 18C4.2 15.8 10.8 15.1 15.8 15.8C16.2 18.3 17 20.5 18 21.6ZM18.8 2.8C18.4 2.4 17.8 2.20001 17.2 2.40001C14.4 3.30001 6.9 3.2 5.5 2C6.8 3.3 7.4 5.5 7.7 7.7C9 7.9 10.3 8 11.7 8C15.8 8 19.8 7.2 21.5 5.5L18.8 2.8Z"
                                      fill="currentColor"
                                    ></path>
                                    <path
                                      opacity="0.3"
                                      d="M21.2 17.3C21.4 17.9 21.2 18.5 20.8 18.9L18 21.6C15.8 19.4 15.1 12.8 15.8 7.8C18.3 7.4 20.4 6.70001 21.5 5.60001C20.4 7.00001 20.2 14.5 21.2 17.3ZM8 11.7C8 9 7.7 4.2 5.5 2L2.8 4.8C2.4 5.2 2.2 5.80001 2.4 6.40001C2.7 7.40001 3.00001 9.2 3.10001 11.7C3.10001 15.5 2.40001 17.6 2.10001 18C3.20001 16.9 5.3 16.2 7.8 15.8C8 14.2 8 12.7 8 11.7Z"
                                      fill="currentColor"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.totalEventOnlineOffline}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Event Hybrid
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-12 mt-5">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Daftar Event
                        </h1>
                      </div>
                      <div className="card-toolbar">
                        <div className="ml-5 d-flex align-items-center position-relative my-1 me-2">
                          <span className="svg-icon svg-icon-1 position-absolute ms-6">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                              className="mh-50px"
                            >
                              <rect
                                opacity="0.5"
                                x="17.0365"
                                y="15.1223"
                                width="8.15546"
                                height="2"
                                rx="1"
                                transform="rotate(45 17.0365 15.1223)"
                                fill="#a1a5b7"
                              ></rect>
                              <path
                                d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                fill="#a1a5b7"
                              ></path>
                            </svg>
                          </span>
                          <input
                            type="text"
                            data-kt-user-table-filter="search"
                            className="form-control form-control-sm form-control-solid w-250px ps-14"
                            placeholder="Cari Event"
                            onKeyPress={this.handleKeyPress}
                            onChange={this.handleChangeSearch}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="card-body">
                      <div class="table-responsive">
                        <DataTable
                          columns={this.columns}
                          data={this.state.datax}
                          progressPending={this.state.loading}
                          highlightOnHover
                          pointerOnHover
                          pagination
                          paginationServer
                          paginationTotalRows={this.state.totalRows}
                          paginationComponentOptions={{
                            selectAllRowsItem: true,
                            selectAllRowsItemText: "Semua",
                          }}
                          paginationDefaultPage={this.state.currentPage}
                          onChangeRowsPerPage={(
                            currentRowsPerPage,
                            currentPage,
                          ) => {
                            this.handlePerRowsChange(
                              currentRowsPerPage,
                              currentPage,
                              "row-change",
                            );
                          }}
                          onChangePage={(page, totalRows) => {
                            this.handlePerRowsChange(
                              page,
                              totalRows,
                              "page-change",
                            );
                          }}
                          customStyles={this.customStyles}
                          persistTableHead={true}
                          onSort={this.handleSort}
                          noDataComponent={
                            <div className="mt-5">Tidak Ada Data</div>
                          }
                          sortServer
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* Pratinjau */}
        <div className="modal fade" tabindex="-1" id="pratinjau">
          <div className="modal-dialog modal-lg">
            <div className="modal-content">
              <div className="modal-header">
                <h5 class="modal-title">Pratinjau</h5>
                <div
                  class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <span class="svg-icon svg-icon-2x">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <rect
                        opacity="0.5"
                        x="6"
                        y="17.3137"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(-45 6 17.3137)"
                        fill="currentColor"
                      />
                      <rect
                        x="7.41422"
                        y="6"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(45 7.41422 6)"
                        fill="currentColor"
                      />
                    </svg>
                  </span>
                </div>
              </div>
              <div className="modal-body">
                <span class="badge badge-light-primary">
                  {this.state.pratinjau.kategori}
                </span>
                <br></br>
                <h3 className="my-3">{this.state.pratinjau.judul}</h3> <br />
                <img
                  src={
                    process.env.REACT_APP_BASE_API_URI +
                    "/publikasi/get-file?path=" +
                    this.state.pratinjau.gambar
                  }
                  width="100%"
                  className="rounded"
                />
                <div>
                  <div>
                    <small className="text-muted mr-2">
                      <i class="bi bi-calendar me-2"></i>
                      {this.state.pratinjau.tanggal_event ?? "-"}
                    </small>
                  </div>
                  <div>
                    <small className="text-muted">
                      <i className="bi bi-pin-map me-2"></i>
                      {this.state.pratinjau.lokasi}
                    </small>
                  </div>
                  <div>
                    <small className="text-muted">
                      <i className="bi bi-mic me-2"></i>
                      {this.state.pratinjau.jenis == 1
                        ? "Online"
                        : this.state.pratinjau.jenis == 2
                          ? "Hybird"
                          : "Offline"}
                    </small>
                  </div>
                </div>
                <div className="my-3">
                  <span className="fw-bolder mb-0 fs-6">Deskripsi</span>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: this.state.pratinjau.deskripsi,
                    }}
                  />
                  <div className="mt-2">
                    <div>
                      <strong
                        className="fw-bold"
                        style={{ fontWeight: "bold" }}
                      >
                        Pembicara:
                      </strong>
                      <ul>
                        {this.state.pratinjau.pembicara &&
                          this.state.pratinjau.pembicara.map((elem) => {
                            return <li>{elem}</li>;
                          })}
                      </ul>
                    </div>
                  </div>
                  {this.state.pratinjau && this.state.pratinjau.jenis != 0 && (
                    <div className="row mt-5">
                      <div className="col-12">
                        <span className="fw-bolder mb-0 fs-6">
                          Link (Zoom/Ms Team)
                        </span>
                        <div>
                          <a
                            className="btn btn-success btn-sm my-2"
                            target="_blank"
                            href={this.state.pratinjau.link}
                          >
                            Link Event
                          </a>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
                <div className="modal-footer">
                  <a
                    href={"/publikasi/edit-event/" + this.state.pratinjau.id}
                    className="btn btn-warning btn-sm me-3"
                  >
                    Edit
                  </a>
                  <button
                    className="btn btn-light btn-sm"
                    data-bs-dismiss="modal"
                  >
                    Close
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* Filter */}
        <div className="modal fade" tabindex="-1" id="filter">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">
                  <span className="svg-icon svg-icon-5 me-1">
                    <i className="bi bi-sliders text-black"></i>
                  </span>
                  Filter Data Event
                </h5>
                <div
                  class="btn btn-icon btn-sm btn-active-light-primary ms-2"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <span class="svg-icon svg-icon-2x">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <rect
                        opacity="0.5"
                        x="6"
                        y="17.3137"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(-45 6 17.3137)"
                        fill="currentColor"
                      />
                      <rect
                        x="7.41422"
                        y="6"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(45 7.41422 6)"
                        fill="currentColor"
                      />
                    </svg>
                  </span>
                </div>
              </div>
              <form action="#" onSubmit={this.handleClickFilter}>
                <input
                  type="hidden"
                  name="csrf-token"
                  value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                />
                <div class="modal-body">
                  <div class="fv-row form-group mb-7">
                    <label className="form-label">Kategori</label>
                    <Select
                      id="kategori"
                      name="kategori"
                      value={this.state.valKategoriFilter}
                      placeholder="Silahkan pilih Kategori"
                      noOptionsMessage={({ inputValue }) =>
                        !inputValue
                          ? this.state.dataxkategori
                          : "Data tidak tersedia"
                      }
                      className="form-select-sm selectpicker p-0"
                      options={this.state.dataxkategori}
                      onChange={this.handleChangeKategoriFilter}
                    />
                  </div>
                  <div className="fv-row form-group mb-7">
                    <label className="form-label">Status</label>
                    <Select
                      id="status"
                      name="status"
                      value={this.state.valStatusFilter}
                      placeholder="Silahkan pilih Status"
                      noOptionsMessage={({ inputValue }) =>
                        !inputValue ? this.state.status : "Data tidak tersedia"
                      }
                      className="form-select-sm selectpicker p-0"
                      options={this.status}
                      onChange={this.handleChangeStatusFilter}
                    />
                  </div>
                  <div className="fv-row form-group mb-7">
                    <label className="form-label">Jenis Event</label>
                    <Select
                      id="jenis"
                      name="jenis"
                      value={this.state.valStatusEventFilter}
                      placeholder="Silahkan pilih Jenis Event"
                      noOptionsMessage={({ inputValue }) =>
                        !inputValue
                          ? this.state.jenis_event
                          : "Data tidak tersedia"
                      }
                      className="form-select-sm selectpicker p-0"
                      options={this.option_jenis_event}
                      onChange={this.handleChangeStatusEventFilter}
                    />
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="d-flex justify-content-between">
                    <button
                      type="reset"
                      class="btn btn-sm btn-light me-3"
                      onClick={this.handleClickReset}
                    >
                      Reset
                    </button>
                    <button
                      type="submit"
                      class="btn btn-sm btn-primary"
                      data-bs-dismiss="modal"
                    >
                      Apply Filter
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
