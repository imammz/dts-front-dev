import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Select from "react-select";
import Flatpickr from "react-flatpickr";
import { Indonesian } from "flatpickr/dist/l10n/id.js";
import { kuotaNumericOnly } from "../helper";
import { handleFormatDate } from "./../../pelatihan/Pelatihan/helper";
import Editor from "@arbor-dev/ckeditor5-arbor-custom";
export default class EventTambahContent extends React.Component {
  constructor(props) {
    super(props);
    this.formEventRef = React.createRef(null);
    this.fileThumbnailRef = React.createRef(null);
    this.formDatax = new FormData();
    this.handleClick = this.handleClickAction.bind(this);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleChangeThumbnail = this.handleChangeThumbnailAction.bind(this);
    this.handleChangeIsi = this.handleChangeIsiAction.bind(this);
    this.state = {
      datax: [],
      loading: true,
      isLoading: false,
      dataxkategori: [],
      fields: {},
      errors: {},
      valJenisEvent: null,
      inputPembicara: [
        {
          id: 0,
          nama: "",
          occupation: "",
        },
      ],
      pembicaras: [],
      deskripsi: "",
      valTanggalMulaiEvent: "",
      valTanggalSelesaiEvent: "",
      valTanggalPublish: "",
      valKategoriEvent: "",
      valStatus: "",
      sizeSquare: {
        width: 844,
        height: 650,
      },
      message_resolusi: "Ukuran Image Size Harus 844 x 650",
    };
    this.formDatax = new FormData();
    this.handleAddInput = this.addInput.bind(this);
  }

  addInput = () => {
    const lastId =
      this.state.inputPembicara[this.state.inputPembicara.length - 1].id;
    this.setState({
      inputPembicara: [
        ...this.state.inputPembicara,
        {
          id: lastId + 1,
          nama: "",
          occupation: "",
        },
      ],
    });
  };

  handleDeleteField = (index) => {
    let formValues = this.state.inputPembicara;
    formValues.splice(index, 1);
    this.setState({ inputPembicara: formValues });
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  handleChangeIsiAction = (value) => {
    this.setState({ deskripsi: value });
  };

  // optionstatuspendaftaran = [
  //     { value: '1', label: 'Buka' },
  //     { value: '0', label: 'Tutup' }
  // ];

  optionstatus = [
    { value: "1", label: "Publish" },
    { value: "0", label: "Unpublish" },
  ];

  optionjenisevent = [
    { value: "1", label: "Online" },
    { value: "0", label: "Offline" },
    { value: "2", label: "Online dan Offline" },
  ];
  handleChangeThumbnailAction(e) {
    const errors = this.state.errors;

    const logofile = e.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(logofile);
    reader.onload = function (ol) {
      const image = new Image();
      image.src = ol.target.result;
      let sizeSquare = this.state.sizeSquare;
      let messagex = this.state.message_resolusi;
      document.addEventListener("imageLoad", (e) => {
        const { height, width } = e.detail;
        if (sizeSquare.height != height || sizeSquare.width != width) {
          swal
            .fire({
              title: "Wrong Image Size",
              text: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
              allowOutsideClick: false,
            })
            .then((result) => {
              this.fileThumbnailRef.current.value = null;
            });
          this.formDatax.delete("thumbnail");
          errors["thumbnail"] = this.state.message_resolusi;
          this.setState({
            errors,
          });
        } else {
          delete errors["thumbnail"];
          this.setState({
            errors,
          });
          this.formDatax.append("thumbnail", logofile);
        }
      });
      image.onload = function () {
        let customEvent = new CustomEvent("imageLoad", {
          detail: { width: this.width, height: this.height },
        });

        document.dispatchEvent(customEvent);
      };
    }.bind(this);
  }
  handleClickBatalAction(e) {
    swal
      .fire({
        title: "Apakah Anda Yakin?",
        icon: "warning",
        confirmButtonText: "Ya",
        showCancelButton: true,
        cancelButtonText: "Tidak",
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
      })
      .then((result) => {
        if (result.isConfirmed) {
          window.location = "/publikasi/event";
        }
      });
  }

  handleInputPembicara = (field, payload, id) => {
    this.state.inputPembicara.map((elem) => {
      if (elem.id == id) {
        return (elem[field] = payload);
      }
    });
  };

  handleClickAction(e) {
    const dataForm = new FormData(e.currentTarget);
    e.preventDefault();
    // this.resetError();
    if (this.validate(e.target)) {
      this.setState({ isLoading: true });
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });
      let today = new Date();
      let tanggal_publish =
        today.getFullYear() +
        "-" +
        (today.getMonth() + 1) +
        "-" +
        today.getDate();
      const dataFormSubmit = new FormData();
      dataFormSubmit.append("kategori_id", dataForm.get("id_kategori"));
      dataFormSubmit.append("users_id", Cookies.get("user_id"));
      if (this.formDatax.get("thumbnail")) {
        dataFormSubmit.append("gambar", this.formDatax.get("thumbnail"), true);
      }
      dataFormSubmit.append("quota_online", dataForm.get("kuota_online") ?? 0);
      dataFormSubmit.append(
        "quota_offline",
        dataForm.get("kuota_offline") ?? 0,
      );

      dataFormSubmit.append("judul_event", dataForm.get("judul"));
      dataFormSubmit.append(
        "tanggal_mulai",
        handleFormatDate(
          this.state.valTanggalMulaiEvent,
          "YYYY-MM-DD HH:mm:ss",
        ),
      );
      dataFormSubmit.append(
        "tanggal_selesai",
        handleFormatDate(
          this.state.valTanggalSelesaiEvent,
          "YYYY-MM-DD HH:mm:ss",
        ),
      );
      dataFormSubmit.append("lokasi", dataForm.get("lokasi"));

      dataFormSubmit.append("jenis", dataForm.get("jenis"));
      dataFormSubmit.append("link", dataForm.get("link"));
      dataFormSubmit.append("deskripsi_event", this.state.deskripsi);
      dataFormSubmit.append("tanggal_publish", tanggal_publish);
      const pembicaras = this.state.inputPembicara.map((elem) => {
        if (elem.nama?.length > 0) {
          return elem.occupation && elem.occupation != ""
            ? `${elem.nama} - ${elem.occupation}`
            : elem;
        }
      });
      if (pembicaras.length > 0) {
        dataFormSubmit.append("pembicara", pembicaras.join(";"));
      }
      dataFormSubmit.append("publish", dataForm.get("status"));
      dataFormSubmit.append("status_event", 2);

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/publikasi/tambah-event",
          dataFormSubmit,
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                allowOutsideClick: false,
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/publikasi/event";
                }
              });
          } else {
            this.setState({ isLoading: false });
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          console.log(error);
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          // if (!statux) {
          swal
            .fire({
              title: messagex ?? error.message,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
          // }
        });
    } else {
      this.setState({ isLoading: false });
      swal.fire({
        title: "Masih Terdapat Error Pada Isian!",
        icon: "warning",
      });
    }
  }
  validate(formElement) {
    const errors = this.state.errors;
    if (this.state.deskripsi == "" || this.state.deskripsi == null) {
      errors["isi"] = "Tidak Boleh Kosong";
    }
    if (
      this.state.valTanggalMulaiEvent == "" ||
      this.state.valTanggalMulaiEvent == null
    ) {
      errors["tanggal_mulai_event"] = "Tidak Boleh Kosong";
    }
    if (
      this.state.valTanggalSelesaiEvent == "" ||
      this.state.valTanggalSelesaiEvent == null
    ) {
      errors["tanggal_selesai_event"] = "Tidak Boleh Kosong";
    }

    this.setState({ errors: errors }, () => {
      const inputs = formElement.querySelectorAll("input");
      inputs.forEach((input) => {
        input.focus();
        input.blur();
      });
    });
    console.log(this.state.errors);
    return Object.keys(this.state.errors).length == 0;
  }
  validURL(str, key) {
    const errors = this.state.errors;
    var pattern = new RegExp(
      "^(https?:\\/\\/)?" + // protocol
        "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
        "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
        "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
        "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
        "(\\#[-a-z\\d_]*)?$",
      "i",
    ); // fragment locator
    if (!!pattern.test(str)) {
      delete errors[key];
      this.setState({ errors });
    } else {
      errors[key] = "Bukan Valid URL";
      this.setState({ errors });
    }
  }
  emptyValidation(value, key) {
    const errors = this.state.errors;
    if (value == "" || value == null) {
      errors[key] = "Tidak boleh kosong";
      this.setState({ errors });
      return Promise.reject("Tidak boleh kosong");
    } else {
      delete errors[key];
      this.setState({ errors });
      return Promise.resolve(value);
    }
  }
  positivenumberValidation(value, key) {
    const errors = this.state.errors;
    if (parseInt(value, 0) < 1) {
      errors[key] = "Angka tidak boleh kurang dari 1";
      this.setState({ errors });
      return Promise.reject("Angka tidak boleh kurang dari 1");
    } else {
      delete errors[key];
      this.setState({ errors });
      return Promise.resolve(value);
    }
  }

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    this.handleReload();
  }

  handleReload(page, newPerPage) {
    const dataKategorik = { start: 0, length: 100, jenis_kategori: "Event" };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/list-kategori",
        dataKategorik,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxkategori = [];
        optionx.map((data) =>
          dataxkategori.push({ value: data.id, label: data.nama }),
        );
        this.setState({ dataxkategori });
      });
  }

  render() {
    let rowCounter = 1;
    const styleImg = {
      width: "40px",
      height: "30px",
    };
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="32"
                      height="32"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        opacity="0.3"
                        d="M12.9 10.7L3 5V19L12.9 13.3C13.9 12.7 13.9 11.3 12.9 10.7Z"
                        fill="currentColor"
                      ></path>
                      <path
                        d="M21 10.7L11.1 5V19L21 13.3C22 12.7 22 11.3 21 10.7Z"
                        fill="#f1416c"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Publikasi
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Event
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Tambah Event
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <form
                        className="form"
                        action="#"
                        onSubmit={this.handleClick}
                        ref={this.formEventRef}
                      >
                        <input
                          type="hidden"
                          name="csrf-token"
                          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                        />
                        <div className="row justify-content-center">
                          <div className="form-group fv-row mb-7 col-12">
                            <label className="form-label required">
                              Kategori
                            </label>
                            <Select
                              id="id_kategori"
                              name="id_kategori"
                              placeholder="Silahkan pilih"
                              noOptionsMessage={() => "Data tidak tersedia"}
                              className="form-select-sm selectpicker p-0"
                              //onChange={this.handleChangeAkademi}
                              options={this.state.dataxkategori}
                              onBlur={(e) => {
                                this.emptyValidation(
                                  this.state.valKategoriEvent,
                                  "id_kategori",
                                );
                              }}
                              onChange={(selected) => {
                                this.emptyValidation(
                                  selected.value,
                                  "id_kategori",
                                );
                                this.setState({
                                  valKategoriEvent: selected.value,
                                });
                              }}
                            />
                            <span style={{ color: "red" }}>
                              {this.state.errors["id_kategori"]}
                            </span>
                          </div>

                          <div className="col-lg-12 mb-7 fv-row">
                            <label className="form-label required">
                              Judul Event
                            </label>
                            <input
                              className="form-control form-control-sm form-input"
                              placeholder="Masukkan Judul Disini"
                              name="judul"
                              onChange={(e) => {
                                this.emptyValidation(e.target.value, "judul");
                              }}
                              onBlur={(e) => {
                                this.emptyValidation(e.target.value, "judul");
                              }}
                            />
                            <span style={{ color: "red" }}>
                              {this.state.errors["judul"]}
                            </span>
                          </div>

                          <div className="form-group fv-row mb-7">
                            <label className="form-label">Banner Event</label>
                            <input
                              ref={this.fileThumbnailRef}
                              type="file"
                              className="form-control form-control-sm font-size-h4"
                              name="thumbnail"
                              id="thumbnail"
                              accept=".png,.jpg,.jpeg,.svg"
                              onChange={(e) => {
                                this.handleChangeThumbnail(e);
                                // this.emptyValidation(e.target.value, "thumbnail");
                              }}
                              onBlur={(e) => {
                                // this.emptyValidation(e.target.value, "thumbnail");
                              }}
                            />
                            <small className="text-muted">
                              {this.state.message_resolusi}
                            </small>
                            <div>
                              <span style={{ color: "red" }}>
                                {this.state.errors["thumbnail"]}
                              </span>
                            </div>
                          </div>

                          <div className="form-group fv-row mb-7">
                            <label className="form-label required">
                              Deskripsi Event
                            </label>
                            <CKEditor
                              editor={Editor}
                              className="form-input"
                              name="deskripsi"
                              config={{
                                ckfinder: {
                                  // Upload the images to the server using the CKFinder QuickUpload command.
                                  uploadUrl:
                                    process.env.REACT_APP_BASE_API_URI +
                                    "/publikasi/ckeditor-upload-image",
                                },
                              }}
                              onReady={(editor) => {
                                // You can store the "editor" and use when it is needed.
                              }}
                              // onChange={ ( event, editor ) => {
                              //     const data = editor.getData();
                              //     console.log( { event, editor, data } );
                              // } }
                              onChange={(event, editor) => {
                                this.emptyValidation(editor.getData(), "isi");
                                const data = editor.getData();
                                this.handleChangeIsi(data);
                              }}
                              onBlur={(event, editor) => {
                                this.emptyValidation(editor.getData(), "isi");
                              }}
                              onFocus={(event, editor) => {}}
                            />
                            <span style={{ color: "red" }}>
                              {this.state.errors["isi"]}
                            </span>
                          </div>

                          <div className="form-group fv-row mb-7">
                            <label className="form-label required">
                              Jenis Event
                            </label>
                            <Select
                              name="jenis"
                              id="jenis"
                              onBlur={(e) => {
                                this.emptyValidation(
                                  this.state.valJenisEvent,
                                  "jenis",
                                );
                              }}
                              onChange={(value) => {
                                this.emptyValidation(value.value, "jenis");
                                this.setState({
                                  valJenisEvent: value,
                                });
                              }}
                              placeholder="Silahkan pilih"
                              noOptionsMessage={() => "Data tidak tersedia"}
                              className="form-select-sm selectpicker p-0 form-input"
                              options={this.optionjenisevent}
                            />
                            <span style={{ color: "red" }}>
                              {this.state.errors["jenis"]}
                            </span>
                          </div>

                          {this.state.valJenisEvent &&
                            this.state.valJenisEvent.value != 0 && (
                              <div className="col-lg-12 mb-7 fv-row">
                                <label className="form-label required">
                                  Link Event
                                </label>
                                <input
                                  className="form-control form-control-sm form-input"
                                  placeholder="http://www.example.com"
                                  name="link"
                                  onBlur={(e) => {
                                    this.emptyValidation(
                                      e.target.value,
                                      "link",
                                    ).then((value) => {
                                      this.validURL(value, "link");
                                    });
                                  }}
                                  onChange={(e) => {
                                    this.emptyValidation(
                                      e.target.value,
                                      "link",
                                    ).then((value) => {
                                      this.validURL(value, "link");
                                    });
                                  }}
                                />
                                <span style={{ color: "red" }}>
                                  {this.state.errors["link"]}
                                </span>
                              </div>
                            )}

                          <div className="form-group col-lg-6 mb-7">
                            <label
                              className="form-label required"
                              htmlFor="title"
                            >
                              Tanggal Mulai Event
                            </label>
                            <br />
                            <div className="input-group date">
                              <Flatpickr
                                options={{
                                  locale: Indonesian,
                                  dateFormat: "d F Y, H:i",
                                  enableTime: "true",
                                }}
                                className="form-control form-control-sm form-input"
                                placeholder="Masukkan tanggal event"
                                name="tanggal_mulai_event"
                                value={this.state.valTanggalMulaiEvent}
                                onChange={(value) => {
                                  this.setState({
                                    valTanggalMulaiEvent: value[0],
                                  });
                                  this.emptyValidation(
                                    value[0],
                                    "tanggal_mulai_event",
                                  );
                                }}
                              />
                            </div>
                            <span style={{ color: "red" }}>
                              {this.state.errors["tanggal_mulai_event"]}
                            </span>
                          </div>

                          <div className="col-lg-6 mb-7">
                            <label
                              className="form-label required"
                              htmlFor="title"
                            >
                              Tanggal Selesai Event
                            </label>
                            <br />
                            <div className="input-group date">
                              <Flatpickr
                                options={{
                                  locale: Indonesian,
                                  dateFormat: "d F Y, H:i",
                                  enableTime: "true",
                                  minDate:
                                    this.state.valTanggalMulaiEvent ?? "",
                                }}
                                disabled={this.state.valTanggalMulaiEvent == ""}
                                className="form-control form-control-sm form-input"
                                placeholder="Masukkan tanggal event"
                                name="tanggal_selesai_event"
                                value={this.state.valTanggalSelesaiEvent}
                                onChange={(value) => {
                                  this.setState({
                                    valTanggalSelesaiEvent: value[0],
                                  });

                                  this.emptyValidation(
                                    value[0],
                                    "tanggal_selesai_event",
                                  );
                                }}
                              />
                            </div>
                            <span style={{ color: "red" }}>
                              {this.state.errors["tanggal_selesai_event"]}
                            </span>
                          </div>

                          {(this.state.valJenisEvent?.value == 1 ||
                            this.state.valJenisEvent?.value == 2) && (
                            <div
                              className={`mb-7 fv-row ${
                                this.state.valJenisEvent?.value == 1
                                  ? "col-lg-12"
                                  : "col-lg-6"
                              }`}
                            >
                              <label className="form-label required">
                                Kuota Online
                              </label>
                              <input
                                className="form-control form-control-sm form-input"
                                placeholder="Masukkan kuota peserta event disini"
                                name="kuota_online"
                                onBlur={(e) => {
                                  this.emptyValidation(
                                    e.target.value,
                                    "kuota_online",
                                  ).then((value) => {
                                    this.positivenumberValidation(
                                      value,
                                      "kuota_online",
                                    );
                                  });
                                }}
                                onChange={(e) => {
                                  kuotaNumericOnly(e.target);
                                  this.emptyValidation(
                                    e.target.value,
                                    "kuota_online",
                                  ).then((value) => {
                                    this.positivenumberValidation(
                                      value,
                                      "kuota_online",
                                    );
                                  });
                                }}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["kuota_online"]}
                              </span>
                            </div>
                          )}

                          {(this.state.valJenisEvent?.value == 0 ||
                            this.state.valJenisEvent?.value == 2) && (
                            <div
                              className={`mb-7 fv-row ${
                                this.state.valJenisEvent?.value == 0
                                  ? "col-lg-12"
                                  : "col-lg-6"
                              }`}
                            >
                              <label className="form-label required">
                                Kuota Offline
                              </label>
                              <input
                                className="form-control form-control-sm form-input"
                                placeholder="Masukkan kuota peserta event disini"
                                name="kuota_offline"
                                onBlur={(e) => {
                                  this.emptyValidation(
                                    e.target.value,
                                    "kuota_offline",
                                  ).then((value) => {
                                    this.positivenumberValidation(
                                      value,
                                      "kuota_offline",
                                    );
                                  });
                                }}
                                onChange={(e) => {
                                  kuotaNumericOnly(e.target);
                                  this.emptyValidation(
                                    e.target.value,
                                    "kuota_offline",
                                  ).then((value) => {
                                    this.positivenumberValidation(
                                      value,
                                      "kuota_offline",
                                    );
                                  });
                                }}
                              />
                              <span style={{ color: "red" }}>
                                {this.state.errors["kuota_offline"]}
                              </span>
                            </div>
                          )}

                          {this.state.valJenisEvent &&
                            this.state.valJenisEvent.value != 1 && (
                              <div className="col-lg-12 mb-7 fv-row">
                                <label
                                  className={`form-label ${
                                    this.state.valJenisEvent.value == 0
                                      ? "required"
                                      : ""
                                  }`}
                                >
                                  Lokasi
                                </label>
                                <input
                                  className="form-control form-control-sm form-input"
                                  placeholder="Masukkan lokasi event disini"
                                  name="lokasi"
                                  onBlur={(e) => {
                                    if (this.state.valJenisEvent == 1) {
                                      this.emptyValidation(
                                        e.target.value,
                                        "lokasi",
                                      );
                                    }
                                  }}
                                  onChange={(e) => {
                                    if (this.state.valJenisEvent == 1) {
                                      this.emptyValidation(
                                        e.target.value,
                                        "lokasi",
                                      );
                                    }
                                  }}
                                />
                                <span style={{ color: "red" }}>
                                  {this.state.errors["lokasi"]}
                                </span>
                              </div>
                            )}

                          <div className="form-group fv-row mb-7">
                            <label className="form-label required">
                              Status
                            </label>
                            <Select
                              name="status"
                              placeholder="Silahkan pilih"
                              noOptionsMessage={() => "Data tidak tersedia"}
                              className="form-select-sm selectpicker p-0 form-input"
                              options={this.optionstatus}
                              onChange={(e) => {
                                this.emptyValidation(e.value, "status");
                                this.setState({ valStatus: e.value });
                              }}
                              onBlur={(e) => {
                                this.emptyValidation(
                                  this.state.valStatus,
                                  "status",
                                );
                              }}
                            />
                            <span style={{ color: "red" }}>
                              {this.state.errors["status"]}
                            </span>
                          </div>
                        </div>
                        <div className="my-5 border-top"></div>
                        <h5 className="mb-5">Pembicara / Figur</h5>
                        {this.state.inputPembicara.map((elem, index) => {
                          return (
                            <div className="row align-items-center mb-2">
                              <div className="col-5">
                                <div className="form-group fv-row">
                                  <input
                                    className="form-control form-control-sm pembicara form-input"
                                    placeholder="Nama pembicara"
                                    defaultValue={elem.nama}
                                    onBlur={(evt) => {
                                      // this.emptyValidation(
                                      //   evt.target.value,
                                      //   `pembicara_${elem.id}`
                                      // );
                                      this.handleInputPembicara(
                                        "nama",
                                        evt.target.value,
                                        elem.id,
                                      );
                                    }}
                                    onChange={(evt) => {
                                      // this.emptyValidation(
                                      //   evt.target.value,
                                      //   `pembicara_${elem.id}`
                                      // );
                                    }}
                                    key={`pembicara_${elem.id}`}
                                    name={`pembicara_${elem.id}`}
                                  />
                                  <span style={{ color: "red" }}>
                                    {this.state.errors[`pembicara_${elem.id}`]}
                                  </span>
                                </div>
                              </div>
                              <div
                                className="col-1 mb-0"
                                style={{ textAlign: "center" }}
                              >
                                <p className="fw-bolder fs-1 text-muted  mb-0">
                                  -
                                </p>
                              </div>
                              <div className={index == 0 ? "col-6" : "col-5"}>
                                <div className="form-group fv-row">
                                  <input
                                    className="form-control form-control-sm pembicara"
                                    placeholder="Occupation"
                                    defaultValue={elem.occupation}
                                    onBlur={(evt) => {
                                      this.handleInputPembicara(
                                        "occupation",
                                        evt.target.value,
                                        elem.id,
                                      );
                                    }}
                                    key={`occupation_${elem.id}`}
                                    name={`occupation_${elem.id}`}
                                  />

                                  <span style={{ color: "red" }}>
                                    {this.state.errors["occupation"]}
                                  </span>
                                </div>
                              </div>
                              <div className="col-1 center">
                                {index != 0 && (
                                  <a
                                    className="btn btn-danger btn-sm btn-block"
                                    onClick={() => {
                                      this.handleDeleteField(index);
                                    }}
                                    style={{ width: "100%" }}
                                  >
                                    <span
                                      className="fw-bold"
                                      style={{ fontWeight: "bold!important" }}
                                    >
                                      <i className="bi bi-trash"></i>
                                    </span>
                                  </a>
                                )}
                              </div>
                            </div>
                          );
                        })}
                        <div className="row">
                          <div className="mt-3">
                            <a
                              className="btn btn-light text-success d-block fw-semibold btn-block btn-sm"
                              onClick={() => {
                                this.handleAddInput();
                              }}
                            >
                              <i className="bi text-success fw-semibold bi-plus-circle"></i>
                              Tambah
                            </a>
                          </div>
                        </div>
                        <div className="form-group fv-row pt-7 my-7">
                          <div className="d-flex justify-content-center mb-7">
                            <button
                              onClick={this.handleClickBatal}
                              type="reset"
                              className="btn btn-md btn-light me-3"
                              data-kt-menu-dismiss="true"
                            >
                              Batal
                            </button>
                            <button
                              type="submit"
                              className="btn btn-primary btn-md"
                              id="submitQuestion1"
                              disabled={this.state.isLoading}
                            >
                              {this.state.isLoading ? (
                                <>
                                  <span
                                    className="spinner-border spinner-border-sm me-2"
                                    role="status"
                                    aria-hidden="true"
                                  ></span>
                                  <span className="sr-only">Loading...</span>
                                  Loading...
                                </>
                              ) : (
                                <>
                                  <i className="fa fa-paper-plane me-1"></i>
                                  Simpan
                                </>
                              )}
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
