import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../../event/Event-Tambah-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const EventTambah = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default EventTambah;
