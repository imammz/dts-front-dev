export function indonesianDateFormat(formatTanggal, separator = "-") {
  let text = "";
  if (formatTanggal) {
    const bulanIndo = [
      "",
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "Mei",
      "Jun",
      "Jul",
      "Agt",
      "Sept",
      "Okt",
      "Nov",
      "Des",
    ];
    const splitTanggal = formatTanggal.split(separator);
    const tahun = splitTanggal[0];
    const bulan = splitTanggal[1];
    const tanggal = splitTanggal[2];
    text = tanggal + " " + bulanIndo[parseInt(bulan)] + " " + tahun;
  }
  return text;
}

export function indonesianDateFormat2(formatTanggal, separator = "-") {
  let text = "";
  if (formatTanggal) {
    const bulanIndo = [
      "",
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "Mei",
      "Jun",
      "Jul",
      "Agt",
      "Sept",
      "Okt",
      "Nov",
      "Des",
    ];
    const splitTanggal = formatTanggal.split(separator);
    const tahun = splitTanggal[2];
    const bulan = splitTanggal[1];
    const tanggal = splitTanggal[0];
    text = tanggal + " " + bulanIndo[parseInt(bulan)] + " " + tahun;
  }
  return text;
}

export function indonesianDateFormatWithoutTime(formatTanggal) {
  let text = "";
  if (formatTanggal) {
    const bulanIndo = [
      "",
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "Mei",
      "Jun",
      "Jul",
      "Agt",
      "Sept",
      "Okt",
      "Nov",
      "Des",
    ];
    const splitTanggal = formatTanggal.split("-");
    const tahun = splitTanggal[0];
    const bulan = splitTanggal[1];
    const tanggal = splitTanggal[2];

    const splitSpasiTgl = tanggal.split(" ");
    const tanggalParse = parseInt(splitSpasiTgl[0], 10);
    text = tanggalParse + " " + bulanIndo[parseInt(bulan)] + " " + tahun;
  }
  return text;
}

export function indonesianTimeFormat(dateTime) {
  let text = "";
  if (dateTime) {
    const time = new Date(dateTime);
    text =
      String(time.getHours()).padStart(2, "0") +
      ":" +
      String(time.getMinutes()).padStart(2, "0");
  }
  return text;
}

export function reverseIndonesianDateFormat(formatTanggal, time = false) {
  let text = "";
  if (formatTanggal) {
    const bulanIndo = [
      "",
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "Mei",
      "Jun",
      "Jul",
      "Agt",
      "Sept",
      "Okt",
      "Nov",
      "Des",
    ];
    const splitTanggal = time
      ? formatTanggal.split(", ")[0].split(" ")
      : formatTanggal.split(" ");
    const tahun = splitTanggal[2];
    const bulan = splitTanggal[1];
    const tanggal = splitTanggal[0];
    const indexBulan =
      bulanIndo.indexOf(bulan) < 10
        ? `0${bulanIndo.indexOf(bulan)}`
        : bulanIndo.indexOf(bulan);
    text = time
      ? tahun +
        "-" +
        indexBulan +
        "-" +
        tanggal +
        " " +
        formatTanggal.split(", ")[1]
      : tahun + "-" + indexBulan + "-" + tanggal;
  }
  return text;
}

export function dmyToYmd(dMy, separator = "-") {
  const splitDate = dMy.split(separator);
  const bulanEnglish = [
    "",
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  const tahun = splitDate[2];
  const bulan = splitDate[1];
  const tanggal = splitDate[0];
  const indexBulan =
    bulanEnglish.indexOf(bulan) < 10
      ? `0${bulanEnglish.indexOf(bulan)}`
      : bulanEnglish.indexOf(bulan);

  return tahun + "-" + indexBulan + "-" + tanggal;
}

export function ymdToDmy(ymd, separator = "-") {
  const splitDate = ymd.split(separator);
  const bulanEnglish = [
    "",
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  const tahun = splitDate[0];
  const bulan = splitDate[1];
  const tanggal = splitDate[2];
  const indexBulan =
    bulanEnglish.indexOf(bulan) < 10
      ? `0${bulanEnglish.indexOf(bulan)}`
      : bulanEnglish.indexOf(bulan);

  return tanggal + "-" + bulanEnglish[parseInt(bulan)] + "-" + tahun;
}

export function dmmmyEnglishIndo(dmmmy, separator = " ") {
  const splitDate = dmmmy.split(separator);
  const bulanEnglish = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  const months_indo = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "Mei",
    "Jun",
    "Jul",
    "Agt",
    "Sept",
    "Okt",
    "Nov",
    "Des",
  ];
  const tahun = splitDate[2];
  let bulan = splitDate[1];
  const tanggal = splitDate[0];

  for (let i = 0; i < bulanEnglish.length; i++) {
    if (bulan == bulanEnglish[i]) {
      bulan = months_indo[i];
    }
  }

  return tanggal + " " + bulan + " " + tahun;
}

export function dmmmyToYmd(tgl, separator = " ") {
  const splitDate = tgl.split(separator);
  const months_indo = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "Mei",
    "Jun",
    "Jul",
    "Agt",
    "Sept",
    "Okt",
    "Nov",
    "Des",
  ];
  const tahun = splitDate[2];
  const bulan = splitDate[1];
  const tanggal = splitDate[0];
  let indexbulan;
  for (let i = 0; i < months_indo.length; i++) {
    if (bulan == months_indo[i]) {
      indexbulan = i + 1;
    }
  }

  return tahun + "-" + indexbulan + "-" + tanggal;
}

export const dateDifference = (date) => {
  return Math.floor((new Date().getTime() - new Date(date)) / 86400000);
};

export const timeDifference = (time, hours) => {
  const timeDiff = new Date().getHours() - new Date(time).getHours();

  return 0 <= timeDiff && timeDiff <= hours;
};

export function capitalizeFirstLetter(string) {
  if (string != null) {
    let upp = "";
    try {
      upp = string.charAt(0).toUpperCase() + string.slice(1);
    } catch (err) {
      upp = "";
    }
    return upp;
  } else {
    return "";
  }
}

export function capitalizeTheFirstLetterOfEachWord(words) {
  if (words != null) {
    var separateWord = words.toLowerCase().split(" ");
    for (var i = 0; i < separateWord.length; i++) {
      separateWord[i] =
        separateWord[i].charAt(0).toUpperCase() + separateWord[i].substring(1);
    }
    return separateWord.join(" ");
  } else {
    return "";
  }
}

export function getYoutubeThumbnail(string) {
  let url_video = string.replace("https://youtube.com/watch?v=", "");
  url_video = string.replace("https://youtu.be/", "");
  url_video = string.replace("https://www.youtube.com/embed/", "");

  let results = url_video.match("[\\?&]v=([^&#]*)");
  let video = results == null ? url_video : results[1];
  return `http://img.youtube.com/vi/${video}/2.jpg`;
}

export function getDateTime() {
  const today = new Date();
  const date =
    today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
  const time =
    today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  const dateTime = date + " " + time;
  return dateTime;
}

export function customLoader() {
  return (
    <div style={{ padding: "24px" }}>
      <img src="/assets/media/loader/loader-biru.gif" />
    </div>
  );
}

export function numericOnly(elem) {
  elem.value = elem.value.replace(/[^\d*\.?\d*$]/, "");
}

export function numericOnlyDirect(elem) {
  return (elem = elem.replace(/[^\d*\.?\d*$]/, ""));
}

export function kuotaNumericOnly(elem) {
  elem.value = elem.value.replace(/[^\d]/, "");
}

export function kuotaNumericOnlyDirect(elem) {
  return (elem = elem.replace(/[^\d]/, ""));
}

export function statusPelaksanaan(start, end) {
  const today = new Date();
  const date_start = new Date(start);
  const date_end = new Date(end);
  let status = false;

  // 0 blm mulai, 1 sementara, 2 selesai
  if (today < date_start) {
    status = 0;
  } else if (today >= date_start && today <= date_end) {
    status = 1;
  } else if (today > date_end) {
    status = 2;
  }

  return status;
}

export function statusPelaksanaanWithLabel(start, end) {
  const today = new Date();
  const date_start = new Date(start);
  const date_end = new Date(end);
  let status = false;

  // 0 blm mulai, 1 sementara, 2 selesai
  if (today < date_start) {
    status = "Belum Mulai";
  } else if (today >= date_start && today <= date_end) {
    status = "Sedang Dilaksanakan";
  } else if (today > date_end) {
    status = "Selesai";
  }

  return status;
}

export function statusPelaksanaanWithLabel2(start, end) {
  const pelatihan_start_reform = start.split(" ")[0];
  const pelatihan_end_reform = end.split(" ")[0];

  const pelatihan_start_reform_2 = pelatihan_start_reform.split("-");
  const pelatihan_end_reform_2 = pelatihan_end_reform.split("-");

  const pelatihan_start_new =
    pelatihan_start_reform_2[2] +
    "-" +
    pelatihan_start_reform_2[1] +
    "-" +
    pelatihan_start_reform_2[0];
  const pelatihan_end_new =
    pelatihan_end_reform_2[2] +
    "-" +
    pelatihan_end_reform_2[1] +
    "-" +
    pelatihan_end_reform_2[0];

  const today = new Date();
  const date_start = new Date(pelatihan_start_new);
  const date_end = new Date(pelatihan_end_new);
  let status = false;

  // 0 blm mulai, 1 sementara, 2 selesai
  if (today < date_start) {
    status = "Belum Mulai";
  } else if (today >= date_start && today <= date_end) {
    status = "Sedang Dilaksanakan";
  } else if (today > date_end) {
    status = "Selesai";
  }

  return status;
}

export function colorStatusPelaksanaan(start, end) {
  const today = new Date();
  const date_start = new Date(start);
  const date_end = new Date(end);
  let status = false;

  // 0 blm mulai, 1 sementara, 2 selesai
  if (today < date_start) {
    status = "danger";
  } else if (today >= date_start && today <= date_end) {
    status = "primary";
  } else if (today > date_end) {
    status = "success";
  }

  return status;
}

export function colorStatusPelaksanaan2(start, end) {
  const pelatihan_start_reform = start.split(" ")[0];
  const pelatihan_end_reform = end.split(" ")[0];

  const pelatihan_start_reform_2 = pelatihan_start_reform.split("-");
  const pelatihan_end_reform_2 = pelatihan_end_reform.split("-");

  const pelatihan_start_new =
    pelatihan_start_reform_2[2] +
    "-" +
    pelatihan_start_reform_2[1] +
    "-" +
    pelatihan_start_reform_2[0];
  const pelatihan_end_new =
    pelatihan_end_reform_2[2] +
    "-" +
    pelatihan_end_reform_2[1] +
    "-" +
    pelatihan_end_reform_2[0];

  const today = new Date();
  const date_start = new Date(pelatihan_start_new);
  const date_end = new Date(pelatihan_end_new);
  let status = false;

  // 0 blm mulai, 1 sementara, 2 selesai
  if (today < date_start) {
    status = "danger";
  } else if (today >= date_start && today <= date_end) {
    status = "primary";
  } else if (today > date_end) {
    status = "success";
  }

  return status;
}

export function getColorClassStatusPendaftaran(status = "") {
  let s = status?.toLowerCase() || "";
  let color = "bg-yellow";
  switch (s) {
    case "submit":
      color = "primary";
      break;

    case "tdk. lulus administrasi":
      color = "danger";
      break;

    case "tes substansi":
      color = "primary";
      break;

    case "tdk. lulus tes substansi":
      color = "danger";
      break;

    case "tdk. lulus pelatihan - nilai":
      color = "danger";
      break;

    case "tdk. lulus pelatihan - kehadiran":
      color = "danger";
      break;

    case "tdk. lulus pelatihan - tidak hadir":
      color = "danger";
      break;

    case "seleksi akhir":
      color = "primary";
      break;

    case "ditolak":
      color = "danger";
      break;

    case "diterima":
      color = "success";
      break;

    case "pelatihan":
      color = "success";
      break;

    case "administrasi akhir":
      color = "primary";
      break;

    case "lulus pelatihan":
      color = "success";
      break;

    case "lulus pelatihan - kehadiran":
      color = "success";
      break;

    case "lulus pelatihan - nilai":
      color = "success";
      break;

    case "lulus test substansi":
      color = "success";
      break;

    case "tidak lulus pelatihan":
      color = "danger";
      break;

    case "cadangan":
      color = "warning";
      break;

    case "mengundurkan diri":
      color = "danger";
      break;

    case "pembatalan":
      color = "danger";
      break;

    case "banned":
      color = "danger";
      break;

    default:
      color = "primary";
      break;
  }
  return color;
}
