import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import Select from "react-select";
import "./assets/custom.css";
import {
  indonesianTimeFormat,
  indonesianDateFormat,
  capitalizeFirstLetter,
  timeDifference,
  dateDifference,
  getDateTime,
} from "../helper";

export default class SideWidgetEditContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickDelete = this.handleClickDeleteAction.bind(this);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleSearch = this.handleSearchAction.bind(this);
    this.handleClickNoFilter = this.handleClickNoFilterAction.bind(this);
    this.handleClickFilterPublish =
      this.handleClickFilterPublishAction.bind(this);
    this.handleClickPratinjau = this.handleClickPratinjauAction.bind(this);
    this.handleClickFilter = this.handleClickFilterAction.bind(this);
    this.handleClickReset = this.handleClickResetAction.bind(this);
    this.handleChangeStatusFilter =
      this.handleChangeStatusFilterAction.bind(this);
    this.handleSort = this.handleSortAction.bind(this);
    this.handleClickFilterUnpublish =
      this.handleClickFilterUnpublishAction.bind(this);
  }
  state = {
    datax: [],
    loading: true,
    totalRows: 0,
    newPerPage: 10,
    totalPublish: 0,
    totalSideWidget: 0,
    totalAuthor: 0,
    tempLastNumber: 0,
    currentPage: 0,
    statusPublish: "",
    valStatusFilter: [],
    pratinjau: [],
    column: "id",
    sortDirection: "desc",
    searchText: "",
  };

  handleClickPratinjauAction(e) {
    const judul = e.currentTarget.getAttribute("judul");
    const deskripsi = e.currentTarget.getAttribute("deskripsi");
    const tanggal_event = e.currentTarget.getAttribute("tanggal_event");
    const wording_button = e.currentTarget.getAttribute("wording_button");
    const link = e.currentTarget.getAttribute("link");
    const publish = e.currentTarget.getAttribute("publish");
    const pinned = e.currentTarget.getAttribute("pinned");
    const id = e.currentTarget.getAttribute("id");
    const icon = e.currentTarget.getAttribute("icon");

    this.setState(
      {
        pratinjau: {
          judul: judul,
          deskripsi: deskripsi,
          tanggal_event: tanggal_event,
          wording_button: wording_button,
          publish: publish,
          link: link,
          pinned: pinned,
          id: id,
          icon: icon,
        },
      },
      () => {},
    );
  }

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  option_jenis_event = [
    { value: "", label: "Pilih Semua" },
    { value: 1, label: "Online" },
    { value: 0, label: "Offline" },
    { value: 2, label: "Online dan Offline" },
  ];
  status = [
    { value: "", label: "Pilih Semua" },
    { value: 1, label: "Publish" },
    { value: 0, label: "Unpublish" },
  ];
  columns = [
    {
      name: "No",
      center: true,
      cell: (row, index) => this.state.tempLastNumber + index + 1,
      width: "70px",
    },
    {
      Header: "Widget",
      accessor: "",
      name: "Judul",
      sortable: true,
      className: "min-w-300px mw-300px",
      width: "320px",
      grow: 6,
      selector: (row, index) => capitalizeFirstLetter(row.judul_widget),
      cell: (row, index) => {
        return (
          <div>
            <label className="d-flex flex-stack cursor-pointer my-2">
              <a
                href="#"
                data-bs-toggle="modal"
                data-bs-target="#pratinjau"
                onClick={this.handleClickPratinjau}
                judul={row.judul_widget}
                deskripsi={row.deskripsi}
                tanggal_event={row.created_at}
                link={row.link}
                icon={row.icon}
                wording_button={row.wording_button}
                id={row.id}
                publish={row.publish}
                pinned={row.pinned}
                title="Preview"
                className="text-dark"
              >
                <span className="d-flex align-items-center me-2">
                  <i className={`${row.icon} text-primary fa-2x me-3`}></i>
                  <span className="fw-bolder fs-7">
                    {capitalizeFirstLetter(row.judul_widget)}
                  </span>
                </span>
              </a>
            </label>
          </div>
        );
      },
    },
    {
      name: "Tgl. Dibuat",
      sortable: true,
      className: "min-w-100px",
      grow: 2,
      selector: (row) => row.created_at,
      cell: (row, index) => {
        return (
          <label className="d-flex flex-stack mb- mt-1">
            <span className="d-flex align-items-center me-2">
              <span className="d-flex flex-column">
                <span className="mb-0 fs-7">
                  <p style={{ textTransform: "capitalize" }} className="mb-0">
                    {indonesianDateFormat(row.created_at.split(" ")[0])}
                  </p>
                </span>
                {/* <span className="fs-7 mt-0 text-muted">{indonesianTimeFormat(row.tanggal_event)}</span> */}
              </span>
            </span>
          </label>
        );
      },
    },
    {
      name: "Author",
      sortable: true,
      className: "min-w-100px",
      grow: 2,
      selector: (row) => capitalizeFirstLetter(row.user_name),
      cell: (row) => {
        return (
          <span className="d-flex flex-column">
            <span
              className="fs-7"
              style={{
                overflow: "hidden",
                whiteSpace: "wrap",
                textOverflow: "ellipses",
              }}
            >
              {capitalizeFirstLetter(row.user_name)}
            </span>
          </span>
        );
      },
    },
    {
      name: "Status",
      sortable: true,
      center: true,
      className: "min-w-100px",
      grow: 2,
      selector: (row, index) => row.publish,
      cell: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.publish == 1 ? "success" : "danger") +
              " fs-7 m-1"
            }
          >
            {row.publish == 1 ? "Publish" : "Unpublish"}
          </span>
        </div>
      ),
    },
    {
      name: "Aksi",
      center: true,
      width: "150px",
      cell: (row) => (
        <div>
          <a
            href="#"
            data-bs-toggle="modal"
            data-bs-target="#pratinjau"
            onClick={this.handleClickPratinjau}
            judul={row.judul_widget}
            deskripsi={row.deskripsi}
            tanggal_event={row.created_at}
            link={row.link}
            icon={row.icon}
            wording_button={row.wording_button}
            id={row.id}
            publish={row.publish}
            pinned={row.pinned}
            title="Preview"
            className="btn btn-icon btn-bg-primary btn-sm me-1"
            alt="Lihat"
          >
            <i className="bi bi-eye-fill text-white"></i>
          </a>
          <a
            href={"/publikasi/edit-side-widget/" + row.id}
            id={row.id}
            className="btn btn-icon btn-bg-warning btn-sm me-1"
            title="Edit"
            alt="Edit"
          >
            <i className="bi bi-gear-fill text-white"></i>
          </a>
          <a
            href="#"
            id={row.id}
            onClick={this.handleClickDelete}
            title="Hapus"
            className="btn btn-icon btn-bg-danger btn-sm me-1"
          >
            <i className="bi bi-trash-fill text-white"></i>
          </a>
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };
  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    this.handleReload();
  }

  handleClickNoFilterAction(e) {
    this.handleClickHeaderFilterAction({
      status: "",
    });
  }

  handleClickFilterPublishAction(e) {
    this.handleClickHeaderFilterAction({
      status: 1,
    });
  }

  handleClickFilterUnpublishAction(e) {
    this.handleClickHeaderFilterAction({
      status: 0,
    });
  }

  handleClickResetAction() {
    this.setState({ valStatusFilter: [] });
    this.setState(
      {
        statusPublish: "",
      },
      () => {
        this.handleReload();
      },
    );
  }
  handleChangeStatusFilterAction = (selectedOption) => {
    this.setState({
      valStatusFilter: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  };

  handleClickHeaderFilterAction = (payload) => {
    this.setState({ loading: true });
    this.setState({ statusPublish: payload.status });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/filter-widget",
        {
          status: payload.status,
          start: 0,
          length: this.state.newPerPage,
          search: this.state.searchText,
          sort_by: this.state.column,
          sort_val: this.state.sortDirection.toUpperCase(),
        },
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          const datax =
            res.data.result.Data[0] == null ? [] : res.data.result.Data;
          this.setState({ currentPage: 1 });
          this.setState({ tempLastNumber: 0 });
          this.setState({ datax });
          this.setState({ totalRows: res.data.result.TotalLength });
          this.setState({ loading: false });
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                // this.handleClickResetAction();
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      })
      .catch((error) => {
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      });
  };

  handleClickFilterAction(e) {
    const dataForm = new FormData(e.currentTarget);
    e.preventDefault();
    this.setState({ loading: true });
    const status = dataForm.get("status");
    dataForm.append("start", 0);
    dataForm.append("length", this.state.newPerPage);
    dataForm.append("search", this.state.searchText);
    dataForm.append("sort_by", this.state.column);
    dataForm.append("sort_val", this.state.sortDirection);
    if (status == null) {
      this.handleReload();
    } else {
      this.setState({ statusPublish: status });
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/publikasi/filter-widget",
          dataForm,
          this.configs,
        )
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            const datax =
              res.data.result.Data[0] == null ? [] : res.data.result.Data;
            this.setState({ currentPage: 1 });
            this.setState({ tempLastNumber: 0 });
            this.setState({ datax });
            this.setState({ totalRows: res.data.result.TotalLength });
            this.setState({ loading: false });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                this.setState({ datax: [] });
                this.setState({ loading: false });
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    }
  }

  handleClickDeleteAction(e) {
    const idx = e.currentTarget.id;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          const data = { id: idx };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI +
                "/publikasi/softdelete-widget",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload(
                        this.state.currentPage,
                        this.state.newPerPage,
                      );
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmationBUttonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload();
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload();
                    }
                  });
              }
            });
        }
      });
  }

  handleReload(page, newPerPage) {
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });
    const dataBody = {
      start: start_tmp,
      length: length_tmp,
      status: this.state.statusPublish,
      search: this.state.searchText,
      sort_by: this.state.column,
      sort_val: this.state.sortDirection.toUpperCase(),
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/filter-widget",
        dataBody,
        this.configs,
      )
      .then((res) => {
        this.setState({ loading: false });
        const datax =
          res.data.result.Data[0] == null ? [] : res.data.result.Data;
        this.setState({ datax });
        this.setState({ loading: false });
        this.setState({ totalRows: res.data.result.TotalLength });
        this.setState({ totalPublish: res.data.result.TotalPublish });
        this.setState({ totalSideWidget: res.data.result.Total });
        this.setState({ totalAuthor: res.data.result.TotalAuthor });
        this.setState({ currentPage: page });
      });
  }

  handlePageChange = (page) => {
    this.setState({ loading: true });
    this.handleReload(page, this.state.newPerPage);
  };
  handlePerRowsChange = async (newPerPage, page) => {
    this.setState({ loading: true });
    this.setState({ newPerPage: newPerPage });
    this.handleReload(page, newPerPage);
  };

  handleSortAction(column, sortDirection) {
    let server_name = "";
    if (column.name == "Judul") {
      server_name = "judul_widget";
    } else if (column.name == "Tgl. Dibuat") {
      server_name = "created_at";
    } else if (column.name == "Author") {
      server_name = "user_name";
    } else if (column.name == "Status") {
      server_name = "publish";
    }

    this.setState(
      {
        column: server_name,
        sortDirection: sortDirection,
      },
      () => {
        this.handleReload(1, this.state.newPerPage);
      },
    );
  }

  handleSearchAction(e) {
    const searchText = e.currentTarget.value;
    this.setState({ searchText }, () => {
      if (searchText == "") {
        this.handleReload();
      }
    });
  }
  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    this.setState({ searchText });
    if (e.key == "Enter") {
      this.setState({ loading: true });
      let data = {
        search: searchText,
        start: 0,
        length: this.state.newPerPage,
        sort_by: this.state.column,
        sort_val: this.state.sortDirection,
        status: this.state.statusPublish,
      };
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/publikasi/filter-widget",
          data,
          this.configs,
        )
        .then((res) => {
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            const datax =
              res.data.result.Data[0] == null ? [] : res.data.result.Data;
            this.setState({ currentPage: 1 });
            this.setState({ tempLastNumber: 0 });
            this.setState({ datax });
            this.setState({ totalRows: res.data.result.TotalLength });
            this.setState({ loading: false });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                this.setState({ datax: [] });
                this.setState({ loading: false });
              });
          }
        })
        .catch((error) => {
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    }
  }

  render() {
    let rowCounter = 1;
    const styleImg = {
      width: "40px",
      height: "30px",
    };
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="32"
                      height="32"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        opacity="0.3"
                        d="M12.9 10.7L3 5V19L12.9 13.3C13.9 12.7 13.9 11.3 12.9 10.7Z"
                        fill="currentColor"
                      ></path>
                      <path
                        d="M21 10.7L11.1 5V19L21 13.3C22 12.7 22 11.3 21 10.7Z"
                        fill="#f1416c"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Publikasi
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Side Widget
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                  data-bs-toggle="modal"
                  data-bs-target="#filter"
                >
                  <i className="bi bi-sliders"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Filter
                  </span>
                </button>

                <a
                  href="/publikasi/tambah-side-widget"
                  className="btn btn-success fw-bolder btn-sm"
                >
                  <i className="bi bi-plus-circle"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Tambah Widget
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-7"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 col-md-12 col-sm-12">
                    <div className="row">
                      <div className="col-xl-4 mb-3">
                        <a
                          href="#"
                          className="card hoverable"
                          onClick={this.handleClickNoFilter}
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-primary mb-3 mt-1">
                                <span className="svg-icon svg-icon-info svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <path
                                      d="M11.2929 2.70711C11.6834 2.31658 12.3166 2.31658 12.7071 2.70711L15.2929 5.29289C15.6834 5.68342 15.6834 6.31658 15.2929 6.70711L12.7071 9.29289C12.3166 9.68342 11.6834 9.68342 11.2929 9.29289L8.70711 6.70711C8.31658 6.31658 8.31658 5.68342 8.70711 5.29289L11.2929 2.70711Z"
                                      fill="currentColor"
                                    ></path>
                                    <path
                                      d="M11.2929 14.7071C11.6834 14.3166 12.3166 14.3166 12.7071 14.7071L15.2929 17.2929C15.6834 17.6834 15.6834 18.3166 15.2929 18.7071L12.7071 21.2929C12.3166 21.6834 11.6834 21.6834 11.2929 21.2929L8.70711 18.7071C8.31658 18.3166 8.31658 17.6834 8.70711 17.2929L11.2929 14.7071Z"
                                      fill="currentColor"
                                    ></path>
                                    <path
                                      opacity="0.3"
                                      d="M5.29289 8.70711C5.68342 8.31658 6.31658 8.31658 6.70711 8.70711L9.29289 11.2929C9.68342 11.6834 9.68342 12.3166 9.29289 12.7071L6.70711 15.2929C6.31658 15.6834 5.68342 15.6834 5.29289 15.2929L2.70711 12.7071C2.31658 12.3166 2.31658 11.6834 2.70711 11.2929L5.29289 8.70711Z"
                                      fill="currentColor"
                                    ></path>
                                    <path
                                      opacity="0.3"
                                      d="M17.2929 8.70711C17.6834 8.31658 18.3166 8.31658 18.7071 8.70711L21.2929 11.2929C21.6834 11.6834 21.6834 12.3166 21.2929 12.7071L18.7071 15.2929C18.3166 15.6834 17.6834 15.6834 17.2929 15.2929L14.7071 12.7071C14.3166 12.3166 14.3166 11.6834 14.7071 11.2929L17.2929 8.70711Z"
                                      fill="currentColor"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.totalSideWidget || 0}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Total Widget
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-xl-4 mb-3">
                        <a
                          href="#"
                          onClick={this.handleClickFilterPublish}
                          className="card hoverable"
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-success mb-3 mt-1">
                                <span className="svg-icon svg-icon-success svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <path
                                      d="M11.2929 2.70711C11.6834 2.31658 12.3166 2.31658 12.7071 2.70711L15.2929 5.29289C15.6834 5.68342 15.6834 6.31658 15.2929 6.70711L12.7071 9.29289C12.3166 9.68342 11.6834 9.68342 11.2929 9.29289L8.70711 6.70711C8.31658 6.31658 8.31658 5.68342 8.70711 5.29289L11.2929 2.70711Z"
                                      fill="#47BE7D"
                                    ></path>
                                    <path
                                      d="M11.2929 14.7071C11.6834 14.3166 12.3166 14.3166 12.7071 14.7071L15.2929 17.2929C15.6834 17.6834 15.6834 18.3166 15.2929 18.7071L12.7071 21.2929C12.3166 21.6834 11.6834 21.6834 11.2929 21.2929L8.70711 18.7071C8.31658 18.3166 8.31658 17.6834 8.70711 17.2929L11.2929 14.7071Z"
                                      fill="#47BE7D"
                                    ></path>
                                    <path
                                      opacity="0.3"
                                      d="M5.29289 8.70711C5.68342 8.31658 6.31658 8.31658 6.70711 8.70711L9.29289 11.2929C9.68342 11.6834 9.68342 12.3166 9.29289 12.7071L6.70711 15.2929C6.31658 15.6834 5.68342 15.6834 5.29289 15.2929L2.70711 12.7071C2.31658 12.3166 2.31658 11.6834 2.70711 11.2929L5.29289 8.70711Z"
                                      fill="#47BE7D"
                                    ></path>
                                    <path
                                      opacity="0.3"
                                      d="M17.2929 8.70711C17.6834 8.31658 18.3166 8.31658 18.7071 8.70711L21.2929 11.2929C21.6834 11.6834 21.6834 12.3166 21.2929 12.7071L18.7071 15.2929C18.3166 15.6834 17.6834 15.6834 17.2929 15.2929L14.7071 12.7071C14.3166 12.3166 14.3166 11.6834 14.7071 11.2929L17.2929 8.70711Z"
                                      fill="#47BE7D"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.totalPublish || 0}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Widget Publish
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-xl-4 mb-3">
                        <a
                          href="#"
                          onClick={this.handleClickFilterUnpublish}
                          className="card hoverable"
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-danger mb-3 mt-1">
                                <span className="svg-icon svg-icon-danger svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <path
                                      d="M11.2929 2.70711C11.6834 2.31658 12.3166 2.31658 12.7071 2.70711L15.2929 5.29289C15.6834 5.68342 15.6834 6.31658 15.2929 6.70711L12.7071 9.29289C12.3166 9.68342 11.6834 9.68342 11.2929 9.29289L8.70711 6.70711C8.31658 6.31658 8.31658 5.68342 8.70711 5.29289L11.2929 2.70711Z"
                                      fill="#F1416C"
                                    ></path>
                                    <path
                                      d="M11.2929 14.7071C11.6834 14.3166 12.3166 14.3166 12.7071 14.7071L15.2929 17.2929C15.6834 17.6834 15.6834 18.3166 15.2929 18.7071L12.7071 21.2929C12.3166 21.6834 11.6834 21.6834 11.2929 21.2929L8.70711 18.7071C8.31658 18.3166 8.31658 17.6834 8.70711 17.2929L11.2929 14.7071Z"
                                      fill="#F1416C"
                                    ></path>
                                    <path
                                      opacity="0.3"
                                      d="M5.29289 8.70711C5.68342 8.31658 6.31658 8.31658 6.70711 8.70711L9.29289 11.2929C9.68342 11.6834 9.68342 12.3166 9.29289 12.7071L6.70711 15.2929C6.31658 15.6834 5.68342 15.6834 5.29289 15.2929L2.70711 12.7071C2.31658 12.3166 2.31658 11.6834 2.70711 11.2929L5.29289 8.70711Z"
                                      fill="#F1416C"
                                    ></path>
                                    <path
                                      opacity="0.3"
                                      d="M17.2929 8.70711C17.6834 8.31658 18.3166 8.31658 18.7071 8.70711L21.2929 11.2929C21.6834 11.6834 21.6834 12.3166 21.2929 12.7071L18.7071 15.2929C18.3166 15.6834 17.6834 15.6834 17.2929 15.2929L14.7071 12.7071C14.3166 12.3166 14.3166 11.6834 14.7071 11.2929L17.2929 8.70711Z"
                                      fill="#F1416C"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.totalSideWidget -
                                    this.state.totalPublish || 0}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Widget Unpublish
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-12 mt-5">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Daftar Widget
                        </h1>
                      </div>
                      <div className="card-toolbar">
                        <div className="ml-5 d-flex align-items-center position-relative my-1 me-2">
                          <span className="svg-icon svg-icon-1 position-absolute ms-6">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                              className="mh-50px"
                            >
                              <rect
                                opacity="0.5"
                                x="17.0365"
                                y="15.1223"
                                width="8.15546"
                                height="2"
                                rx="1"
                                transform="rotate(45 17.0365 15.1223)"
                                fill="#a1a5b7"
                              ></rect>
                              <path
                                d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                fill="#a1a5b7"
                              ></path>
                            </svg>
                          </span>
                          <input
                            type="text"
                            data-kt-user-table-filter="search"
                            className="form-control form-control-sm form-control-solid w-250px ps-14"
                            placeholder="Cari Widget"
                            onKeyPress={this.handleKeyPress}
                            onChange={this.handleSearch}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="table-responsive">
                        <DataTable
                          columns={this.columns}
                          data={this.state.datax}
                          progressPending={this.state.loading}
                          highlightOnHover
                          pointerOnHover
                          pagination
                          paginationServer
                          paginationTotalRows={this.state.totalRows}
                          paginationComponentOptions={{
                            selectAllRowsItem: true,
                            selectAllRowsItemText: "Semua",
                          }}
                          paginationDefaultPage={this.state.currentPage}
                          onChangeRowsPerPage={this.handlePerRowsChange}
                          onChangePage={this.handlePageChange}
                          customStyles={this.customStyles}
                          persistTableHead={true}
                          onSort={this.handleSort}
                          noDataComponent={
                            <div className="mt-5">Tidak Ada Data</div>
                          }
                          sortServer
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* Pratinjau */}
        <div className="modal fade" tabindex="-1" id="pratinjau">
          <div className="modal-dialog modal-md">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Pratinjau</h5>
                <div
                  className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <span className="svg-icon svg-icon-2x">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <rect
                        opacity="0.5"
                        x="6"
                        y="17.3137"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(-45 6 17.3137)"
                        fill="currentColor"
                      />
                      <rect
                        x="7.41422"
                        y="6"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(45 7.41422 6)"
                        fill="currentColor"
                      />
                    </svg>
                  </span>
                </div>
              </div>
              <div className="modal-body">
                <div className="m-5" key={this.state.pratinjau.id}>
                  <div className="card border shadow lift px-5 pt-6 pb-8">
                    <div className="d-inline-block rounded-circle mb-4">
                      <div
                        className="d-flex justify-content-center align-items-center"
                        style={{
                          backgroundColor: "#c2dffb",
                          color: "#196ECD",
                          width: "4.1875rem",
                          height: "4.1875rem",
                          borderRadius: "50%",
                        }}
                      >
                        <i
                          className={`${this.state.pratinjau.icon} fa-2x text-primary`}
                        ></i>
                      </div>
                    </div>
                    <h5>{this.state.pratinjau.judul}</h5>
                    <div
                      className="pratinjau"
                      dangerouslySetInnerHTML={{
                        __html: this.state.pratinjau.deskripsi,
                      }}
                    ></div>
                    <a
                      href={
                        this.state.pratinjau?.link?.includes("http://")
                          ? this.state.pratinjau.link
                          : `//${this.state.pratinjau.link}`
                      }
                      target="blank"
                      className="btn btn-outline btn-primary btn-xs btn-pill mb-1 mt-5"
                    >
                      {this.state.pratinjau.wording_button}
                    </a>
                  </div>
                </div>
              </div>

              <div className="modal-footer">
                <a
                  href={
                    "/publikasi/edit-side-widget/" + this.state.pratinjau.id
                  }
                  className="btn btn-warning btn-sm me-3"
                >
                  Edit
                </a>
                <button
                  className="btn btn-light btn-sm"
                  data-bs-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
        {/* Filter */}
        <div className="modal fade" tabindex="-1" id="filter">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">
                  <span className="svg-icon svg-icon-5 me-1">
                    <i className="bi bi-sliders text-black"></i>
                  </span>
                  Filter Data Widget
                </h5>
                <div
                  className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <span className="svg-icon svg-icon-2x">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <rect
                        opacity="0.5"
                        x="6"
                        y="17.3137"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(-45 6 17.3137)"
                        fill="currentColor"
                      />
                      <rect
                        x="7.41422"
                        y="6"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(45 7.41422 6)"
                        fill="currentColor"
                      />
                    </svg>
                  </span>
                </div>
              </div>
              <form action="#" onSubmit={this.handleClickFilter}>
                <input
                  type="hidden"
                  name="csrf-token"
                  value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                />
                <div className="modal-body">
                  <div className="fv-row form-group mb-7">
                    <label className="form-label">Status</label>
                    <Select
                      id="status"
                      name="status"
                      value={this.state.valStatusFilter}
                      placeholder="Silahkan pilih Status"
                      noOptionsMessage={({ inputValue }) =>
                        !inputValue ? this.state.status : "Data tidak tersedia"
                      }
                      className="form-select-sm selectpicker p-0"
                      options={this.status}
                      onChange={this.handleChangeStatusFilter}
                    />
                  </div>
                </div>
                <div className="modal-footer">
                  <div className="d-flex justify-content-between">
                    <button
                      type="reset"
                      className="btn btn-sm btn-light me-3"
                      onClick={this.handleClickReset}
                    >
                      Reset
                    </button>
                    <button
                      type="submit"
                      className="btn btn-sm btn-primary"
                      data-bs-dismiss="modal"
                    >
                      Apply Filter
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
