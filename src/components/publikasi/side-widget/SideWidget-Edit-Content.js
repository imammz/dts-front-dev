import React, { useState } from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Select from "react-select";
import { reverseIndonesianDateFormat } from "../helper";
import FontAwesomeIcon from "./assets/fontawesome-icon.js";
import Editor from "@arbor-dev/ckeditor5-arbor-custom";
export default class SideWidgetEditContent extends React.Component {
  constructor(props) {
    super(props);
    this.formDatax = new FormData();
    this.handleClick = this.handleClickAction.bind(this);
    this.handleClickDelete = this.handleClickDeleteAction.bind(this);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleChangeStatus = this.handleChangeStatusAction.bind(this);
    // this.handleChangePinned = this.handleChangePinnedAction.bind(this);
    this.handleChangeIcon = this.handleChangeIconAction.bind(this);
    this.handleChangeDeskripsi = this.handleChangeDeskripsiAction.bind(this);
    this.state = {
      datax: [],
      isLoading: false,
      loading: true,
      fields: {},
      errors: {},
      id_widget: "",
      dataxwidget: [],
      // valPinnedWidget: [],
      valStatus: [],
      valIconWidget: [],
      deskripsi: "",
      iconOption: [],
      inputPembicara: [
        {
          id: 1,
          nama: "",
          occupation: "",
        },
      ],
    };
    this.handleAddInput = this.addInput.bind(this);
    this.handleHapusField = this.handleDeleteField.bind(this);
    this.formDatax = new FormData();
  }

  addInput = () => {
    const lastId =
      this.state.inputPembicara[this.state.inputPembicara.length - 1].id;
    this.setState({
      inputPembicara: [
        ...this.state.inputPembicara,
        {
          id: lastId + 1,
          nama: "",
          occupation: "",
        },
      ],
    });
  };

  handleDeleteField = (index) => {
    let formValues = this.state.inputPembicara;
    formValues.splice(index, 1);
    this.setState({ formValues });
  };

  handleInputPembicara = (field, payload, idx) => {
    this.state.inputPembicara[idx][field] = payload;
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  optionstatus = [
    { value: "1", label: "Publish" },
    { value: "0", label: "Unpublish" },
  ];

  handleChangeDeskripsiAction(value) {
    this.state.deskripsi = value;
  }

  handleClickBatalAction(e) {
    swal
      .fire({
        title: "Apakah Anda Yakin?",
        icon: "warning",
        confirmButtonText: "Ya",
        showCancelButton: true,
        cancelButtonText: "Tidak",
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
      })
      .then((result) => {
        if (result.isConfirmed) {
          window.location = "/publikasi/side-widget";
        }
      });
  }

  handleClickDeleteAction(e) {
    const idx = window.loca;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          const data = { id: this.state.id_widget };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI +
                "/publikasi/softdelete-widget",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    window.location = "/publikasi/side-widget";
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmationBUttonText: "Ok",
                  })
                  .then((result) => {});
              }
            })
            .catch((error) => {
              let statux = error.response.data.Status;
              let messagex = error.response.data.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    // window?.location?.reload();
                  });
              }
            });
        }
      });
  }
  handleClickAction(e) {
    const dataForm = new FormData(e.currentTarget);
    // this.resetError();
    e.preventDefault();
    if (this.validate(e.target)) {
      this.setState({ isLoading: true });
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });
      let today = new Date();
      let tanggal_publish =
        today.getFullYear() +
        "-" +
        (today.getMonth() + 1) +
        "-" +
        today.getDate();

      const dataFormSubmit = new FormData();
      dataFormSubmit.append("id", this.state.id_widget);
      dataFormSubmit.append("users_id", Cookies.get("user_id"));
      dataFormSubmit.append("judul_widget", dataForm.get("judul_widget"));
      dataFormSubmit.append("icon", this.state.valIconWidget.value);
      dataFormSubmit.append("link", dataForm.get("link"));
      dataFormSubmit.append("deskripsi", this.state.deskripsi);
      dataFormSubmit.append("wording_button", dataForm.get("wording_button"));
      dataFormSubmit.append("publish", this.state.valStatus.value);
      // dataFormSubmit.append("pinned", this.state.valPinnedWidget.value);

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/publikasi/update-widget",
          dataFormSubmit,
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                allowOutsideClick: false,
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/publikasi/side-widget";
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      this.setState({ isLoading: false });
      swal.fire({
        title: "Masih Terdapat Error Pada Isian!",
        icon: "warning",
      });
    }
  }

  validate(formElement) {
    const errors = this.state.errors;
    if (this.state.deskripsi == "" || this.state.deskripsi == null) {
      errors["deskripsi"] = "Tidak Boleh Kosong";
    }

    this.setState({ errors: errors }, () => {
      const inputs = formElement.querySelectorAll("input");
      inputs.forEach((input) => {
        input.focus();
        input.blur();
      });
    });
    return Object.keys(this.state.errors).length == 0;
  }
  validURL(str, key) {
    const errors = this.state.errors;
    var pattern = new RegExp(
      "^(https?:\\/\\/)?" + // protocol
        "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
        "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
        "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
        "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
        "(\\#[-a-z\\d_]*)?$",
      "i",
    ); // fragment locator
    if (!!pattern.test(str)) {
      delete errors[key];
      this.setState({ errors });
    } else {
      errors[key] = "Bukan Valid URL";
      this.setState({ errors });
    }
  }
  emptyValidation(value, key) {
    const errors = this.state.errors;
    if (value == "" || value == null) {
      errors[key] = "Tidak boleh kosong";
      this.setState({ errors });
      return Promise.reject("Tidak boleh kosong");
    } else {
      delete errors[key];
      this.setState({ errors });
      return Promise.resolve(value);
    }
  }

  // handleValidation(e) {
  //   const dataForm = new FormData(e.currentTarget);
  //   const fieldValidate = [
  //     "judul_widget",
  //     "link",
  //     "deskripsi",
  //     "icon",
  //     "wording_button",
  //     "publish",
  //   ];
  //   const check = this.checkEmpty(dataForm, fieldValidate);
  //   return check;
  // }

  // resetError() {
  //   let errors = {};
  //   errors[
  //     ("judul_widget", "link", "deskripsi", "icon", "wording_button", "publish")
  //   ] = "";
  //   this.setState({ errors: errors });
  // }

  // validURL(str) {
  //   var pattern = new RegExp(
  //     "^(https?:\\/\\/)?" + // protocol
  //       "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
  //       "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
  //       "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
  //       "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
  //       "(\\#[-a-z\\d_]*)?$",
  //     "i"
  //   ); // fragment locator
  //   return !!pattern.test(str);
  // }

  // checkEmpty(dataForm, fieldName) {
  //   let errors = {};
  //   let formIsValid = true;
  //   for (const field in fieldName) {
  //     const nameAttribute = fieldName[field];
  //     if (nameAttribute == "deskripsi" && this.state.deskripsi?.length == 0) {
  //       errors[nameAttribute] = "Tidak Boleh Kosong";
  //       formIsValid = false;
  //     }
  //     if (dataForm.get(nameAttribute) == "") {
  //       errors[nameAttribute] = "Tidak Boleh Kosong";
  //       formIsValid = false;
  //     }
  //   }
  //   if (!this.validURL(dataForm.get("link"))) {
  //     errors["link"] = "Bukan Valid URL";
  //     formIsValid = false;
  //   }
  //   this.setState({ errors: errors });
  //   return formIsValid;
  // }

  handleChangeStatusAction = (selectedOption) => {
    this.setState({
      valStatus: { value: selectedOption.value, label: selectedOption.label },
    });
  };

  // handleChangePinnedAction = (selectedOption) => {
  //     this.setState({ valPinnedWidget: { value: selectedOption.value, label: selectedOption.label } });
  // }

  handleChangeIconAction = (selectedOption) => {
    this.setState({
      valIconWidget: {
        value: selectedOption.value,
        label: selectedOption.label,
      },
    });
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    let segment_url = window.location.pathname.split("/");
    let id_widget = segment_url[3];
    this.setState({ id_widget: id_widget });
    let data = {
      id: id_widget,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/cari-widget",
        data,
        this.configs,
      )
      .then((res) => {
        const dataxwidget =
          res.data.result.Data[0] == null ? [] : res.data.result.Data[0];
        // this.setState({ valPinnedWidget: { value: dataxwidget.pinned, label: dataxwidget.pinned == 1 ? "Pinned" : "Unpinned" } });
        this.setState({
          valStatus: {
            value: dataxwidget.publish,
            label: dataxwidget.publish == 1 ? "Publish" : "Unpublish",
          },
        });
        this.setState({
          valIconWidget: { value: dataxwidget.icon, label: dataxwidget.icon },
        });
        this.setState({ dataxwidget }, function () {
          this.handleReload();
          console.log(this.state.dataxwidget);
        });
      });
  }

  handleReload(page, newPerPage) {
    const iconOption = FontAwesomeIcon.map((icon) => ({
      label: icon,
      value: icon,
    }));
    this.setState({ iconOption });
  }

  render() {
    let rowCounter = 1;
    const styleImg = {
      width: "40px",
      height: "30px",
    };
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="32"
                      height="32"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        opacity="0.3"
                        d="M12.9 10.7L3 5V19L12.9 13.3C13.9 12.7 13.9 11.3 12.9 10.7Z"
                        fill="currentColor"
                      ></path>
                      <path
                        d="M21 10.7L11.1 5V19L21 13.3C22 12.7 22 11.3 21 10.7Z"
                        fill="#f1416c"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Publikasi
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Side Widget
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary me-2"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>

                <button
                  className="btn btn-sm btn-danger btn-active-light-info"
                  onClick={this.handleClickDelete}
                >
                  <i className="bi bi-trash-fill text-white pe-1"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Hapus
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Edit Widget
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <form
                        className="form"
                        action="#"
                        onSubmit={this.handleClick}
                      >
                        <input
                          type="hidden"
                          name="csrf-token"
                          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                        />

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">
                            Judul Widget
                          </label>
                          <input
                            className="form-control form-control-sm"
                            placeholder="Masukkan Judul Disini"
                            name="judul_widget"
                            defaultValue={this.state.dataxwidget.judul_widget}
                            onChange={(e) => {
                              this.emptyValidation(
                                e.target.value,
                                "judul_widget",
                              );
                            }}
                            onBlur={(e) => {
                              this.emptyValidation(
                                e.target.value,
                                "judul_widget",
                              );
                            }}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["judul_widget"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">
                            Informasi Widget
                          </label>
                          <CKEditor
                            editor={Editor}
                            name="deskripsi"
                            data={this.state.dataxwidget.deskripsi}
                            config={{
                              ckfinder: {
                                // Upload the images to the server using the CKFinder QuickUpload command.
                                uploadUrl:
                                  process.env.REACT_APP_BASE_API_URI +
                                  "/publikasi/ckeditor-upload-image",
                              },
                            }}
                            onChange={(widget, editor) => {
                              const data = editor.getData();
                              this.handleChangeDeskripsi(data);
                              this.emptyValidation(data, "deskripsi");
                            }}
                            onBlur={(widget, editor) => {
                              this.emptyValidation(
                                editor.getData(),
                                "deskripsi",
                              );
                            }}
                            onFocus={(widget, editor) => {}}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["deskripsi"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          {/* pakai master icon keenthemes/metronic/fontawesome */}
                          <label className="form-label required">
                            Icon Widget
                          </label>
                          <Select
                            name="icon"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            options={this.state.iconOption}
                            onBlur={(e) => {
                              this.emptyValidation(
                                this.state.valIconWidget,
                                "icon",
                              );
                            }}
                            onChange={(value) => {
                              this.emptyValidation(value.value, "icon");
                              this.handleChangeIcon(value);
                            }}
                            getOptionLabel={(e) => (
                              <div
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                }}
                              >
                                <span>
                                  <i
                                    className={`${e.value} fa-2x text-primary`}
                                  ></i>
                                </span>
                                <span style={{ marginLeft: 5 }}>{e.label}</span>
                              </div>
                            )}
                            value={this.state.valIconWidget}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["icon"]}
                          </span>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">
                            Link Action Side Widget
                          </label>
                          <input
                            className="form-control form-control-sm"
                            placeholder="www.example.com"
                            name="link"
                            defaultValue={this.state.dataxwidget.link}
                            onBlur={(e) => {
                              this.emptyValidation(e.target.value, "link").then(
                                (value) => {
                                  this.validURL(value, "link");
                                },
                              );
                            }}
                            onChange={(e) => {
                              this.emptyValidation(e.target.value, "link").then(
                                (value) => {
                                  this.validURL(value, "link");
                                },
                              );
                            }}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["link"]}
                          </span>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">
                            Wording Button
                          </label>
                          <input
                            className="form-control form-control-sm"
                            placeholder="Pelajari"
                            name="wording_button"
                            defaultValue={this.state.dataxwidget.wording_button}
                            onChange={(e) => {
                              this.emptyValidation(
                                e.target.value,
                                "wording_button",
                              );
                            }}
                            onBlur={(e) => {
                              this.emptyValidation(
                                e.target.value,
                                "wording_button",
                              );
                            }}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["wording_button"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">Status</label>
                          <Select
                            name="publish"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            options={this.optionstatus}
                            value={this.state.valStatus}
                            onChange={(e) => {
                              this.emptyValidation(e.value, "publish");
                              this.handleChangeStatus(e);
                            }}
                            onBlur={(e) => {
                              this.emptyValidation(
                                this.state.valStatus,
                                "publish",
                              );
                            }}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["publish"]}
                          </span>
                        </div>

                        <div className="form-group fv-row pt-7 mb-7">
                          <div className="d-flex justify-content-center mb-7">
                            <button
                              onClick={this.handleClickBatal}
                              type="reset"
                              className="btn btn-md btn-light me-3"
                              data-kt-menu-dismiss="true"
                            >
                              Batal
                            </button>
                            <button
                              type="submit"
                              className="btn btn-primary btn-md"
                              id="submitQuestion1"
                              disabled={this.state.isLoading}
                            >
                              {this.state.isLoading ? (
                                <>
                                  <span
                                    className="spinner-border spinner-border-sm me-2"
                                    role="status"
                                    aria-hidden="true"
                                  ></span>
                                  <span className="sr-only">Loading...</span>
                                  Loading...
                                </>
                              ) : (
                                <>
                                  <i className="fa fa-paper-plane me-1"></i>
                                  Simpan
                                </>
                              )}
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
