import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Select from "react-select";
import { reverseIndonesianDateFormat } from "../helper";
import FontAwesomeIcon from "./assets/fontawesome-icon.js";
import Editor from "@arbor-dev/ckeditor5-arbor-custom";

export default class SideWidgetTambahContent extends React.Component {
  constructor(props) {
    super(props);
    this.formDatax = new FormData();
    this.handleClick = this.handleClickAction.bind(this);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleChangeIsi = this.handleChangeIsiAction.bind(this);
    this.state = {
      datax: [],
      isLoading: false,
      loading: true,
      fields: {},
      errors: {},
      deskripsi: "",
      iconOption: [],
      valIcon: "",
      valStatus: "",
    };
    this.formDatax = new FormData();
  }

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  handleChangeIsiAction = (value) => {
    this.state.deskripsi = value;
  };

  optionPublish = [
    { value: "1", label: "Publish" },
    { value: "0", label: "Unpublish" },
  ];

  handleClickBatalAction(e) {
    swal
      .fire({
        title: "Apakah Anda Yakin?",
        icon: "warning",
        confirmButtonText: "Ya",
        showCancelButton: true,
        cancelButtonText: "Tidak",
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
      })
      .then((result) => {
        if (result.isConfirmed) {
          window.location = "/publikasi/side-widget";
        }
      });
  }

  handleClickAction(e) {
    const dataForm = new FormData(e.currentTarget);
    e.preventDefault();
    // this.resetError();
    if (this.validate(e.target)) {
      this.setState({ isLoading: true });
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });

      const dataFormSubmit = new FormData();
      dataFormSubmit.append("users_id", Cookies.get("user_id"));
      dataFormSubmit.append("judul_widget", dataForm.get("judul_widget"));
      dataFormSubmit.append("icon", dataForm.get("icon"));
      dataFormSubmit.append("link", dataForm.get("link"));
      dataFormSubmit.append("deskripsi", this.state.deskripsi);
      dataFormSubmit.append("wording_button", dataForm.get("wording_button"));
      dataFormSubmit.append("publish", dataForm.get("publish"));
      // dataFormSubmit.append("pinned", dataForm.get("pinned"));

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/publikasi/tambah-widget",
          dataFormSubmit,
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                allowOutsideClick: false,
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/publikasi/side-widget";
                }
              });
          } else {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      this.setState({ isLoading: false });
      swal.fire({
        title: "Masih Terdapat Error Pada Isian!",
        icon: "warning",
      });
    }
  }

  validate(formElement) {
    const errors = this.state.errors;
    if (this.state.deskripsi == "" || this.state.deskripsi == null) {
      errors["deskripsi"] = "Tidak Boleh Kosong";
    }

    this.setState({ errors: errors }, () => {
      const inputs = formElement.querySelectorAll("input");
      inputs.forEach((input) => {
        input.focus();
        input.blur();
      });
    });
    return Object.keys(this.state.errors).length == 0;
  }
  validURL(str, key) {
    const errors = this.state.errors;
    var pattern = new RegExp(
      "^(https?:\\/\\/)?" + // protocol
        "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
        "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
        "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
        "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
        "(\\#[-a-z\\d_]*)?$",
      "i",
    ); // fragment locator
    if (!!pattern.test(str)) {
      delete errors[key];
      this.setState({ errors });
    } else {
      errors[key] = "Bukan Valid URL";
      this.setState({ errors });
    }
  }
  emptyValidation(value, key) {
    const errors = this.state.errors;
    if (value == "" || value == null) {
      errors[key] = "Tidak boleh kosong";
      this.setState({ errors });
      return Promise.reject("Tidak boleh kosong");
    } else {
      delete errors[key];
      this.setState({ errors });
      return Promise.resolve(value);
    }
  }

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    this.handleReload();
  }

  handleReload(page, newPerPage) {
    const iconOption = FontAwesomeIcon.map((icon) => ({
      label: icon,
      value: icon,
    }));
    this.setState({ iconOption });
  }

  render() {
    let rowCounter = 1;
    const styleImg = {
      width: "40px",
      height: "30px",
    };
    const formatOptionLabel = ({ value, label }) => (
      <div style={{ display: "flex", flexDirection: "row", columnGap: "5px" }}>
        {/* <div dangerouslySetInnerHTML={{ __html: value }}></div> */}
        <div>{value}</div>
        <div style={{ marginLeft: "10px" }}>{label}</div>
      </div>
    );
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="32"
                      height="32"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        opacity="0.3"
                        d="M12.9 10.7L3 5V19L12.9 13.3C13.9 12.7 13.9 11.3 12.9 10.7Z"
                        fill="currentColor"
                      ></path>
                      <path
                        d="M21 10.7L11.1 5V19L21 13.3C22 12.7 22 11.3 21 10.7Z"
                        fill="#f1416c"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Publikasi
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Side Widget
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Tambah Widget
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <form
                        className="form"
                        action="#"
                        onSubmit={this.handleClick}
                      >
                        <input
                          type="hidden"
                          name="csrf-token"
                          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                        />

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">
                            Judul Widget
                          </label>
                          <input
                            className="form-control form-control-sm"
                            placeholder="Masukkan Judul Disini"
                            name="judul_widget"
                            onChange={(e) => {
                              this.emptyValidation(
                                e.target.value,
                                "judul_widget",
                              );
                            }}
                            onBlur={(e) => {
                              this.emptyValidation(
                                e.target.value,
                                "judul_widget",
                              );
                            }}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["judul_widget"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">
                            Informasi Widget
                          </label>
                          <CKEditor
                            editor={Editor}
                            name="deskripsi"
                            onReady={(editor) => {}}
                            config={{
                              ckfinder: {
                                // Upload the images to the server using the CKFinder QuickUpload command.
                                uploadUrl:
                                  process.env.REACT_APP_BASE_API_URI +
                                  "/publikasi/ckeditor-upload-image",
                              },
                            }}
                            onChange={(event, editor) => {
                              const data = editor.getData();
                              this.emptyValidation(data, "deskripsi");
                              this.handleChangeIsi(data);
                            }}
                            onBlur={(event, editor) => {
                              this.emptyValidation(
                                editor.getData(),
                                "deskripsi",
                              );
                            }}
                            onFocus={(event, editor) => {}}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["deskripsi"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">
                            Icon Widget
                          </label>
                          <Select
                            name="icon"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={() => "Data tidak tersedia"}
                            className="form-select-sm selectpicker p-0"
                            options={this.state.iconOption}
                            onBlur={(e) => {
                              this.emptyValidation(this.state.valIcon, "icon");
                            }}
                            onChange={(value) => {
                              this.emptyValidation(value.value, "icon");
                              this.setState({
                                valIcon: value,
                              });
                            }}
                            getOptionLabel={(e) => (
                              <div
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                }}
                              >
                                <span>
                                  <i
                                    className={`${e.value} fa-2x text-primary`}
                                  ></i>
                                </span>
                                <span style={{ marginLeft: 5 }}>{e.label}</span>
                              </div>
                            )}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["icon"]}
                          </span>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">
                            Link Action Side Widget
                          </label>
                          <input
                            className="form-control form-control-sm"
                            placeholder="www.example.com"
                            name="link"
                            onBlur={(e) => {
                              this.emptyValidation(e.target.value, "link").then(
                                (value) => {
                                  this.validURL(value, "link");
                                },
                              );
                            }}
                            onChange={(e) => {
                              this.emptyValidation(e.target.value, "link").then(
                                (value) => {
                                  this.validURL(value, "link");
                                },
                              );
                            }}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["link"]}
                          </span>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">
                            Wording Button
                          </label>
                          <input
                            className="form-control form-control-sm"
                            placeholder="Pelajari"
                            name="wording_button"
                            onChange={(e) => {
                              this.emptyValidation(
                                e.target.value,
                                "wording_button",
                              );
                            }}
                            onBlur={(e) => {
                              this.emptyValidation(
                                e.target.value,
                                "wording_button",
                              );
                            }}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["wording_button"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">Status</label>
                          <Select
                            name="publish"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={() => "Data tidak tersedia"}
                            className="form-select-sm selectpicker p-0"
                            options={this.optionPublish}
                            onBlur={(e) => {
                              this.emptyValidation(
                                this.state.valStatus,
                                "publish",
                              );
                            }}
                            onChange={(value) => {
                              this.emptyValidation(value.value, "publish");
                              this.setState({
                                valStatus: value,
                              });
                            }}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["publish"]}
                          </span>
                        </div>

                        <div className="form-group fv-row pt-7 mb-7">
                          <div className="d-flex justify-content-center mb-7">
                            <button
                              onClick={this.handleClickBatal}
                              type="reset"
                              className="btn btn-md btn-light me-3"
                              data-kt-menu-dismiss="true"
                            >
                              Batal
                            </button>
                            <button
                              type="submit"
                              className="btn btn-primary btn-md"
                              id="submitQuestion1"
                              disabled={this.state.isLoading}
                            >
                              {this.state.isLoading ? (
                                <>
                                  <span
                                    className="spinner-border spinner-border-sm me-2"
                                    role="status"
                                    aria-hidden="true"
                                  ></span>
                                  <span className="sr-only">Loading...</span>
                                  Loading...
                                </>
                              ) : (
                                <>
                                  <i className="fa fa-paper-plane me-1"></i>
                                  Simpan
                                </>
                              )}
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
