import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../SideWidget-Tambah-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const SideWidgetTambah = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default SideWidgetTambah;
