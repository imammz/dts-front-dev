import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import Select from "react-select";
import DataTable from "react-data-table-component";
import {
  indonesianDateFormat,
  capitalizeFirstLetter,
  getYoutubeThumbnail,
  getDateTime,
} from "../helper";

export default class VideoContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickDelete = this.handleClickDeleteAction.bind(this);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
    this.handleClickFilterPublish =
      this.handleClickFilterPublishAction.bind(this);
    this.handleClickFilterUnpublish =
      this.handleClickFilterUnpublishAction.bind(this);
    this.handleClickFilter = this.handleClickFilterAction.bind(this);
    this.handleClickReset = this.handleClickResetAction.bind(this);
    this.handleChangeKategoriFilter =
      this.handleChangeKategoriFilterAction.bind(this);
    this.handleChangeStatusFilter =
      this.handleChangeStatusFilterAction.bind(this);
    this.handleClickPratinjau = this.handleClickPratinjauAction.bind(this);
    this.handleChangeSearch = this.handleChangeSearchAction.bind(this);
    this.closeModal = this.closeModalAction.bind(this);
    this.handleSort = this.handleSortAction.bind(this);
  }
  state = {
    datax: [],
    loading: true,
    totalRows: 0,
    newPerPage: 10,
    totalViews: 0,
    totalAuthor: 0,
    totalPublish: 0,
    totalUnpublish: 0,
    tempLastNumber: 0,
    currentPage: 0,
    isSearch: false,
    statusPublish: "",
    column: "id",
    dataxkategori: [],
    valKategoriFilter: [],
    valStatusFilter: [],
    pratinjau: [],
    isOpen: false,
    isFilter: false,
    filterValue: {},
    kategori: "",
    status: "",
    searchText: "",
    sort_by: "tanggal_publish",
    sort_val: "DESC",
    from_pagination_change: 0,
    paginationResetDefaultPage: false,
  };
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };
  status = [
    { value: "", label: "Semua" },
    { value: 1, label: "Publish" },
    { value: 0, label: "Unpublish" },
  ];
  columns = [
    {
      name: "No",
      center: true,
      cell: (row, index) => this.state.tempLastNumber + index + 1,
      width: "70px",
    },
    {
      name: "Judul",
      sortable: true,
      className: "min-w-300px mw-300px",
      width: "320px",
      grow: 6,
      selector: (row) => capitalizeFirstLetter(row.judul_video),
      cell: (row) => {
        if (row.gambar == null) {
          return (
            <div>
              <label className="d-flex flex-stack cursor-pointer my-3">
                <a
                  href="#"
                  data-bs-toggle="modal"
                  data-bs-target="#pratinjau"
                  onClick={this.handleClickPratinjau}
                  kategori={row.kategori}
                  judul={row.judul_video}
                  url_video={row.url_video}
                  isi={row.deskripsi_video}
                  tag={row.tag}
                  id={row.id}
                  total_views={row.total_views}
                  tanggal_publish={row.tanggal_publish}
                  title="Pratinjau"
                  className="text-dark"
                >
                  <span className="d-flex align-items-center">
                    <span className="symbol symbol-75px">
                      <span
                        className="symbol-label"
                        style={{ backgroundColor: "transparent" }}
                      >
                        <span className="svg-icon">
                          <img
                            className="w-75 rounded my-0"
                            src={getYoutubeThumbnail(row.url_video)}
                          />
                        </span>
                      </span>
                    </span>
                    <span className="d-flex flex-column">
                      <span
                        className="fw-bolder fs-7"
                        style={{
                          overflow: "hidden",
                          whiteSpace: "wrap",
                          textOverflow: "ellipses",
                        }}
                      >
                        {capitalizeFirstLetter(row.judul_video)}
                      </span>
                    </span>
                  </span>
                </a>
              </label>
            </div>
          );
        } else {
          return (
            <div>
              <label className="d-flex flex-stack cursor-pointer my-3">
                <a
                  href="#"
                  data-bs-toggle="modal"
                  data-bs-target="#pratinjau"
                  onClick={this.handleClickPratinjau}
                  kategori={row.kategori}
                  judul={row.judul_video}
                  url_video={row.url_video}
                  isi={row.deskripsi_video}
                  tag={row.tag}
                  id={row.id}
                  total_views={row.total_views}
                  tanggal_publish={row.tanggal_publish}
                  title="Pratinjau"
                  className="text-dark"
                >
                  <span className="d-flex align-items-center">
                    <span className="symbol symbol-75px mb-0">
                      <span
                        className="symbol-label"
                        style={{ backgroundColor: "transparent" }}
                      >
                        <span className="svg-icon">
                          <img
                            className="w-75 rounded my-0"
                            src={
                              process.env.REACT_APP_BASE_API_URI +
                              "/publikasi/get-file?path=" +
                              row.gambar
                            }
                          />
                        </span>
                      </span>
                    </span>
                    <span className="d-flex flex-column">
                      <span
                        className="fw-bolder fs-7"
                        style={{
                          overflow: "hidden",
                          whiteSpace: "wrap",
                          textOverflow: "ellipses",
                        }}
                      >
                        {capitalizeFirstLetter(row.judul_video)}
                      </span>
                    </span>
                  </span>
                </a>
              </label>
            </div>
          );
        }
      },
    },
    {
      name: "Kategori",
      sortable: true,
      className: "min-w-100px",
      grow: 2,
      selector: (row) => capitalizeFirstLetter(row.kategori),
      cell: (row) => {
        return (
          <span className="d-flex flex-column">
            <span
              className="fs-7"
              style={{
                overflow: "hidden",
                whiteSpace: "wrap",
                textOverflow: "ellipses",
              }}
            >
              {capitalizeFirstLetter(row.kategori)}
            </span>
          </span>
        );
      },
    },
    {
      name: "Tgl.Dibuat",
      sortable: true,
      className: "min-w-100px",
      grow: 2,
      selector: (row) => indonesianDateFormat(row.tanggal_publish),
      cell: (row) => {
        return (
          <span className="d-flex flex-column">
            <span
              className="fs-7"
              style={{
                overflow: "hidden",
                whiteSpace: "wrap",
                textOverflow: "ellipses",
              }}
            >
              {indonesianDateFormat(row.tanggal_publish)}
            </span>
          </span>
        );
      },
    },
    {
      name: "Author",
      sortable: true,
      className: "min-w-100px",
      grow: 2,
      selector: (row) => capitalizeFirstLetter(row.user_name),
      cell: (row) => {
        return (
          <span className="d-flex flex-column">
            <span
              className="fs-7"
              style={{
                overflow: "hidden",
                whiteSpace: "wrap",
                textOverflow: "ellipses",
              }}
            >
              {capitalizeFirstLetter(row.user_name)}
            </span>
          </span>
        );
      },
    },
    {
      name: "Status",
      center: true,
      sortable: true,
      className: "min-w-150px",
      grow: 2,
      cell: (row) => (
        <div>
          <span
            className={
              "badge badge-light-" +
              (row.publish == 1 ? "success" : "danger") +
              " fs-7 m-1"
            }
          >
            {row.publish == 1 ? "Publish" : "Unpublish"}
          </span>
        </div>
      ),
    },
    {
      name: "Aksi",
      center: true,
      width: "150px",
      cell: (row) => (
        <div>
          <a
            href="#"
            data-bs-toggle="modal"
            data-bs-target="#pratinjau"
            onClick={this.handleClickPratinjau}
            kategori={row.kategori}
            judul={row.judul_video}
            url_video={row.url_video}
            isi={row.deskripsi_video}
            tag={row.tag}
            id={row.id}
            total_views={row.total_views}
            tanggal_publish={row.tanggal_publish}
            className="btn btn-icon btn-bg-primary btn-sm me-1"
            title="Lihat"
            alt="Lihat"
          >
            <i className="bi bi-eye-fill text-white"></i>
          </a>
          <a
            href={"/publikasi/edit-video/" + row.id + "/" + row.tag}
            id={row.id}
            className="btn btn-icon btn-bg-warning btn-sm me-1"
            title="Edit"
            alt="Edit"
          >
            <i className="bi bi-gear-fill text-white"></i>
          </a>
          <a
            href="#"
            id={row.id}
            onClick={this.handleClickDelete}
            title="Hapus"
            className="btn btn-icon btn-bg-danger btn-sm me-1"
          >
            <i className="bi bi-trash-fill text-white"></i>
          </a>
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };
  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    const bodyKategori = { start: 0, length: 100, jenis_kategori: "Video" };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/list-kategori",
        bodyKategori,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxkategori = [];
        dataxkategori.push({ value: "", label: "Semua" });
        optionx.map((data) =>
          dataxkategori.push({ value: data.id, label: data.nama }),
        );
        this.setState({ dataxkategori });
      });
    this.handleReload();
  }

  handleClickFilterAction(e) {
    const dataForm = new FormData(e.currentTarget);
    e.preventDefault();
    this.setState({ loading: true });
    const kategori = dataForm.get("kategori");
    const status = dataForm.get("status");

    this.setState(
      {
        kategori,
        status,
      },
      () => {
        this.handleReload(1, this.state.newPerPage);
      },
    );
  }

  handleClickResetAction() {
    this.setState(
      {
        valKategoriFilter: [],
        valStatusFilter: [],
        isFilter: false,
        status: "",
        kategori: "",
        paginationResetDefaultPage: !this.state.paginationResetDefaultPage,
      },
      () => {
        this.handleReload(1, this.state.newPerPage);
      },
    );
  }

  handleChangeKategoriFilterAction = (selectedOption) => {
    this.setState({
      valKategoriFilter: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  };

  handleChangeStatusFilterAction = (selectedOption) => {
    this.setState({
      valStatusFilter: {
        label: selectedOption.label,
        value: selectedOption.value,
      },
    });
  };

  handleClickFilterPublishAction(e) {
    this.setState({ status: 1 }, function () {
      this.handleReload(1, this.state.newPerPage);
    });
  }

  handleClickFilterUnpublishAction(e) {
    this.setState({ status: 0 }, function () {
      this.handleReload(1, this.state.newPerPage);
    });
  }

  handleClickPratinjauAction(e) {
    const kategori = e.currentTarget.getAttribute("kategori");
    const judul = e.currentTarget.getAttribute("judul");
    let url_video = e.currentTarget.getAttribute("url_video");
    const isi = e.currentTarget.getAttribute("isi");
    const tag = e.currentTarget.getAttribute("tag");
    const tanggal_publish = e.currentTarget.getAttribute("tanggal_publish");
    const total_views = e.currentTarget.getAttribute("total_views");
    const id = e.currentTarget.getAttribute("id");

    url_video = url_video.replace("watch?v=", "embed/");
    url_video = url_video.replace(
      "https://youtu.be/",
      "https://www.youtube.com/embed/",
    );

    this.setState(
      {
        pratinjau: {
          kategori: kategori,
          judul: judul,
          url_video: url_video,
          isi: isi,
          tag: tag,
          tanggal_publish: tanggal_publish,
          total_views: total_views,
          id: id,
        },
      },
      () => {},
    );
  }

  handleClickDeleteAction(e) {
    const idx = e.currentTarget.id;
    swal
      .fire({
        title: "Apakah anda yakin ?",
        text: "Data ini tidak bisa dikembalikan!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, hapus!",
        cancelButtonText: "Tidak",
      })
      .then((result) => {
        if (result.isConfirmed) {
          const data = { id: idx };
          axios
            .post(
              process.env.REACT_APP_BASE_API_URI +
                "/publikasi/softdelete-video",
              data,
              this.configs,
            )
            .then((res) => {
              const statux = res.data.result.Status;
              const messagex = res.data.result.Message;
              if (statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "success",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload(
                        this.state.currentPage,
                        this.state.newPerPage,
                      );
                    }
                  });
              } else {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmationBUttonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload();
                    }
                  });
              }
            })
            .catch((error) => {
              let statux = error.response.data.result.Status;
              let messagex = error.response.data.result.Message;
              if (!statux) {
                swal
                  .fire({
                    title: messagex,
                    icon: "warning",
                    confirmButtonText: "Ok",
                  })
                  .then((result) => {
                    if (result.isConfirmed) {
                      this.handleReload();
                    }
                  });
              }
            });
        }
      });
  }

  handleReload(page, newPerPage) {
    this.setState({ loading: true });
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    if (page != 1 && page != undefined) {
      start_tmp = newPerPage * page;
      start_tmp = start_tmp - newPerPage;
    }
    this.setState({ tempLastNumber: start_tmp });

    const dataBody = {
      start: start_tmp,
      length: length_tmp,
      status: this.state.status,
      kategori: this.state.kategori,
      search: this.state.searchText,
      sort_by: this.state.sort_by,
      sort_val: this.state.sort_val.toUpperCase(),
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/filter-video",
        dataBody,
        this.configs,
      )
      .then((res) => {
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;
        if (statux) {
          const datax = res.data.result.Data;
          this.setState({ datax });
          this.setState({ loading: false });
          this.setState({ totalRows: res.data.result.TotalLength });
          this.setState({ totalAuthor: res.data.result.TotalAuthor });
          this.setState({ totalPublish: res.data.result.TotalPublish });
          this.setState({ totalUnpublish: res.data.result.TotalUnpublish });
          this.setState({ totalViews: res.data.result.TotalViews });
          this.setState({ currentPage: page });
          if (this.state.paginationResetDefaultPage == true) {
            this.setState({ paginationResetDefaultPage: false });
          }
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      })
      .catch((error) => {
        console.log(error);
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      });
  }

  handlePageChange = (page) => {
    if (this.state.from_pagination_change == 0) {
      this.setState({ loading: true });
      this.handleReload(page, this.state.newPerPage);
    }
  };
  handlePerRowsChange = async (newPerPage, page) => {
    if (this.state.from_pagination_change == 1) {
      this.setState({ loading: true });
      this.setState({ newPerPage: newPerPage }, () => {
        this.handleReload(page, this.state.newPerPage);
      });
    }
  };

  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    if (e.key == "Enter") {
      if (searchText == "") {
        this.setState(
          {
            isSearch: false,
            searchText: "",
          },
          () => {
            this.handleReload();
          },
        );
      } else {
        this.setState({ loading: true });
        this.setState({ isSearch: true });
        this.setState({ searchText: searchText }, () => {
          this.handleReload();
        });
      }
    }
  }

  handleChangeSearchAction(e) {
    const searchText = e.currentTarget.value;
    if (searchText == "") {
      this.setState(
        {
          loading: true,
          searchText: "",
        },
        () => {
          this.handleReload();
        },
      );
    }
  }

  closeModalAction(e) {
    const className = e.currentTarget.className;
    if (!className.includes("show")) {
      this.setState({ pratinjau: [] });
    }
  }

  handleSortAction(column, sortDirection) {
    let server_name = "";
    if (column.name == "Judul") {
      server_name = "judul_video";
    } else if (column.name == "Kategori") {
      server_name = "kategori";
    } else if (column.name == "Tgl.Dibuat") {
      server_name = "tanggal_publish";
    } else if (column.name == "Author") {
      server_name = "user_name";
    } else if (column.name == "Status") {
      server_name = "publish";
    }

    this.setState(
      {
        sort_by: server_name,
        sort_val: sortDirection,
      },
      () => {
        this.handleReload(1, this.state.newPerPage);
      },
    );
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="32"
                      height="32"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        opacity="0.3"
                        d="M12.9 10.7L3 5V19L12.9 13.3C13.9 12.7 13.9 11.3 12.9 10.7Z"
                        fill="currentColor"
                      ></path>
                      <path
                        d="M21 10.7L11.1 5V19L21 13.3C22 12.7 22 11.3 21 10.7Z"
                        fill="#f1416c"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Publikasi
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Video
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  className="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder me-2"
                  data-bs-toggle="modal"
                  data-bs-target="#filter"
                >
                  <i className="bi bi-sliders"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Filter
                  </span>
                </button>

                <a
                  href="/publikasi/tambah-video"
                  className="btn btn-success fw-bolder btn-sm"
                >
                  <i className="bi bi-plus-circle"></i>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Tambah Video
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-7"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 col-md-12 col-sm-12">
                    <div className="row">
                      <div className="col-6 col-lg-3 mb-3">
                        <a
                          href="#"
                          onClick={this.handleClickFilterPublish}
                          className="card hoverable"
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-success mb-3 mt-1">
                                <span className="svg-icon svg-icon-success svg-icon-3x">
                                  <i
                                    className="bi bi-camera-video-fill"
                                    style={{
                                      fontSize: "30px",
                                      color: "#50cd89",
                                    }}
                                  ></i>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.totalPublish}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Video Publish
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-6 col-lg-3 mb-3">
                        <a
                          href="#"
                          onClick={this.handleClickFilterUnpublish}
                          className="card hoverable"
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-danger mb-3 mt-1">
                                <span className="svg-icon svg-icon-danger svg-icon-3x">
                                  <span className="svg-icon svg-icon-success svg-icon-3x">
                                    <i
                                      className="bi bi-camera-video-off-fill"
                                      style={{
                                        fontSize: "30px",
                                        color: "#F1416C",
                                      }}
                                    ></i>
                                  </span>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.totalUnpublish}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Video Unpublish
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-6 col-lg-3 mb-3">
                        <a href="#" className="card hoverable">
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-info mb-3 mt-1">
                                <span className="svg-icon svg-icon-info svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <path
                                      d="M20 14H18V10H20C20.6 10 21 10.4 21 11V13C21 13.6 20.6 14 20 14ZM21 19V17C21 16.4 20.6 16 20 16H18V20H20C20.6 20 21 19.6 21 19ZM21 7V5C21 4.4 20.6 4 20 4H18V8H20C20.6 8 21 7.6 21 7Z"
                                      fill="#7239EA"
                                    ></path>
                                    <path
                                      opacity="0.3"
                                      d="M17 22H3C2.4 22 2 21.6 2 21V3C2 2.4 2.4 2 3 2H17C17.6 2 18 2.4 18 3V21C18 21.6 17.6 22 17 22ZM10 7C8.9 7 8 7.9 8 9C8 10.1 8.9 11 10 11C11.1 11 12 10.1 12 9C12 7.9 11.1 7 10 7ZM13.3 16C14 16 14.5 15.3 14.3 14.7C13.7 13.2 12 12 10.1 12C8.10001 12 6.49999 13.1 5.89999 14.7C5.59999 15.3 6.19999 16 7.39999 16H13.3Z"
                                      fill="#7239EA"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.totalAuthor}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Total Author
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-6 col-lg-3 mb-3">
                        <a href="#" className="card hoverable">
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-primary mb-3 mt-1">
                                <span className="svg-icon svg-icon-primary svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <rect
                                      x="8"
                                      y="9"
                                      width="3"
                                      height="10"
                                      rx="1.5"
                                      fill="currentColor"
                                    ></rect>
                                    <rect
                                      opacity="0.5"
                                      x="13"
                                      y="5"
                                      width="3"
                                      height="14"
                                      rx="1.5"
                                      fill="currentColor"
                                    ></rect>
                                    <rect
                                      x="18"
                                      y="11"
                                      width="3"
                                      height="8"
                                      rx="1.5"
                                      fill="currentColor"
                                    ></rect>
                                    <rect
                                      x="3"
                                      y="13"
                                      width="3"
                                      height="6"
                                      rx="1.5"
                                      fill="currentColor"
                                    ></rect>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.totalViews}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Total Views
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-12 mt-5">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Daftar Video
                        </h1>
                      </div>
                      <div className="card-toolbar">
                        <div className="ml-5 d-flex align-items-center position-relative my-1 me-2">
                          <span className="svg-icon svg-icon-1 position-absolute ms-6">
                            <svg
                              width="24"
                              height="24"
                              viewBox="0 0 24 24"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                              className="mh-50px"
                            >
                              <rect
                                opacity="0.5"
                                x="17.0365"
                                y="15.1223"
                                width="8.15546"
                                height="2"
                                rx="1"
                                transform="rotate(45 17.0365 15.1223)"
                                fill="#a1a5b7"
                              ></rect>
                              <path
                                d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                fill="#a1a5b7"
                              ></path>
                            </svg>
                          </span>
                          <input
                            type="text"
                            data-kt-user-table-filter="search"
                            className="form-control form-control-sm form-control-solid w-250px ps-14"
                            placeholder="Cari Video"
                            onKeyPress={this.handleKeyPress}
                            onChange={this.handleChangeSearch}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="card-body">
                      <div className="table-responsive">
                        <DataTable
                          columns={this.columns}
                          data={this.state.datax}
                          progressPending={this.state.loading}
                          highlightOnHover
                          pointerOnHover
                          pagination
                          paginationServer
                          paginationResetDefaultPage={
                            this.state.paginationResetDefaultPage
                          }
                          paginationTotalRows={this.state.totalRows}
                          paginationComponentOptions={{
                            selectAllRowsItem: true,
                            selectAllRowsItemText: "Semua",
                          }}
                          onChangeRowsPerPage={(
                            currentRowsPerPage,
                            currentPage,
                          ) => {
                            this.setState(
                              {
                                from_pagination_change: 1,
                              },
                              () => {
                                this.handlePerRowsChange(
                                  currentRowsPerPage,
                                  currentPage,
                                );
                              },
                            );
                          }}
                          onChangePage={(page, totalRows) => {
                            this.setState(
                              {
                                from_pagination_change: 0,
                              },
                              () => {
                                this.handlePageChange(page, totalRows);
                              },
                            );
                          }}
                          customStyles={this.customStyles}
                          persistTableHead={true}
                          noDataComponent={
                            <div className="mt-5">Tidak Ada Data</div>
                          }
                          onSort={this.handleSort}
                          sortServer
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          data-backdrop="static"
          data-keyboard="false"
          onBlur={this.closeModal}
          className="modal fade"
          tabIndex="-1"
          id="pratinjau"
        >
          <div className="modal-dialog modal-lg">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Pratinjau</h5>
                <div
                  className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <span className="svg-icon svg-icon-2x">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <rect
                        opacity="0.5"
                        x="6"
                        y="17.3137"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(-45 6 17.3137)"
                        fill="currentColor"
                      />
                      <rect
                        x="7.41422"
                        y="6"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(45 7.41422 6)"
                        fill="currentColor"
                      />
                    </svg>
                  </span>
                </div>
              </div>
              <div className="modal-body">
                <div style={{ width: "100%", height: "50vh" }}>
                  <iframe
                    src={this.state.pratinjau.url_video}
                    frameBorder="0"
                    allow="autoplay; encrypted-media"
                    allowFullScreen
                    width="100%"
                    height="100%"
                    title="video"
                  />
                </div>
                <span className="mt-5 badge badge-light-primary">
                  {this.state.pratinjau.kategori}
                </span>
                <br />
                <br />
                <h2 className=" mb-3">{this.state.pratinjau.judul}</h2>
                <div
                  dangerouslySetInnerHTML={{ __html: this.state.pratinjau.isi }}
                />
                <br />
                <span className="text-muted">
                  {indonesianDateFormat(this.state.pratinjau.tanggal_publish)} |{" "}
                  {this.state.pratinjau.total_views} ditonton
                </span>
              </div>
              <div className="modal-footer">
                <a
                  href={
                    "/publikasi/edit-video/" +
                    this.state.pratinjau.id +
                    "/" +
                    this.state.pratinjau.tag
                  }
                  className="btn btn-warning btn-sm me-3"
                >
                  Edit
                </a>
                <button
                  className="btn btn-light btn-sm"
                  data-bs-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="modal fade" tabIndex="-1" id="filter">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">
                  <span className="svg-icon svg-icon-5 me-1">
                    <i className="bi bi-sliders text-black"></i>
                  </span>
                  Filter Data Video
                </h5>
                <div
                  className="btn btn-icon btn-sm btn-active-light-primary ms-2"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                >
                  <span className="svg-icon svg-icon-2x">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <rect
                        opacity="0.5"
                        x="6"
                        y="17.3137"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(-45 6 17.3137)"
                        fill="currentColor"
                      />
                      <rect
                        x="7.41422"
                        y="6"
                        width="16"
                        height="2"
                        rx="1"
                        transform="rotate(45 7.41422 6)"
                        fill="currentColor"
                      />
                    </svg>
                  </span>
                </div>
              </div>
              <form
                action="#"
                onSubmit={this.handleClickFilter}
                className="form"
              >
                <input
                  type="hidden"
                  name="csrf-token"
                  value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                />
                <div className="modal-body">
                  <div className="fv-row form-group mb-7">
                    <label className="form-label">Kategori</label>
                    <Select
                      id="kategori"
                      name="kategori"
                      value={this.state.valKategoriFilter}
                      placeholder="Silahkan pilih Kategori"
                      noOptionsMessage={({ inputValue }) =>
                        !inputValue
                          ? this.state.dataxkategori
                          : "Data tidak tersedia"
                      }
                      className="form-select-sm selectpicker p-0"
                      options={this.state.dataxkategori}
                      onChange={this.handleChangeKategoriFilter}
                    />
                  </div>
                  <div className="fv-row form-group mb-7">
                    <label className="form-label">Status</label>
                    <Select
                      id="status"
                      name="status"
                      value={this.state.valStatusFilter}
                      placeholder="Silahkan pilih Status"
                      noOptionsMessage={({ inputValue }) =>
                        !inputValue
                          ? this.state.dataxkategori
                          : "Data tidak tersedia"
                      }
                      className="form-select-sm selectpicker p-0"
                      options={this.status}
                      onChange={this.handleChangeStatusFilter}
                    />
                  </div>
                </div>
                <div className="modal-footer">
                  <div className="d-flex justify-content-between">
                    <button
                      type="reset"
                      className="btn btn-sm btn-reset me-3"
                      onClick={this.handleClickReset}
                    >
                      Reset
                    </button>
                    <button
                      type="submit"
                      className="btn btn-sm btn-primary"
                      data-bs-dismiss="modal"
                    >
                      Apply Filter
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
