import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Select from "react-select";
import { TagsInput } from "react-tag-input-component";
import "../artikel/style_tags.css";
import Editor from "@arbor-dev/ckeditor5-arbor-custom";

export default class VideoTambahContent extends React.Component {
  constructor(props) {
    super(props);
    this.formDatax = new FormData();
    this.onTagsChanged = this.onTagsChanged.bind(this);
    this.handleClick = this.handleClickAction.bind(this);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleChangeThumbnail = this.handleChangeThumbnailAction.bind(this);
    this.handleChangeDeskripsi = this.handleChangeDeskripsiAction.bind(this);
    this.handleTanggalPublish = this.handleTanggalPublishAction.bind(this);
    this.handleChangeJudul = this.handleChangeJudulAction.bind(this);
    this.handleChangeUrl = this.handleChangeUrlAction.bind(this);
    this.handleChangeKategori = this.handleChangeKategoriAction.bind(this);
    this.handleChangeStatus = this.handleChangeStatusAction.bind(this);
    this.handleChangeDate = this.handleChangeDateAction.bind(this);

    this.state = {
      datax: [],
      loading: true,
      isLoading: false,
      dataxkategori: [],
      tags: [],
      fields: {},
      errors: {},
      deskripsi_video: "",
      thumbnail: "",
      tanggal_publish: "",
    };
    this.formDatax = new FormData();
  }

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  optionstatus = [
    { value: "1", label: "Publish" },
    { value: "0", label: "Unpublish" },
  ];

  handleClickBatalAction(e) {
    swal
      .fire({
        title: "Apakah Anda Yakin?",
        icon: "warning",
        confirmButtonText: "Ya",
        showCancelButton: true,
        cancelButtonText: "Tidak",
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
      })
      .then((result) => {
        if (result.isConfirmed) {
          //window.location = '/publikasi/video';
          window.history.back();
        }
      });
  }

  handleChangeThumbnailAction(e) {
    const logofile = e.target.files[0];
    this.formDatax.append("thumbnail", logofile);
  }

  handleChangeDeskripsiAction(value) {
    const errors = this.state.errors;
    errors["deskripsi"] = "";
    this.setState({
      errors,
    });

    this.state.deskripsi_video = value;
  }

  handleTanggalPublishAction(value) {
    this.state.tanggal_publish = value.target.value;
  }

  handleClickAction(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });
      const dataFormSubmit = new FormData();
      dataFormSubmit.append("users_id", Cookies.get("user_id"));
      dataFormSubmit.append("judul_video", dataForm.get("judul"));
      dataFormSubmit.append("deskripsi_video", this.state.deskripsi_video);
      dataFormSubmit.append("url_video", dataForm.get("link_url"));
      dataFormSubmit.append("kategori_id", dataForm.get("id_kategori"));
      dataFormSubmit.append("tag", this.state.tags.join(","));
      if (this.formDatax.get("thumbnail")) {
        dataFormSubmit.append("gambar", this.formDatax.get("thumbnail"));
      }
      dataFormSubmit.append("tanggal_publish", dataForm.get("date_publish"));
      dataFormSubmit.append("publish", dataForm.get("status"));
      dataFormSubmit.append("release", 1);

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/publikasi/tambah-video",
          dataFormSubmit,
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                allowOutsideClick: false,
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/publikasi/video";
                }
              });
          } else {
            this.setState({ isLoading: false });
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      this.setState({ isLoading: false });
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const check = this.checkEmpty(
      dataForm,
      ["judul", "id_kategori", "status", "link_url", "date_publish"],
      [],
    );
    return check;
  }

  validURL(str) {
    var pattern = new RegExp(
      "^(https?:\\/\\/)?" + // protocol
        "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
        "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
        "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
        "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
        "(\\#[-a-z\\d_]*)?$",
      "i",
    ); // fragment locator
    return !!pattern.test(str);
  }

  resetError() {
    let errors = {};
    errors[
      ("judul",
      "link_url",
      "id_kategori",
      "status",
      "deskripsi",
      "date_publish")
    ] = "";
    this.setState({ errors: errors });
  }

  checkEmpty(dataForm, fieldName, fileFieldName) {
    let errors = {};
    let formIsValid = true;
    for (const field in fieldName) {
      const nameAttribute = fieldName[field];
      if (dataForm.get(nameAttribute) == "") {
        errors[nameAttribute] = "Tidak Boleh Kosong";
        formIsValid = false;
      }
    }
    if (fileFieldName) {
      for (const field in fileFieldName) {
        const nameAttribute = fileFieldName[field];

        if (dataForm.get(nameAttribute).name == "") {
          errors[nameAttribute] = "Tidak Boleh Kosong";
          formIsValid = false;
        }
      }
    }
    if (this.state.deskripsi_video == "") {
      errors["deskripsi"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }
    if (!this.validURL(dataForm.get("link_url"))) {
      errors["link_url"] = "Bukan Valid URL";
      formIsValid = false;
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  onTagsChanged(tags) {
    this.setState({ tags });
  }

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    this.handleReload();
  }

  handleReload(page, newPerPage) {
    const dataKategorik = { start: 0, length: 100, jenis_kategori: "Video" };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/list-kategori",
        dataKategorik,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxkategori = [];
        optionx.map((data) =>
          dataxkategori.push({ value: data.id, label: data.nama }),
        );
        this.setState({ dataxkategori });
      });
  }

  handleChangeJudulAction(e) {
    const errors = this.state.errors;
    errors["judul"] = "";
    this.setState({
      errors,
    });
  }

  handleChangeUrlAction(e) {
    const errors = this.state.errors;
    errors["link_url"] = "";
    this.setState({
      errors,
    });
  }

  handleChangeKategoriAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["id_kategori"] = "";
    this.setState({
      errors,
    });
  };

  handleChangeStatusAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["status"] = "";
    this.setState({
      errors,
    });
  };

  handleChangeDateAction(e) {
    const errors = this.state.errors;
    errors["date_publish"] = "";
    this.setState({
      errors,
    });
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="32"
                      height="32"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        opacity="0.3"
                        d="M12.9 10.7L3 5V19L12.9 13.3C13.9 12.7 13.9 11.3 12.9 10.7Z"
                        fill="currentColor"
                      ></path>
                      <path
                        d="M21 10.7L11.1 5V19L21 13.3C22 12.7 22 11.3 21 10.7Z"
                        fill="#f1416c"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Publikasi
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Video
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Tambah Video
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <form
                        className="form"
                        action="#"
                        onSubmit={this.handleClick}
                      >
                        <input
                          type="hidden"
                          name="csrf-token"
                          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                        />
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">Judul</label>
                          <input
                            className="form-control form-control-sm"
                            placeholder="Masukkan Judul Disini"
                            name="judul"
                            onChange={this.handleChangeJudul}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["judul"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">
                            Deskripsi Video
                          </label>
                          <CKEditor
                            editor={Editor}
                            name="deskripsi"
                            onReady={(editor) => {}}
                            onChange={(event, editor) => {
                              const data = editor.getData();
                              this.handleChangeDeskripsi(data);
                            }}
                            config={{
                              ckfinder: {
                                // Upload the images to the server using the CKFinder QuickUpload command.
                                uploadUrl:
                                  process.env.REACT_APP_BASE_API_URI +
                                  "/publikasi/ckeditor-upload-image",
                              },
                            }}
                            onBlur={(event, editor) => {}}
                            onFocus={(event, editor) => {}}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["deskripsi"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label">Upload Thumbnail</label>
                          <input
                            type="file"
                            className="form-control form-control-sm font-size-h4"
                            name="thumbnail"
                            id="thumbnail"
                            accept=".png,.jpg,.jpeg,.svg"
                            onChange={this.handleChangeThumbnail}
                          />
                          <small className="text-muted">
                            Resolusi yang direkomendasikan adalah 837 * 640.
                            Fokus visual pada bagian tengah gambar.
                          </small>
                          <div>
                            <span style={{ color: "red" }}>
                              {this.state.errors["thumbnail"]}
                            </span>
                          </div>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">
                            Link URL Video
                          </label>
                          <input
                            className="form-control form-control-sm"
                            placeholder="Masukkan URL Video Disini"
                            name="link_url"
                            onChange={this.handleChangeUrl}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["link_url"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">
                            Kategori
                          </label>
                          <Select
                            id="id_kategori"
                            name="id_kategori"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.dataxkategori
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            options={this.state.dataxkategori}
                            onChange={this.handleChangeKategori}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["id_kategori"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label">Tag</label>
                          <TagsInput
                            value={this.state.tags}
                            onChange={this.onTagsChanged}
                            name="tags"
                            placeHolder="Isi Tag Disini dan Enter"
                            className="form-control form-control-sm"
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["tags"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">Status</label>
                          <Select
                            name="status"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            options={this.optionstatus}
                            onChange={this.handleChangeStatus}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["status"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">
                            Tanggal Publish
                          </label>
                          <div className="pl-7">
                            <input
                              id="date_publish"
                              name="date_publish"
                              className="form-control form-control-sm"
                              placeholder="Masukkan Tanggal Publish"
                              onChange={this.handleChangeDate}
                            />
                          </div>
                          <span style={{ color: "red" }}>
                            {this.state.errors["date_publish"]}
                          </span>
                        </div>

                        <div className="form-group fv-row pt-7 mb-7">
                          <div className="d-flex justify-content-center mb-7">
                            <button
                              onClick={this.handleClickBatal}
                              type="reset"
                              className="btn btn-md btn-light me-3"
                              data-kt-menu-dismiss="true"
                            >
                              Batal
                            </button>
                            <button
                              type="submit"
                              className="btn btn-primary btn-md"
                              id="submitQuestion1"
                              disabled={this.state.isLoading}
                            >
                              {this.state.isLoading ? (
                                <>
                                  <span
                                    className="spinner-border spinner-border-sm me-2"
                                    role="status"
                                    aria-hidden="true"
                                  ></span>
                                  <span className="sr-only">Loading...</span>
                                  Loading...
                                </>
                              ) : (
                                <>
                                  <i className="fa fa-paper-plane me-1"></i>
                                  Simpan
                                </>
                              )}
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
