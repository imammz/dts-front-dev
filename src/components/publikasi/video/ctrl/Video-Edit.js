import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../Video-Edit-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const VideoEdit = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default VideoEdit;
