import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import Select from "react-select";
import { TagsInput } from "react-tag-input-component";
import "../artikel/style_tags.css";
import Flatpickr from "react-flatpickr";
import { Indonesian } from "flatpickr/dist/l10n/id.js";
import moment from "moment";
import Editor from "@arbor-dev/ckeditor5-arbor-custom";

export default class ArtikelTambahContent extends React.Component {
  constructor(props) {
    super(props);
    this.formDatax = new FormData();
    this.onTagsChanged = this.onTagsChanged.bind(this);
    this.handleClick = this.handleClickAction.bind(this);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleChangeThumbnail = this.handleChangeThumbnailAction.bind(this);
    this.handleChangeIsi = this.handleChangeIsiAction.bind(this);
    this.handleChangeJudul = this.handleChangeJudulAction.bind(this);
    this.handleChangeKategori = this.handleChangeKategoriAction.bind(this);
    this.handleChangeStatus = this.handleChangeStatusAction.bind(this);
    this.state = {
      datax: [],
      isLoading: false,
      loading: true,
      dataxakademi: [],
      dataxkategori: [],
      tags: [],
      fields: {},
      errors: {},
      isi_artikel: "",
      thumbnail: "",
      judul: "",
      valKategori: "",
      valStatus: "",
      tanggal_publish: "",
    };
    this.formDatax = new FormData();
  }

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  optionstatus = [
    { value: "1", label: "Publish" },
    { value: "0", label: "Unpublish" },
  ];

  handleClickBatalAction(e) {
    swal
      .fire({
        title: "Apakah Anda Yakin?",
        icon: "warning",
        confirmButtonText: "Ya",
        showCancelButton: true,
        cancelButtonText: "Tidak",
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
      })
      .then((result) => {
        if (result.isConfirmed) {
          //window.location = '/publikasi/artikel';
          window.history.back();
        }
      });
  }

  handleChangeThumbnailAction(e) {
    const logofile = e.target.files[0];
    this.formDatax.append("thumbnail", logofile);
  }

  handleChangeIsiAction(value) {
    const errors = this.state.errors;
    errors["isi"] = "";

    this.setState({
      errors,
    });

    this.state.isi_artikel = value;
  }

  handleChangeJudulAction(e) {
    const errors = this.state.errors;
    errors["judul"] = "";

    const judul = e.currentTarget.value;
    this.setState({
      judul,
      errors,
    });
  }

  handleChangeKategoriAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["id_kategori"] = "";
    this.setState({
      errors,
    });

    this.setState(
      {
        //level_footer: selectedOption.value,
        valKategori: {
          value: selectedOption.value,
          label: selectedOption.label,
        },
      },
      () => {},
    );
  };

  handleChangeStatusAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["status"] = "";
    this.setState({
      errors,
    });

    this.setState(
      {
        //level_footer: selectedOption.value,
        valStatus: { value: selectedOption.value, label: selectedOption.label },
      },
      () => {},
    );
  };

  handleClickAction(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });
      let slug = dataForm.get("judul").replace(" ", "-");

      const dataFormSubmit = new FormData();
      dataFormSubmit.append("users_id", Cookies.get("user_id"));
      dataFormSubmit.append("judul_artikel", dataForm.get("judul"));
      dataFormSubmit.append("slug", slug);
      dataFormSubmit.append("isi_artikel", this.state.isi_artikel);
      dataFormSubmit.append("kategori_akademi", dataForm.get("id_akademi"));
      dataFormSubmit.append("kategori_id", dataForm.get("id_kategori"));
      dataFormSubmit.append("tag", this.state.tags.join(","));
      if (this.formDatax.get("thumbnail")) {
        dataFormSubmit.append("gambar", this.formDatax.get("thumbnail"));
      }
      dataFormSubmit.append(
        "tanggal_publish",
        this.handleDate(this.state.tanggal_publish),
      );
      dataFormSubmit.append("publish", dataForm.get("status"));
      dataFormSubmit.append("release", 1);

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/publikasi/tambah-artikel",
          dataFormSubmit,
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                confirmButtonText: "Ok",
                allowOutsideClick: false,
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/publikasi/artikel";
                }
              });
          } else {
            this.setState({ isLoading: false });
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      this.setState({ isLoading: false });
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleDate(dateString) {
    if (moment(dateString, "DD MMMM YYYY").isValid()) {
      return moment(dateString, "DD MMMM YYYY").format("YYYY-MM-DD");
    }
    return dateString;
  }
  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const check = this.checkEmpty(
      dataForm,
      ["judul", "id_kategori", "status"],
      [""],
    );
    return check;
  }

  resetError() {
    let errors = {};
    errors[("judul", "id_kategori", "status", "isi", "tanggal_publish")] = "";
    this.setState({ errors: errors });
  }

  checkEmpty(dataForm, fieldName, fileFieldName) {
    let errors = {};
    let formIsValid = true;
    for (const field in fieldName) {
      const nameAttribute = fieldName[field];
      if (dataForm.get(nameAttribute) == "") {
        errors[nameAttribute] = "Tidak Boleh Kosong";
        formIsValid = false;
      }
    }
    if (this.state.isi_artikel == "") {
      errors["isi"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }
    if (this.state.tanggal_publish == "") {
      errors["tanggal_publish"] = "Tidak Boleh Kosong";
      formIsValid = false;
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  onTagsChanged(tags) {
    this.setState({ tags });
  }

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    this.handleReload();
  }

  handleReload(page, newPerPage) {
    let start_tmp = 0;
    let length_tmp = newPerPage != undefined ? newPerPage : 10;
    const dataAkademik = { start: 0, length: 100, status: "publish" };
    const dataKategorik = { start: 0, length: 100, jenis_kategori: "Artikel" };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/list-akademi",
        dataAkademik,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxakademi = [];
        dataxakademi.push({ value: null, label: "--Tidak Memilih Akademi--" });
        optionx.map((data) =>
          dataxakademi.push({
            value: data.slug,
            label: data.name + " (" + data.slug + ")",
          }),
        );
        this.setState({ dataxakademi });
      });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/list-kategori",
        dataKategorik,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxkategori = [];
        optionx.map((data) =>
          dataxkategori.push({ value: data.id, label: data.nama }),
        );
        this.setState({ dataxkategori });
      });
  }

  render() {
    let rowCounter = 1;
    const styleImg = {
      width: "40px",
      height: "30px",
    };
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="32"
                      height="32"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        opacity="0.3"
                        d="M12.9 10.7L3 5V19L12.9 13.3C13.9 12.7 13.9 11.3 12.9 10.7Z"
                        fill="currentColor"
                      ></path>
                      <path
                        d="M21 10.7L11.1 5V19L21 13.3C22 12.7 22 11.3 21 10.7Z"
                        fill="#f1416c"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Publikasi
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Artikel
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Tambah Artikel
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <form
                        className="form"
                        action="#"
                        onSubmit={this.handleClick}
                      >
                        <input
                          type="hidden"
                          name="csrf-token"
                          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                        />
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">Judul</label>
                          <input
                            className="form-control form-control-sm"
                            placeholder="Masukkan Judul Disini"
                            name="judul"
                            value={this.state.judul}
                            onChange={this.handleChangeJudul}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["judul"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">
                            Isi Artikel
                          </label>
                          <CKEditor
                            editor={Editor}
                            name="isi"
                            onReady={(editor) => {
                              // You can store the "editor" and use when it is needed.
                            }}
                            config={{
                              ckfinder: {
                                // Upload the images to the server using the CKFinder QuickUpload command.
                                uploadUrl:
                                  process.env.REACT_APP_BASE_API_URI +
                                  "/publikasi/ckeditor-upload-image",
                              },
                              mediaEmbed: {
                                previewsInData: true,
                              },
                            }}
                            onChange={(event, editor) => {
                              const data = editor.getData();
                              this.handleChangeIsi(data);
                            }}
                            onBlur={(event, editor) => {}}
                            onFocus={(event, editor) => {}}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["isi"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label">Upload Thumbnail</label>
                          <input
                            type="file"
                            className="form-control form-control-sm font-size-h4"
                            name="thumbnail"
                            id="thumbnail"
                            accept=".png,.jpg,.jpeg,.svg"
                            onChange={this.handleChangeThumbnail}
                          />
                          <small className="text-muted">
                            Resolusi yang direkomendasikan adalah 837 * 640.
                            Fokus visual pada bagian tengah gambar.
                          </small>
                          <div>
                            <span style={{ color: "red" }}>
                              {this.state.errors["thumbnail"]}
                            </span>
                          </div>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label">Akademi</label>
                          <Select
                            id="id_akademi"
                            name="id_akademi"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.dataxakademi
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            //onChange={this.handleChangeAkademi}
                            options={this.state.dataxakademi}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["id_akademi"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">
                            Kategori
                          </label>
                          <Select
                            id="id_kategori"
                            name="id_kategori"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.dataxkategori
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            onChange={this.handleChangeKategori}
                            value={this.state.valKategori}
                            options={this.state.dataxkategori}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["id_kategori"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label">Tag</label>
                          <TagsInput
                            value={this.state.tags}
                            onChange={this.onTagsChanged}
                            name="tags"
                            placeHolder="Isi Tag Disini dan Enter"
                            className="form-control form-control-sm"
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["tags"]}
                          </span>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">
                            Tanggal Artikel
                          </label>
                          <Flatpickr
                            options={{
                              locale: Indonesian,
                              dateFormat: "d F Y",
                              enableTime: "false",
                            }}
                            className="form-control form-control-sm"
                            placeholder="Masukkan tanggal artikel"
                            name="tanggal_publish"
                            onChange={(value) => {
                              this.setState(
                                { tanggal_publish: value[0] },
                                () => {
                                  if (value[0]) {
                                    let tanggal_artikel_error =
                                      this.state.errors;
                                    tanggal_artikel_error["tanggal_publish"] =
                                      "";
                                    this.setState({
                                      errors: tanggal_artikel_error,
                                    });
                                  }
                                },
                              );
                            }}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["tanggal_publish"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">Status</label>
                          <Select
                            name="status"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            options={this.optionstatus}
                            value={this.state.valStatus}
                            onChange={this.handleChangeStatus}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["status"]}
                          </span>
                        </div>

                        <div className="form-group fv-row pt-7 mb-7">
                          <div className="d-flex justify-content-center mb-7">
                            <button
                              onClick={this.handleClickBatal}
                              type="reset"
                              className="btn btn-md btn-light me-3"
                              data-kt-menu-dismiss="true"
                            >
                              Batal
                            </button>
                            <button
                              disabled={this.state.isLoading}
                              type="submit"
                              className="btn btn-primary btn-md"
                              id="submitQuestion1"
                            >
                              {this.state.isLoading ? (
                                <>
                                  <span
                                    className="spinner-border spinner-border-sm me-2"
                                    role="status"
                                    aria-hidden="true"
                                  ></span>
                                  <span className="sr-only">Loading...</span>
                                  Loading...
                                </>
                              ) : (
                                <>
                                  <i className="fa fa-paper-plane me-1"></i>
                                  Simpan
                                </>
                              )}
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
