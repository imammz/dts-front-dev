import React, { useEffect } from "react";
import Header from "../../../../components/Header";
import Breadcumb from "../../../../components/Breadcumb";
import Content from "../../artikel/Artikel-Content";
import SideNav from "../../../../components/SideNav";
import Footer from "../../../../components/Footer";
import { cekPermition, logout } from "../../../AksesHelper";
import Swal from "sweetalert2";

const Artikel = () => {
  useEffect(() => {
    if (cekPermition().view !== 1) {
      Swal.fire({
        title: "Akses Tidak Diizinkan",
        html: "<i> Anda akan kembali kehalaman login </i>",
        icon: "warning",
      }).then(() => {
        logout();
      });
    }
  }, []);

  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default Artikel;
