import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import Select from "react-select";

export default class ImagetronTambahContent extends React.Component {
  constructor(props) {
    super(props);
    this.formDatax = new FormData();
    this.handleClick = this.handleClickAction.bind(this);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleChangeThumbnail = this.handleChangeThumbnailAction.bind(this);
    this.handleChangeKategori = this.handleChangeKategoriAction.bind(this);
    this.handleChangeJudul = this.handleChangeJudulAction.bind(this);
    this.handleChangeStatus = this.handleChangeStatusAction.bind(this);
    this.handleChangePinned = this.handleChangePinnedAction.bind(this);

    this.state = {
      datax: [],
      isLoading: false,
      loading: true,
      dataxkategori: [],
      fields: {},
      errors: {},
      sizeSlideshow: {
        width: 874,
        height: 445,
      },
      sizeSquare: {
        width: 844,
        height: 650,
      },
      currentKategori: "",
      message_resolusi: "",
    };
    this.pinnedstatus = [];
    this.formDatax = new FormData();
  }

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  optionstatus = [
    { value: "1", label: "Publish" },
    { value: "0", label: "Unpublish" },
  ];

  squarepinnedstatus = [
    { value: "0", label: "Pinned" },
    { value: "1", label: "Pinned + Sidebar" },
    { value: "2", label: "Sidebar" },
  ];

  slidepinnedstatus = [
    { value: "0", label: "No" },
    { value: "1", label: "Yes" },
  ];

  handleClickBatalAction(e) {
    swal
      .fire({
        title: "Apakah Anda Yakin?",
        icon: "warning",
        confirmButtonText: "Ya",
        showCancelButton: true,
        cancelButtonText: "Tidak",
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
      })
      .then((result) => {
        if (result.isConfirmed) {
          //window.location = "/publikasi/imagetron";
          window.history.back();
        }
      });
  }

  readFile(logofile) {
    const image = new Image();
    image.src = logofile.target.result;
    image.onload = function () {};
  }
  handleChangeThumbnailAction = async (e) => {
    const errors = this.state.errors;
    errors["thumbnail"] = "";
    this.setState({
      errors,
    });

    const logofile = e.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(logofile);

    try {
      await new Promise((resolve, reject) => {
        reader.onload = (ol) => {
          const image = new Image();
          image.src = ol.target.result;

          image.onload = () => {
            const { height, width } = image;
            const kategori = this.state.currentKategori;
            const sizeSlideshow = this.state.sizeSlideshow;
            const sizeSquare = this.state.sizeSquare;
            const messagex = this.state.message_resolusi;

            if (kategori === "Slideshow") {
              if (
                sizeSlideshow.height !== height ||
                sizeSlideshow.width !== width
              ) {
                reject(new Error("Wrong image size"));
              } else {
                this.formDatax.append("thumbnail", logofile);
                resolve();
              }
            } else if (kategori === "Square") {
              if (sizeSquare.height !== height || sizeSquare.width !== width) {
                reject(new Error("Wrong image size"));
              } else {
                this.formDatax.append("thumbnail", logofile);
                resolve();
              }
            } else {
              reject(new Error("Invalid category"));
            }
          };
        };
      });
    } catch (error) {
      const messagex = this.state.message_resolusi;
      swal
        .fire({
          title: "Wrong Image Size",
          text: messagex,
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          e.target.value = null;
          this.fileInput.value = "";
        });

      this.formDatax.delete("thumbnail");
    }
  };

  handleClickAction(e) {
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      this.setState({ isLoading: true });
      swal.fire({
        title: "Mohon Tunggu!",
        icon: "info", // add html attribute if you want or remove
        allowOutsideClick: false,
        didOpen: () => {
          swal.showLoading();
        },
      });
      let today = new Date();
      let tanggal_publish =
        today.getFullYear() +
        "-" +
        (today.getMonth() + 1) +
        "-" +
        today.getDate();

      const dataFormSubmit = new FormData();
      dataFormSubmit.append("users_id", Cookies.get("user_id"));
      dataFormSubmit.append("judul_imagetron", dataForm.get("judul"));
      dataFormSubmit.append("kategori_id", dataForm.get("id_kategori"));
      dataFormSubmit.append("link_url", dataForm.get("link"));
      dataFormSubmit.append("gambar", this.formDatax.get("thumbnail"));
      dataFormSubmit.append("tanggal_publish", tanggal_publish);
      dataFormSubmit.append("publish", dataForm.get("status"));
      if (dataForm.get("pinned") == null) {
        dataFormSubmit.append("pinned", "");
      } else {
        dataFormSubmit.append("pinned", dataForm.get("pinned"));
      }

      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/publikasi/tambah-imagetron",
          dataFormSubmit,
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                allowOutsideClick: false,
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/publikasi/imagetron";
                }
              });
          } else {
            this.setState({ isLoading: false });
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      this.setState({ isLoading: false });
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const check = this.checkEmpty(
      dataForm,
      ["judul", "id_kategori", "status", "pinned"],
      ["thumbnail"],
    );
    return check;
  }

  validURL(str) {
    var pattern = new RegExp(
      "^(https?:\\/\\/)?" + // protocol
        "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
        "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
        "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
        "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
        "(\\#[-a-z\\d_]*)?$",
      "i",
    ); // fragment locator
    return !!pattern.test(str);
  }

  resetError() {
    let errors = {};
    errors[("judul", "id_kategori", "status", "pinned", "thumbnail")] = "";
    this.setState({ errors: errors });
  }

  checkEmpty(dataForm, fieldName, fileFieldName) {
    let errors = {};
    let formIsValid = true;
    for (const field in fieldName) {
      const nameAttribute = fieldName[field];
      if (dataForm.get(nameAttribute) == "") {
        errors[nameAttribute] = "Tidak Boleh Kosong";
        formIsValid = false;
      }
    }
    if (fileFieldName) {
      for (const field in fileFieldName) {
        const nameAttribute = fileFieldName[field];
        if (dataForm.get(nameAttribute).name == "") {
          errors[nameAttribute] = "Tidak Boleh Kosong";
          formIsValid = false;
        }
      }
    }
    if (!this.validURL(dataForm.get("link")) && dataForm.get("link") != "") {
      errors["link"] = "Bukan Valid URL";
      formIsValid = false;
    }
    this.setState({ errors: errors });
    return formIsValid;
  }

  handleChangeKategoriAction = (selectedOption) => {
    const errors = this.state.errors;
    errors["id_kategori"] = "";
    this.setState({
      errors,
    });

    if (selectedOption.label === "Slideshow") {
      this.setState({ pinnedstatus: this.slidepinnedstatus });
      this.setState({ currentKategori: "Slideshow" });
      this.setState({
        message_resolusi: "Ukuran Image Size Harus 874 x 445",
      });
      this.fileInput.value = "";
    } else if (selectedOption.label === "Square") {
      this.setState({ pinnedstatus: this.squarepinnedstatus });
      this.setState({ currentKategori: "Square" });
      this.setState({
        message_resolusi: "Ukuran Image Size Harus 844 x 650",
      });
      this.fileInput.value = "";
    }
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    this.setState({ pinnedstatus: this.slidepinnedstatus });
    this.handleReload();
  }

  handleReload(page, newPerPage) {
    const dataKategorik = {
      start: 0,
      length: 100,
      jenis_kategori: "Imagetron",
    };
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/list-kategori",
        dataKategorik,
        this.configs,
      )
      .then((res) => {
        const optionx = res.data.result.Data;
        const dataxkategori = [];
        optionx.map((data) =>
          dataxkategori.push({ value: data.id, label: data.nama }),
        );
        this.setState({ dataxkategori });
      });
  }

  handleChangeStatusAction = (selectedData) => {
    const errors = this.state.errors;
    errors["status"] = "";
    this.setState({
      errors,
    });
  };

  handleChangeJudulAction(e) {
    const errors = this.state.errors;
    errors["judul"] = "";
    this.setState({
      errors,
    });
  }

  handleChangePinnedAction(e) {
    const errors = this.state.errors;
    errors["pinned"] = "";
    this.setState({
      errors,
    });
  }

  render() {
    let rowCounter = 1;
    const styleImg = {
      width: "40px",
      height: "30px",
    };
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="32"
                      height="32"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        opacity="0.3"
                        d="M12.9 10.7L3 5V19L12.9 13.3C13.9 12.7 13.9 11.3 12.9 10.7Z"
                        fill="currentColor"
                      ></path>
                      <path
                        d="M21 10.7L11.1 5V19L21 13.3C22 12.7 22 11.3 21 10.7Z"
                        fill="#f1416c"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Publikasi
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Imagetron
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Tambah Imagetron
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <form
                        className="form"
                        action="#"
                        onSubmit={this.handleClick}
                      >
                        <input
                          type="hidden"
                          name="csrf-token"
                          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                        />

                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">
                            Kategori
                          </label>
                          <Select
                            id="id_kategori"
                            name="id_kategori"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.dataxkategori
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            onChange={this.handleChangeKategori}
                            options={this.state.dataxkategori}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["id_kategori"]}
                          </span>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">Judul</label>
                          <input
                            className="form-control form-control-sm"
                            placeholder="Masukkan Judul Disini"
                            name="judul"
                            onChange={this.handleChangeJudul}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["judul"]}
                          </span>
                        </div>

                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label">Link URL</label>
                          <input
                            className="form-control form-control-sm"
                            placeholder="http://www.example.com"
                            name="link"
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["link"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">
                            Upload Gambar
                          </label>
                          <input
                            ref={(ref) => (this.fileInput = ref)}
                            type="file"
                            className="form-control form-control-sm font-size-h4"
                            name="thumbnail"
                            id="thumbnail"
                            accept=".png,.jpg,.jpeg,.svg"
                            onChange={this.handleChangeThumbnail}
                          />
                          <small className="text-muted">
                            {this.state.message_resolusi}
                          </small>
                          <div>
                            <span style={{ color: "red" }}>
                              {this.state.errors["thumbnail"]}
                            </span>
                          </div>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">Status</label>
                          <Select
                            name="status"
                            placeholder="Silahkan pilih"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            options={this.optionstatus}
                            onChange={this.handleChangeStatus}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["status"]}
                          </span>
                        </div>
                        {this.state.currentKategori == "Square" && (
                          <div className="form-group fv-row mb-7">
                            <label className="form-label required">
                              Pinned
                            </label>
                            <Select
                              name="pinned"
                              placeholder="Silahkan pilih"
                              noOptionsMessage={({ inputValue }) =>
                                !inputValue
                                  ? this.state.datax
                                  : "Data tidak tersedia"
                              }
                              className="form-select-sm selectpicker p-0"
                              options={this.state.pinnedstatus}
                              onChange={this.handleChangePinned}
                            />
                            <span style={{ color: "red" }}>
                              {this.state.errors["pinned"]}
                            </span>
                          </div>
                        )}
                        <div className="form-group fv-row pt-7 mb-7">
                          <div className="d-flex justify-content-center mb-7">
                            <button
                              onClick={this.handleClickBatal}
                              type="reset"
                              className="btn btn-md btn-light me-3"
                              data-kt-menu-dismiss="true"
                            >
                              Batal
                            </button>
                            <button
                              type="submit"
                              className="btn btn-primary btn-md"
                              id="submitQuestion1"
                              disabled={this.state.isLoading}
                            >
                              {this.state.isLoading ? (
                                <>
                                  <span
                                    className="spinner-border spinner-border-sm me-2"
                                    role="status"
                                    aria-hidden="true"
                                  ></span>
                                  <span className="sr-only">Loading...</span>
                                  Loading...
                                </>
                              ) : (
                                <>
                                  <i className="fa fa-paper-plane me-1"></i>
                                  Simpan
                                </>
                              )}
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
