import React from "react";
import Header from "../../../../components/Header";
import Breadcumb from "../../../../components/Breadcumb";
import Content from "../../imagetron/Imagetron-Tambah-Content";
import SideNav from "../../../../components/SideNav";
import Footer from "../../../../components/Footer";

const ImagetronTambah = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default ImagetronTambah;
