import React from "react";
import Header from "../../../../components/Header";
import Breadcumb from "../../../../components/Breadcumb";
import Content from "../../faq/Faq-Tambah-Content";
import SideNav from "../../../../components/SideNav";
import Footer from "../../../../components/Footer";

const FaqTambah = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default FaqTambah;
