import React from "react";
import Header from "../../../Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../Faq-Edit-Content";
import SideNav from "../../../SideNav";
import Footer from "../../../Footer";

const FaqEdit = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default FaqEdit;
