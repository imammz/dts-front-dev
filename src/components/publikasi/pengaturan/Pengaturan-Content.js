import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import { getDateTime } from "../helper";

export default class PengaturanContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleUploadImage = this.handleUploadImageAction.bind(this);
    this.handleUploadImagetron = this.handleUploadImagetronAction.bind(this);
    this.handleBatasSlider = this.handleBatasSliderAction.bind(this);
    this.handleMaximalFaq = this.handleMaximalFaqAction.bind(this);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleChangeThumbnail = this.handleChangeThumbnailAction.bind(this);
    this.handleClickSimpan = this.handleClickSimpanAction.bind(this);
    this.formDatax = new FormData();
  }
  state = {
    datax: [],
    loading: true,
    isLoading: false,
    upload_imagetron: "",
    slider_limit: "",
    upload_image: "",
    faq_pin_limit: "",
    default_thumbnail: "",
    err_upload_image: "",
    err_upload_imagetron: "",
    err_slider_limit: "",
    err_faq_pin_limit: "",
    err_default_thumbnail: "",
    is_save_image: false,
  };
  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  handleChangeThumbnailAction(e) {
    const logofile = e.target.files[0];
    this.formDatax.append("default_thumbnail", logofile);
    if (e.target.value == "") {
      this.state.err_default_thumbnail = "Tidak Boleh Kosong";
    } else {
      this.state.err_default_thumbnail = "";
    }
  }

  handleUploadImageAction(e) {
    this.setState({ upload_image: e.target.value });
    if (e.target.value == "") {
      this.state.err_upload_image = "Tidak Boleh Kosong";
    } else if (e.target.value >= 5) {
      this.state.err_upload_image = "Tidak Boleh Lebih Dari 5 MB";
    } else {
      this.state.err_upload_image = "";
    }
  }

  handleUploadImagetronAction(e) {
    this.setState({ upload_imagetron: e.target.value });
    if (e.target.value == "") {
      this.state.err_upload_imagetron = "Tidak Boleh Kosong";
    } else if (e.target.value >= 5) {
      this.state.err_upload_imagetron = "Tidak Boleh Lebih Dari 5 MB";
    } else {
      this.state.err_upload_imagetron = "";
    }
  }

  handleBatasSliderAction(e) {
    this.setState({ slider_limit: e.target.value });
    if (e.target.value == "") {
      this.state.err_slider_limit = "Tidak Boleh Kosong";
    } else if (e.target.value >= 7) {
      this.state.err_slider_limit = "Tidak Boleh Lebih Dari 7";
    } else {
      this.state.err_slider_limit = "";
    }
  }

  handleMaximalFaqAction(e) {
    this.setState({ faq_pin_limit: e.target.value });
    if (e.target.value == "") {
      this.state.err_faq_pin_limit = "Tidak Boleh Kosong";
    } else if (e.target.value >= 5) {
      this.state.err_faq_pin_limit = "Tidak Boleh Lebih Dari 5";
    } else {
      this.state.err_faq_pin_limit = "";
    }
  }

  handleClickSimpanAction(e) {
    e.preventDefault();
    const dataFormSubmit = new FormData(e.currentTarget);
    this.setState({ isLoading: true });
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    if (this.formDatax.get("default_thumbnail")) {
      dataFormSubmit.append(
        "default_thumbnail",
        this.formDatax.get("default_thumbnail"),
      );
    }

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/update-setting-all",
        dataFormSubmit,
        this.configs,
      )
      .then((res) => {
        this.setState({ isLoading: false });
        const statux = res.data.result.Status;
        const messagex = res.data.result.Message;

        if (statux) {
          swal
            .fire({
              title: messagex,
              icon: "success",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                if (this.state.is_save_image) {
                  this.setState({ is_save_image: false });
                  this.handleReload();
                }
              }
            });
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
              }
            });
        }
      })
      .catch((error) => {
        this.setState({ isLoading: false });
        swal.hideLoading();
        swal
          .fire({
            title: error,
            icon: "warning",
            confirmButtonText: "Ok",
          })
          .then((result) => {
            if (result.isConfirmed) {
            }
          });
      });
  }

  handleClickBatalAction(e) {
    swal
      .fire({
        title: "Apakah Anda Yakin?",
        icon: "warning",
        confirmButtonText: "Ya",
        showCancelButton: true,
        cancelButtonText: "Tidak",
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
      })
      .then((result) => {
        if (result.isConfirmed) {
          this.handleReload();
        }
      });
  }

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }

    this.handleReload();
  }

  handleReload() {
    swal.fire({
      title: "Mohon Tunggu!",
      icon: "info", // add html attribute if you want or remove
      allowOutsideClick: false,
      didOpen: () => {
        swal.showLoading();
      },
    });
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/list-setting",
        null,
        this.configs,
      )
      .then((res) => {
        swal.close();
        const datax = res.data.result.Data;
        for (const i in datax) {
          for (const j in this.state) {
            if (j === datax[i].name) {
              this.setState({ [datax[i].name]: datax[i].value });
            }
          }
        }
      });
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="32"
                      height="32"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        opacity="0.3"
                        d="M12.9 10.7L3 5V19L12.9 13.3C13.9 12.7 13.9 11.3 12.9 10.7Z"
                        fill="currentColor"
                      ></path>
                      <path
                        d="M21 10.7L11.1 5V19L21 13.3C22 12.7 22 11.3 21 10.7Z"
                        fill="#f1416c"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Publikasi
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Pengaturan
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2"></div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Pengaturan
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <form
                        className="form"
                        action="#"
                        onSubmit={this.handleClickSimpan}
                      >
                        <input
                          type="hidden"
                          name="csrf-token"
                          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                        />
                        <div className="form-group row">
                          <label className="col-lg-2 col-form-label text-lg-right">
                            Upload Image
                          </label>
                          <div className="col-lg-1 col-xl-3">
                            <input
                              className="form-control"
                              placeholder="MB"
                              name="upload_image"
                              type="number"
                              min="0"
                              onChange={this.handleUploadImage}
                              value={this.state.upload_image}
                            />
                            <div>
                              <span style={{ color: "red" }}>
                                {this.state.err_upload_image}
                              </span>
                            </div>
                          </div>
                          <label className="col-lg-1 col-form-label text-lg-right">
                            MB
                          </label>
                        </div>

                        <div className="form-group row mt-3">
                          <label className="col-lg-2 col-form-label text-lg-right">
                            Upload Imagetron
                          </label>
                          <div className="col-lg-1 col-xl-3">
                            <input
                              className="form-control"
                              placeholder="MB"
                              name="upload_imagetron"
                              type="number"
                              min="0"
                              onChange={this.handleUploadImagetron}
                              value={this.state.upload_imagetron}
                            />
                            <div>
                              <span style={{ color: "red" }}>
                                {this.state.err_upload_imagetron}
                              </span>
                            </div>
                          </div>
                          <label className="col-lg-1 col-form-label text-lg-right">
                            MB
                          </label>
                        </div>

                        <div className="form-group row mt-3">
                          <label className="col-lg-2 col-form-label text-lg-right">
                            Batas Slider
                          </label>
                          <div className="col-lg-1 col-xl-3">
                            <input
                              className="form-control"
                              placeholder="Page"
                              name="slider_limit"
                              type="number"
                              min="0"
                              onChange={this.handleBatasSlider}
                              value={this.state.slider_limit}
                            />
                            <div>
                              <span style={{ color: "red" }}>
                                {this.state.err_slider_limit}
                              </span>
                            </div>
                          </div>
                          <label className="col-lg-1 col-form-label text-lg-right">
                            page
                          </label>
                        </div>

                        <div className="form-group row mt-3">
                          <label className="col-lg-2 col-form-label text-lg-right">
                            Maximal FAQ
                          </label>
                          <div className="col-lg-1 col-xl-3">
                            <input
                              className="form-control"
                              placeholder="Page"
                              name="faq_pin_limit"
                              type="number"
                              min="0"
                              onChange={this.handleMaximalFaq}
                              value={this.state.faq_pin_limit}
                            />
                            <div>
                              <span style={{ color: "red" }}>
                                {this.state.err_faq_pin_limit}
                              </span>
                            </div>
                          </div>
                          <label className="col-lg-1 col-form-label text-lg-right">
                            page
                          </label>
                        </div>

                        <div className="form-group row mt-3">
                          <label className="col-lg-1 col-form-label text-lg-right">
                            Default Thumbnail
                          </label>
                          <div className="col-lg-1">
                            <img
                              style={{ width: "60px" }}
                              src={
                                process.env.REACT_APP_BASE_API_URI +
                                "/publikasi/get-file?path=" +
                                this.state.default_thumbnail
                              }
                            />
                          </div>
                          <div className="col-lg-1 col-xl-3">
                            <input
                              type="file"
                              className="form-control form-control-sm font-size-h4"
                              name="default_thumbnail"
                              id="thumbnail"
                              accept=".png,.jpg,.jpeg,.svg"
                              onChange={this.handleChangeThumbnail}
                            />
                            <div>
                              <span style={{ color: "red" }}>
                                {this.state.err_default_thumbnail}
                              </span>
                            </div>
                          </div>
                          <label className="col-lg-1 col-form-label text-lg-right">
                            page
                          </label>
                        </div>

                        <div className="form-group fv-row pt-7 my-7">
                          <div className="d-flex justify-content-start mb-7">
                            <button
                              onClick={this.handleClickBatal}
                              type="reset"
                              className="btn btn-light btn-md me-3"
                              data-kt-menu-dismiss="true"
                            >
                              Batal
                            </button>
                            <button
                              type="submit"
                              className="btn btn-primary btn-md"
                              data-kt-menu-dismiss="true"
                              disabled={this.state.isLoading}
                            >
                              {this.state.isLoading ? (
                                <>
                                  <span
                                    className="spinner-border spinner-border-sm me-2"
                                    role="status"
                                    aria-hidden="true"
                                  ></span>
                                  <span className="sr-only">Loading...</span>
                                  Loading...
                                </>
                              ) : (
                                <>
                                  <i className="fa fa-paper-plane me-1"></i>
                                  Simpan
                                </>
                              )}
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
