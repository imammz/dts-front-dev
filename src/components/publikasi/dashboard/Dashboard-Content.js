import React, { PureComponent } from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import TextField from "@material-ui/core/TextField";
import {
  PieChart,
  Pie,
  Sector,
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";

export default class ArtikelContent extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    datax: [],
    berita: [],
    artikel: [],
    galeri: [],
    video: [],
    event: [],
    barData: [],
    pieData: [],
    loading: true,
    totalRows: 0,
    newPerPage: 10,
    totalArtikel: 0,
    totalPublish: 0,
    totalUnpublish: 0,
    totalAuthor: 0,
    tempLastNumber: 0,
    currentPage: 0,
    maxBar: 0,
    username: Cookies.get("user_name"),
  };

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  renderCustomBarLabel = ({ payload, x, y, width, height, value }) => {
    let elem = (
      <text
        x={x + width / 2}
        y={y}
        fill="#fff"
        textAnchor="middle"
        dy={-5}
      >{`${value}`}</text>
    );
    if (value == this.state.maxBar) {
      elem = (
        <text
          x={x + width / 2}
          y={y}
          fill="#fff"
          textAnchor="middle"
          dx={-20}
          dy={10}
        >{`${value}`}</text>
      );
    }
    return elem;
  };
  data = {
    berita: [
      {
        judul: "Pengumuman seleksi peserta",
        kategori: "Press Release",
        created_by: "superadmin",
        dibaca: 78,
        img_url: "xxxxxxx",
      },
      {
        judul: "Pengumuman seleksi pesert2",
        kategori: "Press Release",
        created_by: "Admin Publikasi",
        dibaca: 70,
        img_url: "xxxxxxx",
      },
      {
        judul: "Pengumuman seleksi pesert3",
        kategori: "Press Release",
        created_by: "Admin Publikasi",
        dibaca: 72,
        img_url: "xxxxxxx",
      },
    ],
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    this.handleReload();
  }

  handleReload(page, newPerPage) {
    console.log("page " + page + " ;newPerPage " + newPerPage);
    const dataBody = {};
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/publikasi/dashboard",
        dataBody,
        this.configs,
      )
      .then((res) => {
        let countPublish = 0;
        let countUnpublish = 0;
        let countArtikel = 0;
        let maxBarData = -999;
        this.setState({ artikel: res.data.result.Top.Artikel });
        this.setState({ berita: res.data.result.Top.Informasi });
        this.setState({ video: res.data.result.Top.Video });
        this.setState({ event: res.data.result.Top.Event });
        this.setState({ barData: res.data.result.Data });
        this.setState({ pieData: res.data.result.Data2 });
        this.setState({
          totalAuthor:
            res.data.result.Data2[0].author + res.data.result.Data2[1].author,
        });
        res.data.result.Data.forEach((element) => {
          countPublish = countPublish + element.publish;
          countUnpublish += element.unpublish;
          countArtikel += element.author;
          if (maxBarData < element.publish) {
            maxBarData = element.publish;
          }
          if (maxBarData < element.unpublish) {
            maxBarData = element.unpublish;
          }
        });
        this.setState({ totalPublish: countPublish });
        this.setState({ totalUnpublish: countUnpublish });
        this.setState({ countArtikel: countArtikel });
        this.setState({ maxBar: maxBarData });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    let rowCounter = 1;
    const styleImg = {
      width: "40px",
      height: "30px",
    };

    const headerStyle = {
      backgroundPosition: "left bottom",
      backgroundImage: 'url("/assets/media/svg/brand-logos/Frame-White.svg")',
      backgroundRepeat: "no-repeat",
      borderRadius: "6px",
      minHeight: "15vh",
    };

    const PIE_COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#4CBDE2", "#4299E1"];
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-3 my-1">
                  <span className="me-3">
                    <svg
                      width="32"
                      height="32"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        opacity="0.3"
                        d="M12.9 10.7L3 5V19L12.9 13.3C13.9 12.7 13.9 11.3 12.9 10.7Z"
                        fill="currentColor"
                      ></path>
                      <path
                        d="M21 10.7L11.1 5V19L21 13.3C22 12.7 22 11.3 21 10.7Z"
                        fill="#f1416c"
                      ></path>
                    </svg>
                  </span>{" "}
                  Publikasi
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <a
                  href="#"
                  className="btn btn-success fw-bolder btn-sm"
                  data-kt-menu-trigger="click"
                  data-kt-menu-placement="bottom-end"
                >
                  <i className="bi bi-plus-circle"></i>{" "}
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Tambah Data
                  </span>
                  <span className="svg-icon svg-icon-5 m-0">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      className="mh-50px"
                    >
                      <path
                        d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                        fill="currentColor"
                      ></path>
                    </svg>
                  </span>
                </a>
                <div
                  className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4"
                  data-kt-menu="true"
                >
                  <div className="menu-item px-3">
                    <a
                      href="/publikasi/tambah-artikel"
                      className="menu-link px-3"
                    >
                      Artikel
                    </a>
                  </div>
                  <div className="menu-item px-3">
                    <a
                      href="/publikasi/tambah-informasi"
                      className="menu-link px-3"
                    >
                      Informasi
                    </a>
                  </div>
                  <div className="menu-item px-3">
                    <a
                      href="/publikasi/tambah-video"
                      className="menu-link px-3"
                    >
                      Video
                    </a>
                  </div>
                  <div className="menu-item px-3">
                    <a href="/publikasi/tambah-faq" className="menu-link px-3">
                      FAQ
                    </a>
                  </div>
                  <div className="menu-item px-3">
                    <a
                      href="/publikasi/tambah-imagetron"
                      className="menu-link px-3"
                    >
                      Imagetron
                    </a>
                  </div>
                  <div className="menu-item px-3">
                    <a
                      href="/publikasi/tambah-event"
                      className="menu-link px-3"
                    >
                      Event
                    </a>
                  </div>
                  <div className="menu-item px-3">
                    <a
                      href="/publikasi/tambah-side-widget"
                      className="menu-link px-3"
                    >
                      Side Widget
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-7"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 col-md-12 col-sm-12 col-xxl-12">
                    <div className="card bg-white card-custom">
                      <div className="card-body" style={headerStyle}>
                        <div className="d-flex align-items-center mb-10">
                          <div className="d-flex flex-column flex-grow-1 font-weight-bold">
                            <div className="row">
                              <div className="col-md-6">
                                <div className="col-md-12 mt-5">
                                  <h4 className="fw-bolder text-primary">
                                    Halo {this.state.username}
                                  </h4>
                                </div>
                                <div className="col-md-10 col-lg-12">
                                  <p className="fw-bold text-muted fs-6">
                                    Selamat datang di Dashboard Publikasi, ada
                                    informasi apa hari ini ?
                                  </p>
                                </div>
                              </div>
                              <div className="col-md-6">
                                <div
                                  className="ml-auto float-right ilustrator-dashboard"
                                  style={{
                                    position: "absolute",
                                    right: "-40px",
                                    top: "12px",
                                  }}
                                >
                                  <div
                                    style={{
                                      display: "inline-block",
                                      maxWidth: "100%",
                                      overflow: "hidden",
                                      position: "relative",
                                      boxSizing: "border-box",
                                      margin: "0px",
                                    }}
                                  >
                                    <div
                                      style={{
                                        boxSizing: "border-box",
                                        display: "block",
                                        maxWidth: "100%",
                                      }}
                                    >
                                      <img
                                        alt=""
                                        aria-hidden="true"
                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMzAwIiBoZWlnaHQ9IjE0NSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4="
                                        style={{
                                          maxWidth: "100%",
                                          display: "block",
                                        }}
                                      />
                                    </div>
                                    <img
                                      alt="dashboard-pict"
                                      src="/assets/media/svg/brand-logos/ilustrator.svg"
                                      style={{
                                        position: "absolute",
                                        inset: "0px",
                                        display: "block",
                                        minWidth: "100%",
                                        maxWidth: "100%",
                                        minHeight: "100%",
                                        maxHeight: "100%",
                                      }}
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row mt-7">
                  <div className="col-lg-6 col-md-6 col-sm-6 col-xxl-6">
                    <div className="card card-custom">
                      <div
                        className="card-body"
                        style={{
                          backgroundColor: "rgb(33, 84, 128)",
                          borderRadius: "6px",
                        }}
                      >
                        <h3 className="card-title me-3 mr-2 text-white">
                          Total Publish dan Unpublish
                        </h3>
                        <div className="d-flex align-items-center justify-content-center col-sm-12">
                          <BarChart
                            width={500}
                            height={300}
                            data={this.state.barData}
                            barSize={20}
                            margin={{
                              top: 5,
                              right: 40,
                              left: 40,
                              bottom: 5,
                            }}
                          >
                            {/* <CartesianGrid strokeDasharray="3 3" /> */}
                            <XAxis stroke="#fff" dataKey="name" />
                            <Tooltip />
                            <Legend />
                            <Bar
                              dataKey="publish"
                              fill="#4299E1"
                              label={this.renderCustomBarLabel}
                            />
                            <Bar
                              dataKey="unpublish"
                              fill="#4CBDE2"
                              label={this.renderCustomBarLabel}
                            />
                          </BarChart>
                        </div>
                      </div>
                      <div className="card-body mb-10 flex-column">
                        <h3 className="card-title me-3 mr-2 text-muted mb-7">
                          Total Konten
                        </h3>
                        <div className="row">
                          <div className="col-12 col-sm-12 col-md-6 col-lg-6 d-flex flex-row">
                            <div
                              style={{
                                backgroundColor: "rgb(66, 153, 225)",
                                width: "50px",
                                height: "50px",
                                borderRadius: "6px",
                                marginRight: "10px",
                              }}
                            ></div>
                            <div>
                              <h3 className="fw-bolder">
                                {this.state.totalPublish}
                              </h3>
                              <div className="text-muted">Publish</div>
                            </div>
                          </div>
                          <div className="col-12 col-sm-12 col-md-6 col-lg-6 d-flex flex-row">
                            <div
                              style={{
                                backgroundColor: "rgb(76, 189, 226)",
                                width: "50px",
                                height: "50px",
                                borderRadius: "6px",
                                marginRight: "10px",
                              }}
                            ></div>
                            <div>
                              <h3 className="fw-bolder">
                                {this.state.totalUnpublish}
                              </h3>
                              <div className="text-muted">Belum Publish</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6 col-xxl-6">
                    <div className="card card-custom">
                      <div className="card-body">
                        <h3 className="card-title me-3 mr-2 text-dark">
                          Count Artikel
                        </h3>
                        <div className="d-flex align-items-center justify-content-center col-sm-12">
                          <PieChart
                            width={500}
                            height={300}
                            onMouseEnter={this.onPieEnter}
                          >
                            <Pie
                              data={this.state.pieData}
                              innerRadius={60}
                              outerRadius={80}
                              fill="#8884d8"
                              paddingAngle={1}
                              dataKey="author"
                              label
                            >
                              {this.state.barData.map((entry, index) => (
                                <Cell
                                  key={`cell-${index}`}
                                  fill={PIE_COLORS[index % PIE_COLORS.length]}
                                />
                              ))}
                            </Pie>
                            <Tooltip />
                            <Legend />
                          </PieChart>
                        </div>
                      </div>
                      <div className="card-body mb-10 flex-column">
                        <h3 className="card-title me-3 mr-2 text-muted mb-7">
                          Total Artikel
                        </h3>
                        <div className="row">
                          <div className="col-12 col-sm-12 col-md-6 col-lg-6 d-flex flex-row">
                            <div
                              style={{
                                width: "50px",
                                height: "50px",
                                borderRadius: "6px",
                                marginRight: "10px",
                              }}
                            >
                              <img
                                src="/assets/media/svg/social-logos/mail-purple.svg"
                                alt="author-total"
                                width={"100%"}
                                height={"100%"}
                              />
                            </div>
                            <div>
                              <h3 className="fw-bolder">
                                {this.state.totalAuthor}
                              </h3>
                              <div className="text-muted">
                                Artikel Internal dan Peserta
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6 col-xxl-6 pt-7">
                    <div className="card card-custom">
                      <div className="card-header border-0">
                        <div className="card-title">
                          <h4 className="me-3 mr-2">3 Top Informasi</h4>
                        </div>
                      </div>
                      <div className="card-body">
                        {this.state.berita.map((dat) => {
                          return (
                            <div className="row" key={dat.id}>
                              <div className="col-3">
                                <img
                                  src={`${process.env.REACT_APP_BASE_API_URI}/publikasi/get-file?path=${dat.gambar}`}
                                  alt="berita-img"
                                  style={{
                                    width: "94px",
                                    height: "63px",
                                    borderRadius: "5px",
                                  }}
                                />
                              </div>
                              <div className="col-6">
                                <h6 className="fw-bolder">
                                  {dat.judul_informasi.length > 25
                                    ? dat.judul_informasi.slice(0, 25) + "..."
                                    : dat.judul_informasi}
                                </h6>
                                <p className="text-muted mb-0">
                                  <span className="text-primary">
                                    {dat.kategori}
                                  </span>
                                </p>
                                <p className="text-muted">
                                  <span className="text-primary">
                                    {dat.name}
                                  </span>
                                </p>
                              </div>
                              <div
                                className="col-3"
                                style={{ textAlign: "right" }}
                              >
                                <h1
                                  className="fw-bolder mb-n2"
                                  style={{ fontSize: "2.5em" }}
                                >
                                  {dat.total_views}
                                </h1>
                                <p className="text-muted">Dibaca</p>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6 col-xxl-6 pt-7">
                    <div className="card card-custom">
                      <div className="card-header border-0">
                        <div className="card-title">
                          <h4 className="me-3 mr-2">3 Top Artikel</h4>
                        </div>
                      </div>
                      <div className="card-body">
                        {this.state.artikel.map((dat) => {
                          return (
                            <div className="row" key={dat.id}>
                              <div className="col-3">
                                <img
                                  src={`${process.env.REACT_APP_BASE_API_URI}/publikasi/get-file?path=${dat.gambar}`}
                                  alt="artikel-img"
                                  style={{
                                    width: "94px",
                                    height: "63px",
                                    borderRadius: "5px",
                                  }}
                                />
                              </div>
                              <div className="col-6">
                                <h6 className="fw-bolder">
                                  {dat.judul_artikel.length > 25
                                    ? dat.judul_artikel.slice(0, 25) + "..."
                                    : dat.judul_artikel}
                                </h6>
                                <p className="text-muted mb-0">
                                  <span className="text-primary">
                                    {dat.kategori}
                                  </span>
                                </p>
                                <p className="text-muted">
                                  <span className="text-primary">
                                    {dat.name}
                                  </span>
                                </p>
                              </div>
                              <div
                                className="col-3"
                                style={{ textAlign: "right" }}
                              >
                                <h1
                                  className="fw-bolder mb-n2"
                                  style={{ fontSize: "2.5em" }}
                                >
                                  {dat.total_views}
                                </h1>
                                <p className="text-muted">Dibaca</p>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6 col-xxl-6 pt-7">
                    <div className="card card-custom">
                      <div className="card-header border-0">
                        <div className="card-title">
                          <h4 className="me-3 mr-2">3 Top Event</h4>
                        </div>
                      </div>
                      <div className="card-body">
                        {this.state.event.map((dat) => {
                          return (
                            <div className="row" key={dat.id}>
                              {/* <div className="col-3">
                                                            <img src={dat.gambar} alt="berita-img" style={{width: '94px', height: '63px', borderRadius: '5px'}}/>
                                                        </div> */}
                              <div className="col-8">
                                <h6 className="fw-bolder">
                                  {dat.judul_event.length > 50
                                    ? dat.judul_event.slice(0, 50) + "..."
                                    : dat.judul_event}
                                </h6>
                                <p className="text-muted mb-0">
                                  <span className="text-primary">
                                    {dat.kategori}
                                  </span>
                                </p>
                                <p className="text-muted">
                                  <span className="text-primary">
                                    {dat.created_by}
                                  </span>
                                </p>
                              </div>
                              <div
                                className="col-4"
                                style={{ textAlign: "right" }}
                              >
                                <h1
                                  className="fw-bolder mb-n2"
                                  style={{ fontSize: "2.5em" }}
                                >
                                  {dat.total_views}
                                </h1>
                                <p className="text-muted">Dibaca</p>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6 col-xxl-6 pt-7">
                    <div className="card card-custom">
                      <div className="card-header border-0">
                        <div className="card-title">
                          <h4 className="me-3 mr-2">3 Top Video</h4>
                        </div>
                      </div>
                      <div className="card-body">
                        {this.state.video.map((dat) => {
                          return (
                            <div className="row" key={dat.id}>
                              <div className="col-3">
                                <img
                                  src={`${dat.gambar}`}
                                  alt="video-img"
                                  style={{
                                    width: "94px",
                                    height: "63px",
                                    borderRadius: "5px",
                                  }}
                                />
                              </div>
                              <div className="col-6">
                                <h6 className="fw-bolder">
                                  {dat.judul_video.length > 25
                                    ? dat.judul_video.slice(0, 25) + "..."
                                    : dat.judul_video}
                                </h6>
                                <p className="text-muted mb-0">
                                  <span className="text-primary">
                                    {dat.kategori}
                                  </span>
                                </p>
                                <p className="text-muted">
                                  <span className="text-primary">
                                    {dat.name}
                                  </span>
                                </p>
                              </div>
                              <div
                                className="col-3"
                                style={{ textAlign: "right" }}
                              >
                                <h1
                                  className="fw-bolder mb-n2"
                                  style={{ fontSize: "2.5em" }}
                                >
                                  {dat.total_views}
                                </h1>
                                <p className="text-muted">Dibaca</p>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
