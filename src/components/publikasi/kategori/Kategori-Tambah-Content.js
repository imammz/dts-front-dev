import React from "react";
import swal from "sweetalert2";
import axios from "axios";
import Cookies from "js-cookie";
import Select from "react-select";

export default class KategoriTambahContent extends React.Component {
  constructor(props) {
    super(props);
    this.formDatax = new FormData();
    this.handleClick = this.handleClickAction.bind(this);
    this.handleClickBatal = this.handleClickBatalAction.bind(this);
    this.handleChangeNama = this.handleChangeNamaAction.bind(this);
    this.handleChangeStatus = this.handleChangeStatusAction.bind(this);
    this.state = {
      isLoading: false,
      datax: [],
      loading: true,
      fields: {},
      errors: {},
    };
  }

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  optionstatus = [
    { value: "Informasi", label: "Informasi" },
    { value: "Artikel", label: "Artikel" },
    { value: "Video", label: "Video" },
    { value: "Imagetron", label: "Imagetron" },
    { value: "Faq", label: "Faq" },
    { value: "Event", label: "Event" },
    { value: "Panduan", label: "Panduan" },
  ];

  handleClickBatalAction(e) {
    swal
      .fire({
        title: "Apakah Anda Yakin?",
        icon: "warning",
        confirmButtonText: "Ya",
        showCancelButton: true,
        cancelButtonText: "Tidak",
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
      })
      .then((result) => {
        if (result.isConfirmed) {
          //window.location = '/publikasi/kategori';
          window.history.back();
        }
      });
  }

  handleClickAction(e) {
    this.setState({ isLoading: true });
    const dataForm = new FormData(e.currentTarget);
    this.resetError();
    e.preventDefault();
    if (this.handleValidation(e)) {
      axios
        .post(
          process.env.REACT_APP_BASE_API_URI + "/publikasi/tambah-kategori",
          dataForm,
          this.configs,
        )
        .then((res) => {
          this.setState({ isLoading: false });
          const statux = res.data.result.Status;
          const messagex = res.data.result.Message;
          if (statux) {
            swal
              .fire({
                title: messagex,
                icon: "success",
                allowOutsideClick: false,
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                  window.location = "/publikasi/kategori";
                }
              });
          } else {
            this.setState({ isLoading: false });
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          let statux = error.response.data.result.Status;
          let messagex = error.response.data.result.Message;
          if (!statux) {
            swal
              .fire({
                title: messagex,
                icon: "warning",
                confirmButtonText: "Ok",
              })
              .then((result) => {
                if (result.isConfirmed) {
                }
              });
          }
        });
    } else {
      this.setState({ isLoading: false });
      swal
        .fire({
          title: "Mohon Periksa Isian",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
          }
        });
    }
  }

  handleValidation(e) {
    const dataForm = new FormData(e.currentTarget);
    const check = this.checkEmpty(dataForm, ["nama", "jenis_kategori"]);
    return check;
  }

  resetError() {
    let errors = {};
    errors[("nama", "jenis_kategori")] = "";
    this.setState({ errors: errors });
  }

  checkEmpty(dataForm, fieldName) {
    let errors = {};
    let formIsValid = true;
    for (const field in fieldName) {
      const nameAttribute = fieldName[field];
      if (dataForm.get(nameAttribute) == "") {
        errors[nameAttribute] = "Tidak Boleh Kosong";
        formIsValid = false;
      }
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
  }

  handleChangeStatusAction = (selectedData) => {
    const errors = this.state.errors;
    errors["jenis_kategori"] = "";
    this.setState({
      errors,
    });
  };

  handleChangeNamaAction(e) {
    const errors = this.state.errors;
    errors["nama"] = "";
    this.setState({
      errors,
    });
  }

  render() {
    let rowCounter = 1;
    const styleImg = {
      width: "40px",
      height: "30px",
    };
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <div>
                <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                  <span className="me-3">
                    <svg
                      width="32"
                      height="32"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        opacity="0.3"
                        d="M12.9 10.7L3 5V19L12.9 13.3C13.9 12.7 13.9 11.3 12.9 10.7Z"
                        fill="currentColor"
                      ></path>
                      <path
                        d="M21 10.7L11.1 5V19L21 13.3C22 12.7 22 11.3 21 10.7Z"
                        fill="#f1416c"
                      ></path>
                    </svg>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Publikasi
                    <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                  </span>
                  Kategori
                </h1>
              </div>
            </div>

            <div className="d-flex align-items-end my-2">
              <div>
                <button
                  onClick={this.handleClickBatal}
                  type="reset"
                  className="btn btn-sm btn-light btn-active-light-primary"
                  data-kt-menu-dismiss="true"
                >
                  <span className="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                    <i className="fa fa-chevron-left"></i>
                  </span>
                  <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                    Kembali
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-0"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="col-lg-12 mt-7">
                  <div className="card border">
                    <div className="card-header">
                      <div className="card-title">
                        <h1
                          className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                          style={{ textTransform: "capitalize" }}
                        >
                          Tambah Kategori Publikasi
                        </h1>
                      </div>
                    </div>
                    <div className="card-body">
                      <form
                        className="form"
                        action="#"
                        onSubmit={this.handleClick}
                      >
                        <input
                          type="hidden"
                          name="csrf-token"
                          value="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
                        />
                        <div className="col-lg-12 mb-7 fv-row">
                          <label className="form-label required">
                            Nama Kategori
                          </label>
                          <input
                            className="form-control form-control-sm"
                            placeholder="Masukkan Nama Kategori Disini"
                            name="nama"
                            onChange={this.handleChangeNama}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["nama"]}
                          </span>
                        </div>

                        <div className="form-group fv-row mb-7">
                          <label className="form-label required">Section</label>
                          <Select
                            name="jenis_kategori"
                            placeholder="Silahkan pilih Section"
                            noOptionsMessage={({ inputValue }) =>
                              !inputValue
                                ? this.state.datax
                                : "Data tidak tersedia"
                            }
                            className="form-select-sm selectpicker p-0"
                            options={this.optionstatus}
                            onChange={this.handleChangeStatus}
                          />
                          <span style={{ color: "red" }}>
                            {this.state.errors["jenis_kategori"]}
                          </span>
                        </div>

                        <div className="form-group fv-row pt-7 mb-7">
                          <div className="d-flex justify-content-center mb-7">
                            <button
                              onClick={this.handleClickBatal}
                              type="reset"
                              className="btn btn-md btn-light me-3"
                              data-kt-menu-dismiss="true"
                            >
                              Batal
                            </button>
                            <button
                              type="submit"
                              className="btn btn-primary btn-md"
                              id="submitQuestion1"
                              disabled={this.state.isLoading}
                            >
                              {this.state.isLoading ? (
                                <>
                                  <span
                                    className="spinner-border spinner-border-sm me-2"
                                    role="status"
                                    aria-hidden="true"
                                  ></span>
                                  <span className="sr-only">Loading...</span>
                                  Loading...
                                </>
                              ) : (
                                <>
                                  <i className="fa fa-paper-plane me-1"></i>
                                  Simpan
                                </>
                              )}
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
