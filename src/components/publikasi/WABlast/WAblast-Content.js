import React from "react";
import axios from "axios";
import swal from "sweetalert2";
import Cookies from "js-cookie";
import DataTable from "react-data-table-component";
import {
  capitalizeFirstLetter,
  capitalizeTheFirstLetterOfEachWord,
} from "../../publikasi/helper";
import moment from "moment/moment";
import Select from "react-select";

export default class WAblastContent extends React.Component {
  constructor(props) {
    super(props);
    this.handleSort = this.handleSortAction.bind(this);
    this.handleChangeSearch = this.handleChangeSearchAction.bind(this);
    this.handleKeyPress = this.handleKeyPressAction.bind(this);
  }

  state = {
    datax: [],
    loading: false,
    totalRows: "",
    tempLastNumber: 0,
    column: "",
    sortDirection: "created_at DESC",
    sort: "created_at DESC",
    param: "",
    currentPage: 1,
    newPerPage: 10,
    totalRequest: 0,
    totalTerkirim: 0,
    totalDitolak: 0,
    totalSemua: 0,
  };

  arrayDefaultRole = [1, 11, 86, 107, 109, 110, 117, 120];

  configs = {
    headers: {
      Authorization: "Bearer " + Cookies.get("token"),
    },
  };

  columns = [
    {
      name: "No",
      center: "true",
      cell: (row, index) => row.number,
      width: "70px",
    },
    {
      name: "ID",
      width: "100px",
      sortable: true,
      selector: (row) =>
        capitalizeFirstLetter(row.pelatihan.slug_akademi + row.pelatihan.id),
      cell: (row) => {
        return (
          <div>
            <span className="fs-7">
              {row.pelatihan.slug_akademi + row.pelatihan.id}
            </span>
          </div>
        );
      },
    },
    {
      name: "Nama Pelatihan",
      sortable: true,
      selector: (row) => row.pelatihan.nama_pelatihan,
      width: "350px",
      cell: (row) => (
        <div>
          <a
            href={"/publikasi/wa-blast/detail/" + row.id}
            id={row.id}
            title="Detail"
            className="text-dark"
          >
            <label className="d-flex flex-stack my-2 cursor-pointer">
              <span className="d-flex align-items-center me-2">
                <span className="symbol symbol-50px me-6">
                  <span className="symbol-label bg-light-primary">
                    <span className="svg-icon svg-icon-1 svg-icon-primary">
                      {row.mitra_logo == null ? (
                        <img
                          src={`/assets/media/logos/logo-kominfo.png`}
                          alt=""
                          height="100px"
                          className="symbol-label"
                        />
                      ) : (
                        <img
                          src={
                            process.env.REACT_APP_BASE_API_URI +
                            "/download/get-file?path=" +
                            row.mitra_logo +
                            "&disk=dts-storage-partnership"
                          }
                          alt=""
                          className="symbol-label"
                        />
                      )}
                    </span>
                  </span>
                </span>
                <span className="d-flex flex-column my-2">
                  <span className="text-muted fs-7 fw-semibold">
                    {row.pelatihan.metode_pelatihan}
                  </span>
                  <span
                    className="fw-bolder fs-7"
                    style={{
                      overflow: "hidden",
                      whiteSpace: "wrap",
                      textOverflow: "ellipses",
                    }}
                  >
                    {row.pelatihan.nama_pelatihan} (Batch{" "}
                    {row.pelatihan.batch ? row.pelatihan.batch : "1"})
                  </span>
                  <h6 className="text-muted fs-7 fw-semibold mb-1">
                    {row.nama_penyelenggara}
                  </h6>
                </span>
              </span>
            </label>
          </a>
        </div>
      ),
    },
    {
      name: "Total Penerima",
      width: "150px",
      sortable: true,
      selector: (row) => (row.peserta ? JSON.parse(row.peserta).length : 0),
      cell: (row) => (
        <div>
          <span className="fs-7">
            {row.peserta ? JSON.parse(row.peserta).length : 0}
          </span>
        </div>
      ),
    },
    {
      name: "Log",
      sortable: true,
      center: false,
      //width: '180px',
      selector: (row) => row.name,
      cell: (row) => (
        <div>
          <span className="fw-semibold fs-7">{row.name}</span>
          <br />
          <span className="fs-8">{row.created_at}</span>
        </div>
      ),
    },
    {
      name: "Status",
      sortable: true,
      center: true,
      selector: (row) => row.status,
      cell: (row) => (
        <div>
          {row.status == 1 ? (
            <span className={"badge badge-light-primary fs-7 d-block m-1"}>
              Requested
            </span>
          ) : row.status == 2 ? (
            <span className={"badge badge-light-success fs-7 d-block m-1"}>
              Selesai Diproses
            </span>
          ) : row.status == 3 ? (
            <span className={"badge badge-light-danger fs-7 d-block m-1"}>
              Ditolak
            </span>
          ) : (
            <span className={"badge badge-light-danger fs-7 d-block m-1"}>
              Unknown
            </span>
          )}
        </div>
      ),
    },
    {
      name: "Aksi",
      center: true,
      right: true,
      cell: (row) => (
        <div>
          <a
            href={"/publikasi/wa-blast/detail/" + row.id}
            title="Detail"
            className="btn btn-icon btn-bg-primary btn-sm me-1"
          >
            <i className="bi bi-eye-fill text-white"></i>
          </a>
          <a
            href={
              process.env.REACT_APP_BASE_API_URI +
              "/get-file?path=" +
              row.file_result +
              "&disk=dts-storage-publikasi"
            }
            title="Unduh"
            className="btn btn-icon btn-bg-success btn-sm me-1"
          >
            <i className="bi bi-cloud-download text-white"></i>
          </a>
        </div>
      ),
    },
  ];
  customStyles = {
    headCells: {
      style: {
        background: "rgb(243, 246, 249)",
      },
    },
  };

  componentDidMount() {
    if (Cookies.get("token") == null) {
      swal
        .fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        })
        .then((result) => {
          if (result.isConfirmed) {
            window.location = "/";
          }
        });
    }
    this.handleReload();
  }

  handleReload(page) {
    this.setState({ loading: true });

    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/whatsapp-broadcast-request-list",
        {},
        this.configs,
      )
      .then((res) => {
        this.setState({ loading: false });
        let datax = [];
        if (this.state.param) {
          datax = res.data.result.Data.filter((item) => {
            // Filter only column pelatihan.nama_pelatihan and name
            return (
              item.pelatihan.nama_pelatihan
                .toLowerCase()
                .includes(this.state.param.toLowerCase()) ||
              item.name.toLowerCase().includes(this.state.param.toLowerCase())
            );
          });
        } else {
          datax = res.data.result.Data;
        }
        /** Add Number Column */
        datax = datax.map((item, index) => {
          return {
            ...item,
            number: index + 1,
          };
        });
        const statusx = res.data.result.Status;
        const messagex = res.data.result.Message;
        const total = res.data.result.Total;
        const totalPeserta = datax.reduce((acc, item) => {
          if (item.peserta && item.status == 2) {
            return acc + JSON.parse(item.peserta).length;
          } else {
            return acc;
          }
        }, 0);
        if (statusx) {
          this.setState({ datax });
          this.setState({ totalRows: total });
          this.setState({ currentPage: page });
          this.setState({
            totalRequest: datax.filter(
              (item) =>
                item.status === 1 || item.status === 2 || item.status === 3,
            ).length,
            totalTerkirim: datax.filter((item) => item.status === 2).length,
            totalDitolak: datax.filter((item) => item.status === 3).length,
            totalSemua: totalPeserta,
          });
        } else {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      })
      .catch((error) => {
        console.log(error);
        let statux = error.response.data.result.Status;
        let messagex = error.response.data.result.Message;
        if (!statux) {
          swal
            .fire({
              title: messagex,
              icon: "warning",
              confirmButtonText: "Ok",
            })
            .then((result) => {
              if (result.isConfirmed) {
                this.setState({ datax: [] });
                this.setState({ loading: false });
              }
            });
        }
      });
  }
  handlePageChange = (page) => {
    this.setState({ loading: true });
    this.handleReload(page, this.state.newPerPage);
  };
  handlePerRowsChange = async (newPerPage, page) => {
    this.setState({ loading: true });
    this.setState({ newPerPage: newPerPage });
    this.handleReload(page, newPerPage);
  };

  handleSortAction(column, sortDirection) {
    const selectedColumn = column.name
      .replace(/\s/g, "_")
      .replace(/[^a-zA-Z0-9_]/g, "");

    this.setState({ sort: selectedColumn }, () => {
      this.handleReload(1, this.state.newPerPage);
    });
  }

  handleChangeSearchAction(e) {
    const searchText = e.currentTarget.value;
    if (searchText == "") {
      this.setState(
        {
          loading: true,
          param: "",
        },
        () => {
          this.handleReload();
        },
      );
    }
  }

  handleKeyPressAction(e) {
    const searchText = e.currentTarget.value;
    if (e.key == "Enter") {
      if (searchText == "") {
        this.setState(
          {
            isSearch: false,
            param: "",
          },
          () => {
            this.handleReload();
          },
        );
      } else {
        this.setState({ loading: true });
        this.setState({ isSearch: true });
        this.setState({ param: searchText }, () => {
          this.handleReload();
        });
      }
    }
  }

  render() {
    return (
      <div>
        <div className="toolbar" id="kt_toolbar">
          <div
            id="kt_toolbar_container"
            className="container-fluid d-flex flex-stack my-2"
          >
            <div className="d-flex align-items-start my-2">
              <h1 className="d-flex align-items-center text-dark fw-bolder fs-5 my-1">
                <span className="me-3">
                  <svg
                    width="32"
                    height="32"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      opacity="0.3"
                      d="M12.9 10.7L3 5V19L12.9 13.3C13.9 12.7 13.9 11.3 12.9 10.7Z"
                      fill="currentColor"
                    ></path>
                    <path
                      d="M21 10.7L11.1 5V19L21 13.3C22 12.7 22 11.3 21 10.7Z"
                      fill="#f1416c"
                    ></path>
                  </svg>
                </span>
                <span className="d-md-inline d-lg-inline d-xl-inline d-none">
                  Publikasi
                  <i className="bi bi-chevron-right fw-bolder text-dark mx-2"></i>
                </span>
                WhatsApp Blast
              </h1>
            </div>
          </div>
        </div>

        <div
          className="wrapper d-flex flex-column flex-row-fluid pt-7"
          id="kt_wrapper"
        >
          <div
            className="content d-flex flex-column flex-column-fluid pt-0"
            id="kt_content"
          >
            <div className="post d-flex flex-column-fluid" id="kt_post">
              <div id="kt_content_container" className="container-xxl">
                <div className="row">
                  <div className="col-lg-12 col-md-12 col-sm-12">
                    <div className="row">
                      <div className="col-xl-3 mb-3">
                        <a href="#" className="card hoverable">
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-primary mb-3 mt-1">
                                <span className="svg-icon svg-icon-primary svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <rect
                                      x="8"
                                      y="9"
                                      width="3"
                                      height="10"
                                      rx="1.5"
                                      fill="currentColor"
                                    ></rect>
                                    <rect
                                      opacity="0.5"
                                      x="13"
                                      y="5"
                                      width="3"
                                      height="14"
                                      rx="1.5"
                                      fill="currentColor"
                                    ></rect>
                                    <rect
                                      x="18"
                                      y="11"
                                      width="3"
                                      height="8"
                                      rx="1.5"
                                      fill="currentColor"
                                    ></rect>
                                    <rect
                                      x="3"
                                      y="13"
                                      width="3"
                                      height="6"
                                      rx="1.5"
                                      fill="currentColor"
                                    ></rect>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.totalRequest}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Total Pengajuan
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-xl-3 mb-3">
                        <a
                          href="#"
                          onClick={this.handleClickFilterPublish}
                          className="card hoverable"
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-success mb-3 mt-1">
                                <span className="svg-icon svg-icon-success svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <path
                                      opacity="0.3"
                                      d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM12.5 18C12.5 17.4 12.6 17.5 12 17.5H8.5C7.9 17.5 8 17.4 8 18C8 18.6 7.9 18.5 8.5 18.5L12 18C12.6 18 12.5 18.6 12.5 18ZM16.5 13C16.5 12.4 16.6 12.5 16 12.5H8.5C7.9 12.5 8 12.4 8 13C8 13.6 7.9 13.5 8.5 13.5H15.5C16.1 13.5 16.5 13.6 16.5 13ZM12.5 8C12.5 7.4 12.6 7.5 12 7.5H8C7.4 7.5 7.5 7.4 7.5 8C7.5 8.6 7.4 8.5 8 8.5H12C12.6 8.5 12.5 8.6 12.5 8Z"
                                      fill="#47BE7D"
                                    ></path>
                                    <rect
                                      x="7"
                                      y="17"
                                      width="6"
                                      height="2"
                                      rx="1"
                                      fill="#47BE7D"
                                    ></rect>
                                    <rect
                                      x="7"
                                      y="12"
                                      width="10"
                                      height="2"
                                      rx="1"
                                      fill="#47BE7D"
                                    ></rect>
                                    <rect
                                      x="7"
                                      y="7"
                                      width="6"
                                      height="2"
                                      rx="1"
                                      fill="#47BE7D"
                                    ></rect>
                                    <path
                                      d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z"
                                      fill="#47BE7D"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.totalTerkirim}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Selesai
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-xl-3 mb-3">
                        <a
                          href="#"
                          onClick={this.handleClickFilterUnpublish}
                          className="card hoverable"
                        >
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-danger mb-3 mt-1">
                                <span className="svg-icon svg-icon-danger svg-icon-3x">
                                  <svg
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mh-50px"
                                  >
                                    <path
                                      opacity="0.3"
                                      d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM12.5 18C12.5 17.4 12.6 17.5 12 17.5H8.5C7.9 17.5 8 17.4 8 18C8 18.6 7.9 18.5 8.5 18.5L12 18C12.6 18 12.5 18.6 12.5 18ZM16.5 13C16.5 12.4 16.6 12.5 16 12.5H8.5C7.9 12.5 8 12.4 8 13C8 13.6 7.9 13.5 8.5 13.5H15.5C16.1 13.5 16.5 13.6 16.5 13ZM12.5 8C12.5 7.4 12.6 7.5 12 7.5H8C7.4 7.5 7.5 7.4 7.5 8C7.5 8.6 7.4 8.5 8 8.5H12C12.6 8.5 12.5 8.6 12.5 8Z"
                                      fill="#F1416C"
                                    ></path>
                                    <rect
                                      x="7"
                                      y="17"
                                      width="6"
                                      height="2"
                                      rx="1"
                                      fill="#F1416C"
                                    ></rect>
                                    <rect
                                      x="7"
                                      y="12"
                                      width="10"
                                      height="2"
                                      rx="1"
                                      fill="#F1416C"
                                    ></rect>
                                    <rect
                                      x="7"
                                      y="7"
                                      width="6"
                                      height="2"
                                      rx="1"
                                      fill="#F1416C"
                                    ></rect>
                                    <path
                                      d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z"
                                      fill="#F1416C"
                                    ></path>
                                  </svg>
                                </span>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.totalDitolak}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Ditolak
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div className="col-xl-3 mb-3">
                        <a href="#" className="card hoverable">
                          <div className="card-body p-1">
                            <div className="d-flex flex-stack flex-grow-1 p-6">
                              <div className="d-flex flex-center w-50px h-50px rounded-3 bg-light-primary mb-3 mt-1">
                                <svg
                                  width="24"
                                  height="24"
                                  viewBox="0 0 18 18"
                                  fill="none"
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="mh-50px"
                                >
                                  <path
                                    opacity="0.3"
                                    d="M16.5 9C16.5 13.125 13.125 16.5 9 16.5C4.875 16.5 1.5 13.125 1.5 9C1.5 4.875 4.875 1.5 9 1.5C13.125 1.5 16.5 4.875 16.5 9Z"
                                    fill="currentColor"
                                  ></path>
                                  <path
                                    d="M9 16.5C10.95 16.5 12.75 15.75 14.025 14.55C13.425 12.675 11.4 11.25 9 11.25C6.6 11.25 4.57499 12.675 3.97499 14.55C5.24999 15.75 7.05 16.5 9 16.5Z"
                                    fill="currentColor"
                                  ></path>
                                  <rect
                                    x="7"
                                    y="6"
                                    width="4"
                                    height="4"
                                    rx="2"
                                    fill="currentColor"
                                  ></rect>
                                </svg>
                              </div>
                              <div className="d-flex flex-column text-end mt-n4">
                                <h1
                                  className="mb-n1"
                                  style={{ fontSize: "28px" }}
                                >
                                  {this.state.totalSemua}
                                </h1>
                                <div className="fw-bolder text-muted fs-7">
                                  Total Penerima
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-12 mt-7">
                    <div className="card border">
                      <div className="card-header">
                        <div className="card-title">
                          <h1
                            className="d-flex align-items-center text-dark fw-bolder my-1 fs-5"
                            style={{ textTransform: "capitalize" }}
                          >
                            Riwayat Pengajuan
                          </h1>
                        </div>
                        <div className="card-toolbar">
                          <div className="d-flex align-items-center position-relative my-1 me-2">
                            <span className="svg-icon svg-icon-1 position-absolute ms-6">
                              <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className="mh-50px"
                              >
                                <rect
                                  opacity="0.5"
                                  x="17.0365"
                                  y="15.1223"
                                  width="8.15546"
                                  height="2"
                                  rx="1"
                                  transform="rotate(45 17.0365 15.1223)"
                                  fill="currentColor"
                                ></rect>
                                <path
                                  d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                  fill="currentColor"
                                ></path>
                              </svg>
                            </span>
                            <input
                              type="text"
                              data-kt-user-table-filter="search"
                              className="form-control form-control-sm form-control-solid w-250px ps-14"
                              placeholder="Cari"
                              onKeyPress={this.handleKeyPress}
                              onChange={this.handleChangeSearch}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="card-body">
                        <div className="table-responsive">
                          <DataTable
                            columns={this.columns}
                            data={this.state.datax}
                            progressPending={this.state.loading}
                            highlightOnHover
                            pointerOnHover
                            pagination
                            customStyles={this.customStyles}
                            persistTableHead={true}
                            noDataComponent={
                              <div className="mt-5">Tidak Ada Data</div>
                            }
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
