import React, { useEffect } from "react";
import Header from "../../../../components/Header";
import Breadcumb from "../../../Breadcumb";
import Content from "../WAblast-Content";
import SideNav from "../../../../components/SideNav";
import Footer from "../../../../components/Footer";
import { cekPermition, logout } from "../../../AksesHelper";
import Swal from "sweetalert2";

const WAblastContent = () => {
  return (
    <div>
      <SideNav />
      <Header />
      {/* <Breadcumb/> */}
      <Content />
      <Footer />
    </div>
  );
};

export default WAblastContent;
