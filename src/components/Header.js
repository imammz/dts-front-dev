import React, { useEffect, useState } from "react";
import Cookies from "js-cookie";
import { Row } from "reactstrap";

let segment_url = window.location.pathname.split("/");
let urlSegmenttOne = segment_url[2];
let urlSegmentZero = segment_url[1];
let urlSegmentTwo = segment_url[3];

const Header = () => {
  const [user, setUser] = useState({ name: "", role: "", satker_name: "" });
  const [isMitra, setIsMitra] = useState(false);

  useEffect(() => {
    const name = Cookies.get("user_name");
    const role = Cookies.get("user_role");
    const satker_name = Cookies.get("satker_name");
    const id_mitra = Cookies.get("partner_id");
    const role_id = Cookies.remove("role_id");
    setUser({ name: name, role: role, satker_name: satker_name });
    setIsMitra(id_mitra != undefined);
  }, []);

  const logout = () => {
    Cookies.remove("partner_id");
    Cookies.remove("token");
    Cookies.remove("user_id");
    Cookies.remove("user_name");
    Cookies.remove("user_role");
    Cookies.remove("menus");
    Cookies.remove("user_email");
    Cookies.remove("role_id");
    Cookies.remove("role_id_user");
    Cookies.remove("user_role_name");
    Cookies.remove("user_nik");
    if (isMitra) {
      window.location.href = "/signin-mitra";
    } else {
      window.location.href = "/";
    }
  };

  function capitalWord(str) {
    return str.replace(/\w\S*/g, function (kata) {
      const kataBaru = kata.slice(0, 1).toUpperCase() + kata.substr(1);
      console.log(kataBaru);
      return kataBaru;
    });
  }
  return (
    <div>
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0, viewport-fit=cover"
      />
      <meta
        name="description"
        content="Administrator Digital Talent Scholarship Kementerian Komunikasi dan Informatika RI"
      ></meta>
      <meta
        name="keywords"
        content="Digital Talent Scholarship, Digital Talent, Digitalent, Kementerian, Kominfo, Informatika, Sertifikasi, Pelatihan, Bimtek, Bimbingan Teknis, SKKNI, Literasi, SDM, Indonesia, Puslitbang APTIKA IKP, DTS, Scholarship"
      ></meta>
      <meta
        content="19|4aYFm8RGRkKcHI05G4QgI1zjGeRBZEQvK4h5OLZp"
        name="csrf-token"
      />
      <div
        className="wrapper d-flex flex-column flex-row-fluid"
        id="kt_wrapper"
      >
        <div id="kt_header" style={{}} className="header align-items-stretch">
          {/*begin::Container*/}
          <div className="container-fluid d-flex align-items-stretch justify-content-between">
            {/*begin::Aside mobile toggle*/}
            <div
              className="d-flex align-items-center d-lg-none ms-n3 me-1"
              title="Show aside menu"
            >
              <div
                className="btn btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px"
                id="kt_aside_mobile_toggle"
              >
                {/*begin::Svg Icon | path: icons/duotune/abstract/abs015.svg*/}
                <span className="svg-icon svg-icon-2x mt-1">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={24}
                    height={24}
                    viewBox="0 0 24 24"
                    fill="none"
                  >
                    <path
                      d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z"
                      fill="black"
                    />
                    <path
                      opacity="0.3"
                      d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z"
                      fill="black"
                    />
                  </svg>
                </span>
                {/*end::Svg Icon*/}
              </div>
            </div>
            {/*end::Aside mobile toggle*/}
            {/*begin::Mobile logo*/}
            <div className="d-flex align-items-center flex-grow-1 flex-lg-grow-0">
              <a href="/dashboard/digitalent" className="d-lg-none">
                <img src="/assets/media/logos/logo.png" className="h-30px" />
              </a>
            </div>
            {/*end::Mobile logo*/}
            {/*begin::Wrapper*/}

            <div className="d-flex align-items-stretch justify-content-between flex-lg-grow-1">
              <div className="header-menu align-items-stretch ms-n5">
                <div className="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold my-5 my-lg-0 align-items-stretch">
                  <div className="menu-item me-lg-1">
                    <a className="menu-link py-3" href="/dashboard/digitalent">
                      <span className="menu-title">Digitalent</span>
                    </a>
                  </div>

                  <div className="menu-item me-lg-1">
                    <a className="menu-link py-3" href="/dashboard/beasiswa">
                      <span className="menu-title">Beasiswa</span>
                    </a>
                  </div>

                  <div className="menu-item me-lg-1">
                    <a
                      className="menu-link py-3"
                      href="https://imdi.sdmdigital.id"
                      target="_blank"
                    >
                      <span className="menu-title">IMDI</span>
                    </a>
                  </div>

                  <div className="menu-item me-lg-1">
                    <a className="menu-link py-3" href="/dashboard/ga">
                      <span className="menu-title">Analytics</span>
                    </a>
                  </div>

                  <div className="menu-item me-lg-1">
                    <a className="menu-link py-3" href="/panduan">
                      <span className="menu-title">Panduan</span>
                    </a>
                  </div>
                </div>
              </div>

              {/*begin::Topbar*/}
              <div className="d-flex align-items-stretch flex-shrink-0">
                {/*begin::Toolbar wrapper*/}
                <div className="d-flex align-items-stretch flex-shrink-0">
                  {/*begin::User
                    {/*<div className="d-flex align-items-center" data-kt-search-element="toggle" id="kt_header_search_toggle"><div className="btn btn-icon btn-active-light-dark w-30px h-30px w-md-40px h-md-40px"><span className="svg-icon svg-icon-1"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className="mh-50px"><rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="#a1a5b7"></rect><path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="#a1a5b7"></path></svg></span></div></div>
                    <div className="d-flex align-items-center ms-1 ms-lg-3"><div className="btn btn-icon btn-active-light-dark btn-custom position-relative w-30px h-30px w-md-40px h-md-40px" id="kt_drawer_chat_toggle"><span className="svg-icon svg-icon-1"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className="mh-50px"><path opacity="0.3" d="M20 3H4C2.89543 3 2 3.89543 2 5V16C2 17.1046 2.89543 18 4 18H4.5C5.05228 18 5.5 18.4477 5.5 19V21.5052C5.5 22.1441 6.21212 22.5253 6.74376 22.1708L11.4885 19.0077C12.4741 18.3506 13.6321 18 14.8167 18H20C21.1046 18 22 17.1046 22 16V5C22 3.89543 21.1046 3 20 3Z" fill="#a1a5b7"></path><rect x="6" y="12" width="7" height="2" rx="1" fill="#a1a5b7"></rect><rect x="6" y="7" width="12" height="2" rx="1" fill="#a1a5b7"></rect></svg></span><span className="bullet bullet-dot bg-success h-6px w-6px position-absolute translate-middle top-0 start-50 animation-blink"></span></div></div>*/}

                  <div
                    className="d-flex align-items-center ms-1 ms-lg-3"
                    id="kt_header_user_menu_toggle"
                  >
                    {/*begin::Menu wrapper*/}
                    <div
                      className="cursor-pointer symbol symbol-30px symbol-md-40px"
                      data-kt-menu-trigger="click"
                      data-kt-menu-attach="parent"
                      data-kt-menu-placement="bottom-end"
                    >
                      <img
                        src={`https://ui-avatars.com/api/?name=${user.name}`}
                      />
                    </div>
                    {/*begin::Menu*/}
                    <div
                      className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px"
                      data-kt-menu="true"
                    >
                      {/*begin::Menu item*/}
                      <div className="menu-item px-3">
                        <div className="menu-content d-flex align-items-center px-3">
                          {/*begin::Avatar*/}
                          <div className="symbol symbol-50px me-5">
                            <img
                              src={`https://ui-avatars.com/api/?name=${user.name}`}
                            />
                          </div>
                          {/*end::Avatar*/}
                          {/*begin::Username*/}
                          <div className="d-flex flex-column">
                            <div className="fw-bolder d-flex align-items-center fs-7">
                              <span
                                style={{
                                  width: "150px",
                                  whiteSpace: "nowrap",
                                  textOverflow: "ellipsis",
                                  overflow: "hidden",
                                }}
                              >
                                {user.name}
                              </span>
                            </div>
                            <a className="fw-bold text-muted text-hover-primary fs-7">
                              {user.satker_name == "null"
                                ? user.role
                                : user.satker_name}
                            </a>
                          </div>
                          {/*end::Username*/}
                        </div>
                      </div>
                      {/*end::Menu item*/}
                      <div className="separator my-2" />
                      {/*end::Menu separator*/}
                      {/*begin::Menu item*/}
                      {/* <div className="menu-item px-5 my-1">
                        <a href="../../demo1/dist/account/settings.html" className="menu-link px-5">Account Settings</a>
                      </div> */}
                      {/*end::Menu item*/}
                      {/*begin::Menu item*/}
                      <div className="menu-item px-5">
                        <a
                          href="/setting/account"
                          onClick={() => {
                            localStorage.setItem(
                              "back_url",
                              window.location.pathname,
                            );
                          }}
                          className="menu-link px-5"
                        >
                          Settings
                        </a>
                      </div>
                      <div className="menu-item px-5">
                        <a onClick={logout} className="menu-link px-5">
                          Sign Out
                        </a>
                      </div>
                      {/*end::Menu item*/}
                    </div>
                    {/*end::Menu*/}
                    {/*end::Menu wrapper*/}
                  </div>
                  {/*end::User */}
                </div>
                {/*end::Toolbar wrapper*/}
              </div>
              {/*end::Topbar*/}
            </div>
            {/*end::Wrapper*/}
          </div>
          {/*end::Container*/}
        </div>
      </div>
    </div>
  );
};

export default Header;
