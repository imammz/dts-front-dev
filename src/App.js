import React, { useEffect } from "react";
import { Routes, Route, useNavigate } from "react-router-dom";
import DashboardDigitalent from "./components/dashboard/digitalent";
import DashboardSimonas from "./components/dashboard/simonas";
import DashboardBeasiswa from "./components/dashboard/beasiswa";
import DashboardAnalytics from "./components/dashboard/ga";
import DashboardEvaluasi from "./components/dashboard/dashboardevaluasi";
import DashboardSKM from "./components/dashboard/dashboardskm";
import DaftarPesertaBeasiswa from "./components/daftar-peserta/beasiswa";
import DaftarPesertaSimonas from "./components/daftar-peserta/simonas";
import DaftarPesertaDigitalent from "./components/daftar-peserta/digitalent";
import PelatihanAkademi from "./components/pelatihan/Akademi-Content/AkademiList";
import PelatihanAkademiContent from "./components/pelatihan/Akademi-Content/AkademiContent";
import PelatihanAkademiAdd from "./components/pelatihan/Akademi-Content/AddAkademi";
import ForgetPassword from "./pages/forget-password";
import SignIn from "./pages/sign-in";
import SignUp from "./pages/sign-up";
import ResetPassword from "./pages/reset-password";
import VerifikasiOtp from "./pages/verifikasi-otp";
import KonfirmPassword from "./pages/konfirm-password";
import ReloadPage from "./reload-page-502";
// import TestSubstansi from "./components/subvit/test-substansi/ctrl/Testsubstansi";
import TestSubstansi from "./components/subvit/test-substansi/list";
import TestSubstansiTambah from "./components/subvit/test-substansi/add";
import TestSubstansiEdit from "./components/subvit/test-substansi/edit";
import TestSubstansiClone from "./components/subvit/test-substansi/clone";
import TestSubstansiListSoal from "./components/subvit/test-substansi/soal/list";
import TestSubstansiAddSoal from "./components/subvit/test-substansi/soal/add";
import TestSubstansiImportSoal from "./components/subvit/test-substansi/soal/import";
import TestSubstansiEditSoal from "./components/subvit/test-substansi/soal/edit";
import TestSubstansiListTipeSoal from "./components/subvit/test-substansi/tipe-soal/list/index";
import TestSubstansiAddTipeSoal from "./components/subvit/test-substansi/tipe-soal/add";
import TestSubstansiEditTipeSoal from "./components/subvit/test-substansi/tipe-soal/edit";
import TestSubstansiReport from "./components/subvit/test-substansi/report";
import TestSubstansiReportIndividu from "./components/subvit/test-substansi/ctrl/Testsubstansi-report-individu";
import Survey from "./components/subvit/survey/ctrl/Survey";
import Trivia from "./components/subvit/triviax/ctrl/Trivia";
import TriviaClone from "./components/subvit/triviax/ctrl/Trivia-clone";
import TriviaTambah from "./components/subvit/triviax/ctrl/Trivia-tambah";
import TriviaEdit from "./components/subvit/triviax/ctrl/Trivia-edit";
import TriviaListSoal from "./components/subvit/triviax/ctrl/Trivia-list-soal";
import TriviaAddSoal from "./components/subvit/triviax/ctrl/Trivia-add-soal";
import TriviaEditSoal from "./components/subvit/triviax/ctrl/Trivia-edit-soal";
import TriviaListImport from "./components/subvit/triviax/ctrl/Trivia-list-import";
import TriviaReport from "./components/subvit/triviax/ctrl/Trivia-report";
import PelatihanTema from "./components/pelatihan/Tema/ctrl/Tema";
import PelatihanPelatihan from "./components/pelatihan/Pelatihan/ctrl/Pelatihan";
import PelatihanReviewpelatihan from "./components/pelatihan/Reviewpelatihan/ctrl/Reviewpelatihan";
import PelatihanTemaTambah from "./components/pelatihan/Tema/ctrl/Tema-tambah";
import PelatihanTemaEdit from "./components/pelatihan/Tema/ctrl/Tema-edit";
import PelatihanPendaftaran from "./components/Pendaftaran-Content/PendaftaranListAkademi";
import PelatihanPendaftaranContent from "./components/Pendaftaran-Content/PendaftaranListAkademi";
import PelatihanPendaftaranContentAdd from "./components/Pendaftaran-Content/PendaftaranDataList";
import PelatihanPendaftaranEdit from "./components/Pendaftaran-Content/EditFormBuilder";
import SiteManagementDashboard from "./components/partnership/dashboard/ctrl/Dashboard";
import SiteManagementUser from "./components/site-management/user/UserList";
import SiteManagementUserContent from "./components/site-management/user/userContent";
import PelatihanPelatihanTambah from "./components/pelatihan/Pelatihan/ctrl/Pelatihan-tambah";
import PelatihanPelatihanEdit from "./components/pelatihan/Pelatihan/ctrl/Pelatihan-edit";
import PelatihanViewContent from "./components/pelatihan/Pelatihan/ctrl/Pelatihan-view";
import PelatihanAkademiList from "./components/pelatihan/Akademi-Content/AkademiList";
import PelatihanAkademiTambah from "./components/pelatihan/Akademi-Content/AddAkademi";
import PelatihanAkademiEdit from "./components/pelatihan/Akademi-Content/AkademiContent";
import PelatihanRepositorieList from "./components/Pendaftaran-Content/PendaftaranList";
import FormRepositoriesList from "./components/Pendaftaran-Content/PendaftaranList";

import FormRepositoriesEdit from "./components/Pendaftaran-Content/Pendaftaran_element_edit";
import FormRepositoriesPreview from "./components/Pendaftaran-Content/Pendaftaran_element_preview";
import FormRepositoriesAdd from "./components/Pendaftaran-Content/Pendaftaran_element";

import MasterGroupRepo from "./components/repo/RepositoryList";
import MasterGroupRepoAdd from "./components/repo/RepositoryAdd";
import PendafataranCopyForm from "./components/Pendaftaran-Content/CopyForm";
import PreviewPendaftaranForm from "./components/Pendaftaran-Content/PreviewPendaftaran";
import PendaftaranSingleElement from "./components/Pendaftaran-Content/PendaftaranSingleElement";
import PartnershipDashboard from "./components/partnership/dashboard/ctrl/Dashboard";
import PartnershipDirektori_moupks from "./components/partnership/direktori_moupks/content/LIstKerjasamadisetujui";
import PartnershipMitra from "./components/partnership/mitra/ctrl/Mitra";
import PartnershipMitraTambah from "./components/partnership/mitra/ctrl/Mitra-tambah";
import PartnershipMitraEdit from "./components/partnership/mitra/ctrl/Mitra-edit";
import PartnershipKerjasamaReviewByAdmin from "./components/partnership/kerjasama-review/index";
import EditReviewKerjasama from "./components/partnership/kerjasama-review/edit";
import ViewReviewKerjasama from "./components/partnership/kerjasama-review/review";
import RepositoryAdd from "./components/repo/RepositoryAdd";
import AkademiPreview from "./components/pelatihan/Akademi-Content/AkademiPreview";
import PelatihanReviewpelatihanView from "./components/pelatihan/Reviewpelatihan/ctrl/Reviewpelatihan-view";
// import PartnershipKerjasama from './components/partnership/kerjasama/ctrl/Kerjasama';
import PartnershipKerjasama from "./components/partnership/kerjasama/Content/LIstKerjasama";
// import PartnershipKerjasamaTambah from './components/partnership/kerjasama/ctrl/Kerjasama-tambah';
import PartnershipKerjasamaTambah from "./components/partnership/kerjasama/Content/AddKerjasama";
import PartnershipMasteKerjasama from "./components/partnership/masterkerjasama/ListMasterKerjasama";
import PartnershipMasteKerjasamaAdd from "./components/partnership/masterkerjasama/AddMasterKerjasama";
import PartnershipMasteKerjasamaEdit from "./components/partnership/masterkerjasama/ContentMasterKerjasama";
// import PartnershipKerjasamaEdit from './components/partnership/kerjasama/ctrl/Kerjasama-edit';
import PartnershipKerjasamaReview from "./components/partnership/kerjasama/Content/ReviewKerjasama";
import PartnershipMoUReview from "./components/partnership/direktori_moupks/content/ReviewKerjasamadisetujui";
import PartnershipKerjasamaEdit from "./components/partnership/kerjasama/Content/EditKerjasama";
import PartnershipMoUEdit from "./components/partnership/direktori_moupks/content/EditKerjasamadisetujui";
import SubvitTestSubstansiTambah from "./components/subvit/test-substansi/ctrl/Testsubstansi-tambah";
import SubvitTestSubstansiEdit from "./components/subvit/test-substansi/ctrl/Testsubstansi-edit";
import SubvitSurveyTambah from "./components/subvit/survey/ctrl/Survey-tambah";
import SubvitSurveyEdit from "./components/subvit/survey/ctrl/Survey-edit";
import SubvitSurveyListImport from "./components/subvit/survey/ctrl/Survey-list-import";
import SubvitSurveyListIsian from "./components/subvit/survey/ctrl/Survey-list-isian";
import SubvitSurveyReportIndividu from "./components/subvit/survey/ctrl/Survey-report-individu";
import AddTTD from "./components/partnership/ttd/AddTtd";
import ListTtd from "./components/partnership/ttd/ListTtd";
import EditTTD from "./components/partnership/ttd/EditTtd";
import ListRekapPendaftaran from "./components/Pendaftaran-Content/ListRekapPendaftaran";
import DetailRekapPendaftaran from "./components/Pendaftaran-Content/DetailRekapPendaftaran";
import ListZonasi from "./components/site-management/zonasi/ListZonasi";
import ListDetailRekapPendaftaran from "./components/Pendaftaran-Content/SubDetailRekap";
import SubDetailRekapPendaftaran from "./components/Pendaftaran-Content/SubRekapPendaftaranDetail";
import PelatihanReportpelatihan from "./components/pelatihan/Reportpelatihan/ctrl/Reportpelatihan";
// import PelatihanReportpelatihanView from "./components/pelatihan/Reportpelatihan/ctrl/Reportpelatihan-view";
import PelatihanReportpelatihanView from "./components/pelatihan/Reportpelatihan/view";
import PelatihanTemaView from "./components/pelatihan/Tema/ctrl/Tema-view";
import ArtikelView from "./components/publikasi/artikel/ctrl/Artikel-View";
import ArtikelTambah from "./components/publikasi/artikel/ctrl/Artikel-Tambah";
import ArtikelEdit from "./components/publikasi/artikel/ctrl/Artikel-Edit";
import KategoriView from "./components/publikasi/kategori/ctrl/Kategori-View";
import KategoriTambah from "./components/publikasi/kategori/ctrl/Kategori-Tambah";
import KategoriEdit from "./components/publikasi/kategori/ctrl/Kategori-Edit";
import InformasiView from "./components/publikasi/informasi/ctrl/Informasi-View";
import InformasiTambah from "./components/publikasi/informasi/ctrl/Informasi-Tambah";
import InformasiEdit from "./components/publikasi/informasi/ctrl/Informasi-Edit";
import VideoView from "./components/publikasi/video/ctrl/Video-View";
import VideoEdit from "./components/publikasi/video/ctrl/Video-Edit";
import VideoTambah from "./components/publikasi/video/ctrl/Video-Tambah";
import FaqView from "./components/publikasi/faq/ctrl/Faq-View";
import FaqEdit from "./components/publikasi/faq/ctrl/Faq-Edit";
import FaqTambah from "./components/publikasi/faq/ctrl/Faq-Tambah";
import ImagetronView from "./components/publikasi/imagetron/ctrl/Imagetron-View";
import ImagetronEdit from "./components/publikasi/imagetron/ctrl/Imagetron-Edit";
import ImagetronTambah from "./components/publikasi/imagetron/ctrl/Imagetron-Tambah";
import EventView from "./components/publikasi/event/ctrl/Event-View";
import EventEdit from "./components/publikasi/event/ctrl/Event-Edit";
import EventTambah from "./components/publikasi/event/ctrl/Event-Tambah";
import GaleriView from "./components/publikasi/galeri/ctrl/Galeri-View";
import Dashboard from "./components/publikasi/dashboard/ctrl/Dashboard";
import Pengaturan from "./components/publikasi/pengaturan/ctrl/Pengaturan";
import PelatihanPelatihansView from "./components/pelatihan/Pelatihan/ctrl/Pelatihan-view";
import PartnershipMitraView from "./components/partnership/mitra/ctrl/Mitra-view";
import PendaftaranDataLKanban from "./components/Pendaftaran-Content/PendaftaranDataListKanban";
import PelatihanRekappendaftaran from "./components/pelatihan/Rekappendaftaran/ctrl/Rekappendaftaran";
import PelatihanRekappendaftaranView from "./components/pelatihan/Rekappendaftaran/ctrl/Rekappendaftaran-view";
import PelatihanRekappendaftaranDetail from "./components/pelatihan/Rekappendaftaran/ctrl/Rekappendaftaran-detail";
import PelatihanRekappendaftaranViewDaftarPeserta from "./components/pelatihan/Rekappendaftaran/ctrl/Rekappendaftaran-view-daftarpeserta";
import PelatihanRekappendaftaranImport from "./components/pelatihan/Rekappendaftaran/ctrl/Rekappendaftaran-view-import";
import PelatihanRekappendaftaranImportScore from "./components/pelatihan/Rekappendaftaran/ctrl/Rekappendaftaran-view-import-score";
import EditPendaftaranDataLKanban from "./components/Pendaftaran-Content/EditPendaftaranDataListKanban";
import CopyPendaftaranDataListKanban from "./components/Pendaftaran-Content/CopyPendaftaranDataListKanban";
import SubvitSurveyClone from "./components/subvit/survey/ctrl/Survey-clone";
import SubvitSurveyListSoal from "./components/subvit/survey/ctrl/Survey-list-soal";
import SubvitSurveyEditSoal from "./components/subvit/survey/ctrl/Survey-edit-soal";
import SubvitSurveyReport from "./components/subvit/survey/ctrl/Survey-report";
import SubvitSurveyAddSoal from "./components/subvit/survey/ctrl/Survey-add-soal";
import SubvitSurveyMaster from "./components/subvit/survey/ctrl/Survey-master";
import SubvitSurveyMasterTambah from "./components/subvit/survey/ctrl/Survey-master-tambah";
import SubvitSurveyMasterEdit from "./components/subvit/survey/ctrl/Survey-master-edit";
import SideWidgetView from "./components/publikasi/side-widget/ctrl/SideWidget-View";
import SideWidgetEdit from "./components/publikasi/side-widget/ctrl/SideWidget-Edit";
import SideWidgetTambah from "./components/publikasi/side-widget/ctrl/SideWidget-Tambah";

import ExportData from "./components/site-management/export-data/ctrl/ExportData";
import ExportDataTambah from "./components/site-management/export-data/ctrl/ExportData-Tambah";
import ExportDataDetail from "./components/site-management/export-data/ctrl/ExportData-Detail";
import MasterSatuanKerja from "./components/site-management/master/master-satuan-kerja/list";
import AddSatuanKerja from "./components/site-management/master/master-satuan-kerja/add";
import ViewSatuanKerja from "./components/site-management/master/master-satuan-kerja/view";
import EditSatuanKerja from "./components/site-management/master/master-satuan-kerja/edit";
import MasterZonasi from "./components/site-management/master/master-zonasi/list";
import AddZonasi from "./components/site-management/master/master-zonasi/add";
import ViewZonasi from "./components/site-management/master/master-zonasi/view";
import EditZonasi from "./components/site-management/master/master-zonasi/edit";
import MasterReference from "./components/site-management/reference/list";
import AddReference from "./components/site-management/reference/add";
import ViewReference from "./components/site-management/reference/view";
import EditReference from "./components/site-management/reference/edit";
import SettingAddon from "./components/site-management/setting-addon/ctrl/SettingAddon";
import SettingAddonTambah from "./components/site-management/setting-addon/ctrl/SettingAddon-Tambah";
import SettingAddonEdit from "./components/site-management/setting-addon/ctrl/SettingAddon-Edit";
import SettingPage from "./components/site-management/setting-page/ctrl/SettingPage";
import SettingPageTambah from "./components/site-management/setting-page/ctrl/SettingPage-Tambah";
import SettingPageEdit from "./components/site-management/setting-page/ctrl/SettingPage-Edit";
import SettingPagePreview from "./components/site-management/setting-page/ctrl/SettingPage-Preview";
import SettingGeneral from "./components/site-management/setting-general/ctrl/SettingGeneral";
import SettingMenu from "./components/site-management/setting-menu/ctrl/SettingMenu";
import SettingMenuTambah from "./components/site-management/setting-menu/ctrl/SettingMenu-Tambah";
import SettingMenuEdit from "./components/site-management/setting-menu/ctrl/SettingMenu-Edit";
import SettingPelatihan from "./components/site-management/setting-pelatihan/ctrl/SettingPelatihan";
import BroadcastEmail from "./components/site-management/setting-pelatihan/ctrl/BroadcastEmail";
import Broadcast from "./components/site-management/broadcast/ctrl/Broadcast-View";
import BroadcastTambah from "./components/site-management/broadcast/ctrl/Broadcast-Tambah";
import BroadcastEdit from "./components/site-management/broadcast/ctrl/Broadcast-Edit";
import RequestWABlastTambah from "./components/site-management/setting-pelatihan/ctrl/RequestWABlast-tambah";
import RequestWABlastView from "./components/site-management/setting-pelatihan/ctrl/RequestWABlast-view";
import RequestWABlastViewContentDetail from "./components/site-management/setting-pelatihan/ctrl/RequestWABlast-detail";

import WAblastContent from "./components/publikasi/WABlast/ctrl/WAblast";
import WAblastViewContent from "./components/publikasi/WABlast/ctrl/WAblast-view";

import TambahMou from "./components/partnership/direktori_moupks/content/AddKerjasamadisetujui";
import TteP12 from "./components/sertifikat/kelola-sertifikat/ctrl/Ttd-View";
import SertifikatList from "./components/sertifikat/daftar-sertifikat/ctrl/Sertifikat-View";
import SertifikatPersetujuan from "./components/sertifikat/persetujuan/ctrl/Persetujuan-View";
import SertifikatPesertaList from "./components/sertifikat/daftar-sertifikat/ctrl/Sertifikat-Peserta-View";
import SertifikatUploadBackground from "./components/sertifikat/daftar-sertifikat/ctrl/Sertifikat-Upload-Background";
import LogSertifikat from "./components/sertifikat/log-sertifikat/ctrl/Log-View";

import PelatihanSilabus from "./components/pelatihan/Silabus/ctrl/Silabus";
import PelatihanSilabusTambah from "./components/pelatihan/Silabus/ctrl/Silabus-create";
import PelatihanSilabusEdit from "./components/pelatihan/Silabus/ctrl/Silabus-edit";
import UserAdmin from "./components/site-management/user-management/ctrl/UserAdmin-View";
import UserAdminPreview from "./components/site-management/user-management/ctrl/UserAdmin-Preview-View";
import UserMitra from "./components/site-management/user-management/ctrl/UserMitra-View";
import UserMitraAdd from "./components/site-management/user-management/ctrl/UserMitra-Tambah-View";
import UserMitraEdit from "./components/site-management/user-management/ctrl/UserMitra-Edit-View";
import UserMitraPreview from "./components/site-management/user-management/ctrl/UserMitra-Preview-View";
import UserDTS from "./components/site-management/user-management/ctrl/UserDTS-View";
import UserDTSEdit from "./components/site-management/user-management/ctrl/UserDTS-Edit";
import UserDTSPreview from "./components/site-management/user-management/ctrl/UserDTS-Preview";

import DashboardSubvit from "./components/subvit/dashboard/ctrl/DashboardSubvit";

import Role from "./components/site-management/role/ctrl/Role";
import RoleTambah from "./components/site-management/role/ctrl/Role-Tambah";
import RoleDetail from "./components/site-management/role/ctrl/Role-Detail";
import RoleEdit from "./components/site-management/role/ctrl/Role-Edit";
import UserAdminAdd from "./components/site-management/user-management/ctrl/UserAdmin-Tambah-View";
import FormRepoView from "./components/pelatihan/Form/ctrl/FormRepo-View";
import UserAdminEdit from "./components/site-management/user-management/ctrl/UserAdmin-Edit-View";
import SignInMitra from "./pages/sign-in-mitra";
import FormKategori from "./components/pelatihan/Form/ctrl/FormKategori-View";
import StrestTest from "./pages/Strest-test";
import FormKategoriTambah from "./components/pelatihan/Form/ctrl/FormKategoriTambah-View";
import FormKategoriEdit from "./components/pelatihan/Form/ctrl/FormKategoriEdit-View";
import Cookies from "js-cookie";
import LogSubm from "./components/site-management/log-subm/ctrl/LogSubm";
import LogSubmDetail from "./components/site-management/log-subm/ctrl/LogSubm-Detail";

import Panduan from "./components/site-management/panduan/ctrl/Panduan-View";
import PanduanTambah from "./components/site-management/panduan/ctrl/Panduan-Tambah";
import PanduanEdit from "./components/site-management/panduan/ctrl/Panduan-Edit";
import PanduanStatis from "./components/site-management/panduan/ctrl/Panduan-Statis";
import PanduanStatisDetail from "./components/site-management/panduan/ctrl/Panduan-Statis-Detail";
import UserAccountView from "./components/settings/UserAccountView";
import FormListKanbanAdd from "./components/Pendaftaran-Content/FormListKanbanAdd";
import { cekPermition, logout } from "./components/AksesHelper";
import Swal from "sweetalert2";
import Diploy from "./components/dashboard/diploy";
import axios from "axios";
import Header from "./components/Header";
import moment from "moment";
// import { ApmRoute } from '@elastic/apm-rum-react';
// import { withTransaction } from '@elastic/apm-rum-react';

function App() {
  const token = Cookies.get("token");
  const partner_id = Cookies.get("partner_id");
  const nav = useNavigate();

  const logout = () => {
    Cookies.remove("partner_id");
    Cookies.remove("token");
    Cookies.remove("user_id");
    Cookies.remove("user_name");
    Cookies.remove("user_role");
    Cookies.remove("menus");
    Cookies.remove("user_email");
    Cookies.remove("role_id");
    Cookies.remove("role_id_user");
    Cookies.remove("user_role_name");
    Cookies.remove("user_nik");
    Cookies.remove("user_email_reset");
    Cookies.remove("pin");
    window.location.href = "/";
  };

  const cekAuth = () => {
    axios
      .post(
        process.env.REACT_APP_BASE_API_URI + "/cekauth",
        {},
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + Cookies.get("token"),
          },
        },
      )
      .then(function (response) {})
      .catch(function (error) {
        Swal.fire({
          title: "Unauthenticated.",
          text: "Silahkan login ulang",
          icon: "warning",
          confirmButtonText: "Ok",
        });

        logout();
      });
  };

  useEffect(() => {
    if (
      Cookies.get("token") != null &&
      window.location.pathname != "/" &&
      window.location.pathname != "/signin-mitra" &&
      window.location.pathname != "/reset-password" &&
      window.location.pathname != "/verifikasi-otp" &&
      window.location.pathname != "/konfirm-password"
    ) {
      cekAuth();

      setInterval(() => {
        cekAuth();
      }, 60000);
    }

    try {
      window.$("input.number-only").bind({
        keydown: function (e) {
          if (e.shiftKey === true) {
            if (e.which == 9) {
              return true;
            }
            return false;
          }
          if (e.which > 57) {
            return false;
          }
          if (e.which == 32) {
            return false;
          }
          return true;
        },
      });

      window.$("input.prevent-quote").bind({
        keydown: function (e) {
          if (e.key == "'" || e.key == '"') {
            return false;
          }
          return true;
        },
      });
    } catch (err) {}
  }, []);

  useEffect(() => {
    // console.log(nav.ur)
    // if (!["/", "/signin-mitra"].includes(window.location.pathname)) {
    //   if (token || partner_id) {
    //     window.location.href = "/"
    //   }
    // }

    if (Cookies.get("token") != null && cekPermition().view !== 1) {
      /*
      Swal.fire({
          title: "Akses Tidak Diizinkan",
          html: "<i> Anda akan kembali kehalaman login </i>",
          icon: "warning",
        }).then(()=>{
         // logout();
        });
        */
    }
    let segment_url = window.location.pathname;
    if (
      Cookies.get("token") == null &&
      segment_url != "/" &&
      window.location.pathname != "/signin-mitra" &&
      window.location.pathname != "/reset-password" &&
      window.location.pathname != "/verifikasi-otp" &&
      window.location.pathname != "/konfirm-password"
    ) {
      window.location = "/";

      Swal.fire({
        title: "Unauthenticated.",
        text: "Silahkan login ulang",
        icon: "warning",
        confirmButtonText: "Ok",
      }).then((result) => {
        if (result.isConfirmed) {
          window.location = "/";
        }
      });
    }
  }, []);

  return (
    <div className="App">
      <>
        <Routes>
          <Route
            path="dashboard/digitalent"
            element={<DashboardDigitalent />}
          />

          <Route path="dashboard/diploy" element={<Diploy />} />
          <Route
            path="dashboard/dashboardevaluasi"
            element={<DashboardEvaluasi />}
          />
          <Route path="dashboard/kepuasan-layanan" element={<DashboardSKM />} />
          <Route path="dashboard/simonas" element={<DashboardSimonas />} />
          <Route path="dashboard/beasiswa" element={<DashboardBeasiswa />} />
          <Route path="dashboard/ga" element={<DashboardAnalytics />} />

          <Route path="sign-up" element={<SignUp />} />
          <Route path="/" element={<SignIn />} />
          <Route path="reset-password" element={<ResetPassword />} />
          <Route path="verifikasi-otp" element={<VerifikasiOtp />} />
          <Route path="konfirm-password" element={<KonfirmPassword />} />

          <Route path="/signin-mitra" element={<SignInMitra />} />

          <Route path="/reload-page-502" element={<ReloadPage />} />

          <Route path="forget-password" element={<ForgetPassword />} />
          <Route
            path="daftar-peserta/beasiswa"
            element={<DaftarPesertaBeasiswa />}
          />
          <Route
            path="daftar-peserta/digitalent"
            element={<DaftarPesertaDigitalent />}
          ></Route>
          <Route
            path="daftar-peserta/simonas"
            element={<DaftarPesertaSimonas />}
          ></Route>
          <Route
            path="pelatihan/akademi"
            element={<PelatihanAkademi />}
          ></Route>
          <Route
            path="pelatihan/pendaftaran"
            element={<PelatihanPendaftaran />}
          ></Route>

          <Route
            path="pelatihan/akademi/add"
            element={<PelatihanAkademiAdd />}
          ></Route>
          <Route
            path="subvit/test-substansi"
            element={<TestSubstansi />}
          ></Route>
          <Route
            path="subvit/test-substansi/add"
            element={<TestSubstansiTambah />}
          ></Route>
          <Route
            path="subvit/test-substansi/edit/:id"
            element={<TestSubstansiEdit />}
          ></Route>
          <Route
            path="subvit/test-substansi/soal/list/:id"
            element={<TestSubstansiListSoal />}
          ></Route>
          <Route
            path="subvit/test-substansi/soal/add/:id"
            element={<TestSubstansiAddSoal />}
          ></Route>
          <Route
            path="subvit/test-substansi/soal/import/:id"
            element={<TestSubstansiImportSoal />}
          ></Route>
          <Route
            path="subvit/test-substansi/soal/edit/:testid/:id"
            element={<TestSubstansiEditSoal />}
          ></Route>
          <Route
            path="subvit/test-substansi/clone/:id"
            element={<TestSubstansiClone />}
          ></Route>
          <Route
            path="subvit/test-substansi/report/:id"
            element={<TestSubstansiReport />}
          ></Route>
          <Route
            path="subvit/test-substansi/report/:id/:no/:training"
            element={<TestSubstansiReportIndividu />}
          ></Route>
          <Route
            path="subvit/test-substansi/tipe-soal/list"
            element={<TestSubstansiListTipeSoal />}
          ></Route>
          <Route
            path="subvit/test-substansi/tipe-soal/add"
            element={<TestSubstansiAddTipeSoal />}
          ></Route>
          <Route
            path="subvit/test-substansi/tipe-soal/edit/:id"
            element={<TestSubstansiEditTipeSoal />}
          ></Route>
          <Route path="subvit/survey" element={<Survey />}></Route>
          <Route path="subvit/trivia" element={<Trivia />}></Route>
          <Route path="subvit/trivia/tambah" element={<TriviaTambah />}></Route>
          <Route path="subvit/trivia/edit/:id" element={<TriviaEdit />}></Route>
          <Route
            path="subvit/trivia/clone/:id"
            element={<TriviaClone />}
          ></Route>
          <Route
            path="subvit/trivia/list-soal/:id"
            element={<TriviaListSoal />}
          ></Route>
          <Route
            path="subvit/trivia/add-soal/:id"
            element={<TriviaAddSoal />}
          ></Route>
          <Route
            path="subvit/trivia/edit-soal/:id/:no"
            element={<TriviaEditSoal />}
          ></Route>
          <Route
            path="subvit/trivia/list-soal/:id/import"
            element={<TriviaListImport />}
          ></Route>
          <Route
            path="subvit/trivia/report/:id"
            element={<TriviaReport />}
          ></Route>
          <Route path="pelatihan/tema" element={<PelatihanTema />}></Route>
          <Route
            path="pelatihan/pelatihan"
            element={<PelatihanPelatihan />}
          ></Route>
          <Route
            path="pelatihan/reviewpelatihan"
            element={<PelatihanReviewpelatihan />}
          ></Route>
          <Route
            path="pelatihan/tambah-tema"
            element={<PelatihanTemaTambah />}
          ></Route>
          <Route
            path="pelatihan/edit-tema/:id"
            element={<PelatihanTemaEdit />}
          ></Route>
          <Route
            path="pelatihan/akademi"
            element={<PelatihanAkademi />}
            exact
          ></Route>
          <Route
            path="pelatihan/akademi/content/:id"
            element={<PelatihanAkademiContent />}
            exact
          ></Route>
          <Route
            path="pelatihan/pendaftaran"
            element={<PelatihanPendaftaran />}
            exact
          ></Route>
          <Route
            path="pelatihan/pendaftaran/content/:id"
            element={<PelatihanPendaftaranContent />}
            exact
          ></Route>
          <Route
            path="pelatihan/pendaftaran/edit/:id"
            element={<EditPendaftaranDataLKanban />}
            exact
          ></Route>
          <Route
            path="pelatihan/pendaftaran/edit_old/:id"
            element={<PelatihanPendaftaranEdit />}
            exact
          ></Route>
          <Route
            path="pelatihan/pendaftaran/add_old"
            element={<PelatihanPendaftaranContentAdd />}
            exact
          ></Route>
          <Route
            path="site-management/dashboard"
            element={<SiteManagementDashboard />}
          ></Route>
          <Route
            path="site-management/user/list"
            element={<SiteManagementUser />}
            exact
          ></Route>
          <Route
            path="site-management/user/content/:id"
            element={<SiteManagementUserContent />}
            exact
          ></Route>

          <Route
            path="pelatihan/tambah-pelatihan"
            element={<PelatihanPelatihanTambah />}
            exact
          ></Route>
          <Route
            path="pelatihan/edit-pelatihan/:id"
            element={<PelatihanPelatihanEdit />}
            exact
          ></Route>
          <Route
            path="pelatihan/akademi/list"
            element={<PelatihanAkademiList />}
            exact
          ></Route>
          <Route
            path="pelatihan/akademi/add"
            element={<PelatihanAkademiTambah />}
            exact
          ></Route>
          <Route
            path="pelatihan/akademi/edit/:id"
            element={<PelatihanAkademiEdit />}
            exact
          ></Route>
          <Route
            path="pelatihan/repositories/list"
            element={<PelatihanRepositorieList />}
            exact
          ></Route>
          <Route
            path="repository/element"
            element={<FormRepositoriesList />}
            exact
          ></Route>
          <Route
            path="repository/element/add"
            element={<PendaftaranSingleElement />}
            exact
          ></Route>
          <Route
            path="repository/element/add/old"
            element={<FormRepositoriesAdd />}
            exact
          ></Route>

          <Route
            path="pelatihan/element_old"
            element={<FormRepositoriesList />}
            exact
          ></Route>

          <Route path="pelatihan/element" element={<FormRepoView />}></Route>

          <Route
            path="pelatihan/kategori-element"
            element={<FormRepoView />}
          ></Route>
          <Route
            path="pelatihan/kategori-form"
            element={<FormKategori />}
          ></Route>

          <Route path="setting/account" element={<UserAccountView />}></Route>

          <Route
            path="pelatihan/kategori-form-tambah"
            element={<FormKategoriTambah />}
          ></Route>

          <Route
            path="pelatihan/kategori-form-edit/:id"
            element={<FormKategoriEdit />}
          ></Route>

          <Route
            path="pelatihan/element/add"
            element={<PendaftaranSingleElement />}
            exact
          ></Route>
          <Route
            path="pelatihan/element/edit/:id"
            element={<FormRepositoriesEdit />}
            exact
          ></Route>
          <Route
            path="pelatihan/element/preview/:id"
            element={<FormRepositoriesPreview />}
            exact
          ></Route>
          <Route
            path="pelatihan/element/add/old"
            element={<FormRepositoriesAdd />}
            exact
          ></Route>

          <Route
            path="master/grouprepo"
            element={<MasterGroupRepo />}
            exact
          ></Route>
          <Route
            path="master/grouprepo/add"
            element={<MasterGroupRepoAdd />}
            exact
          ></Route>
          <Route
            path="pendaftaran/copy/:id"
            element={<CopyPendaftaranDataListKanban />}
            exact
          ></Route>
          <Route
            path="pendaftaran/copy_old/:id"
            element={<PendafataranCopyForm />}
            exact
          ></Route>
          <Route
            path="pendaftaran/preview/:id"
            element={<PreviewPendaftaranForm />}
            exact
          ></Route>
          <Route
            path="pendaftaran/repositories"
            element={<PendaftaranSingleElement />}
            exact
          ></Route>
          <Route
            path="partnership/mitra"
            element={<PartnershipMitra />}
          ></Route>
          <Route
            path="partnership/tambah-mitra"
            element={<PartnershipMitraTambah />}
          ></Route>
          <Route
            path="partnership/edit-mitra/:id"
            element={<PartnershipMitraEdit />}
          ></Route>
          <Route
            path="pelatihan/akademi/preview/:id"
            element={<AkademiPreview />}
          ></Route>
          <Route
            path="pelatihan/view-pelatihan/:id"
            element={<PelatihanReviewpelatihanView />}
          ></Route>
          <Route
            path="partnership/dashboard"
            element={<PartnershipDashboard />}
          ></Route>
          <Route
            path="partnership/direktori_moupks"
            element={<PartnershipDirektori_moupks />}
          ></Route>
          <Route
            path="partnership/kerjasama"
            element={<PartnershipKerjasama />}
            exact
          ></Route>
          <Route
            path="partnership/tambah-kerjasama"
            element={<PartnershipKerjasamaTambah />}
            exact
          ></Route>
          <Route
            path="partnership/kerjasama/review/:id"
            element={<PartnershipKerjasamaReview />}
            exact
          ></Route>
          <Route
            path="partnership/kerjasama-review"
            element={<PartnershipKerjasamaReviewByAdmin />}
            exact
          ></Route>
          <Route
            path="partnership/kerjasama-review/edit/:id"
            element={<EditReviewKerjasama />}
            exact
          ></Route>
          <Route
            path="partnership/kerjasama-review/view/:id"
            element={<ViewReviewKerjasama />}
            exact
          ></Route>
          <Route
            path="partnership/mou/review/:id"
            element={<PartnershipMoUReview />}
            exact
          ></Route>
          <Route
            path="partnership/kerjasama/edit/:id"
            element={<PartnershipKerjasamaEdit />}
            exact
          ></Route>
          <Route
            path="partnership/mou/edit/:id"
            element={<PartnershipMoUEdit />}
            exact
          ></Route>
          <Route
            path="partnership/masterkerjasama"
            element={<PartnershipMasteKerjasama />}
            exact
          ></Route>
          <Route
            path="partnership/masterkerjasama/add"
            element={<PartnershipMasteKerjasamaAdd />}
            exact
          ></Route>
          <Route
            path="partnership/masterkerjasama/edit/:id"
            element={<PartnershipMasteKerjasamaEdit />}
            exact
          ></Route>
          <Route
            path="partnership/edit-kerjasama/:id"
            element={<PartnershipKerjasamaEdit />}
          ></Route>
          <Route path="partnership/ttd" element={<ListTtd />} exact></Route>
          <Route path="partnership/ttd/add" element={<AddTTD />} exact></Route>
          <Route
            path="partnership/ttd/edit/:id"
            element={<EditTTD />}
            exact
          ></Route>
          <Route
            path="subvit/tambah-testsubstansi"
            element={<SubvitTestSubstansiTambah />}
          ></Route>
          <Route
            path="subvit/edit-testsubstansi/:id"
            element={<SubvitTestSubstansiEdit />}
          ></Route>
          <Route
            path="subvit/survey/tambah/:id"
            element={<SubvitSurveyTambah />}
          ></Route>
          <Route
            path="subvit/survey/edit/:id"
            element={<SubvitSurveyEdit />}
          ></Route>
          <Route
            path="subvit/survey/clone/:id"
            element={<SubvitSurveyClone />}
          ></Route>
          <Route
            path="subvit/survey/list-soal/:id"
            element={<SubvitSurveyListSoal />}
          ></Route>
          <Route
            path="subvit/survey/edit-soal/:id/:no"
            element={<SubvitSurveyEditSoal />}
          ></Route>
          <Route
            path="subvit/survey/report/:id"
            element={<SubvitSurveyReport />}
          ></Route>
          <Route
            path="subvit/survey/master"
            element={<SubvitSurveyMaster />}
          ></Route>
          <Route
            path="subvit/survey/master/tambah"
            element={<SubvitSurveyMasterTambah />}
          ></Route>
          <Route
            path="subvit/survey/master/edit/:id/:no"
            element={<SubvitSurveyMasterEdit />}
          ></Route>
          <Route
            path="subvit/survey/add-soal/:id"
            element={<SubvitSurveyAddSoal />}
          ></Route>
          <Route
            path="subvit/survey/list-soal/:id/import"
            element={<SubvitSurveyListImport />}
          ></Route>
          <Route
            path="subvit/survey/view-isian-soal/:id"
            element={<SubvitSurveyListIsian />}
          ></Route>
          <Route
            path="subvit/survey/report/:id/:no/:training"
            element={<SubvitSurveyReportIndividu />}
          ></Route>
          <Route
            path="rekap/pendaftaran"
            element={<ListRekapPendaftaran />}
            exact
          ></Route>
          <Route
            path="rekap/pendaftaran/detail/:id"
            element={<DetailRekapPendaftaran />}
            exact
          ></Route>
          <Route
            path="site-management/zonasi"
            element={<ListZonasi />}
            exact
          ></Route>
          <Route
            path="rekap/pendaftaran/view/:id"
            element={<ListDetailRekapPendaftaran />}
            exact
          ></Route>
          <Route
            path="rekap/pendaftaran/view/detail/:id"
            element={<SubDetailRekapPendaftaran />}
            exact
          ></Route>
          <Route
            path="pelatihan/reportpelatihan"
            element={<PelatihanReportpelatihan />}
          ></Route>
          <Route
            path="pelatihan/view-reportpelatihan/:id"
            element={<PelatihanReportpelatihanView />}
          ></Route>
          <Route
            path="pelatihan/view-tema/:id"
            element={<PelatihanTemaView />}
          ></Route>
          <Route path="publikasi/artikel" element={<ArtikelView />}></Route>
          <Route
            path="publikasi/tambah-artikel"
            element={<ArtikelTambah />}
          ></Route>
          <Route
            path="publikasi/edit-artikel/:id/:tag"
            element={<ArtikelEdit />}
          ></Route>
          <Route path="publikasi/kategori" element={<KategoriView />}></Route>
          <Route
            path="publikasi/tambah-kategori"
            element={<KategoriTambah />}
          ></Route>
          <Route
            path="publikasi/edit-kategori/:id"
            element={<KategoriEdit />}
          ></Route>
          <Route path="publikasi/informasi" element={<InformasiView />}></Route>
          <Route
            path="publikasi/tambah-informasi"
            element={<InformasiTambah />}
          ></Route>
          <Route
            path="publikasi/edit-informasi/:id/:tag"
            element={<InformasiEdit />}
          ></Route>
          <Route path="publikasi/video" element={<VideoView />}></Route>
          <Route
            path="publikasi/tambah-video"
            element={<VideoTambah />}
          ></Route>
          <Route
            path="publikasi/edit-video/:id/:tag"
            element={<VideoEdit />}
          ></Route>
          <Route path="publikasi/faq" element={<FaqView />}></Route>
          <Route path="publikasi/edit-faq/:id" element={<FaqEdit />}></Route>
          <Route path="publikasi/tambah-faq" element={<FaqTambah />}></Route>
          <Route path="publikasi/imagetron" element={<ImagetronView />}></Route>
          <Route
            path="publikasi/edit-imagetron/:id"
            element={<ImagetronEdit />}
          ></Route>
          <Route
            path="publikasi/tambah-imagetron"
            element={<ImagetronTambah />}
          ></Route>
          <Route path="publikasi/event" element={<EventView />}></Route>
          <Route
            path="publikasi/edit-event/:id"
            element={<EventEdit />}
          ></Route>
          <Route
            path="publikasi/tambah-event"
            element={<EventTambah />}
          ></Route>
          <Route path="publikasi/galeri" element={<GaleriView />}></Route>
          <Route path="publikasi/dashboard" element={<Dashboard />}></Route>
          <Route path="publikasi/pengaturan" element={<Pengaturan />}></Route>
          <Route
            path="pelatihan/detail-pelatihan/:id"
            element={<PelatihanViewContent />}
          ></Route>
          <Route
            path="partnership/view-mitra/:id"
            element={<PartnershipMitraView />}
          ></Route>
          <Route
            path="pelatihan/pendaftaran/add"
            element={<PendaftaranDataLKanban />}
          ></Route>

          <Route
            path="pelatihan/pendaftaran/add_new"
            element={<FormListKanbanAdd />}
          ></Route>

          <Route
            path="pelatihan/rekappendaftaran"
            element={<PelatihanRekappendaftaran />}
          ></Route>
          <Route
            path="pelatihan/view-rekappendaftaran/:id"
            element={<PelatihanRekappendaftaranView />}
          ></Route>
          <Route
            path="pelatihan/detail-rekappendaftaran/:id"
            element={<PelatihanRekappendaftaranDetail />}
          ></Route>
          <Route
            path="pelatihan/view-rekappendaftaran-daftarpeserta/:id/:idform"
            element={<PelatihanRekappendaftaranViewDaftarPeserta />}
          ></Route>
          <Route
            path="pelatihan/import-rekappendaftaran-daftarpeserta/:id"
            element={<PelatihanRekappendaftaranImport />}
          ></Route>
          <Route
            path="pelatihan/import-rekappendaftaran-score/:id"
            element={<PelatihanRekappendaftaranImportScore />}
          ></Route>
          <Route
            path="publikasi/side-widget"
            element={<SideWidgetView />}
          ></Route>
          <Route
            path="publikasi/edit-side-widget/:id"
            element={<SideWidgetEdit />}
          ></Route>
          <Route
            path="publikasi/tambah-side-widget"
            element={<SideWidgetTambah />}
          ></Route>

          <Route path="site-management/administrator" element={<UserAdmin />} />
          <Route
            path="site-management/administrator/tambah"
            element={<UserAdminAdd />}
          />
          <Route
            path="site-management/administrator/preview/:id"
            element={<UserAdminPreview />}
          />
          <Route
            path="site-management/administrator/edit/:id"
            element={<UserAdminEdit />}
          />

          <Route path="site-management/mitra" element={<UserMitra />} />
          <Route
            path="site-management/mitra/tambah"
            element={<UserMitraAdd />}
          />
          <Route
            path="site-management/mitra/edit/:id"
            element={<UserMitraEdit />}
          />
          <Route
            path="site-management/mitra/preview/:id"
            element={<UserMitraPreview />}
          />

          <Route path="site-management/user-dts" element={<UserDTS />} />
          <Route
            path="site-management/user-dts/edit/:id"
            element={<UserDTSEdit />}
          />
          <Route
            path="site-management/user-dts/preview/:id"
            element={<UserDTSPreview />}
          />

          <Route
            path="site-management/setting/general"
            element={<SettingGeneral />}
          />
          <Route
            path="site-management/setting/page"
            element={<SettingPage />}
          />
          <Route
            path="site-management/setting/page/tambah"
            element={<SettingPageTambah />}
          />
          <Route
            path="site-management/setting/page/edit/:id"
            element={<SettingPageEdit />}
          />
          <Route
            path="site-management/setting/page/preview/:id"
            element={<SettingPagePreview />}
          />
          <Route
            path="site-management/setting/pelatihan"
            element={<SettingPelatihan />}
          />

          <Route
            path="site-management/broadcast-email"
            element={<BroadcastEmail />}
          />

          <Route path="site-management/broadcast" element={<Broadcast />} />
          <Route
            path="site-management/broadcast/tambah"
            element={<BroadcastTambah />}
          />
          <Route
            path="site-management/broadcast/edit/:id"
            element={<BroadcastEdit />}
          />

          <Route path="publikasi/wa-blast/" element={<WAblastContent />} />

          <Route
            path="publikasi/wa-blast/detail/:id"
            element={<WAblastViewContent />}
          />

          <Route
            path="site-management/whatsapp-blast/"
            element={<RequestWABlastView />}
          />

          <Route
            path="site-management/whatsapp-blast/detail/:id"
            element={<RequestWABlastViewContentDetail />}
          />

          <Route
            path="site-management/whatsapp-blast/tambah"
            element={<RequestWABlastTambah />}
          />

          <Route path="site-management/export-data" element={<ExportData />} />
          <Route
            path="site-management/export-data/tambah"
            element={<ExportDataTambah />}
          />
          <Route
            path="site-management/export-data/detail/:id"
            element={<ExportDataDetail />}
          />

          <Route
            path="site-management/master/satuan-kerja"
            element={<MasterSatuanKerja />}
          />
          <Route
            path="site-management/master/satuan-kerja/add"
            element={<AddSatuanKerja />}
          />
          <Route
            path="site-management/master/satuan-kerja/view/:id"
            element={<ViewSatuanKerja />}
          />
          <Route
            path="site-management/master/satuan-kerja/edit/:id"
            element={<EditSatuanKerja />}
          />
          <Route
            path="site-management/master/zonasi"
            element={<MasterZonasi />}
          />
          <Route
            path="site-management/master/zonasi/add"
            element={<AddZonasi />}
          />
          <Route
            path="site-management/master/zonasi/view/:id"
            element={<ViewZonasi />}
          />
          <Route
            path="site-management/master/zonasi/edit/:id"
            element={<EditZonasi />}
          />

          <Route
            path="site-management/reference"
            element={<MasterReference />}
          />
          <Route
            path="site-management/reference/add"
            element={<AddReference />}
          />
          <Route
            path="site-management/reference/view/:id"
            element={<ViewReference />}
          />
          <Route
            path="site-management/reference/edit/:id"
            element={<EditReference />}
          />

          <Route path="partnership/tambahmou" element={<TambahMou />} />

          <Route
            path="pelatihan/silabus"
            element={<PelatihanSilabus />}
          ></Route>
          <Route
            path="pelatihan/create-silabus"
            element={<PelatihanSilabusTambah />}
          ></Route>
          <Route
            path="pelatihan/edit-silabus/:id"
            element={<PelatihanSilabusEdit />}
          ></Route>

          <Route
            path="site-management/setting/general"
            element={<SettingGeneral />}
          />
          <Route
            path="site-management/setting/page"
            element={<SettingPage />}
          />
          <Route
            path="site-management/setting/page/tambah"
            element={<SettingPageTambah />}
          />
          <Route
            path="site-management/setting/page/edit/:id"
            element={<SettingPageEdit />}
          />
          <Route
            path="site-management/setting/page/preview/:id"
            element={<SettingPagePreview />}
          />

          <Route
            path="site-management/setting/menu"
            element={<SettingMenu />}
          />

          <Route
            path="site-management/setting/menu/tambah"
            element={<SettingMenuTambah />}
          />

          <Route
            path="site-management/setting/menu/edit/:id"
            element={<SettingMenuEdit />}
          />

          <Route path="site-management/export-data" element={<ExportData />} />
          <Route
            path="site-management/master/satuan-kerja"
            element={<MasterSatuanKerja />}
          />
          <Route
            path="site-management/master/zonasi"
            element={<MasterZonasi />}
          />
          <Route
            path="site-management/master/zonasi/add"
            element={<AddZonasi />}
          />
          <Route
            path="site-management/master/zonasi/view/:id"
            element={<ViewZonasi />}
          />
          <Route
            path="site-management/master/zonasi/edit/:id"
            element={<EditZonasi />}
          />
          <Route path="partnership/tambahmou" element={<TambahMou />} />
          <Route path="sertifikat/kelola-sertifikat" element={<TteP12 />} />

          <Route
            path="sertifikat/daftar-sertifikat"
            element={<SertifikatList />}
          />
          <Route
            path="sertifikat/persetujuan"
            element={<SertifikatPersetujuan />}
          />
          <Route
            path="sertifikat/sertifikat-daftar-peserta/:id"
            element={<SertifikatPesertaList />}
          />
          <Route
            path="sertifikat/upload-background"
            element={<SertifikatUploadBackground />}
          />
          <Route path="sertifikat/log-sertifikat" element={<LogSertifikat />} />

          <Route
            path="site-management/setting/addon"
            element={<SettingAddon />}
          />
          <Route
            path="site-management/setting/addon/tambah"
            element={<SettingAddonTambah />}
          />
          <Route
            path="site-management/setting/addon/edit/:id"
            element={<SettingAddonEdit />}
          />
          <Route path="subvit/dashboard" element={<DashboardSubvit />} />
          <Route path="site-management/role" element={<Role />} />
          <Route path="subvit/dashboard" element={<DashboardSubvit />} />
          <Route path="site-management/role" element={<Role />} />

          <Route path="site-management/role/tambah" element={<RoleTambah />} />
          <Route
            path="site-management/role/detail/:id"
            element={<RoleDetail />}
          />
          <Route path="site-management/role/edit/:id" element={<RoleEdit />} />

          <Route path="strest-test" element={<StrestTest />} />

          <Route path="site-management/panduan" element={<Panduan />} />
          <Route
            path="site-management/panduan/tambah"
            element={<PanduanTambah />}
          />
          <Route
            path="site-management/panduan/edit/:id"
            element={<PanduanEdit />}
          />
          <Route path="panduan" element={<PanduanStatis />} />
          <Route path="panduan/detail/:id" element={<PanduanStatisDetail />} />

          <Route path="site-management/subm/log-subm" element={<LogSubm />} />
          <Route
            path="site-management/subm/log-subm/detail/:id"
            element={<LogSubmDetail />}
          />
        </Routes>
      </>
    </div>
  );
}

export default App;
