import moment from "moment";

export function fileToBase64(file) {
  const reader = new FileReader();
  reader.readAsDataURL(file);
  return reader.result;
}

export function base64ToFile(dataurl, filename) {
  var arr = dataurl.split(","),
    mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]),
    n = bstr.length,
    u8arr = new Uint8Array(n);

  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }

  return new File([u8arr], filename, { type: mime });
}

export function customLoader() {
  <div style={{ padding: "24px" }}>
    <img src="/assets/media/loader/loader-biru.gif" />
  </div>;
}

// duration in day
export const FormatDateCountDown = (duration) => {
  if (!isNaN(duration)) {
    if (duration > 0) {
      const dur = moment.duration(duration, "days");
      dur.locale("ID");
      const y = Math.floor(dur.asYears());
      const m = Math.floor(
        y > 0 ? dur.subtract(y, "years").asMonths() : dur.asMonths(),
      );
      const d = Math.floor(
        m > 0 ? dur.subtract(m, "month").days() : dur.days(),
      );

      return `${y > 0 ? y + " thn, " : ""}${
        m > 0 ? m + " bln, " : ""
      }${d} hari`;
    } else {
      return "Telah berakhir";
    }
  } else {
    return "";
  }
};

export const isValidUrl = (str) => {
  var pattern = new RegExp(
    "^(https?:\\/\\/)?" + // protocol
      "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
      "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
      "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
      "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
      "(\\#[-a-z\\d_]*)?$",
    "i",
  ); // fragment locator
  return !!pattern.test(str);
};

export const isValidEmail = (str = "") => {
  return str.match(
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  );
};

export function isValidHttpUrl(string) {
  let url;

  try {
    url = new URL(string);
  } catch (_) {
    return false;
  }

  return url.protocol === "http:" || url.protocol === "https:";
}

export function setUpUrl(url = "", isHttps = true, addHttp = false) {
  if (isValidHttpUrl(url)) {
    return url;
  } else {
    // console.log(url,url.includes("www."))
    if (url.includes("www.")) {
      let newUrl = `${isHttps ? "https://" : "http://"}${url}`;
      // console.log(newUrl)
      return newUrl;
    } else {
      if (addHttp) {
        return "https://" + url;
      }
      return "";
    }
  }
}

export const isJsonString = (str) => {
  try {
    JSON.parse(str);
    return true;
  } catch (error) {
    return false;
  }
};

export const convertPeriodeKerjasama = (p) => {
  let periode = `${p}`;
  return periode.substring(periode.length, 2) == "0"
    ? periode.split(".")[0]
    : periode;
};

export const fixBootstrapDropdown = () => {
  if (window?.bootstrap?.Dropdown) {
    let dropdowns = document.querySelectorAll(".dropdown-toggle");
    dropdowns = [...dropdowns].map(
      (dropdownToggleEl) =>
        new window.bootstrap.Dropdown(dropdownToggleEl, {
          popperConfig(defaultBsPopperConfig) {
            return { ...defaultBsPopperConfig, strategy: "fixed" };
          },
        }),
    );
  }
};
