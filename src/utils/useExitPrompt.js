import { useState, useEffect } from "react";

const initBeforeUnLoad = (showExitPrompt) => {
  window.onbeforeunload = (event) => {
    if (showExitPrompt) {
      const e = event || window.event;
      e.preventDefault();
      if (e) {
        e.returnValue = "";
      }
      return "";
    }
  };
};

const initBeforeLoad = (showExitPrompt) => {
  window.onload = function () {
    initBeforeUnLoad(showExitPrompt);
  };
};

export function useExitPrompt(bool) {
  const [showExitPrompt, setShowExitPrompt] = useState(bool);

  initBeforeLoad();

  useEffect(() => {
    initBeforeLoad();
    initBeforeUnLoad(showExitPrompt);
  }, [showExitPrompt]);

  return [showExitPrompt, setShowExitPrompt];
}

export const withExitPrompt = (Component) => {
  const fnc = ({ defaultShowExitPrompt = true, ...props }) => {
    const [showExitPrompt, setShowExitPrompt] = useExitPrompt(
      defaultShowExitPrompt,
    );
    return (
      <Component
        showExitPrompt={showExitPrompt}
        setShowExitPrompt={setShowExitPrompt}
        {...props}
      />
    );
  };
  return fnc;
};
